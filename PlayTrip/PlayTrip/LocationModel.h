//
//  LocationModel.h
//  PlayTrip
//
//  Created by ibuildx on 6/18/17.
//  Copyright © 2017 SamirMAC. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "PlanModel.h"
#import "Country.h"
#import "DistrictModel.h"

@protocol PlanModel;

@protocol Country_data;
@interface Country_data : JSONModel

@property (nonatomic) NSString *_id;
@property (nonatomic) NSString *name;
@property (nonatomic) NSNumber<Optional> *__v;
@property (nonatomic) NSNumber<Optional> *sqlId;
@property (nonatomic) NSString<Optional> *create_date;
@property (nonatomic) NSNumber<Optional> *is_deleted;

//@property (nonatomic) Country<Optional> *country;
@property (nonatomic) NSString<Optional> *country;

@property (nonatomic) NSNumber<Optional> *status;
@property (nonatomic) NSString<Optional> *image;

@property (nonatomic) DistrictModel<Optional> *district;


@property (nonatomic) NSString<Optional> *name_tc;
@property (nonatomic) NSString<Optional> *name_sc;
@property (nonatomic) NSString<Optional> *img_url;



@end


@interface LocationModel : JSONModel
@property (nonatomic) Country_data<Optional> *country_data;
@property (nonatomic) Country_data<Optional> *district_data;
@property (nonatomic) Country_data<Optional> *city_data;

@property (nonatomic) NSMutableArray<Optional,PlanModel> *video_tour;
@property (nonatomic) NSNumber<Optional> *count;

@end


/*
 "district_data": {
 "_id": "592dda728a82c0171c0e7320",
 "name": "Lakshmipur",
 "country": "592dd05c3e01330be8376c7f",
 "sqlId": 368,
 "__v": 0,
 "create_date": "2017-05-30T20:47:46.490Z",
 "is_deleted": false,
 "status": false,
 "image": ""
 }
 
 
 
 "country_data": {
 "_id": "592dd05c3e01330be8376cd2",
 "name": "India",
 "__v": 0,
 "sqlId": 101,
 "create_date": "2017-05-30T20:04:44.946Z",
 "is_deleted": false
 },
*/

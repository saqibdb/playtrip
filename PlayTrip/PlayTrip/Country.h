//
//  Country.h
//  PlayTrip
//
//  Created by ibuildx on 5/31/17.
//  Copyright © 2017 SamirMAC. All rights reserved.
//

#import <JSONModel/JSONModel.h>
@protocol Country;
@interface Country : JSONModel


@property (nonatomic) NSString *_id;
@property (nonatomic) NSString<Optional> *name;
@property (nonatomic) NSNumber<Optional> *sqlId;
@property (nonatomic) NSNumber<Optional> *__v;
@property (nonatomic) NSString<Optional> *create_date;
@property (nonatomic) NSNumber<Optional> *is_deleted;

@property (nonatomic) NSString<Optional> *name_tc;
@property (nonatomic) NSString<Optional> *name_sc;
@property (nonatomic) NSString<Optional> *img_url;

@property (nonatomic) NSString<Optional> *iso;


/*
 "_id": "592dd05c3e01330be8376c8c",
 "name": "British Indian Ocean Territory",
 "__v": 0,
 "name_tc": "英屬印度洋領地",
 "name_sc": "英属印度洋领地",
 "iso": "IO",
 "sqlId": 31,
 "create_date": "2017-05-30T20:04:44.910Z",
 "is_deleted": false
 

 */



@end

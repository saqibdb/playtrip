
#import "MenuButtonViewController.h"
#import "ColorConstants.h"
#import "DeviceInfo.h"
#import "MapViewController.h"
#import "ProfileViewController.h"
#import "NotificationViewController.h"
#import "KGModalWrapper.h"
#import "CreateMyPlanView.h"
#import "VideoViewController.h"
#import "CreateDraftView.h"
#import "Localisator.h"
#import "DraftTour.h"
#import "MapAttractionsViewController.h"
#import "DraftTourViewController.h"

@interface MenuButtonViewController ()
@end

@implementation MenuButtonViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.translucent = NO;
    [self.menuView1 setHidden:true];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.menuView1.backgroundColor = [UIColor clearColor];
    // Arun :: To do try to find a proper solution instead of adding it in every time.
    
    //[self.view layoutIfNeeded];
    [self addBaseButtons];
    [self addMenuButtons];
    [self.menuView1 setHidden:true];
    
}


-(void)viewDidAppear:(BOOL)animated {
    self.navigationController.navigationBar.translucent = NO;
    
}

-(void)updateMenuButtons {
    [self addBaseButtons];
    
    
   
    
    
    [self addMenuButtons];
    self.menuHomeLabel.text = LOCALIZATION(@"home");
    self.menuProfileLabel.text = LOCALIZATION(@"profile");
    self.menuBookingLabel.text = LOCALIZATION(@"booking");
    self.menuMapLabel.text = LOCALIZATION(@"map");
    self.menuCreateLabel.text = LOCALIZATION(@"create_my_plan_menu");
}

-(void)addMenuButtons {
    [self addHomeView];
    [self addProfileView];
    //[self addNotificationView];
    [self addBookingView];
    [self addMapView];
    [self addCreateView];
}

-(void)addHomeView {
    if(_homeView){
        [_homeView removeFromSuperview];
    }
 _homeView = [[UIView alloc]init];
    
    CGFloat buttonWidth = _menuView1.frame.size.width / 5;
    
    _homeView.frame = CGRectMake(0, 5, buttonWidth, buttonWidth);

    UIButton * homeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [homeButton setImage:nil forState:UIControlStateNormal];
    homeButton.frame = CGRectMake(5, 0, (buttonWidth / 4) * 3, (buttonWidth / 4) * 3);
    
    UIImageView * homeImage = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"home_menu.png"]];
    homeImage.frame = CGRectMake(10, 5, buttonWidth / 2.5, buttonWidth / 2.5);
    
    homeImage.center = homeButton.center;

    
    homeButton.layer.cornerRadius = homeButton.layer.frame.size.height / 2;
    homeButton.backgroundColor = [ColorConstants appYellowColor];
    [homeButton addTarget:self action:@selector(homeButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    self.menuHomeLabel = [[UILabel alloc] init];
    self.menuHomeLabel.frame = CGRectMake(5, ((buttonWidth / 4) * 3) + 2, (buttonWidth / 4) * 3, 12.0);
    self.menuHomeLabel.textAlignment = NSTextAlignmentCenter;
    self.menuHomeLabel.font = [self.menuHomeLabel.font fontWithSize:8];
    self.menuHomeLabel.text = LOCALIZATION(@"home");
    [_homeView addSubview:homeButton];
    [_homeView addSubview:self.menuHomeLabel];
    [_homeView addSubview:homeImage];
    [self.menuView1 addSubview:_homeView];
}

-(void)addProfileView {
    
    if(_profileView){
        [_profileView removeFromSuperview];
    }
     _profileView = [[UIView alloc]init];
    CGFloat buttonWidth = _menuView1.frame.size.width / 5;

    
    _profileView.frame = CGRectMake((buttonWidth * 1), 5, buttonWidth, buttonWidth);
    UIButton * profileButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [profileButton setImage:nil forState:UIControlStateNormal];
   
    UIImageView * profileImage = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"user.png"]];
    profileImage.frame = CGRectMake(10, 5, buttonWidth / 2.5, buttonWidth / 2.5);
    
    
    
    
    profileButton.frame = CGRectMake(5, 0, (buttonWidth / 4) * 3, (buttonWidth / 4) * 3);
    
    
    profileImage.center = profileButton.center;
    
    
    profileButton.layer.cornerRadius = profileButton.layer.frame.size.height / 2;
    profileButton.backgroundColor = [ColorConstants appYellowColor];
    [profileButton addTarget:self action:@selector(profileButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    self.menuProfileLabel = [[UILabel alloc] init];
    self.menuProfileLabel.frame = CGRectMake(5, ((buttonWidth / 4) * 3) + 2, (buttonWidth / 4) * 3, 12.0);
    self.menuProfileLabel.textAlignment = NSTextAlignmentCenter;
    self.menuProfileLabel.font = [self.menuProfileLabel.font fontWithSize:8];
    self.menuProfileLabel.text = LOCALIZATION(@"profile");
    
    [_profileView addSubview:profileButton];
    [_profileView addSubview:self.menuProfileLabel];
    [_profileView addSubview:profileImage];
    [self.menuView1 addSubview:_profileView];
}

-(void)addNotificationView {
    
 _notificationView = [[UIView alloc]init];
    _notificationView.frame = CGRectMake(50, 5, 40.0, 40.0);
    UIButton * notificationButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [notificationButton setImage:nil forState:UIControlStateNormal];
    notificationButton.frame = CGRectMake(5, 0, 30.0, 30.0);
    
    UIImageView * notificationImage = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"notification_bell.png"]];
    notificationImage.frame = CGRectMake(10, 5, 20.0, 20.0);
    
    notificationButton.layer.cornerRadius = notificationButton.layer.frame.size.height / 2;
    notificationButton.backgroundColor = [ColorConstants appYellowColor];
    [notificationButton addTarget:self action:@selector(notificationButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    UILabel * notificationLabel = [[UILabel alloc] init];
    notificationLabel.frame = CGRectMake(-5, 32, 50.0, 12.0);
    notificationLabel.textAlignment = NSTextAlignmentCenter;
    notificationLabel.font = [notificationLabel.font fontWithSize:8];
    notificationLabel.text = @"Notification";
    [_notificationView addSubview:notificationButton];
    [_notificationView addSubview:notificationLabel];
    [_notificationView addSubview:notificationImage];
    
    [self.menuView1 addSubview:_notificationView];
}

-(void)addBookingView {
    if(_bookingView){
        [_bookingView removeFromSuperview];
    }
 _bookingView = [[UIView alloc]init];
    CGFloat buttonWidth = _menuView1.frame.size.width / 5;

    _bookingView.frame = CGRectMake((buttonWidth * 2), 5, buttonWidth, buttonWidth);
    UIButton * bookingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [bookingButton setImage:nil forState:UIControlStateNormal];
    bookingButton.frame = CGRectMake(5, 0, (buttonWidth / 4) * 3, (buttonWidth / 4) * 3);
    
    UIImageView * bookImage = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"book.png"]];
    bookImage.frame = CGRectMake(10, 5, buttonWidth / 2, buttonWidth / 2);
    
    
    bookImage.center = bookingButton.center;

    
    bookingButton.layer.cornerRadius = bookingButton.layer.frame.size.height / 2;
    bookingButton.backgroundColor = [ColorConstants appYellowColor];
    [bookingButton addTarget:self action:@selector(bookingButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    self.menuBookingLabel = [[UILabel alloc] init];
    self.menuBookingLabel.frame = CGRectMake(5, ((buttonWidth / 4) * 3) + 2, (buttonWidth / 4) * 3, 12.0);
    self.menuBookingLabel.textAlignment = NSTextAlignmentCenter;
    self.menuBookingLabel.font = [self.menuBookingLabel.font fontWithSize:8];
    self.menuBookingLabel.text = LOCALIZATION(@"booking");
    [_bookingView addSubview:bookingButton];
    [_bookingView addSubview:self.menuBookingLabel];
    [_bookingView addSubview:bookImage];
    [self.menuView1 addSubview:_bookingView];
}

-(void)addMapView {
    if(_mapView){
        [_mapView removeFromSuperview];
    }
  _mapView = [[UIView alloc]init];
    CGFloat buttonWidth = _menuView1.frame.size.width / 5;

    _mapView.frame = CGRectMake((buttonWidth * 3), 5, buttonWidth, buttonWidth);
    UIButton * mapButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [mapButton setImage:nil forState:UIControlStateNormal];
    mapButton.frame = CGRectMake(5, 0, (buttonWidth / 4) * 3, (buttonWidth / 4) * 3);
    
    UIImageView * mapImage = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"pin_menu.png"]];
    mapImage.frame = CGRectMake(10, 5, buttonWidth / 2, buttonWidth / 2);
    
    mapImage.center = mapButton.center;

    
    
    mapButton.layer.cornerRadius = mapButton.layer.frame.size.height / 2;
    mapButton.backgroundColor = [ColorConstants appYellowColor];
    [mapButton addTarget:self action:@selector(mapButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    self.menuMapLabel = [[UILabel alloc] init];
    self.menuMapLabel.frame = CGRectMake(5, ((buttonWidth / 4) * 3) + 2, (buttonWidth / 4) * 3, 12.0);
    self.menuMapLabel.textAlignment = NSTextAlignmentCenter;
    self.menuMapLabel.font = [self.menuMapLabel.font fontWithSize:8];
    self.menuMapLabel.text = LOCALIZATION(@"map");
    [_mapView addSubview:mapButton];
    [_mapView addSubview:self.menuMapLabel];
    [_mapView addSubview:mapImage];
    [self.menuView1 addSubview:_mapView];
}

-(void)addCreateView {
    if(_createView){
        [_createView removeFromSuperview];
    }
    _createView = [[UIView alloc]init];
    CGFloat buttonWidth = _menuView1.frame.size.width / 5;

    
    _createView.frame = CGRectMake((buttonWidth * 4), 5, buttonWidth, buttonWidth);
    UIButton * createButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [createButton setImage:nil forState:UIControlStateNormal];
    createButton.frame = CGRectMake(5, 0, (buttonWidth / 4) * 3, (buttonWidth / 4) * 3);
    
    UIImageView * createImage = [[UIImageView alloc] initWithImage: [UIImage imageNamed:@"add_date.png"]];
    createImage.frame = CGRectMake(10, 5, buttonWidth / 2, buttonWidth / 2);
    
    createImage.center = createButton.center;

    
    createButton.layer.cornerRadius = createButton.layer.frame.size.height / 2;
    createButton.backgroundColor = [ColorConstants appYellowColor];
    [createButton addTarget:self action:@selector(createButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    self.menuCreateLabel = [[UILabel alloc] init];
    self.menuCreateLabel.frame = CGRectMake(5, ((buttonWidth / 4) * 3) + 2, (buttonWidth / 4) * 3, 24.0);
    self.menuCreateLabel.textAlignment = NSTextAlignmentCenter;
    self.menuCreateLabel.font = [self.menuCreateLabel.font fontWithSize:8];
    self.menuCreateLabel.numberOfLines = 2;
    self.menuCreateLabel.text = LOCALIZATION(@"create_my_plan_menu");
    [_createView addSubview:createButton];
    [_createView addSubview:self.menuCreateLabel];
    [_createView addSubview:createImage];
    [self.menuView1 addSubview:_createView];
}

-(void) addBaseButtons {
    
    
    
    
    
    if(!_leftButton){
        _leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_leftButton setImage:[UIImage imageNamed:@"menu_left.png"] forState:UIControlStateNormal];
        _leftButton.frame = CGRectMake(0.0,[AppDelegate appDelegate].window.frame.size.height - 114, 50.0, 50.0);
        
        
        _leftButton.backgroundColor = [UIColor clearColor];
        [_leftButton addTarget:self action:@selector(leftButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
   
    if(!_rightButton){
        _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rightButton setImage:[UIImage imageNamed:@"menu_right.png"] forState:UIControlStateNormal];
        _rightButton.frame = CGRectMake([AppDelegate appDelegate].window.frame.size.width  - 50, [AppDelegate appDelegate].window.frame.size.height - 114, 50.0, 50.0);
        
        
        _rightButton.backgroundColor = [UIColor clearColor];
        [_rightButton addTarget:self action:@selector(rightButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    }
    
    
    _menuView1 = [[UIView alloc] init];
    
    CGRect mf = [AppDelegate appDelegate].window.frame;
    NSLog(@"MF: %f", mf.size.width);
    _menuView1.frame = CGRectMake(0,mf.size.height - ((mf.size.width / 5) + 124), mf.size.width, (mf.size.width / 5) + 10);
    _menuView1.backgroundColor = [UIColor whiteColor];
    
    self.menuView1.hidden = true;
    
    [self.view addSubview:_leftButton];
    [self.view addSubview:_rightButton];
    [self.view addSubview:_menuView1];
}

-(void)rightButtonClicked {
    if (self.menuView1.hidden == true) {
        [self updateMenuButtons];
        self.menuView1.hidden = false;
    } else {
        self.menuView1.hidden = true;
    }
}

-(void)leftButtonClicked {
    
    Account * account = [AccountManager Instance].activeAccount;
    if(account){
        if (account.isSkip == true) {
            [self.navigationController popToRootViewControllerAnimated:NO];
            [[AppDelegate appDelegate] showLogin:self];
            return;
        }
    }
    
    DraftTour * dTour = [DraftTour getDraft];
    if (!dTour) {
        CreateDraftView * view = [CreateDraftView initWithNib];
        [KGModalWrapper showWithContentView:view];
    } else {
//        VideoViewController * controller = [VideoViewController initViewController];
        DraftTourViewController * controller = [DraftTourViewController initViewController];
        [self.navigationController pushViewController:controller animated:true];
    }
}

-(void)homeButtonClicked {
    self.menuView1.hidden = true;
    [self.navigationController popToRootViewControllerAnimated:true];
}

-(void)profileButtonClicked {
    self.menuView1.hidden = true;

    ProfileViewController * controller = [ProfileViewController initViewController];
    UIViewController * topController = [Utils getTopViewControllerFromNavControl];
    if ([controller class] != [topController class]) {
        [self.navigationController pushViewController:controller animated:false];
    }
}

-(void)notificationButtonClicked {
    self.menuView1.hidden = true;
    NotificationViewController * controller = [NotificationViewController initViewController];
    UIViewController * topController = [Utils getTopViewControllerFromNavControl];
    if ([controller class] != [topController class]) {
        [self.navigationController pushViewController:controller animated:true];
    }
}

-(void)bookingButtonClicked {
    self.menuView1.hidden = true;
}

-(void)mapButtonClicked {
    self.menuView1.hidden = true;
    MapViewController * controller = [MapViewController initViewController];
//    MapAttractionsViewController * controller = [MapAttractionsViewController initViewController];
    UIViewController * topController = [Utils getTopViewControllerFromNavControl];
    if ([controller class] != [topController class]) {
        [self.navigationController pushViewController:controller animated:true];
    }
}

-(void)createButtonClicked {
    self.menuView1.hidden = true;
    CreateMyPlanView *view = [CreateMyPlanView initWithNib];
    [KGModalWrapper showWithContentView:view];
}


-(void)viewWillDisappear:(BOOL)animated {
    self.menuView1.hidden = true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

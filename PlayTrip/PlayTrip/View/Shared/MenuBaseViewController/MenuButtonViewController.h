
#import <UIKit/UIKit.h>
#import "LoginDelegate.h"
#import "CommonNavigationController.h"

@interface MenuButtonViewController : UIViewController <LoginDelegate> {
    UIViewController * topViewController;
}

@property (strong, nonatomic) UILabel* menuHomeLabel;
@property (strong, nonatomic) UILabel* menuProfileLabel;
@property (strong, nonatomic) UILabel* menuBookingLabel;
@property (strong, nonatomic) UILabel* menuMapLabel;
@property (strong, nonatomic) UILabel* menuCreateLabel;

@property (strong, nonatomic) UIView *menuView1;
@property (strong, nonatomic) UIButton * leftButton;
@property (strong, nonatomic) UIButton * rightButton;
@property (strong, nonatomic) UIView * homeView;
@property (strong, nonatomic) UIView * profileView;
@property (strong, nonatomic) UIView * notificationView;
@property (strong, nonatomic) UIView * bookingView;
@property (strong, nonatomic) UIView * mapView;
@property (strong, nonatomic) UIView * createView;
@end

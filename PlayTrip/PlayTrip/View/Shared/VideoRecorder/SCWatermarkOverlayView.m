//
//  SCWatermarkOverlayView.m
//  SCRecorderExamples
//
//  Created by Simon CORSIN on 16/06/15.
//
//

#import "SCWatermarkOverlayView.h"

@interface SCWatermarkOverlayView() {
    UILabel *_watermarkLabel;
//    UILabel *_watermarkLabel1;
    UILabel *_timeLabel;
}


@end

@implementation SCWatermarkOverlayView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        _watermarkLabel = [UILabel new];
        _watermarkLabel.textColor = [UIColor whiteColor];
        _watermarkLabel.font = [UIFont boldSystemFontOfSize:30];
        _watermarkLabel.text = @"PlayTrip";
        
        
//        _watermarkLabel1 = [UILabel new];
//        _watermarkLabel1.text = @"Caption Here. Caption Here. Caption Here";
//        _watermarkLabel1.backgroundColor = [UIColor darkGrayColor];
//        _watermarkLabel1.textColor = [UIColor whiteColor];
//        _watermarkLabel.font = [UIFont boldSystemFontOfSize:70];

        
        _timeLabel = [UILabel new];
        _timeLabel.font = [UIFont boldSystemFontOfSize:40];
        
        [self addSubview:_watermarkLabel];
        [self addSubview:_timeLabel];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
        
    static const CGFloat inset = 8;
    
    CGSize size = self.bounds.size;
    
    [_watermarkLabel sizeToFit];
    CGRect watermarkFrame = _watermarkLabel.frame;
    watermarkFrame.origin.x = size.width - watermarkFrame.size.width - inset;
    watermarkFrame.origin.y = 10;//size.height - watermarkFrame.size.height - inset;
    _watermarkLabel.frame = watermarkFrame;
    
//    [_watermarkLabel1 sizeToFit];
//    CGRect watermarkFrame1 = _watermarkLabel1.frame;
//    watermarkFrame1.origin.x = 20;//size.width - watermarkFrame1.size.width - inset;
//    watermarkFrame1.origin.y = size.height - 50;
//    _watermarkLabel.frame = watermarkFrame1;
    
    [_timeLabel sizeToFit];
    CGRect timeLabelFrame = _timeLabel.frame;
    timeLabelFrame.origin.y = inset;
    timeLabelFrame.origin.x = inset;
    _timeLabel.frame = timeLabelFrame;
}

- (void)updateWithVideoTime:(NSTimeInterval)time {
//    NSDate *currentDate = [self.date dateByAddingTimeInterval:time];
    _timeLabel.text = @"";
//    _timeLabel.text = [NSString stringWithFormat:@"%@", currentDate];
}

@end


#import "Utils.h"
#import "Constants.h"
#import "CommonNavigationController.h"

@implementation Utils

+(void)showAlertWithMessage:(NSString *)message{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:actionOk];
    [[Utils getTopViewController] presentViewController:alertController animated:YES completion:nil];
}

+ (UIViewController*) getTopViewController {
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    
    return topController;
}

+ (UIViewController*) getTopViewControllerFromNavControl {
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController) {
        topController = topController.presentedViewController;
    }
    CommonNavigationController * commonController = (CommonNavigationController *)topController;
    return commonController.topViewController;
}

+ (NSString *)getTimeString:(int)minutes {
    NSTimeInterval duration = (double)(minutes*60);
    NSInteger hours = floor(duration/(60*60));
    NSInteger mins = floor((duration/60) - hours * 60);
    
    return [NSString stringWithFormat:@"%02ld:%02ld", (long)hours,(long)mins];
}

+ (int)RandomId {
    int r = arc4random_uniform(100000);
    r = r * -1;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"NEED_BACKUP_NOTIFICATION" object:nil];
    
    return r;
}

+(UIImage *)generateThumbImageFromURL:(NSURL*)partOneUrl {
    AVURLAsset *asset1 = [[AVURLAsset alloc] initWithURL:partOneUrl options:nil];
    AVAssetImageGenerator *generate1 = [[AVAssetImageGenerator alloc] initWithAsset:asset1];
    generate1.appliesPreferredTrackTransform = YES;
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 2);
    CGImageRef oneRef = [generate1 copyCGImageAtTime:time actualTime:NULL error:&err];
    UIImage *one = [[UIImage alloc] initWithCGImage:oneRef];
    NSData *thumbData = UIImageJPEGRepresentation(one,0.2);
    UIImage * img1 = [UIImage imageWithData:thumbData];
    return img1;
}

+ (int)RandomIdPlus {
    int r = arc4random_uniform(100000);
    
    return r;
}

+ (NSString*) getNonmilitaryTime:(NSString*) str{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm"];
    
    NSDate *dt = [formatter dateFromString:str];
    [formatter setDateFormat:@"h:mm a"];
    return [formatter stringFromDate:dt];
}

+ (NSString*) getMilitaryTime:(NSString*) str{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];
    
    NSDate *dt = [formatter dateFromString:str];
    [formatter setDateFormat:@"HH:mm"];
    return [formatter stringFromDate:dt];
}

+ (NSString *) addSuffixToNumber:(int) number
{
    NSString *suffix;
    int ones = number % 10;
    int temp = floor(number/10.0);
    int tens = temp%10;
    
    if (tens ==1) {
        suffix = @"th";
    } else if (ones ==1){
        suffix = @"st";
    } else if (ones ==2){
        suffix = @"nd";
    } else if (ones ==3){
        suffix = @"rd";
    } else {
        suffix = @"th";
    }
    
    NSString *completeAsString = [NSString stringWithFormat:@"%d%@",number,suffix];
    return completeAsString;
}

+ (NSDateFormatter*)getDateFormatterWithFormatString:(NSString*)format
{
    static NSDateFormatter* formatter = nil;
    if (!formatter)
    {
        formatter = [[NSDateFormatter alloc] init];
    }
    [formatter setDateFormat:format];
    return formatter;
}



+(NSString *)dateFormatMMddyyyy:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *dateformat = [formatter stringFromDate:date];
    return dateformat;
}

+(NSString *)dateFormatyyyyMMdd:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateformat = [formatter stringFromDate:date];
    return dateformat;
}

+(NSString *)dateFormatyyyyMMddHHmm:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *dateformat = [formatter stringFromDate:date];
    
    NSDateFormatter *dFormatter = [[NSDateFormatter alloc] init];
    [dFormatter setDateFormat:@"HH:mm"];
    NSString *timeformat = [dFormatter stringFromDate:date];
    
    NSString * totalStr = [NSString stringWithFormat:@"%@ %@", dateformat, timeformat];
    
    return totalStr;
}
+(NSString *)timeFormatHHmm:(NSDate *)date{
   NSDateFormatter *dFormatter = [[NSDateFormatter alloc] init];
    [dFormatter setDateFormat:@"hh:mm a"];
    NSString *timeformat = [dFormatter stringFromDate:date];
    
    NSString * totalStr = [NSString stringWithFormat:@"%@", timeformat];
    
    return totalStr;
}


+(NSString *)dateFormatMMMyyyy:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM yyyy"];
    NSString *dateformat = [formatter stringFromDate:date];
    return dateformat;
}

+(NSString *)dateFormatddMM:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM"];
    NSString *dateformat = [formatter stringFromDate:date];
    return dateformat;
}

+(NSString *)dateFormatddMMyyy:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyy"];
    NSString *dateformat = [formatter stringFromDate:date];
    return dateformat;
}

+(NSString *)getDayFromDate:(NSDate *)date {
    NSCalendar * calendar = [NSCalendar currentCalendar];
    NSDateComponents * dateComponents = [calendar components: NSCalendarUnitDay | NSCalendarUnitWeekday fromDate:date];
    
    return calendar.standaloneWeekdaySymbols[dateComponents.weekday - 1];
}





+(NSString *)getDayFromDateInLanguage:(NSDate *)date andLocaleIdentifier:(NSString *)identifier {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];

    
    
    dateFormatter.locale=[[NSLocale alloc] initWithLocaleIdentifier:identifier];
  
    
    
    
    dateFormatter.dateFormat=@"EEEE";
    return [[dateFormatter stringFromDate:date] capitalizedString];
}

+(NSString *)getDayShortFromDate:(NSDate *)date {
    NSCalendar * calendar = [NSCalendar currentCalendar];
    NSDateComponents * dateComponents = [calendar components: NSCalendarUnitDay | NSCalendarUnitWeekday fromDate:date];
    return calendar.shortWeekdaySymbols[dateComponents.weekday - 1];
}


+ (NSDate *)addDays:(NSInteger)days toDate:(NSDate *)originalDate {
    NSDateComponents *components= [[NSDateComponents alloc] init];
    [components setDay:days];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    return [calendar dateByAddingComponents:components toDate:originalDate options:0];
}

+(NSDate *)reduceDays:(NSInteger)days toDate:(NSDate *)originalDate {
    NSDate *newDate = [originalDate dateByAddingTimeInterval:-(days*24*60*60)];
    return newDate;
}

+(NSString *)getDaysBetweenFromDate:(NSDate *)from_date andToDate:(NSDate *)to_date {
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:from_date
                                                          toDate:to_date
                                                         options:0];
    NSString * days = [NSString stringWithFormat:@"%ld", (long)[components day]+1];
    return days;
}

+(NSString *)strFromDate:(NSDate *)date {
    NSString * finalStr = @"";
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MM/dd/yyyy"];
    NSString *a = [dateFormatter stringFromDate:date];
    finalStr = a;
    
    return finalStr;
}


+(NSString *)getRequiredStringForDate:(NSDate *)date { // Arun :: In format : (dd/MM day)
    
    NSString * dateString = [self dateFormatddMM:date];
    
    NSCalendar * calendar = [NSCalendar currentCalendar];
    
    NSDateComponents * dateComponents = [calendar components: NSCalendarUnitDay | NSCalendarUnitWeekday fromDate:date];
    NSString * dayString = calendar.shortWeekdaySymbols[dateComponents.weekday - 1];
    
    NSString * fullString = [NSString stringWithFormat: @"%@ %@", dateString, dayString];
    return fullString;
}

+(NSString *)dateFormathhmma:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];
    NSString *timeformat = [formatter stringFromDate:date];
    return timeformat;
}

+(NSString *)getCorrectDateFormathhmmaa:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"hh:mm a"];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [formatter setTimeZone:timeZone];
    NSString *timeformat = [formatter stringFromDate:date];
    return timeformat;
}


+(NSString *)getHourAndMinutesFromDate:(NSString *)dateStr{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzzz"];
    NSDate *date = [dateFormatter dateFromString:dateStr];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
    NSInteger hour = [components hour];
    NSInteger minute = [components minute];
    NSString *resultString = [NSString stringWithFormat:@"%ld : %ld", (long)hour, (long)minute];
    return resultString;
}

+(BOOL)validateEmailWithString:(NSString*)email {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+(BOOL)isPastDate:(NSDate *)pastdate {
    NSDate *today = [NSDate date]; // it will give you current date
    NSComparisonResult result;
    //has three possible values: NSOrderedSame,NSOrderedDescending, NSOrderedAscending
    
    result = [today compare:pastdate]; // comparing two dates
    
    if(result==NSOrderedAscending){
        return false;
    }
    else if(result==NSOrderedDescending){
        return true;
    }
    else{
        return false;
    }
}

+ (NSString *)getHeaderVersion {
    
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSString * build = [[NSBundle mainBundle] objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    NSString * iOSVersion =[UIDevice currentDevice].systemName;
    NSString * ver= [NSString stringWithFormat:@"iOS %@ %@ %@",version,build,iOSVersion];
    return ver;
    
}

+ (NSDate*)dateJSONTransformer:(NSString*)dateString {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ssZZZ"];
    return [dateFormatter dateFromString:dateString];
}

+ (NSDate*) dateFrom:(NSString*)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSDate *dt = [formatter dateFromString:[NSString stringWithFormat:@"%@", date]];
    return dt;
    
}

+ (NSString *)getCurrencyStringFromAmount:(float)amount {
    return [Utils getCurrencyStringFromAmountString:[NSString stringWithFormat:@"%.2f", amount]];
}

+ (NSString *)getCurrencyStringFromAmountString:(NSString *)amount {
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    [formatter setNumberStyle: NSNumberFormatterCurrencyStyle];
    [formatter setLenient:YES];
    [formatter setGeneratesDecimalNumbers:YES];
    NSDecimalNumber *number = (NSDecimalNumber*) [formatter numberFromString:amount];
    return [formatter stringFromNumber:number];
}

+(float)getValueFromCurrencyStyleString:(NSString *)str{
    NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
    [nf setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSNumber *number = [nf numberFromString:str];
    return [number floatValue];
}

CGRect TEXT_SIZE(NSString* str, int font_size) {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGRect size = [str boundingRectWithSize:CGSizeMake(screenRect.size.width, screenRect.size.height)
                                    options:NSStringDrawingUsesLineFragmentOrigin
                                 attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:font_size]}
                                    context:nil];
    return size;
}



+(void)getAddressFromLocation:(CLLocation *)location complationBlock:(addressCompletion)completionBlock {
    __block CLPlacemark* placemark;
    __block NSString *address = nil;
    
    CLGeocoder* geocoder = [CLGeocoder new];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
         if (error == nil && [placemarks count] > 0) {
             placemark = [placemarks lastObject];
             address = [NSString stringWithFormat:@"%@, %@ %@", placemark.name, placemark.postalCode, placemark.locality];
             completionBlock(address);
         }
     }];
}

+(void)getPlaceFromLocation:(CLLocation *)location complationBlock:(addressCompletionWithPlace)completionBlock {
    __block CLPlacemark* placemark;
    CLGeocoder* geocoder = [CLGeocoder new];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        if (error == nil && [placemarks count] > 0) {
            placemark = [placemarks lastObject];
            completionBlock(placemark);
        }
    }];
}



+(BOOL)isValid:(id)obj {
    if (obj && obj != nil && obj != (id)[NSNull null]) {
        return YES;
    } else {
        return NO;
    }
}


#pragma mark - UserDefault

+ (NSString *)getValueForKey:(NSString *)key{
    return [[NSUserDefaults standardUserDefaults] stringForKey:key];
}

+ (NSMutableArray *)getArrayForKey:(NSString *)key{
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

+ (void)setValue:(NSString *)value Key:(NSString *)key{
    [[NSUserDefaults standardUserDefaults] setObject:value forKey:key];
    [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:value, nil] forKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (void)setArray:(NSMutableArray *)value Key:(NSString *)key{
    
    NSMutableArray *archiveArray = [NSMutableArray arrayWithCapacity:value.count];
    for (NSString *str in value) {
        NSData *personEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:str];
        [archiveArray addObject:personEncodedObject];
    }

    [[NSUserDefaults standardUserDefaults] setObject:archiveArray forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
// Continue editing your last video?
//Continue  Cancel
+(void)addBottomBorderForTextField:(UITextField *)textField{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 2;
    border.borderColor = [UIColor whiteColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
}

+(CGFloat)widthOfString:(NSString *)string withFont:(UIFont *)font {
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:string attributes:attributes] size].width;
}

+(BOOL)getBOOLForKey:(NSString *)key{
    return [[NSUserDefaults standardUserDefaults] boolForKey:key];
}

+(void)removeKey:(NSString *)key{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
}


+(void)setBOOL:(BOOL)value Key:(NSString *)key{
    [[NSUserDefaults standardUserDefaults] setBool:value forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString*)encodeStringTo64:(NSString*)fromString
{
    NSData *plainData = [fromString dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String;
    if ([plainData respondsToSelector:@selector(base64EncodedStringWithOptions:)]) {
        base64String = [plainData base64EncodedStringWithOptions:kNilOptions];  // iOS 7+
    }
    return base64String;
}

+ (NSString *)mimeTypeForData:(NSData *)data {
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"image/jpeg";
            break;
        case 0x89:
            return @"image/png";
            break;
        case 0x47:
            return @"image/gif";
            break;
        case 0x49:
        case 0x4D:
            return @"image/tiff";
            break;
        case 0x25:
            return @"application/pdf";
            break;
        case 0xD0:
            return @"application/vnd";
            break;
        case 0x46:
            return @"text/plain";
            break;
        default:
            return @"application/octet-stream";
    }
    return nil;
}

+(UILabel *)roundCornersOnView:(UILabel *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius {
    if (tl || tr || bl || br) {
        UIRectCorner corner = 0;
        if (tl) corner = corner | UIRectCornerTopLeft;
        if (tr) corner = corner | UIRectCornerTopRight;
        if (bl) corner = corner | UIRectCornerBottomLeft;
        if (br) corner = corner | UIRectCornerBottomRight;
        
        UILabel *roundedView = view;
        UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:roundedView.bounds byRoundingCorners:corner cornerRadii:CGSizeMake(radius, radius)];
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.frame = roundedView.bounds;
        maskLayer.path = maskPath.CGPath;
        roundedView.layer.mask = maskLayer;
        return roundedView;
    }
    return view;
}

@end

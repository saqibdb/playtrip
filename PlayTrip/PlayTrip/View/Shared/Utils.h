
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CLAvailability.h>
#import <CoreLocation/CoreLocation.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

#define kLoginAutheticationHeader                               @"kLoginAutheticationHeader"


typedef void(^addressCompletion)(NSString *);

typedef void(^addressCompletionWithPlace)(CLPlacemark *);


@interface Utils : NSObject

+ (UIViewController*) getTopViewController;

+ (NSString*) getTimeString:(int)minutes;

+ (int)RandomId;

+ (int)RandomIdPlus;
+(UIImage *)generateThumbImageFromURL:(NSURL*)partOneUrl;
+ (NSString *) addSuffixToNumber:(int) number;

+ (NSString*) getNonmilitaryTime:(NSString*) str; // with AP/PM

+ (NSString*) getMilitaryTime:(NSString*) str; // with 24 hors

+(BOOL)validateEmailWithString:(NSString*)email;

+ (NSString *)getHeaderVersion;
+(NSString *)getHourAndMinutesFromDate:(NSString *)dateStr;
+(NSString *)dateFormatMMddyyyy:(NSDate *)date;
+(NSString *)dateFormatMMMyyyy:(NSDate *)date;
+(NSString *)dateFormatddMM:(NSDate *)date;
+(NSString *)dateFormathhmma:(NSDate *)date;
+(NSString *)dateFormatddMMyyy:(NSDate *)date;
+ (NSDate*) dateFrom:(NSString*)date;

+ (NSString *)getCurrencyStringFromAmount:(float)amount;

+ (NSString *)getCurrencyStringFromAmountString:(NSString *)amount;

+ (NSDateFormatter*)getDateFormatterWithFormatString:(NSString*)format;

+ (float)getValueFromCurrencyStyleString:(NSString *)str;

+ (NSDate *)addDays:(NSInteger)days toDate:(NSDate *)originalDate;
+(NSDate *)reduceDays:(NSInteger)days toDate:(NSDate *)originalDate;
NSPredicate* NON_DELETED_PREDECATE();

CGRect TEXT_SIZE(NSString* str, int font_size);

+ (NSString*)encodeStringTo64:(NSString*)fromString;

#pragma mark - Userdefault
+ (void) setValue:(NSString *)value Key:(NSString *)key;
+ (void)setArray:(NSMutableArray *)value Key:(NSString *)key;
+ (NSMutableArray *)getArrayForKey:(NSString *)key;
+ (NSString *) getValueForKey:(NSString *)key;
+(BOOL)getBOOLForKey:(NSString *)key;
+(void)removeKey:(NSString *)key;
+(void)setBOOL:(BOOL)value Key:(NSString *)key;
+ (UIViewController*) getTopViewControllerFromNavControl;
+(NSString *)getDayFromDate:(NSDate *)date;
+(NSString *)getDayFromDateInLanguage:(NSDate *)date andLocaleIdentifier:(NSString *)identifier;



+(NSString *)getDayShortFromDate:(NSDate *)date;
+(NSString *)getRequiredStringForDate:(NSDate *)date;
+(void)getAddressFromLocation:(CLLocation *)location complationBlock:(addressCompletion)completionBlock;
+(NSString *)dateFormatyyyyMMdd:(NSDate *)date;
+(NSString *)dateFormatyyyyMMddHHmm:(NSDate *)date;
+(void)addBottomBorderForTextField:(UITextField *)textField;
+(CGFloat)widthOfString:(NSString *)string withFont:(UIFont *)font;
+(BOOL) isValid:(id)obj;
+(void)showAlertWithMessage:(NSString *)message;
+ (NSString *)mimeTypeForData:(NSData *)data;
+ (UILabel *)roundCornersOnView:(UILabel *)view onTopLeft:(BOOL)tl topRight:(BOOL)tr bottomLeft:(BOOL)bl bottomRight:(BOOL)br radius:(float)radius;
+(NSString *)timeFormatHHmm:(NSDate *)date;
+(NSString *)strFromDate:(NSDate *)date;
+(NSString *)getDaysBetweenFromDate:(NSDate *)from_date andToDate:(NSDate *)to_date;

+(void)getPlaceFromLocation:(CLLocation *)location complationBlock:(addressCompletionWithPlace)completionBlock ;
@end

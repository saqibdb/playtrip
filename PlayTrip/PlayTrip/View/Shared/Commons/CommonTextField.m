
#import "CommonTextField.h"

@implementation CommonTextField


-(void)awakeFromNib {
    [super awakeFromNib];
    self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholder attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
}


@end

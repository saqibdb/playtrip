
#import "CommonButton.h"
#import "ColorConstants.h"

@implementation CommonButton

-(void)awakeFromNib {
    [super awakeFromNib];
    
    self.layer.cornerRadius = self.layer.frame.size.height / 2;
    self.backgroundColor = [ColorConstants appYellowColor];
}

@end

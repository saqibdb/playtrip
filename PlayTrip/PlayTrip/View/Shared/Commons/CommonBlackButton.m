
#import "CommonBlackButton.h"

@implementation CommonBlackButton

-(void)awakeFromNib {
    [super awakeFromNib];
    self.layer.borderColor = [UIColor darkGrayColor].CGColor;
    self.layer.borderWidth = 1;
    self.layer.cornerRadius = 13.0;
}


@end

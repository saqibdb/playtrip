
#import "CreateVideoTourView.h"
#import "Utils.h"

@implementation CreateVideoTourView

+ (id)initWithNib{
    CreateVideoTourView*  view = [[[NSBundle mainBundle] loadNibNamed:@"CreateVideoTourView" owner:nil options:nil] objectAtIndex:0];
    [view.tblView reloadData];
    view->lblTopMain =  [Utils roundCornersOnView:view->lblTopMain onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];
    return view;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return @"Itnerary Name";
    }else if (section == 1){
        return @"Language";
    }else{
        return @"Date";
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }else if (section == 1){
        return 1;
    }else{
        return 2;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    if (indexPath.section == 0) {
        cell.textLabel.text = @"Japan 6 Day Tour";
    }else if (indexPath.section == 1){
        cell.textLabel.text = @"English";
    }else{
        cell.textLabel.text = @"2016-9-14 (Sunday)";
    }

    return cell;
}

@end


#import "CreateDraftView.h"
#import "DraftTour.h"
#import "Account.h"
#import "AccountManager.h"
#import "VideoViewController.h"
#import "ColorConstants.h"
#import "Utils.h"
#import "KGModalWrapper.h"
#import "ActionSheetDatePicker.h"
#import "Constants.h"
#import "DraftTourViewController.h"
#import "Localisator.h"

@implementation CreateDraftView

+ (id)initWithNib{
    CreateDraftView*  view = [[[NSBundle mainBundle] loadNibNamed:@"CreateDraftView" owner:nil options:nil] objectAtIndex:0];
    
    view.dateView.layer.cornerRadius = view.dateView.layer.frame.size.height / 2;
    view.dateView.layer.borderColor = [ColorConstants appBrownColor].CGColor;
    view.dateView.layer.borderWidth = 1;
    
    view->selectDate = [NSDate date];
    NSString * day = [Utils getDayFromDate:[NSDate date]];
    NSLog(@"%@",day);
    NSString * date = [Utils dateFormatyyyyMMdd:[NSDate date]];
    view.lblDate.text= [NSString stringWithFormat:@"%@ (%@)", date, day];
    
    view.lblTopMain =  [Utils roundCornersOnView:view.lblTopMain onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];
    
    [view.lblTopMain setText:LOCALIZATION(@"create_video_tour")];
    [view.lblInitName setText:LOCALIZATION(@"intinerary_name")];
    [view.lblBeginDay setText:LOCALIZATION(@"begin_day")];
    [view.txtName setPlaceholder:LOCALIZATION(@"name")];
    [view.btnsave setTitle:LOCALIZATION(@"save") forState:UIControlStateNormal];
    [view.btnCancel setTitle:LOCALIZATION(@"cancel") forState:UIControlStateNormal];

    
    /*
    view.lblTopMain.text = [NSString stringWithFormat:NSLocalizedString(@"create_video_tour", nil)];
    view.lblInitName.text = [NSString stringWithFormat:NSLocalizedString(@"intinerary_name", nil)];
    view.lblBeginDay.text = [NSString stringWithFormat:NSLocalizedString(@"begin_day", nil)];
    //view.lblDate.text = [NSString stringWithFormat:NSLocalizedString(@"select_date", nil)];
    view.txtName.placeholder = [NSString stringWithFormat:NSLocalizedString(@"name", nil)];
    [view.btnsave setTitle:NSLocalizedString(@"save", nil) forState:UIControlStateNormal];
    [view.btnCancel setTitle:NSLocalizedString(@"cancel", nil) forState:UIControlStateNormal];
    */
    return view;
}

- (IBAction)cancelClicked:(id)sender {
    [KGModalWrapper hideView];
}

- (IBAction)saveClicked:(id)sender {
    if (_txtName.text.length <= 0) {
        [Utils showAlertWithMessage:@"Plaese enter name"];
    } else if (!selectDate) {
        [Utils showAlertWithMessage:@"Please select date"];
    } else {
        Account * account = [AccountManager Instance].activeAccount;
        NSString * timeStamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
        DraftTour* dTour = [DraftTour newEntity];
        dTour.create_date = [NSDate date];
        dTour.name = _txtName.text;
        dTour.language = account.language;
        dTour.from_date = selectDate;
        dTour.to_date = selectDate;
        dTour.entity_id = timeStamp;
        dTour.days = @1;
        [DraftTour saveEntity];
        [KGModalWrapper hideView];
        
        double delayInSeconds = 2.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            NSString * msg = [NSString stringWithFormat:@"Video Tour Draft created Successfully"];
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:msg preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                VideoViewController * controller = [VideoViewController initViewController];
//                DraftTourViewController * controller = [DraftTourViewController initViewController];
                [[Utils getTopViewControllerFromNavControl].navigationController pushViewController:controller animated:YES];
            }];
            [alert addAction:okayAction];
            
            [[Utils getTopViewController] presentViewController:alert animated:YES completion:nil];
        });
    }
}

-(void)showPicker:(id)sender {
    [ActionSheetDatePicker showPickerWithTitle:@"Select Date" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] minimumDate:[NSDate date] maximumDate:nil doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        selectDate = selectedDate;
        NSString * day = [Utils getDayFromDate:selectedDate];
        NSLog(@"%@",day);
        NSString * date = [Utils dateFormatyyyyMMdd:selectedDate];
        _lblDate.text= [NSString stringWithFormat:@"%@ (%@)", date, day];
        
    } cancelBlock:nil origin:sender];
}

-(void)dateClicked:(id)sender {
//    [self showPicker:sender];
}

@end

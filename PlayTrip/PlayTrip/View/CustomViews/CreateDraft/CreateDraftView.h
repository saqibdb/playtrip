
#import <UIKit/UIKit.h>

@interface CreateDraftView : UIView {
    NSDate * selectDate;
}

@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UILabel *lblTopMain;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UIView * dateView;
@property (strong, nonatomic) IBOutlet UILabel *lblBeginDay;
@property (strong, nonatomic) IBOutlet UILabel *lblInitName;
@property (strong, nonatomic) IBOutlet UIButton *btnsave;
@property (strong, nonatomic) IBOutlet UIButton *btnCancel;

-(IBAction)dateClicked:(id)sender;

-(IBAction)cancelClicked:(id)sender;

-(IBAction)saveClicked:(id)sender;

+ (id)initWithNib;

@end

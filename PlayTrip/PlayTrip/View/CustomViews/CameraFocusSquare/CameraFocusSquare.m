
#import <UIKit/UIKit.h>

@interface CameraFocusSquare : UIView 

- (instancetype)initWithTouchPoint:(CGPoint)touchPoint;
- (void)updatePoint:(CGPoint)touchPoint;
- (void)animateFocusingAction;

@end


#import "AddRemark.h"
#import "Utils.h"
#import "ActionSheetPicker.h"
#import "ActionSheetDatePicker.h"
#import "ColorConstants.h"

@implementation AddRemark

+ (id)initWithNib{
    AddRemark*  view = [[[NSBundle mainBundle] loadNibNamed:@"AddRemark" owner:nil options:nil] objectAtIndex:0];
    
    view.timeView.layer.cornerRadius = view.timeView.layer.frame.size.height / 2;
    view.timeView.layer.borderColor= [ColorConstants appBrownColor].CGColor;
    view.timeView.layer.borderWidth=1;
    view.txtView.layer.borderWidth = 1;
    view.txtView.layer.cornerRadius = 5;
    view.txtView.layer.borderColor= [ColorConstants appBrownColor].CGColor;
    view->lblTopMain =  [Utils roundCornersOnView:view->lblTopMain onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];
    return view;
}

- (IBAction)actionShowPicker:(id)sender{
    [ActionSheetDatePicker showPickerWithTitle:@"Select Date" datePickerMode:UIDatePickerModeTime selectedDate:[NSDate date] minimumDate:nil maximumDate:[NSDate date] doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        NSLog(@"%@",selectedDate);
    } cancelBlock:nil origin:sender];
}

- (IBAction)timeClicked:(id)sender {
}


@end

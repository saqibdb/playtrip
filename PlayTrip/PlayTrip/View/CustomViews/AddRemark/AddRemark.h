
#import <UIKit/UIKit.h>

@interface AddRemark : UIView {
    
    IBOutlet UILabel *lblTopMain;
}

@property (nonatomic)IBOutlet UIButton *btnCancel;
@property (nonatomic)IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UIView *timeView;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
- (IBAction)timeClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UITextView *txtView;

+ (id)initWithNib;

@end

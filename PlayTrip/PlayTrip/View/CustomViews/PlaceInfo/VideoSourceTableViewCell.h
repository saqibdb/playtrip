//
//  VideoSourceTableViewCell.h
//  PlayTrip
//
//  Created by MAC2 on 2016-12-14.
//  Copyright © 2016 SamirMAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoSourceTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *userImg;
@end

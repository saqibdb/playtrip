//
//  VideoCollectionTableViewCell.h
//  PlayTrip
//
//  Created by MAC2 on 2016-12-14.
//  Copyright © 2016 SamirMAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoCollectionTableViewCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>
@property (strong, nonatomic) IBOutlet UICollectionView *collectionview;

@end

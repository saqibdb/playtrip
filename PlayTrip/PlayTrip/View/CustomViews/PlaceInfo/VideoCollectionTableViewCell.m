//
//  VideoCollectionTableViewCell.m
//  PlayTrip
//
//  Created by MAC2 on 2016-12-14.
//  Copyright © 2016 SamirMAC. All rights reserved.
//

#import "VideoCollectionTableViewCell.h"
#import "CollectionViewCell.h"

@implementation VideoCollectionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self registerNib];
    [_collectionview reloadData];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)registerNib{
    UINib *cell = [UINib nibWithNibName:@"CollectionViewCell" bundle:nil];
    [_collectionview registerNib:cell forCellWithReuseIdentifier:@"CollectionViewCell"];
}

#pragma mark -- CollectionView Delegate and DataSource Method

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CollectionViewCell *cell = (CollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionViewCell" forIndexPath:indexPath];
    
//    cell.img = [];
    [cell.img setBackgroundColor:[UIColor greenColor]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(50,50);
}
@end

//
//  VideoSourceTableViewCell.m
//  PlayTrip
//
//  Created by MAC2 on 2016-12-14.
//  Copyright © 2016 SamirMAC. All rights reserved.
//

#import "VideoSourceTableViewCell.h"

@implementation VideoSourceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _userImg.layer.cornerRadius = _userImg.frame.size.width/2;
    _userImg.layer.masksToBounds = YES;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

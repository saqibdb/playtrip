
#import <UIKit/UIKit.h>

@interface TotalView : UIView

@property (strong, nonatomic) IBOutlet UILabel *lblTotal;

+ (id)initWithNib;

@end


#import <UIKit/UIKit.h>

@interface ReportView : UIView{
    
    IBOutlet UILabel *lblTopMain;
}

@property (strong, nonatomic) IBOutlet UITableView *tblView;
@property (strong, nonatomic) IBOutlet UIButton *btnSend;
@property (weak, nonatomic) IBOutlet UIButton *btnCancle;

+(ReportView *)initWithNib;
- (IBAction)cancelClicked:(id)sender;
- (IBAction)sendClicked:(id)sender;

@end

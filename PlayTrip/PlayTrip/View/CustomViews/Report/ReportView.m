
#import "ReportView.h"
#import "ReportCell.h"
#import "KGModalWrapper.h"
#import "Utils.h"

@implementation ReportView

+ (id)initWithNib{
    ReportView*  view = [[[NSBundle mainBundle] loadNibNamed:@"ReportView" owner:nil options:nil] objectAtIndex:0];
    view->lblTopMain =  [Utils roundCornersOnView:view->lblTopMain onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];

    return view;
}

-(void)awakeFromNib {
    [super awakeFromNib];
    [_tblView registerNib:[UINib nibWithNibName:@"ReportCell" bundle:nil] forCellReuseIdentifier:@"ReportCell"];
}

- (IBAction)cancelClicked:(id)sender {
    [KGModalWrapper hideView];
}

- (IBAction)sendClicked:(id)sender {
    [KGModalWrapper hideView];
}



@end

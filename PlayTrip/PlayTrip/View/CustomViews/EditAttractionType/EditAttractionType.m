
#import "EditAttractionType.h"
#import "Utils.h"

@implementation EditAttractionType

+ (id)initWithNib{
    EditAttractionType*  view = [[[NSBundle mainBundle] loadNibNamed:@"EditAttractionType" owner:nil options:nil] objectAtIndex:0];
    [view.tblView reloadData];
    
    view.btnSave.layer.cornerRadius = 12;
    view.btnSave.layer.borderWidth = 1;
    view.btnSave.layer.borderColor = [UIColor blackColor].CGColor;

    view.btnCancel.layer.cornerRadius = 12;
    view.btnCancel.layer.borderWidth = 1;
    view.btnCancel.layer.borderColor = [UIColor blackColor].CGColor;
    view->lblTopMain =  [Utils roundCornersOnView:view->lblTopMain onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];

    return view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    if (indexPath.row == 0) {
        cell.textLabel.text = @"Eating";
    }else if (indexPath.row == 1){
        cell.textLabel.text = @"Photo-Taking";
    }else if (indexPath.row == 2){
        cell.textLabel.text = @"Shopping";
    }else{
        cell.textLabel.text = @"Hotel";
    }
    
    return cell;
}


@end


#import <UIKit/UIKit.h>

@interface EditAttractionType : UIView<UITableViewDelegate,UITableViewDataSource> {
    
    IBOutlet UILabel *lblTopMain;
}

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UITableView *tblView;

+ (id)initWithNib;

@end

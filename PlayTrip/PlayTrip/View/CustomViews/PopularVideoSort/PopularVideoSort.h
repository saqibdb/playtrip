
#import <UIKit/UIKit.h>

@interface PopularVideoSort : UIView

@property (strong, nonatomic) IBOutlet UITableView *tblView;

+ (id)initWithNib;

@end

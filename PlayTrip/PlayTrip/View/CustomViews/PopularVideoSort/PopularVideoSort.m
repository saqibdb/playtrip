
#import "PopularVideoSort.h"
#import "VideoSortTableViewCell.h"

@implementation PopularVideoSort

+ (id)initWithNib{
    /*
     this function is used for initalise view
     */
    
    PopularVideoSort*  view = [[[NSBundle mainBundle] loadNibNamed:@"PopularVideoSort" owner:nil options:nil] objectAtIndex:0];
    view.tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [view registerNib];
//    view.tblView.layer.borderWidth = 1;
//    view.tblView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [view.tblView reloadData];
    view.tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    return view;
}

- (void)registerNib{
    /*
     this method is used for register Nib to table view
     */
    UINib *cellAirTicker = [UINib nibWithNibName:@"VideoSortTableViewCell" bundle:nil];
    [_tblView registerNib:cellAirTicker forCellReuseIdentifier:@"VideoSortTableViewCell"];
}

#pragma mark -- TableView Delegate and dataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    /*
     this function is used for set number of row in table view
     */
    
    return 9;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    /*
     this function will setup cell for tableview
     */
    
    VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"VideoSortTableViewCell" forIndexPath:indexPath];
    cell.btnFirst.layer.cornerRadius = 5.0;
    cell.btnSecond.layer.cornerRadius = 5.0;
    
    if (indexPath.row == 0) {
        cell.lblText.text  =@"Title";
    }else if (indexPath.row == 1){
        cell.lblText.text = @"Time";
    }else if (indexPath.row == 2){
        cell.lblText.text = @"Bookmark";
    }else if (indexPath.row == 3){
        cell.lblText.text = @"View";
    }else if (indexPath.row == 4){
        cell.lblText.text = @"Share";
    }else if (indexPath.row == 5){
        cell.lblText.text = @"Rewards";
    }else if (indexPath.row == 6){
        cell.lblText.text = @"Attraction";
    }else if (indexPath.row == 7){
        cell.lblText.text = @"Add to My Plan";
    }else if (indexPath.row == 8){
        cell.lblText.text = @"Video Tour";
    }
    
    if (indexPath.row == 0) {
        [cell.btnFirst setImage:[UIImage imageNamed:@"a_z_us"] forState:UIControlStateNormal];
        [cell.btnFirst setImage:[UIImage imageNamed:@"a_z_s"] forState:UIControlStateSelected];
        [cell.btnSecond setImage:[UIImage imageNamed:@"z_a_us"] forState:UIControlStateNormal];
        [cell.btnSecond setImage:[UIImage imageNamed:@"z_a_s"] forState:UIControlStateSelected];
         [cell.btnFirst setSelected:YES];
    } else {
        [cell.btnFirst setImage:[UIImage imageNamed:@"up_us"] forState:UIControlStateNormal];
        [cell.btnFirst setImage:[UIImage imageNamed:@"up_s"] forState:UIControlStateSelected];
        [cell.btnSecond setImage:[UIImage imageNamed:@"down_us"] forState:UIControlStateNormal];
        [cell.btnSecond setImage:[UIImage imageNamed:@"down_s"] forState:UIControlStateSelected];
        [cell.btnFirst setSelected:YES];
    }    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    /*
     this method is used for get Selection of table view
     */
    
}

@end

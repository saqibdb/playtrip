
#import <UIKit/UIKit.h>

@interface EditFirstDay : UIView{
    
    IBOutlet UILabel *lblTopMain;
}
@property (nonatomic)IBOutlet UIButton *btnSave;

@property (nonatomic)IBOutlet UIButton *btnCancel;

@property (nonatomic)IBOutlet UIButton *btnDay;

@property (nonatomic)IBOutlet UIButton *btnMonth;
@property (weak, nonatomic) IBOutlet UIView *dayView;
@property (weak, nonatomic) IBOutlet UIView *monthView;
@property (weak, nonatomic) IBOutlet UILabel *lblDay;
@property (weak, nonatomic) IBOutlet UILabel *lblMonth;
@property (weak, nonatomic) IBOutlet UILabel *lblDayName;
@property (weak, nonatomic) IBOutlet NSString *dateString;


+ (id)initWithNib;
- (IBAction)cancleClicked:(id)sender;
- (IBAction)saveClicked:(id)sender;
- (IBAction)DatePickerClicked:(id)sender;

@end

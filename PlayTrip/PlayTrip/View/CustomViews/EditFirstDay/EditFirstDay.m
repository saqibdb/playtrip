
#import "EditFirstDay.h"
#import "ColorConstants.h"
#import "KGModalWrapper.h"
#import "ActionSheetPicker.h"
#import "ActionSheetDatePicker.h"
#import "Utils.h"

@implementation EditFirstDay

+ (id)initWithNib{
    EditFirstDay*  view = [[[NSBundle mainBundle] loadNibNamed:@"EditFirstDay" owner:nil options:nil] objectAtIndex:0];
    view->lblTopMain =  [Utils roundCornersOnView:view->lblTopMain onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];

   
    
    return view;
}

- (IBAction)cancleClicked:(id)sender {
    [KGModalWrapper hideView];
}

-(void)saveClicked:(id)sender {}

- (IBAction)DatePickerClicked:(id)sender {
    [ActionSheetDatePicker showPickerWithTitle:@"Select Date" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] minimumDate:[NSDate date] maximumDate:nil doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        _dateString=[Utils dateFormatMMddyyyy:selectedDate];
        _lblMonth.text=[Utils dateFormatMMMyyyy:selectedDate];
        NSArray *dateArray = [_dateString componentsSeparatedByString:@"/"];
        _lblDay.text = [dateArray objectAtIndex:0];
        _lblDayName.text =[NSString stringWithFormat:@"(%@)", [Utils getDayFromDate:selectedDate]];
    } cancelBlock:nil origin:sender];
}

@end

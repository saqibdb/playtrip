
#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface CreateMyPlanView : UIView <LoginDelegate> {
    NSString *dateString;
    NSString *dateString1;
    IBOutlet UITextField *txtName;
    IBOutlet UITextField *txtday;
    
    IBOutlet UILabel *lblTopMain;
    
    IBOutlet UILabel *lblDayName;
    IBOutlet UILabel *lblDay;
    IBOutlet UILabel *lblMonth;
}

+ (id)initWithNib;

@property (nonatomic)  IBOutlet UIButton * btnDay;

@property (nonatomic)  IBOutlet UIButton * btnMonth;

@property (nonatomic)  IBOutlet UIButton * btnCancel;

@property (nonatomic)  IBOutlet UIButton * btnSave;
@property (weak, nonatomic) IBOutlet UIView *dayView;
@property (weak, nonatomic) IBOutlet UIView *monthView;


@property (strong, nonatomic) IBOutlet UILabel *lblIntinerareyName;
@property (strong, nonatomic) IBOutlet UILabel *lblDays;
@property (strong, nonatomic) IBOutlet UILabel *lblFirstDayTrip;






- (IBAction)actionShowPicker:(id)sender;
- (IBAction)saveClicked:(id)sender;
- (IBAction)cancleClicked:(id)sender;


@end

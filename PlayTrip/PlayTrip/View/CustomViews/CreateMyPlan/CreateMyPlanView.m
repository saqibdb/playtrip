
#import "CreateMyPlanView.h"
#import "ActionSheetPicker.h"
#import "ActionSheetDatePicker.h"
#import "PlayTripManager.h"
#import "AccountManager.h"
#import "Account.h"
#import "UIAlertController+Blocks.h"
#import "KGModalWrapper.h"
#import "ColorConstants.h"
#import "Localisator.h"

@implementation CreateMyPlanView

+ (id)initWithNib{
    CreateMyPlanView*  view = [[[NSBundle mainBundle] loadNibNamed:@"CreateMyPlanView" owner:nil options:nil] objectAtIndex:0];
    
    view.dayView.layer.cornerRadius = view.dayView.layer.frame.size.height / 2;
    view.dayView.layer.borderColor= [ColorConstants appBrownColor].CGColor;
    view.dayView.layer.borderWidth=1;
    view.monthView.layer.cornerRadius = view.monthView.layer.frame.size.height / 2;
    view.monthView.layer.borderColor= [ColorConstants appBrownColor].CGColor;
    view.monthView.layer.borderWidth=1;
    
    view->dateString=[Utils dateFormatMMddyyyy:[NSDate date]];
    view->lblMonth.text=[Utils dateFormatMMMyyyy:[NSDate date]];
    NSArray *dateArray = [view->dateString componentsSeparatedByString:@"/"];
    view->lblDay.text = [dateArray objectAtIndex:1];
    
    NSDate * now = [NSDate date];
    
    
    
    
    
    view->lblDayName.text =[NSString stringWithFormat:@"(%@)", [Utils getDayFromDateInLanguage:now andLocaleIdentifier:[Localisator sharedInstance].currentLanguage]];

    
    //view->lblDayName.text =[NSString stringWithFormat:@"(%@)", [Utils getDayFromDate:now]];
    
    view->lblTopMain =  [Utils roundCornersOnView:view->lblTopMain onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];
    
    // Set localize strings
    [view->lblTopMain setText:LOCALIZATION(@"create_my_plan")];
    [view.lblIntinerareyName setText:LOCALIZATION(@"intinerary_name")];
    [view.lblDays setText:LOCALIZATION(@"days")];
    [view.lblFirstDayTrip setText:LOCALIZATION(@"first_day_trip")];
    [view->txtName setPlaceholder:LOCALIZATION(@"name")];
    [view->txtday setPlaceholder:LOCALIZATION(@"day")];
    [view.btnCancel setTitle:LOCALIZATION(@"cancel") forState:UIControlStateNormal];
    [view.btnSave setTitle:LOCALIZATION(@"save") forState:UIControlStateNormal];
    
    /*
    view->lblTopMain.text = [NSString stringWithFormat:NSLocalizedString(@"create_my_plan", nil)];
    view.lblIntinerareyName.text = [NSString stringWithFormat:NSLocalizedString(@"intinerary_name", nil)];
    view.lblDays.text = [NSString stringWithFormat:NSLocalizedString(@"days", nil)];
    view.lblFirstDayTrip.text = [NSString stringWithFormat:NSLocalizedString(@"first_day_trip", nil)];
    view->txtName.text = [NSString stringWithFormat:NSLocalizedString(@"name", nil)];
    view->txtday.text = [NSString stringWithFormat:NSLocalizedString(@"day", nil)];
    */
    return view;
}

- (IBAction)actionShowPicker:(id)sender{
    [ActionSheetDatePicker showPickerWithTitle:@"Select Date" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] minimumDate:[NSDate date] maximumDate:nil doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        NSLog(@"%@",selectedDate);
        dateString=[Utils dateFormatMMddyyyy:selectedDate];
        lblMonth.text=[Utils dateFormatMMMyyyy:selectedDate];
        NSArray *dateArray = [dateString componentsSeparatedByString:@"/"];
        lblDay.text = [dateArray objectAtIndex:1];
        lblDayName.text = [Utils getDayFromDate:selectedDate];
    } cancelBlock:nil origin:sender];
}

- (IBAction)saveClicked:(id)sender {
    NSString *message = @"";
   
    NSString *trimmed = [txtName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];

    if(trimmed.length <= 0){
        message = @"Please Enter Name";
    }else  if(txtday.text.length <= 0){
        message = @"Please Enter Days";
    }else{
        [self createMyPlanApi];
    }
    
    if(message.length > 0){
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:okayAction];
        
        [[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:alert animated:YES completion:^{
        }];
        return;

    }
}

- (IBAction)cancleClicked:(id)sender {
    [KGModalWrapper hideView];
}

-(void)createMyPlanApi{
    
    if ([txtday.text integerValue] > 0) {
        Account * account = [AccountManager Instance].activeAccount;
        if(account){
            if (account.isSkip == true) {
                [KGModalWrapper hideView];
                [[AppDelegate appDelegate] showLogin:self];
            }else {
                [self createPlan];
            }
           
        }else{
            [self createPlan];
        }
    } else {
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Set valid number of days" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:okayAction];
        [[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:alert animated:true completion:nil];
    }
}

-(void)createPlan{
    Account *account = [AccountManager Instance].activeAccount;
    [[PlayTripManager Instance] createMyPlanWithUser:account.userId withName:txtName.text withFromDate:dateString withDays:txtday.text withBlock:^(id result, NSString *error) {
        if(!error){
            
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Plan created successfully" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [KGModalWrapper hideView];
                
                Account * account = [AccountManager Instance].activeAccount;
                [[PlayTripManager Instance] loadPlanByUserID:account.userId WithBlock:^(id result, NSString *error) {
                }];
            }];
            [alert addAction:okayAction];
            
            [[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:alert animated:YES completion:^{
            }];
            return;
        }else{
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:error preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:okayAction];
            
            [[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:alert animated:YES completion:^{
            }];
            return;
        }
        
    }];
}

#pragma mark - <LoginDelegate>
-(void)loginSuccessfully{
    [self createPlan];
}

@end

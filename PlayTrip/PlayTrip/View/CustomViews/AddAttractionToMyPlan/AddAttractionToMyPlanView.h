
#import "Plan.h"
#import <UIKit/UIKit.h>
#import "LoginDelegate.h"
#import "PlanModel.h"

@interface AddAttractionToMyPlanView : UIView<UITableViewDelegate,UITableViewDataSource, LoginDelegate>{
    NSMutableArray * planArray;
     NSMutableArray * planName;
    NSNumber * numberOfDays;
    Plan * selectedPlan;
    NSNumber *selectedDay;
    NSMutableArray *allAttractions;
    NSMutableArray *attractionId;
    NSDate * startDate;
    NSString * selectedAttractionId;
    NSMutableArray *attArray;
    NSMutableArray *attDataArray;
    NSMutableArray *selectedAttractions;
    int selectedPlanDays;
    
    IBOutlet UILabel *lblTopMain;
    NSMutableArray *attData;
    
    AttractionDataModel *attrDataModel;
    PlanModel *selectedPlanModelNew;
}

@property (weak, nonatomic) IBOutlet UIButton *btnTour;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectedPlan;
@property (weak, nonatomic) IBOutlet UIView *planView;


@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UIButton *createMyPlanBtn;


+ (id)initWithNibWithAttractionId:(NSString *)attrId;
+ (id)initWithNibWithAttractionDataModel:(AttractionDataModel *)attractionDataModel;
+ (id)initWithNibWithPlanModel:(PlanModel *)planModel;

- (IBAction)cancleClicked:(id)sender;
- (IBAction)saveClicked:(id)sender;
- (IBAction)planListClicked:(id)sender;
- (void)getPlanByUserApi;
- (IBAction)oneDayClicked:(id)sender;

@end

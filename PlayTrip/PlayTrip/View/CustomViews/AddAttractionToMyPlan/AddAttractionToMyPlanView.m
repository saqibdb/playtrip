
#import "AddAttractionToMyPlanView.h"
#import "DashboardViewController.h"
#import "KGModalWrapper.h"
#import "ActionSheetPicker.h"
#import "PlayTripManager.h"
#import "Account.h"
#import "AccountManager.h"
#import "AppDelegate.h"
#import "Plan.h"
#import "AddAttractionToPlanCell.h"
#import "Attractions.h"
#import "ColorConstants.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "Localisator.h"
#import "CreateMyPlanView.h"

@implementation AddAttractionToMyPlanView{
    NSMutableArray *myPlansArray;
    PlanModel *selectedPlanModel;
}

+ (id)initWithNibWithAttractionId:(NSString *)attrId {
    AddAttractionToMyPlanView*  view = [[[NSBundle mainBundle] loadNibNamed:@"AddAttractionToMyPlan" owner:nil options:nil] objectAtIndex:0];
    view.planView.layer.cornerRadius = view.planView.layer.frame.size.height / 2;
    view.planView.layer.borderColor= [ColorConstants appBrownColor].CGColor;
    view.planView.layer.borderWidth=1;
    
    view->selectedAttractionId = attrId;
    view->planName = [NSMutableArray new];
    view->planArray = [NSMutableArray new];
    view->attArray = [NSMutableArray new];
    view->attDataArray = [NSMutableArray new];
    view->selectedAttractions = [NSMutableArray new];
    
    view->lblTopMain =  [Utils roundCornersOnView:view->lblTopMain onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];

    
    //view->lblTopMain.text = [NSString stringWithFormat:NSLocalizedString(@"login_playTrip", nil)];
    [view->_lblSelectedPlan setText:LOCALIZATION(@"select_plan")];
    [view->_btnCancel setTitle:LOCALIZATION(@"cancel") forState:UIControlStateNormal];
    [view->_btnSave setTitle:LOCALIZATION(@"save") forState:UIControlStateNormal];
    [view->_createMyPlanBtn setTitle:LOCALIZATION(@"create_my_plan") forState:UIControlStateNormal];
    return view;
}

+ (id)initWithNibWithAttractionDataModel:(AttractionDataModel *)attractionDataModel {
    AddAttractionToMyPlanView*  view = [[[NSBundle mainBundle] loadNibNamed:@"AddAttractionToMyPlan" owner:nil options:nil] objectAtIndex:0];
    view.planView.layer.cornerRadius = view.planView.layer.frame.size.height / 2;
    view.planView.layer.borderColor= [ColorConstants appBrownColor].CGColor;
    view.planView.layer.borderWidth=1;
    
    view->attrDataModel = attractionDataModel;
    view->planName = [NSMutableArray new];
    view->planArray = [NSMutableArray new];
    view->attArray = [NSMutableArray new];
    view->attDataArray = [NSMutableArray new];
    view->selectedAttractions = [NSMutableArray new];
    
    view->lblTopMain =  [Utils roundCornersOnView:view->lblTopMain onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];
    
    return view;
}

+ (id)initWithNibWithPlanModel:(PlanModel *)planModel {
    AddAttractionToMyPlanView*  view = [[[NSBundle mainBundle] loadNibNamed:@"AddAttractionToMyPlan" owner:nil options:nil] objectAtIndex:0];
    view.planView.layer.cornerRadius = view.planView.layer.frame.size.height / 2;
    view.planView.layer.borderColor= [ColorConstants appBrownColor].CGColor;
    view.planView.layer.borderWidth=1;
    
    view->selectedPlanModelNew = planModel;
    view->planName = [NSMutableArray new];
    view->planArray = [NSMutableArray new];
    view->attArray = [NSMutableArray new];
    view->attDataArray = [NSMutableArray new];
    view->selectedAttractions = [NSMutableArray new];
    
    view->lblTopMain =  [Utils roundCornersOnView:view->lblTopMain onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];
    
    return view;
}


- (IBAction)cancleClicked:(id)sender {
    
    [KGModalWrapper hideView];
}

-(void)saveWithPlan {
    if(myPlansArray.count<=0){
        [Utils showAlertWithMessage:@"Please Select Plan"];
    }
    else{
        if(!selectedPlanModel){
            [Utils showAlertWithMessage:@"No plan selected"];
        }
        else{
            attractionId = [NSMutableArray new];
            
            __block int counter = 0;
            
            [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
            [SVProgressHUD showWithStatus:@"Please wait.."];
            
            
            
            if (selectedPlanModelNew) {
                
                for (AttractionModel *attraction in selectedPlanModelNew.attractions) {
                    for (AttractionDataModel *attrDataModelNew in attraction.attr_data) {
                        
                        
                        
                        counter = counter + 1;

                        Account * account = [AccountManager Instance].activeAccount;
                        
                        NSDictionary *parameters = @{ @"attrdata_id": attrDataModelNew._id,
                                                      @"plan": selectedPlanModel._id,
                                                      @"day": attraction.day,
                                                      @"user":account.userId};
                        
                        
                        
                        
                        [[PlayTripManager Instance] addAttractionToMyPlanWithDictionary:parameters withBlock:^(id result, NSString *error) {
                            counter = counter - 1;
                            if (error) {
                                NSLog(@"%@", error);
                                if (counter == 0) {
                                    [self getPlanByUserApiNew];
                                    
                                    [SVProgressHUD dismiss];
                                    UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Error in Attractions adding" preferredStyle:UIAlertControllerStyleAlert];
                                    UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                        [KGModalWrapper hideView];
                                    }];
                                    [alert addAction:okayAction];
                                }
                            } else {
                                
                                
                                
                                
                                NSError* errorData;
                                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                                     options:kNilOptions
                                                                                       error:&errorData];
                                if (errorData) {
                                    NSLog(@"ERROR GOTTEN = %@" ,errorData.description);
                                }
                                else{
                                    NSLog(@"JSON %@", json);
                                }
                                
                                
                                
                                
                                if (counter == 0) {
                                    [SVProgressHUD dismiss];
                                    UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Attractions added" preferredStyle:UIAlertControllerStyleAlert];
                                    UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                        [KGModalWrapper hideView];
                                    }];
                                    [alert addAction:okayAction];
                                    
                                    [[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:alert animated:YES completion:^{
                                    }];
                                }
                            }
                            }];
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        /*
                        NSMutableDictionary * dict = [NSMutableDictionary new];
                        [dict setValue:attrDataModelNew.info._id forKey:@"info"];
                        [dict setValue:attrDataModelNew.video_id._id forKey:@"video_id"];
                        [dict setValue:attrDataModelNew.info.address forKey:@"desc"];
                        [dict setValue:@"12:00" forKey:@"time"];

                        
                        Account * account = [AccountManager Instance].activeAccount;

                        
                        NSDictionary *parameters = @{ @"attraction_id": attrDataModelNew._id,
                                                      @"plan": selectedPlanModel._id,
                                                      @"day": attraction.day,
                                                      @"user":account.userId};
                                                      
                        counter = counter + 1;

                        [[PlayTripManager Instance] addAttractionToMyPlanWithDictionary:parameters withBlock:^(id result, NSString *error) {
                            counter = counter - 1;
                            if (error) {
                                NSLog(@"%@", error);
                                if (counter == 0) {
                                    [self getPlanByUserApiNew];
                                    
                                    [SVProgressHUD dismiss];
                                    UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Error in Attractions adding" preferredStyle:UIAlertControllerStyleAlert];
                                    UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                        [KGModalWrapper hideView];
                                    }];
                                    [alert addAction:okayAction];
                                }
                            } else {
                                
                                if (counter == 0) {
                                    [SVProgressHUD dismiss];
                                    UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Attractions added" preferredStyle:UIAlertControllerStyleAlert];
                                    UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                        [KGModalWrapper hideView];
                                    }];
                                    [alert addAction:okayAction];
                                    
                                    [[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:alert animated:YES completion:^{
                                    }];
                                }
                            }
                        }];
                        
                        /*
                         
                         for (DraftAttrData * dAD in [dAttrs.draftAttrDataSet allObjects]) {
                         NSMutableDictionary * infoDict = [NSMutableDictionary new];
                         [infoDict setValue:dAD.draftInfo.entity_id forKey:@"info"];
                         [infoDict setValue:dAD.time forKey:@"time"];
                         [infoDict setValue:dAD.desc forKey:@"desc"];
                         [infoDict setValue:dAD.draftVideoId.entity_id forKey:@"video_id"];
                         [attrArray addObject:infoDict];
                         }
                         [attractionDict setValue:[NSNumber numberWithInteger:dAttrs.day.integerValue] forKey:@"day"];
                         [attractionDict setValue:attrArray forKey:@"attr"];
                         [attractionDict setValue:account.userId forKey:@"user"];
                         
                         
                        
                        
                        [attractionId addObject:dict];
                    }
                    
                    if (selectedPlanModel._id) {
                        counter = counter + 1;
                        
                        [[PlayTripManager Instance]addAttractionToMyPlan:selectedPlanModel._id forDay:attraction.day withAttractions:attractionId WithBlock:^(id result, NSString *error) {
                            counter = counter - 1;
                            if (error) {
                                NSLog(@"%@", error);
                                if (counter == 0) {
                                    [self getPlanByUserApiNew];

                                    [SVProgressHUD dismiss];
                                    UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Error in Attractions adding" preferredStyle:UIAlertControllerStyleAlert];
                                    UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                        [KGModalWrapper hideView];
                                    }];
                                    [alert addAction:okayAction];
                                }
                            } else {
                                
                                if (counter == 0) {
                                    [SVProgressHUD dismiss];
                                    UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Attractions added" preferredStyle:UIAlertControllerStyleAlert];
                                    UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                                        [KGModalWrapper hideView];
                                    }];
                                    [alert addAction:okayAction];
                                    
                                    [[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:alert animated:YES completion:^{
                                    }];
                                }
                            }
                        }];*/
                    }
                    
                }
                
                
            }
                        
        }
    }
}

- (IBAction)saveClicked:(id)sender {
    if (selectedPlanModelNew) {
        [self saveWithPlan];
    }
    else{
        if(myPlansArray.count<=0){
            [Utils showAlertWithMessage:@"Please Select Plan"];
        }
        else{
            if(selectedDay != nil){
                //[self addAttractionToMyPlanApi];
                [self addAttractionToMyPlanApiNew];
            }else if(!selectedPlanModel){
                [Utils showAlertWithMessage:@"No plan selected"];
            } else{
                UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Please Select Day" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:okayAction];
                [[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:alert animated:YES completion:^{
                }];
            }
        }
 
    }
    
    


    /*
    if(planArray.count<=0){
        [Utils showAlertWithMessage:@"Please Select Plan"];
    }else{
        if(selectedDay != nil){
            [self addAttractionToMyPlanApi];
        }else if(!selectedPlan){
            [Utils showAlertWithMessage:@"No plan selected"];
        } else{
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Please Select Day" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
            [alert addAction:okayAction];
            [[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:alert animated:YES completion:^{
            }];
        }
    }
     */
}

- (IBAction)planListClicked:(id)sender {
    
    if (myPlansArray.count) {
        [self showPicker:sender];
    }
    else{
        [self getPlanByUserApiNew];
    }
    
    /*
    if (planName.count <= 0) {
        [self getPlanByUserApi];
    } else {
        [self showPicker:sender];
    }
     */
}

-(void)showPicker:(id)sender {
    
    NSMutableArray *allPlanNames = [[NSMutableArray alloc] init];
    for (PlanModel *planModel in myPlansArray) {
        if (planModel.name.length) {
            [allPlanNames addObject:planModel.name];
        }
    }
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Plans" rows:allPlanNames initialSelection:0  doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        //        [_btnTour setTitle:[planName objectAtIndex:selectedIndex] forState:UIControlStateNormal];
        _lblSelectedPlan.text =[allPlanNames objectAtIndex:selectedIndex] ;
        [self getDataWithIndex:selectedIndex];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    } origin:sender];
}

-(void)getDataWithIndex:(NSInteger)number {
    
    PlanModel *planModel = [myPlansArray objectAtIndex:number];
    
    selectedPlanModel = planModel;
    [_tblView reloadData];

    
    /*
    Plan *p = [planArray objectAtIndex:number];
    selectedPlan = p;
    selectedPlanDays = selectedPlan.daysValue;
    selectedAttractions = [[selectedPlan.attractionsSet allObjects] mutableCopy];
    numberOfDays = p.days;
    startDate = selectedPlan.from_date;
    [_tblView reloadData];
     */
}

-(void)addAttractionToMyPlanApi {
    allAttractions = [NSMutableArray new];
    attractionId = [NSMutableArray new];
    allAttractions = [[selectedPlan.attractionsSet allObjects] mutableCopy];
    
    if (selectedAttractionId) {
        NSMutableDictionary * dict = [NSMutableDictionary new];
        [dict setValue:selectedAttractionId forKey:@"info"];
        [dict setValue:@"" forKey:@"time"];
        [attractionId addObject:dict];
    }
    
    if (selectedPlan.entity_id) {
        [[PlayTripManager Instance]addAttractionToMyPlan:selectedPlan.entity_id forDay:selectedDay withAttractions:attractionId WithBlock:^(id result, NSString *error) {
            if (error) {
                NSLog(@"%@", error);
            } else {
                //NSLog(@"%@", result);
                Account * account = [AccountManager Instance].activeAccount;
                [[PlayTripManager Instance] loadPlanByUserID:account.userId WithBlock:^(id result, NSString *error) {
                }];
                
                UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Attractions added" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [KGModalWrapper hideView];
                }];
                [alert addAction:okayAction];
                
                [[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:alert animated:YES completion:^{
                }];
            }
        }];
    }
}

-(void)getPlanByUserApi {
    Account * account = [AccountManager Instance].activeAccount;
    if(account){
        if (account.isSkip == true) {
            [[AppDelegate appDelegate] showLogin:self];
        } else {
            [self loadPlan];
        }
    }else{
        [[AppDelegate appDelegate] showLogin:self];
    }
}

-(void)loadPlan {
    Account *account = [AccountManager Instance].activeAccount;
    [[PlayTripManager Instance] loadPlanByUserID:account.userId WithBlock:^(id result, NSString *error) {
        if(!error){
            planArray = [Plan getVideoTourDraft];
            //NSLog(@"%@", planArray);
            for(int i= 0 ; i<planArray.count ; i++){
                Plan *p = [planArray objectAtIndex:i];
                if (p.name) {
                    [planName addObject:p.name];
                }
            }
            if(planArray.count <=0){
                UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"There are No Plans for you" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                [alert addAction:okayAction];
                [[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:alert animated:YES completion:^{
                }];
            }else{
                [self showPicker:self.btnTour];
            }
        }
    }];
}

- (IBAction)oneDayClicked:(id)sender {
    
    CreateMyPlanView *view = [CreateMyPlanView initWithNib];
    [KGModalWrapper showWithContentView:view];
    
    
    
    
    /*
    if(selectedPlan){
        Account *account = [AccountManager Instance].activeAccount;
        //    int value = (int)selectedPlan.daysValue;
        NSNumber *planDays = [NSNumber numberWithInt:selectedPlanDays + 1];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD showWithStatus:@"Please wait.."];
        
        [[PlayTripManager Instance]editMyPlanWithPlanId:selectedPlan.entity_id WithUser:account.userId withName:selectedPlan.name withFromDate:[Utils dateFormatMMddyyyy:selectedPlan.from_date] withDays:planDays withBlock:^(id result, NSString *error) {
            if(error){
                [SVProgressHUD dismiss];
                NSLog(@"%@",error);
            }else{
                [SVProgressHUD dismiss];
                //NSLog(@"%@",result);
                selectedPlanDays++;
                [_tblView reloadData];
            }
        }];
    }else{
          [Utils showAlertWithMessage:@"Please Select Plan"];
    }
    */
    
}




#pragma mark - <LoginDelegate>
-(void)loginSuccessfully{
    [self loadPlan];
}

#pragma mark - <UITableViewDelegate,UITableViewDataSource>

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
//    return [selectedPlan.days integerValue];
    return [selectedPlanModel.days intValue];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"AddAttractionToPlanCell";
    AddAttractionToPlanCell *cell = (AddAttractionToPlanCell *)[_tblView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:nil options:nil];
        cell = (AddAttractionToPlanCell *)[nib objectAtIndex:0];
    }
    
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = indexPath.row;
//    if (dayComponent.day >= 1) {
//        dayComponent.day = dayComponent.day-1;
//    }
    
    NSString *fromDateStr = ([selectedPlanModel.from_date length]>19 ? [selectedPlanModel.from_date substringToIndex:19] : selectedPlanModel.from_date);
    
    
    
    
    //2017-06-21T07:24:32.570Z
    
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    
    NSDate* fromDateNew = [dateFormatter dateFromString:fromDateStr];

    
    
    NSCalendar *theCalendar = [NSCalendar currentCalendar];
    NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:fromDateNew options:0];
    
    NSString * fullString = [Utils getRequiredStringForDate:nextDate];
    cell.accessoryType = UITableViewCellAccessoryNone;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    int cInd = (int) indexPath.row+1;
    cell.lblPlanName.text=[NSString stringWithFormat:@"Day %d (%@)", cInd, fullString];
    
    //NSLog(@"%@", selectedPlan.attractions);
    int aCount = 0;
    
    for (AttractionModel *attraction in selectedPlanModel.attractions) {
        if ([attraction.day intValue] == indexPath.row+1) {
            aCount = (int)attraction.attr_data.count;
        }
    }
    
    /*
    if (selectedPlan.attractions.count > 0) {
        for (Attractions * attrac in selectedPlan.attractions) {
            if (attrac.dayValue == indexPath.row+1) {
                aCount = (int)attrac.attractionData.count;
            }
        }
    }
     */
    cell.lblNumber.text = [NSString stringWithFormat:@"%d",aCount];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle =UITableViewCellSelectionStyleGray;
    selectedDay = [NSNumber numberWithInteger:indexPath.row+1];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* uncheckCell = [tableView cellForRowAtIndexPath:indexPath];
    uncheckCell.accessoryType = UITableViewCellAccessoryNone;
}

-(void)awakeFromNib {
    [super awakeFromNib];
    
}

-(void)getPlanByUserApiNew {
    Account *account = [AccountManager Instance].activeAccount;
    if(account){
        if (account.isSkip == true) {
            [[AppDelegate appDelegate] showLogin:self];
        } else {
            
            [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
            [SVProgressHUD showWithStatus:@"Loading My Plans..."];
            
            [[PlayTripManager Instance] loadPlanByUserID:account.userId WithBlockNew:^(id result, NSString *error) {
                if(!error){
                    NSError* errorData;
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                         options:kNilOptions
                                                                           error:&errorData];
                    if (errorData) {
                        NSLog(@"ERROR GOTTEN = %@" ,errorData.description);
                    }
                    NSArray* plansDicts = [json objectForKey:@"data"];
                    myPlansArray = [[NSMutableArray alloc] init];
                    for (NSDictionary *planDict in plansDicts) {
                        NSError *error;
                        PlanModel *planModel = [[PlanModel alloc] initWithDictionary:planDict error:&error];
                        if (error) {
                            NSLog(@"ERROR GOTTEN AT JSONMODEL = %@" ,error.description);
                        }
                        [myPlansArray addObject:planModel];
                    }
                    
                    NSLog(@"TOTAL MY PLANS ARE = %lu" ,(unsigned long)myPlansArray.count);
                    
                    
                }
                [SVProgressHUD dismiss];
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[tblView reloadData];
                    [self showPicker:self.btnTour];
                });
                
            }];
        }
    }
}

-(void)addAttractionToMyPlanApiNew {
    //allAttractions = [NSMutableArray new];
    attractionId = [NSMutableArray new];
    //allAttractions = [[selectedPlan.attractionsSet allObjects] mutableCopy];
    
    if (attrDataModel) {
        NSMutableDictionary * dict = [NSMutableDictionary new];
        [dict setValue:attrDataModel.info._id forKey:@"info"];
        [dict setValue:attrDataModel.video_id._id forKey:@"video_id"];
        [dict setValue:attrDataModel.info.detail forKey:@"desc"];

        [dict setValue:@"12:00" forKey:@"time"];
        [attractionId addObject:dict];
    }
    
    if (selectedPlanModel._id) {
        Account * account = [AccountManager Instance].activeAccount;

        NSDictionary *parameters = @{ @"attrdata_id": attrDataModel._id,
                                      @"plan": selectedPlanModel._id,
                                      @"day": selectedDay,
                                      @"user":account.userId};
        
        
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD showWithStatus:@"Saving to My Plans..."];
        
        [[PlayTripManager Instance] addAttractionToMyPlanWithDictionary:parameters withBlock:^(id result, NSString *error) {
            if (error) {
                NSLog(@"%@", error);
            } else {
                //NSLog(@"%@", result);
                //[self getPlanByUserApiNew];
                [SVProgressHUD dismiss];
                
                
                NSError* errorData;
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                     options:kNilOptions
                                                                       error:&errorData];
                if (errorData) {
                    NSLog(@"ERROR GOTTEN = %@" ,errorData.description);
                }
                else{
                    NSLog(@"REturend Date - %@" , json);
                }

                
                
                UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Attractions added" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [KGModalWrapper hideView];
                }];
                [alert addAction:okayAction];
                
                [[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:alert animated:YES completion:^{
                    
                }];
            }
        }];
        
        /*
        [[PlayTripManager Instance]addAttractionToMyPlan:selectedPlanModel._id forDay:selectedDay withAttractions:attractionId WithBlock:^(id result, NSString *error) {
            if (error) {
                NSLog(@"%@", error);
            } else {
                //NSLog(@"%@", result);
                [self getPlanByUserApiNew];
                [SVProgressHUD dismiss];

                
                UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Attractions added" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [KGModalWrapper hideView];
                }];
                [alert addAction:okayAction];
                
                [[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:alert animated:YES completion:^{
                    
                }];
            }
        }];
         */
    }
}


@end

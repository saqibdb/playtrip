
#import "AddToPlan.h"
#import "ColorConstants.h"
#import "Utils.h"

@implementation AddToPlan

+ (id)initWithNib{
    AddToPlan*  view = [[[NSBundle mainBundle] loadNibNamed:@"AddToPlan" owner:nil options:nil] objectAtIndex:0];
    
    view.dayView.layer.cornerRadius = view.dayView.layer.frame.size.height / 2;
    view.dayView.layer.borderColor= [ColorConstants appBrownColor].CGColor;
    view.dayView.layer.borderWidth=1;
    
    view.dateView.layer.cornerRadius = view.dateView.layer.frame.size.height / 2;
    view.dateView.layer.borderColor= [ColorConstants appBrownColor].CGColor;
    view.dateView.layer.borderWidth=1;
    
    view.monthView.layer.cornerRadius = view.monthView.layer.frame.size.height / 2;
    view.monthView.layer.borderColor= [ColorConstants appBrownColor].CGColor;
    view.monthView.layer.borderWidth=1;
    
    view->lblTopMain =  [Utils roundCornersOnView:view->lblTopMain onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];
    
    return view;
}

- (IBAction)createPlanClicked:(id)sender {
}

@end

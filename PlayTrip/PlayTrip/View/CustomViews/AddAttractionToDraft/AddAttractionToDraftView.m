
#import "AddAttractionToDraftView.h"
#import "KGModalWrapper.h"
#import "ActionSheetPicker.h"
#import "ColorConstants.h"
#import "Utils.h"
#import "SVProgressHUD.h"
#import "AddAttractionToPlanCell.h"
#import "Attractions.h"
#import "DraftAttraction.h"
#import "DraftAttrData.h"
#import "DraftInfo.h"
#import "DraftVideoId.h"
#import "Constants.h"

@implementation AddAttractionToDraftView

+ (id)initWithNibAttractionId:(NSString *)attrId {
    AddAttractionToDraftView*  view = [[[NSBundle mainBundle] loadNibNamed:@"AddAttractionToDraftView" owner:nil options:nil] objectAtIndex:0];
    view.planView.layer.cornerRadius = view.planView.layer.frame.size.height / 2;
    view.planView.layer.borderColor= [ColorConstants appBrownColor].CGColor;
    view.planView.layer.borderWidth=1;
    
    view->planName = [NSMutableArray new];
    view->planArray = [NSMutableArray new];
    view->attArray = [NSMutableArray new];
    view->attDataArray = [NSMutableArray new];
    view->selectedAttractions = [NSMutableArray new];
    view->selectedAttractionId = attrId;
    
    view->lblTopMain =  [Utils roundCornersOnView:view->lblTopMain onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];
    view->dTour = [DraftTour getDraft];
    view->_lblSelectedPlan.text = view->dTour.name;
    
    return view;
}

- (IBAction)cancleClicked:(id)sender {
    [KGModalWrapper hideView];
}

- (IBAction)oneDayClicked:(id)sender {
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Do you want to add one more day to your plan?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {

        int a = dTour.days.intValue +1;
        NSNumber *planDays = [NSNumber numberWithInt:a];
        
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        dayComponent.day = a;
        //    if (dayComponent.day >= 1) {
//        dayComponent.day = dayComponent.day-1;
        //    }
        
        NSCalendar *theCalendar = [NSCalendar currentCalendar];
        NSDate *endDate = [theCalendar dateByAddingComponents:dayComponent toDate:dTour.from_date options:0];
        
        dTour.days = planDays;
        dTour.to_date = endDate;
        
        [DraftTour saveEntity];
        [_tblView reloadData];
        
    }];
    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:nil];
    
    [alert addAction:yesAction];
    [alert addAction:noAction];
    [[Utils getTopViewController] presentViewController:alert animated:YES completion:nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return dTour.days.integerValue;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"AddAttractionToPlanCell";
    AddAttractionToPlanCell *cell = (AddAttractionToPlanCell *)[_tblView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:cellIdentifier owner:nil options:nil];
        cell = (AddAttractionToPlanCell *)[nib objectAtIndex:0];
    }
    
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = indexPath.row;
    //    if (dayComponent.day >= 1) {
//    dayComponent.day = dayComponent.day-1;
    //    }
    
    NSCalendar *theCalendar = [NSCalendar currentCalendar];
    NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:dTour.from_date options:0];
    
    NSString * fullString = [Utils getRequiredStringForDate:nextDate];
    cell.accessoryType = UITableViewCellAccessoryNone;
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    int cInd = (int) indexPath.row+1;
    cell.lblPlanName.text=[NSString stringWithFormat:@"Day %d (%@)", cInd, fullString];
    
    int aCount = 0;
    if (dTour.draftAttractions.count > 0) {
        for (DraftAttraction * attrac in [[dTour.draftAttractionsSet allObjects] mutableCopy]) {
            if (attrac.dayValue == indexPath.row+1) {
                aCount = (int)attrac.draftAttrDataSet.count;
//                aCount++;
            }
        }
    }
    cell.lblNumber.text = [NSString stringWithFormat:@"%d",aCount];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selectionStyle =UITableViewCellSelectionStyleGray;
    selectedDay = [NSNumber numberWithInteger:indexPath.row+1];    
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* uncheckCell = [tableView cellForRowAtIndexPath:indexPath];
    uncheckCell.accessoryType = UITableViewCellAccessoryNone;
}


-(void)saveClicked:(id)sender {
    NSString * timeStamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
    
    Attractions * attrs = [Attractions getByEntityId:selectedAttractionId];
    if (attrs) {
        DraftAttraction * dAttr = [DraftAttraction getByEntityId:selectedAttractionId];
        if (!dAttr) {
            dAttr = [DraftAttraction newEntity];
            dAttr.entity_id = attrs.entity_id;
        }
        dAttr.address = attrs.address;
        dAttr.day = selectedDay;
        dAttr.detail = attrs.detail;
        dAttr.loc_lat = attrs.loc_lat;
        dAttr.loc_lng = attrs.loc_long;
        dAttr.name = attrs.name;
        dAttr.phone_no = attrs.phone_no;
        dAttr.web_url = attrs.web_url;
        
        
        DraftAttrData * dAttrData = [DraftAttrData newEntity];
        dAttrData.desc = @"";
        dAttrData.time = @"";
        dAttrData.entity_id = timeStamp;
        
        DraftInfo * dInfo = [DraftInfo newEntity];
        dInfo.name = attrs.name;
        dInfo.loc_long = attrs.loc_long;
        dInfo.loc_lat = attrs.loc_lat;
        dInfo.language = @"en";
        dInfo.phone_no = attrs.phone_no;
        dInfo.address = attrs.address;
        dInfo.detail = attrs.detail;
        dInfo.web_url = attrs.web_url;
        dInfo.entity_id = attrs.entity_id;
        
        DraftVideoId * dVideoId = [DraftVideoId newEntity];
        dVideoId.file_name = @"";
        dVideoId.thumbnail = @"";
        dVideoId.user = @"";
        dVideoId.entity_id = timeStamp;
        
        [DraftVideoId saveEntity];
        [DraftInfo saveEntity];
        [DraftAttrData saveEntity];
        [DraftAttraction saveEntity];
        [DraftTour saveEntity];
        
        [dAttrData setDraftInfo:dInfo];
        [dInfo setDraftAttrData:dAttrData];
        
        [dAttrData setDraftVideoId:dVideoId];
        [dVideoId setDraftAttrData:dAttrData];
        
        [dAttr addDraftAttrDataObject:dAttrData];
        [dAttrData setDraftAttractions:dAttr];
        
        NSSet * aSet = [[NSSet alloc] initWithObjects:dTour, nil];
        
        [dAttr setDraftTour:aSet];
        [dTour addDraftAttractionsObject:dAttr];
        
        [DraftVideoId saveEntity];
        [DraftInfo saveEntity];
        [DraftAttrData saveEntity];
        [DraftAttraction saveEntity];
        [DraftTour saveEntity];
    }
    [KGModalWrapper hideView];
}

@end


#import <UIKit/UIKit.h>
#import "DraftTour.h"
@interface AddAttractionToDraftView : UIView <UITableViewDelegate,UITableViewDataSource> {
    
    NSMutableArray * planArray;
    NSMutableArray * planName;
    NSNumber * numberOfDays;
    NSNumber *selectedDay;
    NSMutableArray *allAttractions;
    NSMutableArray *attractionId;
    NSString * selectedAttractionId;
    NSMutableArray *attArray;
    NSMutableArray *attDataArray;
    NSMutableArray *selectedAttractions;
    
    IBOutlet UILabel *lblTopMain;
    NSMutableArray *attData;
    
    DraftTour* dTour;
}

@property (weak, nonatomic) IBOutlet UIButton *btnTour;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectedPlan;
@property (weak, nonatomic) IBOutlet UIView *planView;


@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;


+ (id)initWithNibAttractionId:(NSString *)attrId;

- (IBAction)cancleClicked:(id)sender;
- (IBAction)saveClicked:(id)sender;
- (IBAction)oneDayClicked:(id)sender;

@end

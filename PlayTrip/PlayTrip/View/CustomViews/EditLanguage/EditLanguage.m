
#import "EditLanguage.h"
#import "Utils.h"

@implementation EditLanguage

+ (id)initWithNib{
    EditLanguage*  view = [[[NSBundle mainBundle] loadNibNamed:@"EditLanguage" owner:nil options:nil] objectAtIndex:0];
    
    view.btnCancel.layer.cornerRadius = 12;
    view.btnCancel.layer.borderWidth = 1;
    view.btnCancel.layer.borderColor = [UIColor blackColor].CGColor;
    
    view.btnSave.layer.cornerRadius = 12;
    view.btnSave.layer.borderWidth = 1;
    view.btnSave.layer.borderColor = [UIColor blackColor].CGColor;
    view->lblTopMain =  [Utils roundCornersOnView:view->lblTopMain onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];

    return view;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    cell.textLabel.text = @"English";
    return cell;
}

@end

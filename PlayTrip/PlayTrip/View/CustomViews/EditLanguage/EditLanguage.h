
#import <UIKit/UIKit.h>

@interface EditLanguage : UIView<UITableViewDataSource,UITableViewDelegate>{
    
    IBOutlet UILabel *lblTopMain;
}

@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;

+ (id)initWithNib;

@end

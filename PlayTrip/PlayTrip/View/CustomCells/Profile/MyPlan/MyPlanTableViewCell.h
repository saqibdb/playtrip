
#import <UIKit/UIKit.h>

@interface MyPlanTableViewCell : UITableViewCell{
    BOOL select;
}
@property (strong, nonatomic) IBOutlet UIView *viewOption;
@property (strong, nonatomic) IBOutlet UILabel *lblPlanName;
@property (strong, nonatomic) IBOutlet UILabel *lblPlanDays;
@property (strong, nonatomic) IBOutlet UILabel *lblPlanDates;
@property (strong, nonatomic) IBOutlet UILabel *lblDescription;
@property (strong, nonatomic) IBOutlet UILabel *lblCreateDay;
@property (strong, nonatomic) IBOutlet UIButton *btnDelete;
@property (strong, nonatomic) IBOutlet UILabel *lblLastEdited;
@property (strong, nonatomic) IBOutlet UIButton *btnView;
@property (weak, nonatomic) IBOutlet UIImageView *imgLanguage;

@property (weak, nonatomic) IBOutlet UILabel *tag1;
@property (weak, nonatomic) IBOutlet UILabel *tag2;
@property (weak, nonatomic) IBOutlet UILabel *tag3;
@property (weak, nonatomic) IBOutlet UILabel *tag4;
@property (weak, nonatomic) IBOutlet UILabel *tag5;
@property (weak, nonatomic) IBOutlet UILabel *lblLanguage;



- (IBAction)listClicked:(id)sender;

@end

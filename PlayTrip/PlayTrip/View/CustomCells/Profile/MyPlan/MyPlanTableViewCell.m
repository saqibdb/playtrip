
#import "MyPlanTableViewCell.h"

@implementation MyPlanTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _btnDelete.hidden = YES;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}


- (IBAction)listClicked:(id)sender {
    if(select){
        select = false;
        _btnDelete.hidden = YES;
    } else {
        select = true;
        _btnDelete.hidden = NO;
    }

}
@end

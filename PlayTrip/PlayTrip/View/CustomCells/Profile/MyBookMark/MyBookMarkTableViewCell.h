
#import <UIKit/UIKit.h>
#import "HMSegmentedControl.h"
#import "URLSchema.h"

@interface MyBookMarkTableViewCell : UITableViewCell<UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource>{

}

@property (weak, nonatomic) NSMutableArray *arrData_video_tours;
@property (weak, nonatomic) NSMutableArray *arrData_users;
@property (weak, nonatomic) NSMutableArray *arrData_attractions;
@property (weak, nonatomic) NSMutableArray *arrData_locations;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionview;
@property (weak, nonatomic) IBOutlet UITableView *tblView;
@property (retain, nonatomic) HMSegmentedControl *segment;

@end

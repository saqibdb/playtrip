
#import "MyBookMarkTableViewCell.h"
#import "LocationCell.h"
#import "UserTabCell.h"
#import "AttractionsVideoCell.h"
#import "LatestVideoTourCollectionViewCell.h"
#import "NSDate+Extentsion.h"
#import "BookmarkAttractions.h"
#import "BookmarkLocation.h"
#import "BookmarkUser.h"
#import "BookmarkVideotour.h"
#import "Utils.h"
#import "Constants.h"
#import "Account.h"
#import "AccountManager.h"
#import "PlayTripManager.h"
#import "CommonUser.h"
#import "URLSchema.h"
#import "AttractionData.h"
#import "Attractions.h"
#import "VideoTour.h"
#import "ItineraryViewController.h"
#import "PlaceInformationViewController.h"
#import "CommonUser.h"
#import "UserDetailViewController.h"
#import "Info.h"
#import "Localisator.h"


@implementation MyBookMarkTableViewCell
@synthesize arrData_video_tours,arrData_attractions,arrData_locations,arrData_users;
- (void)awakeFromNib {
    [super awakeFromNib];
    [self setUpSegment];
    [self registerCell];
    [self reloadData];
}

- (void)setUpSegment{
    _segment = [[HMSegmentedControl alloc] initWithSectionTitles:@[LOCALIZATION(@"video_tours"), LOCALIZATION(@"attr_videos"), LOCALIZATION(@"users")]];
    

    
    [_segment setFrame:CGRectMake(0, 0,[[UIScreen mainScreen] bounds].size.width, 35)];
    __unsafe_unretained typeof(self) weakSelf = self;
    [_segment setIndexChangeBlock:^(NSInteger index) {
        [weakSelf reloadData];
    }];
    _segment.backgroundColor = [UIColor whiteColor];
    _segment.titleTextAttributes = @{NSForegroundColorAttributeName : [UIColor darkGrayColor]};
    _segment.titleTextAttributes = @{NSFontAttributeName : [UIFont systemFontOfSize:12]};
    [_segment setUserDraggable:YES];
    _segment.selectionStyle = HMSegmentedControlSelectionStyleTextWidthStripe;
    _segment.segmentWidthStyle = HMSegmentedControlSegmentWidthStyleFixed;
    _segment.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    _segment.shouldAnimateUserSelection = NO;
    _segment.selectedSegmentIndex = 0;
    
    [self.contentView addSubview:_segment];
}

- (void)registerCell{
    UINib *cellLocation = [UINib nibWithNibName:@"LocationCell" bundle:nil];
    [_tblView registerNib:cellLocation forCellReuseIdentifier:@"LocationCell"];
    
    UINib *cellLatestVideoTourCollectionViewCell = [UINib nibWithNibName:@"LatestVideoTourCollectionViewCell" bundle:nil];
    
    [_collectionview registerNib:cellLatestVideoTourCollectionViewCell forCellWithReuseIdentifier:@"LatestVideoTourCollectionViewCell"];
    
    UINib *cellAttractionsVideoCell = [UINib nibWithNibName:@"AttractionsVideoCell" bundle:nil];
    [_collectionview registerNib:cellAttractionsVideoCell forCellWithReuseIdentifier:@"AttractionsVideoCell"];
    
    UINib *cellUserTabCell = [UINib nibWithNibName:@"UserTabCell" bundle:nil];
    [_collectionview registerNib:cellUserTabCell forCellWithReuseIdentifier:@"UserTabCell"];
}

- (IBAction)actionSegmentChanged:(id)sender{
    [self reloadData];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)reloadData{
    switch (_segment.selectedSegmentIndex) {
        case 0:
        {
            [self.tblView setHidden:YES];
            [self.collectionview setHidden:NO];
            [self.collectionview reloadData];
        }
            break;
        case 1:
        {
            [self.tblView setHidden:YES];
            [self.collectionview setHidden:NO];
            [self.collectionview reloadData];
        }
            break;
        case 2:
        {
            [self.tblView setHidden:YES];
            [self.collectionview setHidden:NO];
            [self.collectionview reloadData];
            
        }
            break;
        case 3:
        {
            
            
            
        }
            break;
            
        default:{
            [self.tblView setHidden:YES];
            [self.collectionview setHidden:NO];
            [self.collectionview reloadData];
            
        }
            break;
    }
    
}

#pragma mark -- TableView Delegate and DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    switch (_segment.selectedSegmentIndex) {
        case 0:
        {
            return 0;//arrData_video_tours.count;
        }
            break;
        case 1:
        {
            return 0;//arrData_attractions.count;
        }
            break;
        case 2:
        {
            return 0;//arrData_users.count;
        }
            break;
        case 3:
        {
            return 0;//arrData_users.count;
        }
            break;
            
        default:{
            return 0;
        }
            break;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    LocationCell *cell = (LocationCell*)[tableView dequeueReusableCellWithIdentifier:@"LocationCell" forIndexPath:indexPath];
    return cell;
}

#pragma mark -- CollectionView Delegate and DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    switch (_segment.selectedSegmentIndex) {
        case 0: {
            return arrData_video_tours.count;
        }
            break;
        case 1: {
            return arrData_attractions.count;
        }
            break;
        case 2: {
            return arrData_users.count;
        }
            break;
        case 3: {
            return arrData_users.count;
        }
            break;
            
        default:{
            return 0;
        }
            break;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
       Account * account = [AccountManager Instance].activeAccount;
    if (_segment.selectedSegmentIndex == 0) {
        
        LatestVideoTourCollectionViewCell *cell = (LatestVideoTourCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"LatestVideoTourCollectionViewCell" forIndexPath:indexPath];

        BookmarkVideotour * v = [arrData_video_tours objectAtIndex:indexPath.row];

        NSMutableArray *attraction = [NSMutableArray new];
        NSMutableArray *attractionData = [NSMutableArray new];
        Attractions *a;
        AttractionData *aData;
        attraction = [[v.attractionsSet allObjects] mutableCopy];
        if(attraction.count>0){
            a =[attraction objectAtIndex:0];
        }
        
        if (account) {
            if ([v.bookmark_users_string containsString:account.userId]) {
                [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                cell.btnBookMark.selected = YES;
            } else {
                [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
                cell.btnBookMark.selected = NO;
            }
        } else {
            [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
            cell.btnBookMark.selected = NO;
        }
        attractionData = [[a.attractionDataSet allObjects] mutableCopy];
        if(attractionData.count>0){
            aData = [attractionData objectAtIndex:0];
            cell.lblDescription.text = [NSString stringWithFormat:@"[%@ days] %@",v.days, aData.info.address];
        } else {
            cell.lblDescription.text = [NSString stringWithFormat:@"[%@ days]",v.days];
        }
        
        cell.lblFromDate.text = [Utils dateFormatyyyyMMdd:v.from_date];
        cell.lblVideoName.text = v.name;
        cell.lblUserName.text = v.userBookMark.full_name;
        
        if(![v.userBookMark.avatar isEqualToString:@""]){
            NSString * imgId = v.userBookMark.avatar;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            cell.imgUserImg.url = [NSURL URLWithString:urlString];
            cell.imgUserImg.layer.cornerRadius = cell.imgUserImg.frame.size.width / 2;
            cell.imgUserImg.layer.masksToBounds = YES;
        }else{
            cell.imgUserImg.backgroundColor = [UIColor blueColor];
            cell.imgUserImg.layer.cornerRadius = cell.imgUserImg.frame.size.width / 2;
            cell.imgUserImg.layer.masksToBounds = YES;
        }
        if(![v.thumbnail isEqualToString:@""]){
            NSString * imgId = v.thumbnail;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            cell.imgThumbnail.url = [NSURL URLWithString:urlString];
        }
        cell.btnBookMark.tag = indexPath.row;
        [cell.btnBookMark addTarget:self action:@selector(bookmarkClickedVideoTour:) forControlEvents:UIControlEventTouchUpInside];
        cell.lblBookmarkCount.text = [NSString stringWithFormat:@"%@",v.total_bookmark];
        cell.lblViewCount.text = [NSString stringWithFormat:@"%@",v.total_view];
        cell.lblRevertCount.text = [NSString stringWithFormat:@"%@",v.remuneration_amount];
        cell.lblShareCount.text = [NSString stringWithFormat:@"%@",v.total_share];
        return cell;
    } else if (_segment.selectedSegmentIndex == 1){
        AttractionsVideoCell *cell = (AttractionsVideoCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"AttractionsVideoCell" forIndexPath:indexPath];
        
        BookmarkAttractions *att = [arrData_attractions objectAtIndex:indexPath.item];
        if (account) {
            if ([att.bookmark_users_string containsString:account.userId]) {
                [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                cell.btnBookMark.selected = YES;
            } else {
                [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
                cell.btnBookMark.selected = NO;
            }
        } else {
            [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
            cell.btnBookMark.selected = NO;
        }
        cell.lblFromDate.text = [Utils dateFormatyyyyMMdd:att.create_date];
        cell.lblVideoName.text = att.name;
        cell.lblUserName.text = att.userBookmarkAttraction.full_name;
        
        if(![att.userBookmarkAttraction.avatar isEqualToString:@""]){
            NSString * imgId = att.userBookmarkAttraction.avatar;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            cell.imgUserImg.url = [NSURL URLWithString:urlString];
            cell.imgUserImg.layer.cornerRadius = cell.imgUserImg.frame.size.width / 2;
            cell.imgUserImg.layer.masksToBounds = YES;
        }else{
            cell.imgUserImg.backgroundColor = [UIColor blueColor];
            cell.imgUserImg.layer.cornerRadius = cell.imgUserImg.frame.size.width / 2;
            cell.imgUserImg.layer.masksToBounds = YES;
            
        }
        //        if(![att.thumbnail isEqualToString:@""]){
        //            NSString * imgId = v.thumbnail;
        //            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
        //            cell.imgThumbnail.url = [NSURL URLWithString:urlString];
        //        }else{
        //            //            cell.imgThumbnail.image = [UIImage imageNamed:@"mexico.jpg"];
        //        }
        
        
        cell.btnBookMark.tag = indexPath.row;
        [cell.btnBookMark addTarget:self action:@selector(bookmarkClickedBookmarkAttraction:) forControlEvents:UIControlEventTouchUpInside];
        cell.lblTTotalBookMark.text = [NSString stringWithFormat:@"%@",att.total_bookmark];
        cell.lblTTotalView.text = [NSString stringWithFormat:@"%@",att.total_view];
        cell.lblTTotalShare.text = [NSString stringWithFormat:@"%@",att.total_share];
        return cell;
    } else if(_segment.selectedSegmentIndex == 2){
        
        UserTabCell *cell = (UserTabCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"UserTabCell" forIndexPath:indexPath];
        
         BookmarkUser *user = [arrData_users objectAtIndex:indexPath.row];
        if(user.avatar.length > 0){
            cell.imgUser.layer.cornerRadius = cell.imgUser.frame.size.width / 2;
            NSString * imgId = user.avatar;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            cell.imgUser.url = [NSURL URLWithString:urlString];
        }else{
            cell.imgUser.image = [UIImage imageNamed:@"avatarPlacHolder"];
        }
        cell.lblName.text = user.full_name;
        cell.lblPlay.text = [NSString stringWithFormat:@"%@",user.total_share];;
        cell.lblMoney.text =@"100";
        cell.lblBookMark.text = [NSString stringWithFormat:@"%@",user.total_bookmark];
        
        cell.btnBookmark.tag = indexPath.row;
        if (account) {
            if ([user.bookmark_users_string containsString:account.userId]) {
                [cell.btnBookmark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                cell.btnBookmark.selected = YES;
            } else {
                [cell.btnBookmark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
                cell.btnBookmark.selected = NO;
            }
        } else {
            [cell.btnBookmark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
            cell.btnBookmark.selected = NO;
        }
        [cell.btnBookmark addTarget:self action:@selector(bookmarkClickedUser:) forControlEvents:UIControlEventTouchUpInside];
        return cell;

    }
    return nil;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
     UIViewController *contr = [Utils getTopViewControllerFromNavControl];
    if (_segment.selectedSegmentIndex == 0) {
         BookmarkVideotour * v = [arrData_video_tours objectAtIndex:indexPath.row];
        ItineraryViewController *controller = [ItineraryViewController initControllerBookmark:v];
       [contr.navigationController pushViewController:controller animated:NO];
    }else if(_segment.selectedSegmentIndex == 1){
          BookmarkAttractions *obj = [arrData_attractions objectAtIndex:indexPath.item];
        PlaceInformationViewController *controller = [PlaceInformationViewController initControllerBookmark:obj];
        [contr.navigationController pushViewController:controller animated:NO];
    }else if(_segment.selectedSegmentIndex == 2){
        BookmarkUser *user = [arrData_users objectAtIndex:indexPath.row];
        Users * mainUser = [Users getByEntityId:user.entity_id];
        if (mainUser) {
            UserDetailViewController * controller = [UserDetailViewController initViewController:mainUser];
            [contr.navigationController pushViewController:controller animated:NO];
        }
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (_segment.selectedSegmentIndex == 0) {
         collectionView.frame = CGRectMake(0, 44, self.contentView.frame.size.width, 246);
        return CGSizeMake(150 ,246);
    }else if (_segment.selectedSegmentIndex == 1) {
          collectionView.frame = CGRectMake(0, 44, self.contentView.frame.size.width, 229);
        return CGSizeMake(150 ,229);
    }else if (_segment.selectedSegmentIndex == 2){
        collectionView.frame = CGRectMake(0, 44, self.contentView.frame.size.width, 150);
        return CGSizeMake(150,150);
    }
    return CGSizeMake((collectionView.frame.size.width/2)-10, 200);
}

-(void)bookmarkClickedBookmarkAttraction:(UIButton *)sender {
    BookmarkAttractions * att = [arrData_attractions objectAtIndex:[sender tag]];
    [self commanApiWithModel:ModelAttraction WithId:att.entity_id WithType:TypeBookmark];
}

-(void)bookmarkClickedUser:(UIButton *)sender {
    BookmarkUser *user = [arrData_users objectAtIndex:[sender tag]];    [self commanApiWithModel:ModelUser WithId:user.entity_id WithType:TypeBookmark];
}
-(void)bookmarkClickedVideoTour:(UIButton *)sender {
    BookmarkVideotour * v = [arrData_video_tours objectAtIndex:[sender tag]];
    [self commanApiWithModel:ModelPlan WithId:v.entity_id WithType:TypeBookmark];
}

-(void)commanApiWithModel:(NSString *)modelValue WithId:(NSString *)idValue WithType:(NSString *)typeValue {
    [[PlayTripManager Instance] addCountForModel:modelValue withType:typeValue forId:idValue WithBlock:^(id result, NSString *error) {
        if(!error){
            
           [[NSNotificationCenter defaultCenter] postNotificationName:@"ProfileReloadTable" object:nil];
            
            
            if(modelValue == ModelUser){
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"BookmarkDetailsUpdated" object:nil];
            }else if(modelValue == ModelPlan){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"BookmarkDetailsUpdated" object:nil];
            }else if(modelValue == ModelAttraction){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"BookmarkDetailsUpdated" object:nil];

            }
           
        }
    }];
}


@end

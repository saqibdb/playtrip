
#import "ProfileTopViewCell.h"

@implementation ProfileTopViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    CALayer* layer = [_lblBookmarkCount layer];
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.borderColor = [UIColor lightGrayColor].CGColor;
    bottomBorder.borderWidth = 1;
    bottomBorder.frame = CGRectMake(-20, layer.frame.size.height+4, layer.frame.size.width + 40, 1);
    [bottomBorder setBorderColor:[UIColor lightGrayColor].CGColor];
    [layer addSublayer:bottomBorder];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end

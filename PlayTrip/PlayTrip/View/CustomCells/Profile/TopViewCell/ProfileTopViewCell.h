

#import <UIKit/UIKit.h>
#import "URLImageView.h"

@interface ProfileTopViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet URLImageView *imgCover;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet URLImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UIButton *btnImg;
@property (weak, nonatomic) IBOutlet UIButton *btnCover;

@property (weak, nonatomic) IBOutlet UILabel *lblBookmarkCount;
@property (weak, nonatomic) IBOutlet UILabel *lblRewardCount;
@property (weak, nonatomic) IBOutlet UIButton *btnSettings;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;

@property (weak, nonatomic) IBOutlet UILabel *lblBookmark;
@property (weak, nonatomic) IBOutlet UILabel *lblViews;
@property (weak, nonatomic) IBOutlet UILabel *lblShares;

@property (weak, nonatomic) IBOutlet UILabel *lblMoney;

@end


#import <UIKit/UIKit.h>
#import "URLImageView.h"

@interface WhatToDoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet URLImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblText;
@property (strong, nonatomic) IBOutlet UIImageView *imgCheck;
@property (weak, nonatomic) IBOutlet UILabel *lblLine;

@end


#import "WhatToDoTableViewCell.h"

@implementation WhatToDoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _imgIcon.layer.cornerRadius = _imgIcon.frame.size.width / 2;
    _imgIcon.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end

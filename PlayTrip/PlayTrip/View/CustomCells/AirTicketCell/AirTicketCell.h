
#import <UIKit/UIKit.h>

@interface AirTicketCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblDateStart;
@property (weak, nonatomic) IBOutlet UILabel *lblPlaceName;
@property (weak, nonatomic) IBOutlet UILabel *lblToAirportName;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblDuration;


@end

//
//  AddAttractionToPlanCell.h
//  PlayTrip
//
//  Created by MAC5 on 23/01/17.
//  Copyright © 2017 SamirMAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddAttractionToPlanCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblPlanName;

@end

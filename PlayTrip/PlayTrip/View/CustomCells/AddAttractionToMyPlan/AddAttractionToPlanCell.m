
#import "AddAttractionToPlanCell.h"

@implementation AddAttractionToPlanCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _lblNumber.layer.cornerRadius = _lblNumber.layer.frame.size.height / 2;
    _lblNumber.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end

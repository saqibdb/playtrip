
#import <UIKit/UIKit.h>
#import "URLImageView.h"

@interface UserTabCell : UICollectionViewCell

@property (nonatomic) IBOutlet URLImageView * imgUser;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UIButton *btnBookmark;

@property (strong, nonatomic) IBOutlet UILabel *lblPlay;

- (IBAction)bookmarkClicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *mainView;


@property (strong, nonatomic) IBOutlet UILabel *lblMoney;
@property (strong, nonatomic) IBOutlet UILabel *lblBookMark;
@property (weak, nonatomic) IBOutlet UILabel *lblViews;
@property (weak, nonatomic) IBOutlet UILabel *lblShares;




@end

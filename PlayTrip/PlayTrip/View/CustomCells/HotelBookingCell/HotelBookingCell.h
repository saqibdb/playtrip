#import <UIKit/UIKit.h>

@interface HotelBookingCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblHotelName;
@property (weak, nonatomic) IBOutlet UIImageView *imgMain;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblDateFull;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;


@end



#import "VideoCollectionTableViewCell.h"
#import "CollectionViewCell.h"

@implementation VideoCollectionTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self registerNib];
    [_collectionview reloadData];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)registerNib{
    UINib *cell = [UINib nibWithNibName:@"CollectionViewCell" bundle:nil];
    [_collectionview registerNib:cell forCellWithReuseIdentifier:@"CollectionViewCell"];
}

#pragma mark -- CollectionView Delegate and DataSource Method

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CollectionViewCell *cell = (CollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionViewCell" forIndexPath:indexPath];
    [cell.img setBackgroundColor:[UIColor greenColor]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(50,50);
}
@end

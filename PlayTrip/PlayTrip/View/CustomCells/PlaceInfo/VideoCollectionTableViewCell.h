

#import <UIKit/UIKit.h>

@interface VideoCollectionTableViewCell : UITableViewCell<UICollectionViewDelegate,UICollectionViewDataSource>
@property (strong, nonatomic) IBOutlet UICollectionView *collectionview;

@end


#import "VideoSourceTableViewCell.h"

@implementation VideoSourceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _userImg.layer.cornerRadius = _userImg.frame.size.width/2;
    _userImg.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end

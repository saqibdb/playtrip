
#import <UIKit/UIKit.h>

@interface ReportCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
//@property (weak, nonatomic) IBOutlet UIView *backBrownView;

@property (strong, nonatomic) IBOutlet UILabel *lblDescription;

@end


#import "SettingLanguageCell.h"

@implementation SettingLanguageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _btn1.layer.borderWidth=1;
    _btn1.layer.cornerRadius = 5;
    _btn1.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _btn2.layer.borderWidth=1;
    _btn2.layer.cornerRadius = 5;
    _btn2.layer.borderColor = [UIColor darkGrayColor].CGColor;
    _btn3.layer.borderWidth=1;
    _btn3.layer.cornerRadius = 5;
    _btn3.layer.borderColor = [UIColor darkGrayColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (IBAction)btn1Clicked:(id)sender {
    _btn1.backgroundColor = [UIColor colorWithRed:219/255.0f green:219/255.0f blue:219/255.0f alpha:1];
    _btn2.backgroundColor = [UIColor clearColor];
    _btn3.backgroundColor = [UIColor clearColor];
    _btn1.selected = YES;
    _btn2.selected = NO;
    _btn3.selected = NO;
}

- (IBAction)btn2Clicked:(id)sender {
    _btn1.backgroundColor = [UIColor clearColor];
    _btn2.backgroundColor = [UIColor colorWithRed:219/255.0f green:219/255.0f blue:219/255.0f alpha:1];
    _btn3.backgroundColor = [UIColor clearColor];
    _btn1.selected = NO;
    _btn2.selected = YES;
    _btn3.selected = NO;
}

- (IBAction)btn3Clicked:(id)sender {
    _btn1.backgroundColor = [UIColor clearColor];
    _btn2.backgroundColor = [UIColor clearColor];
    _btn3.backgroundColor = [UIColor colorWithRed:219/255.0f green:219/255.0f blue:219/255.0f alpha:1];
    _btn1.selected = NO;
    _btn2.selected = NO;
    _btn3.selected = YES;
}

@end

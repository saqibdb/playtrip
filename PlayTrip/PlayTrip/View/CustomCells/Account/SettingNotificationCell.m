
#import "SettingNotificationCell.h"
#import "ColorConstants.h"
@implementation SettingNotificationCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}
- (IBAction)switchChanged:(id)sender {
    if (_switchMain.isOn) {
        [_switchMain setOn:NO];
        [_switchMain setOnTintColor:[UIColor grayColor]];
    }else{
        [_switchMain setOn:YES];
        [_switchMain setOnTintColor:[ColorConstants appBrownColor]];
    }
    
}

@end

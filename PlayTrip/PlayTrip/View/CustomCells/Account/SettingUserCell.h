
#import <UIKit/UIKit.h>

@interface SettingUserCell : UITableViewCell

@property (nonatomic) IBOutlet UILabel * lblKey;
@property (strong, nonatomic) IBOutlet UITextField *txtValue;

@end

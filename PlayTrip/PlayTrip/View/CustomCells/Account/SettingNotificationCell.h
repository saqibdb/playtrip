
#import <UIKit/UIKit.h>

@interface SettingNotificationCell : UITableViewCell

@property (nonatomic) IBOutlet UILabel * lblKey;
@property (strong, nonatomic) IBOutlet UISwitch *switchMain;

@end


#import <UIKit/UIKit.h>

@interface TagsCollectionCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblMain;
@property (strong, nonatomic) IBOutlet UIButton *btnClose;

@end

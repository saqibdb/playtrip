
#import <UIKit/UIKit.h>
#import "URLImageView.h"
@interface DistrictCollectionCell : UICollectionViewCell

@property (nonatomic) IBOutlet URLImageView * imgMain;

@property (nonatomic) IBOutlet UILabel * lblName;

@property (nonatomic) IBOutlet UILabel * lblCount;


@end

//
//  DistrictCityCountCell.h
//  PlayTrip
//
//  Created by ibuildx on 6/9/17.
//  Copyright © 2017 SamirMAC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCMBorderView.h"


@interface DistrictCityCountCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleText;
@property (weak, nonatomic) IBOutlet CCMBorderView *outerBorderView;
@property (weak, nonatomic) IBOutlet UIButton *titleButton;

@end

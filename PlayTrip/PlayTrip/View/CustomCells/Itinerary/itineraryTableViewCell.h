
#import <UIKit/UIKit.h>
#import "URLImageView.h"

@interface ItineraryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnDesc;
@property (weak, nonatomic) IBOutlet UIButton *btnPlayVideo;

@property (weak, nonatomic) IBOutlet UIButton *btnGreyDelete;
@property (weak, nonatomic) IBOutlet UILabel * lblTime;
@property (weak, nonatomic) IBOutlet URLImageView *imgFull;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblBookmarkCount;
@property (weak, nonatomic) IBOutlet UILabel *lblViewsCount;
@property (weak, nonatomic) IBOutlet UILabel *lblShareCount;
@property (weak, nonatomic) IBOutlet UIView *borderView;
@property (weak, nonatomic) IBOutlet UIButton *btnCategory;
@property (weak, nonatomic) IBOutlet UIButton *btnTime;
@property (weak, nonatomic) IBOutlet UIButton *btnEndTime;






@property (weak, nonatomic) IBOutlet UIView *bookmarkView;
@property (weak, nonatomic) IBOutlet UIView *viewsView;
@property (weak, nonatomic) IBOutlet UIView *shareView;
@property (weak, nonatomic) IBOutlet URLImageView *imgCategory;
@property (weak, nonatomic) IBOutlet UIButton *bookMarkButton;
@property (weak, nonatomic) IBOutlet UIButton *dragButton;

@end

//
//  AddAttractionCell.h
//  PlayTrip
//
//  Created by MAC2 on 2016-12-27.
//  Copyright © 2016 SamirMAC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddAttractionCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblPlanName;
@property (strong, nonatomic) IBOutlet UILabel *lblNumber;
@end

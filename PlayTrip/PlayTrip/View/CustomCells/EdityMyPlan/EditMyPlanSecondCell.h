
#import <UIKit/UIKit.h>

@interface EditMyPlanSecondCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIImageView *imgLeft;
@property (weak, nonatomic) IBOutlet UILabel *lblMain;
@property (weak, nonatomic) IBOutlet UIImageView *imgWarning;
@property (weak, nonatomic) IBOutlet UIImageView *imgSelected;
@property (weak, nonatomic) IBOutlet UIButton *btnSelected;

@property (weak, nonatomic) IBOutlet UIButton *iteneraryBtn;
@property (weak, nonatomic) IBOutlet UIButton *airTickerBtn;
@property (weak, nonatomic) IBOutlet UIButton *hotelBtn;
@property (weak, nonatomic) IBOutlet UIButton *carBtn;


@end

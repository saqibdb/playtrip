
#import <UIKit/UIKit.h>

@interface EditMyPlanFirstCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *txtPlanName;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UIView *routeView;
@property (weak, nonatomic) IBOutlet UIButton *btnRoute;
@property (weak, nonatomic) IBOutlet UILabel *lblDays;
@property (weak, nonatomic) IBOutlet UIButton *btnLanguage;
@property (weak, nonatomic) IBOutlet UITextField* totf;
@property (weak, nonatomic) IBOutlet UILabel* lblRouteMap;
@property (weak, nonatomic) IBOutlet UIButton *fromButton;
@property (weak, nonatomic) IBOutlet UIButton *toButton;





@end


#import <UIKit/UIKit.h>

@interface LanguageTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblLanguage;
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;
@property (weak, nonatomic) IBOutlet UILabel *lblLine;


@end

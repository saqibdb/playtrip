
#import "VideoCollCell.h"

@implementation VideoCollCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.imgMain.layer.cornerRadius = 5.0;
    self.imgMain.layer.masksToBounds = YES;
}

@end

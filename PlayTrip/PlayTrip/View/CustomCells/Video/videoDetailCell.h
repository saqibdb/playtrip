

#import <UIKit/UIKit.h>
#import "URLImageView.h"

@interface videoDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnCheck;
@property (weak, nonatomic) IBOutlet URLImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lbltime;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;

@end

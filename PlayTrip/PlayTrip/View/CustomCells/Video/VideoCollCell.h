
#import <UIKit/UIKit.h>

@interface VideoCollCell : UICollectionViewCell {}

@property (weak, nonatomic) IBOutlet UIImageView *imgMain;
@property (weak, nonatomic) IBOutlet UIButton *btnDelete;
@property (weak, nonatomic) IBOutlet UIButton *btnFull;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;

@end

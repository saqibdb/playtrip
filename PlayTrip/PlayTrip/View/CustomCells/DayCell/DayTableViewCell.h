
#import <UIKit/UIKit.h>

@interface DayTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIButton *btnLeftMinus;
@property (weak, nonatomic) IBOutlet UIButton *btnLeftPlus;

@property (weak, nonatomic) IBOutlet UIButton *btnRightMinus;
@property (weak, nonatomic) IBOutlet UIButton *btnRightPlus;
@property (weak, nonatomic) IBOutlet UITextField *txtLeftDigit;
@property (weak, nonatomic) IBOutlet UITextField *txtRightDigit;

@property (strong, nonatomic) IBOutlet UILabel *lblTo;
@end

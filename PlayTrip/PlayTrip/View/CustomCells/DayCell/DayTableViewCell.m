
#import "DayTableViewCell.h"

@implementation DayTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self textBottomLineWithTextField:_txtLeftDigit];
    [self textBottomLineWithTextField:_txtRightDigit];
}

-(void) textBottomLineWithTextField:(UITextField *)txt{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 2;
    border.borderColor = [UIColor colorWithRed:212/255.0f green:212/255.0f blue:212/255.0f alpha:1.0f].CGColor;
    // 320 - 36
    // x - x*0.1125
    border.frame = CGRectMake(0, txt.frame.size.height - borderWidth, self.frame.size.width*0.1125f/*txt.frame.size.width*/, txt.frame.size.height);
    border.borderWidth = borderWidth;
    //[txt.layer addSublayer:border];
    txt.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (IBAction)actionRightPlus:(id)sender{
    _txtRightDigit.text = [NSString stringWithFormat:@"%d",[_txtRightDigit.text intValue]+1];
}

- (IBAction)actionRightMinus:(id)sender{
    if([_txtRightDigit.text integerValue] > 1){
        _txtRightDigit.text = [NSString stringWithFormat:@"%d",[_txtRightDigit.text intValue]-1];
    }
}

- (IBAction)actionLeftPlus:(id)sender{
    _txtLeftDigit.text = [NSString stringWithFormat:@"%d",[_txtLeftDigit.text intValue]+1];
}

- (IBAction)actionLeftminus:(id)sender{
    if([_txtLeftDigit.text integerValue] > 1){
        _txtLeftDigit.text = [NSString stringWithFormat:@"%d",[_txtLeftDigit.text intValue]-1];
    }
}

@end

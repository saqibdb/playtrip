
#import <UIKit/UIKit.h>
#import "URLImageView.h"

@interface AttractionsVideoCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet URLImageView *imgUserImg;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (strong, nonatomic) IBOutlet UILabel *lblFromDate;
@property (strong, nonatomic) IBOutlet UILabel *lblVideoName;
@property (strong, nonatomic) IBOutlet UIButton *btnBookMark;
- (IBAction)bookmarkClicked:(id)sender;
@property (strong, nonatomic) IBOutlet URLImageView *imgThumbnail;

@property (strong, nonatomic) IBOutlet UILabel *lblTTotalView;
@property (strong, nonatomic) IBOutlet UILabel *lblTTotalBookMark;
@property (strong, nonatomic) IBOutlet UILabel *lblTTotalShare;
@property (weak, nonatomic) IBOutlet UIView *mainView;


@end

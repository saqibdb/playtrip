
#import "AttractionsVideoCell.h"

@implementation AttractionsVideoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _mainView.layer.borderWidth = 1;
    
    _mainView.layer.borderColor = [UIColor colorWithRed:235/255.f green:235/255.f blue:235/255.f alpha:1.0f].CGColor;
}
- (IBAction)bookmarkClicked:(id)sender {
    if (_btnBookMark.selected) {
        _btnBookMark.selected = NO;
        [_btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
    } else {
        _btnBookMark.selected = YES;
        [_btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
    }
}

@end

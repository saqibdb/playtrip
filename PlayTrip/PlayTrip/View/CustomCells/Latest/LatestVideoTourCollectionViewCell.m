
#import "LatestVideoTourCollectionViewCell.h"

@implementation LatestVideoTourCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.contentView.layer.borderWidth = 1;
    
    self.contentView.layer.borderColor = [UIColor colorWithRed:235/255.f green:235/255.f blue:235/255.f alpha:1.0f].CGColor;
    
}

- (IBAction)bookmarkClicked:(id)sender {
    if (_btnBookMark.selected) {
        _btnBookMark.selected = NO;
        [_btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
    } else {
        _btnBookMark.selected = YES;
        [_btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
    }
}

@end

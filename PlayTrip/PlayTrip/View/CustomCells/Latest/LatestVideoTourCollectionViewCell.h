
#import <UIKit/UIKit.h>
#import "URLImageView.h"

@interface LatestVideoTourCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (strong, nonatomic) IBOutlet URLImageView *imgUserImg;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (strong, nonatomic) IBOutlet UILabel *lblFromDate;
@property (strong, nonatomic) IBOutlet UILabel *lblVideoName;
@property (strong, nonatomic) IBOutlet UILabel *lblDescription;
@property (strong, nonatomic) IBOutlet URLImageView *imgThumbnail;
@property (strong, nonatomic) IBOutlet UIButton *btnBookMark;

@property (strong, nonatomic) IBOutlet UILabel *lblBookmarkCount;
@property (strong, nonatomic) IBOutlet UILabel *lblViewCount;
@property (strong, nonatomic) IBOutlet UILabel *lblShareCount;
@property (strong, nonatomic) IBOutlet UILabel *lblRevertCount;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblLanguage;
@property (weak, nonatomic) IBOutlet UILabel *lblCountry;
@property (weak, nonatomic) IBOutlet UILabel *lblDistrict;
@property (weak, nonatomic) IBOutlet UILabel *lblCity;
@property (weak, nonatomic) IBOutlet UIImageView *languageImage;


- (IBAction)bookmarkClicked:(id)sender;

@end

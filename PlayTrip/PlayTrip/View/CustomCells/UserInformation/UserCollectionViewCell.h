//
//  UserCollectionViewCell.h
//  PlayTrip
//
//  Created by MAC5 on 23/01/17.
//  Copyright © 2017 SamirMAC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "URLImageView.h"

@interface UserCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIView *mainView;

@property (nonatomic) IBOutlet URLImageView * imgUser;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UIButton *btnBookmark;

@property (strong, nonatomic) IBOutlet UILabel *lblPlay;
@property (strong, nonatomic) IBOutlet UILabel *lblMoney;
@property (strong, nonatomic) IBOutlet UILabel *lblBookMark;
@property (weak, nonatomic) IBOutlet UILabel *lblViews;
@property (weak, nonatomic) IBOutlet UILabel *lblShares;






- (IBAction)bookmarkClicked:(id)sender;

@end

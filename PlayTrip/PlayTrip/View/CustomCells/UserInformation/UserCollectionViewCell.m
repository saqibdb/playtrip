//
//  UserCollectionViewCell.m
//  PlayTrip
//
//  Created by MAC5 on 23/01/17.
//  Copyright © 2017 SamirMAC. All rights reserved.
//

#import "UserCollectionViewCell.h"

@implementation UserCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.imgUser.layer.cornerRadius = self.imgUser.layer.frame.size.height / 2;
    self.imgUser.layer.masksToBounds = true;
    self.contentView.layer.borderWidth = 1;
    
    self.contentView.layer.borderColor = [UIColor colorWithRed:235/255.f green:235/255.f blue:235/255.f alpha:1.0f].CGColor;
}
-(void)prepareForReuse {
    self.imgUser.image = nil;
}

- (IBAction)bookmarkClicked:(id)sender {
    if (_btnBookmark.selected) {
        _btnBookmark.selected = NO;
        [_btnBookmark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
    } else {
        _btnBookmark.selected = YES;
        [_btnBookmark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
    }
}


@end

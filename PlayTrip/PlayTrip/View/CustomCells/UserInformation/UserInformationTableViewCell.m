
#import "UserInformationTableViewCell.h"

@implementation UserInformationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
     _btnView.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (IBAction)listClicked:(id)sender {
    if(select){
        select = false;
         _btnView.hidden = YES;
    } else {
        select = true;
         _btnView.hidden = NO;
    }
    
}
@end

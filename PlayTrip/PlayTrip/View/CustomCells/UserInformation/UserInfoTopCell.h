//
//  UserInfoTopCell.h
//  PlayTrip
//
//  Created by MAC5 on 27/01/17.
//  Copyright © 2017 SamirMAC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "URLImageView.h"

@interface UserInfoTopCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet URLImageView *imgCover;
@property (weak, nonatomic) IBOutlet UILabel *lblBookmarkCount;
@property (weak, nonatomic) IBOutlet UILabel *lblRewardCount;
@property (weak, nonatomic) IBOutlet URLImageView *imgAvatar;
@property (weak, nonatomic) IBOutlet UIButton *btnBookmark;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalVideoTourCount;




@property (strong, nonatomic) IBOutlet UILabel *lblMoney;
@property (strong, nonatomic) IBOutlet UILabel *lblBookMark;
@property (weak, nonatomic) IBOutlet UILabel *lblViews;
@property (weak, nonatomic) IBOutlet UILabel *lblShares;



@end

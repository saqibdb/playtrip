
#import <UIKit/UIKit.h>
#import "URLImageView.h"
@interface UserInformationTableViewCell : UITableViewCell{
    BOOL select;
}
@property (strong, nonatomic) IBOutlet URLImageView *imgVideoTour;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;

@property (strong, nonatomic) IBOutlet UILabel *lblVideoTourName;
@property (strong, nonatomic) IBOutlet UILabel *lblCreateDate;
@property (strong, nonatomic) IBOutlet UILabel *lblDescription;
@property (strong, nonatomic) IBOutlet UILabel *lblBookmarks;
@property (strong, nonatomic) IBOutlet UILabel *lblViews;
@property (strong, nonatomic) IBOutlet UILabel *lblShares;
@property (strong, nonatomic) IBOutlet UILabel *lblRevert;
@property (strong, nonatomic) IBOutlet UIButton *btnList;
@property (strong, nonatomic) IBOutlet UIButton *btnDelete;
@property (strong, nonatomic) IBOutlet UIButton *btnEdit;
@property (strong, nonatomic) IBOutlet UIButton *btnBookmark;
@property (strong, nonatomic) IBOutlet UIView *btnView;
@property (strong, nonatomic) IBOutlet UIButton *btnBookMark;
@property (weak, nonatomic) IBOutlet UILabel *lblCountry;
@property (weak, nonatomic) IBOutlet UILabel *lblDistrict;
@property (weak, nonatomic) IBOutlet UILabel *lblCity;
@property (weak, nonatomic) IBOutlet UILabel *lblLanguge;
@property (weak, nonatomic) IBOutlet UIImageView *languageImage;


- (IBAction)listClicked:(id)sender;

@end


#import <UIKit/UIKit.h>
#import "URLImageView.h"

@interface LocationCell : UITableViewCell

@property (strong, nonatomic) IBOutlet URLImageView *imgAvatar;
@property (strong, nonatomic) IBOutlet UILabel *lblname;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UIButton *btnDelete;


@end

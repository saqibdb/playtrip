
#import "VideoSortTableViewCell.h"

@implementation VideoSortTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}
- (IBAction)actionBtnSelected:(id)sender {
    if (![_btnFirst isSelected]) {
        [_btnFirst setSelected:YES];
        [_btnSecond setSelected:NO];
    }else{
        [_btnFirst setSelected:NO];
        [_btnSecond setSelected:YES];
    }
}

@end

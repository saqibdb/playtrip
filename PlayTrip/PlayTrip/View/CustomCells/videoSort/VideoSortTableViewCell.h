
#import <UIKit/UIKit.h>

@interface VideoSortTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblText;
@property (strong, nonatomic) IBOutlet UIButton *btnFirst;
@property (strong, nonatomic) IBOutlet UIButton *btnSecond;

@end

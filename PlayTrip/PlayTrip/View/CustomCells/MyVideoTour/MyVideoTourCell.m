//
//  MyVideoTourCell.m
//  PlayTrip
//
//  Created by samcom.niki on 21/03/17.
//  Copyright © 2017 SamirMAC. All rights reserved.
//

#import "MyVideoTourCell.h"
#import "ColorConstants.h"
#import "Localisator.h"

@implementation MyVideoTourCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _routeView.layer.cornerRadius =_routeView.frame.size.height / 2;
    
   _tipsView.layer.borderColor = [ColorConstants appBrownColor].CGColor;
    _tipsView.layer.borderWidth = 1;
    _tipsView.layer.cornerRadius = 8;
    
    _fromView.layer.borderColor = [ColorConstants appBrownColor].CGColor;
    _fromView.layer.borderWidth = 1;
    
    _toView.layer.borderColor = [ColorConstants appBrownColor].CGColor;
    _toView.layer.borderWidth = 1;

    _fromView.layer.cornerRadius = _fromView.layer.frame.size.height/2;
    _toView.layer.cornerRadius = _toView.layer.frame.size.height/2;
    
    [self.txtTips setContentInset:UIEdgeInsetsMake(-1, -5, 0, 0)];
    [self.txtTips setTextColor:[UIColor grayColor]];
    [self.txtTips setText:LOCALIZATION(@"share_tips")];
    [self.txtTips setDelegate:self];
}

- (IBAction)noClicked:(id)sender {
    [_btnYes setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnNo setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    
}

- (IBAction)yesClicked:(id)sender {
    
    [_btnNo setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [_btnYes setImage:[UIImage imageNamed:@"RadioButton-Selected.png"]
            forState:UIControlStateNormal];
   
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if([textView.text isEqualToString:LOCALIZATION(@"share_tips")]){
        textView.text = @"";
        [textView setTextColor:[UIColor blackColor]];
    }
    return true;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    if([textView.text isEqualToString:@""]){
        textView.text = LOCALIZATION(@"share_tips");
        [textView setTextColor:[UIColor grayColor]];
    }
    return true;
}

@end

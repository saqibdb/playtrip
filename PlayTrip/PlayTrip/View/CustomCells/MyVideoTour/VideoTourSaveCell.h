
#import <UIKit/UIKit.h>

@interface VideoTourSaveCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIButton *btnContinueVideoTour;
//@property (weak, nonatomic) IBOutlet UIButton *btnAddAttraction;

@property (weak, nonatomic) IBOutlet UIButton *btnDeleteDraft;

@property (weak, nonatomic) IBOutlet UIButton * btnSaveToDevice;
@property (weak, nonatomic) IBOutlet UIButton *btnUpload;


@property (strong, nonatomic) IBOutlet UIButton *lblContinueVidTour;




@end


#import <UIKit/UIKit.h>
#import "URLImageView.h"

@interface CategoryCell : UICollectionViewCell {}

@property (weak, nonatomic) IBOutlet URLImageView *imgMain;

@end


#import "VideoTourSaveCell.h"
#import "ColorConstants.h"

@implementation VideoTourSaveCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _btnContinueVideoTour.layer.cornerRadius = 5;
    _btnDeleteDraft.layer.cornerRadius = 5;
    _btnSaveToDevice.layer.cornerRadius = 5;
    _btnUpload.layer.cornerRadius = 5;

    
    _btnDeleteDraft.backgroundColor = [ColorConstants appRedColor];
  //  _btnContinueVideoTour.backgroundColor = [ColorConstants appYellowColor];
    _btnSaveToDevice.backgroundColor = [ColorConstants appBrownColor];
    _btnUpload.backgroundColor = [ColorConstants appYellowColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end

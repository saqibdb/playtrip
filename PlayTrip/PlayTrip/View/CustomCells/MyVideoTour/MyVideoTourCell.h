
#import <UIKit/UIKit.h>
#import "URLImageView.h"

@interface MyVideoTourCell : UITableViewCell <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UIButton *btnRoute;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UIImageView *imgSpeaker;
@property (weak, nonatomic) IBOutlet UIView *fromView;
@property (weak, nonatomic) IBOutlet UIView *toView;
@property (weak, nonatomic) IBOutlet UIView *routeView;
@property (weak, nonatomic) IBOutlet UIView *tipsView;
@property (weak, nonatomic) IBOutlet UIButton *btnYes;
@property (weak, nonatomic) IBOutlet UIButton *btnNo;
@property (weak, nonatomic) IBOutlet UILabel *lblFromDate;
@property (weak, nonatomic) IBOutlet UILabel *lblToDate;
@property (weak, nonatomic) IBOutlet UIButton *btnSpeaker;
@property (weak, nonatomic) IBOutlet UIButton *btnFromDate;
@property (weak, nonatomic) IBOutlet UIButton *btnToDate;
@property (weak, nonatomic) IBOutlet UITextView *txtTips;
@property (weak, nonatomic) IBOutlet URLImageView *imgMain;
@property (weak, nonatomic) IBOutlet UIButton *btnMainImage;



@property (strong, nonatomic) IBOutlet UILabel *lblRouteMap;
@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@property (strong, nonatomic) IBOutlet UILabel *lblTraveltips;
@property (strong, nonatomic) IBOutlet UILabel *lblItinerary;
@property (strong, nonatomic) IBOutlet UILabel *lblTo;
@property (strong, nonatomic) IBOutlet UILabel *lblSelfDrive;
@property (strong, nonatomic) IBOutlet UILabel *lblYes;
@property (strong, nonatomic) IBOutlet UILabel *lblNo;






- (IBAction)noClicked:(id)sender;
- (IBAction)yesClicked:(id)sender;
@end


//
//  AttractionModel.m
//  PlayTrip
//
//  Created by ibuildx on 6/6/17.
//  Copyright © 2017 SamirMAC. All rights reserved.
//

#import "AttractionModel.h"

@implementation AttractionModel

/*
 @property (nonatomic) NSString *_id;
 
 
 @property (nonatomic) NSString<Optional> *name; +
 
 
 @property (nonatomic) NSString<Optional> *loc_long; +
 @property (nonatomic) NSString<Optional> *loc_lat; +
 @property (nonatomic) NSString<Optional> *last_updated; -
 @property (nonatomic) NSString<Optional> *create_date; -
 @property (nonatomic) NSString<Optional> *geotype; -
 @property (nonatomic) NSString<Optional> *cover_photo_url; -
 @property (nonatomic) NSString<Optional> *description; +
 @property (nonatomic) NSString<Optional> *phone_no; +
 @property (nonatomic) NSString<Optional> *hours_info; -
 @property (nonatomic) NSString<Optional> *web_url; +
 @property (nonatomic) NSString<Optional> *address; +
 @property (nonatomic) NSString<Optional> *place_id; -
 
 
 
 @property (nonatomic) NSNumber<Optional> *__v;
 @property (nonatomic) NSNumber<Optional> *is_deleted;
 @property (nonatomic) NSNumber<Optional> *total_view;
 @property (nonatomic) NSNumber<Optional> *total_share;
 @property (nonatomic) NSNumber<Optional> *total_bookmark;
 @property (nonatomic) NSString<Optional> *detail; +
 
 
 @property (nonatomic) UserModel<Optional> *user;
 
 @property (nonatomic) NSMutableArray<Optional> *remarks;
 @property (nonatomic) NSMutableArray<Optional> *tags;
 @property (nonatomic) NSMutableArray<Optional> *category;
 
 @property (nonatomic) NSMutableArray<UserModel>  *bookmark_users;
 
 @property (nonatomic) Country<Optional> *country_id;
 @property (nonatomic) DistrictModel<Optional> *district_id;
 @property (nonatomic) CityModel<Optional> *city_id;
 
 
 @property (nonatomic) NSMutableArray<Optional, AttractionDataModel> *attr_data;
 
 
 @property (nonatomic) NSNumber<Optional> *remuneration_amount;
 @property (nonatomic) NSNumber<Optional> *refer_count;
 @property (nonatomic) NSNumber<Optional> *total_attractions;
 
 @property (nonatomic) NSNumber<Optional> *day; +
 */

- (instancetype)initWithDraftAttraction:(DraftAttraction*)da {
    self = [super init];
    if (self) {
        self.name = da.name;
        self.loc_long = da.loc_lng.stringValue;
        self.loc_lat = da.loc_lat.stringValue;
        self.descriptionAM = da.description;
        self.phone_no = da.phone_no;
        self.web_url = da.web_url;
        self.address = da.address;
        self.day = da.day;
        self.detail = da.detail;
        
        NSArray* dadArray = da.draftAttrData.allObjects;
        self.attr_data = [[NSMutableArray<Optional, AttractionDataModel> alloc]init];
        for(int i=0; i<dadArray.count; i++){
            DraftAttrData* dad = dadArray[i];
            AttractionDataModel* adm = [[AttractionDataModel alloc]initWithDraftAttrData:dad];
            [self.attr_data addObject:adm];
        }
    }
    return self;
}

@end

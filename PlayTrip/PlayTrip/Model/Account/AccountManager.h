
#import <Foundation/Foundation.h>
#import "Account.h"

@interface AccountManager : NSObject {
    Account *_activeAccount;
    
    NSMutableDictionary *_savedAccounts;
}

@property (nonatomic, readwrite, retain)Account *activeAccount;

+(AccountManager*)Instance;

-(void)saveAccount;

@end


#import <Foundation/Foundation.h>
#import "RequestKit.h"
#import "AccountAuthenticateDelegate.h"

@interface Account : NSObject<NSCoding,RequestDelegate>

@property (nonatomic, readwrite, retain) id<AccountAuthenticateDelegate> delegate;

@property (nonatomic, strong) NSString *email,*first_name,*full_name,*phone_number,*provider,*last_name,*image, *userId,*username,*password,*api_key,*OldPassword,*Newpassword,*create_date,*group_id,*cover_pic,*avatar,*last_updated, *bank_account, *bank_name, * holder_name, *currency, *language, *totalBookmark, *totalViews, *totalShares,*userToken;

@property (nonatomic, strong) NSNumber * notification_get_reward, *notification_bookmark_user_update, *notification_reward_information, *notification_made_reservation, *wifiOnly;

@property (nonatomic, readonly) BOOL authenticated;
@property (nonatomic) BOOL isSkip, isLoggedIn;

@property (nonatomic,strong)NSDictionary *loginTypeObject;

- (void)saveAccountWithDict:(NSDictionary *)dict;
-(void)saveLanguage:(id<AccountAuthenticateDelegate>) del;
- (void)authenticateWithDelegate:(id<AccountAuthenticateDelegate>) del;
- (void)authenticateWithLoc:(NSMutableArray *)loc WithDelegate:(id<AccountAuthenticateDelegate>) del;
- (void)changePasswordWithDelegate:(id<AccountAuthenticateDelegate>) del;
- (void)registerWithDelegate:(id<AccountAuthenticateDelegate>) del;
- (void)getUserDetails:(id<AccountAuthenticateDelegate>) del;
- (void)updateUserDetails:(id<AccountAuthenticateDelegate>) del;
- (void)uploadAvtar:(UIImage *)img WithDelegate:(id<AccountAuthenticateDelegate>) del;
- (void)uploadCoverPic:(UIImage *)img WithDelegate:(id<AccountAuthenticateDelegate>) del;
@end


#import <Foundation/Foundation.h>

@protocol LoginDelegate <NSObject>

- (void) loginSuccessfully;

@end

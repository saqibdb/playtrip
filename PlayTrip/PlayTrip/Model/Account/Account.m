
#import "Account.h"
#import "AppDelegate.h"
#import "Utils.h"
#import "URLSchema.h"

#define ENCODING_VERSION        2

@implementation Account

-(id)init{
    self = [super init];
    _isSkip = NO;
    _isLoggedIn = NO;
    _userId = @"";
    _email = @"" ;
    _username = @"" ;
    _last_name = @"";
    _first_name = @"" ;
    _group_id = @"";
    _cover_pic = @"";
    _avatar = @"" ;
    _phone_number = @"" ;
    _last_updated = @"" ;
    _create_date = @"" ;
    _provider = @"" ;
    _full_name = @"";
    _userToken = @"";
    _bank_account = @"" ;
    _bank_name = @"" ;
    _currency = @"" ;
    _language = @"";
    _totalViews = @"";
    _totalShares = @"";
    _totalBookmark = @"";
    _notification_get_reward = [NSNumber numberWithBool:NO] ;
    _notification_bookmark_user_update = [NSNumber numberWithBool:NO];
    _notification_reward_information = [NSNumber numberWithBool:NO];
    _notification_made_reservation = [NSNumber numberWithBool:NO];
    _wifiOnly  = [NSNumber numberWithBool:YES];
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super init];
    if (self) {
        _isSkip = [[aDecoder decodeObjectForKey:@"isSkip"]boolValue];
        _isLoggedIn = [[aDecoder decodeObjectForKey:@"isLoggedIn"]boolValue] ;
        _userId = [aDecoder decodeObjectForKey:@"userId"];
        _email = [aDecoder decodeObjectForKey:@"email"] ;
        _username = [aDecoder decodeObjectForKey:@"username"] ;
        _last_name = [aDecoder decodeObjectForKey:@"last_name"];
        _first_name = [aDecoder decodeObjectForKey:@"first_name"] ;
        _group_id = [aDecoder decodeObjectForKey:@"group_id"];
        _cover_pic = [aDecoder decodeObjectForKey:@"cover_pic"];
        _avatar = [aDecoder decodeObjectForKey:@"avatar"] ;
        _phone_number = [aDecoder decodeObjectForKey:@"phone_number"] ;
        _last_updated = [aDecoder decodeObjectForKey:@"last_updated"] ;
        _create_date = [aDecoder decodeObjectForKey:@"create_date"] ;
        _provider = [aDecoder decodeObjectForKey:@"provider"] ;
        _full_name = [aDecoder decodeObjectForKey:@"full_name"];
        _userToken = [aDecoder decodeObjectForKey:@"token"];
        _bank_account = [aDecoder decodeObjectForKey:@"bank_account"] ;
        _bank_name = [aDecoder decodeObjectForKey:@"bank_name"] ;
        _currency = [aDecoder decodeObjectForKey:@"currency"] ;
        _language = [aDecoder decodeObjectForKey:@"language"];
        _totalViews = [aDecoder decodeObjectForKey:@"total_view"];
        _totalShares = [aDecoder decodeObjectForKey:@"total_share"];
        _totalBookmark = [aDecoder decodeObjectForKey:@"total_bookmark"];
        _notification_get_reward = [aDecoder decodeObjectForKey:@"notification_get_reward"] ;
        _notification_bookmark_user_update = [aDecoder decodeObjectForKey:@"notification_bookmark_user_update"] ;
        _notification_reward_information = [aDecoder decodeObjectForKey:@"notification_reward_information"] ;
        _notification_made_reservation = [aDecoder decodeObjectForKey:@"notification_made_reservation"];
        _wifiOnly = [aDecoder decodeObjectForKey:@"wifiOnly"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:[NSNumber numberWithBool:_isSkip] forKey:@"isSkip"];
    [aCoder encodeObject:[NSNumber numberWithBool:_isLoggedIn] forKey:@"isLoggedIn"];
    [aCoder encodeObject:_userId forKey:@"userId"];
    [aCoder encodeObject:_email forKey:@"email"];
    [aCoder encodeObject:_username forKey:@"username"];
    [aCoder encodeObject:_last_name forKey:@"last_name"];
    [aCoder encodeObject:_first_name forKey:@"first_name"];
    [aCoder encodeObject:_group_id forKey:@"group_id"];
    [aCoder encodeObject:_cover_pic forKey:@"cover_pic"];
    [aCoder encodeObject:_avatar forKey:@"avatar"];
    [aCoder encodeObject:_phone_number forKey:@"phone_number"];
    [aCoder encodeObject:_last_updated forKey:@"last_updated"];
    [aCoder encodeObject:_create_date forKey:@"create_date"];
    [aCoder encodeObject:_provider forKey:@"provider"];
    [aCoder encodeObject:_full_name forKey:@"full_name"];
    [aCoder encodeObject:_bank_account forKey:@"bank_account"];
    [aCoder encodeObject:_bank_name forKey:@"bank_name"];
    [aCoder encodeObject:_currency forKey:@"currency"];
    [aCoder encodeObject:_language forKey:@"language"];
    [aCoder encodeObject:_totalViews forKey:@"total_view"];
    [aCoder encodeObject:_totalShares forKey:@"total_share"];
    [aCoder encodeObject:_totalBookmark forKey:@"total_bookmark"];
    [aCoder encodeObject:_userToken forKey:@"token"];

    
    [aCoder encodeObject:_notification_get_reward forKey:@"notification_get_reward"];
    [aCoder encodeObject:_notification_bookmark_user_update forKey:@"notification_bookmark_user_update"];
    [aCoder encodeObject:_notification_reward_information forKey:@"notification_reward_information"];
    [aCoder encodeObject:_notification_made_reservation forKey:@"notification_made_reservation"];
    [aCoder encodeObject:_wifiOnly forKey:@"wifiOnly"];
}

-(void)saveAccountWithDict:(NSDictionary *)dict {
    if ([dict objectForKey:@"_id"] && [dict objectForKey:@"_id"] != nil) {
        _userId = [dict objectForKey:@"_id"];
    }
    if ([dict objectForKey:@"email"] && [dict objectForKey:@"email"] != nil) {
        _email = [dict objectForKey:@"email"];
    }
    if ([dict objectForKey:@"user_name"] && [dict objectForKey:@"user_name"] != nil) {
        _username = [dict objectForKey:@"user_name"];
    }
    if ([dict objectForKey:@"last_name"] && [dict objectForKey:@"last_name"] != nil) {
        _last_name = [dict objectForKey:@"last_name"];
    }
    if ([dict objectForKey:@"first_name"] && [dict objectForKey:@"first_name"] != nil) {
        _first_name = [dict objectForKey:@"first_name"];
    }
    if ([dict objectForKey:@"full_name"] && [dict objectForKey:@"full_name"] != nil) {
        _full_name = [dict objectForKey:@"full_name"];
    }

    if ([dict objectForKey:@"group_id"] && [dict objectForKey:@"group_id"] != nil) {
        _group_id = [dict objectForKey:@"group_id"];
    }
    if ([dict objectForKey:@"cover_pic"] && [dict objectForKey:@"cover_pic"] != nil) {
        _cover_pic = [dict objectForKey:@"cover_pic"];
    }
    if ([dict objectForKey:@"avatar"] && [dict objectForKey:@"avatar"] != nil) {
        _avatar = [dict objectForKey:@"avatar"];
    }
    if ([dict objectForKey:@"phone_number"] && [dict objectForKey:@"phone_number"] != nil) {
        _phone_number = [dict objectForKey:@"phone_number"];
    }
    if ([dict objectForKey:@"last_updated"] && [dict objectForKey:@"last_updated"] != nil) {
        _last_updated = [dict objectForKey:@"last_updated"];
    }
    if ([dict objectForKey:@"create_date"] && [dict objectForKey:@"create_date"] != nil) {
        _create_date = [dict objectForKey:@"create_date"];
    }
    if ([dict objectForKey:@"provider"] && [dict objectForKey:@"provider"] != nil) {
        _provider = [dict objectForKey:@"provider"];
    }
    if ([dict objectForKey:@"bank_account"] && [dict objectForKey:@"bank_account"] != nil) {
        _bank_account = [dict objectForKey:@"bank_account"];
    }
    if ([dict objectForKey:@"bank_name"] && [dict objectForKey:@"bank_name"] != nil) {
        _bank_name = [dict objectForKey:@"bank_name"];
    }
    
    if ([dict objectForKey:@"acc_hold_name"] && [dict objectForKey:@"acc_hold_name"] != nil) {
        _holder_name = [dict objectForKey:@"acc_hold_name"];
    }
    
    
    if ([dict objectForKey:@"currency"] && [dict objectForKey:@"currency"] != nil) {
        _currency = [dict objectForKey:@"currency"];
    }
    if ([dict objectForKey:@"language"] && [dict objectForKey:@"language"] != nil) {
        _language = [dict objectForKey:@"language"];
    }
    if ([dict objectForKey:@"total_bookmark"] && [dict objectForKey:@"total_bookmark"] != nil) {
        _totalBookmark =[NSString stringWithFormat:@"%@",[dict objectForKey:@"total_bookmark"]];
    }
    if ([dict objectForKey:@"total_share"] && [dict objectForKey:@"total_share"] != nil) {
        _totalShares = [NSString stringWithFormat:@"%@",[dict objectForKey:@"total_share"]];
    }
    if ([dict objectForKey:@"total_view"] && [dict objectForKey:@"total_view"] != nil) {
        _totalViews = [NSString stringWithFormat:@"%@",[dict objectForKey:@"total_view"]];
    }
    if ([dict objectForKey:@"wifiOnly"] && [dict objectForKey:@"wifiOnly"] != nil) {
        _wifiOnly = [dict objectForKey:@"wifiOnly"];
    }
    
//    NSString * str = [NSString stringWithFormat:@"%@:%@",self.username , self.password];
//    NSString * base64String = [Utils encodeStringTo64:_userToken];
    NSString * str = [NSString stringWithFormat:@"Bearer %@",_userToken];
    
    [Utils setValue:str Key:kLoginAutheticationHeader];
  
    
    
    if ([dict objectForKey:@"notification"] && [dict objectForKey:@"notification"] != nil) {
        NSDictionary * notification = [dict objectForKey:@"notification"];
        
        if ([notification objectForKey:@"get_reward"] && [notification objectForKey:@"get_reward"] != nil) {
            _notification_get_reward = [notification objectForKey:@"get_reward"];
        }
        
        if ([notification objectForKey:@"bookmark_user_update"] && [notification objectForKey:@"bookmark_user_update"] != nil) {
            _notification_bookmark_user_update = [notification objectForKey:@"bookmark_user_update"];
        }
        
        if ([notification objectForKey:@"reward_information"] && [notification objectForKey:@"reward_information"] != nil) {
            _notification_reward_information = [notification objectForKey:@"reward_information"];
        }
        
        if ([notification objectForKey:@"made_reservation"] && [notification objectForKey:@"made_reservation"] != nil) {
            _notification_made_reservation = [notification objectForKey:@"made_reservation"];
        }
    }
}


@dynamic authenticated;

-(BOOL)authenticated {
    return _username != nil;
}

- (void)authenticateWithLoc:(NSMutableArray *)loc WithDelegate:(id<AccountAuthenticateDelegate>) del{
    _delegate = del;
    Request *request = [[Request alloc] initWithUrl:kLoginUrl andDelegate:self andMethod:POST];
    request.Tag = kLoginUrl;
    [request setParameter:_username forKey:@"user_name"];
    [request setParameter:_password forKey:@"password"];
    [request setParameter:loc forKey:@"loc"];
    [request startRequest];
}

- (void)authenticateWithDelegate:(id<AccountAuthenticateDelegate>) del{
    _delegate = del;
    Request *request = [[Request alloc] initWithUrl:kLoginUrl andDelegate:self andMethod:POST];
    request.Tag = kLoginUrl;
    [request setParameter:_username forKey:@"user_name"];
    [request setParameter:_password forKey:@"password"];
    [request startRequest];
}

- (void)registerWithDelegate:(id<AccountAuthenticateDelegate>) del{
    _delegate = del;
    Request *request = [[Request alloc] initWithUrl:kRegisterUrl andDelegate:self andMethod:POST];
    request.Tag = kRegisterUrl;

    if ([_provider isEqualToString:@""]) {
//        [request setParameter:_provider forKey:@"provider"];
        [request setParameter:_email forKey:@"email"];
        [request setParameter:_username forKey:@"user_name"];
        [request setParameter:_first_name forKey:@"first_name"];
        [request setParameter:_last_name forKey:@"last_name"];
        [request setParameter:_password forKey:@"password"];
    }else if ([_provider isEqualToString:@"facebook"]) {
        [request setParameter:_provider forKey:@"provider"];
        [request setParameter:_email forKey:@"email"];
        [request setParameter:_username forKey:@"user_name"];
        [request setParameter:_first_name forKey:@"first_name"];
        [request setParameter:_last_name forKey:@"last_name"];
        [request setParameter:_loginTypeObject forKey:@"facebook"];
    }else if ([_provider isEqualToString:@"Google"]) {
        [request setParameter:_provider forKey:@"provider"];
        [request setParameter:_email forKey:@"email"];
        [request setParameter:_username forKey:@"user_name"];
        [request setParameter:_username forKey:@"first_name"];
        [request setParameter:@"" forKey:@"last_name"];
        [request setParameter:_image forKey:@"Google"];
    }
    [request startRequest];
}

- (void)changePasswordWithDelegate:(id<AccountAuthenticateDelegate>) del{
    _delegate=del;
    Request *request = [[Request alloc] initWithUrl:kResetPassword andDelegate:self andMethod:POST];
    [request setParameter:_email forKey:@"email"];
    request.Tag = kResetPassword;
    [request startRequest];                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          }

- (void)getUserDetails:(id<AccountAuthenticateDelegate>) del{
    _delegate=del;
//    NSString * urlString = [NSString stringWithFormat:@"%@%@",kGetUserDetails,self.userId];
    Request *request = [[Request alloc] initWithUrl:kGetUserDetails andDelegate:self];
    request.Tag = kGetUserDetails;
    [request startRequest];
}

-(void)saveLanguage:(id<AccountAuthenticateDelegate>) del{
    _delegate=del;
    NSString * urlString = [NSString stringWithFormat:@"%@%@", kUpdateUserDetails,self.userId];
    Request *request = [[Request alloc] initWithUrl:urlString andDelegate:self andMethod:POST];
    BOOL isLangUpdated = [Utils getBOOLForKey:kIsUpdatedLanguage];
    
    if (isLangUpdated) {
        NSString * langStr = [Utils getValueForKey:kUpdatedLanguage];
        [request setParameter:langStr forKey:@"language"];
        request.Tag = kUpdateUserLanguage;
        [request startRequest];
    }
}

- (void)updateUserDetails:(id<AccountAuthenticateDelegate>) del{
    _delegate=del;
    //NSString * urlString = [NSString stringWithFormat:@"%@%@", kUpdateUserDetails,self.userId];
    Request *request = [[Request alloc] initWithUrl:kUpdateUserDetails andDelegate:self andMethod:POST];
    
    if (_phone_number) {
        [request setParameter:_phone_number forKey:@"phone_number"];
    }
//    if (_email) {
//        [request setParameter:_email forKey:@"email"];
//    }
    
    if (![_currency isEqual: @""]) {
        [request setParameter:_currency forKey:@"currency"];
    }
    if (_language) {
        [request setParameter:_language forKey:@"language"];
        
    }
    if (_holder_name) {
        [request setParameter:_holder_name forKey:@"acc_hold_name"];
    }
    if (_bank_account) {
        [request setParameter:_bank_account forKey:@"bank_account"];
    }
    if (_bank_name) {
        [request setParameter:_bank_name forKey:@"bank_name"];
    }
    [request setParameter:_wifiOnly forKey:@"wifiOnly"];
    
    NSMutableDictionary * notificationDict = [NSMutableDictionary new];
    
    [notificationDict setObject:_notification_get_reward forKey:@"get_reward"];
    [notificationDict setObject:_notification_bookmark_user_update forKey:@"bookmark_user_update"];
    [notificationDict setObject:_notification_made_reservation forKey:@"made_reservation"];
    [notificationDict setObject:_notification_reward_information forKey:@"reward_information"];
    
    [request setParameter:notificationDict forKey:@"notification"];
    
    request.Tag = kUpdateUserDetails;
    [request startRequest];
}

- (void)uploadAvtar:(UIImage *)img WithDelegate:(id<AccountAuthenticateDelegate>) del{
    _delegate=del;
    NSString * urlString = [NSString stringWithFormat:@"%@%@", kUploadUserAvatar,self.userId];
    Request *request = [[Request alloc] initWithUrl:urlString andDelegate:self andMethod:MULTI_PART_FORM];
    NSData *imageData = [NSData dataWithData:UIImageJPEGRepresentation(img, 1.0)];
    [request setParameter:imageData forKey:@"file"];
    request.Tag = kUploadUserAvatar;
    [request startRequest];
}

- (void)uploadCoverPic:(UIImage *)img WithDelegate:(id<AccountAuthenticateDelegate>) del{
    _delegate=del;
    NSString * urlString = [NSString stringWithFormat:@"%@%@", kUploadUserCoverPic,self.userId];
    Request *request = [[Request alloc] initWithUrl:urlString andDelegate:self andMethod:MULTI_PART_FORM];
    NSData *imageData = [NSData dataWithData:UIImageJPEGRepresentation(img, 1.0)];
    [request setParameter:imageData forKey:@"file"];
    request.Tag = kUploadUserCoverPic;
    [request startRequest];
}

#pragma RequestDelegate
- (void)RequestDidSuccess:(Request *)request {
    if(request.IsSuccess){
        if([request.Tag isEqual:kLoginUrl]) {
            NSDictionary *dictData = request.serverData;
            if ([[dictData objectForKey:@"is_error"] integerValue] > 0) {
                if (_delegate && [_delegate respondsToSelector:@selector(accountDidFailAuthentication:)]){
                    [_delegate accountDidFailAuthentication:[dictData objectForKey:@"message"]];
                    return;
                }
            }
            if ([dictData objectForKey:@"data"] && [dictData objectForKey:@"data"] != nil ) {
                NSDictionary *dictUser = [dictData objectForKey:@"data"];
                _userToken =[dictUser objectForKey:@"token"];
                if ([dictUser objectForKey:@"user"] && [dictUser objectForKey:@"user"] != nil) {
                    
                    NSDictionary *dict = [dictUser objectForKey:@"user"];
                    [self saveAccountWithDict:dict];
                }
            }
        } else if ([request.Tag isEqual:kRegisterUrl]) {
            NSDictionary *dictData = request.serverData;
            if ([[dictData objectForKey:@"is_error"] integerValue] > 0) {
                if (_delegate && [_delegate respondsToSelector:@selector(accountDidFailAuthentication:)]){
                    [_delegate accountDidFailAuthentication:[dictData objectForKey:@"message"]];
                    return;
                }
            }
            if ([dictData objectForKey:@"data"] && [dictData objectForKey:@"data"] != nil ) {
                NSDictionary *dictUser = [dictData objectForKey:@"data"];
                  _userToken =[dictUser objectForKey:@"token"];
                if ([dictUser objectForKey:@"user"] && [dictUser objectForKey:@"user"] != nil) {
                    NSDictionary *dict = [dictUser objectForKey:@"user"];
                    [self saveAccountWithDict:dict];
                }
            }
        } else if ([request.Tag isEqual:kUpdateUserLanguage]) {
            NSDictionary *dictData = request.serverData;
            if ([[dictData objectForKey:@"is_error"] integerValue] > 0) {
                if (_delegate && [_delegate respondsToSelector:@selector(accountDidFailAuthentication:)]){
                    [_delegate accountDidFailAuthentication:[dictData objectForKey:@"message"]];
                    return;
                }
            }
            if ([dictData objectForKey:@"data"] && [dictData objectForKey:@"data"] != nil ) {
                NSDictionary *dictUser = [dictData objectForKey:@"data"];
                [self saveAccountWithDict:dictUser];
                [Utils setBOOL:false Key:kIsUpdatedLanguage];
            }
            
        }else if ([request.Tag isEqual:kGetUserDetails] || [request.Tag isEqual:kUploadUserAvatar] || [request.Tag isEqual:kUpdateUserDetails] || [request.Tag isEqual:kUploadUserCoverPic]) {
            NSDictionary *dictData = request.serverData;
            if ([[dictData objectForKey:@"is_error"] integerValue] > 0) {
                if (_delegate && [_delegate respondsToSelector:@selector(accountDidFailAuthentication:)]){
                    [_delegate accountDidFailAuthentication:[dictData objectForKey:@"message"]];
                    return;
                }
            }
            if ([dictData objectForKey:@"data"] && [dictData objectForKey:@"data"] != nil ) {
                NSDictionary *dictUser = [dictData objectForKey:@"data"];
                [self saveAccountWithDict:dictUser];
            }
        }
    }
    if ([request.Tag isEqualToString:kResetPassword]) {
        NSDictionary *dictData = request.serverData;
        if ([[dictData objectForKey:@"is_error"] integerValue]) {
            if (_delegate && [_delegate respondsToSelector:@selector(accountDidFailAuthentication:)]){
                [_delegate accountDidFailAuthentication:[dictData objectForKey:@"message"]];
                return;
            }
        }
    }
    if (_delegate && [_delegate respondsToSelector:@selector(accountAuthenticatedWithAccount:)]){
        [_delegate accountAuthenticatedWithAccount:self];
    }
}

- (void)RequestDidFailForRequest:(Request *)request withError:(NSError *)error{
//    [_delegate accountDidFailAuthentication:error.localizedDescription];
    [_delegate accountDidFailAuthentication:[error.userInfo objectForKey:@"message"]];
}

@end



#import <Foundation/Foundation.h>

@class Account;

@protocol AccountAuthenticateDelegate <NSObject>

-(void)accountAuthenticatedWithAccount:(Account*) account;
-(void)accountDidFailAuthentication:(NSString*) error;

@end
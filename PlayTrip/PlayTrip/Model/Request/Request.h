
#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
#import "RequestDelegate.h"
#import "Utils.h"

typedef enum {
    GET = 1,
    POST = 2,
    PUT = 3,
    DELETE = 4,
    PATCH = 5,
    MULTI_PART_FORM = 6,
}RequestMethod;

@class AFRequest;

typedef void (^ResponseBlock)(BOOL success, AFRequest *request);


@interface Request : NSObject {
    id<RequestDelegate> _delegate;
    NSString *_urlPart;
    
    NSMutableDictionary *_postParameters;
    
    RequestMethod _methodType;
    
    NSString *_postData;
    NSString *authenticationHeader;
    
    id _serverData;
    
    NSString *_rawResponse;
    
    BOOL _IsSuccess;
    
    NSInteger _StatusCode;
    
    NSString *_Tag;
    
    NSError *_error;
    NSMutableArray *keyValueArray;
}

@property (nonatomic, retain, readwrite) NSString *postData;

@property (nonatomic, retain, readwrite) id serverData;

@property (nonatomic, retain, readwrite) NSString *rawResponse;

@property (nonatomic, assign) NSInteger StatusCode;

@property (nonatomic, assign) BOOL IsSuccess;

@property (nonatomic, retain, readwrite) NSString *Tag;

@property (nonatomic, retain, readwrite) NSError *error;

@property (nonatomic, assign) RequestMethod methodType;
@property (readwrite, copy) ResponseBlock responseBlock;


+ (instancetype)requestWithURLPart:(NSString *)url_part method:(RequestMethod)method dict:(NSDictionary *)dict block:(ResponseBlock)block;

- (id) initWithUrl:(NSString*) urlString andDelegate:(id<RequestDelegate>) del;

- (id) initWithUrl:(NSString*) urlString andDelegate:(id<RequestDelegate>) del andMethod:(RequestMethod) method;

- (void)setParameter:(id)parameter forKey:(NSString *)key;

- (void) setPostParameters:(NSMutableDictionary*)dict;

- (void)startRequest;

@end

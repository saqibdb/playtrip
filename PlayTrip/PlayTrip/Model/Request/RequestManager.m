
#import "RequestManager.h"
#import "URLSchema.h"
@implementation RequestManager

+ (id) Instance {
    static RequestManager *__sharedDataModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedDataModel = [[RequestManager alloc] init];
    });
    return __sharedDataModel;
}


- (AFHTTPSessionManager*) requestManager {
        if (_manager == nil) {
        NSURL *url = [NSURL URLWithString:baseURL]; // Arun :: set the base url here
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        _manager = [[AFHTTPSessionManager alloc] initWithBaseURL:url sessionConfiguration:configuration];
        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"multipart/form-data",@"application/x-www-form-urlencoded",@"application/javascript",@"application/json", nil];
       // [_manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
       // [_manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        _manager.requestSerializer = [AFJSONRequestSerializer serializer];
        _manager.responseSerializer = [AFJSONResponseSerializer serializer];
        _manager.securityPolicy.allowInvalidCertificates = NO;
        }
    return _manager;
}
//
//- (AFHTTPSessionManager*) requestManagerttttttttt {
//
//        NSURL *url = [NSURL URLWithString:@"http://192.168.1.27:9000"]; // Arun :: set the base url here
//        
//        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
//        _manager = [[AFHTTPSessionManager alloc] initWithBaseURL:url sessionConfiguration:configuration];
//        _manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"multipart/form-data",@"application/x-www-form-urlencoded",@"application/javascript",@"application/json", nil];
//
//        _manager.requestSerializer = [AFJSONRequestSerializer serializer];
//        _manager.responseSerializer = [AFJSONResponseSerializer serializer];
//        _manager.securityPolicy.allowInvalidCertificates = NO;
//
//    return _manager;
//}

@end


#import <Foundation/Foundation.h>

//static NSString *baseURL =                      @"http://192.168.1.148:9000"; // local
//static NSString *baseURL =                      @"http://162.250.121.171:3006"; // live

//static NSString *baseURL =                      @"http://54.254.206.27:9001"; // New
static NSString *baseURL =                      @"http://54.254.206.27:9000"; // New Working
//static NSString *baseURL =                      @"http://54.255.183.117:9000"; // New Special



//Production server: 54.255.183.117
//Testing server: 54.254.206.27
//http://162.250.121.171:3006
//http://54.254.206.27

// Account Urls
static NSString *kResetPassword =               @"/api/users/forgotpassword";
static NSString *kLoginUrl    =                 @"/auth/local";
static NSString *kRegisterUrl =                 @"/api/users/register/local";
static NSString *kGetUserDetails =              @"/api/users";
static NSString *kTermsAndConditions =          @"/api/query/Terms/";
static NSString *kUpdateUserLanguage =          @"updateLangugage";
static NSString *kUpdateUserDetails =           @"/api/users/updateprofile/";
static NSString *kUploadUserAvatar =            @"/api/users/upload/avatar/";
static NSString *kUploadUserCoverPic =          @"/api/users/upload/coverpic/";
static NSString *kGetImage =                    @"/api/image/get/";
static NSString *kGetVideo =                    @"/get_video/";

static NSString *kVideoTour =                   @"/api/query/VideoTour";
static NSString *kCreatePlan =                  @"/api/query/execute/putpost/Plan";
static NSString *kCreateAttraction=               @"api/query/execute/putpost/Attraction";
static NSString *kCreateAttrData=               @"api/query/execute/putpost/AttractionData";
static NSString *kPlanBYUserID =                @"/api/query/Plan/getbyuser/%@";
static NSString *kPlanBYPlanID =                @"/api/query/Plan/%@";
static NSString *kDashboardUser =               @"/api/users/recommended/data";
static NSString *kDashboardVideoTour =          @"/api/query/execute/conditions/Plan";
static NSString *kGetAllAttractions  =          @"/api/attractions/getall";
static NSString *kDeleteTour         =          @"/api/query/soft/Plan";
static NSString *kAddAttractionToMyPlan =       @"/api/attractions/create/attraction_to_plan";
static NSString *kGetDistricts =                @"/api/district/get/withcount/";
static NSString *kGetReports   =                @"/api/query/Reasons/";
static NSString *kAddSpamToPlan      =          @"/api/query/Spams/";
static NSString *kSearchDistrictString =        @"/api/tags/get/search_data/";
static NSString *kUploadVideo          =        @"/api/video/upload/video";
static NSString *kCountsCommon          =       @"/api/counts/addedit/";
static NSString *kGetAllCategory =              @"/api/query/Category";
static NSString *kTagsAddEdit    =              @"/api/tags/addoredit";
static NSString *kTagsBookmarkDetails    =      @"/api/counts/bookmarks/getall";
static NSString *kSaveSearchString       =      @"/api/searches/manage_count";
static NSString *kMostPopularSearch      =      @"/api/query/execute/conditions/SearchKeyword";

static NSString *kSearchVideoTour        =      @"/api/searches/filter/videotour";
static NSString *kSearchAttraction       =      @"/api/searches/filter/attractions";

static NSString *kMostpopularAttractionData =   @"/api/query/execute/conditions/Attraction";

static NSString *kDeleteAttraction          =   @"/api/videotour/delete/attraction";
static NSString *kDeleteDay                 =   @"/api/videotour/delete/tour/day";


static NSString *kUploadCoverPhoto          =   @"/api/videotour/update/coverphoto";
static NSString *kUpdateMultipleAttrData    =   @"/api/attractions/update/multiple/data";

static NSString *kCreateVideoTourAllDetails =   @"/api/attractions/create/videotour/with_all_details";
static NSString *kCreateVideoTour           =   @"/api/videotour/addtour/";
static NSString *kGetAllAttrData            =   @"/api/query/AttractionData";

//Arun:: not using it, only for request.tag
static NSString *kGetCmsData  =                 @"kGetCmsData";
static NSString *kMostPopularPlan    =          @"kMostPopularPlan";
static NSString *kMostPopularVideoTour =        @"kMostPopularVideoTour";
static NSString *kMostPopularSearchAttraction = @"kMostPopularSearchAttraction";
static NSString *kEditPlan                    = @"kEditPlan";
//static NSString *kMostPopularAttractionData = @"kMostPopularAttractionData";


static NSString *kGetAllCountries            =   @"/api/query/Country";
static NSString *kGetAllDistricts            =   @"/api/query/execute/conditions/District";
static NSString *kGetAllCities             =   @"/api/query/execute/conditions/City";
static NSString *kGetAttraction            =   @"/api/query/Attraction";

static NSString *kGetMostPopular =   @"/api/videotour/mostpopular";

static NSString *kGetMostPopularAttraction =   @"/api/videotour/mostpopular/by/attraction";

static NSString *kGetMostPopularUsers =   @"/api/users/mostrecommended";

static NSString *kGetUserPlans =   @"/api/query/Plan/getbyuser/";


static NSString *kGetAllCountriesNew            =   @"/api/videotour/bylocation";
static NSString *kSearchAttractionNew           =   @"/api/query/execute/conditions/Attraction";
static NSString *kGetAllAttractionsWithData     =   @"/api/videotour/getallvideos";

static NSString *kGetMergedVideo                =   @"/uploads/";



static NSString *kGetMostPopularAttractionVideo =   @"/api/attractions/mostpopular";



@interface URLSchema : NSObject

@end

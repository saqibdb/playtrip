

#import "Request.h"
#import "RequestManager.h"
#import "AccountManager.h"
#import "URLSchema.h"

@implementation Request

@synthesize postData = _postData;
@synthesize serverData = _serverData;
@synthesize rawResponse = _rawResponse;
@synthesize StatusCode = _StatusCode;
@synthesize IsSuccess = _IsSuccess;
@synthesize Tag = _Tag;
@synthesize error = _error;
@synthesize methodType = _methodType;

+ (instancetype)requestWithURLPart:(NSString *)url_part method:(RequestMethod)method dict:(NSDictionary *)dict block:(ResponseBlock)block {
    Request *request = [Request new];
    
    request->_methodType = method;
    request.responseBlock = block;
    request->_urlPart = url_part;
    if (dict) {
        request->_postParameters = [dict mutableCopy];
    } else {
        request->_postParameters = [[NSMutableDictionary alloc] init];
    }
    [request startRequest];
    return request;
}

- (id)initWithUrl:(NSString *)urlString andDelegate:(id<RequestDelegate>)del {
    return [self initWithUrl:urlString andDelegate:del andMethod:GET];
}

- (id)initWithUrl:(NSString *)urlString andDelegate:(id<RequestDelegate>)del andMethod:(RequestMethod)method {
    self = [super init];
    
    if (self) {
        _urlPart = urlString;
        _methodType = method;
        _postParameters = [[NSMutableDictionary alloc] init];
        _delegate = del;
    }
    return self;
}

- (void)setParameter:(id)parameter forKey:(NSString *)key {
    [_postParameters setObject:parameter forKey:key];
}

- (void) setPostParameters:(NSMutableDictionary*)dict {
    _postParameters = dict;
}

- (void)startRequest{
   // NSLog(@"%@", [Utils getValueForKey:kLoginAutheticationHeader]);
    [[[RequestManager Instance] requestManager].requestSerializer setValue:[Utils getValueForKey:kLoginAutheticationHeader] forHTTPHeaderField:@"Authorization"];
    NSLog(@"Auth is %@", [Utils getValueForKey:kLoginAutheticationHeader]);
    if (_methodType == GET) {
        [[[RequestManager Instance] requestManager] GET:_urlPart parameters:_postParameters success:^void(NSURLSessionDataTask * task, id responseObject) {
            _serverData = responseObject;
            NSHTTPURLResponse* r = (NSHTTPURLResponse*)task.response;
            self.StatusCode = r.statusCode;
            if ([[responseObject objectForKey:@"is_error"]intValue] != 1) {
                self.IsSuccess = YES;
                [self requestSuccess];
            }else{
                self.IsSuccess = NO;
                NSError *error = [NSError errorWithDomain:@"Application" code:100 userInfo:@{ @"message":[responseObject objectForKey:@"message"] }];
                [self requestFailedWithError:error];
            }
        } failure:^void(NSURLSessionDataTask * task, NSError *error) {
            NSHTTPURLResponse* r = (NSHTTPURLResponse*)task.response;
            self.StatusCode = r.statusCode;
            self.IsSuccess = NO;
            self.serverData = nil;
            [self requestFailedWithError:error];
        }];
    } else if (_methodType == POST) {
        [[[RequestManager Instance] requestManager] POST:_urlPart parameters:_postParameters success:^void(NSURLSessionDataTask * task, id responseObject){
            _serverData = responseObject;
            NSHTTPURLResponse* r = (NSHTTPURLResponse*)task.response;
            self.StatusCode = r.statusCode;
            if ([[responseObject objectForKey:@"is_error"]intValue] != 1) {
                self.IsSuccess = YES;
                [self requestSuccess];
            }else{
                self.IsSuccess = NO;
                NSError *error = [NSError errorWithDomain:@"Application" code:100 userInfo:@{ @"message":[responseObject objectForKey:@"message"] }];
                [self requestFailedWithError:error];
            }
        } failure:^void(NSURLSessionDataTask * task, NSError * error) {
            NSHTTPURLResponse* r = (NSHTTPURLResponse*)task.response;
            self.StatusCode = r.statusCode;
            self.IsSuccess = NO;
            self.serverData = nil;
            [self requestFailedWithError:error];
        }];
    } else if (_methodType == PUT) {
        [[[RequestManager Instance] requestManager] PUT:_urlPart parameters:_postParameters success:^void(NSURLSessionDataTask * task, id responseObject) {
            _serverData = responseObject;
            NSHTTPURLResponse* r = (NSHTTPURLResponse*)task.response;
            self.StatusCode = r.statusCode;
            self.IsSuccess = YES;
            [self requestSuccess];
        } failure:^void(NSURLSessionDataTask * task, NSError * error) {
            NSHTTPURLResponse* r = (NSHTTPURLResponse*)task.response;
            self.StatusCode = r.statusCode;
            self.IsSuccess = NO;
            self.serverData = nil;
            [self requestFailedWithError:error];
        }];
    } else if (_methodType == DELETE) {
        [[[RequestManager Instance] requestManager] DELETE:_urlPart parameters:_postParameters success:^void(NSURLSessionDataTask * task, id responseObject) {
            _serverData = responseObject;
            NSHTTPURLResponse* r = (NSHTTPURLResponse*)task.response;
            self.StatusCode = r.statusCode;
            self.IsSuccess = YES;
            [self requestSuccess];
        } failure:^void(NSURLSessionDataTask * task, NSError * error) {
            NSHTTPURLResponse* r = (NSHTTPURLResponse*)task.response;
            self.StatusCode = r.statusCode;
            self.IsSuccess = NO;
            self.serverData = nil;
            [self requestFailedWithError:error];
        }];
    }  else if (_methodType == MULTI_PART_FORM){
        
        [[[RequestManager Instance] requestManager] POST:_urlPart parameters:_postParameters constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
            NSString * mimeTypeStr = [Utils mimeTypeForData:[_postParameters objectForKey:@"file"]];
            if ([self.Tag isEqualToString:kUploadUserAvatar] || [self.Tag isEqualToString:kUploadUserCoverPic]) {
                [formData appendPartWithFileData:[_postParameters objectForKey:@"file"] name:@"file" fileName:@"file" mimeType:mimeTypeStr];
            } else if ([self.Tag isEqualToString:kUploadVideo]) {
//                [formData appendPartWithFileData:[_postParameters objectForKey:@"file"] name:@"file" fileName:@"file" mimeType:@"video/mp4"];
                NSError *error;
                [formData appendPartWithFileURL:[_postParameters objectForKey:@"url"] name:@"file" fileName:[_postParameters objectForKey:@"path"] mimeType:@"video/mp4" error:&error];
            }
        } success:^(NSURLSessionDataTask * task, id responseObject) {
            _serverData = responseObject;
            NSHTTPURLResponse* r = (NSHTTPURLResponse*)task.response;
            self.StatusCode = r.statusCode;
            self.IsSuccess = YES;
            [self requestSuccess];
        } failure:^(NSURLSessionDataTask * task, NSError * error) {
            NSHTTPURLResponse* r = (NSHTTPURLResponse*)task.response;
            self.StatusCode = r.statusCode;
            self.IsSuccess = NO;
            self.serverData = nil;
            [self requestFailedWithError:error];
        }];
    }
}

- (void) requestSuccess {
    [_delegate RequestDidSuccess:self];
}

- (void) requestFailedWithError:(NSError*)error {
    self.error = error;
    
    if (_delegate && [_delegate respondsToSelector:@selector(RequestDidFailForRequest:withError:)]) {
        if ([error.userInfo objectForKey:@"message"] && [error.userInfo objectForKey:@"message"] != nil) {
            [_delegate RequestDidFailForRequest:self withError:error];
        } else {
            [_delegate RequestDidFailForRequest:self withError:error];
        }
        
    }
}

@end

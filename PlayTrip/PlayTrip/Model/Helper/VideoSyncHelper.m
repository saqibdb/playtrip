
#import "VideoSyncHelper.h"
#import "AppDelegate.h"
#import "LocalVideoObject.h"
#import "PlayTripManager.h"

@implementation VideoSyncHelper

+ (VideoSyncHelper*) Instance {
    static VideoSyncHelper *__sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[VideoSyncHelper alloc] init];
    });
    return __sharedInstance;
}

- (instancetype)init {
    return self;
}

- (void) startSync {
    if ([[AppDelegate appDelegate] isReachable]) {
        mainArray = [NSMutableArray new];
        mainArray = [LocalVideoObject getToBeUploadedVids];
        selectedArray = [LocalVideoObject getToBeUploadedVids];
        
        if (mainArray.count > 0) {
            [self uploadForIndex:0];
        }
    }
}

-(void) uploadForIndex:(NSInteger)index{
    LocalVideoObject * vidObj = [mainArray objectAtIndex:index];
    vidObj.isUploading = @"Yes";
    [LocalVideoObject saveEntity];
    NSURL *movieUrl = [NSURL fileURLWithPath:vidObj.vidPath];
    [[PlayTripManager Instance] uploadVideoWithVideoUrl:movieUrl WithAttrId:vidObj.attrDataId WithBlock:^(id result, NSString *error) {
        [LocalVideoObject deleteObj:vidObj];
        [mainArray removeObjectAtIndex:index];
        if (mainArray.count > 0) {
            [self uploadForIndex:0];
        }
    }];
}

@end

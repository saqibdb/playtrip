
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "LocalVideoObject.h"

@interface VideoSyncHelper : NSObject {
    NSMutableArray * mainArray;
    NSMutableArray * selectedArray;
}

+ (VideoSyncHelper*) Instance;

- (void) startSync;

@end

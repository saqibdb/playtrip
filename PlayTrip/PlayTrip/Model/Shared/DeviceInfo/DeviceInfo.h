
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define iPhone1G         @"iPhone 1G"
#define iPhone3G         @"iPhone 3G"
#define iPhone3GS        @"iPhone 3GS"
#define VerizoniPhone4   @"Verizon iPhone 4"
#define iPhone4          @"iPhone 4"
#define iPhone4CDMA      @"iPhone 4 CDMA"
#define iPhone4S         @"iPhone 4S"
#define iPhone5          @"iPhone 5"
#define iPhone5C         @"iPhone 5c"
#define iPhone5S         @"iPhone 5s"
#define iPhone6          @"iPhone 6"
#define iPhone6Plus      @"iPhone 6 Plus"
#define iPhone6SPlus     @"iPhone 6S Plus"
#define iPhone6S         @"iPhone 6S"
#define iPhone7          @"iPhone 7"
#define iPhone7Plus      @"iPhone 7 Plus"
#define iPodTouch1G      @"iPod Touch 1G"
#define iPodTouch2G      @"iPod Touch 2G"
#define iPodTouch3G      @"iPod Touch 3G"
#define iPodTouch4G      @"iPod Touch 4G"
#define iPodTouch5G      @"iPod Touch 5G"

#define iPad             @"iPad"
#define iPadMini         @"iPad Mini"
#define iPad2            @"iPhone 2"
#define iPad3            @"iPhone 3"
#define iPad4            @"iPhone 4"
#define iPadAir          @"iPhone Air"

#define iPhone3Series    @"iPhone 3 series"
#define iPhone4Series    @"iPhone 4 series"
#define iPhone5Series    @"iPhone 5 series"
#define iPhone6Series    @"iPhone 6 series"
#define iPhone7Series    @"iPhone 7 series"

#define iPodSeries       @"iPod Series"
#define iPadSeries       @"iPad Series"

#define iPad2Series      @"iPad 2 Series"
#define iPad3Series      @"iPad 3 Series"
#define iPad4Series      @"iPad 4 Series"
#define iPadMiniSeries   @"iPad Mini Series"
#define iPadAirSeries    @"iPad Air Series"

@interface DeviceInfo : NSString

+ (NSString *) getModelName;
+ (NSString *) getModelDescription;
+ (NSString *) getSystemVersion;
+ (NSString *) getSystemName;
+ (NSString *) getBatteryLevel;
+ (NSString *) getSimulatorName;
+ (NSString *) getSeriesName;

@end

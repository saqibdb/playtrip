
#import "DeviceInfo.h"

#include <sys/sysctl.h>
#include <sys/utsname.h>

@implementation DeviceInfo

+ (NSString *)getModelName{
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    else if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    else if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    else if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    else if ([platform isEqualToString:@"iPhone3,2"])    return @"iPhone 4 CDMA";
    else if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    else if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    else if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5"; //(GSM)
    else if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5"; // (GSM+CDMA)
    else if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c"; // (GSM)
    else if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c"; // (GSM+CDMA)
    else if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s"; // (GSM)
    else if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s"; // (GSM+CDMA)
    else if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    else if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    else if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone 6S";
    else if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone 6S Plus";
    else if ([platform isEqualToString:@"iPhone9,1"])    return @"iPhone 7";
    else if ([platform isEqualToString:@"iPhone9,2"])    return @"iPhone 7 Plus";
    else if ([platform isEqualToString:@"iPhone9,3"])    return @"iPhone 7 Plus";
    else if ([platform isEqualToString:@"iPhone9,4"])    return @"iPhone 7 Plus";
    else if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    else if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    else if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    else if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    else if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    else if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    else if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2"; //  (WiFi)
    else if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2"; //  (Cellular)
    else if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2"; //  (Cellular)
    else if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2"; //  (WiFi)
    else if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini"; //  (WiFi)
    else if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini"; //  (Cellular)
    else if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini"; //  (Cellular)
    else if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3"; //  (WiFi)
    else if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3"; //  (Cellular)
    else if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3"; //  (Cellular)
    else if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4"; //  (WiFi)
    else if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4"; //  (Cellular)
    else if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4"; //  (Cellular)
    else if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air"; // (WiFi)
    else if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air"; //  (Cellular)
    else if ([platform isEqualToString:@"i386"])         return @"Simulator";
    else if ([platform isEqualToString:@"x86_64"])       return @"Simulator";
    
    return @"Unknown";
    
}

+ (NSString *)getModelDescription{
    NSString* desc = [NSString stringWithFormat:@"%@",[UIDevice currentDevice].description];
    return desc;
}

+ (NSString *)getSystemVersion{
    NSString* vers = [NSString stringWithFormat:@"%@",[UIDevice currentDevice].systemVersion];
    return vers;
}

+ (NSString *)getSystemName{
    NSString* vers = [NSString stringWithFormat:@"%@",[UIDevice currentDevice].systemName];
    return vers;
}

+ (NSString *)getBatteryLevel{
    NSString* vers = [NSString stringWithFormat:@"%f",[UIDevice currentDevice].batteryLevel];
    return vers;
}

+ (NSString *) getSimulatorName {
    
    NSString *platform  = [NSString stringWithFormat:@"%s",getenv("SIMULATOR_MODEL_IDENTIFIER")];
    
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    else if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    else if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    else if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    else if ([platform isEqualToString:@"iPhone3,2"])    return @"iPhone 4 CDMA";
    else if ([platform isEqualToString:@"iPhone3,3"])    return @"Verizon iPhone 4";
    else if ([platform isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    else if ([platform isEqualToString:@"iPhone5,1"])    return @"iPhone 5"; //(GSM)
    else if ([platform isEqualToString:@"iPhone5,2"])    return @"iPhone 5"; // (GSM+CDMA)
    else if ([platform isEqualToString:@"iPhone5,3"])    return @"iPhone 5c"; // (GSM)
    else if ([platform isEqualToString:@"iPhone5,4"])    return @"iPhone 5c"; // (GSM+CDMA)
    else if ([platform isEqualToString:@"iPhone6,1"])    return @"iPhone 5s"; // (GSM)
    else if ([platform isEqualToString:@"iPhone6,2"])    return @"iPhone 5s"; // (GSM+CDMA)
    else if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    else if ([platform isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    else if ([platform isEqualToString:@"iPhone8,1"])    return @"iPhone 6S";
    else if ([platform isEqualToString:@"iPhone8,2"])    return @"iPhone 6S Plus";
    else if ([platform isEqualToString:@"iPhone9,1"])    return @"iPhone 7";
    else if ([platform isEqualToString:@"iPhone9,2"])    return @"iPhone 7 Plus";
    else if ([platform isEqualToString:@"iPhone9,3"])    return @"iPhone 7 Plus";
    else if ([platform isEqualToString:@"iPhone9,4"])    return @"iPhone 7 Plus";
    else if ([platform isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    else if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    else if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    else if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    else if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    else if ([platform isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    else if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    else if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2"; //  (WiFi)
    else if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2"; //  (Cellular)
    else if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2"; //  (Cellular)
    else if ([platform isEqualToString:@"iPad2,4"])      return @"iPad 2"; //  (WiFi)
    else if ([platform isEqualToString:@"iPad2,5"])      return @"iPad Mini"; //  (WiFi)
    else if ([platform isEqualToString:@"iPad2,6"])      return @"iPad Mini"; //  (Cellular)
    else if ([platform isEqualToString:@"iPad2,7"])      return @"iPad Mini"; //  (Cellular)
    else if ([platform isEqualToString:@"iPad3,1"])      return @"iPad 3"; //  (WiFi)
    else if ([platform isEqualToString:@"iPad3,2"])      return @"iPad 3"; //  (Cellular)
    else if ([platform isEqualToString:@"iPad3,3"])      return @"iPad 3"; //  (Cellular)
    else if ([platform isEqualToString:@"iPad3,4"])      return @"iPad 4"; //  (WiFi)
    else if ([platform isEqualToString:@"iPad3,5"])      return @"iPad 4"; //  (Cellular)
    else if ([platform isEqualToString:@"iPad3,6"])      return @"iPad 4"; //  (Cellular)
    else if ([platform isEqualToString:@"iPad4,1"])      return @"iPad Air"; // (WiFi)
    else if ([platform isEqualToString:@"iPad4,2"])      return @"iPad Air"; //  (Cellular)
    
    return @"Unknown";
}

+ (NSString *) getSeriesName {
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    NSString * simName = [NSString stringWithFormat:@"%s",getenv("SIMULATOR_MODEL_IDENTIFIER")];
    
    if ([platform isEqualToString:@"iPhone1,1"] || [simName isEqualToString:@"iPhone1,1"]) {
        return @"iPhone 1G";
    }
    
    else if ([platform isEqualToString:@"iPhone1,2"] || [platform isEqualToString:@"iPhone2,1"] || [simName isEqualToString:@"iPhone1,2"] || [simName isEqualToString:@"iPhone2,1"]) {
        return @"iPhone 3 series";
    }
    
    else if ([platform isEqualToString:@"iPhone3,1"] || [platform isEqualToString:@"iPhone3,2"] || [platform isEqualToString:@"iPhone3,3"] || [platform isEqualToString:@"iPhone4,1"]|| [simName isEqualToString:@"iPhone3,1"] || [simName isEqualToString:@"iPhone3,2"] || [simName isEqualToString:@"iPhone3,3"] || [simName isEqualToString:@"iPhone4,1"]) {
       return @"iPhone 4 series";
    }
    
    else if ([platform isEqualToString:@"iPhone5,1"] || [platform isEqualToString:@"iPhone5,2"] || [platform isEqualToString:@"iPhone5,3"] || [platform isEqualToString:@"iPhone5,4"] || [platform isEqualToString:@"iPhone6,1"] || [platform isEqualToString:@"iPhone6,2"] || [simName isEqualToString:@"iPhone5,1"] || [simName isEqualToString:@"iPhone5,2"] || [simName isEqualToString:@"iPhone5,3"] || [simName isEqualToString:@"iPhone5,4"] || [simName isEqualToString:@"iPhone6,1"] || [simName isEqualToString:@"iPhone6,2"]) {
            return @"iPhone 5 series";
    }

    else if ([platform isEqualToString:@"iPhone7,2"] || [platform isEqualToString:@"iPhone7,1"] || [platform isEqualToString:@"iPhone8,2"] || [platform isEqualToString:@"iPhone8,1"] || [simName isEqualToString:@"iPhone7,2"] || [simName isEqualToString:@"iPhone7,1"] || [simName isEqualToString:@"iPhone8,2"] || [simName isEqualToString:@"iPhone8,1"]) {
        return @"iPhone 6 series";
    }
    
    else if ([platform isEqualToString:@"iPhone9,2"] || [platform isEqualToString:@"iPhone9,1"] || [platform isEqualToString:@"iPhone9,4"] || [platform isEqualToString:@"iPhone9,3"] || [simName isEqualToString:@"iPhone9,2"] || [simName isEqualToString:@"iPhone9,1"] || [simName isEqualToString:@"iPhone9,3"] || [simName isEqualToString:@"iPhone9,4"]) {
        return @"iPhone 7 series";
    }
    
    else if ([platform isEqualToString:@"iPod1,1"] || [platform isEqualToString:@"iPod5,1"] || [platform isEqualToString:@"iPod4,1"] || [platform isEqualToString:@"iPod3,1"] || [platform isEqualToString:@"iPod2,1"] || [simName isEqualToString:@"iPod1,1"] || [simName isEqualToString:@"iPod5,1"] || [simName isEqualToString:@"iPod4,1"] || [simName isEqualToString:@"iPod3,1"] || [simName isEqualToString:@"iPod2,1"]) {
        return @"iPod series";
    }
    
    else if ([platform isEqualToString:@"iPad1,1"] || [simName isEqualToString:@"iPad1,1"]) {
         return @"iPad Series";
    }
    
    else if ([platform isEqualToString:@"iPad2,1"] || [platform isEqualToString:@"iPad2,2"] || [platform isEqualToString:@"iPad2,3"] || [platform isEqualToString:@"iPad2,4"] || [simName isEqualToString:@"iPad2,1"] || [simName isEqualToString:@"iPad2,2"] || [simName isEqualToString:@"iPad2,3"] || [simName isEqualToString:@"iPad2,4"]) {
        return @"iPad 2 series";
    }
    
    else if ([platform isEqualToString:@"iPad2,5"] || [platform isEqualToString:@"iPad2,6"] || [platform isEqualToString:@"iPad2,7"] || [simName isEqualToString:@"iPad2,5"] || [simName isEqualToString:@"iPad2,6"] || [simName isEqualToString:@"iPad2,7"]) {
        return @"iPad Mini series";
    }
    
    else if ([platform isEqualToString:@"iPad3,1"] || [platform isEqualToString:@"iPad3,2"] || [platform isEqualToString:@"iPad3,3"] || [simName isEqualToString:@"iPad3,1"] || [simName isEqualToString:@"iPad3,2"] || [simName isEqualToString:@"iPad3,3"]) {
             return @"iPad 3 series";
    }
    
    else if ([platform isEqualToString:@"iPad3,4"] || [platform isEqualToString:@"iPad3,5"] || [platform isEqualToString:@"iPad3,6"] || [simName isEqualToString:@"iPad3,4"] || [simName isEqualToString:@"iPad3,5"] || [simName isEqualToString:@"iPad3,6"]) {
          return @"iPad 4 series";
    }
    
    else if ([platform isEqualToString:@"iPad4,1"] || [platform isEqualToString:@"iPad4,2"] || [simName isEqualToString:@"iPad4,1"] || [simName isEqualToString:@"iPad4,2"]) {
        return @"iPad Air Series";
    }
    
    return @"Unknown";
}

@end

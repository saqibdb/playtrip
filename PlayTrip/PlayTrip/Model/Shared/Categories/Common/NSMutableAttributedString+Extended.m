//
//  NSMutableAttributedString+Extended.m
//  CourseKart
//
//  Created by Samir on 5/2/16.
//  Copyright © 2016 Samir. All rights reserved.
//

#import "NSMutableAttributedString+Extended.h"
#import <CoreText/CoreText.h>
@implementation NSMutableAttributedString (Extended)
- (void) setTextColor:(UIColor*)color range:(NSRange)range {
    // kCTForegroundColorAttributeName
    [self removeAttribute:(NSString*)kCTForegroundColorAttributeName range:range]; // Work around for Apple leak
    [self addAttribute:(NSString*)kCTForegroundColorAttributeName value:(id)color.CGColor range:range];
}
@end

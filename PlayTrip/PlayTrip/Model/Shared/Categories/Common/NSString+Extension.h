//
//  NSString+Extension.h
//  FieldWork
//
//  Created by SamirMAC on 1/22/16.
//
//

#import <UIKit/UIKit.h>

@interface NSString (Extension)

- (NSString *)URLStringByAppendingQueryString:(NSString *)queryString;

@end

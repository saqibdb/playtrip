//
//  NSMutableAttributedString+Extended.h
//  CourseKart
//
//  Created by Samir on 5/2/16.
//  Copyright © 2016 Samir. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface NSMutableAttributedString (Extended)
- (void) setTextColor:(UIColor*)color range:(NSRange)range;
@end


#import <CoreData/CoreData.h>

@interface NSManagedObject (Duplicate)

- (instancetype)duplicateAssociated;
- (instancetype)duplicateUnassociated;

@end

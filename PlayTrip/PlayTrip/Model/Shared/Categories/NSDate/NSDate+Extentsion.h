//
//  NSDate+Extentsion.h
//  FWModel
//
//  Created by SamirMAC on 11/12/15.
//  Copyright (c) 2015 SamirMAC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Extentsion)

+ (NSDate *) date2;
- (BOOL) isSameDayAs:(NSDate *)date2;
- (BOOL) isOneDayPrevious:(NSDate *)date2;
+ (NSDate *)getDateFromJSONString:(NSString *)str;
+ (NSDate *) date2With15MinutesInterval;
- (NSString*) getJSONDateString;
- (int)dateCompareValue;
+ (NSDate *)systemDate;
- (NSDate*) changeTimeZoneToLocal;
- (NSString *)stringWithFormat:(NSString *)format;

@end

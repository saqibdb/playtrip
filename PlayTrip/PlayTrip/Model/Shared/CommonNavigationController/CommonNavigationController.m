
#import "CommonNavigationController.h"
#import "ColorConstants.h"

@interface CommonNavigationController ()

@end

@implementation CommonNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)awakeFromNib{
    [super awakeFromNib];
    
        [UINavigationBar appearance].backIndicatorImage = [UIImage imageNamed:@"back.png"];
        [UINavigationBar appearance].backIndicatorTransitionMaskImage = [UIImage imageNamed:@"back.png"];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationBar.translucent = NO;
    self.navigationBar.shadowImage = [UIImage new];
    self.view.backgroundColor = [ColorConstants appYellowColor];
    [self.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    


}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end


#import "CDHelper.h"

@implementation CDHelper

+ (NSDictionary*) mappingForClass:(Class)cls {
    NSDictionary *dictRoot = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Mapping" ofType:@"plist"]];
    NSDictionary *keys = [dictRoot objectForKey:NSStringFromClass(cls)];
    return keys;
}

@end


#import <Foundation/Foundation.h>

@interface CDHelper : NSObject

+ (NSDictionary*) mappingForClass:(Class)cls;

@end

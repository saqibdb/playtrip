
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface ColorConstants : NSObject

+ (nonnull UIColor *)appYellowColor;
+ (nonnull UIColor *)appBrownColor;
+ (nonnull UIColor *)appGrayColor;
+ (nonnull UIColor *)appRedColor;


+ (nonnull UIColor *)categoryYellowColor;
+ (nonnull UIColor *)categoryRedColor;
+ (nonnull UIColor *)categoryGreenColor;
+ (nonnull UIColor *)categoryBlueColor;
@end

//
//  Constant.h
//  Infonet
//
//  Created by samcom on 11/26/16.
//  Copyright © 2016 app. All rights reserved.
//

#ifndef Constant_h
#define Constant_h


typedef void (^ItemLoadedBlock)(id result, NSString *error);
typedef void(^CallBackCompletion)(id object);
#define kALERT_TITLE nil
#define kInternet_Not_Available @"Internet is not available. Please turn on internet"
#define c_ADDED                                     [NSNumber numberWithInt:100]

#define kPayPalEnvironment PayPalEnvironmentNoNetwork

#define kMerchantPrivacyPolicyURL @"https://www.paypal.com/webapps/mpp/ua/privacy-full"

#define kMerchantUserAgreementURL @"https://www.paypal.com/webapps/mpp/ua/useragreement-full"
#endif


#import "Utils.h"
@import Foundation;
@import AWSCore;




#ifndef Constants_h
#define Constants_h
typedef void(^CallBackCompletion)(id object);

typedef void (^ItemLoadedBlock)(id result, NSString *error);



static NSString *ALERT_TITLE                            =        @"PlayTrip";
static NSString *kLoginChanged                          =        @"LoginChanged";
static NSString *kCheckInternet                         =        @"Please check your Internet Connection";
static NSString *kStopShowAlert                         =        @"StopShowAlert";

static NSString *kIsUpdatedLanguage                     =        @"IsUpdatedLanguage";
static NSString *kUpdatedLanguage                       =        @"UpdatedLanguage";

// Language Constants
static NSString *Language_English                       =        @"en";
static NSString *Language_Simple_Chineese               =        @"zh-Hans";
static NSString *Language_Traditional_Chineese          =        @"zh-Hant";

//static NSString *Language_Simple_Chineese               =        @"sc";
//static NSString *Language_Traditional_Chineese          =        @"tc";

//Currency Constants
static NSString *Currency_USD                           =        @"USD";
static NSString *Currency_HKD                           =        @"HKD";
static NSString *Currency_RMB                           =        @"RMB";

// Model Constants
static NSString *ModelPlan                              =        @"Plan";
static NSString *ModelUser                              =        @"User";
static NSString *ModelAttraction                        =        @"Attraction";

// Type Constants
static NSString *TypeShare                              =        @"share";
static NSString *TypeViews                              =        @"views";
static NSString *TypeBookmark                           =        @"bookmark";

static NSString *IsPreviewAdded                         =       @"IsPreviewAdded";


// NSUserDefaults
static NSString *LocationId                             =       @"LocationId";
static NSString *LocationName                           =       @"LocationName";
static NSString *VidSegArray                            =       @"VidSegArray";
static NSString *AudMergedNotCompleted                  =       @"AudMergedNotCompleted";
#endif








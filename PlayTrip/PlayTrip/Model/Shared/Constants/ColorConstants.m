
#import "ColorConstants.h"

#define RGB(r,g,b) \
{ \
static UIColor *rgb; \
if(!rgb) \
rgb = [UIColor colorWithRed: r / 255.0 green: g / 255.0 blue: b / 255.0 alpha: 1.0]; \
return rgb; \
}

#define RGBA(r,g,b,a) \
{ \
static UIColor *rgb; \
if(!rgb) \
rgb = [UIColor colorWithRed: r / 255.0 green: g / 255.0 blue: b / 255.0 alpha: a]; \
return rgb; \
}

@implementation ColorConstants

+ (UIColor *)appYellowColor                     RGB(252, 238, 32) // fcee20
+ (UIColor *)appBrownColor                     RGB(81, 53, 40)
+ (UIColor *)appGrayColor                     RGB(238, 238, 238)
+ (UIColor *)appRedColor                     RGB(255, 0, 32)


+ (UIColor *)categoryYellowColor                     RGB(253, 156, 55)
+ (UIColor *)categoryRedColor                     RGB(229, 72, 59)
+ (UIColor *)categoryGreenColor                     RGB(114, 218, 79)
+ (UIColor *)categoryBlueColor                     RGB(99, 176, 249)



@end

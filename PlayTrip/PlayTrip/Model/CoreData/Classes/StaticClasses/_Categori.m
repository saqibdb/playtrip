// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Categori.m instead.

#import "_Categori.h"

@implementation CategoriID
@end

@implementation _Categori

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Categori" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Categori";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Categori" inManagedObjectContext:moc_];
}

- (CategoriID*)objectID {
	return (CategoriID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"is_publishedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"is_published"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic desc;

@dynamic entity_id;

@dynamic image;

@dynamic is_published;

- (BOOL)is_publishedValue {
	NSNumber *result = [self is_published];
	return [result boolValue];
}

- (void)setIs_publishedValue:(BOOL)value_ {
	[self setIs_published:@(value_)];
}

- (BOOL)primitiveIs_publishedValue {
	NSNumber *result = [self primitiveIs_published];
	return [result boolValue];
}

- (void)setPrimitiveIs_publishedValue:(BOOL)value_ {
	[self setPrimitiveIs_published:@(value_)];
}

@dynamic last_updated;

@dynamic name;

@dynamic info;

@dynamic mostPopularAttractions;

@end

@implementation CategoriAttributes 
+ (NSString *)desc {
	return @"desc";
}
+ (NSString *)entity_id {
	return @"entity_id";
}
+ (NSString *)image {
	return @"image";
}
+ (NSString *)is_published {
	return @"is_published";
}
+ (NSString *)last_updated {
	return @"last_updated";
}
+ (NSString *)name {
	return @"name";
}
@end

@implementation CategoriRelationships 
+ (NSString *)info {
	return @"info";
}
+ (NSString *)mostPopularAttractions {
	return @"mostPopularAttractions";
}
@end


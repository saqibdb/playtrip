// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BookmarkDetail.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class BookmarkAttractions;
@class BookmarkLocation;
@class BookmarkUser;
@class BookmarkVideotour;

@interface BookmarkDetailID : NSManagedObjectID {}
@end

@interface _BookmarkDetail : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) BookmarkDetailID *objectID;

@property (nonatomic, strong, nullable) NSSet<BookmarkAttractions*> *bookmarkAttractions;
- (nullable NSMutableSet<BookmarkAttractions*>*)bookmarkAttractionsSet;

@property (nonatomic, strong, nullable) NSSet<BookmarkLocation*> *bookmarkLocation;
- (nullable NSMutableSet<BookmarkLocation*>*)bookmarkLocationSet;

@property (nonatomic, strong, nullable) NSSet<BookmarkUser*> *bookmarkUser;
- (nullable NSMutableSet<BookmarkUser*>*)bookmarkUserSet;

@property (nonatomic, strong, nullable) NSSet<BookmarkVideotour*> *bookmarkVideotour;
- (nullable NSMutableSet<BookmarkVideotour*>*)bookmarkVideotourSet;

@end

@interface _BookmarkDetail (BookmarkAttractionsCoreDataGeneratedAccessors)
- (void)addBookmarkAttractions:(NSSet<BookmarkAttractions*>*)value_;
- (void)removeBookmarkAttractions:(NSSet<BookmarkAttractions*>*)value_;
- (void)addBookmarkAttractionsObject:(BookmarkAttractions*)value_;
- (void)removeBookmarkAttractionsObject:(BookmarkAttractions*)value_;

@end

@interface _BookmarkDetail (BookmarkLocationCoreDataGeneratedAccessors)
- (void)addBookmarkLocation:(NSSet<BookmarkLocation*>*)value_;
- (void)removeBookmarkLocation:(NSSet<BookmarkLocation*>*)value_;
- (void)addBookmarkLocationObject:(BookmarkLocation*)value_;
- (void)removeBookmarkLocationObject:(BookmarkLocation*)value_;

@end

@interface _BookmarkDetail (BookmarkUserCoreDataGeneratedAccessors)
- (void)addBookmarkUser:(NSSet<BookmarkUser*>*)value_;
- (void)removeBookmarkUser:(NSSet<BookmarkUser*>*)value_;
- (void)addBookmarkUserObject:(BookmarkUser*)value_;
- (void)removeBookmarkUserObject:(BookmarkUser*)value_;

@end

@interface _BookmarkDetail (BookmarkVideotourCoreDataGeneratedAccessors)
- (void)addBookmarkVideotour:(NSSet<BookmarkVideotour*>*)value_;
- (void)removeBookmarkVideotour:(NSSet<BookmarkVideotour*>*)value_;
- (void)addBookmarkVideotourObject:(BookmarkVideotour*)value_;
- (void)removeBookmarkVideotourObject:(BookmarkVideotour*)value_;

@end

@interface _BookmarkDetail (CoreDataGeneratedPrimitiveAccessors)

- (NSMutableSet<BookmarkAttractions*>*)primitiveBookmarkAttractions;
- (void)setPrimitiveBookmarkAttractions:(NSMutableSet<BookmarkAttractions*>*)value;

- (NSMutableSet<BookmarkLocation*>*)primitiveBookmarkLocation;
- (void)setPrimitiveBookmarkLocation:(NSMutableSet<BookmarkLocation*>*)value;

- (NSMutableSet<BookmarkUser*>*)primitiveBookmarkUser;
- (void)setPrimitiveBookmarkUser:(NSMutableSet<BookmarkUser*>*)value;

- (NSMutableSet<BookmarkVideotour*>*)primitiveBookmarkVideotour;
- (void)setPrimitiveBookmarkVideotour:(NSMutableSet<BookmarkVideotour*>*)value;

@end

@interface BookmarkDetailRelationships: NSObject
+ (NSString *)bookmarkAttractions;
+ (NSString *)bookmarkLocation;
+ (NSString *)bookmarkUser;
+ (NSString *)bookmarkVideotour;
@end

NS_ASSUME_NONNULL_END

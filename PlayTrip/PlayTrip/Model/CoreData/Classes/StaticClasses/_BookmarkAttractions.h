// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BookmarkAttractions.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class BookmarkDetail;
@class CommonUser;

@interface BookmarkAttractionsID : NSManagedObjectID {}
@end

@interface _BookmarkAttractions : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) BookmarkAttractionsID *objectID;

@property (nonatomic, strong, nullable) NSString* address;

@property (nonatomic, strong, nullable) NSString* bookmark_users_string;

@property (nonatomic, strong, nullable) NSDate* create_date;

@property (nonatomic, strong, nullable) NSString* detail;

@property (nonatomic, strong, nullable) NSString* entity_id;

@property (nonatomic, strong, nullable) NSString* geotype;

@property (nonatomic, strong, nullable) NSString* hours_info;

@property (nonatomic, strong, nullable) NSString* language;

@property (nonatomic, strong, nullable) NSDate* last_updated;

@property (nonatomic, strong, nullable) NSNumber* loc_lat;

@property (atomic) double loc_latValue;
- (double)loc_latValue;
- (void)setLoc_latValue:(double)value_;

@property (nonatomic, strong, nullable) NSNumber* loc_long;

@property (atomic) double loc_longValue;
- (double)loc_longValue;
- (void)setLoc_longValue:(double)value_;

@property (nonatomic, strong, nullable) NSString* name;

@property (nonatomic, strong, nullable) NSString* phone_no;

@property (nonatomic, strong, nullable) NSString* place_id;

@property (nonatomic, strong, nullable) NSNumber* total_bookmark;

@property (atomic) int32_t total_bookmarkValue;
- (int32_t)total_bookmarkValue;
- (void)setTotal_bookmarkValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSNumber* total_share;

@property (atomic) int32_t total_shareValue;
- (int32_t)total_shareValue;
- (void)setTotal_shareValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSNumber* total_view;

@property (atomic) int32_t total_viewValue;
- (int32_t)total_viewValue;
- (void)setTotal_viewValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSString* video_id;

@property (nonatomic, strong, nullable) NSString* web_url;

@property (nonatomic, strong, nullable) BookmarkDetail *bookmarkAttractions;

@property (nonatomic, strong, nullable) CommonUser *userBookmarkAttraction;

@end

@interface _BookmarkAttractions (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveAddress;
- (void)setPrimitiveAddress:(nullable NSString*)value;

- (nullable NSString*)primitiveBookmark_users_string;
- (void)setPrimitiveBookmark_users_string:(nullable NSString*)value;

- (nullable NSDate*)primitiveCreate_date;
- (void)setPrimitiveCreate_date:(nullable NSDate*)value;

- (nullable NSString*)primitiveDetail;
- (void)setPrimitiveDetail:(nullable NSString*)value;

- (nullable NSString*)primitiveEntity_id;
- (void)setPrimitiveEntity_id:(nullable NSString*)value;

- (nullable NSString*)primitiveGeotype;
- (void)setPrimitiveGeotype:(nullable NSString*)value;

- (nullable NSString*)primitiveHours_info;
- (void)setPrimitiveHours_info:(nullable NSString*)value;

- (nullable NSString*)primitiveLanguage;
- (void)setPrimitiveLanguage:(nullable NSString*)value;

- (nullable NSDate*)primitiveLast_updated;
- (void)setPrimitiveLast_updated:(nullable NSDate*)value;

- (nullable NSNumber*)primitiveLoc_lat;
- (void)setPrimitiveLoc_lat:(nullable NSNumber*)value;

- (double)primitiveLoc_latValue;
- (void)setPrimitiveLoc_latValue:(double)value_;

- (nullable NSNumber*)primitiveLoc_long;
- (void)setPrimitiveLoc_long:(nullable NSNumber*)value;

- (double)primitiveLoc_longValue;
- (void)setPrimitiveLoc_longValue:(double)value_;

- (nullable NSString*)primitiveName;
- (void)setPrimitiveName:(nullable NSString*)value;

- (nullable NSString*)primitivePhone_no;
- (void)setPrimitivePhone_no:(nullable NSString*)value;

- (nullable NSString*)primitivePlace_id;
- (void)setPrimitivePlace_id:(nullable NSString*)value;

- (nullable NSNumber*)primitiveTotal_bookmark;
- (void)setPrimitiveTotal_bookmark:(nullable NSNumber*)value;

- (int32_t)primitiveTotal_bookmarkValue;
- (void)setPrimitiveTotal_bookmarkValue:(int32_t)value_;

- (nullable NSNumber*)primitiveTotal_share;
- (void)setPrimitiveTotal_share:(nullable NSNumber*)value;

- (int32_t)primitiveTotal_shareValue;
- (void)setPrimitiveTotal_shareValue:(int32_t)value_;

- (nullable NSNumber*)primitiveTotal_view;
- (void)setPrimitiveTotal_view:(nullable NSNumber*)value;

- (int32_t)primitiveTotal_viewValue;
- (void)setPrimitiveTotal_viewValue:(int32_t)value_;

- (nullable NSString*)primitiveVideo_id;
- (void)setPrimitiveVideo_id:(nullable NSString*)value;

- (nullable NSString*)primitiveWeb_url;
- (void)setPrimitiveWeb_url:(nullable NSString*)value;

- (BookmarkDetail*)primitiveBookmarkAttractions;
- (void)setPrimitiveBookmarkAttractions:(BookmarkDetail*)value;

- (CommonUser*)primitiveUserBookmarkAttraction;
- (void)setPrimitiveUserBookmarkAttraction:(CommonUser*)value;

@end

@interface BookmarkAttractionsAttributes: NSObject 
+ (NSString *)address;
+ (NSString *)bookmark_users_string;
+ (NSString *)create_date;
+ (NSString *)detail;
+ (NSString *)entity_id;
+ (NSString *)geotype;
+ (NSString *)hours_info;
+ (NSString *)language;
+ (NSString *)last_updated;
+ (NSString *)loc_lat;
+ (NSString *)loc_long;
+ (NSString *)name;
+ (NSString *)phone_no;
+ (NSString *)place_id;
+ (NSString *)total_bookmark;
+ (NSString *)total_share;
+ (NSString *)total_view;
+ (NSString *)video_id;
+ (NSString *)web_url;
@end

@interface BookmarkAttractionsRelationships: NSObject
+ (NSString *)bookmarkAttractions;
+ (NSString *)userBookmarkAttraction;
@end

NS_ASSUME_NONNULL_END

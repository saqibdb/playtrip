// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to DraftTour.m instead.

#import "_DraftTour.h"

@implementation DraftTourID
@end

@implementation _DraftTour

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"DraftTour" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"DraftTour";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"DraftTour" inManagedObjectContext:moc_];
}

- (DraftTourID*)objectID {
	return (DraftTourID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"daysValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"days"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"self_driveValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"self_drive"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic create_date;

@dynamic days;

- (int32_t)daysValue {
	NSNumber *result = [self days];
	return [result intValue];
}

- (void)setDaysValue:(int32_t)value_ {
	[self setDays:@(value_)];
}

- (int32_t)primitiveDaysValue {
	NSNumber *result = [self primitiveDays];
	return [result intValue];
}

- (void)setPrimitiveDaysValue:(int32_t)value_ {
	[self setPrimitiveDays:@(value_)];
}

@dynamic duration;

@dynamic entity_id;

@dynamic from_date;

@dynamic language;

@dynamic last_updated;

@dynamic merged_video;

@dynamic name;

@dynamic self_drive;

- (BOOL)self_driveValue {
	NSNumber *result = [self self_drive];
	return [result boolValue];
}

- (void)setSelf_driveValue:(BOOL)value_ {
	[self setSelf_drive:@(value_)];
}

- (BOOL)primitiveSelf_driveValue {
	NSNumber *result = [self primitiveSelf_drive];
	return [result boolValue];
}

- (void)setPrimitiveSelf_driveValue:(BOOL)value_ {
	[self setPrimitiveSelf_drive:@(value_)];
}

@dynamic thumbnail;

@dynamic to_date;

@dynamic travel_tip;

@dynamic attractions;

- (NSMutableSet<Attractions*>*)attractionsSet {
	[self willAccessValueForKey:@"attractions"];

	NSMutableSet<Attractions*> *result = (NSMutableSet<Attractions*>*)[self mutableSetValueForKey:@"attractions"];

	[self didAccessValueForKey:@"attractions"];
	return result;
}

@dynamic draftAttractions;

- (NSMutableSet<DraftAttraction*>*)draftAttractionsSet {
	[self willAccessValueForKey:@"draftAttractions"];

	NSMutableSet<DraftAttraction*> *result = (NSMutableSet<DraftAttraction*>*)[self mutableSetValueForKey:@"draftAttractions"];

	[self didAccessValueForKey:@"draftAttractions"];
	return result;
}

@end

@implementation DraftTourAttributes 
+ (NSString *)create_date {
	return @"create_date";
}
+ (NSString *)days {
	return @"days";
}
+ (NSString *)duration {
	return @"duration";
}
+ (NSString *)entity_id {
	return @"entity_id";
}
+ (NSString *)from_date {
	return @"from_date";
}
+ (NSString *)language {
	return @"language";
}
+ (NSString *)last_updated {
	return @"last_updated";
}
+ (NSString *)merged_video {
	return @"merged_video";
}
+ (NSString *)name {
	return @"name";
}
+ (NSString *)self_drive {
	return @"self_drive";
}
+ (NSString *)thumbnail {
	return @"thumbnail";
}
+ (NSString *)to_date {
	return @"to_date";
}
+ (NSString *)travel_tip {
	return @"travel_tip";
}
@end

@implementation DraftTourRelationships 
+ (NSString *)attractions {
	return @"attractions";
}
+ (NSString *)draftAttractions {
	return @"draftAttractions";
}
@end


// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to DraftInfo.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class DraftAttrData;

@interface DraftInfoID : NSManagedObjectID {}
@end

@interface _DraftInfo : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) DraftInfoID *objectID;

@property (nonatomic, strong, nullable) NSString* address;

@property (nonatomic, strong, nullable) NSString* detail;

@property (nonatomic, strong, nullable) NSString* entity_id;

@property (nonatomic, strong, nullable) NSString* geotype;

@property (nonatomic, strong, nullable) NSString* language;

@property (nonatomic, strong, nullable) NSNumber* loc_lat;

@property (atomic) int32_t loc_latValue;
- (int32_t)loc_latValue;
- (void)setLoc_latValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSNumber* loc_long;

@property (atomic) double loc_longValue;
- (double)loc_longValue;
- (void)setLoc_longValue:(double)value_;

@property (nonatomic, strong, nullable) NSString* name;

@property (nonatomic, strong, nullable) NSString* phone_no;

@property (nonatomic, strong, nullable) NSString* web_url;

@property (nonatomic, strong, nullable) DraftAttrData *draftAttrData;

@end

@interface _DraftInfo (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveAddress;
- (void)setPrimitiveAddress:(nullable NSString*)value;

- (nullable NSString*)primitiveDetail;
- (void)setPrimitiveDetail:(nullable NSString*)value;

- (nullable NSString*)primitiveEntity_id;
- (void)setPrimitiveEntity_id:(nullable NSString*)value;

- (nullable NSString*)primitiveGeotype;
- (void)setPrimitiveGeotype:(nullable NSString*)value;

- (nullable NSString*)primitiveLanguage;
- (void)setPrimitiveLanguage:(nullable NSString*)value;

- (nullable NSNumber*)primitiveLoc_lat;
- (void)setPrimitiveLoc_lat:(nullable NSNumber*)value;

- (int32_t)primitiveLoc_latValue;
- (void)setPrimitiveLoc_latValue:(int32_t)value_;

- (nullable NSNumber*)primitiveLoc_long;
- (void)setPrimitiveLoc_long:(nullable NSNumber*)value;

- (double)primitiveLoc_longValue;
- (void)setPrimitiveLoc_longValue:(double)value_;

- (nullable NSString*)primitiveName;
- (void)setPrimitiveName:(nullable NSString*)value;

- (nullable NSString*)primitivePhone_no;
- (void)setPrimitivePhone_no:(nullable NSString*)value;

- (nullable NSString*)primitiveWeb_url;
- (void)setPrimitiveWeb_url:(nullable NSString*)value;

- (DraftAttrData*)primitiveDraftAttrData;
- (void)setPrimitiveDraftAttrData:(DraftAttrData*)value;

@end

@interface DraftInfoAttributes: NSObject 
+ (NSString *)address;
+ (NSString *)detail;
+ (NSString *)entity_id;
+ (NSString *)geotype;
+ (NSString *)language;
+ (NSString *)loc_lat;
+ (NSString *)loc_long;
+ (NSString *)name;
+ (NSString *)phone_no;
+ (NSString *)web_url;
@end

@interface DraftInfoRelationships: NSObject
+ (NSString *)draftAttrData;
@end

NS_ASSUME_NONNULL_END

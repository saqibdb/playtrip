// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BookmarkLocation.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class BookmarkDetail;

@interface BookmarkLocationID : NSManagedObjectID {}
@end

@interface _BookmarkLocation : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) BookmarkLocationID *objectID;

@property (nonatomic, strong, nullable) BookmarkDetail *bookmarkLocation;

@end

@interface _BookmarkLocation (CoreDataGeneratedPrimitiveAccessors)

- (BookmarkDetail*)primitiveBookmarkLocation;
- (void)setPrimitiveBookmarkLocation:(BookmarkDetail*)value;

@end

@interface BookmarkLocationRelationships: NSObject
+ (NSString *)bookmarkLocation;
@end

NS_ASSUME_NONNULL_END

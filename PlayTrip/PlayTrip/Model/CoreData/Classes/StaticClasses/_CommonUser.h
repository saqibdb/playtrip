// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to CommonUser.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class VideoTour;
@class MostPopularVideoTour;
@class AttractionData;
@class Attractions;
@class Info;
@class BookmarkVideotour;
@class BookmarkAttractions;
@class MostPopularAttractions;
@class MostPopularPlan;

@interface CommonUserID : NSManagedObjectID {}
@end

@interface _CommonUser : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) CommonUserID *objectID;

@property (nonatomic, strong, nullable) NSString* avatar;

@property (nonatomic, strong, nullable) NSString* cover_pic;

@property (nonatomic, strong, nullable) NSDate* create_date;

@property (nonatomic, strong, nullable) NSString* email;

@property (nonatomic, strong, nullable) NSString* entity_id;

@property (nonatomic, strong, nullable) NSString* facebook_id;

@property (nonatomic, strong, nullable) NSString* first_name;

@property (nonatomic, strong, nullable) NSString* full_name;

@property (nonatomic, strong, nullable) NSString* geotype;

@property (nonatomic, strong, nullable) NSString* google_id;

@property (nonatomic, strong, nullable) NSString* group_id;

@property (nonatomic, strong, nullable) NSNumber* is_active;

@property (atomic) BOOL is_activeValue;
- (BOOL)is_activeValue;
- (void)setIs_activeValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSNumber* is_locked;

@property (atomic) BOOL is_lockedValue;
- (BOOL)is_lockedValue;
- (void)setIs_lockedValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSNumber* is_recommended;

@property (atomic) BOOL is_recommendedValue;
- (BOOL)is_recommendedValue;
- (void)setIs_recommendedValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSNumber* is_verified;

@property (atomic) BOOL is_verifiedValue;
- (BOOL)is_verifiedValue;
- (void)setIs_verifiedValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSDate* last_logged_date;

@property (nonatomic, strong, nullable) NSString* last_name;

@property (nonatomic, strong, nullable) NSDate* last_updated;

@property (nonatomic, strong, nullable) NSNumber* logged_in_count;

@property (atomic) int32_t logged_in_countValue;
- (int32_t)logged_in_countValue;
- (void)setLogged_in_countValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSString* name;

@property (nonatomic, strong, nullable) NSString* phone_number;

@property (nonatomic, strong, nullable) NSString* provider;

@property (nonatomic, strong, nullable) NSString* role;

@property (nonatomic, strong, nullable) NSNumber* total_bookmark;

@property (atomic) int32_t total_bookmarkValue;
- (int32_t)total_bookmarkValue;
- (void)setTotal_bookmarkValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSNumber* total_share;

@property (atomic) int32_t total_shareValue;
- (int32_t)total_shareValue;
- (void)setTotal_shareValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSNumber* total_view;

@property (atomic) int32_t total_viewValue;
- (int32_t)total_viewValue;
- (void)setTotal_viewValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSString* user_name;

@property (nonatomic, strong, nullable) VideoTour *user;

@property (nonatomic, strong, nullable) MostPopularVideoTour *user1;

@property (nonatomic, strong, nullable) AttractionData *userAttrData;

@property (nonatomic, strong, nullable) Attractions *userAttraction;

@property (nonatomic, strong, nullable) Info *userAttractionData;

@property (nonatomic, strong, nullable) BookmarkVideotour *userBookMark;

@property (nonatomic, strong, nullable) BookmarkAttractions *userBookmarkAttraction;

@property (nonatomic, strong, nullable) MostPopularAttractions *userMostPopularAttractionData;

@property (nonatomic, strong, nullable) MostPopularPlan *userObject;

@end

@interface _CommonUser (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveAvatar;
- (void)setPrimitiveAvatar:(nullable NSString*)value;

- (nullable NSString*)primitiveCover_pic;
- (void)setPrimitiveCover_pic:(nullable NSString*)value;

- (nullable NSDate*)primitiveCreate_date;
- (void)setPrimitiveCreate_date:(nullable NSDate*)value;

- (nullable NSString*)primitiveEmail;
- (void)setPrimitiveEmail:(nullable NSString*)value;

- (nullable NSString*)primitiveEntity_id;
- (void)setPrimitiveEntity_id:(nullable NSString*)value;

- (nullable NSString*)primitiveFacebook_id;
- (void)setPrimitiveFacebook_id:(nullable NSString*)value;

- (nullable NSString*)primitiveFirst_name;
- (void)setPrimitiveFirst_name:(nullable NSString*)value;

- (nullable NSString*)primitiveFull_name;
- (void)setPrimitiveFull_name:(nullable NSString*)value;

- (nullable NSString*)primitiveGeotype;
- (void)setPrimitiveGeotype:(nullable NSString*)value;

- (nullable NSString*)primitiveGoogle_id;
- (void)setPrimitiveGoogle_id:(nullable NSString*)value;

- (nullable NSString*)primitiveGroup_id;
- (void)setPrimitiveGroup_id:(nullable NSString*)value;

- (nullable NSNumber*)primitiveIs_active;
- (void)setPrimitiveIs_active:(nullable NSNumber*)value;

- (BOOL)primitiveIs_activeValue;
- (void)setPrimitiveIs_activeValue:(BOOL)value_;

- (nullable NSNumber*)primitiveIs_locked;
- (void)setPrimitiveIs_locked:(nullable NSNumber*)value;

- (BOOL)primitiveIs_lockedValue;
- (void)setPrimitiveIs_lockedValue:(BOOL)value_;

- (nullable NSNumber*)primitiveIs_recommended;
- (void)setPrimitiveIs_recommended:(nullable NSNumber*)value;

- (BOOL)primitiveIs_recommendedValue;
- (void)setPrimitiveIs_recommendedValue:(BOOL)value_;

- (nullable NSNumber*)primitiveIs_verified;
- (void)setPrimitiveIs_verified:(nullable NSNumber*)value;

- (BOOL)primitiveIs_verifiedValue;
- (void)setPrimitiveIs_verifiedValue:(BOOL)value_;

- (nullable NSDate*)primitiveLast_logged_date;
- (void)setPrimitiveLast_logged_date:(nullable NSDate*)value;

- (nullable NSString*)primitiveLast_name;
- (void)setPrimitiveLast_name:(nullable NSString*)value;

- (nullable NSDate*)primitiveLast_updated;
- (void)setPrimitiveLast_updated:(nullable NSDate*)value;

- (nullable NSNumber*)primitiveLogged_in_count;
- (void)setPrimitiveLogged_in_count:(nullable NSNumber*)value;

- (int32_t)primitiveLogged_in_countValue;
- (void)setPrimitiveLogged_in_countValue:(int32_t)value_;

- (nullable NSString*)primitiveName;
- (void)setPrimitiveName:(nullable NSString*)value;

- (nullable NSString*)primitivePhone_number;
- (void)setPrimitivePhone_number:(nullable NSString*)value;

- (nullable NSString*)primitiveProvider;
- (void)setPrimitiveProvider:(nullable NSString*)value;

- (nullable NSString*)primitiveRole;
- (void)setPrimitiveRole:(nullable NSString*)value;

- (nullable NSNumber*)primitiveTotal_bookmark;
- (void)setPrimitiveTotal_bookmark:(nullable NSNumber*)value;

- (int32_t)primitiveTotal_bookmarkValue;
- (void)setPrimitiveTotal_bookmarkValue:(int32_t)value_;

- (nullable NSNumber*)primitiveTotal_share;
- (void)setPrimitiveTotal_share:(nullable NSNumber*)value;

- (int32_t)primitiveTotal_shareValue;
- (void)setPrimitiveTotal_shareValue:(int32_t)value_;

- (nullable NSNumber*)primitiveTotal_view;
- (void)setPrimitiveTotal_view:(nullable NSNumber*)value;

- (int32_t)primitiveTotal_viewValue;
- (void)setPrimitiveTotal_viewValue:(int32_t)value_;

- (nullable NSString*)primitiveUser_name;
- (void)setPrimitiveUser_name:(nullable NSString*)value;

- (VideoTour*)primitiveUser;
- (void)setPrimitiveUser:(VideoTour*)value;

- (MostPopularVideoTour*)primitiveUser1;
- (void)setPrimitiveUser1:(MostPopularVideoTour*)value;

- (AttractionData*)primitiveUserAttrData;
- (void)setPrimitiveUserAttrData:(AttractionData*)value;

- (Attractions*)primitiveUserAttraction;
- (void)setPrimitiveUserAttraction:(Attractions*)value;

- (Info*)primitiveUserAttractionData;
- (void)setPrimitiveUserAttractionData:(Info*)value;

- (BookmarkVideotour*)primitiveUserBookMark;
- (void)setPrimitiveUserBookMark:(BookmarkVideotour*)value;

- (BookmarkAttractions*)primitiveUserBookmarkAttraction;
- (void)setPrimitiveUserBookmarkAttraction:(BookmarkAttractions*)value;

- (MostPopularAttractions*)primitiveUserMostPopularAttractionData;
- (void)setPrimitiveUserMostPopularAttractionData:(MostPopularAttractions*)value;

- (MostPopularPlan*)primitiveUserObject;
- (void)setPrimitiveUserObject:(MostPopularPlan*)value;

@end

@interface CommonUserAttributes: NSObject 
+ (NSString *)avatar;
+ (NSString *)cover_pic;
+ (NSString *)create_date;
+ (NSString *)email;
+ (NSString *)entity_id;
+ (NSString *)facebook_id;
+ (NSString *)first_name;
+ (NSString *)full_name;
+ (NSString *)geotype;
+ (NSString *)google_id;
+ (NSString *)group_id;
+ (NSString *)is_active;
+ (NSString *)is_locked;
+ (NSString *)is_recommended;
+ (NSString *)is_verified;
+ (NSString *)last_logged_date;
+ (NSString *)last_name;
+ (NSString *)last_updated;
+ (NSString *)logged_in_count;
+ (NSString *)name;
+ (NSString *)phone_number;
+ (NSString *)provider;
+ (NSString *)role;
+ (NSString *)total_bookmark;
+ (NSString *)total_share;
+ (NSString *)total_view;
+ (NSString *)user_name;
@end

@interface CommonUserRelationships: NSObject
+ (NSString *)user;
+ (NSString *)user1;
+ (NSString *)userAttrData;
+ (NSString *)userAttraction;
+ (NSString *)userAttractionData;
+ (NSString *)userBookMark;
+ (NSString *)userBookmarkAttraction;
+ (NSString *)userMostPopularAttractionData;
+ (NSString *)userObject;
@end

NS_ASSUME_NONNULL_END

// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LocalAttrTags.m instead.

#import "_LocalAttrTags.h"

@implementation LocalAttrTagsID
@end

@implementation _LocalAttrTags

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"LocalAttrTags" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"LocalAttrTags";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"LocalAttrTags" inManagedObjectContext:moc_];
}

- (LocalAttrTagsID*)objectID {
	return (LocalAttrTagsID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic entity_id;

@dynamic tagName;

@end

@implementation LocalAttrTagsAttributes 
+ (NSString *)entity_id {
	return @"entity_id";
}
+ (NSString *)tagName {
	return @"tagName";
}
@end


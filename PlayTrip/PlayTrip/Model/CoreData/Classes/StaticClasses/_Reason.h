// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Reason.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@interface ReasonID : NSManagedObjectID {}
@end

@interface _Reason : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) ReasonID *objectID;

@property (nonatomic, strong, nullable) NSDate* create_date;

@property (nonatomic, strong, nullable) NSString* desc;

@property (nonatomic, strong, nullable) NSString* entity_id;

@property (nonatomic, strong, nullable) NSString* title;

@end

@interface _Reason (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSDate*)primitiveCreate_date;
- (void)setPrimitiveCreate_date:(nullable NSDate*)value;

- (nullable NSString*)primitiveDesc;
- (void)setPrimitiveDesc:(nullable NSString*)value;

- (nullable NSString*)primitiveEntity_id;
- (void)setPrimitiveEntity_id:(nullable NSString*)value;

- (nullable NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(nullable NSString*)value;

@end

@interface ReasonAttributes: NSObject 
+ (NSString *)create_date;
+ (NSString *)desc;
+ (NSString *)entity_id;
+ (NSString *)title;
@end

NS_ASSUME_NONNULL_END

// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MostPopularAttractions.m instead.

#import "_MostPopularAttractions.h"

@implementation MostPopularAttractionsID
@end

@implementation _MostPopularAttractions

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MostPopularAttractions" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MostPopularAttractions";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MostPopularAttractions" inManagedObjectContext:moc_];
}

- (MostPopularAttractionsID*)objectID {
	return (MostPopularAttractionsID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"loc_latValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loc_lat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loc_longValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loc_long"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"total_bookmarkValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"total_bookmark"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"total_shareValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"total_share"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"total_viewValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"total_view"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic address;

@dynamic bookmark_users_string;

@dynamic create_date;

@dynamic detail;

@dynamic entity_id;

@dynamic geotype;

@dynamic hours_info;

@dynamic language;

@dynamic last_updated;

@dynamic loc_lat;

- (double)loc_latValue {
	NSNumber *result = [self loc_lat];
	return [result doubleValue];
}

- (void)setLoc_latValue:(double)value_ {
	[self setLoc_lat:@(value_)];
}

- (double)primitiveLoc_latValue {
	NSNumber *result = [self primitiveLoc_lat];
	return [result doubleValue];
}

- (void)setPrimitiveLoc_latValue:(double)value_ {
	[self setPrimitiveLoc_lat:@(value_)];
}

@dynamic loc_long;

- (double)loc_longValue {
	NSNumber *result = [self loc_long];
	return [result doubleValue];
}

- (void)setLoc_longValue:(double)value_ {
	[self setLoc_long:@(value_)];
}

- (double)primitiveLoc_longValue {
	NSNumber *result = [self primitiveLoc_long];
	return [result doubleValue];
}

- (void)setPrimitiveLoc_longValue:(double)value_ {
	[self setPrimitiveLoc_long:@(value_)];
}

@dynamic name;

@dynamic phone_no;

@dynamic place_id;

@dynamic total_bookmark;

- (int32_t)total_bookmarkValue {
	NSNumber *result = [self total_bookmark];
	return [result intValue];
}

- (void)setTotal_bookmarkValue:(int32_t)value_ {
	[self setTotal_bookmark:@(value_)];
}

- (int32_t)primitiveTotal_bookmarkValue {
	NSNumber *result = [self primitiveTotal_bookmark];
	return [result intValue];
}

- (void)setPrimitiveTotal_bookmarkValue:(int32_t)value_ {
	[self setPrimitiveTotal_bookmark:@(value_)];
}

@dynamic total_share;

- (int32_t)total_shareValue {
	NSNumber *result = [self total_share];
	return [result intValue];
}

- (void)setTotal_shareValue:(int32_t)value_ {
	[self setTotal_share:@(value_)];
}

- (int32_t)primitiveTotal_shareValue {
	NSNumber *result = [self primitiveTotal_share];
	return [result intValue];
}

- (void)setPrimitiveTotal_shareValue:(int32_t)value_ {
	[self setPrimitiveTotal_share:@(value_)];
}

@dynamic total_view;

- (int32_t)total_viewValue {
	NSNumber *result = [self total_view];
	return [result intValue];
}

- (void)setTotal_viewValue:(int32_t)value_ {
	[self setTotal_view:@(value_)];
}

- (int32_t)primitiveTotal_viewValue {
	NSNumber *result = [self primitiveTotal_view];
	return [result intValue];
}

- (void)setPrimitiveTotal_viewValue:(int32_t)value_ {
	[self setPrimitiveTotal_view:@(value_)];
}

@dynamic video_id;

@dynamic web_url;

@dynamic categori;

- (NSMutableSet<Categori*>*)categoriSet {
	[self willAccessValueForKey:@"categori"];

	NSMutableSet<Categori*> *result = (NSMutableSet<Categori*>*)[self mutableSetValueForKey:@"categori"];

	[self didAccessValueForKey:@"categori"];
	return result;
}

@dynamic userMostPopularAttractionData;

@end

@implementation MostPopularAttractionsAttributes 
+ (NSString *)address {
	return @"address";
}
+ (NSString *)bookmark_users_string {
	return @"bookmark_users_string";
}
+ (NSString *)create_date {
	return @"create_date";
}
+ (NSString *)detail {
	return @"detail";
}
+ (NSString *)entity_id {
	return @"entity_id";
}
+ (NSString *)geotype {
	return @"geotype";
}
+ (NSString *)hours_info {
	return @"hours_info";
}
+ (NSString *)language {
	return @"language";
}
+ (NSString *)last_updated {
	return @"last_updated";
}
+ (NSString *)loc_lat {
	return @"loc_lat";
}
+ (NSString *)loc_long {
	return @"loc_long";
}
+ (NSString *)name {
	return @"name";
}
+ (NSString *)phone_no {
	return @"phone_no";
}
+ (NSString *)place_id {
	return @"place_id";
}
+ (NSString *)total_bookmark {
	return @"total_bookmark";
}
+ (NSString *)total_share {
	return @"total_share";
}
+ (NSString *)total_view {
	return @"total_view";
}
+ (NSString *)video_id {
	return @"video_id";
}
+ (NSString *)web_url {
	return @"web_url";
}
@end

@implementation MostPopularAttractionsRelationships 
+ (NSString *)categori {
	return @"categori";
}
+ (NSString *)userMostPopularAttractionData {
	return @"userMostPopularAttractionData";
}
@end


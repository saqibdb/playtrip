// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SearchData.m instead.

#import "_SearchData.h"

@implementation SearchDataID
@end

@implementation _SearchData

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"SearchData" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"SearchData";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"SearchData" inManagedObjectContext:moc_];
}

- (SearchDataID*)objectID {
	return (SearchDataID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"loc_latValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loc_lat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loc_longValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loc_long"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic address;

@dynamic entity_id;

@dynamic geotype;

@dynamic loc_lat;

- (double)loc_latValue {
	NSNumber *result = [self loc_lat];
	return [result doubleValue];
}

- (void)setLoc_latValue:(double)value_ {
	[self setLoc_lat:@(value_)];
}

- (double)primitiveLoc_latValue {
	NSNumber *result = [self primitiveLoc_lat];
	return [result doubleValue];
}

- (void)setPrimitiveLoc_latValue:(double)value_ {
	[self setPrimitiveLoc_lat:@(value_)];
}

@dynamic loc_long;

- (double)loc_longValue {
	NSNumber *result = [self loc_long];
	return [result doubleValue];
}

- (void)setLoc_longValue:(double)value_ {
	[self setLoc_long:@(value_)];
}

- (double)primitiveLoc_longValue {
	NSNumber *result = [self primitiveLoc_long];
	return [result doubleValue];
}

- (void)setPrimitiveLoc_longValue:(double)value_ {
	[self setPrimitiveLoc_long:@(value_)];
}

@dynamic name;

@dynamic place_id;

@end

@implementation SearchDataAttributes 
+ (NSString *)address {
	return @"address";
}
+ (NSString *)entity_id {
	return @"entity_id";
}
+ (NSString *)geotype {
	return @"geotype";
}
+ (NSString *)loc_lat {
	return @"loc_lat";
}
+ (NSString *)loc_long {
	return @"loc_long";
}
+ (NSString *)name {
	return @"name";
}
+ (NSString *)place_id {
	return @"place_id";
}
@end


// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to DraftAttrData.m instead.

#import "_DraftAttrData.h"

@implementation DraftAttrDataID
@end

@implementation _DraftAttrData

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"DraftAttrData" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"DraftAttrData";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"DraftAttrData" inManagedObjectContext:moc_];
}

- (DraftAttrDataID*)objectID {
	return (DraftAttrDataID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
    NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

    if ([key isEqualToString:@"SequenceValue"]) {
        NSSet *affectingKey = [NSSet setWithObject:@"Sequence"];
        keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
        return keyPaths;
    }
    

	return keyPaths;
}

@dynamic desc;

@dynamic entity_id;

@dynamic time;

@dynamic sequence;

- (int32_t)sequenceValue {
    NSNumber *result = [self sequence];
    return [result intValue];
}

- (void)setSequenceValue:(int32_t)value_ {
    [self setSequence:@(value_)];
}

- (int32_t)primitiveSequenceValue {
    NSNumber *result = [self primitiveSequence];
    return [result intValue];
}

- (void)setPrimitiveSequenceValue:(int32_t)value_ {
    [self setPrimitiveSequence:@(value_)];
}



@dynamic draftAttractions;

@dynamic draftInfo;

@dynamic draftVideoId;

@end

@implementation DraftAttrDataAttributes 
+ (NSString *)desc {
	return @"desc";
}
+ (NSString *)entity_id {
	return @"entity_id";
}
+ (NSString *)time {
	return @"time";
}
+ (NSString *)sequence {
    return @"sequence";
}
@end

@implementation DraftAttrDataRelationships 
+ (NSString *)draftAttractions {
	return @"draftAttractions";
}
+ (NSString *)draftInfo {
	return @"draftInfo";
}
+ (NSString *)draftVideoId {
	return @"draftVideoId";
}
@end


// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to DraftVideoId.m instead.

#import "_DraftVideoId.h"

@implementation DraftVideoIdID
@end

@implementation _DraftVideoId

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"DraftVideoId" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"DraftVideoId";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"DraftVideoId" inManagedObjectContext:moc_];
}

- (DraftVideoIdID*)objectID {
	return (DraftVideoIdID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic entity_id;

@dynamic file_name;

@dynamic thumbnail;

@dynamic user;

@dynamic draftAttrData;

@end

@implementation DraftVideoIdAttributes 
+ (NSString *)entity_id {
	return @"entity_id";
}
+ (NSString *)file_name {
	return @"file_name";
}
+ (NSString *)thumbnail {
	return @"thumbnail";
}
+ (NSString *)user {
	return @"user";
}
@end

@implementation DraftVideoIdRelationships 
+ (NSString *)draftAttrData {
	return @"draftAttrData";
}
@end


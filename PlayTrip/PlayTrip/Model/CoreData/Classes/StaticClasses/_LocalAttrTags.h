// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LocalAttrTags.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@interface LocalAttrTagsID : NSManagedObjectID {}
@end

@interface _LocalAttrTags : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) LocalAttrTagsID *objectID;

@property (nonatomic, strong, nullable) NSString* entity_id;

@property (nonatomic, strong, nullable) NSString* tagName;

@end

@interface _LocalAttrTags (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveEntity_id;
- (void)setPrimitiveEntity_id:(nullable NSString*)value;

- (nullable NSString*)primitiveTagName;
- (void)setPrimitiveTagName:(nullable NSString*)value;

@end

@interface LocalAttrTagsAttributes: NSObject 
+ (NSString *)entity_id;
+ (NSString *)tagName;
@end

NS_ASSUME_NONNULL_END

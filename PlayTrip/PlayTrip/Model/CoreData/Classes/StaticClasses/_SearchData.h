// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to SearchData.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@interface SearchDataID : NSManagedObjectID {}
@end

@interface _SearchData : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) SearchDataID *objectID;

@property (nonatomic, strong, nullable) NSString* address;

@property (nonatomic, strong, nullable) NSString* entity_id;

@property (nonatomic, strong, nullable) NSString* geotype;

@property (nonatomic, strong, nullable) NSNumber* loc_lat;

@property (atomic) double loc_latValue;
- (double)loc_latValue;
- (void)setLoc_latValue:(double)value_;

@property (nonatomic, strong, nullable) NSNumber* loc_long;

@property (atomic) double loc_longValue;
- (double)loc_longValue;
- (void)setLoc_longValue:(double)value_;

@property (nonatomic, strong, nullable) NSString* name;

@property (nonatomic, strong, nullable) NSString* place_id;

@end

@interface _SearchData (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveAddress;
- (void)setPrimitiveAddress:(nullable NSString*)value;

- (nullable NSString*)primitiveEntity_id;
- (void)setPrimitiveEntity_id:(nullable NSString*)value;

- (nullable NSString*)primitiveGeotype;
- (void)setPrimitiveGeotype:(nullable NSString*)value;

- (nullable NSNumber*)primitiveLoc_lat;
- (void)setPrimitiveLoc_lat:(nullable NSNumber*)value;

- (double)primitiveLoc_latValue;
- (void)setPrimitiveLoc_latValue:(double)value_;

- (nullable NSNumber*)primitiveLoc_long;
- (void)setPrimitiveLoc_long:(nullable NSNumber*)value;

- (double)primitiveLoc_longValue;
- (void)setPrimitiveLoc_longValue:(double)value_;

- (nullable NSString*)primitiveName;
- (void)setPrimitiveName:(nullable NSString*)value;

- (nullable NSString*)primitivePlace_id;
- (void)setPrimitivePlace_id:(nullable NSString*)value;

@end

@interface SearchDataAttributes: NSObject 
+ (NSString *)address;
+ (NSString *)entity_id;
+ (NSString *)geotype;
+ (NSString *)loc_lat;
+ (NSString *)loc_long;
+ (NSString *)name;
+ (NSString *)place_id;
@end

NS_ASSUME_NONNULL_END

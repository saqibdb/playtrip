// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BookmarkDetail.m instead.

#import "_BookmarkDetail.h"

@implementation BookmarkDetailID
@end

@implementation _BookmarkDetail

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"BookmarkDetail" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"BookmarkDetail";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"BookmarkDetail" inManagedObjectContext:moc_];
}

- (BookmarkDetailID*)objectID {
	return (BookmarkDetailID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic bookmarkAttractions;

- (NSMutableSet<BookmarkAttractions*>*)bookmarkAttractionsSet {
	[self willAccessValueForKey:@"bookmarkAttractions"];

	NSMutableSet<BookmarkAttractions*> *result = (NSMutableSet<BookmarkAttractions*>*)[self mutableSetValueForKey:@"bookmarkAttractions"];

	[self didAccessValueForKey:@"bookmarkAttractions"];
	return result;
}

@dynamic bookmarkLocation;

- (NSMutableSet<BookmarkLocation*>*)bookmarkLocationSet {
	[self willAccessValueForKey:@"bookmarkLocation"];

	NSMutableSet<BookmarkLocation*> *result = (NSMutableSet<BookmarkLocation*>*)[self mutableSetValueForKey:@"bookmarkLocation"];

	[self didAccessValueForKey:@"bookmarkLocation"];
	return result;
}

@dynamic bookmarkUser;

- (NSMutableSet<BookmarkUser*>*)bookmarkUserSet {
	[self willAccessValueForKey:@"bookmarkUser"];

	NSMutableSet<BookmarkUser*> *result = (NSMutableSet<BookmarkUser*>*)[self mutableSetValueForKey:@"bookmarkUser"];

	[self didAccessValueForKey:@"bookmarkUser"];
	return result;
}

@dynamic bookmarkVideotour;

- (NSMutableSet<BookmarkVideotour*>*)bookmarkVideotourSet {
	[self willAccessValueForKey:@"bookmarkVideotour"];

	NSMutableSet<BookmarkVideotour*> *result = (NSMutableSet<BookmarkVideotour*>*)[self mutableSetValueForKey:@"bookmarkVideotour"];

	[self didAccessValueForKey:@"bookmarkVideotour"];
	return result;
}

@end

@implementation BookmarkDetailRelationships 
+ (NSString *)bookmarkAttractions {
	return @"bookmarkAttractions";
}
+ (NSString *)bookmarkLocation {
	return @"bookmarkLocation";
}
+ (NSString *)bookmarkUser {
	return @"bookmarkUser";
}
+ (NSString *)bookmarkVideotour {
	return @"bookmarkVideotour";
}
@end


// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to District.m instead.

#import "_District.h"

@implementation DistrictID
@end

@implementation _District

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"District" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"District";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"District" inManagedObjectContext:moc_];
}

- (DistrictID*)objectID {
	return (DistrictID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"plan_cntValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"plan_cnt"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic create_date;

@dynamic entity_id;

@dynamic image;

@dynamic longitude;

@dynamic name;

@dynamic plan_cnt;

- (int32_t)plan_cntValue {
	NSNumber *result = [self plan_cnt];
	return [result intValue];
}

- (void)setPlan_cntValue:(int32_t)value_ {
	[self setPlan_cnt:@(value_)];
}

- (int32_t)primitivePlan_cntValue {
	NSNumber *result = [self primitivePlan_cnt];
	return [result intValue];
}

- (void)setPrimitivePlan_cntValue:(int32_t)value_ {
	[self setPrimitivePlan_cnt:@(value_)];
}

@dynamic status;

@end

@implementation DistrictAttributes 
+ (NSString *)create_date {
	return @"create_date";
}
+ (NSString *)entity_id {
	return @"entity_id";
}
+ (NSString *)image {
	return @"image";
}
+ (NSString *)longitude {
	return @"longitude";
}
+ (NSString *)name {
	return @"name";
}
+ (NSString *)plan_cnt {
	return @"plan_cnt";
}
+ (NSString *)status {
	return @"status";
}
@end


// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to DraftAttrData.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class DraftAttraction;
@class DraftInfo;
@class DraftVideoId;

@interface DraftAttrDataID : NSManagedObjectID {}
@end

@interface _DraftAttrData : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) DraftAttrDataID *objectID;

@property (nonatomic, strong, nullable) NSString* desc;

@property (nonatomic, strong, nullable) NSString* entity_id;

@property (nonatomic, strong, nullable) NSString* time;

@property (nonatomic, strong, nullable) DraftAttraction *draftAttractions;

@property (nonatomic, strong, nullable) DraftInfo *draftInfo;

@property (nonatomic, strong, nullable) DraftVideoId *draftVideoId;



@property (nonatomic, strong, nullable) NSNumber* sequence;

@property (atomic) int32_t sequenceValue;
- (int32_t)sequenceValue;
- (void)setSequenceValue:(int32_t)value_;


@end

@interface _DraftAttrData (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveDesc;
- (void)setPrimitiveDesc:(nullable NSString*)value;

- (nullable NSString*)primitiveEntity_id;
- (void)setPrimitiveEntity_id:(nullable NSString*)value;

- (nullable NSString*)primitiveTime;
- (void)setPrimitiveTime:(nullable NSString*)value;

- (DraftAttraction*)primitiveDraftAttractions;
- (void)setPrimitiveDraftAttractions:(DraftAttraction*)value;

- (DraftInfo*)primitiveDraftInfo;
- (void)setPrimitiveDraftInfo:(DraftInfo*)value;

- (DraftVideoId*)primitiveDraftVideoId;
- (void)setPrimitiveDraftVideoId:(DraftVideoId*)value;

- (nullable NSNumber*)primitiveSequence;
- (void)setPrimitiveSequence:(nullable NSNumber*)value;


- (int32_t)primitiveSequenceValue;
- (void)setSequenceValue:(int32_t)value_;

@end

@interface DraftAttrDataAttributes: NSObject 
+ (NSString *)desc;
+ (NSString *)entity_id;
+ (NSString *)time;
+ (NSNumber *)sequence;

@end

@interface DraftAttrDataRelationships: NSObject
+ (NSString *)draftAttractions;
+ (NSString *)draftInfo;
+ (NSString *)draftVideoId;
@end

NS_ASSUME_NONNULL_END

// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AttractionData.m instead.

#import "_AttractionData.h"

@implementation AttractionDataID
@end

@implementation _AttractionData

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"AttractionData" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"AttractionData";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"AttractionData" inManagedObjectContext:moc_];
}

- (AttractionDataID*)objectID {
	return (AttractionDataID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic desc;

@dynamic entity_id;

@dynamic time;

@dynamic attractionData;

@dynamic info;

@dynamic userAttractionData;

@dynamic videoId;

@end

@implementation AttractionDataAttributes 
+ (NSString *)desc {
	return @"desc";
}
+ (NSString *)entity_id {
	return @"entity_id";
}
+ (NSString *)time {
	return @"time";
}
@end

@implementation AttractionDataRelationships 
+ (NSString *)attractionData {
	return @"attractionData";
}
+ (NSString *)info {
	return @"info";
}
+ (NSString *)userAttractionData {
	return @"userAttractionData";
}
+ (NSString *)videoId {
	return @"videoId";
}
@end


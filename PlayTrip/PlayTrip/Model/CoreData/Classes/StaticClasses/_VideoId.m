// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to VideoId.m instead.

#import "_VideoId.h"

@implementation VideoIdID
@end

@implementation _VideoId

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"VideoId" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"VideoId";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"VideoId" inManagedObjectContext:moc_];
}

- (VideoIdID*)objectID {
	return (VideoIdID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic entity_id;

@dynamic file_name;

@dynamic thumbnail;

@dynamic user;

@dynamic attrData;

@end

@implementation VideoIdAttributes 
+ (NSString *)entity_id {
	return @"entity_id";
}
+ (NSString *)file_name {
	return @"file_name";
}
+ (NSString *)thumbnail {
	return @"thumbnail";
}
+ (NSString *)user {
	return @"user";
}
@end

@implementation VideoIdRelationships 
+ (NSString *)attrData {
	return @"attrData";
}
@end


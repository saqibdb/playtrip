// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BookmarkVideotour.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class Attractions;
@class BookmarkDetail;
@class CommonUser;

@interface BookmarkVideotourID : NSManagedObjectID {}
@end

@interface _BookmarkVideotour : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) BookmarkVideotourID *objectID;

@property (nonatomic, strong, nullable) NSString* bookmark_users_string;

@property (nonatomic, strong, nullable) NSDate* create_date;

@property (nonatomic, strong, nullable) NSNumber* days;

@property (atomic) int32_t daysValue;
- (int32_t)daysValue;
- (void)setDaysValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSString* entity_id;

@property (nonatomic, strong, nullable) NSDate* from_date;

@property (nonatomic, strong, nullable) NSNumber* is_draft;

@property (atomic) BOOL is_draftValue;
- (BOOL)is_draftValue;
- (void)setIs_draftValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSNumber* is_published;

@property (atomic) BOOL is_publishedValue;
- (BOOL)is_publishedValue;
- (void)setIs_publishedValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSString* language;

@property (nonatomic, strong, nullable) NSDate* last_updated;

@property (nonatomic, strong, nullable) NSString* name;

@property (nonatomic, strong, nullable) NSNumber* remuneration_amount;

@property (atomic) int32_t remuneration_amountValue;
- (int32_t)remuneration_amountValue;
- (void)setRemuneration_amountValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSNumber* self_drive;

@property (atomic) BOOL self_driveValue;
- (BOOL)self_driveValue;
- (void)setSelf_driveValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSString* thumbnail;

@property (nonatomic, strong, nullable) NSDate* to_date;

@property (nonatomic, strong, nullable) NSNumber* total_bookmark;

@property (atomic) int32_t total_bookmarkValue;
- (int32_t)total_bookmarkValue;
- (void)setTotal_bookmarkValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSNumber* total_share;

@property (atomic) int32_t total_shareValue;
- (int32_t)total_shareValue;
- (void)setTotal_shareValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSNumber* total_view;

@property (atomic) int32_t total_viewValue;
- (int32_t)total_viewValue;
- (void)setTotal_viewValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSSet<Attractions*> *attractions;
- (nullable NSMutableSet<Attractions*>*)attractionsSet;

@property (nonatomic, strong, nullable) BookmarkDetail *bookmarkVideotour;

@property (nonatomic, strong, nullable) CommonUser *userBookMark;

@end

@interface _BookmarkVideotour (AttractionsCoreDataGeneratedAccessors)
- (void)addAttractions:(NSSet<Attractions*>*)value_;
- (void)removeAttractions:(NSSet<Attractions*>*)value_;
- (void)addAttractionsObject:(Attractions*)value_;
- (void)removeAttractionsObject:(Attractions*)value_;

@end

@interface _BookmarkVideotour (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveBookmark_users_string;
- (void)setPrimitiveBookmark_users_string:(nullable NSString*)value;

- (nullable NSDate*)primitiveCreate_date;
- (void)setPrimitiveCreate_date:(nullable NSDate*)value;

- (nullable NSNumber*)primitiveDays;
- (void)setPrimitiveDays:(nullable NSNumber*)value;

- (int32_t)primitiveDaysValue;
- (void)setPrimitiveDaysValue:(int32_t)value_;

- (nullable NSString*)primitiveEntity_id;
- (void)setPrimitiveEntity_id:(nullable NSString*)value;

- (nullable NSDate*)primitiveFrom_date;
- (void)setPrimitiveFrom_date:(nullable NSDate*)value;

- (nullable NSNumber*)primitiveIs_draft;
- (void)setPrimitiveIs_draft:(nullable NSNumber*)value;

- (BOOL)primitiveIs_draftValue;
- (void)setPrimitiveIs_draftValue:(BOOL)value_;

- (nullable NSNumber*)primitiveIs_published;
- (void)setPrimitiveIs_published:(nullable NSNumber*)value;

- (BOOL)primitiveIs_publishedValue;
- (void)setPrimitiveIs_publishedValue:(BOOL)value_;

- (nullable NSString*)primitiveLanguage;
- (void)setPrimitiveLanguage:(nullable NSString*)value;

- (nullable NSDate*)primitiveLast_updated;
- (void)setPrimitiveLast_updated:(nullable NSDate*)value;

- (nullable NSString*)primitiveName;
- (void)setPrimitiveName:(nullable NSString*)value;

- (nullable NSNumber*)primitiveRemuneration_amount;
- (void)setPrimitiveRemuneration_amount:(nullable NSNumber*)value;

- (int32_t)primitiveRemuneration_amountValue;
- (void)setPrimitiveRemuneration_amountValue:(int32_t)value_;

- (nullable NSNumber*)primitiveSelf_drive;
- (void)setPrimitiveSelf_drive:(nullable NSNumber*)value;

- (BOOL)primitiveSelf_driveValue;
- (void)setPrimitiveSelf_driveValue:(BOOL)value_;

- (nullable NSString*)primitiveThumbnail;
- (void)setPrimitiveThumbnail:(nullable NSString*)value;

- (nullable NSDate*)primitiveTo_date;
- (void)setPrimitiveTo_date:(nullable NSDate*)value;

- (nullable NSNumber*)primitiveTotal_bookmark;
- (void)setPrimitiveTotal_bookmark:(nullable NSNumber*)value;

- (int32_t)primitiveTotal_bookmarkValue;
- (void)setPrimitiveTotal_bookmarkValue:(int32_t)value_;

- (nullable NSNumber*)primitiveTotal_share;
- (void)setPrimitiveTotal_share:(nullable NSNumber*)value;

- (int32_t)primitiveTotal_shareValue;
- (void)setPrimitiveTotal_shareValue:(int32_t)value_;

- (nullable NSNumber*)primitiveTotal_view;
- (void)setPrimitiveTotal_view:(nullable NSNumber*)value;

- (int32_t)primitiveTotal_viewValue;
- (void)setPrimitiveTotal_viewValue:(int32_t)value_;

- (NSMutableSet<Attractions*>*)primitiveAttractions;
- (void)setPrimitiveAttractions:(NSMutableSet<Attractions*>*)value;

- (BookmarkDetail*)primitiveBookmarkVideotour;
- (void)setPrimitiveBookmarkVideotour:(BookmarkDetail*)value;

- (CommonUser*)primitiveUserBookMark;
- (void)setPrimitiveUserBookMark:(CommonUser*)value;

@end

@interface BookmarkVideotourAttributes: NSObject 
+ (NSString *)bookmark_users_string;
+ (NSString *)create_date;
+ (NSString *)days;
+ (NSString *)entity_id;
+ (NSString *)from_date;
+ (NSString *)is_draft;
+ (NSString *)is_published;
+ (NSString *)language;
+ (NSString *)last_updated;
+ (NSString *)name;
+ (NSString *)remuneration_amount;
+ (NSString *)self_drive;
+ (NSString *)thumbnail;
+ (NSString *)to_date;
+ (NSString *)total_bookmark;
+ (NSString *)total_share;
+ (NSString *)total_view;
@end

@interface BookmarkVideotourRelationships: NSObject
+ (NSString *)attractions;
+ (NSString *)bookmarkVideotour;
+ (NSString *)userBookMark;
@end

NS_ASSUME_NONNULL_END

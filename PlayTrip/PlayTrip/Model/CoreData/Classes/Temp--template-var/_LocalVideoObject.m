// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LocalVideoObject.m instead.

#import "_LocalVideoObject.h"

@implementation LocalVideoObjectID
@end

@implementation _LocalVideoObject

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"LocalVideoObject" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"LocalVideoObject";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"LocalVideoObject" inManagedObjectContext:moc_];
}

- (LocalVideoObjectID*)objectID {
	return (LocalVideoObjectID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic addedAttractions;

@dynamic attrDataId;

@dynamic desc;

@dynamic entity_id;

@dynamic isSelected;

@dynamic isUploading;

@dynamic thumbnail;

@dynamic time;

@dynamic vidPath;

@dynamic videoTourId;

@end

@implementation LocalVideoObjectAttributes 
+ (NSString *)addedAttractions {
	return @"addedAttractions";
}
+ (NSString *)attrDataId {
	return @"attrDataId";
}
+ (NSString *)desc {
	return @"desc";
}
+ (NSString *)entity_id {
	return @"entity_id";
}
+ (NSString *)isSelected {
	return @"isSelected";
}
+ (NSString *)isUploading {
	return @"isUploading";
}
+ (NSString *)thumbnail {
	return @"thumbnail";
}
+ (NSString *)time {
	return @"time";
}
+ (NSString *)vidPath {
	return @"vidPath";
}
+ (NSString *)videoTourId {
	return @"videoTourId";
}
@end


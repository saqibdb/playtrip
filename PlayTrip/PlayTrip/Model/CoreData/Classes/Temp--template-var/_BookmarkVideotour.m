// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BookmarkVideotour.m instead.

#import "_BookmarkVideotour.h"

@implementation BookmarkVideotourID
@end

@implementation _BookmarkVideotour

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"BookmarkVideotour" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"BookmarkVideotour";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"BookmarkVideotour" inManagedObjectContext:moc_];
}

- (BookmarkVideotourID*)objectID {
	return (BookmarkVideotourID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"daysValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"days"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"is_draftValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"is_draft"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"is_publishedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"is_published"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"remuneration_amountValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"remuneration_amount"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"self_driveValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"self_drive"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"total_bookmarkValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"total_bookmark"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"total_shareValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"total_share"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"total_viewValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"total_view"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic bookmark_users_string;

@dynamic create_date;

@dynamic days;

- (int32_t)daysValue {
	NSNumber *result = [self days];
	return [result intValue];
}

- (void)setDaysValue:(int32_t)value_ {
	[self setDays:@(value_)];
}

- (int32_t)primitiveDaysValue {
	NSNumber *result = [self primitiveDays];
	return [result intValue];
}

- (void)setPrimitiveDaysValue:(int32_t)value_ {
	[self setPrimitiveDays:@(value_)];
}

@dynamic entity_id;

@dynamic from_date;

@dynamic is_draft;

- (BOOL)is_draftValue {
	NSNumber *result = [self is_draft];
	return [result boolValue];
}

- (void)setIs_draftValue:(BOOL)value_ {
	[self setIs_draft:@(value_)];
}

- (BOOL)primitiveIs_draftValue {
	NSNumber *result = [self primitiveIs_draft];
	return [result boolValue];
}

- (void)setPrimitiveIs_draftValue:(BOOL)value_ {
	[self setPrimitiveIs_draft:@(value_)];
}

@dynamic is_published;

- (BOOL)is_publishedValue {
	NSNumber *result = [self is_published];
	return [result boolValue];
}

- (void)setIs_publishedValue:(BOOL)value_ {
	[self setIs_published:@(value_)];
}

- (BOOL)primitiveIs_publishedValue {
	NSNumber *result = [self primitiveIs_published];
	return [result boolValue];
}

- (void)setPrimitiveIs_publishedValue:(BOOL)value_ {
	[self setPrimitiveIs_published:@(value_)];
}

@dynamic language;

@dynamic last_updated;

@dynamic name;

@dynamic remuneration_amount;

- (int32_t)remuneration_amountValue {
	NSNumber *result = [self remuneration_amount];
	return [result intValue];
}

- (void)setRemuneration_amountValue:(int32_t)value_ {
	[self setRemuneration_amount:@(value_)];
}

- (int32_t)primitiveRemuneration_amountValue {
	NSNumber *result = [self primitiveRemuneration_amount];
	return [result intValue];
}

- (void)setPrimitiveRemuneration_amountValue:(int32_t)value_ {
	[self setPrimitiveRemuneration_amount:@(value_)];
}

@dynamic self_drive;

- (BOOL)self_driveValue {
	NSNumber *result = [self self_drive];
	return [result boolValue];
}

- (void)setSelf_driveValue:(BOOL)value_ {
	[self setSelf_drive:@(value_)];
}

- (BOOL)primitiveSelf_driveValue {
	NSNumber *result = [self primitiveSelf_drive];
	return [result boolValue];
}

- (void)setPrimitiveSelf_driveValue:(BOOL)value_ {
	[self setPrimitiveSelf_drive:@(value_)];
}

@dynamic thumbnail;

@dynamic to_date;

@dynamic total_bookmark;

- (int32_t)total_bookmarkValue {
	NSNumber *result = [self total_bookmark];
	return [result intValue];
}

- (void)setTotal_bookmarkValue:(int32_t)value_ {
	[self setTotal_bookmark:@(value_)];
}

- (int32_t)primitiveTotal_bookmarkValue {
	NSNumber *result = [self primitiveTotal_bookmark];
	return [result intValue];
}

- (void)setPrimitiveTotal_bookmarkValue:(int32_t)value_ {
	[self setPrimitiveTotal_bookmark:@(value_)];
}

@dynamic total_share;

- (int32_t)total_shareValue {
	NSNumber *result = [self total_share];
	return [result intValue];
}

- (void)setTotal_shareValue:(int32_t)value_ {
	[self setTotal_share:@(value_)];
}

- (int32_t)primitiveTotal_shareValue {
	NSNumber *result = [self primitiveTotal_share];
	return [result intValue];
}

- (void)setPrimitiveTotal_shareValue:(int32_t)value_ {
	[self setPrimitiveTotal_share:@(value_)];
}

@dynamic total_view;

- (int32_t)total_viewValue {
	NSNumber *result = [self total_view];
	return [result intValue];
}

- (void)setTotal_viewValue:(int32_t)value_ {
	[self setTotal_view:@(value_)];
}

- (int32_t)primitiveTotal_viewValue {
	NSNumber *result = [self primitiveTotal_view];
	return [result intValue];
}

- (void)setPrimitiveTotal_viewValue:(int32_t)value_ {
	[self setPrimitiveTotal_view:@(value_)];
}

@dynamic attractions;

- (NSMutableSet<Attractions*>*)attractionsSet {
	[self willAccessValueForKey:@"attractions"];

	NSMutableSet<Attractions*> *result = (NSMutableSet<Attractions*>*)[self mutableSetValueForKey:@"attractions"];

	[self didAccessValueForKey:@"attractions"];
	return result;
}

@dynamic bookmarkVideotour;

@dynamic userBookMark;

@end

@implementation BookmarkVideotourAttributes 
+ (NSString *)bookmark_users_string {
	return @"bookmark_users_string";
}
+ (NSString *)create_date {
	return @"create_date";
}
+ (NSString *)days {
	return @"days";
}
+ (NSString *)entity_id {
	return @"entity_id";
}
+ (NSString *)from_date {
	return @"from_date";
}
+ (NSString *)is_draft {
	return @"is_draft";
}
+ (NSString *)is_published {
	return @"is_published";
}
+ (NSString *)language {
	return @"language";
}
+ (NSString *)last_updated {
	return @"last_updated";
}
+ (NSString *)name {
	return @"name";
}
+ (NSString *)remuneration_amount {
	return @"remuneration_amount";
}
+ (NSString *)self_drive {
	return @"self_drive";
}
+ (NSString *)thumbnail {
	return @"thumbnail";
}
+ (NSString *)to_date {
	return @"to_date";
}
+ (NSString *)total_bookmark {
	return @"total_bookmark";
}
+ (NSString *)total_share {
	return @"total_share";
}
+ (NSString *)total_view {
	return @"total_view";
}
@end

@implementation BookmarkVideotourRelationships 
+ (NSString *)attractions {
	return @"attractions";
}
+ (NSString *)bookmarkVideotour {
	return @"bookmarkVideotour";
}
+ (NSString *)userBookMark {
	return @"userBookMark";
}
@end


// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MostPopularSearchVideoTour.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@interface MostPopularSearchVideoTourID : NSManagedObjectID {}
@end

@interface _MostPopularSearchVideoTour : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) MostPopularSearchVideoTourID *objectID;

@property (nonatomic, strong, nullable) NSString* entity_id;

@property (nonatomic, strong, nullable) NSString* language;

@property (nonatomic, strong, nullable) NSNumber* search_count;

@property (atomic) int32_t search_countValue;
- (int32_t)search_countValue;
- (void)setSearch_countValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSString* search_str;

@property (nonatomic, strong, nullable) NSString* type;

@property (nonatomic, strong, nullable) NSString* user;

@end

@interface _MostPopularSearchVideoTour (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveEntity_id;
- (void)setPrimitiveEntity_id:(nullable NSString*)value;

- (nullable NSString*)primitiveLanguage;
- (void)setPrimitiveLanguage:(nullable NSString*)value;

- (nullable NSNumber*)primitiveSearch_count;
- (void)setPrimitiveSearch_count:(nullable NSNumber*)value;

- (int32_t)primitiveSearch_countValue;
- (void)setPrimitiveSearch_countValue:(int32_t)value_;

- (nullable NSString*)primitiveSearch_str;
- (void)setPrimitiveSearch_str:(nullable NSString*)value;

- (nullable NSString*)primitiveUser;
- (void)setPrimitiveUser:(nullable NSString*)value;

@end

@interface MostPopularSearchVideoTourAttributes: NSObject 
+ (NSString *)entity_id;
+ (NSString *)language;
+ (NSString *)search_count;
+ (NSString *)search_str;
+ (NSString *)type;
+ (NSString *)user;
@end

NS_ASSUME_NONNULL_END

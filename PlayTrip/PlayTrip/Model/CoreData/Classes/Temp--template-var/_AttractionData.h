// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to AttractionData.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class Attractions;
@class DraftAttraction;
@class Info;
@class CommonUser;
@class VideoId;

@interface AttractionDataID : NSManagedObjectID {}
@end

@interface _AttractionData : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AttractionDataID *objectID;

@property (nonatomic, strong, nullable) NSString* desc;

@property (nonatomic, strong, nullable) NSString* entity_id;

@property (nonatomic, strong, nullable) NSString* time;

@property (nonatomic, strong, nullable) Attractions *attractionData;

@property (nonatomic, strong, nullable) DraftAttraction *draftAttraction;

@property (nonatomic, strong, nullable) Info *info;

@property (nonatomic, strong, nullable) CommonUser *userAttractionData;

@property (nonatomic, strong, nullable) VideoId *videoId;

@end

@interface _AttractionData (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveDesc;
- (void)setPrimitiveDesc:(nullable NSString*)value;

- (nullable NSString*)primitiveEntity_id;
- (void)setPrimitiveEntity_id:(nullable NSString*)value;

- (nullable NSString*)primitiveTime;
- (void)setPrimitiveTime:(nullable NSString*)value;

- (Attractions*)primitiveAttractionData;
- (void)setPrimitiveAttractionData:(Attractions*)value;

- (DraftAttraction*)primitiveDraftAttraction;
- (void)setPrimitiveDraftAttraction:(DraftAttraction*)value;

- (Info*)primitiveInfo;
- (void)setPrimitiveInfo:(Info*)value;

- (CommonUser*)primitiveUserAttractionData;
- (void)setPrimitiveUserAttractionData:(CommonUser*)value;

- (VideoId*)primitiveVideoId;
- (void)setPrimitiveVideoId:(VideoId*)value;

@end

@interface AttractionDataAttributes: NSObject 
+ (NSString *)desc;
+ (NSString *)entity_id;
+ (NSString *)time;
@end

@interface AttractionDataRelationships: NSObject
+ (NSString *)attractionData;
+ (NSString *)draftAttraction;
+ (NSString *)info;
+ (NSString *)userAttractionData;
+ (NSString *)videoId;
@end

NS_ASSUME_NONNULL_END

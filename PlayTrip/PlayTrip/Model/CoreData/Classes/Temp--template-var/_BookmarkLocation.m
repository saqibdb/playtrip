// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to BookmarkLocation.m instead.

#import "_BookmarkLocation.h"

@implementation BookmarkLocationID
@end

@implementation _BookmarkLocation

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"BookmarkLocation" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"BookmarkLocation";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"BookmarkLocation" inManagedObjectContext:moc_];
}

- (BookmarkLocationID*)objectID {
	return (BookmarkLocationID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic bookmarkLocation;

@end

@implementation BookmarkLocationRelationships 
+ (NSString *)bookmarkLocation {
	return @"bookmarkLocation";
}
@end


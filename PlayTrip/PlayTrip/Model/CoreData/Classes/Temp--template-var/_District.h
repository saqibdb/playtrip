// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to District.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@interface DistrictID : NSManagedObjectID {}
@end

@interface _District : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) DistrictID *objectID;

@property (nonatomic, strong, nullable) NSDate* create_date;

@property (nonatomic, strong, nullable) NSString* entity_id;

@property (nonatomic, strong, nullable) NSString* image;

@property (nonatomic, strong, nullable) NSString* longitude;

@property (nonatomic, strong, nullable) NSString* name;

@property (nonatomic, strong, nullable) NSNumber* plan_cnt;

@property (atomic) int32_t plan_cntValue;
- (int32_t)plan_cntValue;
- (void)setPlan_cntValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSString* status;

@end

@interface _District (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSDate*)primitiveCreate_date;
- (void)setPrimitiveCreate_date:(nullable NSDate*)value;

- (nullable NSString*)primitiveEntity_id;
- (void)setPrimitiveEntity_id:(nullable NSString*)value;

- (nullable NSString*)primitiveImage;
- (void)setPrimitiveImage:(nullable NSString*)value;

- (nullable NSString*)primitiveLongitude;
- (void)setPrimitiveLongitude:(nullable NSString*)value;

- (nullable NSString*)primitiveName;
- (void)setPrimitiveName:(nullable NSString*)value;

- (nullable NSNumber*)primitivePlan_cnt;
- (void)setPrimitivePlan_cnt:(nullable NSNumber*)value;

- (int32_t)primitivePlan_cntValue;
- (void)setPrimitivePlan_cntValue:(int32_t)value_;

- (nullable NSString*)primitiveStatus;
- (void)setPrimitiveStatus:(nullable NSString*)value;

@end

@interface DistrictAttributes: NSObject 
+ (NSString *)create_date;
+ (NSString *)entity_id;
+ (NSString *)image;
+ (NSString *)longitude;
+ (NSString *)name;
+ (NSString *)plan_cnt;
+ (NSString *)status;
@end

NS_ASSUME_NONNULL_END

// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Categori.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class Info;
@class MostPopularAttractions;

@interface CategoriID : NSManagedObjectID {}
@end

@interface _Categori : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) CategoriID *objectID;

@property (nonatomic, strong, nullable) NSString* desc;

@property (nonatomic, strong, nullable) NSString* entity_id;

@property (nonatomic, strong, nullable) NSString* image;

@property (nonatomic, strong, nullable) NSNumber* is_published;

@property (atomic) BOOL is_publishedValue;
- (BOOL)is_publishedValue;
- (void)setIs_publishedValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSDate* last_updated;

@property (nonatomic, strong, nullable) NSString* name;

@property (nonatomic, strong, nullable) Info *info;

@property (nonatomic, strong, nullable) MostPopularAttractions *mostPopularAttractions;

@end

@interface _Categori (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveDesc;
- (void)setPrimitiveDesc:(nullable NSString*)value;

- (nullable NSString*)primitiveEntity_id;
- (void)setPrimitiveEntity_id:(nullable NSString*)value;

- (nullable NSString*)primitiveImage;
- (void)setPrimitiveImage:(nullable NSString*)value;

- (nullable NSNumber*)primitiveIs_published;
- (void)setPrimitiveIs_published:(nullable NSNumber*)value;

- (BOOL)primitiveIs_publishedValue;
- (void)setPrimitiveIs_publishedValue:(BOOL)value_;

- (nullable NSDate*)primitiveLast_updated;
- (void)setPrimitiveLast_updated:(nullable NSDate*)value;

- (nullable NSString*)primitiveName;
- (void)setPrimitiveName:(nullable NSString*)value;

- (Info*)primitiveInfo;
- (void)setPrimitiveInfo:(Info*)value;

- (MostPopularAttractions*)primitiveMostPopularAttractions;
- (void)setPrimitiveMostPopularAttractions:(MostPopularAttractions*)value;

@end

@interface CategoriAttributes: NSObject 
+ (NSString *)desc;
+ (NSString *)entity_id;
+ (NSString *)image;
+ (NSString *)is_published;
+ (NSString *)last_updated;
+ (NSString *)name;
@end

@interface CategoriRelationships: NSObject
+ (NSString *)info;
+ (NSString *)mostPopularAttractions;
@end

NS_ASSUME_NONNULL_END

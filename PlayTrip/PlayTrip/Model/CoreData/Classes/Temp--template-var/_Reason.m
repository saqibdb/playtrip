// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Reason.m instead.

#import "_Reason.h"

@implementation ReasonID
@end

@implementation _Reason

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Reason" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Reason";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Reason" inManagedObjectContext:moc_];
}

- (ReasonID*)objectID {
	return (ReasonID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	return keyPaths;
}

@dynamic create_date;

@dynamic desc;

@dynamic entity_id;

@dynamic title;

@end

@implementation ReasonAttributes 
+ (NSString *)create_date {
	return @"create_date";
}
+ (NSString *)desc {
	return @"desc";
}
+ (NSString *)entity_id {
	return @"entity_id";
}
+ (NSString *)title {
	return @"title";
}
@end


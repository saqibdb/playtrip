// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to VideoId.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class AttractionData;

@interface VideoIdID : NSManagedObjectID {}
@end

@interface _VideoId : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) VideoIdID *objectID;

@property (nonatomic, strong, nullable) NSString* entity_id;

@property (nonatomic, strong, nullable) NSString* file_name;

@property (nonatomic, strong, nullable) NSString* thumbnail;

@property (nonatomic, strong, nullable) NSString* user;

@property (nonatomic, strong, nullable) AttractionData *attrData;

@end

@interface _VideoId (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveEntity_id;
- (void)setPrimitiveEntity_id:(nullable NSString*)value;

- (nullable NSString*)primitiveFile_name;
- (void)setPrimitiveFile_name:(nullable NSString*)value;

- (nullable NSString*)primitiveThumbnail;
- (void)setPrimitiveThumbnail:(nullable NSString*)value;

- (nullable NSString*)primitiveUser;
- (void)setPrimitiveUser:(nullable NSString*)value;

- (AttractionData*)primitiveAttrData;
- (void)setPrimitiveAttrData:(AttractionData*)value;

@end

@interface VideoIdAttributes: NSObject 
+ (NSString *)entity_id;
+ (NSString *)file_name;
+ (NSString *)thumbnail;
+ (NSString *)user;
@end

@interface VideoIdRelationships: NSObject
+ (NSString *)attrData;
@end

NS_ASSUME_NONNULL_END

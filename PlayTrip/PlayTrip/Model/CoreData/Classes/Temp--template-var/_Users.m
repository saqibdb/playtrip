// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Users.m instead.

#import "_Users.h"

@implementation UsersID
@end

@implementation _Users

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Users" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Users";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Users" inManagedObjectContext:moc_];
}

- (UsersID*)objectID {
	return (UsersID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"is_activeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"is_active"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"is_recommendedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"is_recommended"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"is_verifiedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"is_verified"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"logged_in_countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"logged_in_count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"total_bookmarkValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"total_bookmark"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"total_shareValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"total_share"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"total_viewValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"total_view"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic avatar;

@dynamic bookmark_users_string;

@dynamic cover_pic;

@dynamic create_date;

@dynamic email;

@dynamic entity_id;

@dynamic facebook_id;

@dynamic first_name;

@dynamic full_name;

@dynamic geotype;

@dynamic google_id;

@dynamic group_id;

@dynamic is_active;

- (BOOL)is_activeValue {
	NSNumber *result = [self is_active];
	return [result boolValue];
}

- (void)setIs_activeValue:(BOOL)value_ {
	[self setIs_active:@(value_)];
}

- (BOOL)primitiveIs_activeValue {
	NSNumber *result = [self primitiveIs_active];
	return [result boolValue];
}

- (void)setPrimitiveIs_activeValue:(BOOL)value_ {
	[self setPrimitiveIs_active:@(value_)];
}

@dynamic is_recommended;

- (BOOL)is_recommendedValue {
	NSNumber *result = [self is_recommended];
	return [result boolValue];
}

- (void)setIs_recommendedValue:(BOOL)value_ {
	[self setIs_recommended:@(value_)];
}

- (BOOL)primitiveIs_recommendedValue {
	NSNumber *result = [self primitiveIs_recommended];
	return [result boolValue];
}

- (void)setPrimitiveIs_recommendedValue:(BOOL)value_ {
	[self setPrimitiveIs_recommended:@(value_)];
}

@dynamic is_verified;

- (BOOL)is_verifiedValue {
	NSNumber *result = [self is_verified];
	return [result boolValue];
}

- (void)setIs_verifiedValue:(BOOL)value_ {
	[self setIs_verified:@(value_)];
}

- (BOOL)primitiveIs_verifiedValue {
	NSNumber *result = [self primitiveIs_verified];
	return [result boolValue];
}

- (void)setPrimitiveIs_verifiedValue:(BOOL)value_ {
	[self setPrimitiveIs_verified:@(value_)];
}

@dynamic last_logged_date;

@dynamic last_name;

@dynamic last_updated;

@dynamic logged_in_count;

- (int32_t)logged_in_countValue {
	NSNumber *result = [self logged_in_count];
	return [result intValue];
}

- (void)setLogged_in_countValue:(int32_t)value_ {
	[self setLogged_in_count:@(value_)];
}

- (int32_t)primitiveLogged_in_countValue {
	NSNumber *result = [self primitiveLogged_in_count];
	return [result intValue];
}

- (void)setPrimitiveLogged_in_countValue:(int32_t)value_ {
	[self setPrimitiveLogged_in_count:@(value_)];
}

@dynamic name;

@dynamic phone_number;

@dynamic provider;

@dynamic role;

@dynamic total_bookmark;

- (int32_t)total_bookmarkValue {
	NSNumber *result = [self total_bookmark];
	return [result intValue];
}

- (void)setTotal_bookmarkValue:(int32_t)value_ {
	[self setTotal_bookmark:@(value_)];
}

- (int32_t)primitiveTotal_bookmarkValue {
	NSNumber *result = [self primitiveTotal_bookmark];
	return [result intValue];
}

- (void)setPrimitiveTotal_bookmarkValue:(int32_t)value_ {
	[self setPrimitiveTotal_bookmark:@(value_)];
}

@dynamic total_share;

- (int32_t)total_shareValue {
	NSNumber *result = [self total_share];
	return [result intValue];
}

- (void)setTotal_shareValue:(int32_t)value_ {
	[self setTotal_share:@(value_)];
}

- (int32_t)primitiveTotal_shareValue {
	NSNumber *result = [self primitiveTotal_share];
	return [result intValue];
}

- (void)setPrimitiveTotal_shareValue:(int32_t)value_ {
	[self setPrimitiveTotal_share:@(value_)];
}

@dynamic total_view;

- (int32_t)total_viewValue {
	NSNumber *result = [self total_view];
	return [result intValue];
}

- (void)setTotal_viewValue:(int32_t)value_ {
	[self setTotal_view:@(value_)];
}

- (int32_t)primitiveTotal_viewValue {
	NSNumber *result = [self primitiveTotal_view];
	return [result intValue];
}

- (void)setPrimitiveTotal_viewValue:(int32_t)value_ {
	[self setPrimitiveTotal_view:@(value_)];
}

@dynamic user_name;

@end

@implementation UsersAttributes 
+ (NSString *)avatar {
	return @"avatar";
}
+ (NSString *)bookmark_users_string {
	return @"bookmark_users_string";
}
+ (NSString *)cover_pic {
	return @"cover_pic";
}
+ (NSString *)create_date {
	return @"create_date";
}
+ (NSString *)email {
	return @"email";
}
+ (NSString *)entity_id {
	return @"entity_id";
}
+ (NSString *)facebook_id {
	return @"facebook_id";
}
+ (NSString *)first_name {
	return @"first_name";
}
+ (NSString *)full_name {
	return @"full_name";
}
+ (NSString *)geotype {
	return @"geotype";
}
+ (NSString *)google_id {
	return @"google_id";
}
+ (NSString *)group_id {
	return @"group_id";
}
+ (NSString *)is_active {
	return @"is_active";
}
+ (NSString *)is_recommended {
	return @"is_recommended";
}
+ (NSString *)is_verified {
	return @"is_verified";
}
+ (NSString *)last_logged_date {
	return @"last_logged_date";
}
+ (NSString *)last_name {
	return @"last_name";
}
+ (NSString *)last_updated {
	return @"last_updated";
}
+ (NSString *)logged_in_count {
	return @"logged_in_count";
}
+ (NSString *)name {
	return @"name";
}
+ (NSString *)phone_number {
	return @"phone_number";
}
+ (NSString *)provider {
	return @"provider";
}
+ (NSString *)role {
	return @"role";
}
+ (NSString *)total_bookmark {
	return @"total_bookmark";
}
+ (NSString *)total_share {
	return @"total_share";
}
+ (NSString *)total_view {
	return @"total_view";
}
+ (NSString *)user_name {
	return @"user_name";
}
@end


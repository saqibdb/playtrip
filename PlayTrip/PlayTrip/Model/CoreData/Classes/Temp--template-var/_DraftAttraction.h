// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to DraftAttraction.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class AttractionData;
@class DraftAttrData;
@class DraftTour;

@interface DraftAttractionID : NSManagedObjectID {}
@end

@interface _DraftAttraction : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) DraftAttractionID *objectID;

@property (nonatomic, strong, nullable) NSString* address;

@property (nonatomic, strong, nullable) NSNumber* day;

@property (atomic) int32_t dayValue;
- (int32_t)dayValue;
- (void)setDayValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSString* detail;

@property (nonatomic, strong, nullable) NSString* entity_id;

@property (nonatomic, strong, nullable) NSNumber* loc_lat;

@property (atomic) double loc_latValue;
- (double)loc_latValue;
- (void)setLoc_latValue:(double)value_;

@property (nonatomic, strong, nullable) NSNumber* loc_lng;

@property (atomic) double loc_lngValue;
- (double)loc_lngValue;
- (void)setLoc_lngValue:(double)value_;

@property (nonatomic, strong, nullable) NSString* name;

@property (nonatomic, strong, nullable) NSString* phone_no;

@property (nonatomic, strong, nullable) NSString* web_url;

@property (nonatomic, strong, nullable) NSSet<AttractionData*> *attrData;
- (nullable NSMutableSet<AttractionData*>*)attrDataSet;

@property (nonatomic, strong, nullable) NSSet<DraftAttrData*> *draftAttrData;
- (nullable NSMutableSet<DraftAttrData*>*)draftAttrDataSet;

@property (nonatomic, strong, nullable) NSSet<DraftTour*> *draftTour;
- (nullable NSMutableSet<DraftTour*>*)draftTourSet;

@end

@interface _DraftAttraction (AttrDataCoreDataGeneratedAccessors)
- (void)addAttrData:(NSSet<AttractionData*>*)value_;
- (void)removeAttrData:(NSSet<AttractionData*>*)value_;
- (void)addAttrDataObject:(AttractionData*)value_;
- (void)removeAttrDataObject:(AttractionData*)value_;

@end

@interface _DraftAttraction (DraftAttrDataCoreDataGeneratedAccessors)
- (void)addDraftAttrData:(NSSet<DraftAttrData*>*)value_;
- (void)removeDraftAttrData:(NSSet<DraftAttrData*>*)value_;
- (void)addDraftAttrDataObject:(DraftAttrData*)value_;
- (void)removeDraftAttrDataObject:(DraftAttrData*)value_;

@end

@interface _DraftAttraction (DraftTourCoreDataGeneratedAccessors)
- (void)addDraftTour:(NSSet<DraftTour*>*)value_;
- (void)removeDraftTour:(NSSet<DraftTour*>*)value_;
- (void)addDraftTourObject:(DraftTour*)value_;
- (void)removeDraftTourObject:(DraftTour*)value_;

@end

@interface _DraftAttraction (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveAddress;
- (void)setPrimitiveAddress:(nullable NSString*)value;

- (nullable NSNumber*)primitiveDay;
- (void)setPrimitiveDay:(nullable NSNumber*)value;

- (int32_t)primitiveDayValue;
- (void)setPrimitiveDayValue:(int32_t)value_;

- (nullable NSString*)primitiveDetail;
- (void)setPrimitiveDetail:(nullable NSString*)value;

- (nullable NSString*)primitiveEntity_id;
- (void)setPrimitiveEntity_id:(nullable NSString*)value;

- (nullable NSNumber*)primitiveLoc_lat;
- (void)setPrimitiveLoc_lat:(nullable NSNumber*)value;

- (double)primitiveLoc_latValue;
- (void)setPrimitiveLoc_latValue:(double)value_;

- (nullable NSNumber*)primitiveLoc_lng;
- (void)setPrimitiveLoc_lng:(nullable NSNumber*)value;

- (double)primitiveLoc_lngValue;
- (void)setPrimitiveLoc_lngValue:(double)value_;

- (nullable NSString*)primitiveName;
- (void)setPrimitiveName:(nullable NSString*)value;

- (nullable NSString*)primitivePhone_no;
- (void)setPrimitivePhone_no:(nullable NSString*)value;

- (nullable NSString*)primitiveWeb_url;
- (void)setPrimitiveWeb_url:(nullable NSString*)value;

- (NSMutableSet<AttractionData*>*)primitiveAttrData;
- (void)setPrimitiveAttrData:(NSMutableSet<AttractionData*>*)value;

- (NSMutableSet<DraftAttrData*>*)primitiveDraftAttrData;
- (void)setPrimitiveDraftAttrData:(NSMutableSet<DraftAttrData*>*)value;

- (NSMutableSet<DraftTour*>*)primitiveDraftTour;
- (void)setPrimitiveDraftTour:(NSMutableSet<DraftTour*>*)value;

@end

@interface DraftAttractionAttributes: NSObject 
+ (NSString *)address;
+ (NSString *)day;
+ (NSString *)detail;
+ (NSString *)entity_id;
+ (NSString *)loc_lat;
+ (NSString *)loc_lng;
+ (NSString *)name;
+ (NSString *)phone_no;
+ (NSString *)web_url;
@end

@interface DraftAttractionRelationships: NSObject
+ (NSString *)attrData;
+ (NSString *)draftAttrData;
+ (NSString *)draftTour;
@end

NS_ASSUME_NONNULL_END

// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to DraftInfo.m instead.

#import "_DraftInfo.h"

@implementation DraftInfoID
@end

@implementation _DraftInfo

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"DraftInfo" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"DraftInfo";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"DraftInfo" inManagedObjectContext:moc_];
}

- (DraftInfoID*)objectID {
	return (DraftInfoID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"loc_latValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loc_lat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loc_longValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loc_long"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic address;

@dynamic detail;

@dynamic entity_id;

@dynamic geotype;

@dynamic language;

@dynamic loc_lat;

- (int32_t)loc_latValue {
	NSNumber *result = [self loc_lat];
	return [result intValue];
}

- (void)setLoc_latValue:(int32_t)value_ {
	[self setLoc_lat:@(value_)];
}

- (int32_t)primitiveLoc_latValue {
	NSNumber *result = [self primitiveLoc_lat];
	return [result intValue];
}

- (void)setPrimitiveLoc_latValue:(int32_t)value_ {
	[self setPrimitiveLoc_lat:@(value_)];
}

@dynamic loc_long;

- (double)loc_longValue {
	NSNumber *result = [self loc_long];
	return [result doubleValue];
}

- (void)setLoc_longValue:(double)value_ {
	[self setLoc_long:@(value_)];
}

- (double)primitiveLoc_longValue {
	NSNumber *result = [self primitiveLoc_long];
	return [result doubleValue];
}

- (void)setPrimitiveLoc_longValue:(double)value_ {
	[self setPrimitiveLoc_long:@(value_)];
}

@dynamic name;

@dynamic phone_no;

@dynamic web_url;

@dynamic draftAttrData;

@end

@implementation DraftInfoAttributes 
+ (NSString *)address {
	return @"address";
}
+ (NSString *)detail {
	return @"detail";
}
+ (NSString *)entity_id {
	return @"entity_id";
}
+ (NSString *)geotype {
	return @"geotype";
}
+ (NSString *)language {
	return @"language";
}
+ (NSString *)loc_lat {
	return @"loc_lat";
}
+ (NSString *)loc_long {
	return @"loc_long";
}
+ (NSString *)name {
	return @"name";
}
+ (NSString *)phone_no {
	return @"phone_no";
}
+ (NSString *)web_url {
	return @"web_url";
}
@end

@implementation DraftInfoRelationships 
+ (NSString *)draftAttrData {
	return @"draftAttrData";
}
@end


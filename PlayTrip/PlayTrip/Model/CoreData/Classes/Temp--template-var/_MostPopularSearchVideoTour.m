// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MostPopularSearchVideoTour.m instead.

#import "_MostPopularSearchVideoTour.h"

@implementation MostPopularSearchVideoTourID
@end

@implementation _MostPopularSearchVideoTour

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MostPopularSearchVideoTour" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MostPopularSearchVideoTour";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MostPopularSearchVideoTour" inManagedObjectContext:moc_];
}

- (MostPopularSearchVideoTourID*)objectID {
	return (MostPopularSearchVideoTourID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"search_countValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"search_count"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic entity_id;

@dynamic language;

@dynamic search_count;

- (int32_t)search_countValue {
	NSNumber *result = [self search_count];
	return [result intValue];
}

- (void)setSearch_countValue:(int32_t)value_ {
	[self setSearch_count:@(value_)];
}

- (int32_t)primitiveSearch_countValue {
	NSNumber *result = [self primitiveSearch_count];
	return [result intValue];
}

- (void)setPrimitiveSearch_countValue:(int32_t)value_ {
	[self setPrimitiveSearch_count:@(value_)];
}

@dynamic search_str;

@dynamic type;

@dynamic user;

@end

@implementation MostPopularSearchVideoTourAttributes 
+ (NSString *)entity_id {
	return @"entity_id";
}
+ (NSString *)language {
	return @"language";
}
+ (NSString *)search_count {
	return @"search_count";
}
+ (NSString *)search_str {
	return @"search_str";
}
+ (NSString *)type {
	return @"type";
}
+ (NSString *)user {
	return @"user";
}
@end


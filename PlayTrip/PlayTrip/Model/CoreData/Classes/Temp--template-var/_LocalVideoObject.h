// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to LocalVideoObject.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@interface LocalVideoObjectID : NSManagedObjectID {}
@end

@interface _LocalVideoObject : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) LocalVideoObjectID *objectID;

@property (nonatomic, strong, nullable) NSString* addedAttractions;

@property (nonatomic, strong, nullable) NSString* attrDataId;

@property (nonatomic, strong, nullable) NSString* desc;

@property (nonatomic, strong, nullable) NSString* entity_id;

@property (nonatomic, strong, nullable) NSString* isSelected;

@property (nonatomic, strong, nullable) NSString* isUploading;

@property (nonatomic, strong, nullable) NSData* thumbnail;

@property (nonatomic, strong, nullable) NSString* time;

@property (nonatomic, strong, nullable) NSString* vidPath;

@property (nonatomic, strong, nullable) NSString* videoTourId;

@end

@interface _LocalVideoObject (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveAddedAttractions;
- (void)setPrimitiveAddedAttractions:(nullable NSString*)value;

- (nullable NSString*)primitiveAttrDataId;
- (void)setPrimitiveAttrDataId:(nullable NSString*)value;

- (nullable NSString*)primitiveDesc;
- (void)setPrimitiveDesc:(nullable NSString*)value;

- (nullable NSString*)primitiveEntity_id;
- (void)setPrimitiveEntity_id:(nullable NSString*)value;

- (nullable NSString*)primitiveIsSelected;
- (void)setPrimitiveIsSelected:(nullable NSString*)value;

- (nullable NSString*)primitiveIsUploading;
- (void)setPrimitiveIsUploading:(nullable NSString*)value;

- (nullable NSData*)primitiveThumbnail;
- (void)setPrimitiveThumbnail:(nullable NSData*)value;

- (nullable NSString*)primitiveTime;
- (void)setPrimitiveTime:(nullable NSString*)value;

- (nullable NSString*)primitiveVidPath;
- (void)setPrimitiveVidPath:(nullable NSString*)value;

- (nullable NSString*)primitiveVideoTourId;
- (void)setPrimitiveVideoTourId:(nullable NSString*)value;

@end

@interface LocalVideoObjectAttributes: NSObject 
+ (NSString *)addedAttractions;
+ (NSString *)attrDataId;
+ (NSString *)desc;
+ (NSString *)entity_id;
+ (NSString *)isSelected;
+ (NSString *)isUploading;
+ (NSString *)thumbnail;
+ (NSString *)time;
+ (NSString *)vidPath;
+ (NSString *)videoTourId;
@end

NS_ASSUME_NONNULL_END

// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to DraftAttraction.m instead.

#import "_DraftAttraction.h"

@implementation DraftAttractionID
@end

@implementation _DraftAttraction

+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"DraftAttraction" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"DraftAttraction";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"DraftAttraction" inManagedObjectContext:moc_];
}

- (DraftAttractionID*)objectID {
	return (DraftAttractionID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"dayValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"day"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loc_latValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loc_lat"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"loc_lngValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"loc_lng"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic address;

@dynamic day;

- (int32_t)dayValue {
	NSNumber *result = [self day];
	return [result intValue];
}

- (void)setDayValue:(int32_t)value_ {
	[self setDay:@(value_)];
}

- (int32_t)primitiveDayValue {
	NSNumber *result = [self primitiveDay];
	return [result intValue];
}

- (void)setPrimitiveDayValue:(int32_t)value_ {
	[self setPrimitiveDay:@(value_)];
}

@dynamic detail;

@dynamic entity_id;

@dynamic loc_lat;

- (double)loc_latValue {
	NSNumber *result = [self loc_lat];
	return [result doubleValue];
}

- (void)setLoc_latValue:(double)value_ {
	[self setLoc_lat:@(value_)];
}

- (double)primitiveLoc_latValue {
	NSNumber *result = [self primitiveLoc_lat];
	return [result doubleValue];
}

- (void)setPrimitiveLoc_latValue:(double)value_ {
	[self setPrimitiveLoc_lat:@(value_)];
}

@dynamic loc_lng;

- (double)loc_lngValue {
	NSNumber *result = [self loc_lng];
	return [result doubleValue];
}

- (void)setLoc_lngValue:(double)value_ {
	[self setLoc_lng:@(value_)];
}

- (double)primitiveLoc_lngValue {
	NSNumber *result = [self primitiveLoc_lng];
	return [result doubleValue];
}

- (void)setPrimitiveLoc_lngValue:(double)value_ {
	[self setPrimitiveLoc_lng:@(value_)];
}

@dynamic name;

@dynamic phone_no;

@dynamic web_url;

@dynamic attrData;

- (NSMutableSet<AttractionData*>*)attrDataSet {
	[self willAccessValueForKey:@"attrData"];

	NSMutableSet<AttractionData*> *result = (NSMutableSet<AttractionData*>*)[self mutableSetValueForKey:@"attrData"];

	[self didAccessValueForKey:@"attrData"];
	return result;
}

@dynamic draftAttrData;

- (NSMutableSet<DraftAttrData*>*)draftAttrDataSet {
	[self willAccessValueForKey:@"draftAttrData"];

	NSMutableSet<DraftAttrData*> *result = (NSMutableSet<DraftAttrData*>*)[self mutableSetValueForKey:@"draftAttrData"];

	[self didAccessValueForKey:@"draftAttrData"];
	return result;
}

@dynamic draftTour;

- (NSMutableSet<DraftTour*>*)draftTourSet {
	[self willAccessValueForKey:@"draftTour"];

	NSMutableSet<DraftTour*> *result = (NSMutableSet<DraftTour*>*)[self mutableSetValueForKey:@"draftTour"];

	[self didAccessValueForKey:@"draftTour"];
	return result;
}

@end

@implementation DraftAttractionAttributes 
+ (NSString *)address {
	return @"address";
}
+ (NSString *)day {
	return @"day";
}
+ (NSString *)detail {
	return @"detail";
}
+ (NSString *)entity_id {
	return @"entity_id";
}
+ (NSString *)loc_lat {
	return @"loc_lat";
}
+ (NSString *)loc_lng {
	return @"loc_lng";
}
+ (NSString *)name {
	return @"name";
}
+ (NSString *)phone_no {
	return @"phone_no";
}
+ (NSString *)web_url {
	return @"web_url";
}
@end

@implementation DraftAttractionRelationships 
+ (NSString *)attrData {
	return @"attrData";
}
+ (NSString *)draftAttrData {
	return @"draftAttrData";
}
+ (NSString *)draftTour {
	return @"draftTour";
}
@end


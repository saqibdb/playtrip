// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Attractions.h instead.

#if __has_feature(modules)
    @import Foundation;
    @import CoreData;
#else
    #import <Foundation/Foundation.h>
    #import <CoreData/CoreData.h>
#endif

NS_ASSUME_NONNULL_BEGIN

@class AttractionData;
@class BookmarkVideotour;
@class DraftTour;
@class MostPopularPlan;
@class MostPopularVideoTour;
@class Plan;
@class CommonUser;
@class VideoTour;

@interface AttractionsID : NSManagedObjectID {}
@end

@interface _Attractions : NSManagedObject
+ (instancetype)insertInManagedObjectContext:(NSManagedObjectContext *)moc_;
+ (NSString*)entityName;
+ (nullable NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) AttractionsID *objectID;

@property (nonatomic, strong, nullable) NSString* address;

@property (nonatomic, strong, nullable) NSString* bookmark_users_string;

@property (nonatomic, strong, nullable) NSDate* create_date;

@property (nonatomic, strong, nullable) NSNumber* day;

@property (atomic) int32_t dayValue;
- (int32_t)dayValue;
- (void)setDayValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSString* detail;

@property (nonatomic, strong, nullable) NSString* entity_id;

@property (nonatomic, strong, nullable) NSNumber* has_remark;

@property (atomic) BOOL has_remarkValue;
- (BOOL)has_remarkValue;
- (void)setHas_remarkValue:(BOOL)value_;

@property (nonatomic, strong, nullable) NSDate* last_updated;

@property (nonatomic, strong, nullable) NSNumber* loc_lat;

@property (atomic) double loc_latValue;
- (double)loc_latValue;
- (void)setLoc_latValue:(double)value_;

@property (nonatomic, strong, nullable) NSNumber* loc_long;

@property (atomic) double loc_longValue;
- (double)loc_longValue;
- (void)setLoc_longValue:(double)value_;

@property (nonatomic, strong, nullable) NSString* name;

@property (nonatomic, strong, nullable) NSString* phone_no;

@property (nonatomic, strong, nullable) NSString* plan;

@property (nonatomic, strong, nullable) NSNumber* total_bookmark;

@property (atomic) int32_t total_bookmarkValue;
- (int32_t)total_bookmarkValue;
- (void)setTotal_bookmarkValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSNumber* total_share;

@property (atomic) int32_t total_shareValue;
- (int32_t)total_shareValue;
- (void)setTotal_shareValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSNumber* total_view;

@property (atomic) int32_t total_viewValue;
- (int32_t)total_viewValue;
- (void)setTotal_viewValue:(int32_t)value_;

@property (nonatomic, strong, nullable) NSString* web_url;

@property (nonatomic, strong, nullable) NSSet<AttractionData*> *attractionData;
- (nullable NSMutableSet<AttractionData*>*)attractionDataSet;

@property (nonatomic, strong, nullable) BookmarkVideotour *bookmarkVideoTour;

@property (nonatomic, strong, nullable) DraftTour *draftTour;

@property (nonatomic, strong, nullable) MostPopularPlan *mostPopularPlan;

@property (nonatomic, strong, nullable) MostPopularVideoTour *mostPopularVideoTour;

@property (nonatomic, strong, nullable) Plan *plans;

@property (nonatomic, strong, nullable) CommonUser *userAttraction;

@property (nonatomic, strong, nullable) VideoTour *videoTour;

@end

@interface _Attractions (AttractionDataCoreDataGeneratedAccessors)
- (void)addAttractionData:(NSSet<AttractionData*>*)value_;
- (void)removeAttractionData:(NSSet<AttractionData*>*)value_;
- (void)addAttractionDataObject:(AttractionData*)value_;
- (void)removeAttractionDataObject:(AttractionData*)value_;

@end

@interface _Attractions (CoreDataGeneratedPrimitiveAccessors)

- (nullable NSString*)primitiveAddress;
- (void)setPrimitiveAddress:(nullable NSString*)value;

- (nullable NSString*)primitiveBookmark_users_string;
- (void)setPrimitiveBookmark_users_string:(nullable NSString*)value;

- (nullable NSDate*)primitiveCreate_date;
- (void)setPrimitiveCreate_date:(nullable NSDate*)value;

- (nullable NSNumber*)primitiveDay;
- (void)setPrimitiveDay:(nullable NSNumber*)value;

- (int32_t)primitiveDayValue;
- (void)setPrimitiveDayValue:(int32_t)value_;

- (nullable NSString*)primitiveDetail;
- (void)setPrimitiveDetail:(nullable NSString*)value;

- (nullable NSString*)primitiveEntity_id;
- (void)setPrimitiveEntity_id:(nullable NSString*)value;

- (nullable NSNumber*)primitiveHas_remark;
- (void)setPrimitiveHas_remark:(nullable NSNumber*)value;

- (BOOL)primitiveHas_remarkValue;
- (void)setPrimitiveHas_remarkValue:(BOOL)value_;

- (nullable NSDate*)primitiveLast_updated;
- (void)setPrimitiveLast_updated:(nullable NSDate*)value;

- (nullable NSNumber*)primitiveLoc_lat;
- (void)setPrimitiveLoc_lat:(nullable NSNumber*)value;

- (double)primitiveLoc_latValue;
- (void)setPrimitiveLoc_latValue:(double)value_;

- (nullable NSNumber*)primitiveLoc_long;
- (void)setPrimitiveLoc_long:(nullable NSNumber*)value;

- (double)primitiveLoc_longValue;
- (void)setPrimitiveLoc_longValue:(double)value_;

- (nullable NSString*)primitiveName;
- (void)setPrimitiveName:(nullable NSString*)value;

- (nullable NSString*)primitivePhone_no;
- (void)setPrimitivePhone_no:(nullable NSString*)value;

- (nullable NSString*)primitivePlan;
- (void)setPrimitivePlan:(nullable NSString*)value;

- (nullable NSNumber*)primitiveTotal_bookmark;
- (void)setPrimitiveTotal_bookmark:(nullable NSNumber*)value;

- (int32_t)primitiveTotal_bookmarkValue;
- (void)setPrimitiveTotal_bookmarkValue:(int32_t)value_;

- (nullable NSNumber*)primitiveTotal_share;
- (void)setPrimitiveTotal_share:(nullable NSNumber*)value;

- (int32_t)primitiveTotal_shareValue;
- (void)setPrimitiveTotal_shareValue:(int32_t)value_;

- (nullable NSNumber*)primitiveTotal_view;
- (void)setPrimitiveTotal_view:(nullable NSNumber*)value;

- (int32_t)primitiveTotal_viewValue;
- (void)setPrimitiveTotal_viewValue:(int32_t)value_;

- (nullable NSString*)primitiveWeb_url;
- (void)setPrimitiveWeb_url:(nullable NSString*)value;

- (NSMutableSet<AttractionData*>*)primitiveAttractionData;
- (void)setPrimitiveAttractionData:(NSMutableSet<AttractionData*>*)value;

- (BookmarkVideotour*)primitiveBookmarkVideoTour;
- (void)setPrimitiveBookmarkVideoTour:(BookmarkVideotour*)value;

- (DraftTour*)primitiveDraftTour;
- (void)setPrimitiveDraftTour:(DraftTour*)value;

- (MostPopularPlan*)primitiveMostPopularPlan;
- (void)setPrimitiveMostPopularPlan:(MostPopularPlan*)value;

- (MostPopularVideoTour*)primitiveMostPopularVideoTour;
- (void)setPrimitiveMostPopularVideoTour:(MostPopularVideoTour*)value;

- (Plan*)primitivePlans;
- (void)setPrimitivePlans:(Plan*)value;

- (CommonUser*)primitiveUserAttraction;
- (void)setPrimitiveUserAttraction:(CommonUser*)value;

- (VideoTour*)primitiveVideoTour;
- (void)setPrimitiveVideoTour:(VideoTour*)value;

@end

@interface AttractionsAttributes: NSObject 
+ (NSString *)address;
+ (NSString *)bookmark_users_string;
+ (NSString *)create_date;
+ (NSString *)day;
+ (NSString *)detail;
+ (NSString *)entity_id;
+ (NSString *)has_remark;
+ (NSString *)last_updated;
+ (NSString *)loc_lat;
+ (NSString *)loc_long;
+ (NSString *)name;
+ (NSString *)phone_no;
+ (NSString *)plan;
+ (NSString *)total_bookmark;
+ (NSString *)total_share;
+ (NSString *)total_view;
+ (NSString *)web_url;
@end

@interface AttractionsRelationships: NSObject
+ (NSString *)attractionData;
+ (NSString *)bookmarkVideoTour;
+ (NSString *)draftTour;
+ (NSString *)mostPopularPlan;
+ (NSString *)mostPopularVideoTour;
+ (NSString *)plans;
+ (NSString *)userAttraction;
+ (NSString *)videoTour;
@end

NS_ASSUME_NONNULL_END

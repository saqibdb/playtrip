
#import "_Reason.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>

@interface Reason : _Reason

+(FEMMapping *)defaultMapping ;
+(NSMutableArray *)getAll;

@end

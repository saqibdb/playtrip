#import "VideoTour.h"
#import "CDHelper.h"
#import "CommonUser.h"
#import "NSManagedObject+Mapping.h"
#import "Attractions.h"
#import "Users.h"

@interface VideoTour ()
@end

@implementation VideoTour

+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[VideoTour entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[VideoTour class]] mutableCopy];
    
    [dict removeObjectForKey:@"create_date"];
    [dict removeObjectForKey:@"from_date"];
    [dict removeObjectForKey:@"last_updated"];
    [dict removeObjectForKey:@"to_date"];
    
    [mapping addAttributesFromDictionary:dict];
    
    [mapping addAttribute:[VideoTour dateTimeAttributeFor:@"from_date" andKeyPath:@"from_date"]];
    [mapping addAttribute:[VideoTour dateTimeAttributeFor:@"create_date" andKeyPath:@"create_date"]];
    [mapping addAttribute:[VideoTour dateTimeAttributeFor:@"last_updated" andKeyPath:@"last_updated"]];
    [mapping addAttribute:[VideoTour dateTimeAttributeFor:@"to_date" andKeyPath:@"to_date"]];
    [mapping setPrimaryKey:@"entity_id"];
    
  
    [mapping addToManyRelationshipMapping:[Attractions defaultMapping] forProperty:@"attractions" keyPath:@"attractions"];
    [mapping addRelationshipMapping:[CommonUser defaultMapping] forProperty:@"user" keyPath:@"user"];
    
    return mapping;
}

+(NSMutableArray *)getAll {
    return [[VideoTour  MR_findAllSortedBy:@"from_date" ascending:true] mutableCopy];
}

+(NSMutableArray *)getAllUser:(CommonUser *)user {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(user == %@)",user];
    return [[CommonUser MR_findAllWithPredicate:predicate] mutableCopy];
    
}

+(VideoTour *)getByEntityId:(NSString *)entityId {
    NSPredicate *pre = [NSPredicate predicateWithFormat:@"entity_id == %@", entityId];
    return [VideoTour MR_findFirstWithPredicate:pre];
}

+(NSMutableArray *)getAllByTimeAsc:(BOOL)ascBool {
    return [[VideoTour MR_findAllSortedBy:@"create_date" ascending:ascBool] mutableCopy];
}

+(NSMutableArray *)getAllByBoookmarkAsc:(BOOL)ascBool {
    return [[VideoTour MR_findAllSortedBy:@"total_bookmark" ascending:ascBool] mutableCopy];
}

+(NSMutableArray *)getAllByViewAsc:(BOOL)ascBool {
    return [[VideoTour MR_findAllSortedBy:@"total_view" ascending:ascBool] mutableCopy];
}

+(NSMutableArray *)getAllByShareAsc:(BOOL)ascBool {
    return [[VideoTour MR_findAllSortedBy:@"total_share" ascending:ascBool] mutableCopy];
}

+(NSMutableArray *)getAllByAmountAsc:(BOOL)ascBool {
    return [[VideoTour MR_findAllSortedBy:@"remuneration_amount" ascending:ascBool] mutableCopy];
}

+(NSMutableArray *)getAllByAttrCountAsc:(BOOL)ascBool {
    NSMutableArray *arrData = [[NSMutableArray alloc] init];
    NSMutableArray *outArray = [NSMutableArray new];
    
    for (VideoTour *vt in [self MR_findAll].mutableCopy) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setValue:vt.entity_id forKey:@"entityId"];
        [dict setValue:[NSNumber numberWithInteger:[vt.attractions allObjects].count] forKey:@"count"];
        [arrData addObject:dict];
    }
    NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"count" ascending:ascBool];
    NSArray *arrNew = [arrData sortedArrayUsingDescriptors:[NSArray arrayWithObject:sd]];
    
    for (NSMutableArray * aArray in arrNew) {
        NSPredicate *pre = [NSPredicate predicateWithFormat:@"entity_id == %@", [aArray valueForKey:@"entityId"]];
        VideoTour * vT =  [VideoTour MR_findFirstWithPredicate:pre];
        [outArray addObject:vT];
    }
    
    return outArray;
}

+(NSMutableArray *)getArractForId:(NSString *)eId {
    NSPredicate *pre = [NSPredicate predicateWithFormat:@"entity_id == %@", eId];
    VideoTour * vT =  [VideoTour MR_findFirstWithPredicate:pre];
    return [[vT.attractions allObjects] mutableCopy];;
}

+(VideoTour *)getvideTourByName:(NSString *)name {
    NSPredicate *pre = [NSPredicate predicateWithFormat:@"name == %@", name];
    VideoTour * vT =  [VideoTour MR_findFirstWithPredicate:pre];
    return vT;
}

+(NSMutableArray *)getAllByAddToPlanAsc:(BOOL)ascBool { // Arun :: It is remaining. for now sorting by name. When implemented, just replace the "name" with actual parameter.
    return [[VideoTour MR_findAllSortedBy:@"name" ascending:ascBool] mutableCopy];
}


@end

#import "MostPopularSearchVideoTour.h"
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"
@interface MostPopularSearchVideoTour ()
@end

@implementation MostPopularSearchVideoTour
+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[MostPopularSearchVideoTour entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[MostPopularSearchVideoTour class]] mutableCopy];
    [mapping addAttributesFromDictionary:dict];
    [mapping setPrimaryKey:@"entity_id"];
    return mapping;
}

+(NSMutableArray *)getAll {
    return [[MostPopularSearchVideoTour MR_findAllSortedBy:@"search_str" ascending:true] mutableCopy];
}
@end

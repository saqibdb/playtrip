#import "_Info.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>


@interface Info : _Info

+(FEMMapping *)defaultMapping;
+(NSMutableArray *)getAll;
+(NSMutableArray *)getAllByTimeAsc:(BOOL)ascBool;
+(NSMutableArray *)getAllByBoookmarkAsc:(BOOL)ascBool;
+(NSMutableArray *)getAllByViewAsc:(BOOL)ascBool;
+(NSMutableArray *)getAllByShareAsc:(BOOL)ascBool;
+(NSMutableArray *)getAllByAddToPlanAsc:(BOOL)ascBool;


@end

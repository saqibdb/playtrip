
#import "_AttractionData.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>

@interface AttractionData : _AttractionData

+(FEMMapping *)defaultMapping;
+(NSMutableArray *)getAll;
+(AttractionData *)getByEntityId:(NSString *)entityId;
+(NSMutableArray *)getAllByTimeAsc:(BOOL)ascBool;
+(NSMutableArray *)getAllByBoookmarkAsc:(BOOL)ascBool;
+(NSMutableArray *)getAllByViewAsc:(BOOL)ascBool;
+(NSMutableArray *)getAllByShareAsc:(BOOL)ascBool;
+(NSMutableArray *)getAllByAddToPlanAsc:(BOOL)ascBool;

@end

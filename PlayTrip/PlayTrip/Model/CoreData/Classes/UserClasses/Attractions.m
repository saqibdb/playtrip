
#import "Attractions.h"
#import "CDHelper.h"
#import "CommonUser.h"
#import "NSManagedObject+Mapping.h"
#import "Categori.h"
#import "AttractionData.h"
#import "Users.h"

@interface Attractions ()
@end

@implementation Attractions

+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[Attractions entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[Attractions class]] mutableCopy];
    
    [dict removeObjectForKey:@"create_date"];
    [dict removeObjectForKey:@"last_updated"];
   
    [mapping addAttributesFromDictionary:dict];
    
    [mapping addAttribute:[Attractions dateTimeAttributeFor:@"create_date" andKeyPath:@"create_date"]];
    [mapping addAttribute:[Attractions dateTimeAttributeFor:@"last_updated" andKeyPath:@"last_updated"]];

    [mapping addToManyRelationshipMapping:[AttractionData defaultMapping] forProperty:@"attractionData" keyPath:@"attr_data"];
    [mapping addRelationshipMapping:[CommonUser defaultMapping] forProperty:@"userAttraction" keyPath:@"userAttraction"];

    return mapping;
}


+(void)saveEntity{
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

+(NSMutableArray *)getAll {
    return [[Attractions MR_findAll]mutableCopy];
}

+(Attractions *)getByEntityId:(NSString *)entityId {
    NSPredicate *pre = [NSPredicate predicateWithFormat:@"entity_id == %@", entityId];
    return [Attractions MR_findFirstWithPredicate:pre];
}

+(NSMutableArray *)getAttractionByday:(NSString *)day WithPlanId:(NSString *)pid{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(day == %@) AND (plan == %@)",day,pid];
    return [[Attractions MR_findAllWithPredicate:predicate] mutableCopy];
}

+(NSMutableArray *)getListByName:(NSString *)name{
    NSMutableArray * finalArray = [NSMutableArray new];
    NSMutableArray * localArray = [Attractions getAll];
    for (Attractions * obj in localArray) {
        if (!([obj.name rangeOfString:name options:NSCaseInsensitiveSearch].location == NSNotFound)){
            [finalArray addObject:obj];
        }
    }
    return finalArray;
}

@end

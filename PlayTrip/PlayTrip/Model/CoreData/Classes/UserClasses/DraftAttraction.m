#import "DraftAttraction.h"
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"
#import "AttractionData.h"
#import "DraftTour.h"
#import "DraftAttrData.h"

@interface DraftAttraction ()
@end

@implementation DraftAttraction

+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[DraftAttraction entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[DraftAttraction class]] mutableCopy];
    [mapping addAttributesFromDictionary:dict];

    [mapping addToManyRelationshipMapping:[AttractionData defaultMapping] forProperty:@"attrData" keyPath:@"attrData"];
    [mapping addToManyRelationshipMapping:[DraftAttrData defaultMapping] forProperty:@"draftAttrData" keyPath:@"draftAttrData"];
    [mapping addRelationshipMapping:[DraftTour defaultMapping] forProperty:@"draftTour" keyPath:@"draftTour"];
    
    return mapping;
}

+(NSMutableArray *)getAll {
    return [[DraftAttraction MR_findAll] mutableCopy];
}

+(DraftAttraction *)getByEntityId:(NSString *)entityId {
    NSPredicate *pre = [NSPredicate predicateWithFormat:@"entity_id == %@", entityId];
    return [DraftAttraction MR_findFirstWithPredicate:pre];
}

+(DraftAttraction *)getByDay:(NSString *)day {
    NSPredicate *pre = [NSPredicate predicateWithFormat:@"day == %@", day];
    return [DraftAttraction MR_findFirstWithPredicate:pre];
}

+(DraftAttraction *)newEntity{
    DraftAttraction *new = [DraftAttraction MR_createEntityInContext:[NSManagedObjectContext MR_defaultContext]];
    return new;
}

+(void)saveEntity{
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

+(void)deleteObj:(DraftAttraction *)mainObj {
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        [mainObj MR_deleteEntityInContext:localContext];
    }];
}

+(void)deleteAll {
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        [DraftAttraction MR_truncateAllInContext:localContext];
    }];
}

@end


#import "District.h"
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"

@interface District ()
@end

@implementation District

+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[District entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[District class]] mutableCopy];
    
    [dict removeObjectForKey:@"create_date"];
    
    [mapping addAttributesFromDictionary:dict];
    
    [mapping addAttribute:[District dateTimeAttributeFor:@"create_date" andKeyPath:@"create_date"]];
    
    [mapping setPrimaryKey:@"entity_id"];
    
    return mapping;
}

+(NSMutableArray *)getAll {
    return [[District MR_findAllSortedBy:@"name" ascending:true] mutableCopy];
}

@end

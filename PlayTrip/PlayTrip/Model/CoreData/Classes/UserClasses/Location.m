#import "Location.h"
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"
@interface Location ()

// Private interface goes here.

@end

@implementation Location

+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[Location entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[Location class]] mutableCopy];
    
    
    
    [mapping addAttributesFromDictionary:dict];
    
    
    return mapping;
}

@end

#import "_MostPopularSearchAttraction.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>
@interface MostPopularSearchAttraction : _MostPopularSearchAttraction
+(FEMMapping *)defaultMapping;
+(NSMutableArray *)getAll;
@end

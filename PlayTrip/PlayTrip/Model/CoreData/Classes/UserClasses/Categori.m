#import "Categori.h"
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"

@interface Categori ()
@end

@implementation Categori
+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[Categori entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[Categori class]] mutableCopy];
    
//    [dict removeObjectForKey:@"create_date"];
    [dict removeObjectForKey:@"last_updated"];
   
    
    [mapping addAttributesFromDictionary:dict];
    
//    [mapping addAttribute:[Categori dateTimeAttributeFor:@"create_date" andKeyPath:@"create_date"]];
    [mapping addAttribute:[Categori dateTimeAttributeFor:@"last_updated" andKeyPath:@"last_updated"]];
    
//    [mapping setPrimaryKey:@"entity_id"];
    
    return mapping;
}

+(NSMutableArray *)getAll {
    return [[Categori MR_findAllSortedBy:@"name" ascending:true] mutableCopy];
}

+(Categori *)getByEntityId:(NSString *)entityId {
    NSPredicate *pre = [NSPredicate predicateWithFormat:@"entity_id == %@", entityId];
    return [Categori MR_findFirstWithPredicate:pre];
}

@end

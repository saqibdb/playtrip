
#import "_DraftInfo.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>

@interface DraftInfo : _DraftInfo

+(FEMMapping *)defaultMapping;
+(NSMutableArray *)getAll;
+(DraftInfo *)newEntity;
+(void)saveEntity;
+(void)deleteObj:(DraftInfo *)mainObj;
+(void)deleteAll;

@end

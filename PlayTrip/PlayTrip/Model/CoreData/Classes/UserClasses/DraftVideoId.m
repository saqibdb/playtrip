
#import "DraftVideoId.h"
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"

@interface DraftVideoId ()
@end

@implementation DraftVideoId

+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[DraftVideoId entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[DraftVideoId class]] mutableCopy];
    [mapping addAttributesFromDictionary:dict];
    
    return mapping;
}

+(NSMutableArray *)getAll {
    return [[DraftVideoId MR_findAll] mutableCopy];
}

+(DraftVideoId *)newEntity{
    DraftVideoId *new = [DraftVideoId MR_createEntityInContext:[NSManagedObjectContext MR_defaultContext]];
    return new;
}

+(void)saveEntity{
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

+(void)deleteObj:(DraftVideoId *)mainObj {
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        [mainObj MR_deleteEntityInContext:localContext];
    }];
}

+(void)deleteAll {
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        [DraftVideoId MR_truncateAllInContext:localContext];
    }];
}

@end

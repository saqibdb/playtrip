
#import "_LocalAttrTags.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>

@interface LocalAttrTags : _LocalAttrTags

+(FEMMapping *)defaultMapping;
+(NSMutableArray *)getAll;
+(LocalAttrTags *)getByEntityId:(NSString *)entityId;
+(LocalAttrTags *)newEntity;
+(void)saveEntity;

@end


#import "_DraftAttraction.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>

@interface DraftAttraction : _DraftAttraction

+(FEMMapping *)defaultMapping;
+(NSMutableArray *)getAll;
+(DraftAttraction *)newEntity;
+(void)saveEntity;
+(void)deleteObj:(DraftAttraction *)mainObj;
+(DraftAttraction *)getByEntityId:(NSString *)entityId;
+(DraftAttraction *)getByDay:(NSString *)day;
+(void)deleteAll;

@end

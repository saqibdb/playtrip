#import "MostPopularSearchAttraction.h"
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"
@interface MostPopularSearchAttraction ()
@end

@implementation MostPopularSearchAttraction
+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[MostPopularSearchAttraction entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[MostPopularSearchAttraction class]] mutableCopy];
    [mapping addAttributesFromDictionary:dict];
    [mapping setPrimaryKey:@"entity_id"];
    return mapping;
}

+(NSMutableArray *)getAll {
    return [[MostPopularSearchAttraction MR_findAllSortedBy:@"search_str" ascending:true] mutableCopy];
}

@end


#import "DraftAttrData.h"
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"
#import "DraftInfo.h"
#import "DraftVideoId.h"

@interface DraftAttrData ()
@end

@implementation DraftAttrData

+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[DraftAttrData entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[DraftAttrData class]] mutableCopy];
    [mapping addAttributesFromDictionary:dict];
    
    [mapping addRelationshipMapping:[DraftInfo defaultMapping] forProperty:@"draftInfo" keyPath:@"draftInfo"];
    [mapping addRelationshipMapping:[DraftVideoId defaultMapping] forProperty:@"draftVideoId" keyPath:@"draftVideoId"];
    
    return mapping;
}

+(NSMutableArray *)getAll {
    return [[DraftAttrData MR_findAll] mutableCopy];
}

+(DraftAttrData *)newEntity{
    DraftAttrData *new = [DraftAttrData MR_createEntityInContext:[NSManagedObjectContext MR_defaultContext]];
    return new;
}

+(void)saveEntity{
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

+(void)deleteObj:(DraftAttrData *)mainObj {
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        [mainObj MR_deleteEntityInContext:localContext];
    }];
}

+(void)deleteAll {
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        [DraftAttrData MR_truncateAllInContext:localContext];
    }];
}

@end

#import "AttractionData.h"
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"
#import "Categori.h"
#import "CommonUser.h"
#import "Info.h"
#import "VideoId.h"
@interface AttractionData ()
@end

@implementation AttractionData
+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[AttractionData entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[AttractionData class]] mutableCopy];
    [mapping addAttributesFromDictionary:dict];
    [mapping addRelationshipMapping:[Info defaultMapping] forProperty:@"info" keyPath:@"info"];
    [mapping addRelationshipMapping:[VideoId defaultMapping] forProperty:@"videoId" keyPath:@"video_id"];
    //    [mapping setPrimaryKey:@"entity_id"];
    
    return mapping;
}

+(NSMutableArray *)getAll {
    return [[AttractionData MR_findAll] mutableCopy];
}

+(AttractionData *)getByEntityId:(NSString *)entityId {
    NSPredicate *pre = [NSPredicate predicateWithFormat:@"entity_id == %@", entityId];
    return [AttractionData MR_findFirstWithPredicate:pre];
}

+(NSMutableArray *)getAllByTimeAsc:(BOOL)ascBool {
    return [[AttractionData MR_findAllSortedBy:@"create_date" ascending:ascBool] mutableCopy];
}

+(NSMutableArray *)getAllByBoookmarkAsc:(BOOL)ascBool {
    return [[AttractionData MR_findAllSortedBy:@"total_bookmark" ascending:ascBool] mutableCopy];
}

+(NSMutableArray *)getAllByViewAsc:(BOOL)ascBool {
    return [[AttractionData MR_findAllSortedBy:@"total_view" ascending:ascBool] mutableCopy];
}

+(NSMutableArray *)getAllByShareAsc:(BOOL)ascBool {
    return [[AttractionData MR_findAllSortedBy:@"total_share" ascending:ascBool] mutableCopy];
}

+(NSMutableArray *)getAllByAddToPlanAsc:(BOOL)ascBool { // Arun :: It is remaining. for now sorting by name. When implemented, just replace the "name" with actual parameter.
    return [[AttractionData MR_findAllSortedBy:@"name" ascending:ascBool] mutableCopy];
}

@end

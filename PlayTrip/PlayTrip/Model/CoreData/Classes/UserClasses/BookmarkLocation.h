#import "_BookmarkLocation.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"

@interface BookmarkLocation : _BookmarkLocation

+(FEMMapping *)defaultMapping;
+(NSMutableArray *)getAll ;

@end

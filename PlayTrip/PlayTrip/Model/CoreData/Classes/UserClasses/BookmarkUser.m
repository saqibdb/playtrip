#import "BookmarkUser.h"

@interface BookmarkUser ()
@end

@implementation BookmarkUser

+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[BookmarkUser entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[BookmarkUser class]] mutableCopy];
    [dict removeObjectForKey:@"logged_in_count"];
    [dict removeObjectForKey:@"last_logged_date"];
    [dict removeObjectForKey:@"last_updated"];
    [dict removeObjectForKey:@"create_date"];
    
    [mapping addAttributesFromDictionary:dict];
    
 
    [mapping addAttribute:[BookmarkUser dateTimeAttributeFor:@"create_date" andKeyPath:@"create_date"]];
    [mapping addAttribute:[BookmarkUser dateTimeAttributeFor:@"last_updated" andKeyPath:@"last_updated"]];
    [mapping addAttribute:[BookmarkUser dateTimeAttributeFor:@"last_logged_date" andKeyPath:@"last_logged_date"]];
    
    [mapping setPrimaryKey:@"entity_id"];
    
    return mapping;
}

+(NSMutableArray *)getAll {
    return [[BookmarkUser MR_findAllSortedBy:@"user_name" ascending:true] mutableCopy];
}

+(BookmarkUser *)getByEntityId:(NSString *)entityId {
    NSPredicate *pre = [NSPredicate predicateWithFormat:@"entity_id == %@", entityId];
    return [BookmarkUser MR_findFirstWithPredicate:pre];
}

@end

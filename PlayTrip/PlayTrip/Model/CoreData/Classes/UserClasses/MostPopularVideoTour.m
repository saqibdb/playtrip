
#import "MostPopularVideoTour.h"
#import "CDHelper.h"
#import "CommonUser.h"
#import "NSManagedObject+Mapping.h"
#import "Attractions.h"

@interface MostPopularVideoTour ()
@end

@implementation MostPopularVideoTour

+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[MostPopularVideoTour entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[MostPopularVideoTour class]] mutableCopy];
    
    [dict removeObjectForKey:@"create_date"];
    [dict removeObjectForKey:@"from_date"];
    [dict removeObjectForKey:@"last_updated"];
    [dict removeObjectForKey:@"to_date"];
    
    [mapping addAttributesFromDictionary:dict];
    
    [mapping addAttribute:[MostPopularVideoTour dateTimeAttributeFor:@"from_date" andKeyPath:@"from_date"]];
    [mapping addAttribute:[MostPopularVideoTour dateTimeAttributeFor:@"create_date" andKeyPath:@"create_date"]];
    [mapping addAttribute:[MostPopularVideoTour dateTimeAttributeFor:@"last_updated" andKeyPath:@"last_updated"]];
    [mapping addAttribute:[MostPopularVideoTour dateTimeAttributeFor:@"to_date" andKeyPath:@"to_date"]];
    [mapping setPrimaryKey:@"entity_id"];
    
    
    [mapping addToManyRelationshipMapping:[Attractions defaultMapping] forProperty:@"attractions" keyPath:@"attractions"];
    [mapping addRelationshipMapping:[CommonUser defaultMapping] forProperty:@"user1" keyPath:@"user"];
    
    return mapping;
}

+(NSMutableArray *)getAll {
    return [[MostPopularVideoTour  MR_findAllSortedBy:@"name" ascending:true] mutableCopy];
}

+(NSMutableArray *)getAllByTitleAsc:(BOOL)ascBool {
    return [[MostPopularVideoTour  MR_findAllSortedBy:@"name" ascending:ascBool] mutableCopy];
}
    
+(NSMutableArray *)getAllByTimeAsc:(BOOL)ascBool {
    return [[MostPopularVideoTour MR_findAllSortedBy:@"create_date" ascending:ascBool] mutableCopy];
}

+(NSMutableArray *)getAllByBoookmarkAsc:(BOOL)ascBool {
    return [[MostPopularVideoTour MR_findAllSortedBy:@"total_bookmark" ascending:ascBool] mutableCopy];
}

+(NSMutableArray *)getAllByRewardsAsc:(BOOL)ascBool {
    return [[MostPopularVideoTour MR_findAllSortedBy:@"remuneration_amount" ascending:ascBool] mutableCopy];
}

+(NSMutableArray *)getAllByAddToPlanAsc:(BOOL)ascBool { // Arun :: It is remaining. for now sorting by name. When implemented, just replace the "name" with actual parameter.
    return [[MostPopularVideoTour MR_findAllSortedBy:@"name" ascending:ascBool] mutableCopy];
}

@end

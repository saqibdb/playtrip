#import "_VideoId.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>

@interface VideoId : _VideoId
+(FEMMapping *)defaultMapping;
+(NSMutableArray *)getAll;

@end


#import "_District.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>

@interface District : _District

+(FEMMapping *)defaultMapping ;
+(NSMutableArray *)getAll;

@end

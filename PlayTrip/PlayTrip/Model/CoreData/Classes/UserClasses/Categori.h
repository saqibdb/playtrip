#import "_Categori.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>

@interface Categori : _Categori
+(FEMMapping *)defaultMapping;
+(Categori *)getByEntityId:(NSString *)entityId;
+(NSMutableArray *)getAll ;
@end

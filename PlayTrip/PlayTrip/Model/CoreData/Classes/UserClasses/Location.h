#import "_Location.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>

@interface Location : _Location
+(FEMMapping *)defaultMapping ;

@end

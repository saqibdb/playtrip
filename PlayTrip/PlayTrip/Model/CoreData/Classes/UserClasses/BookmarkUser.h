#import "_BookmarkUser.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"

@interface BookmarkUser : _BookmarkUser
+(FEMMapping *)defaultMapping;
+(NSMutableArray *)getAll ;
+(BookmarkUser *)getByEntityId:(NSString *)entityId ;
@end

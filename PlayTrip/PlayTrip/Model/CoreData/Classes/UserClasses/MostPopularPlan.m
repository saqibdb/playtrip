
#import "MostPopularPlan.h"
#import "CDHelper.h"
#import "CommonUser.h"
#import "NSManagedObject+Mapping.h"
#import "Attractions.h"

@interface MostPopularPlan ()
@end

@implementation MostPopularPlan

+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[MostPopularPlan entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[MostPopularPlan class]] mutableCopy];
    
    [dict removeObjectForKey:@"create_date"];
    [dict removeObjectForKey:@"from_date"];
    [dict removeObjectForKey:@"last_updated"];
    [dict removeObjectForKey:@"to_date"];
    
    [mapping addAttributesFromDictionary:dict];
    
    [mapping addAttribute:[MostPopularPlan dateTimeAttributeFor:@"from_date" andKeyPath:@"from_date"]];
    [mapping addAttribute:[MostPopularPlan dateTimeAttributeFor:@"create_date" andKeyPath:@"create_date"]];
    [mapping addAttribute:[MostPopularPlan dateTimeAttributeFor:@"last_updated" andKeyPath:@"last_updated"]];
    [mapping addAttribute:[MostPopularPlan dateTimeAttributeFor:@"to_date" andKeyPath:@"to_date"]];
    [mapping setPrimaryKey:@"entity_id"];
    
    [mapping addToManyRelationshipMapping:[Attractions defaultMapping] forProperty:@"attractions" keyPath:@"attractions"];
    [mapping addRelationshipMapping:[CommonUser defaultMapping] forProperty:@"userObject" keyPath:@"user"];
    
    return mapping;
}

+(NSMutableArray *)getAll {
    return [[MostPopularPlan  MR_findAllSortedBy:@"name" ascending:true] mutableCopy];
}

@end

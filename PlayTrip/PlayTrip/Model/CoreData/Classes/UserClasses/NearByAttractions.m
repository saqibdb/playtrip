
#import "NearByAttractions.h"
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"
#import "CommonUser.h"
#import "Categori.h"

@interface NearByAttractions ()
@end

@implementation NearByAttractions

+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[NearByAttractions entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[NearByAttractions class]] mutableCopy];
    
    [dict removeObjectForKey:@"create_date"];
    [dict removeObjectForKey:@"last_updated"];
    [dict removeObjectForKey:@"loc_lat"];
    [dict removeObjectForKey:@"loc_long"];
    
    
    
    [mapping addAttributesFromDictionary:dict];
    
    [mapping addAttribute:[NearByAttractions dateTimeAttributeFor:@"create_date" andKeyPath:@"create_date"]];
    [mapping addAttribute:[NearByAttractions dateTimeAttributeFor:@"last_updated" andKeyPath:@"last_updated"]];
    [mapping addAttribute:[NearByAttractions doubleAttributeFor:@"loc_lat" andKeyPath:@"loc_lat"]];
    [mapping addAttribute:[NearByAttractions doubleAttributeFor:@"loc_long" andKeyPath:@"loc_long"]];
    
    [mapping addToManyRelationshipMapping:[Categori defaultMapping] forProperty:@"categori" keyPath:@"category"];
    [mapping addRelationshipMapping:[CommonUser defaultMapping] forProperty:@"userNearByAttraction" keyPath:@"user"];
    //    [mapping setPrimaryKey:@"entity_id"];
    
    return mapping;
}

+(NSMutableArray *)getAll {
    return [[NearByAttractions MR_findAllSortedBy:@"name" ascending:true] mutableCopy];
}

+(NSMutableArray *)getAllByTimeAsc:(BOOL)ascBool {
    return [[NearByAttractions MR_findAllSortedBy:@"create_date" ascending:ascBool] mutableCopy];
}

+(NSMutableArray *)getAllByBoookmarkAsc:(BOOL)ascBool {
    return [[NearByAttractions MR_findAllSortedBy:@"total_bookmark" ascending:ascBool] mutableCopy];
}

+(NSMutableArray *)getAllByViewAsc:(BOOL)ascBool {
    return [[NearByAttractions MR_findAllSortedBy:@"total_view" ascending:ascBool] mutableCopy];
}

+(NSMutableArray *)getAllByShareAsc:(BOOL)ascBool {
    return [[NearByAttractions MR_findAllSortedBy:@"total_share" ascending:ascBool] mutableCopy];
}

+(NSMutableArray *)getAllByAddToPlanAsc:(BOOL)ascBool { // Arun :: It is remaining. for now sorting by name. When implemented, just replace the "name" with actual parameter.
    return [[NearByAttractions MR_findAllSortedBy:@"name" ascending:ascBool] mutableCopy];
}

@end

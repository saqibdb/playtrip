#import "_BookmarkAttractions.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"

@interface BookmarkAttractions : _BookmarkAttractions

+(FEMMapping *)defaultMapping;
+(NSMutableArray *)getAll ;

@end

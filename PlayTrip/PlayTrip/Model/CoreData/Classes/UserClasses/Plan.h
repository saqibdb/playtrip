#import "_Plan.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>

@interface Plan : _Plan

+(FEMMapping *)defaultMapping;
+(NSMutableArray *)getAll;
+(NSMutableArray *)getVideoTourDraft;
+(NSMutableArray *)getPlans;
+(NSMutableArray *)getVideoTours;
+(Plan *)getByEntityId:(NSString *)entityId;
@end

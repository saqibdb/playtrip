
#import "LocalAttrTags.h"
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"

@interface LocalAttrTags ()
@end

@implementation LocalAttrTags


+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[LocalAttrTags entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[LocalAttrTags class]] mutableCopy];
    [mapping addAttributesFromDictionary:dict];
    return mapping;
}

+(NSMutableArray *)getAll {
    return [[LocalAttrTags MR_findAll] mutableCopy];
}

+(LocalAttrTags *)getByEntityId:(NSString *)entityId {
    NSPredicate *pre = [NSPredicate predicateWithFormat:@"entity_id == %@", entityId];
    return [LocalAttrTags MR_findFirstWithPredicate:pre];
}

+(LocalAttrTags *)newEntity{
    LocalAttrTags *new = [LocalAttrTags MR_createEntityInContext:[NSManagedObjectContext MR_defaultContext]];
    return new;
}

+(void)saveEntity{
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}
@end

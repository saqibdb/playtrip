#import "_CommonUser.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>

@interface CommonUser : _CommonUser
+(FEMMapping *)defaultMapping ;
+(NSMutableArray *)getAll;
+(CommonUser *)getUserById:(NSString *)userId;
@end

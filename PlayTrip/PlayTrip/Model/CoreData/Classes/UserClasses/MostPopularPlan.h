#import "_MostPopularPlan.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>

@interface MostPopularPlan : _MostPopularPlan

+(FEMMapping *)defaultMapping;
+(NSMutableArray *)getAll;

@end


#import "_DraftVideoId.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>

@interface DraftVideoId : _DraftVideoId

+(FEMMapping *)defaultMapping;
+(NSMutableArray *)getAll;
+(DraftVideoId *)newEntity;
+(void)saveEntity;
+(void)deleteObj:(DraftVideoId *)mainObj;
+(void)deleteAll;

@end

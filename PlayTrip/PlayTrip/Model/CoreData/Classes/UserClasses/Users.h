
#import "_Users.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>

@interface Users : _Users

+(FEMMapping *)defaultMapping ;
+(NSMutableArray *)getAll;
+(Users *)getByEntityId:(NSString *)entityId;
+(NSMutableArray *)getAllByTitleAsc:(BOOL)ascBool;
+(NSMutableArray *)getAllByBoookmarkAsc:(BOOL)ascBool;
+(NSMutableArray *)getAllByRewardsAsc:(BOOL)ascBool;
+(NSMutableArray *)getAllByVideoTourAsc:(BOOL)ascBool;

@end

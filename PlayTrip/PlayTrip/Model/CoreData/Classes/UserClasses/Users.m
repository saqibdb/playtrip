
#import "Users.h"
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"

@interface Users ()
@end

@implementation Users

+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[Users entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[Users class]] mutableCopy];
    [dict removeObjectForKey:@"logged_in_count"];
    [dict removeObjectForKey:@"last_logged_date"];
    [dict removeObjectForKey:@"last_updated"];
    [dict removeObjectForKey:@"create_date"];
    
    [mapping addAttributesFromDictionary:dict];
    
    [mapping addAttribute:[Users intAttributeFor:@"logged_in_count" andKeyPath:@"logged_in_count"]];
    [mapping addAttribute:[Users dateTimeAttributeFor:@"create_date" andKeyPath:@"create_date"]];
    [mapping addAttribute:[Users dateTimeAttributeFor:@"last_updated" andKeyPath:@"last_updated"]];
    [mapping addAttribute:[Users dateTimeAttributeFor:@"last_logged_date" andKeyPath:@"last_logged_date"]];
    
    [mapping setPrimaryKey:@"entity_id"];

    return mapping;
}

+(NSMutableArray *)getAll {
    return [[Users MR_findAllSortedBy:@"user_name" ascending:true] mutableCopy];
}

+(Users *)getByEntityId:(NSString *)entityId {
    NSPredicate *pre = [NSPredicate predicateWithFormat:@"entity_id == %@", entityId];
    return [Users MR_findFirstWithPredicate:pre];
}

+(NSMutableArray *)getAllByTitleAsc:(BOOL)ascBool {
    return [[Users  MR_findAllSortedBy:@"user_name" ascending:ascBool] mutableCopy];
}

+(NSMutableArray *)getAllByBoookmarkAsc:(BOOL)ascBool {
    return [[Users MR_findAllSortedBy:@"total_bookmark" ascending:ascBool] mutableCopy];
}

+(NSMutableArray *)getAllByRewardsAsc:(BOOL)ascBool {// Arun :: It is remaining. for now sorting by name. When implemented, just replace the "user_name" with actual parameter.
    return [[Users MR_findAllSortedBy:@"user_name" ascending:ascBool] mutableCopy];
}

+(NSMutableArray *)getAllByVideoTourAsc:(BOOL)ascBool { // Arun :: It is remaining. for now sorting by name. When implemented, just replace the "user_name" with actual parameter.
    return [[Users MR_findAllSortedBy:@"user_name" ascending:ascBool] mutableCopy];
}

@end

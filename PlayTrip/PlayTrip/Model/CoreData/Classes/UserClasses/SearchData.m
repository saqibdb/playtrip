#import "SearchData.h"
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"
#import "Location.h"

@interface SearchData ()

// Private interface goes here.

@end

@implementation SearchData

+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[SearchData entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[SearchData class]] mutableCopy];
    
 
    
    [mapping addAttributesFromDictionary:dict];
    [mapping setPrimaryKey:@"entity_id"];
    // [mapping addToManyRelationshipMapping:[Location defaultMapping] forProperty:@"location" keyPath:@"loc"];
    
    return mapping;
}
+(NSMutableArray *)getAll {
    return [[SearchData MR_findAllSortedBy:@"name" ascending:true] mutableCopy];
}

+(NSMutableArray *)getByName:(NSString *)name{
//    NSPredicate *pre = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", name];
//    return [[SearchData MR_findAllWithPredicate:pre] mutableCopy];
    
    NSMutableArray * finalArray = [NSMutableArray new];
    NSMutableArray * localArray = [SearchData getAll];
    
    for (SearchData * obj in localArray) {
        if (!([obj.name rangeOfString:name options:NSCaseInsensitiveSearch].location == NSNotFound)){
            [finalArray addObject:obj];
        }
    }
    return finalArray;
    
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@",
//                              shopSearchBar.text];
//    [fetchRequest setPredicate:predicate];
}

@end

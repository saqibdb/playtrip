
#import "LocalVideoObject.h"
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"

@interface LocalVideoObject ()
@end

@implementation LocalVideoObject

+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[LocalVideoObject entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[LocalVideoObject class]] mutableCopy];
    [mapping addAttributesFromDictionary:dict];    
    
    return mapping;
}

+(NSMutableArray *)getAll {
    return [[LocalVideoObject MR_findAll] mutableCopy];
}

+(LocalVideoObject *)getByEntityId:(NSString *)entityId {
    NSPredicate *pre = [NSPredicate predicateWithFormat:@"entity_id == %@", entityId];
    return [LocalVideoObject MR_findFirstWithPredicate:pre];
}

+(LocalVideoObject *)getByAttrId:(NSString *)attrId {
    NSPredicate *pre = [NSPredicate predicateWithFormat:@"attrDataId == %@", attrId];
    return [LocalVideoObject MR_findFirstWithPredicate:pre];
}

+(LocalVideoObject *)newEntity{
    LocalVideoObject *new = [LocalVideoObject MR_createEntityInContext:[NSManagedObjectContext MR_defaultContext]];
    return new;
}

+(void)saveEntity{
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

+(void)deleteObj:(LocalVideoObject *)vidObj {
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        [vidObj MR_deleteEntityInContext:localContext];
    }];
}

+(NSMutableArray *)getToBeUploadedVids {
    NSMutableArray * finalArray = [NSMutableArray new];
    NSMutableArray * localArray = [self getAll];
    for (LocalVideoObject * vidObj in localArray) {
        if (![vidObj.isUploading isEqualToString:@"Yes"]) {
            if ([vidObj.isSelected isEqualToString:@"Yes"]) {
                [finalArray addObject:vidObj];
            }
        }
    }
    
    
    return finalArray;
}

@end

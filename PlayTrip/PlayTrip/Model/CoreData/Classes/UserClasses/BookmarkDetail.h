#import "_BookmarkDetail.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"

@interface BookmarkDetail : _BookmarkDetail
+(FEMMapping *)defaultMapping;
+(NSMutableArray *)getAll ;
+(BookmarkDetail *)getFirst;
@end

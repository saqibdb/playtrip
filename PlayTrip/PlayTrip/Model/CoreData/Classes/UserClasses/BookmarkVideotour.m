#import "BookmarkVideotour.h"
#import "Users.h"
#import "Attractions.h"
#import "CommonUser.h"

@interface BookmarkVideotour ()
@end

@implementation BookmarkVideotour

+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[BookmarkVideotour entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[BookmarkVideotour class]] mutableCopy];
    
    [dict removeObjectForKey:@"create_date"];
    [dict removeObjectForKey:@"from_date"];
    [dict removeObjectForKey:@"last_updated"];
    [dict removeObjectForKey:@"to_date"];
    
    [mapping addAttributesFromDictionary:dict];
    
    [mapping addAttribute:[BookmarkVideotour dateTimeAttributeFor:@"from_date" andKeyPath:@"from_date"]];
    [mapping addAttribute:[BookmarkVideotour dateTimeAttributeFor:@"create_date" andKeyPath:@"create_date"]];
    [mapping addAttribute:[BookmarkVideotour dateTimeAttributeFor:@"last_updated" andKeyPath:@"last_updated"]];
    [mapping addAttribute:[BookmarkVideotour dateTimeAttributeFor:@"to_date" andKeyPath:@"to_date"]];
    [mapping setPrimaryKey:@"entity_id"];
    
     [mapping addRelationshipMapping:[CommonUser defaultMapping] forProperty:@"userBookMark" keyPath:@"user"];
    [mapping addToManyRelationshipMapping:[Attractions defaultMapping] forProperty:@"attractions" keyPath:@"attractions"];
    
    
    return mapping;
}
+(NSMutableArray *)getAll {
    return [[BookmarkVideotour MR_findAllSortedBy:@"name" ascending:true] mutableCopy];
}
@end

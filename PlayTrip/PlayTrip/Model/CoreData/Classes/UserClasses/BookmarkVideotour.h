#import "_BookmarkVideotour.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"

@interface BookmarkVideotour : _BookmarkVideotour

+(FEMMapping *)defaultMapping;
+(NSMutableArray *)getAll ;

@end

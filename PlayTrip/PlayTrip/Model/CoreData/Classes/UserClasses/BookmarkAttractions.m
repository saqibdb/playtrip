#import "BookmarkAttractions.h"
#import "CommonUser.h"

@interface BookmarkAttractions ()
@end

@implementation BookmarkAttractions

+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[BookmarkAttractions entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[BookmarkAttractions class]] mutableCopy];
    
    
    [dict removeObjectForKey:@"create_date"];
    [dict removeObjectForKey:@"last_updated"];
    [dict removeObjectForKey:@"loc_lat"];
    [dict removeObjectForKey:@"loc_long"];
    
    
    
    [mapping addAttributesFromDictionary:dict];
    
    [mapping addAttribute:[BookmarkAttractions dateTimeAttributeFor:@"create_date" andKeyPath:@"create_date"]];
    [mapping addAttribute:[BookmarkAttractions dateTimeAttributeFor:@"last_updated" andKeyPath:@"last_updated"]];
    [mapping addAttribute:[BookmarkAttractions doubleAttributeFor:@"loc_lat" andKeyPath:@"loc_lat"]];
    [mapping addAttribute:[BookmarkAttractions doubleAttributeFor:@"loc_long" andKeyPath:@"loc_long"]];
    
    [mapping addRelationshipMapping:[CommonUser defaultMapping] forProperty:@"userBookmarkAttraction" keyPath:@"user"];
    
    return mapping;
}

+(NSMutableArray *)getAll {
    return [[BookmarkAttractions MR_findAllSortedBy:@"name" ascending:true] mutableCopy];
}

@end

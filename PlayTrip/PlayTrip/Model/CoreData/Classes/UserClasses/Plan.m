
#import "Plan.h"
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"
#import "Attractions.h"

@interface Plan ()
@end

@implementation Plan

+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[Plan entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[Plan class]] mutableCopy];
    
    [dict removeObjectForKey:@"create_date"];
    [dict removeObjectForKey:@"from_date"];
    [dict removeObjectForKey:@"last_updated"];
    [dict removeObjectForKey:@"to_date"];
    
    [mapping addAttributesFromDictionary:dict];
    
    [mapping addAttribute:[Plan dateTimeAttributeFor:@"from_date" andKeyPath:@"from_date"]];
    [mapping addAttribute:[Plan dateTimeAttributeFor:@"create_date" andKeyPath:@"create_date"]];
    [mapping addAttribute:[Plan doubleAttributeFor:@"duration" andKeyPath:@"duration"]];
    [mapping addAttribute:[Plan dateTimeAttributeFor:@"last_updated" andKeyPath:@"last_updated"]];
      [mapping addAttribute:[Plan dateTimeAttributeFor:@"to_date" andKeyPath:@"to_date"]];
    [mapping setPrimaryKey:@"entity_id"];
    [mapping addToManyRelationshipMapping:[Attractions defaultMapping] forProperty:@"attractions" keyPath:@"attractions"];
    return mapping;
}

+(NSMutableArray *)getAll {
     return [[Plan MR_findAllSortedBy:@"name" ascending:true] mutableCopy];
}

+(Plan *)getByEntityId:(NSString *)entityId {
    NSPredicate *pre = [NSPredicate predicateWithFormat:@"entity_id == %@", entityId];
    return [Plan MR_findFirstWithPredicate:pre];
}

+(NSMutableArray *)getVideoTourDraft {
    NSMutableArray * finalArray = [NSMutableArray new];
    for (Plan * pl in [self getAll]) {
        if ([pl.is_published isEqualToNumber:@0]) {
            if ([pl.is_draft isEqualToNumber:@1]) {
                [finalArray addObject:pl];
            }
        }
    }
    
    return finalArray;
}

+(NSMutableArray *)getPlans {
    NSMutableArray * finalArray = [NSMutableArray new];
    for (Plan * pl in [self getAll]) {
        if ([pl.is_published isEqualToNumber:@0]) {
            if ([pl.is_draft isEqualToNumber:@0]) {
                [finalArray addObject:pl];
            }
        }
    }
    return finalArray;
}

+(NSMutableArray *)getVideoTours {
    NSMutableArray * finalArray = [NSMutableArray new];
    for (Plan * pl in [self getAll]) {
        if ([pl.is_published isEqualToNumber:@1]) {
            [finalArray addObject:pl];
        }
    }
    return finalArray;
}

@end

#import "_SearchData.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>

@interface SearchData : _SearchData
+(FEMMapping *)defaultMapping ;
+(NSMutableArray *)getAll;
+(NSMutableArray *)getByName:(NSString *)name;
@end

#import "_MostPopularVideoTour.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>

@interface MostPopularVideoTour : _MostPopularVideoTour

+(FEMMapping *)defaultMapping;
+(NSMutableArray *)getAll;
+(NSMutableArray *)getAllByTitleAsc:(BOOL)ascBool;
+(NSMutableArray *)getAllByTimeAsc:(BOOL)ascBool;
+(NSMutableArray *)getAllByBoookmarkAsc:(BOOL)ascBool;
+(NSMutableArray *)getAllByRewardsAsc:(BOOL)ascBool;
+(NSMutableArray *)getAllByAddToPlanAsc:(BOOL)ascBool;

@end


#import "_DraftTour.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>

@interface DraftTour : _DraftTour

+(FEMMapping *)defaultMapping;
+(DraftTour *)getDraft;
+(DraftTour *)newEntity;
+(void)saveEntity;
+(void)deleteObj:(DraftTour *)mainObj;

@end

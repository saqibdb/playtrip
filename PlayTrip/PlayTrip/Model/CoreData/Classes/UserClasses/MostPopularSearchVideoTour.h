#import "_MostPopularSearchVideoTour.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>
@interface MostPopularSearchVideoTour : _MostPopularSearchVideoTour
+(FEMMapping *)defaultMapping;
+(NSMutableArray *)getAll;
@end

#import "VideoId.h"
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"

@interface VideoId ()

// Private interface goes here.

@end

@implementation VideoId

+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[VideoId entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[VideoId class]] mutableCopy];
    
//     [dict removeObjectForKey:@"create_date"];
    
    
    [mapping addAttributesFromDictionary:dict];
//     [mapping addAttribute:[VideoId dateTimeAttributeFor:@"create_date" andKeyPath:@"create_date"]];
    
    
    //    [mapping setPrimaryKey:@"entity_id"];
    
    return mapping;
}
+(NSMutableArray *)getAll {
    return [[VideoId MR_findAllInContext:[NSManagedObjectContext MR_defaultContext]] mutableCopy];
}

@end

#import "BookmarkDetail.h"

#import "BookmarkAttractions.h"
#import "BookmarkUser.h"
#import "BookmarkVideotour.h"
#import "BookmarkLocation.h"

@interface BookmarkDetail ()
@end

@implementation BookmarkDetail
+(FEMMapping *)defaultMapping{
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[BookmarkDetail entityName]];
    
     [mapping addToManyRelationshipMapping:[BookmarkAttractions defaultMapping] forProperty:@"bookmarkAttractions" keyPath:@"attractions"];
    
     [mapping addToManyRelationshipMapping:[BookmarkVideotour defaultMapping] forProperty:@"bookmarkVideotour" keyPath:@"video_tours"];
    
    [mapping addToManyRelationshipMapping:[BookmarkUser defaultMapping] forProperty:@"bookmarkUser" keyPath:@"users"];
    
    [mapping addToManyRelationshipMapping:[BookmarkLocation defaultMapping] forProperty:@"bookmarkLocation" keyPath:@"locations"];
    
    return mapping;
}

+(NSMutableArray *)getAll {
    return [[BookmarkDetail MR_findAll]mutableCopy];
}

+(BookmarkDetail *)getFirst {
    return [BookmarkDetail MR_findFirst];
}

@end


#import "_LocalVideoObject.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>

@interface LocalVideoObject : _LocalVideoObject

+(FEMMapping *)defaultMapping;
+(NSMutableArray *)getAll;
+(LocalVideoObject *)getByEntityId:(NSString *)entityId;
+(LocalVideoObject *)getByAttrId:(NSString *)attrId;
+(LocalVideoObject *)newEntity;
+(void)saveEntity;
+(void)deleteObj:(LocalVideoObject *)vidObj;
+(NSMutableArray *)getToBeUploadedVids;
@end


#import "DraftInfo.h"
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"

@interface DraftInfo ()
@end

@implementation DraftInfo

+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[DraftInfo entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[DraftInfo class]] mutableCopy];
    
    [dict removeObjectForKey:@"loc_lat"];
    [dict removeObjectForKey:@"loc_long"];
    
    [mapping addAttributesFromDictionary:dict];
    
    [mapping addAttribute:[DraftInfo doubleAttributeFor:@"loc_lat" andKeyPath:@"loc_lat"]];
    [mapping addAttribute:[DraftInfo doubleAttributeFor:@"loc_long" andKeyPath:@"loc_long"]];
    
    return mapping;
}

+(NSMutableArray *)getAll {
    return [[DraftInfo MR_findAll] mutableCopy];
}

+(DraftInfo *)newEntity{
    DraftInfo *new = [DraftInfo MR_createEntityInContext:[NSManagedObjectContext MR_defaultContext]];
    return new;
}

+(void)saveEntity{
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

+(void)deleteObj:(DraftInfo *)mainObj {
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        [mainObj MR_deleteEntityInContext:localContext];
    }];
}

+(void)deleteAll {
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        [DraftInfo MR_truncateAllInContext:localContext];
    }];
}

@end

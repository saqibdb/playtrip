#import "DraftTour.h"
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"
#import "Attractions.h"

@interface DraftTour ()
@end

@implementation DraftTour

+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[DraftTour entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[DraftTour class]] mutableCopy];
    
    [dict removeObjectForKey:@"create_date"];
    [dict removeObjectForKey:@"from_date"];
    [dict removeObjectForKey:@"to_date"];
    [dict removeObjectForKey:@"last_updated"];
    
    [mapping addAttributesFromDictionary:dict];
    
    [mapping addAttribute:[DraftTour dateTimeAttributeFor:@"from_date" andKeyPath:@"from_date"]];
    [mapping addAttribute:[DraftTour dateTimeAttributeFor:@"last_updated" andKeyPath:@"last_updated"]];
    [mapping addAttribute:[DraftTour dateTimeAttributeFor:@"create_date" andKeyPath:@"create_date"]];

    [mapping addAttribute:[DraftTour dateTimeAttributeFor:@"to_date" andKeyPath:@"to_date"]];
    [mapping setPrimaryKey:@"entity_id"];
    [mapping addToManyRelationshipMapping:[Attractions defaultMapping] forProperty:@"attractions" keyPath:@"attractions"];
    [mapping addToManyRelationshipMapping:[Attractions defaultMapping] forProperty:@"draftAttractions" keyPath:@"draftAttractions"];
    
    return mapping;
}

+(DraftTour *)getDraft {
    return [DraftTour MR_findFirst];
}

+(DraftTour *)newEntity{
    DraftTour *new = [DraftTour MR_createEntityInContext:[NSManagedObjectContext MR_defaultContext]];
    return new;
}

+(void)saveEntity{
    DraftTour * dTour = [self getDraft];
    dTour.last_updated = [NSDate date];
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
}

+(void)deleteObj:(DraftTour *)mainObj {
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        [mainObj MR_deleteEntityInContext:localContext];
    }];
}

@end

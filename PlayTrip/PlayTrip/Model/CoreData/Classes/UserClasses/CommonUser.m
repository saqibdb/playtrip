#import "CommonUser.h"
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"

@interface CommonUser ()
@end

@implementation CommonUser

+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[CommonUser entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[CommonUser class]] mutableCopy];
    [dict removeObjectForKey:@"create_date"];
    [dict removeObjectForKey:@"last_logged_date"];
    [dict removeObjectForKey:@"last_updated"];
   
    
    [mapping addAttributesFromDictionary:dict];
    
   
    [mapping addAttribute:[CommonUser dateTimeAttributeFor:@"create_date" andKeyPath:@"create_date"]];
    [mapping addAttribute:[CommonUser dateTimeAttributeFor:@"last_updated" andKeyPath:@"last_updated"]];
    [mapping addAttribute:[CommonUser dateTimeAttributeFor:@"last_logged_date" andKeyPath:@"last_logged_date"]];
    
    [mapping setPrimaryKey:@"entity_id"];
    
    return mapping;
}

+(NSMutableArray *)getAll {
    return [[CommonUser  MR_findAllSortedBy:@"user_name" ascending:true] mutableCopy];
}

+(CommonUser *)getUserById:(NSString *)userId {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(entity_id == %@)",userId];
    return [[CommonUser MR_findAllWithPredicate:predicate] mutableCopy];
    
}


@end

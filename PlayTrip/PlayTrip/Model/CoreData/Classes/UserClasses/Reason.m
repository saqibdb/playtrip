
#import "Reason.h"
#import "CDHelper.h"
#import "NSManagedObject+Mapping.h"

@interface Reason ()
@end

@implementation Reason

+(FEMMapping *)defaultMapping {
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[Reason entityName]];
    NSMutableDictionary *dict = [[CDHelper mappingForClass:[Reason class]] mutableCopy];
    
    [dict removeObjectForKey:@"create_date"];
    
    [mapping addAttributesFromDictionary:dict];
    
    [mapping addAttribute:[Reason dateTimeAttributeFor:@"create_date" andKeyPath:@"create_date"]];
    
    [mapping setPrimaryKey:@"entity_id"];
    
    
    return mapping;
}

+(NSMutableArray *)getAll {
    return [[Reason MR_findAllSortedBy:@"entity_id" ascending:true] mutableCopy];
}


@end


#import "_Attractions.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>

@interface Attractions : _Attractions

+(FEMMapping *)defaultMapping ;
+(NSMutableArray *)getAll;
+(Attractions *)getByEntityId:(NSString *)entityId;
+(NSMutableArray *)getAttractionByday:(NSString *)day WithPlanId:(NSString *)pid;
+(void)saveEntity;
+(NSMutableArray *)getListByName:(NSString *)name;

@end

#import "BookmarkLocation.h"

@interface BookmarkLocation ()
@end

@implementation BookmarkLocation
+(FEMMapping *)defaultMapping{
    FEMMapping *mapping = [[FEMMapping alloc] initWithEntityName:[BookmarkLocation entityName]];
    return mapping;
}
+(NSMutableArray *)getAll {
    return [[BookmarkLocation MR_findAll]mutableCopy];
}

@end

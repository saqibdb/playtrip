
#import "_VideoTour.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>
@interface VideoTour : _VideoTour

+(FEMMapping *)defaultMapping;
+(NSMutableArray *)getAll;
+(VideoTour *)getByEntityId:(NSString *)entityId;
+(NSMutableArray *)getAllByTimeAsc:(BOOL)ascBool;
+(NSMutableArray *)getAllByBoookmarkAsc:(BOOL)ascBool;
+(NSMutableArray *)getAllByViewAsc:(BOOL)ascBool;
+(NSMutableArray *)getAllByShareAsc:(BOOL)ascBool;
+(NSMutableArray *)getAllByAmountAsc:(BOOL)ascBool;
+(NSMutableArray *)getAllByAttrCountAsc:(BOOL)ascBool;
+(NSMutableArray *)getAllByAddToPlanAsc:(BOOL)ascBool;
+(VideoTour *)getvideTourByName:(NSString *)name;

+(NSMutableArray *)getAllUser:(CommonUser *)user;

@end


#import "_DraftAttrData.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>

@interface DraftAttrData : _DraftAttrData

+(FEMMapping *)defaultMapping;
+(NSMutableArray *)getAll;
+(DraftAttrData *)newEntity;
+(void)saveEntity;
+(void)deleteObj:(DraftAttrData *)mainObj;
+(void)deleteAll;

@end

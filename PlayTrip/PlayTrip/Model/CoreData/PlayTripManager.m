
#import "PlayTripManager.h"
#import "AppDelegate.h"
#import "AccountManager.h"
#import "District.h"
#import "Reason.h"
#import "Account.h"
#import <MagicalRecord/MagicalRecord.h>
#import <FastEasyMapping/FEMMapping.h>
#import "FEMDeserializer.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "Utils.h"
#import "URLSchema.h"
#import "Plan.h"
#import "Attractions.h"
#import "Users.h"
#import "AttractionData.h"
#import "VideoTour.h"
#import "Categori.h"
#import "CommonUser.h"
#import "MostPopularSearchVideoTour.h"
#import "MostPopularSearchAttraction.h"
#import "MostPopularPlan.h"
#import "MostPopularVideoTour.h"
#import "MostPopularAttractions.h"
#import "SearchData.h"
#import <AFNetworking/AFNetworking.h>
#import "AttractionModel.h"
#import <AFNetworking/AFHTTPRequestOperationManager.h>


//ASAPIManager
static PlayTripManager *sharedInstance = nil;

@implementation PlayTripManager

+ (id) Instance {
    if (sharedInstance == nil) {
        sharedInstance = [[PlayTripManager alloc]init];
    }
    return sharedInstance;
}

//Arun :: Whenever you add a class, add it here to clear the database
+ (void)clearDatabBase {
    [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
        [Plan MR_truncateAllInContext:localContext];
        [Attractions MR_truncateAllInContext:localContext];
        
        [Users MR_truncateAllInContext:localContext];
        [CommonUser MR_truncateAllInContext:localContext];
        [District MR_truncateAllInContext:localContext];
        [Reason MR_truncateAllInContext:localContext];
        [VideoTour MR_truncateAllInContext:localContext];
        [MostPopularSearchVideoTour MR_truncateAllInContext:localContext];
        [MostPopularSearchAttraction MR_truncateAllInContext:localContext];
        [BookmarkDetail MR_truncateAllInContext:localContext];
        [BookmarkUser MR_truncateAllInContext:localContext];
        [BookmarkLocation MR_truncateAllInContext:localContext];
        [BookmarkAttractions MR_truncateAllInContext:localContext];
        [BookmarkVideotour MR_truncateAllInContext:localContext];
        [MostPopularVideoTour MR_truncateAllInContext:localContext];
        [MostPopularPlan MR_truncateAllInContext:localContext];
    }];
}

// Arun :: creating tricky json for attractions response as we are getting the lat,long as array, changing it to individual parameters.
-(NSMutableArray*)createTrickyJsonForAttractionLocationValues:(NSMutableArray*)wholeArray {
    NSMutableArray * finalArray = [NSMutableArray new];
    
    for (NSDictionary * dict1 in wholeArray) {
        NSMutableDictionary * dict = [dict1 mutableCopy];
        
        if ([dict objectForKey:@"loc"] && [dict objectForKey:@"loc"] != nil) {
            NSArray * locArray = [dict objectForKey:@"loc"];
            NSString * lng = [locArray objectAtIndex:1];
            NSString * lat = [locArray objectAtIndex:0];
            double lngValue = [lng doubleValue];
            double latValue = [lat doubleValue];
            
            NSNumber *lngNumber = [[NSNumber alloc] initWithDouble:lngValue];
            NSNumber *latNumber = [[NSNumber alloc] initWithDouble:latValue];
            
            [dict setObject:lngNumber forKey:@"loc_long"];
            [dict setObject:latNumber forKey:@"loc_lat"];
            [finalArray addObject:dict];
        }
        else if([dict objectForKey:@"loc_lat"] && [dict objectForKey:@"loc_long"] != nil){
            NSString * lng = [dict objectForKey:@"loc_long"];
            NSString * lat = [dict objectForKey:@"loc_lat"];
            double lngValue = [lng doubleValue];
            double latValue = [lat doubleValue];
            
            NSNumber *lngNumber = [[NSNumber alloc] initWithDouble:lngValue];
            NSNumber *latNumber = [[NSNumber alloc] initWithDouble:latValue];
            
            [dict setObject:lngNumber forKey:@"loc_long"];
            [dict setObject:latNumber forKey:@"loc_lat"];
            [finalArray addObject:dict];
        }
        if ([dict objectForKey:@"district"] && [dict objectForKey:@"district"] != nil) {
            NSMutableDictionary * distDict = [dict objectForKey:@"district"];
            if(distDict){
                NSString * distId =[distDict objectForKey:@"_id"];
                [dict setObject:distId forKey:@"district_id"];
            }
            
            [finalArray addObject:dict];
        }
    }
    return finalArray;
}

-(NSMutableArray*)createTrickyJsonForAttractionDataLocationValues:(NSMutableArray*)wholeArray {
    NSMutableArray * finalArray = [NSMutableArray new];
    
    for (NSDictionary * dict1 in wholeArray) {
        NSMutableDictionary * dict2 = [dict1 mutableCopy];
        
        if ([dict2 objectForKey:@"info"] && [dict2 objectForKey:@"info"] != nil) {
            NSDictionary * dict3 = [dict2 objectForKey:@"info"];
            NSMutableDictionary * dict = [dict3 mutableCopy];
            
            if ([dict objectForKey:@"loc"] && [dict objectForKey:@"loc"] != nil) {
                NSArray * locArray = [dict objectForKey:@"loc"];
                NSString * lng = [locArray objectAtIndex:1];
                NSString * lat = [locArray objectAtIndex:0];
                double lngValue = [lng doubleValue];
                double latValue = [lat doubleValue];
                
                NSNumber *lngNumber = [[NSNumber alloc] initWithDouble:lngValue];
                NSNumber *latNumber = [[NSNumber alloc] initWithDouble:latValue];
                
                [dict setObject:lngNumber forKey:@"loc_long"];
                [dict setObject:latNumber forKey:@"loc_lat"];
                
            }
            else if([dict objectForKey:@"loc_lat"] && [dict objectForKey:@"loc_long"] != nil){
                NSString * lng = [dict objectForKey:@"loc_long"];
                NSString * lat = [dict objectForKey:@"loc_lat"];
                double lngValue = [lng doubleValue];
                double latValue = [lat doubleValue];
                
                NSNumber *lngNumber = [[NSNumber alloc] initWithDouble:lngValue];
                NSNumber *latNumber = [[NSNumber alloc] initWithDouble:latValue];
                
                [dict setObject:lngNumber forKey:@"loc_long"];
                [dict setObject:latNumber forKey:@"loc_lat"];
                [finalArray addObject:dict];
            }
            [dict2 setValue:dict forKey:@"info"];
        }
        [finalArray addObject:dict2];
    }
    return finalArray;
}



-(NSMutableArray *)removeEmptyVideoId:(NSMutableArray *)inArray {
    NSMutableArray * outArray =  [NSMutableArray new];
    
    for (NSDictionary * dict1 in inArray) {
        NSMutableDictionary * dict = [dict1 mutableCopy];
        if ([[dict objectForKey:@"video_id"] isKindOfClass:[NSString class]]) {
            [dict removeObjectForKey:@"video_id"];
        } else {
            NSLog(@"%@", [dict objectForKey:@"video_id"]);
        }
        [outArray addObject:dict];
    }
    return outArray;
}



-(NSMutableArray *)createTrickJsonForAttrDataLocationValues :( NSMutableArray *)inArray {
    NSMutableArray * finalArray = [NSMutableArray new];
    
    
    
    
    
    return finalArray;
}

-(NSMutableDictionary*)createTrickyJsonForDetailsLocationValues:(NSMutableDictionary*)wholeDic {
    
    NSMutableArray * finalArrayAttraction = [NSMutableArray new];
    NSMutableDictionary * finalDict = [NSMutableDictionary new];
    
    [finalDict setObject:[wholeDic valueForKey:@"video_tours"] forKey:@"video_tours"];
    [finalDict setObject:[wholeDic valueForKey:@"users"] forKey:@"users"];
    [finalDict setObject:[wholeDic valueForKey:@"locations"] forKey:@"locations"];
    
    for (NSDictionary * dict1 in [wholeDic valueForKey:@"attractions"]) {
        
        NSMutableDictionary * dict = [dict1 mutableCopy];
        
        if ([dict objectForKey:@"loc"] && [dict objectForKey:@"loc"] != nil) {
            NSArray * locArray = [dict objectForKey:@"loc"];
            NSString * lng = [locArray objectAtIndex:1];
            NSString * lat = [locArray objectAtIndex:0];
            double lngValue = [lng doubleValue];
            double latValue = [lat doubleValue];
            
            NSNumber *lngNumber = [[NSNumber alloc] initWithDouble:lngValue];
            NSNumber *latNumber = [[NSNumber alloc] initWithDouble:latValue];
            
            [dict setObject:lngNumber forKey:@"loc_long"];
            [dict setObject:latNumber forKey:@"loc_lat"];
            [finalArrayAttraction addObject:dict];
        }
        else if([dict objectForKey:@"loc_lat"] && [dict objectForKey:@"loc_long"] != nil){
            NSString * lng = [dict objectForKey:@"loc_long"];
            NSString * lat = [dict objectForKey:@"loc_lat"];
            double lngValue = [lng doubleValue];
            double latValue = [lat doubleValue];
            
            NSNumber *lngNumber = [[NSNumber alloc] initWithDouble:lngValue];
            NSNumber *latNumber = [[NSNumber alloc] initWithDouble:latValue];
            
            [dict setObject:lngNumber forKey:@"loc_long"];
            [dict setObject:latNumber forKey:@"loc_lat"];
            [finalArrayAttraction addObject:dict];
        }
        [finalArrayAttraction addObject:dict];
    }
    [finalDict setObject:finalArrayAttraction forKey:@"attractions"];
    return finalDict;
}





//Arun:: create tricky json for bookmark users. It is coming as array from response without key. so adding all array values as a string with ,

-(NSMutableArray *)createTrickyJsonForBookmarkUsers:(NSMutableArray*)wholeArray {
    NSMutableArray * finalArray = [NSMutableArray new];
    
    for (NSDictionary * dict1 in wholeArray) {
        NSString * finalString  = @"";
        
        NSMutableDictionary * dict = [dict1 mutableCopy];
        
        if ([dict objectForKey:@"bookmark_users"] && [dict objectForKey:@"bookmark_users"] != nil) {
            NSArray * aArray = [dict objectForKey:@"bookmark_users"];
            for (NSString * aString in aArray) {
                NSString * bString = [NSString stringWithFormat:@"%@,",aString];
                finalString = [finalString stringByAppendingString:bString];
            }
            if (finalString.length > 0) {
                if ([[finalString substringFromIndex:[finalString length] - 1] isEqualToString:@","]) {
                    finalString = [finalString substringToIndex:[finalString length] - 1];
                }
            }
            [dict setObject:finalString forKey:@"bookmark_users_string"];
        }
        NSNumber *loc_latNum = @([@"0" floatValue]);
        if ([dict objectForKey:@"loc_lat"]) {
            NSString *loc_latStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"loc_lat"]];
            if (loc_latStr.length) {
                loc_latNum = @([loc_latStr floatValue]);
            }
        }
        [dict setObject:loc_latNum forKey:@"loc_lat"];
        
        NSNumber *loc_longNum = @([@"1" floatValue]);
        if ([dict objectForKey:@"loc_long"]) {
            NSString *loc_longStr = [NSString stringWithFormat:@"%@",[dict objectForKey:@"loc_long"]];
            if (loc_longStr.length) {
                loc_longNum = @([loc_longStr floatValue]);
            }
        }
        [dict setObject:loc_latNum forKey:@"loc_long"];
        
        [finalArray addObject:dict];
    }
    return finalArray;
}


-(NSMutableDictionary*)createTrickyJsonForBookMarkUserDetails:(NSMutableDictionary *)wholeDic {
    NSMutableArray * finalArrayAttraction = [NSMutableArray new];
    NSMutableArray * finalArrayUser = [NSMutableArray new];
    NSMutableArray * finalArrayVideoTour = [NSMutableArray new];
    NSMutableArray * finalArrayLocation = [NSMutableArray new];
    NSMutableDictionary * finalDict = [NSMutableDictionary new];
    finalArrayLocation = [wholeDic valueForKey:@"locations"];
    
    for (NSDictionary * dict1 in [wholeDic valueForKey:@"attractions"]) {
        
        NSString * finalString  = @"";
        
        NSMutableDictionary * dict = [dict1 mutableCopy];
        
        if ([dict objectForKey:@"bookmark_users"] && [dict objectForKey:@"bookmark_users"] != nil) {
            NSArray * aArray = [dict objectForKey:@"bookmark_users"];
            for (NSString * aString in aArray) {
                NSString * bString = [NSString stringWithFormat:@"%@,",aString];
                finalString = [finalString stringByAppendingString:bString];
            }
            if (finalString.length > 0) {
                if ([[finalString substringFromIndex:[finalString length] - 1] isEqualToString:@","]) {
                    finalString = [finalString substringToIndex:[finalString length] - 1];
                }
            }
            [dict setObject:finalString forKey:@"bookmark_users_string"];
        }
        [finalArrayAttraction addObject:dict];
    }
    
    
    for (NSDictionary * dict1 in [wholeDic valueForKey:@"users"]) {
        
        NSString * finalString  = @"";
        
        NSMutableDictionary * dict = [dict1 mutableCopy];
        
        if ([dict objectForKey:@"bookmark_users"] && [dict objectForKey:@"bookmark_users"] != nil) {
            NSArray * aArray = [dict objectForKey:@"bookmark_users"];
            for (NSString * aString in aArray) {
                NSString * bString = [NSString stringWithFormat:@"%@,",aString];
                finalString = [finalString stringByAppendingString:bString];
            }
            if (finalString.length > 0) {
                if ([[finalString substringFromIndex:[finalString length] - 1] isEqualToString:@","]) {
                    finalString = [finalString substringToIndex:[finalString length] - 1];
                }
            }
            [dict setObject:finalString forKey:@"bookmark_users_string"];
        }
        [finalArrayUser addObject:dict];
    }
 
    for (NSDictionary * dict1 in [wholeDic valueForKey:@"video_tours"]) {
        
        NSString * finalString  = @"";
        
        NSMutableDictionary * dict = [dict1 mutableCopy];
        
        if ([dict objectForKey:@"bookmark_users"] && [dict objectForKey:@"bookmark_users"] != nil) {
            NSArray * aArray = [dict objectForKey:@"bookmark_users"];
            for (NSString * aString in aArray) {
                NSString * bString = [NSString stringWithFormat:@"%@,",aString];
                finalString = [finalString stringByAppendingString:bString];
            }
            if (finalString.length > 0) {
                if ([[finalString substringFromIndex:[finalString length] - 1] isEqualToString:@","]) {
                    finalString = [finalString substringToIndex:[finalString length] - 1];
                }
            }
            [dict setObject:finalString forKey:@"bookmark_users_string"];
        }
        [finalArrayVideoTour addObject:dict];
    }
    
    
    [finalDict setObject:finalArrayAttraction forKey:@"attractions"];
    [finalDict setObject:finalArrayVideoTour forKey:@"video_tours"];
    [finalDict setObject:finalArrayUser forKey:@"users"];
    [finalDict setObject:finalArrayLocation forKey:@"locations"];
    
    return finalDict;
}

-(void)loadVideoTour:(ItemLoadedBlock)block {
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kVideoTour andDelegate:self];
        request.Tag = kVideoTour;
        [request startRequest];
    }
}

-(void)getTermsAndConditionsWithBlock:(ItemLoadedBlock)block {
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kTermsAndConditions andDelegate:self];
        request.Tag = kTermsAndConditions;
        [request startRequest];
    }
}

-(void)createMyPlanWithUser:(NSString *)userid withName:(NSString *)name withFromDate:(NSString *)fdate withDays:(id)days withBlock:(ItemLoadedBlock)block {
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kCreatePlan andDelegate:self andMethod:POST];
        [request setParameter:userid forKey:@"user"];
        [request setParameter:name forKey:@"name"];
        [request setParameter:fdate forKey:@"from_date"];
        [request setParameter:days forKey:@"days"];
        [request setParameter:@"plan" forKey:@"type"];
        request.Tag = kCreatePlan;
        [request startRequest];
    }
}

-(void)createVideoTourFromPlanWithUser:(NSString *)userid withPlanId:(NSString *)pId withIsPublished:(BOOL)isPublished withDraft:(BOOL)isDraft WithLanguage:(NSString *)langauge withBlock:(ItemLoadedBlock)block {
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kCreatePlan andDelegate:self andMethod:POST];
        [request setParameter:userid forKey:@"user"];
        [request setParameter:[NSNumber numberWithBool:isPublished] forKey:@"is_published"];
        [request setParameter:[NSNumber numberWithBool:isDraft] forKey:@"is_draft"];
        [request setParameter:langauge forKey:@"language"];
        [request setParameter:@"videoTour" forKey:@"type"];
        request.Tag = kCreatePlan;
        [request startRequest];
    }
}

-(void)editMyPlanWithPlanId:(NSString *)planId WithUser:(NSString *)userid withName:(NSString *)name withFromDate:(NSString *)fdate withDays:(id)days withBlock:(ItemLoadedBlock)block {
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kCreatePlan andDelegate:self andMethod:POST];
        [request setParameter:planId forKey:@"_id"];
        [request setParameter:userid forKey:@"user"];
        [request setParameter:name forKey:@"name"];
        [request setParameter:fdate forKey:@"from_date"];
        [request setParameter:days forKey:@"days"];
        [request setParameter:@"plan" forKey:@"type"];
        request.Tag = kCreatePlan;
        [request startRequest];
    }
}

- (void)loadPlanByUserID:(NSString *)userId WithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        NSString *url= [NSString stringWithFormat:kPlanBYUserID,userId];
        Request *request = [[Request alloc] initWithUrl:url andDelegate:self];
        [request setParameter:@"plan" forKey:@"type"];
        request.Tag = kPlanBYUserID;
        [request startRequest];
    }
}

- (void)loadSpecificPlanByPlanID:(NSString *)planId WithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        NSString *url= [NSString stringWithFormat:kPlanBYPlanID,planId];
        Request *request = [[Request alloc] initWithUrl:url andDelegate:self];
        request.Tag = kPlanBYPlanID;
        [request startRequest];
    }
}

-(void)getDistrictListWithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kGetDistricts andDelegate:self];
        request.Tag = kGetDistricts;
        [request startRequest];
    }
}

-(void)getRecommendedUsersListWithLatitude:(double)lat WithLongitude:(double)lang WithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kDashboardUser andDelegate:self andMethod:GET];
//        Account * account = [AccountManager Instance].activeAccount;
//        if(account){
//            if (account.isSkip != true) {
//                NSString * userId = [AccountManager Instance].activeAccount.userId;
//                [request setParameter:userId forKey:@"id"];
//            }
//        }
//        [request setParameter:[NSNumber numberWithDouble:lat]  forKey:@"longitude"];
//        [request setParameter:[NSNumber numberWithDouble:lang]  forKey:@"latitude"];
        request.Tag = kDashboardUser;
        [request startRequest];
    }
}

-(void)getRecommendedUsersListWithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kDashboardUser andDelegate:self andMethod:GET];
        request.Tag = kDashboardUser;
        [request startRequest];
    }
}

-(void)getReportsListWithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kGetReports andDelegate:self];
        request.Tag = kGetReports;
        [request startRequest];
    }
}

-(void)loadCategory:(ItemLoadedBlock)block {
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kGetAllCategory andDelegate:self];
        request.Tag = kGetAllCategory;
        [request startRequest];
    }
}

- (void)getAttractionsListWithDistance:(id)distance WithLattitude:(double)latitude WithLongitude:(double)longitude WithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kGetAllAttractions andDelegate:self andMethod:POST];
        [request setParameter:@50 forKey:@"max_distance"];
        [request setParameter: [NSNumber numberWithDouble:latitude] forKey:@"latitude"];
        [request setParameter:[NSNumber numberWithDouble:longitude]  forKey:@"longitude"];
        request.Tag = kGetAllAttractions;
        [request startRequest];
    }
}

- (void)getAttractionsListWithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kGetAttraction andDelegate:self andMethod:POST];
        request.Tag = kGetAllAttractions;
        [request startRequest];
    }
}



-(void)getAttractionListWithBlock:(ItemLoadedBlock)block {
    _itemLoadedBlock = block;
    
    
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kGetAttraction andDelegate:self andMethod:GET];
        request.Tag = kGetAllAttractions;
        [request startRequest];
    }
    
    /*
    _itemLoadedBlock = block;
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    
    [manager.requestSerializer setValue:[Utils getValueForKey:kLoginAutheticationHeader] forHTTPHeaderField:@"Authorization"];
    
    
    NSString *requestUrl = [NSString stringWithFormat:@"%@/%@",baseURL , kGetAttraction] ;
    
    [manager GET:requestUrl parameters:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
        
        NSDictionary *data = [responseObject valueForKey:@"data"];
        NSString * aStr = [data valueForKey:@"_id"];
        
        NSLog(@"id - %@" , aStr);
        block(responseObject, nil);
        
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        block(nil, error.description);

    }];
    */
}

-(void)loadPlanByUserID:(NSString *)userId WithBlockNew:(ItemLoadedBlock)block{
    
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",baseURL , kDashboardVideoTour] ;
    NSDictionary *headers = @{ @"content-type": @"application/json" };
    NSDictionary *parameters = @{ @"where": @{ @"type": @"plan", @"user": userId }, @"sort": @{ @"create_date": @-1 } };
    
    //NSDictionary *parameters = @{ @"where": @{ @"user": userId }, @"sort": @{ @"create_date": @-1 } };
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            block(nil ,error.description);
        } else {
            block(data, nil);
        }
    }];
    [dataTask resume];
}


-(void)getAttractionListWithBlockNew:(ItemLoadedBlock)block {
    
    if ([[AppDelegate appDelegate] isReachable]) {
        
        NSString *requestUrl = [NSString stringWithFormat:@"%@%@",baseURL , kGetAttraction] ;

        
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
        [request setHTTPMethod:@"GET"];
        
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            if (error) {
                block(nil ,error.description);
            } else {
                block(data, nil);
            }
        }];
        [dataTask resume];

    }
}

- (void)deleteTourWithId:(NSString *)tourId WithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        NSString *url= [NSString stringWithFormat:@"%@/%@",kDeleteTour,tourId];
        Request *request = [[Request alloc] initWithUrl:url andDelegate:self andMethod:DELETE];
        request.Tag = kDeleteTour;
        [request startRequest];
    }
}

- (void)addAttractionToMyPlan:(NSString *)planId forDay:(NSNumber *)day withAttractions:(NSMutableArray *)attractionArray WithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kAddAttractionToMyPlan andDelegate:self andMethod:POST];
        Account * account = [AccountManager Instance].activeAccount;
        if(account){
            if (account.isSkip != true) {
                NSString * userId = [AccountManager Instance].activeAccount.userId;
                [request setParameter:userId forKey:@"user"];
            }
        }
        
        [request setParameter:day forKey:@"day"];
        [request setParameter:attractionArray forKey:@"attr"];//attr_data
        [request setParameter:planId forKey:@"plan"];
        request.Tag = kAddAttractionToMyPlan;
        [request startRequest];
    }
}

-(void)addSpam:(NSString *)spamId forPlan:(NSString *)planId WithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kAddSpamToPlan andDelegate:self andMethod:PUT];
        Account * account = [AccountManager Instance].activeAccount;
        if(account){
            if (account.isSkip != true) {
                NSString * userId = [AccountManager Instance].activeAccount.userId;
                [request setParameter:userId forKey:@"user"];
            }
        }
        [request setParameter:spamId forKey:@"reason"];
        [request setParameter:@"5863c97955d12b41052ee5f1" forKey:@"plan"];
        request.Tag = kAddSpamToPlan;
        [request startRequest];
    }
}

-(void)searchDistrictWithString:(NSString *)searchStr WithLat:(double)lat andLng:(double)lng WithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    
    if ([[AppDelegate appDelegate] isReachable]) {
        
        Request *request = [[Request alloc] initWithUrl:kSearchDistrictString andDelegate:self andMethod:POST];
        [request setParameter:searchStr forKey:@"searchStr"];
        NSString * latString = [NSString stringWithFormat:@"%f",lat];
        NSString * lngString = [NSString stringWithFormat:@"%f",lng];
        [request setParameter:latString forKey:@"latitude"];
        [request setParameter:lngString forKey:@"longitude"];
        request.Tag = kSearchDistrictString;
        [request startRequest];
    }
}

-(void)tagsAddEditWithName:(NSString *)name andAddress:(NSString *)address andPlaceId:(NSString *)placeId andLat:(NSString *)lat andLng:(NSString *)lng WithBlock:(ItemLoadedBlock)block {
    
    NSMutableDictionary * fullDict = [NSMutableDictionary new];

    
    if (lng && lat) {
        NSMutableArray* locArray = [NSMutableArray new];
        [locArray addObject:lng];
        [locArray addObject:lat];
        [fullDict setValue:locArray forKey:@"loc"];
    }
    if (name) {
        [fullDict setValue:name forKey:@"name"];
    }
    if (placeId) {
        [fullDict setValue:placeId forKey:@"place_id"];
    }
    if (address) {
        [fullDict setValue:address forKey:@"address"];
    }
    NSMutableArray * fullArray = [NSMutableArray new];
    [fullArray addObject:fullDict];
    
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kTagsAddEdit andDelegate:self andMethod:POST];
        [request setParameter:fullArray forKey:@"tags_arr"];
        request.Tag = kTagsAddEdit;
        [request startRequest];
    }
}

- (void)getLatestVideoTourWithWhere:(NSMutableDictionary *)where WithSort:(NSMutableDictionary *)sort WithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kDashboardVideoTour andDelegate:self andMethod:POST];
        Account * account = [AccountManager Instance].activeAccount;
        if(account){
            if (account.isSkip != true) {
                NSString * userId = [AccountManager Instance].activeAccount.userId;
                [request setParameter:userId forKey:@"user"];
            }
        }
        [request setParameter: where forKey:@"where"];
        [request setParameter:sort forKey:@"sort"];
        request.Tag = kDashboardVideoTour;
        [request startRequest];
    }
}

- (void)getLatestVideoTourWithParameters:(NSDictionary *)params WithBlockNew:(ItemLoadedBlock)block{
    
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",baseURL , kDashboardVideoTour] ;

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];

    if (params) {
        NSDictionary *headers = @{ @"content-type": @"application/json"};
        NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
        [request setHTTPBody:postData];
        [request setAllHTTPHeaderFields:headers];
    }
    
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            block(nil, error.description);
        } else {
            block(data, nil);
        }
    }];
    [dataTask resume];
}

- (void)getMostPopularPlansByAttractionWithParameters:(NSDictionary *)params WithBlockNew:(ItemLoadedBlock)block{
    NSDictionary *headers = @{ @"Authorization": [Utils getValueForKey:kLoginAutheticationHeader]};
    
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",baseURL , kGetMostPopularAttraction] ;

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    
    if (params) {
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"Authorization": [Utils getValueForKey:kLoginAutheticationHeader]};
        NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
        [request setHTTPBody:postData];
        [request setAllHTTPHeaderFields:headers];
    }
    else{
        [request setAllHTTPHeaderFields:headers];
    }
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            block(nil, error.description);
        } else {
            block(data, nil);
        }

    }];
    [dataTask resume];
}

- (void)getMostPopularPlansWithParameters:(NSDictionary *)params WithBlockNew:(ItemLoadedBlock)block{
    NSDictionary *headers = @{ @"Authorization": [Utils getValueForKey:kLoginAutheticationHeader]};
    
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",baseURL , kGetMostPopular] ;
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    
    if (params) {
        NSDictionary *headers = @{ @"content-type": @"application/json",
                                   @"Authorization": [Utils getValueForKey:kLoginAutheticationHeader]};
        NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
        [request setHTTPBody:postData];
        [request setAllHTTPHeaderFields:headers];
    }
    else{
        [request setAllHTTPHeaderFields:headers];
    }
    
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            block(nil, error.description);
        } else {
            block(data, nil);
        }
        
    }];
    [dataTask resume];
}

- (void)getRecommendedUsersWithBlockNew:(ItemLoadedBlock)block{
    //NSString *authToken = [Utils getValueForKey:kLoginAutheticationHeader];
    
    //NSDictionary *headers = @{ @"Authorization": authToken};
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",baseURL , kGetMostPopularUsers] ;

    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestUrl]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    //[request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            block(nil, error.description);
        } else {
            block(data, nil);
        }
    }];
    [dataTask resume];
}

- (void)getUserPlansForUserID:(NSString *)userId andWithBlock:(ItemLoadedBlock)block{
    
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@%@",baseURL , kGetUserPlans , userId] ;
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            block(nil, error.description);
        } else {
            block(data, nil);
        }
    }];
    [dataTask resume];
}

- (void)getUsersVideoTourWithWhere:(NSMutableDictionary *)where WithSort:(NSMutableDictionary *)sort WithUserId:(NSString *)userId WithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kDashboardVideoTour andDelegate:self andMethod:POST];
        [request setParameter:userId forKey:@"user"];
        [request setParameter: where forKey:@"where"];
        [request setParameter:sort forKey:@"sort"];
        request.Tag = kDashboardVideoTour;
        [request startRequest];
    }
}

//- (void)createVideoTourToPlanWithSelfDrive:(BOOL)selfDrive WithDays:(NSNumber *)days WithToDate:(NSDate *)toDate WithFromDate:(NSDate *)fromDate WithName:(NSString *)name WithisPublished:(BOOL)isPublished WithisDraft:(BOOL)isDraft WithUserid:(NSString *)userid WithTravelTip:(NSString *)traveltips WithBlock:(ItemLoadedBlock)block{
//    _itemLoadedBlock = block;
//    if ([[AppDelegate appDelegate] isReachable]) {
//        Request *request = [[Request alloc] initWithUrl:kCreateVideoTourFromPlan andDelegate:self andMethod:POST];
//        [request setParameter:[NSNumber numberWithBool:selfDrive] forKey:@"self_drive"];
//        [request setParameter: days forKey:@"days"];
//        [request setParameter:toDate forKey:@"to_date"];
//         [request setParameter:fromDate forKey:@"from_date"];
//         [request setParameter:name forKey:@"name"];
//         [request setParameter:[NSNumber numberWithBool:isPublished] forKey:@"is_published"];
//         [request setParameter:[NSNumber numberWithBool:isDraft] forKey:@"is_draft"];
//          [request setParameter:userid forKey:@"user"];
//           [request setParameter:traveltips forKey:@"travel_tip"];
//        
//        request.Tag = kCreateVideoTourFromPlan;
//        [request startRequest];
//    }
//}




-(void)getMostPopularPlanWithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        NSMutableDictionary *where = [NSMutableDictionary new];
        [where setObject:@"false" forKey:@"is_published"];
        [where setObject:[NSNumber numberWithBool:false] forKey:@"is_deleted"];
        NSMutableDictionary *sort = [NSMutableDictionary new];
        [sort setObject:@"-1" forKey:@"total_share"];
        [sort setObject:@"-1" forKey:@"total_view"];
        [sort setObject:@"-1" forKey:@"total_bookmark"];
        [sort setObject:@"-1" forKey:@"create_date"];
        Request *request = [[Request alloc] initWithUrl:kDashboardVideoTour andDelegate:self andMethod:POST];
        Account * account = [AccountManager Instance].activeAccount;
        if(account){
            if (account.isSkip != true) {
                NSString * userId = [AccountManager Instance].activeAccount.userId;
                [request setParameter:userId forKey:@"user"];
            }
        }
        [request setParameter: where forKey:@"where"];
        [request setParameter:sort forKey:@"sort"];
        request.Tag = kMostPopularPlan;
        [request startRequest];
    }
}

-(void)getMostPopularAttractionsWithBlock:(ItemLoadedBlock)block{
       _itemLoadedBlock = block;
        if ([[AppDelegate appDelegate] isReachable]){
        
        NSMutableDictionary *sort = [NSMutableDictionary new];
        [sort setObject:@"-1" forKey:@"total_share"];
        [sort setObject:@"-1" forKey:@"total_view"];
        [sort setObject:@"-1" forKey:@"total_bookmark"];
        [sort setObject:@"-1" forKey:@"create_date"];
        Request *request = [[Request alloc] initWithUrl:kMostpopularAttractionData andDelegate:self andMethod:POST];
       
        [request setParameter:sort forKey:@"sort"];
        request.Tag = kMostpopularAttractionData;
        [request startRequest];
    }
    
}

-(void)getMostPopularVideoTourWithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        NSMutableDictionary *where = [NSMutableDictionary new];
        [where setObject:@"true" forKey:@"is_published"];
        [where setObject:[NSNumber numberWithBool:false] forKey:@"is_deleted"];
        NSMutableDictionary *sort = [NSMutableDictionary new];
        [sort setObject:@"-1" forKey:@"total_share"];
        [sort setObject:@"-1" forKey:@"total_view"];
        [sort setObject:@"-1" forKey:@"total_bookmark"];
        [sort setObject:@"-1" forKey:@"create_date"];
        Request *request = [[Request alloc] initWithUrl:kDashboardVideoTour andDelegate:self andMethod:POST];
        Account * account = [AccountManager Instance].activeAccount;
        if(account){
            if (account.isSkip != true) {
                NSString * userId = [AccountManager Instance].activeAccount.userId;
                [request setParameter:userId forKey:@"user"];
            }
        }
        [request setParameter: where forKey:@"where"];
        [request setParameter:sort forKey:@"sort"];
        request.Tag = kMostPopularVideoTour;
        [request startRequest];
    }
}

-(void)getVideoTourWithDistrictIdDict:(NSMutableDictionary *)dict WithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kDashboardVideoTour andDelegate:self andMethod:POST];
        [request setParameter: dict forKey:@"where"];
        request.Tag = kDashboardVideoTour;
        [request startRequest];
    }
}

-(void)uploadVideoWithVideoData:(NSData *)videoData WithAttrId:(NSString *)attr_id WithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        
        
        Request *request = [[Request alloc] initWithUrl:kUploadVideo andDelegate:self andMethod:MULTI_PART_FORM];
        NSString *videoURL = [[NSBundle mainBundle] pathForResource:@"LateForWork" ofType:@"mp4"];
        NSData *videoData1 = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath: videoURL]];
        [request setParameter:attr_id forKey:@"attr_id"];
        [request setParameter:videoData1 forKey:@"file"];
        request.Tag = kUploadVideo;
        [request startRequest];
    }
}

-(void)uploadVideoWithVideoUrl:(NSURL *)vidUrl WithAttrId:(NSString *)attr_id WithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        
        
        Request *request = [[Request alloc] initWithUrl:kUploadVideo andDelegate:self andMethod:MULTI_PART_FORM];

        NSString * a = [vidUrl path];
        [request setParameter:a forKey:@"path"];
        [request setParameter:vidUrl forKey:@"url"];
        
//        [request setParameter:attr_id forKey:@"attr_id"];
        request.Tag = kUploadVideo;
        [request startRequest];
    }
}

-(void)addCountForModel:(NSString *)modelName withType:(NSString *)typeName forId:(NSString *)idName WithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kCountsCommon andDelegate:self andMethod:POST];
        [request setParameter: modelName forKey:@"model"];
        [request setParameter: typeName forKey:@"type"];
        [request setParameter: idName forKey:@"_id"];
        Account * account = [AccountManager Instance].activeAccount;
        if(account){
            if (account.isSkip != true) {
                NSString * userId = [AccountManager Instance].activeAccount.userId;
                [request setParameter:userId forKey:@"user_id"];
            }
        }
        request.Tag = kCountsCommon;
        [request startRequest];
    }
}

-(void)LoadBookmarkDetails:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kTagsBookmarkDetails andDelegate:self];
        request.Tag = kTagsBookmarkDetails;
        [request startRequest];
    }
}

-(void)saveSearchDataForModel:(NSString *)modelName WithString:(NSString *)str WithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kSaveSearchString andDelegate:self andMethod:POST];
        Account * account = [AccountManager Instance].activeAccount;
        if(account){
            if (account.isSkip != true) {
                NSString * userId = [AccountManager Instance].activeAccount.userId;
                [request setParameter:userId forKey:@"user"];
            }
        }
        [request setParameter:modelName forKey:@"type"];
        [request setParameter:str forKey:@"search_str"];
        request.Tag = kSaveSearchString;
        [request startRequest];
    }
}

-(void)getMostPopularSearchDataForVideoTour:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        NSMutableDictionary *where = [NSMutableDictionary new];
        [where setObject:@"false" forKey:@"is_deleted"];
        [where setObject:@"video_tour" forKey:@"type"];
        NSMutableDictionary *sort = [NSMutableDictionary new];
        [sort setObject:@"-1" forKey:@"search_count"];
        NSMutableDictionary *limit = [NSMutableDictionary new];
        [limit setObject:@"3" forKey:@"limit"];
        
        Request *request = [[Request alloc] initWithUrl:kMostPopularSearch andDelegate:self andMethod:POST];
        [request setParameter:where forKey:@"where"];
        [request setParameter:sort forKey:@"sort"];
        [request setParameter:limit forKey:@"limit"];
        request.Tag = kMostPopularSearch;
        [request startRequest];
    }
}

-(void)getMostPopularSearchDataForAttraction:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        NSMutableDictionary *where = [NSMutableDictionary new];
        [where setObject:@"false" forKey:@"is_deleted"];
        [where setObject:@"attraction" forKey:@"type"];
        NSMutableDictionary *sort = [NSMutableDictionary new];
        [sort setObject:@"-1" forKey:@"search_count"];
        NSMutableDictionary *limit = [NSMutableDictionary new];
        [limit setObject:@"3" forKey:@"limit"];
        
        Request *request = [[Request alloc] initWithUrl:kMostPopularSearch andDelegate:self andMethod:POST];
        [request setParameter:where forKey:@"where"];
        [request setParameter:sort forKey:@"sort"];
        [request setParameter:limit forKey:@"limit"];
        request.Tag = kMostPopularSearchAttraction;
        [request startRequest];
    }
}

-(void)searchVideoTourWithName:(NSString *)name andLanguageArray:(NSMutableArray *)arrLanguage andStartDate:(NSString *)sDate andEndDate:(NSString *)eDate andSelfDrive:(BOOL)selfDrive WithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kSearchVideoTour andDelegate:self andMethod:POST];
        if (name.length > 0) {
            [request setParameter:name forKey:@"name"];
        }
        if (sDate.intValue > 0 && eDate.intValue > 0) {
            NSMutableDictionary *days = [NSMutableDictionary new];
            [days setObject:sDate forKey:@"$gte"];
            [days setObject:eDate forKey:@"$lte"];
            [request setParameter:days forKey:@"days"];
        }
        //        [request setParameter:arrLanguage forKey:@"language"];
        
        [request setParameter:[NSNumber numberWithBool:selfDrive] forKey:@"self_drive"];
        request.Tag = kSearchVideoTour;
        [request startRequest];
    }
}

-(void)searchAttractionWithCatArray:(NSMutableArray *)catArray andLanguageArray:(NSMutableArray *)langArray andSearchStr:(NSString *)name WithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kSearchAttraction andDelegate:self andMethod:POST];
        if (name.length > 0) {
            [request setParameter:name forKey:@"search_str"];
        }
        if (catArray.count > 0) {
            //            [request setParameter:catArray forKey:@"category"];
        }
        if (langArray.count > 0) {
            //            [request setParameter:langArray forKey:@"language"];
        }
        
        request.Tag = kSearchAttraction;
        [request startRequest];
    }
}

- (void)getCmsDataWithSearchString:(NSString *)searchStr WithLattitude:(double)latitude WithLongitude:(double)longitude WithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kSearchDistrictString andDelegate:self andMethod:POST];
        [request setParameter:searchStr forKey:@"searchStr"];
        [request setParameter: [NSNumber numberWithDouble:latitude] forKey:@"latitude"];
        [request setParameter:[NSNumber numberWithDouble:longitude]  forKey:@"longitude"];
        request.Tag = kGetCmsData;
        [request startRequest];
    }
}

-(void)createAttrDataWithId:(NSString *)attrDataId andTime:(NSString *)timeStr andDesc:(NSString *)descStr andVideoId:(NSString *)videoId WithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kCreateAttrData andDelegate:self andMethod:POST];
        [request setParameter:attrDataId forKey:@"_id"];
        [request setParameter:timeStr forKey:@"time"];
        [request setParameter:descStr forKey:@"desc"];
        [request setParameter:videoId forKey:@"video_id"];
        request.Tag = kCreateAttrData;
        [request startRequest];
    }
}

-(void)deleteAttractionWithId:(NSString *)attraction andplanId:(NSString *)plan andDay:(NSString *)day WithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kDeleteAttraction andDelegate:self andMethod:POST];
        [request setParameter:day forKey:@"day"];
        [request setParameter:plan forKey:@"plan"];
        [request setParameter:attraction forKey:@"attraction"];
        request.Tag = kDeleteAttraction;
        [request startRequest];
    }
}

-(void)deleteDayForPlanId:(NSString *)plan andDay:(NSString *)day_id WithBlock:(ItemLoadedBlock)block{
    _itemLoadedBlock = block;
    if ([[AppDelegate appDelegate] isReachable]) {
        Request *request = [[Request alloc] initWithUrl:kDeleteDay andDelegate:self andMethod:POST];
        [request setParameter:day_id forKey:@"day_id"];
        [request setParameter:plan forKey:@"plan"];
        request.Tag = kDeleteDay;
        [request startRequest];
    }
}

- (void)uploadCoverPhoto:(UIImage *)img ForPlanId:(NSString *)plan_id WithBlock:(ItemLoadedBlock)block {
    _itemLoadedBlock = block;
    Request *request = [[Request alloc] initWithUrl:kUploadCoverPhoto andDelegate:self andMethod:MULTI_PART_FORM];
    NSData *imageData = [NSData dataWithData:UIImageJPEGRepresentation(img, 1.0)];
    [request setParameter:imageData forKey:@"file"];
    [request setParameter:plan_id forKey:@"plan_id"];
    request.Tag = kUploadUserAvatar;
    [request startRequest];
}

-(void)updateMultipleAttrData:(NSMutableArray *)data ForPlanId:(NSString *)plan_id WithBlock:(ItemLoadedBlock)block {
    _itemLoadedBlock = block;
    Request *request = [[Request alloc] initWithUrl:kUpdateMultipleAttrData andDelegate:self andMethod:POST];
    [request setParameter:data forKey:@"data"];
    [request setParameter:plan_id forKey:@"plan_id"];
    request.Tag = kUpdateMultipleAttrData;
    [request startRequest];
}

-(void)editPlanWithId:(NSString *)plan_id WithName:(NSString *)name WithTravelTip:(NSString *)travel_tip WithFromDate:(NSString *)from_date WithToDate:(NSString *)to_date WithDays:(NSString *)days andSelfDrive:(BOOL)selfDrive WithBlock:(ItemLoadedBlock)block {
    
    _itemLoadedBlock = block;
    Request *request = [[Request alloc] initWithUrl:kCreatePlan andDelegate:self andMethod:POST];
    [request setParameter:plan_id forKey:@"_id"];
    [request setParameter:name forKey:@"name"];
    [request setParameter:from_date forKey:@"from_date"];
    [request setParameter:to_date forKey:@"to_date"];
    [request setParameter:days forKey:@"days"];
    [request setParameter:@"plan" forKey:@"type"];
    [request setParameter:[NSNumber numberWithBool:selfDrive] forKey:@"self_drive"];

    if (travel_tip.length > 0) {
        [request setParameter:travel_tip forKey:@"travel_tip"];
    }
    request.Tag = kEditPlan;
    [request startRequest];
}

-(void)createVideoTourForPlan:(NSString *)plan_id WithBlock:(ItemLoadedBlock)block {
    _itemLoadedBlock = block;
    Request *request = [[Request alloc] initWithUrl:kCreateVideoTour andDelegate:self andMethod:POST];
    [request setParameter:plan_id forKey:@"_id"];
    request.Tag = kCreateVideoTour;
    [request startRequest];
}

-(void)createVideoTourWithAllDetails:(NSMutableDictionary *)planDict WithAttractionArray:(NSMutableArray *)attrArray WithBlock:(ItemLoadedBlock)block {
    _itemLoadedBlock = block;
    Request *request = [[Request alloc] initWithUrl:kCreateVideoTourAllDetails andDelegate:self andMethod:POST];
    [request setParameter:planDict forKey:@"plan"];
    [request setParameter:attrArray forKey:@"attractions"];
//    NSMutableDictionary * dict = [NSMutableDictionary new];
//    [dict setValue:planDict forKey:@"plan"];
//    [dict setValue:attrArray forKey:@"attractions"];
//      [request setParameter:dict forKey:@""];
    
    request.Tag = kCreateVideoTourAllDetails;
    [request startRequest];
}

-(void)getAllAttrDataWithBlock:(ItemLoadedBlock)block {
    _itemLoadedBlock = block;
    Request *request = [[Request alloc] initWithUrl:kGetAllAttrData andDelegate:self];
    request.Tag = kGetAllAttrData;
    [request startRequest];
}


-(void)addAttractionWithName:(NSString *)name andCatId:(NSMutableArray *)category AndAddress:(NSString *)address andLocArray:(NSMutableArray *)loc forUserId:(NSString *)user WithBlockNew:(ItemLoadedBlock)block {
    _itemLoadedBlock = block;
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    

    NSString *requestUrl = [NSString stringWithFormat:@"%@/%@",baseURL , kCreateAttraction] ;

    [manager.requestSerializer setValue:[Utils getValueForKey:kLoginAutheticationHeader] forHTTPHeaderField:@"Authorization"];
    
    [manager POST:requestUrl parameters:@{ @"name": name,
                                           @"category": category,
                                           @"address": address,
                                           @"loc": loc,
                                           @"user": user } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {

                                               NSDictionary *data = [responseObject valueForKey:@"data"];
                                               NSString * aStr = [data valueForKey:@"_id"];

                                               
                                               
                                               NSLog(@"id - %@" , aStr);
                                               block(responseObject, nil);
                                               
                                               
    } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
        block(nil, error.description);

    }];
}


-(void)addAttractionWithName:(NSString *)name andCatId:(NSString *)category AndAddress:(NSString *)address andLocArray:(NSMutableArray *)loc andDescription:(NSString *)description andPhoneNo:(NSString *)phoneNo andPhotoUrl:(NSString *)photoUrl andCountryId:(NSString *)countryId andDistrictId:(NSString *)districtId andCityId:(NSString *)cityId forUserId:(NSString *)user WithBlockNew:(ItemLoadedBlock)block {
    
    _itemLoadedBlock = block;
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    
    [manager.requestSerializer setValue:[Utils getValueForKey:kLoginAutheticationHeader] forHTTPHeaderField:@"Authorization"];
    
    
    NSString *requestUrl = [NSString stringWithFormat:@"%@/%@",baseURL , kCreateAttraction] ;
    
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:loc options:NSJSONWritingPrettyPrinted error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    NSDictionary *params = @{ @"name": name,
                              @"category": category,
                              @"address": address,
                              @"description": description,
                              @"phone_no": phoneNo,
                              @"cover_photo_url": photoUrl,
                              @"country_id": countryId,
                              @"district_id": districtId,
                              @"city_id": cityId,
                              @"loc": jsonString,
                              @"user": user,
                              @"loc_lat": loc[0],
                              @"loc_long": loc[1],
                              @"detail" : description,
                              @"is_deleted" : @NO};
    
    [manager POST:requestUrl parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nonnull responseObject) {
                                               
                                               NSDictionary *data = [responseObject valueForKey:@"data"];
                                               NSString * aStr = [data valueForKey:@"_id"];
                                               
                                               
                                               
                                               NSLog(@"id - %@" , aStr);
                                               block(responseObject, nil);
                                               
                                               
                                           } failure:^(NSURLSessionDataTask * _Nonnull task, NSError * _Nonnull error) {
                                               block(nil, error.description);
                                               
                                           }];
}




-(void)addAttractionWithName:(NSString *)name andCatId:(NSMutableArray *)category AndAddress:(NSString *)address andLocArray:(NSMutableArray *)loc forUserId:(NSString *)user WithBlock:(ItemLoadedBlock)block {
    _itemLoadedBlock = block;
    Request *request = [[Request alloc] initWithUrl:kCreateAttraction andDelegate:self andMethod:POST];
    [request setParameter:name forKey:@"name"];
    [request setParameter:category forKey:@"category"];
    [request setParameter:address forKey:@"address"];
    [request setParameter:loc forKey:@"loc"];
    [request setParameter:user forKey:@"user"];
    request.Tag = kCreateAttraction;
    [request startRequest];
}

#pragma mark -RequestDelegate

-(void)RequestDidSuccess:(Request *)request{
    if (request.IsSuccess) {
        if ([request.Tag isEqualToString:kTagsBookmarkDetails]) {
            [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
                [BookmarkDetail MR_truncateAllInContext:localContext];
                [BookmarkUser MR_truncateAllInContext:localContext];
                [BookmarkLocation MR_truncateAllInContext:localContext];
                [BookmarkAttractions MR_truncateAllInContext:localContext];
                [BookmarkVideotour MR_truncateAllInContext:localContext];
                
                NSMutableDictionary * dictBookmark = [self createTrickyJsonForBookMarkUserDetails:[request.serverData objectForKey:@"data"]];
                
                NSMutableDictionary *dictDetails = [self createTrickyJsonForDetailsLocationValues:dictBookmark];
                
                id bookmark = [FEMDeserializer objectFromRepresentation:dictDetails mapping:[BookmarkDetail defaultMapping] context:localContext];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (_itemLoadedBlock) {
                        _itemLoadedBlock(bookmark, nil);
                    }
                });
            }];
        }
        
        if ([request.Tag isEqualToString:kCreatePlan]) {
            if (_itemLoadedBlock) {
                _itemLoadedBlock(nil, nil);
            }
        } else if ([request.Tag isEqualToString:kCreateAttraction]) {
            if (_itemLoadedBlock) {
                _itemLoadedBlock([request.serverData objectForKey:@"data"], nil);
            }
        } else if ([request.Tag isEqualToString:kUploadUserAvatar]) {
            if (_itemLoadedBlock) {
                _itemLoadedBlock([request.serverData objectForKey:@"data"], nil);
            }
        } else if ([request.Tag isEqualToString:kUpdateMultipleAttrData]) {
            if (_itemLoadedBlock) {
                _itemLoadedBlock([request.serverData objectForKey:@"data"], nil);
            }
        } else if ([request.Tag isEqualToString:kCreateVideoTourAllDetails]) {
            if (_itemLoadedBlock) {
                _itemLoadedBlock([request.serverData objectForKey:@"data"], nil);
            }
        } else if ([request.Tag isEqualToString:kCreateVideoTour]) {
            if (_itemLoadedBlock) {
                _itemLoadedBlock([request.serverData objectForKey:@"data"], nil);
            }
        }  else if ([request.Tag isEqualToString:kEditPlan]) {
            if (_itemLoadedBlock) {
                _itemLoadedBlock([request.serverData objectForKey:@"data"], nil);
            }
        } else if ([request.Tag isEqualToString:kAddSpamToPlan]) {
            if (_itemLoadedBlock) {
                _itemLoadedBlock([request.serverData objectForKey:@"data"], nil);
            }
        } else if ([request.Tag isEqualToString:kDeleteDay]) {
            if (_itemLoadedBlock) {
                _itemLoadedBlock([request.serverData objectForKey:@"data"], nil);
            }
        } else if ([request.Tag isEqualToString:kDeleteAttraction]) {
            if (_itemLoadedBlock) {
                _itemLoadedBlock([request.serverData objectForKey:@"data"], nil);
            }
        } else if ([request.Tag isEqualToString:kTagsAddEdit]) {
            if (_itemLoadedBlock) {
                _itemLoadedBlock([request.serverData objectForKey:@"data"], nil);            }
        } else if ([request.Tag isEqualToString:kUploadVideo]) {
            if (_itemLoadedBlock) {
                _itemLoadedBlock([request.serverData objectForKey:@"data"], nil);
            }
        } else if ([request.Tag isEqualToString:kSaveSearchString]) {
            if (_itemLoadedBlock) {}
        }  else if ([request.Tag isEqualToString:kCountsCommon]) {
            if (_itemLoadedBlock) {
                _itemLoadedBlock([request.serverData objectForKey:@"data"], nil);
            }
        } else if ([request.Tag isEqualToString:kSearchDistrictString]) {
            if (_itemLoadedBlock) {
                _itemLoadedBlock([request.serverData objectForKey:@"data"], nil);
            }
        } else if ([request.Tag isEqualToString:kCreateAttrData]) {
            if (_itemLoadedBlock) {
                _itemLoadedBlock([request.serverData objectForKey:@"data"], nil);
            }
        } else if ([request.Tag isEqualToString:kDeleteTour]) {
            if (_itemLoadedBlock) {
                _itemLoadedBlock([request.serverData objectForKey:@"data"], nil);
            }
        } else if ([request.Tag isEqualToString:  kSearchVideoTour]) {
            [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
                [VideoTour MR_truncateAllInContext:localContext];
//                id appt1 = [self createTrickyJsonForBookMarkUserDetails:[request.serverData objectForKey:@"data"]];
                NSArray * aRray = [request.serverData objectForKey:@"data"];
                if (aRray.count > 0) {
                    id appts = [FEMDeserializer collectionFromRepresentation:[request.serverData objectForKey:@"data"] mapping:[VideoTour defaultMapping] context:localContext];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (_itemLoadedBlock) {
                            _itemLoadedBlock(appts, nil);
                        }
                    });
                } else {
                    NSString * aString = [NSString stringWithFormat:@"No data found"];
                    _itemLoadedBlock(nil, aString);
                }
            }];
        } else if([request.Tag isEqual:kSearchAttraction]){
            
            [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
                [Attractions MR_truncateAllInContext:localContext];
//                NSMutableArray *array1 = [self createTrickyJsonForAttractionLocationValues:[request.serverData objectForKey:@"data"]];
//                NSMutableArray * array2 = [self createTrickyJsonForBookmarkUsers:array1];
                NSArray *appts = [FEMDeserializer collectionFromRepresentation:[request.serverData objectForKey:@"data"] mapping:[Attractions defaultMapping] context:localContext];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (_itemLoadedBlock) {
                        _itemLoadedBlock(appts, nil);
                    }
                });
            }];
        } else if([request.Tag isEqual:kGetAllAttrData]){
            [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
//                [AttractionData MR_truncateAllInContext:localContext];
                NSMutableArray *appt = [self createTrickyJsonForAttractionDataLocationValues:[request.serverData objectForKey:@"data"]];
                NSArray *appts = [FEMDeserializer collectionFromRepresentation:appt mapping:[AttractionData defaultMapping] context:localContext];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (_itemLoadedBlock) {
                        _itemLoadedBlock(appts, nil);
                    }
                });
            }];
        }else if([request.Tag isEqual:kMostpopularAttractionData]){
            [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
                [MostPopularAttractions MR_truncateAllInContext:localContext];
                NSMutableArray * arrBookmark = [self createTrickyJsonForBookmarkUsers:[request.serverData objectForKey:@"data"]];
                
                NSMutableArray *appt = [self createTrickyJsonForAttractionLocationValues:arrBookmark];
                NSArray *appts = [FEMDeserializer collectionFromRepresentation:appt mapping:[MostPopularAttractions defaultMapping] context:localContext];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (_itemLoadedBlock) {
                        _itemLoadedBlock(appts, nil);
                    }
                });
            }];
        } else if([request.Tag isEqual:kPlanBYUserID]){
            [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
                [Plan MR_truncateAllInContext:localContext];
                id appt1 = [self createTrickyJsonForBookmarkUsers:[request.serverData objectForKey:@"data"]];
                id appts = [FEMDeserializer collectionFromRepresentation:appt1 mapping:[Plan defaultMapping] context:localContext];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (_itemLoadedBlock) {
                        _itemLoadedBlock(appts, nil);
                    }
                });
            }];
        } else if([request.Tag isEqual:kDashboardUser]){
            [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
                [Users MR_truncateAllInContext:localContext];
                id appt1 = [self createTrickyJsonForBookmarkUsers:[request.serverData objectForKey:@"data"]];
                NSArray *appts = [FEMDeserializer collectionFromRepresentation:appt1 mapping:[Users defaultMapping] context:localContext];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (_itemLoadedBlock) {
                        _itemLoadedBlock(appts, nil);
                    }
                });
            }];
        }else if([request.Tag isEqual:kGetAllCategory]){
            [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
                [Categori MR_truncateAllInContext:localContext];
                NSArray *appts = [FEMDeserializer collectionFromRepresentation:[request.serverData objectForKey:@"data"] mapping:[Categori defaultMapping] context:localContext];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (_itemLoadedBlock) {
                        _itemLoadedBlock([request.serverData objectForKey:@"data"], nil);
                    }
                });
            }];
        } else if([request.Tag isEqual:kGetDistricts]){
            [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
                [District MR_truncateAllInContext:localContext];
                NSArray *appts = [FEMDeserializer collectionFromRepresentation:[request.serverData objectForKey:@"data"] mapping:[District defaultMapping] context:localContext];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (_itemLoadedBlock) {
                        _itemLoadedBlock(appts, nil);
                    }
                });
            }];
        } else if([request.Tag isEqual:kGetReports]){
            [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
                [Reason MR_truncateAllInContext:localContext];
                NSArray *appts = [FEMDeserializer collectionFromRepresentation:[request.serverData objectForKey:@"data"] mapping:[Reason defaultMapping] context:localContext];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (_itemLoadedBlock) {
                        _itemLoadedBlock(appts, nil);
                    }
                });
            }];
        }else if([request.Tag isEqual:kGetCmsData]){
            [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
                [SearchData MR_truncateAllInContext:localContext];
                NSMutableArray *appt = [self createTrickyJsonForAttractionLocationValues:[request.serverData objectForKey:@"data"]];
                NSArray *appts = [FEMDeserializer collectionFromRepresentation:appt mapping:[SearchData defaultMapping] context:localContext];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (_itemLoadedBlock) {
                        _itemLoadedBlock(appts, nil);
                    }
                });
            }];
        }else if([request.Tag isEqual:kGetAllAttractions]){
            [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
                [Attractions MR_truncateAllInContext:localContext];
                NSMutableArray *array1 = [self createTrickyJsonForAttractionLocationValues:[request.serverData objectForKey:@"data"]];
                NSMutableArray * array2 = [self createTrickyJsonForBookmarkUsers:array1];
                NSArray *appts = [FEMDeserializer collectionFromRepresentation:array2 mapping:[Attractions defaultMapping] context:localContext];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (_itemLoadedBlock) {
                        _itemLoadedBlock(appts, nil);
                    }
                });
            }];
        } else if ([request.Tag isEqualToString:kAddAttractionToMyPlan]) {
            NSDictionary *dictData = request.serverData;
            NSDictionary *dictUser = [dictData objectForKey:@"data"];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (_itemLoadedBlock) {
                    _itemLoadedBlock(dictUser, nil);
                }
            });
        } else if([request.Tag isEqual:kTermsAndConditions]){
            NSDictionary *dictData = request.serverData;
            NSDictionary *dictUser = [dictData objectForKey:@"data"];
            dispatch_async(dispatch_get_main_queue(), ^{
                if (_itemLoadedBlock) {
                    _itemLoadedBlock(dictUser, nil);
                }
            });
        } else if([request.Tag isEqual:kDashboardVideoTour]){
            [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
                [VideoTour MR_truncateAllInContext:localContext];
//                [Attractions MR_truncateAllInContext:localContext];
//                [AttractionData MR_truncateAllInContext:localContext];
                id appt1 = [self createTrickyJsonForBookmarkUsers:[request.serverData objectForKey:@"data"]];
                id appts = [FEMDeserializer collectionFromRepresentation:appt1 mapping:[VideoTour defaultMapping] context:localContext];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (_itemLoadedBlock) {
                        _itemLoadedBlock(appts, nil);
                    }
                });
            }];
        } else if([request.Tag isEqual:kMostPopularPlan]){
            [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
                [MostPopularPlan MR_truncateAllInContext:localContext];
                id appt1 = [self createTrickyJsonForBookmarkUsers:[request.serverData objectForKey:@"data"]];
                id appts = [FEMDeserializer collectionFromRepresentation:appt1 mapping:[MostPopularPlan defaultMapping] context:localContext];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (_itemLoadedBlock) {
                        _itemLoadedBlock(appts, nil);
                    }
                });
            }];
        } else if([request.Tag isEqual:kMostPopularVideoTour]){
            [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
                [MostPopularVideoTour MR_truncateAllInContext:localContext];
                id appt1 = [self createTrickyJsonForBookmarkUsers:[request.serverData objectForKey:@"data"]];
                id appts = [FEMDeserializer collectionFromRepresentation:appt1 mapping:[MostPopularVideoTour defaultMapping] context:localContext];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (_itemLoadedBlock) {
                        _itemLoadedBlock(appts, nil);
                    }
                });
            }];
        } else if([request.Tag isEqual:kMostPopularSearch]){
            [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
                [MostPopularSearchVideoTour MR_truncateAllInContext:localContext];
                
                NSArray *appts = [FEMDeserializer collectionFromRepresentation:[request.serverData objectForKey:@"data"] mapping:[MostPopularSearchVideoTour defaultMapping] context:localContext];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (_itemLoadedBlock) {
                        _itemLoadedBlock(appts, nil);
                    }
                });
            }];
        }else if([request.Tag isEqual:kMostPopularSearchAttraction]){
            [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
                [MostPopularSearchAttraction MR_truncateAllInContext:localContext];
                
                NSArray *appts = [FEMDeserializer collectionFromRepresentation:[request.serverData objectForKey:@"data"] mapping:[MostPopularSearchAttraction defaultMapping] context:localContext];
                dispatch_async(dispatch_get_main_queue(), ^{
                    if (_itemLoadedBlock) {
                        _itemLoadedBlock(appts, nil);
                    }
                });
            }];
        }
    }
}

-(void)RequestDidFailForRequest:(Request *)request withError:(NSError *)error{
    if (_itemLoadedBlock) {
        if ([error.userInfo objectForKey:@"errorMessage"] && [error.userInfo objectForKey:@"errorMessage"] != nil) {
            _itemLoadedBlock(nil, [error.userInfo objectForKey:@"errorMessage"]);
        } else {
            NSString * aString = [error.userInfo valueForKey:@"message"];
            _itemLoadedBlock(nil, aString);
        }
    }
}

-(void)getAllCountriesDataWithBlock:(ItemLoadedBlock)block{
    NSString *requestURL = [NSString stringWithFormat:@"%@%@",baseURL, kGetAllCountries];
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager GET:requestURL parameters:nil success:^(NSURLSessionTask *task, id responseObject) {
        block(responseObject, nil);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        block(nil, error.description);
    }];
}
-(void)getAllDistrictsOdCountryId:(NSString *)countryId DataWithBlock:(ItemLoadedBlock)block{
    NSString *requestURL = [NSString stringWithFormat:@"%@%@",baseURL, kGetAllDistricts];
    NSDictionary *headers = @{ @"content-type": @"application/json"};
    NSDictionary *parameters = @{ @"where": @{ @"country": countryId } };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            block(nil, error.description);
            
        } else {
            NSError *errorJson=nil;
            NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorJson];
            
            block(responseDict , nil);
        }
    }];
    [dataTask resume];
}
-(void)getAllCitiesOfDistrictId:(NSString *)districtId DataWithBlock:(ItemLoadedBlock)block{
    NSString *requestURL = [NSString stringWithFormat:@"%@%@",baseURL, kGetAllCities];
    
    NSDictionary *headers = @{ @"content-type": @"application/json"};
    NSDictionary *parameters = @{ @"where": @{ @"district": districtId } };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            block(nil, error.description);

        } else {
            NSError *errorJson=nil;
            NSDictionary* responseDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&errorJson];
            
            block(responseDict , nil);
        }
    }];
    [dataTask resume];
}
- (void)getAllLocationWithDictionary:(NSDictionary *)dict andBlockNew:(ItemLoadedBlock)block{
    NSString *requestURL = [NSString stringWithFormat:@"%@%@",baseURL, kGetAllCountriesNew];
    
//    NSString *authToken = [Utils getValueForKey:kLoginAutheticationHeader]; //TODO returns nil
//    if (!authToken) {
//        return;
//    }
    
    //NSDictionary *headers = @{ @"Authorization": authToken,
                              // @"cache-control": @"no-cache"};
    NSDictionary *headers = @{@"cache-control": @"no-cache"};
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    if (dict) {
//        headers = @{ @"Authorization": authToken,
//                     @"content-type": @"application/json",
//                     @"cache-control": @"no-cache"};
        
        headers = @{ @"content-type": @"application/json",
                     @"cache-control": @"no-cache"};
        NSData *postData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
        [request setHTTPBody:postData];
    }
    [request setAllHTTPHeaderFields:headers];

    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            block(nil, error.description);
        } else {
            block(data, nil);
        }
    }];
    [dataTask resume];
}

-(void)saveEditedPlanForPlan :(PlanModel*)planModel withBlockNew:(ItemLoadedBlock)block{
    
    NSString *requestURL = [NSString stringWithFormat:@"%@%@",baseURL, kCreatePlan];
    NSString *authToken = [Utils getValueForKey:kLoginAutheticationHeader];
    NSDictionary *headers = @{ @"Authorization": authToken,
                               @"content-type": @"application/json",
                               @"cache-control": @"no-cache"};
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    
    NSDictionary *params = @{ @"_id": planModel._id,
                               @"name": planModel.name,
                               @"from_date": planModel.from_date,
                              @"to_date": planModel.to_date,
                              @"days": planModel.days,
                              @"type": @"plan",
                              @"self_drive": planModel.self_drive,
                              @"travel_tip": planModel.travel_tip
                              };
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
    [request setHTTPBody:postData];

    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            block(nil, error.description);
        } else {
            block(data, nil);
        }
    }];
    [dataTask resume];
    
}

-(void)searchAttractionWithDictionary:(NSDictionary *)params WithBlock:(ItemLoadedBlock)block{
    
    NSString *requestURL = [NSString stringWithFormat:@"%@%@",baseURL, kSearchAttractionNew];

    NSDictionary *headers = @{ @"content-type": @"application/json" };
    
    
    //NSDictionary *parameters = @{ @"where": @{ @"name": @"", @"category": @"58d136a72e0fbe7e5867e363" } };
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:params options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            block(nil, error.description);
        } else {
            block(data, nil);
        }
    }];
    [dataTask resume];
}
-(void)getAllCategoriesWithBlock:(ItemLoadedBlock)block{
    NSString *requestURL = [NSString stringWithFormat:@"%@%@",baseURL, kGetAllCategory];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            block(nil, error.description);
        } else {
            block(data, nil);
        }
    }];
    [dataTask resume];
}
- (void)addAttractionToMyPlanWithDictionary:(NSDictionary *)dict withBlock:(ItemLoadedBlock)block{
    NSString *requestURL = [NSString stringWithFormat:@"%@%@",baseURL, kAddAttractionToMyPlan];

    
    NSString *authToken = [Utils getValueForKey:kLoginAutheticationHeader];
    NSDictionary *headers = @{ @"Authorization": authToken,
                               @"content-type": @"application/json"};
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestURL] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            block(nil, error.description);
        } else {
            block(data, nil);
        }
    }];
    [dataTask resume];
}

-(void)loadVideoToursByUserID:(NSString *)userId WithBlockNew:(ItemLoadedBlock)block{
    
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",baseURL , kDashboardVideoTour] ;
    NSDictionary *headers = @{ @"content-type": @"application/json" };
    NSDictionary *parameters = @{ @"where": @{ @"type": @"videotour", @"user": userId }, @"sort": @{ @"create_date": @-1 } };
    
    //NSDictionary *parameters = @{ @"where": @{ @"user": userId }, @"sort": @{ @"create_date": @-1 } };
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            block(nil ,error.description);
        } else {
            block(data, nil);
        }
    }];
    [dataTask resume];
}

-(void)loadAttractionsWithDataWithBlockNew:(ItemLoadedBlock)block{
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",baseURL , kGetAllAttractionsWithData] ;
    NSDictionary *headers = @{ @"cache-control": @"no-cache",
                               @"postman-token": @"66d56584-2b32-1d64-07dd-c8c368122d23" };
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            block(nil ,error.description);
        } else {
            block(data, nil);
        }
    }];
    [dataTask resume];
}

- (void)getMostPopularAttractionVideosWithBlockNew:(ItemLoadedBlock)block{
    NSDictionary *headers = @{ @"content-type": @"application/json"};
    NSDictionary *parameters = @{ @"limit": @{ @"skip": @"", @"limit": @20 } };
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",baseURL , kGetMostPopularAttractionVideo] ;
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:requestUrl] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:10.0];
    [request setHTTPMethod:@"POST"];
    [request setAllHTTPHeaderFields:headers];
    [request setHTTPBody:postData];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            block(nil ,error.description);
        } else {
            block(data, nil);
        }
    }];
    [dataTask resume];
}

- (void)uploadAvatarWithImage:(UIImage *)image WithBlockNew:(ItemLoadedBlock)block{
    NSString *authToken = [Utils getValueForKey:kLoginAutheticationHeader];
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",baseURL , kUploadUserAvatar] ;
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:requestUrl]];
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW" forHTTPHeaderField:@"content-type"];
    [manager.requestSerializer setValue:authToken forHTTPHeaderField:@"authorization"];
    [manager.requestSerializer setValue:@"no-cache" forHTTPHeaderField:@"cache-control"];
    AFHTTPRequestOperation *op = [manager POST:requestUrl parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //do not put image inside parameters dictionary as I did, but append it!
        [formData appendPartWithFileData:imageData name:@"file" fileName:@"avatar.jpg" mimeType:@"image/jpeg"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
        block(responseObject, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
        block(nil ,error.description);
    }];
    [op start];
}

- (void)uploadCoverWithImage:(UIImage *)image WithBlockNew:(ItemLoadedBlock)block{
    NSString *authToken = [Utils getValueForKey:kLoginAutheticationHeader];
    NSString *requestUrl = [NSString stringWithFormat:@"%@%@",baseURL , kUploadUserCoverPic] ;
    AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:requestUrl]];
    NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    [manager.requestSerializer setValue:@"multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW" forHTTPHeaderField:@"content-type"];
    [manager.requestSerializer setValue:authToken forHTTPHeaderField:@"authorization"];
    [manager.requestSerializer setValue:@"no-cache" forHTTPHeaderField:@"cache-control"];
    AFHTTPRequestOperation *op = [manager POST:requestUrl parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        //do not put image inside parameters dictionary as I did, but append it!
        [formData appendPartWithFileData:imageData name:@"file" fileName:@"avatar.jpg" mimeType:@"image/jpeg"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"Success: %@ ***** %@", operation.responseString, responseObject);
        block(responseObject, nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@ ***** %@", operation.responseString, error);
        block(nil ,error.description);
    }];
    [op start];
}
@end

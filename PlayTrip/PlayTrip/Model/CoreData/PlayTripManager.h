
#import <Foundation/Foundation.h>
#import "Request.h"
#import "RequestDelegate.h"
#import "Constants.h"

#import "BookmarkDetail.h"
#import "BookmarkUser.h"
#import "BookmarkAttractions.h"
#import "BookmarkVideotour.h"
#import "BookmarkLocation.h"
#import "PlanModel.h"


@interface PlayTripManager : NSObject<RequestDelegate> {
    ItemLoadedBlock _itemLoadedBlock;
}

+ (id) Instance;
+ (void)clearDatabBase;
-(void)LoadBookmarkDetails:(ItemLoadedBlock)block;

- (void) loadVideoTour:(ItemLoadedBlock)block;
- (void) createMyPlanWithUser:(NSString *)userid withName:(NSString *)name withFromDate:(NSString *)fdate withDays:(id)days withBlock:(ItemLoadedBlock)block;
- (void)loadPlanByUserID:(NSString *)userId WithBlock:(ItemLoadedBlock)block;
- (void)loadSpecificPlanByPlanID:(NSString *)planId WithBlock:(ItemLoadedBlock)block;
- (void)getAttractionsListWithDistance:(id)distance WithLattitude:(double)latitude WithLongitude:(double)longitude WithBlock:(ItemLoadedBlock)block;

- (void)getAttractionsListWithBlock:(ItemLoadedBlock)block;



- (void)editMyPlanWithPlanId:(NSString *)planId WithUser:(NSString *)userid withName:(NSString *)name withFromDate:(NSString *)fdate withDays:(id)days withBlock:(ItemLoadedBlock)block ;
- (void)getTermsAndConditionsWithBlock:(ItemLoadedBlock)block;
- (void)deleteTourWithId:(NSString *)tourId WithBlock:(ItemLoadedBlock)block;
-(void)getRecommendedUsersListWithBlock:(ItemLoadedBlock)block;

-(void)getAttractionListWithBlock:(ItemLoadedBlock)block;
-(void)getAttractionListWithBlockNew:(ItemLoadedBlock)block ;


-(void)addAttractionWithName:(NSString *)name andCatId:(NSMutableArray *)category AndAddress:(NSString *)address andLocArray:(NSMutableArray *)loc forUserId:(NSString *)user WithBlockNew:(ItemLoadedBlock)block;

- (void)addAttractionToMyPlan:(NSString *)planId forDay:(NSNumber *)day withAttractions:(NSMutableArray *)attractionArray WithBlock:(ItemLoadedBlock)block;
- (void)getReportsListWithBlock:(ItemLoadedBlock)block;
- (void)getDistrictListWithBlock:(ItemLoadedBlock)block;
- (void)addSpam:(NSString *)spamId forPlan:(NSString *)planId WithBlock:(ItemLoadedBlock)block;
- (void)searchDistrictWithString:(NSString *)searchStr WithLat:(double)lat andLng:(double)lng WithBlock:(ItemLoadedBlock)block;
- (void)getLatestVideoTourWithWhere:(NSMutableDictionary *)where WithSort:(NSMutableDictionary *)sort WithBlock:(ItemLoadedBlock)block;
- (void)getLatestVideoTourWithParameters:(NSDictionary *)params WithBlockNew:(ItemLoadedBlock)block;
- (void)getMostPopularPlansByAttractionWithParameters:(NSDictionary *)params WithBlockNew:(ItemLoadedBlock)block;
- (void)getMostPopularPlansWithParameters:(NSDictionary *)params WithBlockNew:(ItemLoadedBlock)block;
- (void)getRecommendedUsersWithBlockNew:(ItemLoadedBlock)block;
- (void)getUserPlansForUserID:(NSString *)userId andWithBlock:(ItemLoadedBlock)block;


- (void)getVideoTourWithDistrictIdDict:(NSMutableDictionary *)dict WithBlock:(ItemLoadedBlock)block;
-(void)uploadVideoWithVideoData:(NSData *)videoData WithAttrId:(NSString *)attr_id WithBlock:(ItemLoadedBlock)block;
-(void)uploadVideoWithVideoUrl:(NSURL *)videoUrl WithAttrId:(NSString *)attr_id WithBlock:(ItemLoadedBlock)block;
//- (void)getRecommendedUsersListWithLatitude:(double)lat WithLongitude:(double)lang WithBlock:(ItemLoadedBlock)block;
- (void)addCountForModel:(NSString *)modelName withType:(NSString *)typeName forId:(NSString *)idName WithBlock:(ItemLoadedBlock)block;
- (void)createVideoTourFromPlanWithUser:(NSString *)userid withPlanId:(NSString *)pId withIsPublished:(BOOL)isPublished withDraft:(BOOL)isDraft WithLanguage:(NSString *)langauge withBlock:(ItemLoadedBlock)block ;
- (void)loadCategory:(ItemLoadedBlock)block;
- (void)tagsAddEditWithName:(NSString *)name andAddress:(NSString *)address andPlaceId:(NSString *)placeId andLat:(NSString *)lat andLng:(NSString *)lng WithBlock:(ItemLoadedBlock)block;
-(void)saveSearchDataForModel:(NSString *)modelName WithString:(NSString *)str WithBlock:(ItemLoadedBlock)block;
-(void)getMostPopularSearchDataForVideoTour:(ItemLoadedBlock)block;
-(void)getMostPopularSearchDataForAttraction:(ItemLoadedBlock)block;
-(void)searchVideoTourWithName:(NSString *)name andLanguageArray:(NSMutableArray *)arrLanguage andStartDate:(NSString *)sDate andEndDate:(NSString *)eDate andSelfDrive:(BOOL)selfDrive WithBlock:(ItemLoadedBlock)block;
-(void)searchAttractionWithCatArray:(NSMutableArray *)catArray andLanguageArray:(NSMutableArray *)langArray andSearchStr:(NSString *)name WithBlock:(ItemLoadedBlock)block;
-(void)getMostPopularPlanWithBlock:(ItemLoadedBlock)block;
-(void)getMostPopularVideoTourWithBlock:(ItemLoadedBlock)block;
- (void)getUsersVideoTourWithWhere:(NSMutableDictionary *)where WithSort:(NSMutableDictionary *)sort WithUserId:(NSString *)userId WithBlock:(ItemLoadedBlock)block;
-(void)getMostPopularAttractionsWithBlock:(ItemLoadedBlock)block;
- (void)getCmsDataWithSearchString:(NSString *)searchStr WithLattitude:(double)latitude WithLongitude:(double)longitude WithBlock:(ItemLoadedBlock)block;
-(void)createAttrDataWithId:(NSString *)attrDataId andTime:(NSString *)timeStr andDesc:(NSString *)descStr andVideoId:(NSString *)videoId WithBlock:(ItemLoadedBlock)block;
-(void)deleteAttractionWithId:(NSString *)attraction andplanId:(NSString *)plan andDay:(NSString *)day WithBlock:(ItemLoadedBlock)block;
-(void)deleteDayForPlanId:(NSString *)plan andDay:(NSString *)day_id WithBlock:(ItemLoadedBlock)block;
- (void)uploadCoverPhoto:(UIImage *)img ForPlanId:(NSString *)plan_id WithBlock:(ItemLoadedBlock)block;
-(void)updateMultipleAttrData:(NSMutableArray *)data ForPlanId:(NSString *)plan_id WithBlock:(ItemLoadedBlock)block;
-(void)editPlanWithId:(NSString *)plan_id WithName:(NSString *)name WithTravelTip:(NSString *)travel_tip WithFromDate:(NSString *)from_date WithToDate:(NSString *)to_date WithDays:(NSString *)days andSelfDrive:(BOOL)selfDrive WithBlock:(ItemLoadedBlock)block;
-(void)createVideoTourForPlan:(NSString *)plan_id WithBlock:(ItemLoadedBlock)block;
-(void)getAllAttrDataWithBlock:(ItemLoadedBlock)block;
-(void)addAttractionWithName:(NSString *)name andCatId:(NSMutableArray *)category AndAddress:(NSString *)address andLocArray:(NSMutableArray *)loc forUserId:(NSString *)user WithBlock:(ItemLoadedBlock)block;
-(void)createVideoTourWithAllDetails:(NSMutableDictionary *)planDict WithAttractionArray:(NSMutableArray *)attrArray WithBlock:(ItemLoadedBlock)block;
//- (void)createVideoTourToPlanWithSelfDrive:(BOOL)selfDrive WithDays:(NSNumber *)days WithToDate:(NSDate *)toDate WithFromDate:(NSDate *)fromDate WithName:(NSString *)name WithisPublished:(BOOL)isPublished WithisDraft:(BOOL)isDraft WithUserid:(NSString *)userid WithTravelTip:(NSString *)traveltips WithBlock:(ItemLoadedBlock)block;

-(void)getAllCountriesDataWithBlock:(ItemLoadedBlock)block;
-(void)getAllDistrictsOdCountryId:(NSString *)countryId DataWithBlock:(ItemLoadedBlock)block;
-(void)getAllCitiesOfDistrictId:(NSString *)districtId DataWithBlock:(ItemLoadedBlock)block;


-(void)addAttractionWithName:(NSString *)name andCatId:(NSString *)category AndAddress:(NSString *)address andLocArray:(NSMutableArray *)loc andDescription:(NSString *)description andPhoneNo:(NSString *)phoneNo andPhotoUrl:(NSString *)photoUrl andCountryId:(NSString *)countryId andDistrictId:(NSString *)districtId andCityId:(NSString *)cityId forUserId:(NSString *)user WithBlockNew:(ItemLoadedBlock)block ;


-(NSMutableArray *)createTrickyJsonForBookmarkUsers:(NSMutableArray*)wholeArray;
-(NSMutableArray*)createTrickyJsonForAttractionLocationValues:(NSMutableArray*)wholeArray;



-(void)loadPlanByUserID:(NSString *)userId WithBlockNew:(ItemLoadedBlock)block;
-(void)getAllLocationWithDictionary:(NSDictionary *)dict andBlockNew:(ItemLoadedBlock)block;
-(void)saveEditedPlanForPlan :(PlanModel*)planModel withBlockNew:(ItemLoadedBlock)block;
-(void)searchAttractionWithDictionary:(NSDictionary *)params WithBlock:(ItemLoadedBlock)block;
-(void)getAllCategoriesWithBlock:(ItemLoadedBlock)block;
-(void)addAttractionToMyPlanWithDictionary:(NSDictionary *)dict withBlock:(ItemLoadedBlock)block;
-(void)loadVideoToursByUserID:(NSString *)userId WithBlockNew:(ItemLoadedBlock)block;
-(void)loadAttractionsWithDataWithBlockNew:(ItemLoadedBlock)block;
-(void)getMostPopularAttractionVideosWithBlockNew:(ItemLoadedBlock)block;

-(void)uploadAvatarWithImage:(UIImage *)image WithBlockNew:(ItemLoadedBlock)block;
-(void)uploadCoverWithImage:(UIImage *)image WithBlockNew:(ItemLoadedBlock)block;

@end


#import "FbHelper.h"
#import "Utils.h"
#import <SVProgressHUD/SVProgressHUD.h>
@implementation FbHelper

+ (id)Instance{
    static id sharedManager;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        sharedManager = [[self alloc] init];
    });
    return sharedManager;
}

- (void)authenticateWithBlock:(SocialBlock)block{
    socialBlock = block;
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    UIViewController * cont = [Utils getTopViewController];
    [login logInWithReadPermissions:@[] fromViewController:cont handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        if (error) {
            if (result) {
                socialBlock(nil,error.localizedDescription);
            }
        }
        else {
            if (![FBSDKAccessToken currentAccessToken]) {
                FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]initWithGraphPath:@"me" parameters:@{@"fields" : @"id,name,email,first_name,last_name,picture"                                                                                                            }];
                [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                    if (result) {
                        if (socialBlock) {
                            socialBlock(result,nil);
                        }
                    }else{
                        if (socialBlock) {
                            socialBlock(nil,error.localizedDescription);
                        }
                    }
                }];
            }
        }
    }];
}

@end

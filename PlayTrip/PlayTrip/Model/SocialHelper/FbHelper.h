
#import <Foundation/Foundation.h>
#import <FBSDKCoreKit/FBSDKAccessToken.h>
#import <FBSDKCoreKit/FBSDKGraphRequest.h>
#import <FBSDKLoginKit/FBSDKLoginManager.h>
typedef void (^SocialBlock)(id result, NSString *error);

@interface FbHelper : NSObject{
    SocialBlock socialBlock;
}

+ (id) Instance;

- (void) authenticateWithBlock:(SocialBlock)block;

@end

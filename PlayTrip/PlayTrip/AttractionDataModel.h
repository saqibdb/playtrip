//
//  AttractionDataModel.h
//  PlayTrip
//
//  Created by ibuildx on 6/7/17.
//  Copyright © 2017 SamirMAC. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "UserModel.h"
#import "Country.h"
#import "DraftInfo.h"
#import "DistrictModel.h"
#import "DraftAttrData.h"
#import "CityModel.h"


@protocol VideoModel;
@interface VideoModel : JSONModel

@property (nonatomic) NSString *_id;
@property (nonatomic) NSString *file_name;
@property (nonatomic) NSString *user;
@property (nonatomic) NSNumber<Optional> *__v;
@property (nonatomic) NSString *thumbnail;
@property (nonatomic) NSNumber<Optional> *is_deleted;
@property (nonatomic) NSString *create_date;
@property (nonatomic) NSString *id;


@end

@protocol CategoryModel;
@interface CategoryModel : JSONModel

@property (nonatomic) NSString *_id;
@property (nonatomic) NSString *name;
@property (nonatomic) NSNumber<Optional> *__v;
@property (nonatomic) NSString *last_updated;
@property (nonatomic) NSNumber<Optional> *is_deleted;
@property (nonatomic) NSString *create_date;
@property (nonatomic) NSNumber<Optional> *is_published;
@property (nonatomic) NSString *description;
@property (nonatomic) NSString *color;
@property (nonatomic) NSString *image;
@property (nonatomic) NSString *name_tc;
@property (nonatomic) NSString *name_sc;


/*
 
 "_id": "58d136a52e0fbe7e5867e32c",
 "name": "Hotel",
 "__v": 0,
 "last_updated": "2017-03-21T14:20:21.860Z",
 "is_deleted": false,
 "create_date": "2017-03-21T14:20:21.857Z",
 "is_published": true,
 "description": "",
 "color": "",
 "image": ""
 */
@end

@protocol InfoModel;
@interface InfoModel : JSONModel

@property (nonatomic) NSString *_id;
@property (nonatomic) UserModel<Optional> *user;
@property (nonatomic) NSString<Optional> *name;

@property (nonatomic) CityModel<Optional> *city_id;
@property (nonatomic) DistrictModel<Optional> *district_id;
@property (nonatomic) Country<Optional> *country_id;

@property (nonatomic) NSArray<Optional>  *loc;

@property (nonatomic) NSNumber<Optional> *__v;
@property (nonatomic) NSString<Optional> *loc_long;
@property (nonatomic) NSString<Optional> *loc_lat;


@property (nonatomic) NSNumber<Optional> *is_deleted;
@property (nonatomic) NSString<Optional> *last_updated;
@property (nonatomic) NSString<Optional> *create_date;
@property (nonatomic) NSMutableArray<NSDictionary *>  *remarks;
@property (nonatomic) NSString<Optional> *geotype;
@property (nonatomic) NSNumber<Optional> *total_view;
@property (nonatomic) NSNumber<Optional> *total_share;
@property (nonatomic) NSMutableArray<UserModel *> *bookmark_users;
@property (nonatomic) NSNumber<Optional> *total_bookmark;
@property (nonatomic) NSMutableArray<NSDictionary *>  *tags;
@property (nonatomic) NSString<Optional> *cover_photo_url;
@property (nonatomic) NSString<Optional> *description;
@property (nonatomic) NSString<Optional> *phone_no;
@property (nonatomic) NSString<Optional> *hours_info;
@property (nonatomic) NSString<Optional> *web_url;
@property (nonatomic) NSString<Optional> *address;
@property (nonatomic) NSString<Optional> *detail;
@property (nonatomic) NSString<Optional> *place_id;
@property (nonatomic) NSMutableArray<CategoryModel> *category;

- (instancetype)initWithDraftInfo:(DraftInfo*)di;


@end

@protocol AttractionDataModel;
@interface AttractionDataModel : JSONModel

@property (nonatomic) NSString *_id;
@property (nonatomic) NSString *last_updated;
@property (nonatomic) NSString *create_date;
@property (nonatomic) NSString<Optional> *desc;
@property (nonatomic) NSString<Optional> *time;

@property (nonatomic) NSNumber<Optional> *__v;
@property (nonatomic) NSNumber<Optional> *is_deleted;
@property (nonatomic) NSNumber<Optional> *total_view;
@property (nonatomic) NSNumber<Optional> *total_share;
@property (nonatomic) NSNumber<Optional> *total_bookmark;
@property (nonatomic) NSNumber<Optional> *sequence;



@property (nonatomic) NSMutableArray<Optional,UserModel>  *bookmark_users;

@property (nonatomic) NSMutableArray<NSDictionary *>  *tags;

@property (nonatomic) InfoModel<Optional> *info;
@property (nonatomic) VideoModel<Optional> *video_id;

- (instancetype)initWithDraftAttrData:(DraftAttrData*)dad;

@end


/*

 "__v": 0,
 "is_deleted": false,
 "last_updated": "2017-06-06T11:49:09.991Z",
 "create_date": "2017-06-06T11:49:09.990Z",
 "total_view": 0,
 "total_share": 0,
 "bookmark_users": [],
 "total_bookmark": 0,
 "desc": "test",
 "tags": [],
 "time": "17:18"

*/

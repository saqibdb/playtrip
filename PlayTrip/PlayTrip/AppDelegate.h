
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "DashboardViewController.h"
#import "CommonNavigationController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <Google/SignIn.h>
#import "LoginDelegate.h"
#import "AFNetworkActivityLogger.h"
#import "GTLR/AppAuth.h"

typedef void (^LoginBlock)(BOOL success);

@interface AppDelegate : UIResponder <UIApplicationDelegate,GIDSignInDelegate>

@property (strong, nonatomic, nullable) UIWindow *window;

@property (readonly, strong, nonatomic, nullable) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic, nullable) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic, nullable) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@property(nonatomic, strong, nullable) id<OIDAuthorizationFlowSession> currentAuthorizationFlow;


@property (nonatomic ,retain, nullable) CommonNavigationController * navigationController;
@property (nonatomic ,retain, nullable) DashboardViewController * dashboardViewController;

@property (readonly, strong, nullable) NSPersistentStore *persistentContainer;

@property (nonatomic ,assign, nullable) id<LoginDelegate> delegate;


- (void)saveContext;

- (BOOL)isReachable;
- (BOOL)isReachableViaWifi;

- (void)showLogin:(nullable id<LoginDelegate>)block;
- (void)logout;
- (void)userDidLogin;

+(nullable AppDelegate*)appDelegate;

@end

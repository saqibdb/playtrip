
//
//  AttractionDataModel.m
//  PlayTrip
//
//  Created by ibuildx on 6/7/17.
//  Copyright © 2017 SamirMAC. All rights reserved.
//

#import "AttractionDataModel.h"

@implementation AttractionDataModel

/*
 @property (nonatomic) NSString *_id;
 @property (nonatomic) NSString *last_updated;
 @property (nonatomic) NSString *create_date;
 @property (nonatomic) NSString<Optional> *desc;
 @property (nonatomic) NSString<Optional> *time;
 
 @property (nonatomic) NSNumber<Optional> *__v;
 @property (nonatomic) NSNumber<Optional> *is_deleted;
 @property (nonatomic) NSNumber<Optional> *total_view;
 @property (nonatomic) NSNumber<Optional> *total_share;
 @property (nonatomic) NSNumber<Optional> *total_bookmark;
 @property (nonatomic) NSNumber<Optional> *sequence;
 
 
 
 @property (nonatomic) NSMutableArray<Optional,UserModel>  *bookmark_users;
 
 @property (nonatomic) NSMutableArray<NSDictionary *>  *tags;
 
 @property (nonatomic) InfoModel<Optional> *info;
 @property (nonatomic) VideoModel<Optional> *video_id;
 */

- (instancetype)initWithDraftAttrData:(DraftAttrData*)dad {
    self = [super init];
    if (self) {
        self.sequence = dad.sequence;
        self.info = [[InfoModel alloc]initWithDraftInfo:dad.draftInfo];
    }
    return self;
}

@end

@implementation InfoModel

/*
 @property (nonatomic) NSString *_id;
 @property (nonatomic) UserModel<Optional> *user;
 @property (nonatomic) NSString<Optional> *name;
 
 @property (nonatomic) CityModel<Optional> *city_id;
 @property (nonatomic) DistrictModel<Optional> *district_id;
 @property (nonatomic) Country<Optional> *country_id;
 
 @property (nonatomic) NSArray<Optional>  *loc; +
 
 @property (nonatomic) NSNumber<Optional> *__v;
 @property (nonatomic) NSString<Optional> *loc_long; +
 @property (nonatomic) NSString<Optional> *loc_lat; +
 
 
 @property (nonatomic) NSNumber<Optional> *is_deleted;
 @property (nonatomic) NSString<Optional> *last_updated;
 @property (nonatomic) NSString<Optional> *create_date;
 @property (nonatomic) NSMutableArray<NSDictionary *>  *remarks;
 @property (nonatomic) NSString<Optional> *geotype;
 @property (nonatomic) NSNumber<Optional> *total_view;
 @property (nonatomic) NSNumber<Optional> *total_share;
 @property (nonatomic) NSMutableArray<UserModel *> *bookmark_users;
 @property (nonatomic) NSNumber<Optional> *total_bookmark;
 @property (nonatomic) NSMutableArray<NSDictionary *>  *tags;
 @property (nonatomic) NSString<Optional> *cover_photo_url;
 @property (nonatomic) NSString<Optional> *description;
 @property (nonatomic) NSString<Optional> *phone_no;
 @property (nonatomic) NSString<Optional> *hours_info;
 @property (nonatomic) NSString<Optional> *web_url;
 @property (nonatomic) NSString<Optional> *address;
 @property (nonatomic) NSString<Optional> *detail;
 @property (nonatomic) NSString<Optional> *place_id;
 @property (nonatomic) NSMutableArray<CategoryModel> *category; +
 */

- (instancetype)initWithDraftInfo:(DraftInfo*)di {
    self = [super init];
    if (self) {
        self.loc_long = [di.loc_long stringValue];
        self.loc_lat = [di.loc_lat stringValue];
        NSLog(@"%@", di.geotype);
    }
    return self;
}

@end

@implementation VideoModel
@end

@implementation CategoryModel
@end

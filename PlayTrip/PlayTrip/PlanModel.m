//
//  PlanModel.m
//  PlayTrip
//
//  Created by ibuildx on 6/6/17.
//  Copyright © 2017 SamirMAC. All rights reserved.
//

#import "PlanModel.h"

@implementation PlanModel

/*
 @property (nonatomic) NSString *_id;
 
 @property (nonatomic) NSString<Optional> *to_date; +
 @property (nonatomic) NSString<Optional> *name; +
 @property (nonatomic) NSString<Optional> *from_date; +
 
 @property (nonatomic) NSString<Optional> *language; +
 @property (nonatomic) NSString<Optional> *cover_photo;
 @property (nonatomic) NSString<Optional> *last_updated; +
 @property (nonatomic) NSString<Optional> *create_date; +
 @property (nonatomic) NSString<Optional> *travel_tip; +
 
 @property (nonatomic) NSString *id;
 
 @property (nonatomic) NSNumber<Optional> *days; +
 @property (nonatomic) NSNumber<Optional> *__v;
 @property (nonatomic) NSNumber<Optional> *is_deleted;
 @property (nonatomic) NSNumber<Optional> *is_published;
 @property (nonatomic) NSNumber<Optional> *is_draft;
 @property (nonatomic) NSNumber<Optional> *self_drive; +
 @property (nonatomic) NSNumber<Optional> *remuneration_amount;
 @property (nonatomic) NSNumber<Optional> *refer_count;
 @property (nonatomic) NSNumber<Optional> *total_attractions;
 @property (nonatomic) NSNumber<Optional> *total_view;
 @property (nonatomic) NSNumber<Optional> *total_share;
 @property (nonatomic) NSNumber<Optional> *total_bookmark;
 
 
 
 @property (nonatomic) NSNumber<Optional> *duration; +
 @property (nonatomic) NSString<Optional> *merged_video; +
 @property (nonatomic) NSString<Optional> *thumbnail; +
 
 
 @property (nonatomic) UserModel<Optional> *user;
 
 @property (nonatomic) NSMutableArray<Optional,AttractionModel>  *attractions; +
 @property (nonatomic) NSMutableArray<Optional> *bookmark_users;
 */

- (instancetype)initWithDraftTour:(DraftTour*)dt {
    self = [super init];
    if (self) {
        //self.create_date = dt.create_date;
        self.days = dt.days;
        //self.from_date = dt.from_date;
        //self.to_date = dt.to_date;
        //self.duration = dt.duration;
        self.language = dt.language;
        //self.last_updated = dt.last_updated;
        self.merged_video = dt.merged_video;
        self.name = dt.name;
        self.self_drive = dt.self_drive;
        self.thumbnail = dt.thumbnail;
        self.travel_tip = dt.travel_tip;
        
        NSArray* daArray = dt.draftAttractions.allObjects;
        self.attractions = [NSMutableArray<Optional, AttractionModel> array];
        for(int i=0; i<daArray.count; i++){
            DraftAttraction* da = daArray[i];
            AttractionModel* am = [[AttractionModel alloc]initWithDraftAttraction:da];
            [self.attractions addObject:am];
        }
    }
    return self;
}

@end

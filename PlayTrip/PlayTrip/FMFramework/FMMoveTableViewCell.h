//
//  FMMoveTableViewCell.h
//  FMFramework
//
//  Created by Florian Mielke.
//  Copyright 2012 Florian Mielke. All rights reserved.
//  
#import "FMMoveTableViewCell.h"
#import "itineraryTableViewCell.h"


@interface FMMoveTableViewCell : itineraryTableViewCell

- (void)prepareForMoveSnapshot;
- (void)prepareForMove;

@end

//
//  UserModel.h
//  PlayTrip
//
//  Created by ibuildx on 6/6/17.
//  Copyright © 2017 SamirMAC. All rights reserved.
//

#import <JSONModel/JSONModel.h>

@protocol UserModel;
@interface UserModel : JSONModel


@property (nonatomic) NSString *_id;
@property (nonatomic) NSString<Optional> *user_name;
@property (nonatomic) NSString<Optional> *first_name;
@property (nonatomic) NSString<Optional> *last_name;
@property (nonatomic) NSString<Optional> *email;
@property (nonatomic) NSString<Optional> *cover_pic;
@property (nonatomic) NSString<Optional> *geotype;
@property (nonatomic) NSString<Optional> *avatar;
@property (nonatomic) NSString<Optional> *phone_number;
@property (nonatomic) NSString<Optional> *last_updated;
@property (nonatomic) NSString<Optional> *create_date;
@property (nonatomic) NSString<Optional> *role;
@property (nonatomic) NSString<Optional> *instagram;
@property (nonatomic) NSString<Optional> *google_id;
@property (nonatomic) NSString<Optional> *facebook_id;
@property (nonatomic) NSString<Optional> *provider;
@property (nonatomic) NSString<Optional> *full_name;
@property (nonatomic) NSString *id;


@property (nonatomic) NSNumber<Optional> *__v;

@property (nonatomic) NSNumber<Optional> *wifiOnly;
@property (nonatomic) NSNumber<Optional> *is_recommended;
@property (nonatomic) NSNumber<Optional> *total_view;
@property (nonatomic) NSNumber<Optional> *total_share;
@property (nonatomic) NSNumber<Optional> *credits;
@property (nonatomic) NSNumber<Optional> *total_video_played;
@property (nonatomic) NSNumber<Optional> *total_bookmark;
@property (nonatomic) NSNumber<Optional> *is_deleted;
@property (nonatomic) NSNumber<Optional> *is_active;
@property (nonatomic) NSNumber<Optional> *terms_approved;
@property (nonatomic) NSNumber<Optional> *is_verified;

@property (nonatomic) NSMutableArray<Optional> *bookmark_users;


@property (nonatomic) NSDictionary<Optional> *token;
@property (nonatomic) NSDictionary<Optional> *profile;



@end

@interface facebook : JSONModel

@property (nonatomic) NSString<Optional> *email;
@property (nonatomic) NSString *id;
@property (nonatomic) NSString<Optional>  *last_name;
@property (nonatomic) NSDictionary<Optional>  *picture;

@property (nonatomic) NSString<Optional> *name;
@property (nonatomic) NSString<Optional> *first_name;


@end


/*
 
 "user": {
 "_id": "59227fcf511483d408f8c9e8",
 "user_name": "Awais",
 "first_name": "Awais",
 "facebook": {
 "email": "test@ibuildx.com",
 "id": "358734684522022",
 "last_name": "Ibuildx",
 "picture": {
 "data": {
 "is_silhouette": false,
 "url": "https://fb-s-c-a.akamaihd.net/h-ak-fbx/v/t1.0-1/p50x50/13343139_186915118370647_5399905067718778703_n.jpg?oh=2bfa9c95c5ea875af0905aec1a15b955&oe=59B65839&__gda__=1500911118_73366b97b946dd171caa17079bb72cb0"
 }
 },
 "name": "Awais Ibuildx",
 "first_name": "Awais"
 },
 "last_name": "Ibuildx",
 "email": "test@ibuildx.com",
 "__v": 0,
 "cover_pic": "",
 "geotype": "Point",
 "wifiOnly": true,
 "is_recommended": false,
 "total_view": 0,
 "total_share": 0,
 "bookmark_users": [],
 "credits": 0,
 "total_video_played": 0,
 "total_bookmark": 0,
 "avatar": "",
 "phone_number": "",
 "is_deleted": false,
 "is_active": true,
 "last_updated": "2017-05-22T06:06:07.659Z",
 "create_date": "2017-05-22T06:06:07.656Z",
 "terms_approved": false,
 "role": "User",
 "is_verified": true,
 "instagram": "",
 "google_id": "",
 "facebook_id": "",
 "provider": "facebook",
 "token": {
 "role": "User"
 },
 "full_name": "Awais Ibuildx",
 "profile": {
 "role": "User"
 },
 "id": "59227fcf511483d408f8c9e8"
 },
 
 */







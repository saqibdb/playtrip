
#import "VideoTourViewController.h"
#import "PlayTripManager.h"
#import "VideoTour.h"
#import "MostPopularSearchVideoTour.h"
#import "LanguageTableViewCell.h"
#import "DayTableViewCell.h"
#import "PopularTableViewCell.h"
#import "MHTabBarController.h"
#import "AttractionsVideoViewController.h"
#import "VideosTourViewController.h"
#import "SVProgressHUD.h"
#import "ColorConstants.h"

#define langChi  @"ch"
#define langMnd  @"md"
#define langEng  @"en"

@interface VideoTourViewController ()
@end

@implementation VideoTourViewController

+(VideoTourViewController *)initViewController {
    VideoTourViewController * controller = [[VideoTourViewController alloc] initWithNibName:@"VideoTourViewController" bundle:nil];
    controller.title = @"Video Tour";
    return controller;
}

- (IBAction)yesClicked:(id)sender {
    [btnNo setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [btnYes setImage:[UIImage imageNamed:@"RadioButton-Selected.png"]
            forState:UIControlStateNormal];
    selfDrive = true;
    
    
}

- (IBAction)NoClicked:(id)sender {
    [btnYes setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [btnNo setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    selfDrive = true;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //    checked = false;
    _tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    arrMostSearch = [NSMutableArray new];
    arrLanguage = [NSMutableArray new];
    [self registerNIB];
    //        self.bar = [UISearchBar init];
    self.bar.showsCancelButton = YES;
    self.bar.delegate= self;
    [self getApiResponse];
    
}

-(void)getApiResponse {
    [[PlayTripManager Instance] getMostPopularSearchDataForVideoTour:^(id result, NSString *error) {
        if (error) {
            NSLog(@"%@", error);
        } else {
            arrMostSearch = [MostPopularSearchVideoTour getAll];
            [_tblView reloadData];
        }
    }];
}

- (void)registerNIB{
    UINib *cellLanguage = [UINib nibWithNibName:@"LanguageTableViewCell" bundle:nil];
    [_tblView registerNib:cellLanguage forCellReuseIdentifier:@"LanguageTableViewCell"];
    UINib *cellDay = [UINib nibWithNibName:@"DayTableViewCell" bundle:nil];
    [_tblView registerNib:cellDay forCellReuseIdentifier:@"DayTableViewCell"];
    UINib *cellPopular = [UINib nibWithNibName:@"PopularTableViewCell" bundle:nil];
    [_tblView registerNib:cellPopular forCellReuseIdentifier:@"PopularTableViewCell"];
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.tag == 1) {
        leftDate = textField.text;
    } else if (textField.tag == 2) {
        rightDate = textField.text;
    }
}

-(void)checkedClicked:(id)sender{
    if ([sender tag] == 0) {
        if (chSelected) {
            chSelected = false;
            if ([arrLanguage containsObject:langChi]) {
                [arrLanguage removeObject:langChi];
            }
        } else {
            chSelected = true;
            [arrLanguage addObject:langChi];
        }
    } else if ([sender tag] == 1) {
        if (mnSelected) {
            mnSelected = false;
            if ([arrLanguage containsObject:langMnd]) {
                [arrLanguage removeObject:langMnd];
            }
        } else {
            mnSelected = true;
            [arrLanguage addObject:langMnd];
        }
    } else if ([sender tag] == 2) {
        if (enSelected) {
            enSelected = false;
            if ([arrLanguage containsObject:langEng]) {
                [arrLanguage removeObject:langEng];
            }
        } else {
            enSelected = true;
            [arrLanguage addObject:langEng];
        }
    }
    [self updateLanguageSelected:btnHeaderCheck];
    [_tblView reloadData];
    
}

-(void)updateLanguageSelected:(id)sender {
    if (chSelected && mnSelected && enSelected) {
        fullSelected = true;
    } else {
        fullSelected = false;
    }
    [sender setImage:[UIImage imageNamed:((fullSelected) ? @"checked_black.png" : @"unchecked_black.png")] forState:UIControlStateNormal];
}

-(void)languageSelected:(id)sender {
    
    if (fullSelected) {
        fullSelected = false;
        chSelected = false;
        mnSelected = false;
        enSelected = false;
        for (int i = 0; i<3; i++) {
            NSIndexPath * ip = [NSIndexPath indexPathForRow:i inSection:0];
            
            [_tblView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:ip, nil] withRowAnimation:UITableViewRowAnimationNone];
        }
        if ([arrLanguage containsObject:langChi]) {
            [arrLanguage removeObject:langChi];
        }
        if ([arrLanguage containsObject:langMnd]) {
            [arrLanguage removeObject:langMnd];
        }
        if ([arrLanguage containsObject:langEng]) {
            [arrLanguage removeObject:langEng];
        }
    } else {
        fullSelected = true;
        chSelected = true;
        mnSelected = true;
        enSelected = true;
        for (int i = 0; i<3; i++) {
            NSIndexPath * ip = [NSIndexPath indexPathForRow:i inSection:0];
            [_tblView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:ip, nil] withRowAnimation:UITableViewRowAnimationNone];
        }
        [arrLanguage addObject:langChi];
        [arrLanguage addObject:langMnd];
        [arrLanguage addObject:langEng];
        
        
    }
    [sender setImage:[UIImage imageNamed:((fullSelected) ? @"checked_black.png" : @"unchecked_black.png")] forState:UIControlStateNormal];
}



-(void)leftPlusClicked {
    int a = leftDate.intValue;
    a++;
    leftDate = [NSString stringWithFormat:@"%d",a];
}

-(void)leftMinusClicked {
    int a = leftDate.intValue;
    if (a > 1) {
        a--;
    }
    leftDate = [NSString stringWithFormat:@"%d",a];
}

-(void)rightPlusClicked {
    int a = rightDate.intValue;
    a++;
    rightDate = [NSString stringWithFormat:@"%d",a];
}

-(void)rightMinusClicked {
    int a = rightDate.intValue;
    if (a > 1) {
        a--;
    }
    rightDate = [NSString stringWithFormat:@"%d",a];
}

#pragma mark -- TablerView Datasource and Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 3;
    }else if (section == 1){
        return 1;
    }else{
        if (arrMostSearch.count > 0) {
            return 1;
        } else {
            return 0;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        LanguageTableViewCell *cell = (LanguageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"LanguageTableViewCell" forIndexPath:indexPath];
        if (indexPath.row == 0) {
            cell.lblLanguage.text = @"Chineese";
            if (!chSelected) {
                [cell.btnCheck setImage:[UIImage imageNamed:@"unchecked_black.png"] forState:UIControlStateNormal];
            } else {
                [cell.btnCheck setImage:[UIImage imageNamed:@"checked_black.png"] forState:UIControlStateNormal];
            }
        } else if (indexPath.row == 1) {
            cell.lblLanguage.text = @"Mandarin";
            if (!mnSelected) {
                [cell.btnCheck setImage:[UIImage imageNamed:@"unchecked_black.png"] forState:UIControlStateNormal];
            } else {
                [cell.btnCheck setImage:[UIImage imageNamed:@"checked_black.png"] forState:UIControlStateNormal];
            }
        } else if (indexPath.row == 2) {
            cell.lblLanguage.text = @"English";
            if (!enSelected) {
                [cell.btnCheck setImage:[UIImage imageNamed:@"unchecked_black.png"] forState:UIControlStateNormal];
            } else {
                [cell.btnCheck setImage:[UIImage imageNamed:@"checked_black.png"] forState:UIControlStateNormal];
            }
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.btnCheck.tag = indexPath.row;
        
        [cell.btnCheck addTarget:self action:@selector(checkedClicked:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }else if (indexPath.section == 1){
        DayTableViewCell *cell = (DayTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"DayTableViewCell" forIndexPath:indexPath];
//        cell.txtRightDigit.delegate = self;
//        cell.txtLeftDigit.delegate = self;
        cell.txtLeftDigit.tag = 1;
        cell.txtRightDigit.tag = 2;
        
        rightDate = cell.txtRightDigit.text;
        leftDate = cell.txtLeftDigit.text;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [cell.btnLeftPlus addTarget:self action:@selector(leftPlusClicked) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnLeftMinus addTarget:self action:@selector(leftMinusClicked) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btnRightPlus addTarget:self action:@selector(rightPlusClicked) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnRightMinus addTarget:self action:@selector(rightMinusClicked) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    } else {
        PopularTableViewCell *cell = (PopularTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"PopularTableViewCell" forIndexPath:indexPath];
        if (arrMostSearch.count == 3) {
            MostPopularSearchVideoTour * bMost = [arrMostSearch objectAtIndex:1];
            MostPopularSearchVideoTour * cMost = [arrMostSearch objectAtIndex:2];
//            cell.lblTwo.text = bMost.search_str;
            
//            cell.lblThree.text = cMost.search_str;
            
        } else if (arrMostSearch.count == 2) {
            MostPopularSearchVideoTour * bMost = [arrMostSearch objectAtIndex:1];
//            cell.lblTwo.text = bMost.search_str;
        }
//        MostPopularSearchVideoTour * aMost = [arrMostSearch objectAtIndex:0];
//        cell.lblOne.text = aMost.search_str;
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        
//        cell.lblOne.layer.cornerRadius = 5;
//        cell.lblOne.layer.borderWidth = 1;
//        cell.lblOne.layer.borderColor = [ColorConstants appBrownColor].CGColor;
//        
//        cell.lblTwo.layer.cornerRadius = 5;
//        cell.lblTwo.layer.borderWidth = 1;
//        cell.lblTwo.layer.borderColor = [ColorConstants appBrownColor].CGColor;
//        
//        cell.lblThree.layer.cornerRadius = 5;
//        cell.lblThree.layer.borderWidth = 1;
//        cell.lblThree.layer.borderColor = [ColorConstants appBrownColor].CGColor;
//        
//        UIFont *font = [UIFont systemFontOfSize:17.0];
//        CGFloat strWidth1 = [Utils widthOfString:cell.lblOne.text withFont:font];
//        CGFloat strWidth2 = [Utils widthOfString:cell.lblTwo.text withFont:font];
//        CGFloat strWidth3 = [Utils widthOfString:cell.lblThree.text withFont:font];
//        
//        cell.lblOne.frame = CGRectMake(cell.lblOne.frame.origin.x, cell.lblOne.frame.origin.y, strWidth1+10, cell.lblOne.frame.size.height);
//        
//        cell.lblTwo.frame = CGRectMake(cell.lblOne.frame.origin.x + cell.lblOne.frame.size.width +10 , cell.lblTwo.frame.origin.y, strWidth2+10, cell.lblTwo.frame.size.height);
//        
//        cell.lblThree.frame = CGRectMake(cell.lblTwo.frame.origin.x + cell.lblTwo.frame.size.width +10 , cell.lblTwo.frame.origin.y, strWidth3+10, cell.lblTwo.frame.size.height);
        
        return cell;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(5, 5, 200, 50)];
    UILabel *lblText = [[UILabel alloc] initWithFrame:CGRectMake(8, 5, 200, 25)];
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,1)];
    line.backgroundColor = [UIColor lightGrayColor];
    
    if (section == 0) {
        lblText.text = @"Language";
        
        btnHeaderCheck = [UIButton buttonWithType:UIButtonTypeCustom];
        
        btnHeaderCheck.frame = CGRectMake(self.view.frame.size.width-33, 0, 20, 20);//(292, 0, 20, 20)
        [btnHeaderCheck addTarget:self action:@selector(languageSelected:)forControlEvents:UIControlEventTouchUpInside];
        
        if(fullSelected){
            [btnHeaderCheck setImage:[UIImage imageNamed:@"checked_black.png"] forState:UIControlStateNormal];
        }else{
            [btnHeaderCheck setImage:[UIImage imageNamed:@"unchecked_black.png"] forState:UIControlStateNormal];
        }
        [view addSubview:btnHeaderCheck];
        
    }else if (section == 1){
        lblText.text = @"Day";
        [view addSubview:line];
        
        
    }else if (section == 2){
        lblText.text = @"Most Popular Search";
        [view addSubview:line];
    }
    [view addSubview:lblText];
    [view setBackgroundColor:[UIColor whiteColor]];
    return view;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (section == 1) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 17, _tblView.frame.size.width, 120)];
        [view addSubview:selfDriveView];
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 5, self.view.frame.size.width,1)];
        line.backgroundColor = [UIColor lightGrayColor];
        [view addSubview:line];

        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(selfDriveView.frame.size.height-5, selfDriveView.frame.size.height+10, _tblView.frame.size.width-80, 40)];
        [btn setTitle:@"Search" forState:UIControlStateNormal];
        [btn setBackgroundColor:[ColorConstants appYellowColor]];
        btn.layer.cornerRadius = 18;
        btn.clipsToBounds = YES;
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [view addSubview:btn];
        [btn addTarget:self action:@selector(searchClicked) forControlEvents:UIControlEventTouchUpInside];
        [view setBackgroundColor:[UIColor whiteColor]];
        return view;
    }
    return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 1) {
        return 120;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 35;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        LanguageTableViewCell *cell = (LanguageTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
        [self checkedClicked:cell.btnCheck];
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        LanguageTableViewCell *cell = (LanguageTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
        [self checkedClicked:cell.btnCheck];
    }
}

-(void)searchClicked {
    NSString * sDate = leftDate;
    NSString * eDate = rightDate;
    
    if (leftDate.intValue > rightDate.intValue) {
        [Utils showAlertWithMessage:@"Set to date higher than from date"];
        return;
    }
    if (leftDate.length <= 0 || rightDate.length <= 0) {
        [Utils showAlertWithMessage:@"Set proper days"];
        return;
    }
    
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    if (self.bar.text.length > 0) {
        [[PlayTripManager Instance] saveSearchDataForModel:@"video_tour" WithString:self.bar.text WithBlock:nil];
    }
    
    [[PlayTripManager Instance] searchVideoTourWithName:self.bar.text andLanguageArray:arrLanguage andStartDate:sDate andEndDate:eDate andSelfDrive:selfDrive WithBlock:^(id result, NSString *error) {
        if (error) {
            NSLog(@"%@", error);
            [SVProgressHUD dismiss];
        } else {
            [SVProgressHUD dismiss];
            videoTourArray = [VideoTour getAll];
            if (videoTourArray.count > 0) {
                NSLog(@"%@", videoTourArray);
                [self setTabBars];
            } else {
                [Utils showAlertWithMessage:@"No data found"];
            }
        }
    }];
}

-(void)setTabBars {
    MHTabBarController *tabcontroller = [[MHTabBarController alloc] init];
    tabcontroller.hidesBottomBarWhenPushed = true;
    tabcontroller.navigationController.navigationBarHidden = false;
    
    
    self.bar = [[UISearchBar alloc] initWithFrame:CGRectMake(0,0,320,45)];
    self.bar.showsCancelButton = YES;
    self.bar.autocorrectionType = UITextAutocorrectionTypeNo;
    self.bar.autocapitalizationType = UITextAutocapitalizationTypeNone;
    self.navigationItem.titleView = self.bar;
    
    tabcontroller.navigationItem.titleView = self.bar;
    
    UIImage *buttonImage = [UIImage imageNamed:@"home_brown.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(homeClicked) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    tabcontroller.navigationItem.leftBarButtonItem = customBarItem;
    
    
    UIImage *buttonImageRight = [UIImage imageNamed:@"sort_descending.png"];
    UIButton *buttonRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonRight setImage:buttonImageRight forState:UIControlStateNormal];
    [buttonRight addTarget:self action:@selector(homeClicked) forControlEvents:UIControlEventTouchUpInside];
    buttonRight.frame = CGRectMake(0, 0, buttonImageRight.size.width, buttonImageRight.size.height);
    
    UIBarButtonItem *customBarItemRight = [[UIBarButtonItem alloc] initWithCustomView:buttonRight];
    tabcontroller.navigationItem.rightBarButtonItem = customBarItemRight;
    
    VideosTourViewController *controller1 = [VideosTourViewController initViewControllerWithSearchText:self.bar.text andLanguageArray:arrLanguage andStartDate:leftDate andEndDate:rightDate withSelfDrive:selfDrive];
    
    AttractionsVideoViewController *controller2 = [AttractionsVideoViewController initViewControllerWithSearchText:self.bar.text andLangArray:arrLanguage andCatArray:nil];
    
    controller1.bar = self.bar;
    controller2.bar = self.bar;
    
    NSMutableArray* viewControllers = [[NSMutableArray alloc] init];
    [viewControllers addObject:controller1];
    [viewControllers addObject:controller2];
    
    tabcontroller.viewControllers = viewControllers;
    tabcontroller.selectedIndex = 0;
    [self.navigationController pushViewController:tabcontroller animated:true];
}

-(void)homeClicked {
    [self.navigationController popToRootViewControllerAnimated:true];
}

#pragma Searchbar Delegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)sBar {
    [sBar setShowsCancelButton:YES animated:YES];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)sBar{
    [sBar resignFirstResponder];
    [self searchClicked];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)sBar{
    [sBar resignFirstResponder];
    [sBar setShowsCancelButton:NO animated:YES];
    sBar.text = @"";
}

-(void)viewDidAppear:(BOOL)animated {
    self.leftButton.frame = CGRectMake(0.0, self.view.frame.size.height - 50, 50.0, 50.0);
    self.rightButton.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 50, self.view.frame.size.height - 50, 50.0, 50.0);
    self.menuView1.frame = CGRectMake(0, self.view.frame.size.height - 100, self.menuView1.frame.size.width, self.menuView1.frame.size.height);
    
    
}

-(void)radioButtonSelectedAtIndex:(NSUInteger)index inGroup:(NSString *)groupId{
    NSLog(@"changed to %lu in %@",(unsigned long)index,groupId);
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (indexPath.section == 0) {
//        return 30;
//    }else{
//        return 44;
//    }
//}


#import "AudioRecordViewController.h"

@interface AudioRecordViewController ()

@end

@implementation AudioRecordViewController

+(AudioRecordViewController *)initViewController {
    AudioRecordViewController * controller = [[AudioRecordViewController alloc] initWithNibName:@"AudioRecordViewController" bundle:nil];
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

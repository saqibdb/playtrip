
#import <UIKit/UIKit.h>

#import <MobileCoreServices/MobileCoreServices.h>
#import <UIKit/UIKit.h>
#import <AVKit/AVPlayerViewController.h>
#import <AVKit/AVError.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MediaPlayer/MediaPlayer.h>


@interface AudioRecordViewController : UIViewController <AVAudioRecorderDelegate, AVAudioPlayerDelegate>



@end

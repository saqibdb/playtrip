
#import <UIKit/UIKit.h>

#import <MobileCoreServices/MobileCoreServices.h>
#import <UIKit/UIKit.h>
#import <AVKit/AVPlayerViewController.h>
#import <AVKit/AVError.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MediaPlayer/MediaPlayer.h>


@interface VideoRecordViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate, AVAudioRecorderDelegate, AVAudioPlayerDelegate> { // 
    NSURL * outputVideoUrl;
    int secondsRecordedVideo;
    
    IBOutlet UIButton *btnRecordVideo;
    IBOutlet UIButton *btnRecordAudio;
    NSURL *outputAudioURL;
    NSURL *finalURL;
}

@property (strong, nonatomic) NSURL *videoURL;
@property (strong, nonatomic) MPMoviePlayerController *videoController;
- (IBAction)recordAudioClicked:(id)sender;

- (IBAction)captureClicked:(id)sender;
- (IBAction)playClicked:(id)sender;

@property (strong, nonatomic) MPMoviePlayerController *moviePlayer;


+(VideoRecordViewController *)initViewController;

@end

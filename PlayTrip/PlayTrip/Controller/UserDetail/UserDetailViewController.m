
#import "UserDetailViewController.h"
#import "URLSchema.h"
#import "PlayTripManager.h"
#import "VideoTour.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "UserInformationTableViewCell.h"
#import "Attractions.h"
#import "AttractionData.h"
#import "Account.h"
#import "AccountManager.h"
#import "UIAlertController+Blocks.h"
#import "ColorConstants.h"
#import "UserInfoTopCell.h"
#import "Info.h"
#import "PlanModel.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import "ItineraryViewController.h"





@interface UserDetailViewController (){
    NSMutableArray *arrVideoTour;
    NSMutableArray *allUserPlans;
}

@end

@implementation UserDetailViewController

+ (UserDetailViewController *)initViewController:(Users *)user{
    UserDetailViewController * controller = [[UserDetailViewController alloc]initWithNibName:@"UserDetailViewController" bundle:nil];
    controller->user = user;
    controller.title = user.user_name;
    return controller;
}

+ (UserDetailViewController *)initViewControllerNew:(UserModel *)user{
    UserDetailViewController * controller = [[UserDetailViewController alloc]initWithNibName:@"UserDetailViewController" bundle:nil];
    controller->userModel = user;
    controller.title = user.user_name;
    return controller;
}

+ (UserDetailViewController *)initController:(BookmarkUser *)bookmarkUser{
    UserDetailViewController * controller = [[UserDetailViewController alloc]initWithNibName:@"UserDetailViewController" bundle:nil];
//    controller->bookmarkUser = bookmarkUser;
    controller.title = bookmarkUser.user_name;
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    arrVideoTour = [NSMutableArray new];
    tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    [locationManager startUpdatingLocation];
    
//    if(bookmarkUser){
//        userfullname = bookmarkUser.full_name;
//        bookmarkCount = [NSString stringWithFormat:@"%@",bookmarkUser.total_bookmark];
//        userAvtar = bookmarkUser.avatar;
//        userCoverPic =bookmarkUser.cover_pic;
//        bookmarkString =bookmarkUser.bookmark_users_string;
//        bookmarkUserId =bookmarkUser.entity_id;
//        
//    }
    
    //[self getUserVideoTour];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
     self.navigationController.navigationBarHidden = true;
    
    if (userModel) {
        if (userModel._id) {
            [self getPlansForUser:userModel];
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = false;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.leftButton.frame = CGRectMake(0.0, self.view.frame.size.height - 50, 50.0, 50.0);
    self.rightButton.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 50, self.view.frame.size.height - 50, 50.0, 50.0);
    CGRect mf = self.view.frame;
    self.menuView1.frame = CGRectMake(mf.size.width-250,mf.size.height - 110, self.menuView1.frame.size.width, self.menuView1.frame.size.height);
}

- (void) dealloc {
    [locationManager stopUpdatingLocation];
}


-(void)getPlansForUser:(UserModel *)userMod{
    if (!allUserPlans) {
        allUserPlans = [[NSMutableArray alloc] init];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD showWithStatus:@"Getting Plans..."];
    }
    [[PlayTripManager Instance] getUserPlansForUserID:userMod._id andWithBlock:^(id result, NSString *error) {
        [SVProgressHUD dismiss];
        if (!error) {
            NSError* errorData;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                 options:kNilOptions
                                                                   error:&errorData];
            if (errorData) {
                NSLog(@"ERROR GOTTEN = %@" ,errorData.description);
                return;
            }
            NSArray* plansDicts = [json objectForKey:@"data"];
            allUserPlans = [[NSMutableArray alloc] init];
            for (NSDictionary *planDict in plansDicts) {
                NSError *error;
                PlanModel *planModel = [[PlanModel alloc] initWithDictionary:planDict error:&error];
                if (error) {
                    NSLog(@"ERROR GOTTEN AT JSONMODEL = %@" ,error.description);
                }
                if (![allUserPlans containsObject:planModel]) {
                    [allUserPlans addObject:planModel];
                }
            }
            NSLog(@"Total Plans Gotten = %lu" ,(unsigned long)allUserPlans.count);
            allUserPlans = [[[allUserPlans reverseObjectEnumerator] allObjects] mutableCopy];
            dispatch_async(dispatch_get_main_queue(), ^{
                [tblView reloadData];
            });
        }
    }];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    currentLocation = (CLLocation *)[locations lastObject];
    target = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude);
}

-(void)getRecommendedUsers {
    
    if (!target.latitude || !target.longitude) {
        target = CLLocationCoordinate2DMake(23.6565, 72.345);
    }
    
    if(user){
        [[PlayTripManager Instance]getRecommendedUsersListWithBlock:^(id result, NSString *error) {
            if (!error) {
                user = [Users getByEntityId:user.entity_id];//Updating user object
                [tblView reloadData];
                 [SVProgressHUD dismiss];
            }
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"RecommendedUsersUpdated" object:nil];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"BookmarkDetailsUpdated" object:nil];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"ProfileReloadTable" object:nil];
        }];
    }else{
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"BookmarkDetailsUpdated" object:nil];
//        bookmarkUser = [BookmarkUser getByEntityId:bookmarkUserId];//Updating user object
//        bookmarkString = bookmarkUser.bookmark_users_string;
//        [tblView reloadData];
//        [SVProgressHUD dismiss];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"RecommendedUsersUpdated" object:nil];
//         [[NSNotificationCenter defaultCenter] postNotificationName:@"ProfileReloadTable" object:nil];
    }
}


-(void)setNavBarItems {
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_white.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backClicked)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}


-(void)getUserVideoTour{
    NSMutableDictionary *where = [NSMutableDictionary new];
    [where setObject:@"true" forKey:@"is_published"];
    [where setObject:[NSNumber numberWithBool:false] forKey:@"is_deleted"];
    NSMutableDictionary *sort = [NSMutableDictionary new];
    [sort setObject:@"-1" forKey:@"create_date"];
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Please wait.."];
    if(user){
        userId = user.entity_id;
    }else{
        userId = bookmarkUserId;
    }
    [[PlayTripManager Instance]getUsersVideoTourWithWhere:where WithSort:sort WithUserId:userId WithBlock:^(id result, NSString *error) {
        if(!error){
            arrVideoTour = [VideoTour getAll];
            [tblView reloadData];
            [SVProgressHUD dismiss];
        }
        
    }];
}

-(void)bookmarkClicked{
    if(user){
        userId = user.entity_id;
    }else{
        userId = bookmarkUserId;
    }
    if(userId){
        [self commanApiWithModel:ModelUser WithId:userId WithType:TypeBookmark];
    }
    
}

-(void)commanApiWithModel:(NSString *)modelValue WithId:(NSString *)idValue WithType:(NSString *)typeValue {
    [SVProgressHUD showWithStatus:@"Please wait.."];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    
    [[PlayTripManager Instance] addCountForModel:modelValue withType:typeValue forId:idValue WithBlock:^(id result, NSString *error) {
        if(!error){
            [self getRecommendedUsers];
            [Utils setBOOL:YES Key:@"BookmarkUserUpd"];
//            [SVProgressHUD dismiss];
        }
    }];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    
}
#pragma mark -- TableView Delegate and DataSource Method

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0){
        return 1;
    }else{
        if(allUserPlans.count <= 0){
            return 1;
        }else{
            return allUserPlans.count;
        }
    }
    
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section == 0){
        UserInfoTopCell *topCell = (UserInfoTopCell *)[tblView dequeueReusableCellWithIdentifier:@"UserInfoTopCell"];
        if (topCell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"UserInfoTopCell" owner:nil options:nil];
            topCell = (UserInfoTopCell *)[nib objectAtIndex:0];
        }
        [topCell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [topCell.btnBack addTarget:self action:@selector(backClicked) forControlEvents:UIControlEventTouchUpInside];
        Account * account = [AccountManager Instance].activeAccount;
        if(account){
            if (account.isSkip != true) {
                if(userModel){
                    topCell.lblUserName.text = userModel.full_name;
                    topCell.lblBookmarkCount.text =[NSString stringWithFormat:@"%@", userModel.total_bookmark];
                    topCell.lblTotalVideoTourCount.text = [NSString stringWithFormat:@"%@'s Video Tour(%lu)",userModel.full_name,(unsigned long)allUserPlans.count];
                    
                    
                    
                    
                    
                    if (userModel.credits) {
                        topCell.lblMoney.text =[NSString stringWithFormat:@"%@",userModel.credits];;
                    }
                    else{
                        topCell.lblMoney.text =[NSString stringWithFormat:@"%@",@"0"];;
                    }
                    if (userModel.total_bookmark) {
                        topCell.lblBookMark.text = [NSString stringWithFormat:@"%@",userModel.total_bookmark];
                    }
                    else{
                        topCell.lblBookMark.text = [NSString stringWithFormat:@"%@",@"0"];
                    }
                    if (userModel.total_view) {
                        topCell.lblViews.text = [NSString stringWithFormat:@"%@",userModel.total_view];
                    }
                    else{
                        topCell.lblViews.text = [NSString stringWithFormat:@"%@",@"0"];
                    }
                    if (userModel.total_share) {
                        topCell.lblShares.text = [NSString stringWithFormat:@"%@",userModel.total_share];
                    }
                    else{
                        topCell.lblShares.text = [NSString stringWithFormat:@"%@",@"0"];
                    }

                    
                    topCell.btnBookmark.tag = indexPath.row;
                    
                    [topCell.btnBookmark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
                    topCell.btnBookmark.selected = NO;
                    if (account) {
                        for (NSString *userBookmarked in userModel.bookmark_users) {
                            if ([userBookmarked isEqualToString:account.userId]) {
                                [topCell.btnBookmark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                                topCell.btnBookmark.selected = YES;
                            }
                        }
                    }
                    [topCell.btnBookmark addTarget:self action:@selector(bookmarkClicked) forControlEvents:UIControlEventTouchUpInside];
                    
                    
                    
                    
//                    if ([user.bookmark_users_string containsString:account.userId]) {
//                        [topCell.btnBookmark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
//                        topCell.btnBookmark.selected = YES;
//                    } else {
//                        [topCell.btnBookmark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
//                        topCell.btnBookmark.selected = NO;
//                    }
//                    [topCell.btnBookmark addTarget:self action:@selector(bookmarkClicked) forControlEvents:UIControlEventTouchUpInside];
                    
                    if(![userModel.avatar isEqualToString:@""]){
                        
                        NSString * imgId = userModel.avatar;
                        NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
                        topCell.imgAvatar.url = [NSURL URLWithString:urlString];
                        topCell.imgAvatar.layer.cornerRadius = topCell.imgAvatar.frame.size.width/2;
                        topCell.imgAvatar.layer.masksToBounds = YES;
                    }else{
                        //  imgProfile.image = [UIImage imageNamed:@"mexico.jpg"];
                        topCell.imgAvatar.layer.cornerRadius = topCell.imgAvatar.frame.size.width/2;
                        topCell.imgAvatar.layer.masksToBounds = YES;
                    }
                    if(![userModel.cover_pic isEqualToString:@""]){
                        
                        NSString * imgId = userModel.cover_pic;
                        NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
                        topCell.imgCover.url = [NSURL URLWithString:urlString];
                        
                        topCell.imgCover.layer.masksToBounds = YES;
                    }else{
                        topCell.imgCover.backgroundColor = [ColorConstants appYellowColor];
                    }
                }else{
                    
                    topCell.lblUserName.text = userfullname;
                    topCell.lblBookmarkCount.text =bookmarkCount;
                    topCell.lblTotalVideoTourCount.text = [NSString stringWithFormat:@"%@'s Video Tour(%lu)",userfullname,(unsigned long)allUserPlans.count];
                    
                    if ([bookmarkString containsString:account.userId]) {
                        [topCell.btnBookmark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                        topCell.btnBookmark.selected = YES;
                    } else {
                        [topCell.btnBookmark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
                        topCell.btnBookmark.selected = NO;
                    }
                    [topCell.btnBookmark addTarget:self action:@selector(bookmarkClicked) forControlEvents:UIControlEventTouchUpInside];
                    
                    if(![userAvtar isEqualToString:@""]){
                        
                        NSString * imgId = userAvtar;
                        NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
                        topCell.imgAvatar.url = [NSURL URLWithString:urlString];
                        topCell.imgAvatar.layer.cornerRadius = topCell.imgAvatar.frame.size.width/2;
                        topCell.imgAvatar.layer.masksToBounds = YES;
                    }else{
                        //  imgProfile.image = [UIImage imageNamed:@"mexico.jpg"];
                        topCell.imgAvatar.layer.cornerRadius = topCell.imgAvatar.frame.size.width/2;
                        topCell.imgAvatar.layer.masksToBounds = YES;
                    }
                    if(![userCoverPic isEqualToString:@""]){
                        
                        NSString * imgId = userCoverPic;
                        NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
                        topCell.imgCover.url = [NSURL URLWithString:urlString];
                        
                        topCell.imgCover.layer.masksToBounds = YES;
                    }else{
                        topCell.imgCover.backgroundColor = [ColorConstants appYellowColor];
                    }
                }
            
            }
        }
        return topCell;        
    }else{
        
        if (allUserPlans.count <= 0) {
            UITableViewCell *videoTourCell =  [tableView dequeueReusableCellWithIdentifier:@"VideoTourCell"];
            if (!videoTourCell) {
                videoTourCell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"VideoTourCell"];
            }
            videoTourCell.textLabel.text = @"No VideoTour Found";
            [videoTourCell.textLabel setTextAlignment:NSTextAlignmentCenter];
            [videoTourCell setUserInteractionEnabled:NO];
            return videoTourCell;
        }
        
        
        static NSString *videoTourCellIdentifier = @"UserInformationTableViewCell";
        UserInformationTableViewCell *videoTourCell = (UserInformationTableViewCell *)[tblView dequeueReusableCellWithIdentifier:videoTourCellIdentifier];
        if (videoTourCell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:videoTourCellIdentifier owner:nil options:nil];
            videoTourCell = (UserInformationTableViewCell *)[nib objectAtIndex:0];
        }
        [videoTourCell setSelectionStyle:UITableViewCellSelectionStyleGray];
        videoTourCell.btnList.hidden = YES;
        
        PlanModel *plan = allUserPlans[indexPath.row];
        
        
        videoTourCell.lblVideoTourName.text = plan.name;
        videoTourCell.lblDescription.text = plan.description;
        videoTourCell.lblCreateDate.text = ([plan.create_date length]>10 ? [plan.create_date substringToIndex:10] : plan.create_date);
        
        if(![plan.cover_photo isEqualToString:@""]){
            NSString * imgId = plan.cover_photo;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            //videoTourCell.imgVideoTour.url = [NSURL URLWithString:urlString];
            
            [videoTourCell.imgVideoTour sd_setShowActivityIndicatorView:YES];
            [videoTourCell.imgVideoTour sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            [videoTourCell.imgVideoTour sd_setImageWithURL:[NSURL URLWithString:urlString]
                               placeholderImage:[UIImage imageNamed:@"def_video_img.png"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                                   if (!error) {
                                       videoTourCell.imgVideoTour.image = image;
                                       [videoTourCell layoutIfNeeded];
                                   }
                                   else{
                                       videoTourCell.imgVideoTour.url = [NSURL URLWithString:urlString];
                                   }
                               }];
            
            
            
        }
        videoTourCell.lblLanguge.text = plan.language;
        videoTourCell.lblCountry.hidden = YES;
        videoTourCell.lblDistrict.hidden = YES;
        videoTourCell.lblCity.hidden = YES;
        
        for (AttractionModel *attraction in plan.attractions) {
            if (attraction.attr_data.count) {
                AttractionDataModel *attratctionData = [attraction.attr_data objectAtIndex:0];
                videoTourCell.lblTime.text = attratctionData.time;
                if (attratctionData.info.country_id) {
                    videoTourCell.lblCountry.text = attratctionData.info.country_id.name;
                    videoTourCell.lblDistrict.text = attratctionData.info.district_id.name;
                    videoTourCell.lblCity.text = attratctionData.info.city_id.name;
                    videoTourCell.lblCountry.hidden = NO;
                    videoTourCell.lblDistrict.hidden = NO;
                    videoTourCell.lblCity.hidden = NO;
                    
                    
                    break;
                }
            }
        }
        videoTourCell.lblBookmarks.text = [NSString stringWithFormat:@"%@",plan.total_bookmark];
        videoTourCell.lblViews.text = [NSString stringWithFormat:@"%@",plan.total_view];
        videoTourCell.lblShares.text = [NSString stringWithFormat:@"%@",plan.total_share];
        videoTourCell.lblRevert.text = [NSString stringWithFormat:@"%@",plan.remuneration_amount];
        
        [videoTourCell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
        videoTourCell.btnBookMark.selected = NO;
        Account * account = [AccountManager Instance].activeAccount;

        if (account) {
            for (NSString *userM in plan.bookmark_users) {
                if ([userM isEqualToString:account.userId]) {
                    [videoTourCell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                    videoTourCell.btnBookMark.selected = YES;
                }
            }
        }
        [videoTourCell.btnBookmark addTarget:self action:@selector(bookmarkClickedVideoTour:) forControlEvents:UIControlEventTouchUpInside];
        videoTourCell.btnBookmark.tag = indexPath.row;
        
        videoTourCell.lblLanguge.hidden = YES;
        if([plan.language  isEqualToString: @"sc"]){
            videoTourCell.languageImage.image = [UIImage imageNamed:@"speaker_.png"];
        }else if([plan.language  isEqualToString: @"tc"]){
            videoTourCell.languageImage.image = [UIImage imageNamed:@"speaker_2.png"];
        }else{
            videoTourCell.languageImage.image = [UIImage imageNamed:@"speaker_Eng.png"];
        }
        
        
        
        /*
         
         @property (strong, nonatomic) IBOutlet URLImageView *imgVideoTour;
         @property (weak, nonatomic) IBOutlet UILabel *lblTime;
         
         @property (strong, nonatomic) IBOutlet UILabel *lblVideoTourName;
         @property (strong, nonatomic) IBOutlet UILabel *lblCreateDate;
         @property (strong, nonatomic) IBOutlet UILabel *lblDescription;
         @property (strong, nonatomic) IBOutlet UILabel *lblBookmarks;
         @property (strong, nonatomic) IBOutlet UILabel *lblViews;
         @property (strong, nonatomic) IBOutlet UILabel *lblShares;
         @property (strong, nonatomic) IBOutlet UILabel *lblRevert;
         @property (strong, nonatomic) IBOutlet UIButton *btnList;
         @property (strong, nonatomic) IBOutlet UIButton *btnDelete;
         @property (strong, nonatomic) IBOutlet UIButton *btnEdit;
         @property (strong, nonatomic) IBOutlet UIButton *btnBookmark;
         @property (strong, nonatomic) IBOutlet UIView *btnView;
         @property (strong, nonatomic) IBOutlet UIButton *btnBookMark;
         
         
        VideoTour * v = [arrVideoTour objectAtIndex:indexPath.row];
        
        NSMutableArray *attraction = [NSMutableArray new];
        NSMutableArray *attractionData = [NSMutableArray new];
        Attractions *a;
        AttractionData *aData;
        attraction = [[v.attractionsSet allObjects] mutableCopy];
        if(attraction.count>0){
            a =[attraction objectAtIndex:0];
        }
        if(attractionData.count>0){
            aData = [attractionData objectAtIndex:0];
        }
        
        Account * account = [AccountManager Instance].activeAccount;
        if (account) {
            if ([v.bookmark_users_string containsString:account.userId]) {
                [videoTourCell.btnBookmark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                videoTourCell.btnBookmark.selected = YES;
            } else {
                [videoTourCell.btnBookmark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
                videoTourCell.btnBookmark.selected = NO;
            }
        } else {
            [videoTourCell.btnBookmark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
            videoTourCell.btnBookmark.selected = NO;
        }
        
        videoTourCell.lblCreateDate.text = [Utils dateFormatyyyyMMdd:v.from_date];
        videoTourCell.lblVideoTourName.text = v.name;
        
        videoTourCell.lblDescription.text = [NSString stringWithFormat:@"[%@ days] %@",v.days, aData.info.address];
        
        if(v.thumbnail != nil){
            
            NSString * imgId = v.thumbnail;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            videoTourCell.imgVideoTour.url = [NSURL URLWithString:urlString];
        }else{
            videoTourCell.imgVideoTour.image = [UIImage imageNamed:@"mexico.jpg"];
        }
        videoTourCell.lblBookmarks.text = [NSString stringWithFormat:@"%@",v.total_bookmark];
        videoTourCell.lblViews.text = [NSString stringWithFormat:@"%@",v.total_view];
        videoTourCell.lblRevert.text = [NSString stringWithFormat:@"%@",v.remuneration_amount];
        videoTourCell.lblShares.text = [NSString stringWithFormat:@"%@",v.total_share];
        videoTourCell.btnBookmark.tag = indexPath.row;
        [videoTourCell.btnBookmark addTarget:self action:@selector(bookmarkClickedVideoTour:) forControlEvents:UIControlEventTouchUpInside];
         */
        return videoTourCell;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if(indexPath.section == 1){
        PlanModel *plan = [allUserPlans objectAtIndex:indexPath.row];
        //VideoTour *v = [arrVideoTour objectAtIndex:indexPath.row];
        ItineraryViewController * controller = [ItineraryViewController initViewControllerWithPlan:plan];
        [self.navigationController  pushViewController:controller animated:YES];
    }
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        return 278.0;
    }else{
        return 110.0;
    }
}

-(void) backClicked{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)bookmarkClickedVideoTour:(UIButton *)sender {
    
    if (!sender.selected) {
        [SVProgressHUD show];
        PlanModel *plan = allUserPlans[[sender tag]];
        [[PlayTripManager Instance] addCountForModel:ModelPlan withType:TypeBookmark forId:plan._id WithBlock:^(id result, NSString *error) {
            if(!error){
                [sender setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                sender.selected = YES;
            }
            [SVProgressHUD dismiss];
        }];
    }
    
    
    
    
    
    //VideoTour * v = [arrVideoTour objectAtIndex:sender.tag];
    //[self commanApiWithModel:ModelPlan WithId:plan._id WithType:TypeBookmark WithTagValue:(int)[sender tag]];
}
-(void)commanApiWithModel:(NSString *)modelValue WithId:(NSString *)idValue WithType:(NSString *)typeValue WithTagValue:(int)tagNumber {
    [[PlayTripManager Instance] addCountForModel:modelValue withType:typeValue forId:idValue WithBlock:^(id result, NSString *error) {
        if(error){
            [UIAlertController showAlertInViewController:self withTitle:ALERT_TITLE message:error cancelButtonTitle:@"Ok" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:nil];
        }else{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LatestVideoTourUpdated" object:nil];
            
           
            
            if(tagNumber == 100){
                if([btnBookmark currentImage] == [UIImage imageNamed:@"bookmark_tag_us.png"]){
                    [btnBookmark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                }else{
                    [btnBookmark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
                }
            }
        }
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end

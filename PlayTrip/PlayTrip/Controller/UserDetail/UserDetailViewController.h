
#import <UIKit/UIKit.h>
#import "URLImageView.h"
#import "Users.h"
#import "MenuButtonViewController.h"
#import "BookmarkUser.h"
#import <CoreLocation/CoreLocation.h>
#import "UserModel.h"

@interface UserDetailViewController : MenuButtonViewController <UITableViewDelegate,UITableViewDataSource, CLLocationManagerDelegate> {
    IBOutlet URLImageView *imgCover;
    IBOutlet URLImageView *imgProfile;
    IBOutlet UIButton *btnBookmark;
    
    IBOutlet UITableView *tblView;
    IBOutlet UILabel *lblUserName;
    IBOutlet UILabel *lblReward;
    IBOutlet UILabel *lblBookmarks;
    NSString *userId;
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLLocationCoordinate2D target;
    
    Users *user;
    UserModel *userModel;
//    BookmarkUser *bookmarkUser;
    
    // Temp
    NSString * userfullname;
    NSString * bookmarkCount;
    NSString * userAvtar;
    NSString * userCoverPic;
    NSString * bookmarkString;
    NSString * bookmarkUserId;
    // Temp
    
}
+ (UserDetailViewController *)initViewController:(Users *)user;
+ (UserDetailViewController *)initViewControllerNew:(UserModel *)user;
//+ (UserDetailViewController *)initController:(BookmarkUser *)bookmarkUser;

@end


#import "AttachAudioViewController.h"
#import "KGModalWrapper.h"
#import "VideoCameraInputManager.h"
#import "SVProgressHUD.h"
//#import "VideoDetailsViewController.h"
#import "VideoPostViewController.h"
#import "Constants.h"
#import "Localisator.h"

@interface AttachAudioViewController () {
    VideoCameraInputManager *videoCameraInputManager;
    AVAudioRecorder *recorder;
    AVAudioPlayer *player;
    AVPlayer *avPlayer;
    UIImageView *currentPlayingImageView;
}
@end

@implementation AttachAudioViewController

+(AttachAudioViewController *)initViewControllerWithURL:(NSURL *)vidUrl withTime:(int)recordedVideoTime {
    AttachAudioViewController * controller = [[AttachAudioViewController alloc] initWithNibName:@"AttachAudioViewController" bundle:nil];
    controller->vidUrl = vidUrl;
    controller.title = @"Add Narration";
    controller->secondsRecordedVideo = recordedVideoTime;
    return controller;
}

-(void)viewDidLoad {
    [super viewDidLoad];
    nextViewUrl = vidUrl;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [ self setNavBarItems];
    [videoPreviewView bringSubviewToFront:btnPlay];

    progressLine.progress = 0;
}
-(void)viewDidAppear:(BOOL)animated{
    if (!currentPlayingImageView) {
        currentPlayingImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, videoPreviewView.frame.size.width, videoPreviewView.frame.size.height)];
        [currentPlayingImageView setContentMode:UIViewContentModeScaleAspectFit];
        
        
        
        self.videoController = [[MPMoviePlayerController alloc] init];
       
        if (mergedAudioUrl) {
            [self.videoController setContentURL:mergedAudioUrl];
        }
        else{
            [self.videoController setContentURL:vidUrl];
        }
        
        [self.videoController.view setFrame:CGRectMake(0, 0, videoPreviewView.frame.size.width, videoPreviewView.frame.size.height)];
        
        [self.videoController setControlStyle:MPMovieControlStyleNone];
        [videoPreviewView addSubview:self.videoController.view];
        [videoPreviewView addSubview:currentPlayingImageView];

    }
    
    
    
    
    AVURLAsset* asset = [AVURLAsset URLAssetWithURL:vidUrl options:nil];
    AVAssetImageGenerator* generator = [AVAssetImageGenerator assetImageGeneratorWithAsset:asset];
    generator.appliesPreferredTrackTransform = YES;
    UIImage* image = [UIImage imageWithCGImage:[generator copyCGImageAtTime:CMTimeMake(0, 1) actualTime:nil error:nil]];
    currentPlayingImageView.image = image;
    
    [videoPreviewView bringSubviewToFront:btnPlay];
}

-(void)setNavBarItems {
    self.navigationController.navigationBarHidden = NO;
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_white.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backClicked)];
    UIBarButtonItem * nextButton = [[UIBarButtonItem alloc] initWithTitle:LOCALIZATION(@"next") style:UIBarButtonItemStyleDone target:self action:@selector(nextClicked)];
    
    self.navigationItem.rightBarButtonItem = nextButton;

    self.navigationItem.leftBarButtonItem = backButton;
    
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

-(void)backClicked {
    [self.navigationController popViewControllerAnimated:true];
//    [self.navigationController popToRootViewControllerAnimated:true];
}

-(IBAction)recordAudioClicked:(id)sender {
    NSString * msg = [NSString stringWithFormat:@"Audio will be recorded for 10 seconds. Please keep device volume at maximum and start recording."];
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self playVideoWithURL:vidUrl];
        [self startRecording];
        
    }];
    UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:nil];
    [alert addAction:okayAction];
    [alert addAction:cancelAction];
    [self.navigationController presentViewController:alert animated:true completion:nil];
}

-(void)startRecording {
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
//    [SVProgressHUD showWithStatus:@"Recording..."];
    NSArray *pathComponents = [NSArray arrayWithObjects: [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject], @"MyAudioMemo.m4a", nil];
    recordedAudioUrl = [NSURL fileURLWithPathComponents:pathComponents];
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    
    [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];

    
    
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    recorder = [[AVAudioRecorder alloc] initWithURL:recordedAudioUrl settings:recordSetting error:NULL];
    recorder.delegate = self;
    recorder.meteringEnabled = YES;
    [recorder prepareToRecord];
    
    if (player.playing) {
        [player stop];
    }
    
    if (!recorder.recording) {
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];

        [session setActive:YES error:nil];
        [recorder record];
        audioProgress.progress = 0.0;
        [NSThread detachNewThreadSelector:@selector(startjob) toTarget:self withObject:nil];
        [self performSelector:@selector(stopRecording) withObject:self afterDelay:9];
    } else {
        [recorder pause];
    }
}

- (void )startjob {
    [NSThread sleepForTimeInterval:2];
    [self performSelectorOnMainThread:@selector(moreProgress) withObject:nil waitUntilDone:NO];
}

- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *playerItem = [notification object];
    [playerItem seekToTime:kCMTimeZero];
    for (CALayer * subView in videoPreviewView.layer.sublayers) {
        [subView removeFromSuperlayer];
    }
}

- (void) moreProgress {
    float actual = [audioProgress progress];
    if (actual < 10) {
        audioProgress.progress = actual + 1;
        [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(moreProgress) userInfo:nil repeats:NO];
    }
}

- (void)stopRecording {
    [SVProgressHUD dismiss];
    [recorder stop];
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
}

- (void) audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder    successfully:(BOOL)flag{
    if (flag) {
        [self mergeAndSaveWithAudioUrl:recordedAudioUrl];
    }
}

- (IBAction)cancelAudioClicked:(id)sender {
    [KGModalWrapper hideView];
}

- (IBAction)playClicked:(id)sender {
    [self playVideoWithURL:mergedAudioUrl];
}

- (IBAction)audioListClicked:(id)sender {
    btnDone.enabled = false;
    btnDone.alpha = 0.5;
    [KGModalWrapper showWithContentView:listView];
    [_tblView reloadData];
}

- (IBAction)doneClicked:(id)sender {
    [KGModalWrapper hideView];
    if (recordedAudioUrl) {
        [self mergeAndSaveWithAudioUrl:recordedAudioUrl];
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    if (indexPath.row == 0) {
        cell.textLabel.text = @"Audio 1";
    } else if (indexPath.row == 1) {
        cell.textLabel.text = @"Audio 2";
    } else if (indexPath.row == 2) {
        cell.textLabel.text = @"Audio 3";
    } else if (indexPath.row == 3) {
        cell.textLabel.text = @"Audio 4";
    } else {
        cell.textLabel.text = @"Audio 5";
    }
    cell.accessoryType = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    if (indexPath.row == 0) {
        recordedAudioUrl = [[NSBundle mainBundle] URLForResource: @"audio1" withExtension:@"mp3"];
    } else if (indexPath.row == 1) {
        recordedAudioUrl = [[NSBundle mainBundle] URLForResource: @"audio2" withExtension:@"mp3"];
    } else if (indexPath.row == 2) {
        recordedAudioUrl = [[NSBundle mainBundle] URLForResource: @"audio3" withExtension:@"mp3"];
    } else if (indexPath.row == 3) {
        recordedAudioUrl = [[NSBundle mainBundle] URLForResource: @"audio4" withExtension:@"mp3"];
    } else {
        recordedAudioUrl = [[NSBundle mainBundle] URLForResource: @"audio5" withExtension:@"mp3"];
    }
    btnDone.enabled = true;
    btnDone.alpha = 1.0;
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryNone;
}

-(void)mergeAndSaveWithAudioUrl:(NSURL *)audUrl {
    [SVProgressHUD showWithStatus:@"Merging Audio..."];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    AVMutableComposition* mixComposition = [AVMutableComposition composition];
    NSURL *audio_url = audUrl;
    
    NSURL * video_url = vidUrl;
    
    AVURLAsset  *videoAsset = [[AVURLAsset alloc]initWithURL:video_url options:nil];
    CMTime a = videoAsset.duration;
    
    CMTimeRange video_timeRange = CMTimeRangeMake(kCMTimeZero,a);
    
    AVURLAsset  *audioAsset = [[AVURLAsset alloc]initWithURL:audio_url options:nil];
    
    AVMutableCompositionTrack *b_compositionAudioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    b_compositionAudioTrack.preferredVolume = 100.0;
    
    if ([audioAsset tracksWithMediaType:AVMediaTypeAudio].count <= 0) {
        [Utils showAlertWithMessage:@"Audio not saved properly. Try again"];
        self.view.userInteractionEnabled = true;
        self.navigationController.view.userInteractionEnabled = YES;
        return;
    }
    
    [b_compositionAudioTrack insertTimeRange:video_timeRange ofTrack:[[audioAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0] atTime:kCMTimeZero error:nil];
    
    
    
    
    
    
    
    
    AVMutableCompositionTrack *a_compositionVideoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    [a_compositionVideoTrack insertTimeRange:video_timeRange ofTrack:[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:kCMTimeZero error:nil];

    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    NSString *outputFilePath = [docsDir stringByAppendingPathComponent:[NSString stringWithFormat:@"FinalVideo.mov"]];
    NSURL *outputFileUrl = [NSURL fileURLWithPath:outputFilePath];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:outputFilePath])
        [[NSFileManager defaultManager] removeItemAtPath:outputFilePath error:nil];
    
    AVMutableAudioMix *audioMix = [AVMutableAudioMix audioMix];
    AVMutableAudioMixInputParameters *audioInputParams = [AVMutableAudioMixInputParameters audioMixInputParametersWithTrack:b_compositionAudioTrack] ;
    
    [audioInputParams setVolumeRampFromStartVolume:99.5f toEndVolume:100.0f timeRange:CMTimeRangeMake(kCMTimeZero, videoAsset.duration)];
    [audioInputParams setTrackID:b_compositionAudioTrack.trackID];
    audioMix.inputParameters = [NSArray arrayWithObject:audioInputParams];
    
    
    
    AVAssetExportSession* _assetExport = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
    _assetExport.outputFileType = @"com.apple.quicktime-movie";
    _assetExport.outputURL = outputFileUrl;
    _assetExport.audioMix=audioMix;
    _assetExport.shouldOptimizeForNetworkUse=YES;

    
    
    [_assetExport exportAsynchronouslyWithCompletionHandler:
     ^(void ) {
         dispatch_async(dispatch_get_main_queue(), ^{
             AVURLAsset* avUrl = [[AVURLAsset alloc]initWithURL:_assetExport.outputURL options:nil];
             mergedAudioUrl = [avUrl URL];
             nextViewUrl = mergedAudioUrl;
             [SVProgressHUD dismiss];
             
             [Utils setValue:mergedAudioUrl.absoluteString Key:AudMergedNotCompleted];
//             [self playVideoWithURL:mergedAudioUrl];
         });
     }];
}

-(void)playVideoWithURL:(NSURL *)url {
    
    
    if (url) {
        [self.videoController setContentURL:url];
    }
    else{
        [self.videoController setContentURL:vidUrl];
    }

    
    
    
    [self.videoController play];
    
    currentPlayingImageView.hidden = YES;
    btnPlay.hidden = YES;
    
    
    double delayInSeconds = 11.0;
    countDownTime = 10.0;
    [progressLine setProgress:0 animated:YES];
    timerCountDown = [NSTimer scheduledTimerWithTimeInterval:1  target:self selector:@selector(updateProgressLine) userInfo:nil repeats:YES];
    
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    btnRecordAudio.enabled = NO;
    btnMusicList.enabled = NO;
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        if(self.videoController.playbackState != MPMoviePlaybackStatePlaying) {
            [videoPreviewView bringSubviewToFront:btnPlay];
            btnPlay.hidden = NO;
        }
    });
}

-(void)updateProgressLine {
    countDownTime--;
    if (countDownTime <= 0) {
        //        lblTime.text = [NSString stringWithFormat:@"10s/10s"];
        btnRecordAudio.enabled = YES;
        btnMusicList.enabled = YES;
        [timerCountDown invalidate];
    } else {
        //        lblTime.text = [NSString stringWithFormat:@"%ds/10s", 10 - countDownTime];
    }
    float pro = (10 - countDownTime)/10.0;
    [progressLine setProgress:pro animated:YES];
}

-(void)nextClicked{
    VideoPostViewController * controller = [VideoPostViewController initViewControllerWithURL:nextViewUrl];
    [self.videoController stop];
    [self.navigationController pushViewController:controller animated:true];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.videoController stop];
    [timerCountDown invalidate];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

@end


/*
 
 - (void)exportDidFinish:(AVAssetExportSession*)session {
 if(session.status == AVAssetExportSessionStatusCompleted) {
 NSURL *outputURL = session.outputURL;
 ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
 if([library videoAtPathIsCompatibleWithSavedPhotosAlbum:outputURL]) {
 [library writeVideoAtPathToSavedPhotosAlbum:outputURL
 completionBlock:^(NSURL *assetURL, NSError *error) {
 dispatch_async(dispatch_get_main_queue(), ^{
 if (error) {
 
 } else {
 mergedAudioUrl = outputURL;
 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
 [self playVideoWithURL:outputURL];
 });
 
 }
 });
 }];
 }
 }
 }
 
 
 */

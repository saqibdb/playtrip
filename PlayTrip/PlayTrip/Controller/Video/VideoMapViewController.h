
#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "AttractionData.h"
#import "CLPlacemark+HNKAdditions.h"
#import <CoreLocation/CoreLocation.h>
#import "Categori.h"

@interface VideoMapViewController : UIViewController<GMSMapViewDelegate, CLLocationManagerDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate>{
    
    IBOutlet GMSMapView *mapView;
    IBOutlet UIButton *btnAddAttr;
    IBOutlet UIView *addAttractionView;
    IBOutlet UITextField *txtName;
    IBOutlet UILabel *lblType;
    IBOutlet UILabel *lblTopMain;
    IBOutlet UIView *viewType;
    IBOutlet UIButton *btnSave;
    IBOutlet UIButton *btnCancel;
    
    IBOutlet UIScrollView *scrollviewPop;
    
    

    __weak IBOutlet UIView *viewCountry;
    
    __weak IBOutlet UIView *viewCity;
    
    __weak IBOutlet UIView *viewDistrict;
    
    
    __weak IBOutlet UILabel *lblCountry;
    
    
    __weak IBOutlet UILabel *lblCity;
    
    __weak IBOutlet UILabel *lblDistrict;
    
    
    
    __weak IBOutlet UITextField *txtDiscription;
    
    __weak IBOutlet UITextField *txtPhoneNumber;
    
    __weak IBOutlet UILabel *addressText;
    
    
    
    __weak IBOutlet UIImageView *coverImage;
    
    
    
    
    Categori * selectedCategory;
    Attractions *selectedAttraction;

    NSString * selectedAttractionId;

    NSMutableArray *categoryArray;
    NSMutableArray *attrArray;
    
    NSMutableArray *categoryArrayWithLanguages;

    
    BOOL isallocated, isAddAttrProcess;
    
    CLLocation * selectedLocation;
    CLLocationManager *locationManager;
    CLLocationCoordinate2D target;

    NSString * selectedLocationName;
    NSString *newAddedAttr;
    IBOutlet UIView *viewDetail;
    IBOutlet UIImageView *imgDetail;
    IBOutlet UILabel *lblMainDetail;
    IBOutlet UILabel *lblDescDetail;
    IBOutlet UIButton *btnPick;
    
    
    
    IBOutlet UILabel *lblTop;
    IBOutlet UILabel *lblAddress;
    IBOutlet UILabel *lblLocType;
    IBOutlet UILabel *lblLocCountry;
    IBOutlet UILabel *lblLocDistrict;
    IBOutlet UILabel *lblLocCity;
    
    IBOutlet UILabel *lblImageUploadDescription;
    

    
    
}

- (IBAction)addAttrClicked:(id)sender;
- (IBAction)cancelClicked:(id)sender;
- (IBAction)nextClicked:(id)sender;
- (IBAction)selectTypeClicked:(id)sender;
- (IBAction)myLocationAction:(UIButton *)sender;



- (IBAction)selectCountryClicked:(id)sender;

- (IBAction)selectCityClicked:(id)sender;

- (IBAction)selectDistrictClicked:(id)sender;



- (IBAction)coverCameraAction:(id)sender;


@property (nonatomic) NSMutableArray *allAttractions;
@property (nonatomic) UIImage *videoFirstFrameImage;


+(VideoMapViewController *)initViewController ;

@end

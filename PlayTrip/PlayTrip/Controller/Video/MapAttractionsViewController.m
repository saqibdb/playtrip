
#import "MapAttractionsViewController.h"
#import "ColorConstants.h"
#import "Utils.h"
#import "SVProgressHUD.h"
#import "PlayTripManager.h"
#import "KGModalWrapper.h"
#import "AddAttractionToDraftView.h"

@interface MapAttractionsViewController ()
@end

@implementation MapAttractionsViewController

+(MapAttractionsViewController *)initViewController {
    MapAttractionsViewController * controller = [[MapAttractionsViewController alloc] initWithNibName:@"MapAttractionsViewController" bundle:nil];
    controller.title = @"Map";
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    
    mapView = [GMSMapView new];
    isallocated = NO;
    mapView.myLocationEnabled = YES;
    mapView.delegate = self;
    mapView.settings.myLocationButton = TRUE;
    mapView.settings.compassButton = YES;
    
    self.view = mapView;
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = false;
    [self setNavBarItems];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    [mapView addObserver:self forKeyPath:@"myLocation" options:NSKeyValueObservingOptionNew context:nil];
}

-(void)setNavBarItems {
    // Arun :: customizing the nav bar icons
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[ColorConstants appBrownColor]}];
    [self.navigationController.navigationBar setBackgroundColor:[ColorConstants appYellowColor]];
    
    UIImage *buttonImage = [UIImage imageNamed:@"back.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(backClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
}

-(void)backClicked {
//    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    if (!currentLocationName) {
        currentLocation = newLocation;
        [Utils getAddressFromLocation:newLocation complationBlock:^(NSString *name) {
            if (name) {
                currentLocationName = name;
            }
        }];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if([keyPath isEqualToString:@"myLocation"]) {
        CLLocation *location = [object myLocation];
        target = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        if (isallocated == NO) {
            [self getAttractions];
            isallocated = YES;
        }
    }
}

-(void)getAttractions {
    //    return;
    attrArray = [NSMutableArray new];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Loading..."];
    [[PlayTripManager Instance] getAttractionsListWithDistance:@"50" WithLattitude:target.latitude WithLongitude:target.longitude WithBlock:^(id result, NSString *error) {
        if (error) {
            [SVProgressHUD dismiss];
        } else {
            [SVProgressHUD dismiss];
            attrArray = [Attractions getAll];
            NSLog(@"%lu", (unsigned long)attrArray.count);
            for (Attractions *a in attrArray) {
                GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
                GMSMarker *finishMarker = [[GMSMarker alloc] init];
                finishMarker.title = a.name;
                finishMarker.icon = [UIImage imageNamed:(@"aaa.png")];
                finishMarker.position = CLLocationCoordinate2DMake(a.loc_lat.doubleValue, a.loc_long.doubleValue);
                finishMarker.map = mapView;
                
                bounds = [bounds includingCoordinate:finishMarker.position];
                [mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:30.0f]];
            }
            
        }
    }];
}

-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {
    [KGModalWrapper showWithContentView:detailView];
    detailView.layer.borderColor = [ColorConstants appBrownColor].CGColor;
    btnAddPlan.layer.cornerRadius =  btnAddPlan.layer.frame.size.height / 2;
    
    for (Attractions *a in attrArray) {
        if ([marker.title isEqualToString:a.name]) {
            selectedAttractionId = a.entity_id;
            selectedAttraction = a;
            lblName.text = a.name;
            NSString * fullStr = [NSString stringWithFormat:@"%@\n%@", a.address, a.phone_no];
            lblDetails.text = fullStr;
            break;
        }
    }
    return NO;
}

+ (void)removeGMSBlockingGestureRecognizerFromMapView:(GMSMapView *)mapView {
    if([mapView.settings respondsToSelector:@selector(consumesGesturesInView)]) {
        mapView.settings.consumesGesturesInView = NO;
    } else {
        for (id gestureRecognizer in mapView.gestureRecognizers)  {
            if (![gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]]) {
                [mapView removeGestureRecognizer:gestureRecognizer];
            }
        }
    }
}

- (IBAction)addPlanClicked:(id)sender {
    AddAttractionToDraftView *view = [AddAttractionToDraftView initWithNibAttractionId:selectedAttractionId];
    [KGModalWrapper showWithContentView:view];
}

-(void)viewWillDisappear:(BOOL)animated {
    @try{
        [mapView removeObserver:self forKeyPath:@"myLocation"];
    }@catch(id anException){
        //do nothing, obviously it wasn't attached because an exception was thrown
    }
}

- (void)dealloc {
    @try{
        [mapView removeObserver:self forKeyPath:@"myLocation"];
    }@catch(id anException){
        //do nothing, obviously it wasn't attached because an exception was thrown
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

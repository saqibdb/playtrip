
#import "VideoPostViewController.h"
#import "Utils.h"
#import "ColorConstants.h"
#import "AppDelegate.h"
#import "VideoMapViewController.h"
#import "SVProgressHUD.h"
#import "PlayTripManager.h"
#import "DraftTourViewController.h"

#import "Account.h"
#import "AccountManager.h"
#import "Localisator.h"
#import "KGModalWrapper.h"

@import QuartzCore;
@import CoreText;
@import AWSS3;

@interface VideoPostViewController () <UITextFieldDelegate> {
    UIImageView *currentPlayingImageView;
    UIDatePicker *datePicker;
    UIDatePicker *endDatePicker;
}
@end

@implementation VideoPostViewController

+(VideoPostViewController *)initViewControllerWithURL:(NSURL *)vidUrl {
    VideoPostViewController * controller = [[VideoPostViewController alloc] initWithNibName:@"VideoPostViewController" bundle:nil];
    controller->vidUrl = vidUrl;
    [controller setTitle:LOCALIZATION(@"posy_attraction_video")];
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //self.automaticallyAdjustsScrollViewInsets = YES;
    [self setNeedsStatusBarAppearanceUpdate];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];

    [[NSUserDefaults standardUserDefaults] removeObjectForKey:LocationId];
    
    txtDescription.delegate = self;
    
    
    UIScrollView *topScrollView = (UIScrollView *)self.view;
    topScrollView.contentSize = CGSizeMake(topScrollView.frame.size.width, 1000);
    
    //scrollViewMain.contentSize = CGSizeMake(scrollViewMain.frame.size.width, 1000);
    
    [self initLocManager];
    [self allocArrays];
    [self setPickerForTime];
//    [self getAttractions];
    NSLog (@"Font families: %@", [UIFont familyNames]);

}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = true;
    [[self navigationController] setNavigationBarHidden:YES animated:NO];

    
    btnPostAttractionVideo.enabled = YES;
    [self setViewChanges];
   // [self checkPickedLocation];
    NSDate *now = [NSDate date];
    txtTime.text =[Utils timeFormatHHmm:now];
    txtEndTime.text =[Utils timeFormatHHmm:now];

    txtDate.text = [Utils dateFormatddMMyyy:now];
    tblList.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
//    btnLocation.contentEdgeInsets = UIEdgeInsetsMake(0.0f, 30.0f, 0.0f, 30.0f);
    myLocationView.layer.borderColor = [UIColor whiteColor].CGColor;
    myLocationView.layer.borderWidth = 2.0f;
    
    [self setLocalizedStrings];
    
}

-(void)getAllAttractionsNew {
    if (!allAttractions) {
        allAttractions = [[NSMutableArray alloc] init];
        allAttractionsOrg = [[NSMutableArray alloc] init];
        
    }
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Getting Attractions..."];
    
    [[PlayTripManager Instance] getAttractionListWithBlockNew:^(id result, NSString *error) {
        [SVProgressHUD dismiss];
        if (!error) {
            NSError* error;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                 options:kNilOptions
                                                                   error:&error];
            
            NSArray* attractionsDicts = [json objectForKey:@"data"];
            
            [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
                [Attractions MR_truncateAllInContext:localContext];

                NSMutableArray * array2 = [[PlayTripManager Instance] createTrickyJsonForAttractionLocationValues:[attractionsDicts mutableCopy]];
                NSArray *appts = [FEMDeserializer collectionFromRepresentation:array2 mapping:[Attractions defaultMapping] context:localContext];
                
            }];
            
            allAttractions = [[NSMutableArray alloc] init];
            allAttractionsOrg = [[NSMutableArray alloc] init];

            for (NSDictionary *attractionDict in attractionsDicts) {
                NSError* modelError;
                
                AttractionModel *newModel = [[AttractionModel alloc] initWithDictionary:attractionDict error:&modelError];
                if (modelError) {
                    NSLog(@"ERROR GOTTEN = %@" , modelError.description);
                }
                else{
                    if (![allAttractions containsObject:newModel]) {
                        [allAttractions addObject:newModel];
                        [allAttractionsOrg addObject:newModel];
                    }
                }
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [self checkPickedLocation];
            });
        }
        else{
            
        }
    }];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [Utils addBottomBorderForTextField:txtAttractionName];
    [Utils addBottomBorderForTextField:txtDescription];
    [Utils addBottomBorderForTextField:txtTime];
    [Utils addBottomBorderForTextField:txtDate];
    [Utils addBottomBorderForTextField:txtEndTime];

    //[self setScrollViewContentSize];
    [self getAllAttractionsNew];
    BOOL isAddedAttraction = [Utils getBOOLForKey:@"AddedAttraction"];
    if (isAddedAttraction) {
        [Utils setBOOL:NO Key:@"AddedAttraction"];
        //        [self saveClicked:self];
    }
    
    
    if (!currentPlayingImageView) {
        currentPlayingImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, videoPreviewView.frame.size.width, videoPreviewView.frame.size.height)];
        [currentPlayingImageView setContentMode:UIViewContentModeScaleAspectFit];
        
        
        
        self.videoController = [[MPMoviePlayerController alloc] init];
        
        

        
        if (waterMarkedUrl) {
            [self.videoController setContentURL:waterMarkedUrl];
        }
        else{
            [self.videoController setContentURL:vidUrl];
        }
        

        
        [self.videoController.view setFrame:CGRectMake(0, 0, videoPreviewView.frame.size.width, videoPreviewView.frame.size.height)];
        [self.videoController setControlStyle:MPMovieControlStyleFullscreen];
        [videoPreviewView addSubview:self.videoController.view];
        
        [videoPreviewView addSubview:currentPlayingImageView];
        
    }

    AVURLAsset* asset ;
    
    if (waterMarkedUrl) {
        asset = [AVURLAsset URLAssetWithURL:waterMarkedUrl options:nil];
    }
    else{
        asset = [AVURLAsset URLAssetWithURL:vidUrl options:nil];
    }
    
    AVAssetImageGenerator* generator = [AVAssetImageGenerator assetImageGeneratorWithAsset:asset];
    generator.appliesPreferredTrackTransform = YES;
    UIImage* image = [UIImage imageWithCGImage:[generator copyCGImageAtTime:CMTimeMake(0, 1) actualTime:nil error:nil]];
    currentPlayingImageView.image = image;
    videoFirstFrameImage = image;
    
    [videoPreviewView bringSubviewToFront:btnPlay];
    
    UIScrollView *topScrollView = (UIScrollView *)self.view;
    topScrollView.contentSize = CGSizeMake(topScrollView.frame.size.width, 600);
}

-(void)setLocalizedStrings {

    [lblAttraction setText:LOCALIZATION(@"attractions")];
    [lblDescription setText:LOCALIZATION(@"comment")];
    [lblDate setText:LOCALIZATION(@"date")];
    [lblTime setText:LOCALIZATION(@"start_time")];
    [lblEndtime setText:LOCALIZATION(@"end_time")];

    [btnPostAttractionVideo setTitle:LOCALIZATION(@"posy_attraction_video") forState:UIControlStateNormal];

    
    if ([[[Localisator sharedInstance] currentLanguage] isEqualToString:@"en"]) {
        //bottomTextFieldLineConstraint.constant = 0;
        //bottomTextFieldLineConstraint2.constant = 0;
    }
    else{
        //bottomTextFieldLineConstraint.constant = 40;
        //bottomTextFieldLineConstraint2.constant = 40;
    }
    /*
    lblAttraction.text = [NSString stringWithFormat:NSLocalizedString(@"attractions", nil)];
    lblDescription.text = [NSString stringWithFormat:NSLocalizedString(@"description", nil)];
    lblDate.text = [NSString stringWithFormat:NSLocalizedString(@"date", nil)];
    lblTime.text = [NSString stringWithFormat:NSLocalizedString(@"time", nil)];
    [btnPostAttractionVideo setTitle:NSLocalizedString(@"posy_attraction_video", nil) forState:UIControlStateNormal];
     */
}

-(void)allocArrays {
    arrAttr = [NSMutableArray new];
}

-(void)addDoneToolBar {
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelToolbar)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithToolbar)],
                           nil];
    [numberToolbar sizeToFit];
    txtAttractionName.inputAccessoryView = numberToolbar;
}

-(void)cancelToolbar {
    [txtAttractionName resignFirstResponder];
}

-(void)doneWithToolbar{
    [txtAttractionName resignFirstResponder];
}

-(void)initLocManager {
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager requestAlwaysAuthorization];
    [locationManager startUpdatingLocation];
}

-(void)setViewChanges {
    UIColor *color = [ColorConstants appYellowColor];
    txtAttractionName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:LOCALIZATION(@"type_your_location") attributes:@{NSForegroundColorAttributeName: color}];
    //txtAttractionName.textColor = color;
    //lblAttractionName.textColor = color;
    lblAttractionName.hidden = YES;
    btnDeleteAttr.hidden = YES;
    btnPostAttractionVideo.layer.cornerRadius = 5.0;
    
    viewAttributes.hidden = YES;
}

//-(void)setScrollViewContentSize {
//    attrBoxView.frame = CGRectMake(0, videoPreviewView.frame.size.height + 30, attrBoxView.frame.size.width, attrBoxView.frame.size.height);
//    bottomView.frame = CGRectMake(0,attrBoxView.frame.origin.y+attrBoxView.frame.size.height, bottomView.frame.size.width, bottomView.frame.size.height);
//    CGFloat totalheight = bottomView.frame.origin.y+bottomView.frame.size.height+30;
//    scrollViewMain.contentSize = CGSizeMake(scrollViewMain.frame.size.width, totalheight);
//}

-(void)setTableHidden:(BOOL)isHidden {
    if (isHidden) {
        tblList.hidden = YES;
        progressLine.hidden = NO;
        lblProgressTime.hidden = NO;
        outsideBtn.hidden = YES;
    } else {
        tblList.hidden = NO;
        progressLine.hidden = YES;
        lblProgressTime.hidden = YES;
        outsideBtn.hidden = NO;
    }
}

-(void)setPickerForTime {
    datePicker = [[UIDatePicker alloc]init];
    datePicker.datePickerMode = UIDatePickerModeTime;
    [datePicker addTarget:self action:@selector(updateTime:) forControlEvents:UIControlEventValueChanged];
    endDatePicker.maximumDate = [NSDate date];
    [txtTime setInputView:datePicker];
    
    endDatePicker = [[UIDatePicker alloc]init];
    endDatePicker.datePickerMode = UIDatePickerModeTime;
    endDatePicker.minimumDate = [NSDate date];
    [endDatePicker addTarget:self action:@selector(updateEndTime:) forControlEvents:UIControlEventValueChanged];
    [txtEndTime setInputView:endDatePicker];
}

-(void)updateTime:(id)sender {
    UIDatePicker *picker = (UIDatePicker*)txtTime.inputView;
    //NSString * timeStr = [Utils getHourAndMinutesFromDate:[NSString stringWithFormat:@"%@",picker.date]];
    txtTime.text =[Utils timeFormatHHmm:picker.date];

    //txtTime.text = timeStr;
}


-(void)updateEndTime:(id)sender {
    UIDatePicker *picker = (UIDatePicker*)txtEndTime.inputView;
    txtEndTime.text =[Utils timeFormatHHmm:picker.date];
}



-(void)setSelectedAttractionWithSelectedAttractionModel {
    selectedAttraction = nil;
    Attractions * newAttraction = [Attractions getByEntityId:selectedAttractionModel._id];
    
    
    
    
    /*
    newAttraction.entity_id = selectedAttractionModel._id;
    newAttraction.address = selectedAttractionModel.address;
    newAttraction.detail = selectedAttractionModel.description;
    newAttraction.loc_lat = @([selectedAttractionModel.loc_lat floatValue]);
    newAttraction.loc_long = @([selectedAttractionModel.loc_long floatValue]);
    newAttraction.name = selectedAttractionModel.name;
    newAttraction.phone_no = selectedAttractionModel.phone_no;
    newAttraction.web_url = selectedAttractionModel.web_url;
     */
    selectedAttraction = newAttraction;
    
    
}

-(void)checkPickedLocation {
    pickedLocation = [[NSUserDefaults standardUserDefaults] objectForKey:LocationId];
    if(pickedLocation){
        lblAttractionName.hidden = NO;
        btnDeleteAttr.hidden = NO;
        txtAttractionName.text = @"";
        [txtAttractionName resignFirstResponder];
        
        for (AttractionModel *attraction in allAttractions) {
            if ([attraction._id isEqualToString:pickedLocation]) {
                selectedAttractionModel = attraction;
                lblAttractionName.text = attraction.name;
                [self setSelectedAttractionWithSelectedAttractionModel];
                break;
            }
        }
        
        /*
        Attractions * attr = [Attractions getByEntityId:pickedLocation];
        selectedAttraction = attr;
        lblAttractionName.text = attr.name;
         */
        
        
        btnLocation.hidden=YES;
    }
}

- (IBAction)playClicked:(id)sender {
    if (waterMarkedUrl) {
        //[self playVideoWithURL:waterMarkedUrl];
        [self playVideoInPopUpViewWithURL:waterMarkedUrl];
    } else {
        [self playVideoWithURL:vidUrl];
        //[self playVideoInPopUpViewWithURL:vidUrl];

    }
}

-(void)playVideoWithURL:(NSURL *)url {
    
    
    
    
    //    self.videoController.view.transform = CGAffineTransformMakeRotation(M_PI/2);
    [self.videoController play];
    
    currentPlayingImageView.hidden = YES;
    btnPlay.hidden = YES;
    
    
    
    double delayInSeconds = 12.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        if(self.videoController.playbackState != MPMoviePlaybackStatePlaying) {
            [videoPreviewView bringSubviewToFront:btnPlay];
            [videoPreviewView bringSubviewToFront:tblList];
            [videoPreviewView bringSubviewToFront:btnPlay];

            btnPlay.hidden = NO;

        }
    });
    
    countDownTime = 10.0;
    [progressLine setProgress:0 animated:YES];
    timerCountDown = [NSTimer scheduledTimerWithTimeInterval:1  target:self selector:@selector(updateProgressLine) userInfo:nil repeats:YES];
    [videoPreviewView bringSubviewToFront:tblList];
}

- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *playerItem = [notification object];
    [playerItem seekToTime:kCMTimeZero];
    [videoLayer removeFromSuperlayer];
}

-(void)updateProgressLine {
    countDownTime--;
    if (countDownTime <= 0) {
        [timerCountDown invalidate];
    }
    float pro = (10 - countDownTime)/10.0;
//    lblProgressTime.text = [NSString stringWithFormat:@"%f s", pro];
    [progressLine setProgress:pro animated:YES];
}

- (IBAction)addAttractionClicked:(id)sender {
//    if (![[AppDelegate appDelegate] isReachable]) {
//        [Utils showAlertWithMessage:@"Please connect with internet first"];
//    }else {
        VideoMapViewController *controler = [VideoMapViewController initViewController];
        controler.allAttractions = allAttractionsOrg;
    controler.videoFirstFrameImage = videoFirstFrameImage;
        [self.navigationController pushViewController:controler animated:YES];
//    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    if (!currentLocationName) {
        currentLocation = newLocation;
        [Utils getAddressFromLocation:newLocation complationBlock:^(NSString *name) {
            if (name) {
                currentLocationName = name;
            }
        }];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if([keyPath isEqualToString:@"myLocation"]) {
        CLLocation *location = [object myLocation];
        target = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        if (isallocated == NO) {
            [self getAttractions];
            isallocated = YES;
        }
    }
}

-(void)getAttractions {
    arrAttr = [NSMutableArray new];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Loading..."];
    [[PlayTripManager Instance] getAttractionsListWithDistance:@"50" WithLattitude:target.latitude WithLongitude:target.longitude WithBlock:^(id result, NSString *error) {
        if (error) {
            [SVProgressHUD dismiss];
        } else {
            [SVProgressHUD dismiss];
            arrAttr = [Attractions getAll];
            [tblList reloadData];
        }
    }];
}

-(void)getFilteredAttractionForString:(NSString *)str {
    [self setTableHidden:NO];

    allAttractions = [[NSMutableArray alloc] init];
    for (AttractionModel *attraction in allAttractionsOrg) {
        if ([[attraction.name lowercaseString] containsString:[str lowercaseString]]) {
            [allAttractions addObject:attraction];
        }
    }
    [tblList reloadData];
}

#pragma WaterMark
-(void)MixVideoWithTextWithUrl:(NSURL *)vidUrl WithText:(NSString *)watermarkText {
    AVURLAsset* videoAsset = [[AVURLAsset alloc]initWithURL:self->vidUrl options:nil];
    AVMutableComposition* mixComposition = [AVMutableComposition composition];
    AVMutableCompositionTrack *compositionVideoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    if ([videoAsset tracksWithMediaType:AVMediaTypeVideo].count <= 0) {
        self.view.userInteractionEnabled = true;
        self.navigationController.view.userInteractionEnabled = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        [Utils showAlertWithMessage:@"Video not saved properly. Try again"];
        return;
    }
    
    AVAssetTrack *clipVideoTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    AVMutableCompositionTrack *compositionAudioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    
    if ([videoAsset tracksWithMediaType:AVMediaTypeAudio].count <= 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        [Utils showAlertWithMessage:@"Video not saved properly. Try again"];
        return;
    }
     
    
    AVAssetTrack *clipAudioTrack = [[videoAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
    CMTime a = videoAsset.duration;
    
    [compositionVideoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, a) ofTrack:clipVideoTrack atTime:kCMTimeZero error:nil];
    [compositionAudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, a) ofTrack:clipAudioTrack atTime:kCMTimeZero error:nil];
    
    CGSize sizeOfVideo=[[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] naturalSize];
    
    CATextLayer *textOfvideo=[[CATextLayer alloc] init];
    
    int sizeFont = (((int)sizeOfVideo.height/12) - 10)*1.2 ;

    
    UIFont *attributeFont =  [UIFont fontWithName:fontLabel.font.fontName size:sizeFont];
    
    
    if ([[[Localisator sharedInstance] currentLanguage] isEqualToString:@"en"]) {
        attributeFont =  [UIFont fontWithName:fontLabel.font.fontName size:sizeFont];
    }
    else{
        attributeFont =  [UIFont fontWithName:hkfontLabel.font.fontName size:sizeFont];
    }

    
    NSDictionary* attributes = @{ NSFontAttributeName : attributeFont,
                                  (NSString*)kCTForegroundColorAttributeName: (id)[UIColor whiteColor].CGColor,
                                  (NSString*)kCTStrokeWidthAttributeName: @(-4.0),
                                  (NSString*)kCTStrokeColorAttributeName: (id)[UIColor blackColor].CGColor };

    
    if (!watermarkText) {
        watermarkText = @"";
    }
    
    textOfvideo.string = [[NSAttributedString alloc] initWithString:watermarkText attributes:attributes];

    
    
    
    
    
    //textOfvideo.string = watermarkText;
    //[textOfvideo setFrame:CGRectMake(0, 10, sizeOfVideo.width, sizeOfVideo.height/12)];
    
    
    
    
    //textOfvideo.font = (__bridge CFTypeRef _Nullable)([UIFont systemFontOfSize:sizeFont]);
    //[textOfvideo setFontSize:sizeFont];

   CGSize textBox = [watermarkText sizeWithFont:[UIFont fontWithName:@"Microsoft New Tai Lue" size:sizeFont]];

    [textOfvideo setFrame:CGRectMake((sizeOfVideo.width / 2) - ((textBox.width + 40) / 2), 10, textBox.width+ 30 , sizeOfVideo.height/9)];
    
    
    
    [textOfvideo setAlignmentMode:kCAAlignmentCenter];
    //[textOfvideo setForegroundColor:[[UIColor whiteColor] CGColor]];
    //[textOfvideo setBackgroundColor:[[UIColor darkGrayColor] CGColor]];
    [textOfvideo setBackgroundColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.0].CGColor];
    
    //*************** LABEL Playtrip ****************
    
    CATextLayer *playtripTextOfvideo=[[CATextLayer alloc] init];
    
    NSDictionary* attributesPlaytrip = @{ NSFontAttributeName : [UIFont fontWithName:@"Microsoft New Tai Lue" size:sizeFont / 1.3],
                                  (NSString*)kCTForegroundColorAttributeName: (id)[[UIColor whiteColor] colorWithAlphaComponent:0.7].CGColor,
                                  (NSString*)kCTStrokeWidthAttributeName: @(-2.0),
                                  (NSString*)kCTStrokeColorAttributeName: (id)[[UIColor blackColor] colorWithAlphaComponent:0.7].CGColor };
    
    playtripTextOfvideo.string = [[NSAttributedString alloc] initWithString:@"PlayTrip" attributes:attributesPlaytrip];

    
    
   // playtripTextOfvideo.string = @"PlayTrip";
    playtripTextOfvideo.font = (__bridge CFTypeRef _Nullable)([UIFont fontWithName:@"Microsoft New Tai Lue" size:sizeFont]);
    [playtripTextOfvideo setFontSize:sizeFont];
    
    
    //[playtripTextOfvideo setFrame:CGRectMake((sizeOfVideo.width - (sizeFont*4)), (sizeOfVideo.height - (sizeOfVideo.height / 7)), (sizeFont*4) , (sizeOfVideo.height/12) * 1.2)];
    
    [playtripTextOfvideo setFrame:CGRectMake((sizeOfVideo.width - ((sizeFont/ 1.3)*4)), (sizeOfVideo.height - (sizeOfVideo.height / 8)), ((sizeFont/ 1.3)*4) , (sizeOfVideo.height/12) / 0.9)];
    
    [playtripTextOfvideo setAlignmentMode:kCAAlignmentLeft];
    //[playtripTextOfvideo setForegroundColor:[[UIColor whiteColor] CGColor]];
    //[textOfvideo setBackgroundColor:[[UIColor darkGrayColor] CGColor]];
    [playtripTextOfvideo setBackgroundColor:[UIColor colorWithRed:0/255.0 green:0/255.0 blue:0/255.0 alpha:0.0].CGColor];

    

    CALayer *optionalLayer=[CALayer layer];
    [optionalLayer addSublayer:textOfvideo];
    [optionalLayer addSublayer:playtripTextOfvideo];
    optionalLayer.frame=CGRectMake(0, 10, sizeOfVideo.width, sizeOfVideo.height);
    [optionalLayer setMasksToBounds:YES];
    
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer1 = [CALayer layer];
    parentLayer.frame = CGRectMake(0, 0, sizeOfVideo.width, sizeOfVideo.height);
    videoLayer1.frame = CGRectMake(0, 0, sizeOfVideo.width, sizeOfVideo.height);
    [parentLayer addSublayer:videoLayer1];
    [parentLayer addSublayer:optionalLayer];
    
    AVMutableVideoComposition* videoComp = [AVMutableVideoComposition videoComposition];
    videoComp.renderSize = sizeOfVideo;
    videoComp.frameDuration = CMTimeMake(1, 30);
    videoComp.animationTool = [AVVideoCompositionCoreAnimationTool videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer1 inLayer:parentLayer];
    
    // instruction
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    
    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, [mixComposition duration]);
    AVAssetTrack *videoTrack = [[mixComposition tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    AVMutableVideoCompositionLayerInstruction* layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
    
    instruction.layerInstructions = [NSArray arrayWithObject:layerInstruction];
    videoComp.instructions = [NSArray arrayWithObject: instruction];
    
    /*
     If you want to have 1080p video, please, replace AVAssetExportPresetMediumQuality with AVAssetExportPresetHighestQuality. But then users video in 1080p can't be merged with your ad video!
     *///TODO
    AVAssetExportSession * _assetExport = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
    _assetExport.videoComposition = videoComp;
    
    NSString * timeStamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
    NSString* videoName = [NSString stringWithFormat:@"%@.mov", timeStamp];
    
    NSString *exportPath = [NSTemporaryDirectory() stringByAppendingPathComponent:videoName];
    NSURL *exportUrl = [NSURL fileURLWithPath:exportPath];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:exportPath]) {
        [[NSFileManager defaultManager] removeItemAtPath:exportPath error:nil];
    }
    
    _assetExport.outputFileType = AVFileTypeMPEG4;
    _assetExport.outputURL = exportUrl;
    _assetExport.shouldOptimizeForNetworkUse = YES;
    
    [_assetExport exportAsynchronouslyWithCompletionHandler:
     ^(void ) {
         dispatch_async(dispatch_get_main_queue(), ^{
             //[SVProgressHUD dismiss];
             AVURLAsset* avUrl = [[AVURLAsset alloc]initWithURL:_assetExport.outputURL options:nil];
             NSLog(@"OK %f %f", avUrl.naturalSize.width, _assetExport.asset.naturalSize.width);
             
             self->waterMarkedUrl = [avUrl URL];
             
             self.view.userInteractionEnabled = true;
             self.navigationController.view.userInteractionEnabled = YES;
             [self uploadVideo];

             //[self playVideoInPopUpViewWithURL:[avUrl URL]];
             //[SVProgressHUD dismiss];

             //TODO - When S3 is ready //[self uploadVideoOnS3];
         });
     }];
}


-(void)uploadVideoToS3 {
    
    
    
    NSLog(@"DATA HEERERE");
    NSString *fileName = [[[NSProcessInfo processInfo] globallyUniqueString] stringByAppendingString:@".mp4"];
    
    
    
    
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.bucket = @"playtrip.medias";
    uploadRequest.key = fileName;
    uploadRequest.body = self->waterMarkedUrl;
    //[uploadRequest setACL:AWSS3ObjectCannedACLPublicRead];
    uploadRequest.contentType = @"image/jpg";

    [SVProgressHUD showWithStatus:@"Please Wait..."];
    
    
    [[transferManager upload:uploadRequest] continueWithBlock:^id(AWSTask *task) {
        // Do something with the response
        if (task.error) {
            NSLog(@"ERROR GOTTEN = %@" , task.error.description);
            [SVProgressHUD dismiss];
            
        }
        else{
            NSLog(@"UPLOADED = https://playtrip.medias.s3.amazonaws.com/%@",fileName);
            
            NSString *videoURL = [NSString stringWithFormat:@"https://playtrip.medias.s3.amazonaws.com/%@",fileName];
            
           
    }
        return nil;
    }];
    
    
}

#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == txtDescription) {
        
        if(range.length + range.location > txtDescription.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [txtDescription.text length] + [string length] - range.length;
        return newLength <= 200;
    }
    outsideBtn.hidden = YES;
    [tblList setHidden:YES];
    progressLine.hidden = NO;
    searchTextGlobal =  [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSString * searchString = [NSString stringWithFormat:@"%@", searchTextGlobal];
    if(searchString.length > 0) {
        [self getFilteredAttractionForString:searchString];
    } else {
        allAttractions = [[NSMutableArray alloc] initWithArray:allAttractionsOrg];
        [tblList reloadData];
        [tblList setHidden:YES];
        progressLine.hidden = NO;
        outsideBtn.hidden = YES;
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    searchTextGlobal = @"";
}

#pragma TableView Delegate and DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //return arrAttr.count + 1;
    return allAttractions.count + 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
    }
    
    if (indexPath.row == 0) {
        cell.textLabel.text = @"Add Attraction Point";
//        cell.backgroundColor = [ColorConstants appYellowColor];
//        cell.textLabel.textColor = [ColorConstants appBrownColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    AttractionModel *attraction = [allAttractions objectAtIndex:indexPath.row -1];
    cell.textLabel.text = attraction.name;
    cell.textLabel.textColor = [UIColor blackColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    /*Attractions * attr = [arrAttr objectAtIndex:indexPath.row -1];
    cell.textLabel.text = attr.name;
    cell.textLabel.textColor = [UIColor blackColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    */
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        [self addAttractionClicked:self];
    } else {
        
        
        AttractionModel *attraction = [allAttractions objectAtIndex:indexPath.row -1];
        selectedAttractionModel = attraction;
        lblAttractionName.text = attraction.name;
        [self setSelectedAttractionWithSelectedAttractionModel];

        /*
        Attractions * attr = [arrAttr objectAtIndex:indexPath.row -1];
        selectedAttraction = attr;
        lblAttractionName.text = attr.name;
         */
        lblAttractionName.hidden = NO;
        btnDeleteAttr.hidden = NO;
        txtAttractionName.text = @"";
        btnLocation.hidden=YES;
        [txtAttractionName resignFirstResponder];
        
        
        
        CGSize maximumLabelSize = CGSizeMake(lblAttractionName.frame.size.width, FLT_MAX);
        CGSize expectedLabelSize = [attraction.name sizeWithFont:lblAttractionName.font constrainedToSize:maximumLabelSize lineBreakMode:lblAttractionName.lineBreakMode];
        
//        //adjust the label the the new height.
//        CGRect newFrame = lblAttractionName.frame;
//        newFrame.size.height = expectedLabelSize.height;
//        lblAttractionName.frame = newFrame;
        
        
        [lblAttractionName setPreferredMaxLayoutWidth:expectedLabelSize.height];

        
        
        [self.view layoutIfNeeded];
    }
    [self setTableHidden:YES];
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
//    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)closeClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)deleteAttractionClicked:(id)sender {
    selectedAttractionModel = nil;
    selectedAttraction = nil;
    lblAttractionName.text = @"";
    lblAttractionName.hidden = YES;
    btnDeleteAttr.hidden = YES;
    
    btnLocation.hidden = NO;
}

- (IBAction)outSideAction:(UIButton *)sender {
    [self setTableHidden:YES];
}

- (IBAction)postAttractionClicked:(id)sender {
    
    if (!selectedAttractionModel) {
        [Utils showAlertWithMessage:@"Please add Attraction"];
    } else if (txtDescription.text.length <= 0) {
        [Utils showAlertWithMessage:@"Please enter Comment"];
    } else if (txtTime.text.length <= 0) {
        [Utils showAlertWithMessage:@"Please select time"];
    } else {
        btnPostAttractionVideo.enabled = NO;
        [self setLocalData];
    }
}

-(void)setLocalData {
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"videosData"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [SVProgressHUD showWithStatus:@"Saving..."];
    
    NSString * timeStamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
    
    dTour = [DraftTour getDraft];
    NSInteger days =  [Utils getDayFromDate:dTour.from_date].integerValue;
    days = days+1;
    
    if (dTour.days.integerValue < days) {
        dTour.days = [NSNumber numberWithInteger:days];
        dTour.to_date = [Utils addDays:days toDate:dTour.from_date];
        [DraftTour saveEntity];
    }

    DraftAttraction * dAttr = [DraftAttraction getByEntityId:selectedAttraction.entity_id];
    if (!dAttr) {
        dAttr = [DraftAttraction newEntity];
        dAttr.entity_id = selectedAttraction.entity_id;
    }
    dAttr.address = selectedAttraction.address;
    dAttr.day = [NSNumber numberWithInteger:dTour.days.integerValue];
    dAttr.detail = selectedAttraction.detail;
    dAttr.loc_lat = selectedAttraction.loc_lat;
    dAttr.loc_lng = selectedAttraction.loc_long;
    dAttr.name = selectedAttraction.name;
    dAttr.phone_no = selectedAttraction.phone_no;
    dAttr.web_url = selectedAttraction.web_url;
    
    
    DraftAttrData * dAttrData = [DraftAttrData newEntity];
    dAttrData.desc = txtDescription.text;
    dAttrData.time = [NSString stringWithFormat:@"%@ - %@",txtTime.text ,txtEndTime.text];
    dAttrData.entity_id = timeStamp;
    
    NSLog(@"Draft Tour Count = %u",dTour.draftAttractions.count);
    
    dAttrData.sequence = @(dTour.draftAttractions.count);
    

    
    
    DraftInfo * dInfo = [DraftInfo newEntity];
    dInfo.name = selectedAttraction.name;
    dInfo.loc_long = selectedAttraction.loc_long;
    dInfo.loc_lat = selectedAttraction.loc_lat;
    dInfo.language = @"en";
    dInfo.phone_no = selectedAttraction.phone_no;
    dInfo.address = selectedAttraction.address;
    dInfo.detail = selectedAttraction.detail;
    dInfo.web_url = selectedAttraction.web_url;
    dInfo.entity_id = selectedAttraction.entity_id;
    
    DraftVideoId * dVideoId = [DraftVideoId newEntity];
    dVideoId.file_name = @"";
    dVideoId.thumbnail = @"";
    dVideoId.user = @"";
    dVideoId.entity_id = timeStamp;
    
    [DraftVideoId saveEntity];
    [DraftInfo saveEntity];
    [DraftAttrData saveEntity];
    [DraftAttraction saveEntity];
    [DraftTour saveEntity];
    
    [dAttrData setDraftInfo:dInfo];
    [dInfo setDraftAttrData:dAttrData];
    
    [dAttrData setDraftVideoId:dVideoId];
    [dVideoId setDraftAttrData:dAttrData];
    
    [dAttr addDraftAttrDataObject:dAttrData];
    [dAttrData setDraftAttractions:dAttr];
    
    NSSet * aSet = [[NSSet alloc] initWithObjects:dTour, nil];
    
    [dAttr setDraftTour:aSet];
    [dTour addDraftAttractionsObject:dAttr];
    
    selectedAttractionData = dAttrData;
    
    [DraftVideoId saveEntity];
    [DraftInfo saveEntity];
    [DraftAttrData saveEntity];
    [DraftAttraction saveEntity];
    [DraftTour saveEntity];
    [self MixVideoWithTextWithUrl:vidUrl WithText:lblAttractionName.text];
}






-(void)uploadVideo {
    //[SVProgressHUD showWithStatus:@"Uploading..."];
    NSLog(@"Entity Id = %@",selectedAttractionData.entity_id);
    [[PlayTripManager Instance] uploadVideoWithVideoUrl:waterMarkedUrl WithAttrId:selectedAttractionData.entity_id WithBlock:^(id result, NSString *error) {
        
        if (result) {
            //NSLog(@"%@", result);
            
            DraftAttrData * dAttrData = selectedAttractionData;
            dAttrData.draftVideoId.file_name = [result valueForKey:@"file_name"];
            dAttrData.draftVideoId.entity_id = [result valueForKey:@"id"];
            dAttrData.draftVideoId.thumbnail = [result valueForKey:@"thumbnail"];
            dAttrData.draftVideoId.user = [result valueForKey:@"user"];
            [DraftAttrData saveEntity];
            [SVProgressHUD dismiss];
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Video successfully saved and added to the attraction" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                //                [self.navigationController popToRootViewControllerAnimated:true];
                DraftTourViewController * controller = [DraftTourViewController initViewController];
                [self.navigationController pushViewController:controller animated:YES];
            }];
            [alert addAction:okayAction];
            [self.navigationController presentViewController:alert animated:true completion:nil];
            
        } else {
            btnPostAttractionVideo.enabled = YES;

            [SVProgressHUD dismiss];
            if (!error) {
                error = @"Upload failure. Please retry.";
            }
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:error preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            }];
            [alert addAction:okayAction];
            [self.navigationController presentViewController:alert animated:true completion:nil];
        }
    }];
}

-(void)playVideoInPopUpViewWithURL:(NSURL *)url {
    if (url) {
        self.videoController = [[MPMoviePlayerController alloc] init];
        [self.videoController setContentURL:url];
        [self.videoController.view setFrame:CGRectMake (0, 0, popView.frame.size.width, popView.frame.size.height)];
        self.videoController.controlStyle = MPMovieControlStyleEmbedded;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoPlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:self.videoController];
        [popView addSubview:self.videoController.view];
        [KGModalWrapper showWithContentView:popView];
        [self.videoController setControlStyle:MPMovieControlStyleFullscreen];
        [self.videoController play];
    }
}

- (void)videoPlayBackDidFinish:(NSNotification *)notification {
    [self.videoController stop];
    [self.videoController.view removeFromSuperview];
    self.videoController = nil;
    [KGModalWrapper hideView];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.videoController stop];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
@end


#import <UIKit/UIKit.h>
#import <HNKGooglePlacesAutocomplete/HNKGooglePlacesAutocomplete.h>
#import "CLPlacemark+HNKAdditions.h"
#import <CoreLocation/CoreLocation.h>
#import <AVKit/AVPlayerViewController.h>
#import <AVKit/AVError.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MediaPlayer/MediaPlayer.h>
#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>
#import "GTLRYouTube.h"
#import "AttractionData.h"
#import "Plan.h"
#import "DraftTour.h"
#import "DraftAttrData.h"

@interface VideoDetailsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, NSURLConnectionDelegate> {
    
    IBOutlet UIView *videotourPopView;
    IBOutlet UITableView *tblVideotour;
    IBOutlet UILabel *lblTopMain;
    IBOutlet UIButton *btnCancle;
    IBOutlet UIButton *btnOk;
    IBOutlet UIButton *btnAddAttraction;
    
    IBOutlet UIButton *btnPlay;
    IBOutlet UIProgressView *progressLine;
    
    IBOutlet UIButton *btnMyLocation;
    IBOutlet UIView *attrBoxView;
    IBOutlet UIView *videoView;
    
    IBOutlet UIButton *btnSave;
    IBOutlet UIView *videoTourView;
    IBOutlet UITextField *txtAttractions;
    IBOutlet UITextField *txtDescription;
    IBOutlet UITextField *txtTime;
    IBOutlet UITableView *tblViewAddedAttractions;
    IBOutlet UITableView *tblViewGoogle;
    
    IBOutlet UILabel *lblVideotour;
    
    NSMutableArray * addedAttractionArray;
    NSMutableArray * arrGoogleResponse;
    NSMutableArray * arrGoogleNames;
    NSMutableArray * arrCmsResponse;
    NSMutableArray * arrCmsNames;
    NSMutableArray * arrCmsNamesAll;
    NSMutableArray * arrPlan;
    NSMutableArray * arrPlanName;
    
    GTLRServiceTicket *_uploadFileTicket;
    
    DraftTour * dTour;

    int countDownTime;
    NSTimer * timerCountDown;    
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    NSString * currentLocationName;
    NSString * pickedLocation;
    NSMutableArray *attractionArray;
    
    IBOutlet UIView *endView;
    NSURL * vidUrl;
    NSURL * waterMarkedUrl;
    IBOutlet UIScrollView *scrollViewMain;
    
    IBOutlet UIButton *btnVideotour;
    NSTimer * timer;
//    Plan *selectedPlan;
    
    
    NSMutableArray *outerArray;

    
    DraftAttrData * selectedAttractionData;
    NSDate   *startDate ;
    
    BOOL *firstAdded;
    BOOL *isSelected;
    NSIndexPath *selectedIndex;
    
}

+(VideoDetailsViewController *)initViewControllerWithURL:(NSURL *)vidUrl;
- (IBAction)playClicked:(id)sender;

- (IBAction)addAttractionClicked:(id)sender;

@property (strong, nonatomic) MPMoviePlayerController *videoController;
- (IBAction)saveClicked:(id)sender;

- (IBAction)postClicked:(id)sender;
- (IBAction)selectVideoTourClicked:(id)sender;
- (IBAction)cancleClicked:(id)sender;

- (IBAction)okClicked:(id)sender;

- (IBAction)myLocationClicked:(id)sender;
@property (strong, nonatomic) HNKGooglePlacesAutocompleteQuery *searchQuery;
@property (strong, nonatomic) NSMutableArray *searchResults;
- (IBAction)backClicked:(id)sender;

@end


#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <CoreLocation/CoreLocation.h>
#import "Attractions.h"
#import "AttractionData.h"
#import "MenuButtonViewController.h"

@interface MapAttractionsViewController : MenuButtonViewController <GMSMapViewDelegate, CLLocationManagerDelegate> {
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLLocationCoordinate2D target;
    NSString * currentLocationName;
    
    BOOL isallocated;

    IBOutlet GMSMapView *mapView;

    IBOutlet UIButton *btnAddPlan;
    IBOutlet UILabel *lblDetails;
    IBOutlet UILabel *lblName;
    IBOutlet UIImageView *detailImage;
    IBOutlet UIView *detailView;
    
    NSString * selectedAttractionId;

    Attractions *selectedAttraction;

    NSMutableArray * attrArray;
}

- (IBAction)addPlanClicked:(id)sender;

+(MapAttractionsViewController *)initViewController;


@end

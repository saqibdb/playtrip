
#import <UIKit/UIKit.h>
#import "LoginDelegate.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MediaPlayer/MediaPlayer.h>
#import "SCRecorder.h"
#import "SCRecordSessionManager.h"
#import "SCTouchDetector.h"
#import "VideoCollCell.h"

#define kVideoPreset AVCaptureSessionPresetHigh

@interface VideoViewController : UIViewController <AVAudioRecorderDelegate, AVAudioPlayerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, LoginDelegate,SCRecorderDelegate, SCAssetExportSessionDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UIGestureRecognizerDelegate> {
    SCRecorder *_recorder;
    SCVideoConfiguration *scVideoConfig;
    SCRecordSession *_recordSession;
    NSURL *watermarkedUrl;
    NSURL * importedVideoUrl;
    NSMutableArray * videoClipPaths;
    int secondsRecordedVideo;
    CMTime currentTime;
    IBOutlet UICollectionView *collViewMain;
    IBOutlet UIView *popView;
    IBOutlet UIButton *btnRight;
}
@property (strong, nonatomic) SCAssetExportSession *exportSession;
//@property (nonatomic, strong) NSURL *selectedURL;

@property UITapGestureRecognizer *theTapRecognizer;

- (IBAction)rightClicked:(id)sender;

+(VideoViewController *)initViewController ;
@property (weak, nonatomic) IBOutlet UIProgressView *progress;
@property (weak, nonatomic) IBOutlet UIView *previewView;
@property (weak, nonatomic) IBOutlet UIView *recordView;
@property (weak, nonatomic) IBOutlet UILabel *timeRecordedLabel;
@property (weak, nonatomic) IBOutlet UIButton *btnFlipCamera;
@property (weak, nonatomic) IBOutlet UIButton *btnFlash;
@property (nonatomic, strong) NSURL *selectedURL;
@property (strong, nonatomic) SCPlayer *player;

@property (strong, nonatomic) MPMoviePlayerController *videoController;

- (IBAction)switchCameraMode:(id)sender;
- (IBAction)switchFlash:(id)sender;
- (IBAction)backClicked:(id)sender;

@end

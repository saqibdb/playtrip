
#import "VideoViewController.h"
#import "Account.h"
#import "AccountManager.h"
#import "AppDelegate.h"
#import "KGModalWrapper.h"
#import "PlayTripManager.h"
#import "AttachAudioViewController.h"
#import "SVProgressHUD.h"
#import "Utils.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "SCWatermarkOverlayView.h"
#import "CameraFocusSquare.h"
#import "Localisator.h"
#import "VideoPostViewController.h"

#if TARGET_IPHONE_SIMULATOR
NSString *hello = @"Hello, iPhone simulator!";
#elif TARGET_OS_IPHONE
NSString *hello = @"Hello, device!";
#else
NSString *hello = @"Hello, unknown target!";
#endif




@interface VideoViewController (){
    NSMutableArray *arrVideUrl;
    CameraFocusSquare *_focusSquare;
    
    NSMutableArray *videoDataArray;
    
    NSMutableArray *allAlreadyArray;
    CGSize videosize;
    CGFloat croppingPercent ;
    CGRect mainVieRect ;
}
@end

@implementation VideoViewController

+(VideoViewController *)initViewController {
    VideoViewController * controller = [[VideoViewController alloc] initWithNibName:@"VideoViewController" bundle:nil];
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerNibs];
    arrVideUrl = [[NSMutableArray alloc] init];
    videoDataArray = [[NSMutableArray alloc] init];
    videoClipPaths  = [[NSMutableArray alloc] init];
    allAlreadyArray  = [[NSMutableArray alloc] init];
    
    [self.recordView addGestureRecognizer:[[SCTouchDetector alloc] initWithTarget:self action:@selector(handleTouchDetected:)]];
    _recorder = [SCRecorder recorder];
    _recorder.captureSessionPreset = [SCRecorderTools bestCaptureSessionPresetCompatibleWithAllDevices];
    _recorder.maxRecordDuration = CMTimeMake(10, 1);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideKGView) name:@"HideKGView" object:nil];
    
    
    SCVideoConfiguration *video = _recorder.videoConfiguration;
    //video.sizeAsSquare = YES;
    _recorder.videoConfiguration.size = CGSizeMake(1080, 1080);
    
    _recorder.delegate = self;
    _recorder.autoSetVideoOrientation = NO;
    UIView *previewView = self.previewView;
    _recorder.previewView = previewView;
    
    _recorder.mirrorOnFrontCamera = YES;
    
    //_recorder.videoConfiguration.enabled = YES;
    //_recorder.videoConfiguration.size = CGSizeMake(1920, 1080);
    //_recorder.videoConfiguration.scalingMode = AVVideoScalingModeResizeAspectFill;
 

    _recorder.initializeSessionLazily = NO;
    btnRight.enabled = NO;
    currentTime = kCMTimeZero;
    
    //[_recorder lockFocus];
    
    //  [self importClicked:self];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self prepareSession];
    self.navigationController.navigationBarHidden = YES;
    self.btnFlipCamera.enabled = YES;
     btnRight.enabled = NO;
    
    UILongPressGestureRecognizer *longGR = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    longGR.minimumPressDuration = 0.2;
    [collViewMain addGestureRecognizer:longGR];
    
    /*
    UIPanGestureRecognizer *tapRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    [collViewMain addGestureRecognizer:tapRecognizer];
    */
    
    videoDataArray = [[NSMutableArray alloc] init];
    videoClipPaths  = [[NSMutableArray alloc] init];
    allAlreadyArray  = [[NSMutableArray alloc] init];
    
    //[[NSUserDefaults standardUserDefaults] removeObjectForKey:@"videosData"];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *vidData = [userDefaults objectForKey:@"videosData"];
    CMTime totalTime = kCMTimeZero;
    
    for (NSData *data in vidData) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss'Z'";
        NSDate *date = [NSDate date];
        NSString *tempDir   = [NSTemporaryDirectory() stringByAppendingString:[formatter stringFromDate:date]];
        NSString *d = [NSString stringWithFormat:@"%@%lld.mov", tempDir, arc4random() % 5000000000];
        NSURL *outputURL = [NSURL fileURLWithPath:d];
        
        [data writeToURL:outputURL atomically:YES ];
        if (outputURL != nil) {
            [videoClipPaths addObject:outputURL];
            [allAlreadyArray addObject:outputURL];
        }
        
        
        AVAsset *asset = [AVAsset assetWithURL:outputURL];
        totalTime = CMTimeAdd(totalTime, asset.duration);
        // NSLog(@"%@",totalTime);
        currentTime = totalTime;
        
        
        
    }
    [self updateTimeRecordedLabel];
    [collViewMain reloadData];
    
}

-(void)hideKGView {
    [self.videoController stop];
    [self.videoController.view removeFromSuperview];
    self.videoController = nil;
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [_recorder previewViewFrameChanged];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    importedVideoUrl = [[NSURL alloc] init];
    [_recorder startRunning];
    
    
    //_theTapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget: self action: @selector(handleTap:)];
    //_theTapRecognizer.delegate = self;
    //[_previewView addGestureRecognizer: _theTapRecognizer];
    
}


-(void)checkSavedSegments {
    NSMutableArray * localArray = [Utils getArrayForKey:VidSegArray];
    if (localArray.count > 0) {
        NSMutableArray * localArray1 = [NSMutableArray new];
        
        for (NSData * aData in localArray) {
            NSString* newStr = [NSKeyedUnarchiver unarchiveObjectWithData:aData];
            [localArray1 addObject:newStr];
            
            NSString *urlStr =[NSString stringWithFormat:@"%@", newStr];
            NSURL *aUrl = [NSURL URLWithString:urlStr];
            
            NSError * error = nil;
            
            BOOL success = [[NSFileManager defaultManager] createDirectoryAtPath: [aUrl path] withIntermediateDirectories:YES attributes:nil error:&error];
            
            if (!success) {
                NSLog(@"Error = %@", error);
            } else {
                NSLog(@"Success");
                NSError *error = nil;
                [aData writeToFile:[aUrl path] options:NSDataWritingAtomic error:&error];
                NSLog(@"Write returned error: %@", [error localizedDescription]);
            }
            
            SCRecordSessionSegment * segment = [SCRecordSessionSegment segmentWithURL:aUrl info:nil];
            [_recorder.session addSegment:segment];
            [_recordSession addSegment:segment];
            
            [videoClipPaths addObject:segment.url];
            [Utils setArray:[videoClipPaths mutableCopy] Key:VidSegArray];
            
            [collViewMain performBatchUpdates:^{
                [collViewMain reloadSections:[NSIndexSet indexSetWithIndex:0]];
            } completion:^(BOOL finished) {
                if((int)CMTimeGetSeconds(currentTime)>10){
                    [_recorder stopRunning];
                }
            }];
        }
    }
    //    [Utils removeKey:VidSegArray];
}

- (void)handleTap:(UITapGestureRecognizer *)tapRecognizer {
    CGPoint touchPoint = [tapRecognizer locationInView: _previewView];
    NSLog(@"%f - %f", touchPoint.x, touchPoint.y);
    [_recorder autoFocusAtPoint:touchPoint];
    
    if (!_focusSquare) {
        _focusSquare = [[CameraFocusSquare alloc]initWithTouchPoint:touchPoint];
        [_previewView addSubview:_focusSquare];
        //        [_focusSquare setNeedsDisplay];
    }
    else {
        [_focusSquare updatePoint:touchPoint];
    }
    [_focusSquare animateFocusingAction];
}

-(void)registerNibs {
    // Arun :: registering nib files
    [collViewMain registerClass:[VideoCollCell class] forCellWithReuseIdentifier:@"VideoCollCell"];
    [collViewMain registerNib:[UINib nibWithNibName:@"VideoCollCell" bundle:nil] forCellWithReuseIdentifier:@"VideoCollCell"];
}

- (IBAction)importClicked:(id)sender {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeMovie,      nil];
    [self presentViewController:imagePicker animated:true completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    if ([type isEqualToString:(NSString *)kUTTypeVideo] ||  [type isEqualToString:(NSString *)kUTTypeMovie]) {
        NSURL *urlvideo = [info objectForKey:UIImagePickerControllerMediaURL];
        importedVideoUrl = urlvideo;
        [self dismissViewControllerAnimated:true completion:nil];
        dispatch_async(dispatch_get_main_queue(), ^{
            //[self checkVideoWithUrl:importedVideoUrl];
            
            
            [self cropSelectedVideo:importedVideoUrl];
            btnRight.enabled = YES;


        });
    } else {
        [Utils showAlertWithMessage:@"Select a Video"];
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:true completion:nil];
}

-(void)checkVideoWithUrl:(NSURL *)importedUrl {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    
    [SVProgressHUD showWithStatus:@"Checking Video"];
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:importedUrl options:[NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithBool:YES],                   AVURLAssetPreferPreciseDurationAndTimingKey, nil]];
    
    NSTimeInterval durationInSeconds = 0.0;
    if (asset) {
        durationInSeconds = CMTimeGetSeconds(asset.duration);
    }
    if (durationInSeconds > 1 && durationInSeconds < 11) { // 9(min duration)
        [self videoFixOrientationForUrl:importedVideoUrl];
    } else {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Video duration must be 10 seconds" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction * retryAction = [UIAlertAction actionWithTitle:@"Re-Try" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                importedVideoUrl = [NSURL new];
                [self importClicked:self];
            }];
            UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:nil];
            [alert addAction:retryAction];
            [alert addAction:cancelAction];
            [self.navigationController presentViewController:alert animated:true completion:nil];
        });
    }
}

-(void)switchCameraMode:(id)sender{
    [_recorder switchCaptureDevices];
}

-(void)scWatermark {
    
    SCVideoPlayerView *playerView = [[SCVideoPlayerView alloc] initWithPlayer:_player];
    playerView.playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    // playerView.frame = self.view.frame;
    [_player setItemByAsset:_recordSession.assetRepresentingSegments];
    
    [_player pause];
    [SVProgressHUD showWithStatus:@"Please Wait..."];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    
    [_player pause];
    
    
    SCAssetExportSession *exportSession = [[SCAssetExportSession alloc] initWithAsset:_recordSession.assetRepresentingSegments];
    exportSession.videoConfiguration.preset = SCPresetHighestQuality;
    exportSession.audioConfiguration.preset = SCPresetHighestQuality;
    exportSession.videoConfiguration.maxFrameRate = 35;
    exportSession.outputUrl = _recordSession.outputUrl;
    exportSession.outputFileType = AVFileTypeMPEG4;
    exportSession.delegate = self;
    exportSession.contextType = SCContextTypeAuto;
    self.exportSession = exportSession;
    
    SCWatermarkOverlayView *overlay = [SCWatermarkOverlayView new];
    overlay.date = _recordSession.date;
    exportSession.videoConfiguration.overlay = overlay;
    
    __weak typeof(self) wSelf = self;
    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        __strong typeof(self) strongSelf = wSelf;
        
        if (!exportSession.cancelled) {
            CFTimeInterval time = CACurrentMediaTime();
            NSLog(@"Completed compression in %fs", CACurrentMediaTime() - time);
        }
        
        if (strongSelf != nil) {
            [strongSelf.player play];
            strongSelf.exportSession = nil;
            strongSelf.navigationItem.rightBarButtonItem.enabled = YES;
        }
        
        NSError *error = exportSession.error;
        if (exportSession.cancelled) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
        } else if (error == nil) {
            AVURLAsset* avUrl = [[AVURLAsset alloc]initWithURL:exportSession.outputUrl options:nil];
            watermarkedUrl = [avUrl URL];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
            self.view.userInteractionEnabled = true;
            self.navigationController.view.userInteractionEnabled = YES;
            [self gotoNextViewWithUrl:watermarkedUrl];
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
            });
            if (!exportSession.cancelled) {
                [[[UIAlertView alloc] initWithTitle:@"Failed to save" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            }
        }
    }];
}

- (IBAction)switchFlash:(id)sender {
    NSString *flashModeString = nil;
    if ([_recorder.captureSessionPreset isEqualToString:AVCaptureSessionPresetPhoto]) {
        switch (_recorder.flashMode) {
            case SCFlashModeAuto:
                flashModeString = @"Flash : Off";
                _recorder.flashMode = SCFlashModeOff;
                break;
            case SCFlashModeOff:
                flashModeString = @"Flash : On";
                _recorder.flashMode = SCFlashModeOn;
                break;
            case SCFlashModeOn:
                flashModeString = @"Flash : Light";
                _recorder.flashMode = SCFlashModeLight;
                break;
            case SCFlashModeLight:
                flashModeString = @"Flash : Auto";
                _recorder.flashMode = SCFlashModeAuto;
                break;
            default:
                break;
        }
    } else {
        switch (_recorder.flashMode) {
            case SCFlashModeOff:
                flashModeString = @"Flash : On";
                _recorder.flashMode = SCFlashModeLight;
                break;
            case SCFlashModeLight:
                flashModeString = @"Flash : Off";
                _recorder.flashMode = SCFlashModeOff;
                break;
            default:
                break;
        }
    }
}

- (void)recorder:(SCRecorder *)recorder didCompleteSession:(SCRecordSession *)recordSession {
    [self saveAndShowSession:recordSession];
}

- (void)recorder:(SCRecorder *)recorder didInitializeAudioInSession:(SCRecordSession *)recordSession error:(NSError *)error {
}

- (void)recorder:(SCRecorder *)recorder didInitializeVideoInSession:(SCRecordSession *)recordSession error:(NSError *)error {
}

- (void)recorder:(SCRecorder *)recorder didBeginSegmentInSession:(SCRecordSession *)recordSession error:(NSError *)error {
}

- (void)recorder:(SCRecorder *)recorder didCompleteSegment:(SCRecordSessionSegment *)segment inSession:(SCRecordSession *)recordSession error:(NSError *)error{
    
    CMTime timeValue = kCMTimeZero;
    AVAsset * ass = [AVAsset assetWithURL:segment.url];
    timeValue = ass.duration;
    int pro = (int)CMTimeGetSeconds(timeValue);
    self.btnFlipCamera.enabled = YES;
    if (pro < 1) {
        
        [recorder.session removeSegment:segment];
        return;
    }
    
    if(segment.url){
        
        //TODO
        [self cropSelectedVideo:segment.url];
        
        
        //.....................................
        
        
        if(secondsRecordedVideo>=9){
            btnRight.enabled = YES;
        }else {
            btnRight.enabled = NO;
        }
    }
}

- (void) handleStopButton {
    [_recorder pause:^{
        [self saveAndShowSession:_recorder.session];
    }];
}

-(void)saveAndShowSession:(SCRecordSession *)recordSession {
    [[SCRecordSessionManager sharedInstance] saveRecordSession:recordSession];
    _recordSession = recordSession;
    _player = [SCPlayer player];
    _player.loopEnabled = YES;
    btnRight.enabled = YES;
    
    [self scWatermark];
    return;
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Finished recording video?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDestructive handler:nil];
    [alert addAction:yesAction];
    [alert addAction:noAction];
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}

-(void)deleteVideo:(id)sender{
    UIAlertController * alert  = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Are you sure you want to delete this video" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        NSInteger tagA = [sender tag]-1;
        if (videoClipPaths.count >= tagA) {
            
            id obj  = videoClipPaths[tagA];
            //[_recorder.session removeSegmentAtIndex:tagA deleteFile:YES];
            [allAlreadyArray removeObject:obj];
           
            [videoClipPaths removeObjectAtIndex:tagA];
            videoDataArray = [[NSMutableArray alloc]init];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"videosData"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            videoDataArray = [[NSMutableArray alloc ]init];
            
            
            for (NSURL *url in videoClipPaths) {
                
                //saving video data to user default
                NSData *videoData = [NSData dataWithContentsOfURL:url];
                [videoDataArray addObject:videoData];
            }
            
            [_recorder startRunning];
            [collViewMain performBatchUpdates:^{
                [collViewMain reloadSections:[NSIndexSet indexSetWithIndex:0]];
            } completion:nil];
            btnRight.enabled = NO;
            [self updateTimeRecordedLabel];
        }
    }];
    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:yesAction];
    [alert addAction:noAction];
    [self.navigationController presentViewController:alert animated:true completion:nil];
}

-(void)playSegment:(id)sender{
    NSInteger tagA = [sender tag]-1;
    if (videoClipPaths.count >= tagA) {
        NSURL * aUrl = [videoClipPaths objectAtIndex:tagA];
        [self playVideoInPopUpViewWithURL:aUrl];
    }
}

#pragma Collection View
#pragma mark -<UICollectionViewDelegate,UICollectionViewDataSource>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return videoClipPaths.count;
}

- (void)collectionView:(UICollectionView *)collectionView moveItemAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{
    [self resetUserInteractionAfterSecond];
    
    
    
    
    NSURL *selectedURL = videoClipPaths[sourceIndexPath.row];
    NSURL *finalURL = videoClipPaths[destinationIndexPath.row];
    
    
    
    [videoClipPaths removeObject:selectedURL];

    [videoClipPaths insertObject:selectedURL atIndex:destinationIndexPath.row];
//    [videoClipPaths removeObject:selectedURL];
    
    
    allAlreadyArray = [[NSMutableArray alloc] initWithArray:videoClipPaths];
    
    
    //[videoClipPaths exchangeObjectAtIndex:sourceIndexPath.row withObjectAtIndex:destinationIndexPath.row];
    
    
    
    
    
    //[allAlreadyArray exchangeObjectAtIndex:sourceIndexPath.row withObjectAtIndex:destinationIndexPath.row];
    
    
    
    
    
    
    
    
    // [_recorder.session replaceSegmentAtIndex:sourceIndexPath.row WithIndex:destinationIndexPath.row];
    [Utils setArray:[videoClipPaths mutableCopy] Key:VidSegArray];
    [collViewMain reloadData];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger tagA = indexPath.row;
    if (videoClipPaths.count >= tagA) {
        NSURL * aUrl = [videoClipPaths objectAtIndex:tagA];
        [self playVideoInPopUpViewWithURL:aUrl];
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    VideoCollCell *cell = (VideoCollCell*)[collViewMain dequeueReusableCellWithReuseIdentifier:@"VideoCollCell" forIndexPath:indexPath];
    NSURL * url1 = [videoClipPaths objectAtIndex:indexPath.row];
    UIImage * img1 = [Utils generateThumbImageFromURL:url1];
    
    
    CMTime timeValue = kCMTimeZero;
    AVAsset * ass = [AVAsset assetWithURL:url1];
    timeValue = ass.duration;
    float pro = (float)CMTimeGetSeconds(timeValue);
    if (pro >= 10) {
        pro = 10;
    }
    cell.lblTime.text = [NSString stringWithFormat:@"%0.1fs  ", pro];
    cell.imgMain.image = img1;
    cell.btnDelete.tag = indexPath.row+1;
    [cell.btnDelete addTarget:self action:@selector(deleteVideo:) forControlEvents:UIControlEventTouchUpInside];
    cell.btnFull.hidden = YES;
    //    cell.btnFull.tag = indexPath.row+1;
    //    [cell.btnFull addTarget:self action:@selector(playSegment:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(75,75);
}

-(void)resetUserInteractionAfterSecond {
    self.view.userInteractionEnabled = NO;
    double delayInSeconds = 0.1;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        self.view.userInteractionEnabled = YES;
    });
}



- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

- (void)handleLongPress:(UILongPressGestureRecognizer *)gr{
    NSLog(@"HERER");
    switch(gr.state){
        case UIGestureRecognizerStateBegan: {
            NSLog(@"TRIGERRED");
            NSIndexPath *selectedIndexPath = [collViewMain indexPathForItemAtPoint:[gr locationInView:collViewMain]];
            if(selectedIndexPath == nil) break;
            [collViewMain beginInteractiveMovementForItemAtIndexPath:selectedIndexPath];
            break;
        }
        case UIGestureRecognizerStateChanged: {
            NSLog(@"CHANGED");
            [collViewMain updateInteractiveMovementTargetPosition:[gr locationInView:gr.view]];
            break;
        }
        case UIGestureRecognizerStateEnded: {
            [collViewMain endInteractiveMovement];
            NSLog(@"RENEWED");

            
            UILongPressGestureRecognizer *longGR = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
            longGR.minimumPressDuration = 0.2;
            [collViewMain addGestureRecognizer:longGR];
            
            [collViewMain cancelInteractiveMovement];

            break;
        }
        default: {
            [collViewMain cancelInteractiveMovement];
            break;
        }
    }
}

- (void)updateTimeRecordedLabel {
    
    
    CMTime allAlreadyTimes = kCMTimeZero;
    
    
    for (NSURL *url in allAlreadyArray) {
        
        AVAsset *asset = [AVAsset assetWithURL:url];
        allAlreadyTimes = CMTimeAdd(allAlreadyTimes, asset.duration);
        
    }
    
    
    
    if (_recorder.session != nil) {
        currentTime = kCMTimeZero;
        currentTime = CMTimeAdd(_recorder.session.duration, allAlreadyTimes);
    }
    
    float pro = (int)CMTimeGetSeconds(currentTime)/10.0;
    [_progress setProgress:pro animated:YES];
    
    float t = (float)CMTimeGetSeconds(currentTime);
    if (t >= 10) {
        t = 10;
    }
    self.timeRecordedLabel.text = [NSString stringWithFormat:@"%0.1fs/10s", t];
    secondsRecordedVideo = (int)CMTimeGetSeconds(currentTime);
    
    if(secondsRecordedVideo>=9){
        btnRight.enabled = YES;
        
        if(secondsRecordedVideo>=10){
            [_recorder pause];
        }
    }else {
        btnRight.enabled = NO;
    }
}

- (void)recorder:(SCRecorder *)recorder didAppendVideoSampleBufferInSession:(SCRecordSession *)recordSession {
    [self updateTimeRecordedLabel];
}

- (void)handleTouchDetected:(SCTouchDetector*)touchDetector {
    if ([hello isEqualToString:@"Hello, iPhone simulator!"]) {
        [self importClicked:self];
        return;
    }
    if (touchDetector.state == UIGestureRecognizerStateBegan) {
        [_recorder record];
        //self.btnFlipCamera.enabled = NO;
    } else if (touchDetector.state == UIGestureRecognizerStateEnded) {
        [_recorder pause];
    }
}

-(void)playVideoWithURL:(NSURL *)url {
    if (url) {
        self.videoController = [[MPMoviePlayerController alloc] init];
        [self.videoController setContentURL:url];
        [self.videoController.view setFrame:CGRectMake (0, _previewView.frame.origin.y, _previewView.frame.size.width, _previewView.frame.size.height)];
        self.videoController.controlStyle = MPMovieControlStyleNone;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoPlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:self.videoController];
        collViewMain.userInteractionEnabled = NO;
        [self.view addSubview:self.videoController.view];
        [self.videoController play];
    }
}

-(void)playVideoInPopUpViewWithURL:(NSURL *)url {
    if (url) {
        self.videoController = [[MPMoviePlayerController alloc] init];
        [self.videoController setContentURL:url];
        [self.videoController.view setFrame:CGRectMake (0, 0, popView.frame.size.width, popView.frame.size.height)];
        self.videoController.controlStyle = MPMovieControlStyleNone;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoPlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:self.videoController];
        collViewMain.userInteractionEnabled = NO;
        [popView addSubview:self.videoController.view];
        [KGModalWrapper showWithContentView:popView];
        [self.videoController play];
    }
}

- (void)videoPlayBackDidFinish:(NSNotification *)notification {
    //    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    collViewMain.userInteractionEnabled = YES;
    [KGModalWrapper hideView];
    [self.videoController stop];
    [self.videoController.view removeFromSuperview];
    self.videoController = nil;
}

-(void)MixVideoWithTextWithUrl:(NSURL *)vidUrl {
    
    [SVProgressHUD showWithStatus:@"Please Wait..."];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    
    AVURLAsset* videoAsset = [[AVURLAsset alloc]initWithURL:vidUrl options:nil];
    
    AVMutableComposition* mixComposition = [AVMutableComposition composition];
    
    AVMutableCompositionTrack *compositionVideoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    if ([videoAsset tracksWithMediaType:AVMediaTypeVideo].count <= 0) {
        self.view.userInteractionEnabled = true;
        self.navigationController.view.userInteractionEnabled = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        [Utils showAlertWithMessage:@"Video not saved properly. Try again"];
        return;
    }
    
    AVAssetTrack *clipVideoTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    AVMutableCompositionTrack *compositionAudioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    if ([videoAsset tracksWithMediaType:AVMediaTypeAudio].count <= 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        [Utils showAlertWithMessage:@"Video not saved properly. Try again"];
        return;
    }
    
    AVAssetTrack *clipAudioTrack = [[videoAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
    CMTime a = videoAsset.duration;
    
    [compositionVideoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, a) ofTrack:clipVideoTrack atTime:kCMTimeZero error:nil];
    [compositionAudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, a) ofTrack:clipAudioTrack atTime:kCMTimeZero error:nil];
    
    CGSize sizeOfVideo=[[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] naturalSize];
    
    UIImage *myImage = [UIImage imageNamed:@"logo_text.png"];
    CALayer *aLayer = [CALayer layer];
    //aLayer.contents = (id)myImage.CGImage;
    aLayer.frame = CGRectMake(sizeOfVideo.width - 150, sizeOfVideo.height - 150, 100, 60);
    aLayer.opacity = 0.0;
    
    CATextLayer *textOfvideo=[[CATextLayer alloc] init];
    textOfvideo.string=@"Custom Label For Attractions";
    [textOfvideo setFontSize:58];
    [textOfvideo setFrame:CGRectMake(0, 0, sizeOfVideo.width, sizeOfVideo.height/12)];
    [textOfvideo setAlignmentMode:kCAAlignmentCenter];
    [textOfvideo setForegroundColor:[[UIColor redColor] CGColor]];
    [textOfvideo setBackgroundColor:[[UIColor greenColor] CGColor]];
    
    CALayer *optionalLayer=[CALayer layer];
    [optionalLayer addSublayer:textOfvideo];
    optionalLayer.frame=CGRectMake(0, 0, sizeOfVideo.width, sizeOfVideo.height);
    [optionalLayer setMasksToBounds:YES];
    
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer = [CALayer layer];
    parentLayer.frame = CGRectMake(0, 0, sizeOfVideo.width, sizeOfVideo.height);
    videoLayer.frame = CGRectMake(0, 0, sizeOfVideo.width, sizeOfVideo.height);
    [parentLayer addSublayer:videoLayer];
    [parentLayer addSublayer:optionalLayer];
    
    AVMutableVideoComposition* videoComp = [AVMutableVideoComposition videoComposition];
    videoComp.renderSize = sizeOfVideo;
    videoComp.frameDuration = CMTimeMake(1, 30);
    videoComp.animationTool = [AVVideoCompositionCoreAnimationTool videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer inLayer:parentLayer];
    
    // instruction
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    
    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, [mixComposition duration]);
    AVAssetTrack *videoTrack = [[mixComposition tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    AVMutableVideoCompositionLayerInstruction* layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
    
    instruction.layerInstructions = [NSArray arrayWithObject:layerInstruction];
    videoComp.instructions = [NSArray arrayWithObject: instruction];
    
    AVAssetExportSession * _assetExport = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
    _assetExport.videoComposition = videoComp;
    
    NSString* videoName = @"myplaytripvideo.mov";
    
    NSString *exportPath = [NSTemporaryDirectory() stringByAppendingPathComponent:videoName];
    NSURL *exportUrl = [NSURL fileURLWithPath:exportPath];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:exportPath]) {
        [[NSFileManager defaultManager] removeItemAtPath:exportPath error:nil];
    }
    
    _assetExport.outputFileType = AVFileTypeQuickTimeMovie;
    _assetExport.outputURL = exportUrl;
    _assetExport.shouldOptimizeForNetworkUse = YES;
    
    [_assetExport exportAsynchronouslyWithCompletionHandler:
     ^(void ) {
         dispatch_async(dispatch_get_main_queue(), ^{
             [SVProgressHUD dismiss];
             AVURLAsset* avUrl = [[AVURLAsset alloc]initWithURL:_assetExport.outputURL options:nil];
             watermarkedUrl = [avUrl URL];
             dispatch_async(dispatch_get_main_queue(), ^{
                 [SVProgressHUD dismiss];
             });
             self.view.userInteractionEnabled = true;
             self.navigationController.view.userInteractionEnabled = YES;
             [self playVideoWithURL:watermarkedUrl];
         });
     }];
}

-(void)gotoNextViewWithUrl :(NSURL *)aString {
    [SVProgressHUD dismiss];
    [_player pause];
    
    
    VideoPostViewController * controller = [VideoPostViewController initViewControllerWithURL:aString];
    
    
    
    //AttachAudioViewController * controller = [AttachAudioViewController initViewControllerWithURL:aString withTime:secondsRecordedVideo];
    [self.navigationController pushViewController:controller animated:YES];
}

- (void)videoFixOrientationForUrl:(NSURL *)inUrl{
    AVAsset *firstAsset = [AVAsset assetWithURL:inUrl];
    if(firstAsset !=nil && [[firstAsset tracksWithMediaType:AVMediaTypeVideo] count]>0){
        //Create AVMutableComposition Object.This object will hold our multiple AVMutableCompositionTrack.
        AVMutableComposition* mixComposition = [[AVMutableComposition alloc] init];
        
        //VIDEO TRACK
        AVMutableCompositionTrack *firstTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
        [firstTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, firstAsset.duration) ofTrack:[[firstAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] atTime:kCMTimeZero error:nil];
        AVMutableVideoCompositionInstruction * MainInstruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
        MainInstruction.timeRange = CMTimeRangeMake(kCMTimeZero, firstAsset.duration);
        
        if ([[firstAsset tracksWithMediaType:AVMediaTypeAudio] count]>0) {
            //AUDIO TRACK
            AVMutableCompositionTrack *firstAudioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
            [firstAudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, firstAsset.duration) ofTrack:[[firstAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0] atTime:kCMTimeZero error:nil];
        }
        
        //FIXING ORIENTATION//
        AVMutableVideoCompositionLayerInstruction *FirstlayerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:firstTrack];
        
        AVAssetTrack *FirstAssetTrack = [[firstAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
        
        UIImageOrientation FirstAssetOrientation_  = UIImageOrientationUp;
        
        BOOL  isFirstAssetPortrait_  = NO;
        
        CGAffineTransform firstTransform = FirstAssetTrack.preferredTransform;
        
        if(firstTransform.a == 0 && firstTransform.b == 1.0 && firstTransform.c == -1.0 && firstTransform.d == 0) {
            FirstAssetOrientation_= UIImageOrientationRight; isFirstAssetPortrait_ = YES;
        }
        if(firstTransform.a == 0 && firstTransform.b == -1.0 && firstTransform.c == 1.0 && firstTransform.d == 0) {
            FirstAssetOrientation_ =  UIImageOrientationLeft; isFirstAssetPortrait_ = YES;
        }
        if(firstTransform.a == 1.0 && firstTransform.b == 0 && firstTransform.c == 0 && firstTransform.d == 1.0) {
            FirstAssetOrientation_ =  UIImageOrientationUp;
        }
        if(firstTransform.a == -1.0 && firstTransform.b == 0 && firstTransform.c == 0 && firstTransform.d == -1.0) {
            FirstAssetOrientation_ = UIImageOrientationDown;
        }
        
        CGFloat FirstAssetScaleToFitRatio = 320.0/FirstAssetTrack.naturalSize.width;
        
        if(isFirstAssetPortrait_) {
            FirstAssetScaleToFitRatio = 320.0/FirstAssetTrack.naturalSize.height;
            CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(FirstAssetScaleToFitRatio,FirstAssetScaleToFitRatio);
            [FirstlayerInstruction setTransform:CGAffineTransformConcat(FirstAssetTrack.preferredTransform, FirstAssetScaleFactor) atTime:kCMTimeZero];
        } else {
            CGAffineTransform FirstAssetScaleFactor = CGAffineTransformMakeScale(FirstAssetScaleToFitRatio,FirstAssetScaleToFitRatio);
            [FirstlayerInstruction setTransform:CGAffineTransformConcat(CGAffineTransformConcat(FirstAssetTrack.preferredTransform, FirstAssetScaleFactor),CGAffineTransformMakeTranslation(0, 160)) atTime:kCMTimeZero];
        }
        [FirstlayerInstruction setOpacity:0.0 atTime:firstAsset.duration];
        
        MainInstruction.layerInstructions = [NSArray arrayWithObjects:FirstlayerInstruction,nil];;
        
        AVMutableVideoComposition *MainCompositionInst = [AVMutableVideoComposition videoComposition];
        MainCompositionInst.instructions = [NSArray arrayWithObject:MainInstruction];
        MainCompositionInst.frameDuration = CMTimeMake(1, 30);
        MainCompositionInst.renderSize = CGSizeMake(1020.0, 80.0);
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *myPathDocs =  [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"mergeVideo-%d.mov",arc4random() % 1000]];
        
        NSURL *url = [NSURL fileURLWithPath:myPathDocs];
        
        AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
        
        exporter.outputURL=url;
        exporter.outputFileType = AVFileTypeQuickTimeMovie;
        exporter.videoComposition = MainCompositionInst;
        exporter.shouldOptimizeForNetworkUse = YES;
        [exporter exportAsynchronouslyWithCompletionHandler:^ {
            dispatch_async(dispatch_get_main_queue(), ^{
                importedVideoUrl = exporter.outputURL;
                [SVProgressHUD dismiss];
                //                 [self playVideoWithURL:exporter.outputURL];
                
                [self MixVideoWithTextWithUrl:exporter.outputURL];
                
            });
        }];
    }
}

- (void) handleStopButtonTapped {
    [_recorder pause:^{
        [self saveAndShowSession:_recorder.session];
    }];
}

- (NSString*) applicationDocumentsDirectory {
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString* basePath = ([paths count] > 0) ? [paths objectAtIndex:0] : nil;
    return basePath;
}

#pragma LoginDelegate
-(void)loginSuccessfully {}

#pragma SCRecorder Delegates

- (void)recorder:(SCRecorder *)recorder didSkipVideoSampleBufferInSession:(SCRecordSession *)recordSession {
}

- (void)recorder:(SCRecorder *)recorder didReconfigureAudioInput:(NSError *)audioInputError {
}

- (void)recorder:(SCRecorder *)recorder didReconfigureVideoInput:(NSError *)videoInputError {
}

- (void)handleRetakeButton{
    SCRecordSession *recordSession = _recorder.session;
    
    if (recordSession != nil) {
        _recorder.session = nil;
        
        if ([[SCRecordSessionManager sharedInstance] isSaved:recordSession]) {
            [recordSession endSegmentWithInfo:nil completionHandler:nil];
        } else {
            [recordSession cancelSession:nil];
        }
    }
    
    [self prepareSession];
}

- (void)prepareSession {
    if (_recorder.session == nil) {
        
        SCRecordSession *session = [SCRecordSession recordSession];
        session.fileType = AVFileTypeQuickTimeMovie;
        
        _recorder.session = session;
    }
    //[self updateTimeRecordedLabel];
}

- (IBAction)rightClicked:(id)sender {
    //  [self scWatermark];
    [_recorder stopRunning];
    [self  MergeVideo];
    // [self saveAndShowSession:_recorder.session];
}

#pragma mark - Left cycle

- (IBAction)backClicked:(id)sender {
    if (videoClipPaths.count) {
        
        //NSString *message = [NSString stringWithFormat:LOCALIZATION(@"keep_video")];
        [self showErrorWithMessage:@"PlayTrip" message:LOCALIZATION(@"keep_video")];
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    [_recorder stopRunning];
    _recorder.previewView = nil;
}

- (void)viewDidDisappear:(BOOL)animated {
    
    
    [super viewDidDisappear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    
    
    
    NSLog(@"view will dissapear called");
    [super viewWillDisappear:animated];
    [_recorder stopRunning];
    // [self checkSavedSegments];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

///
-(void)cropSelectedVideo:(NSURL *)url{
    
    NSLog(@"Croping video!");
    
    //[SVProgressHUD showWithStatus:@"Cropping ..."];
    [SVProgressHUD show];
    //NSLog(@"zoom scale is %f ", self.MainView.zoomScale);
    
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",url]] options:nil];
    
    
    videosize = [[[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] naturalSize] ;
    
    NSLog(@"Video nutaral size is %f x %f", videosize.width, videosize.height);
    
    //NSLog(@"%f  *  %f",videosize.width , videosize.height);
    CGRect croppingRect ;
    UIImageOrientation videoOrientation = [self getVideoOrientationFromAsset:asset];
    if (videoOrientation == 0 || videoOrientation == 1) { // Potrait
        croppingRect.size.width = videosize.height ;
        croppingRect.size.height = videosize.height ;
        croppingRect.origin.x = 0 ;
        croppingRect.origin.y =  0  ;
    }
    
    else if (videoOrientation == 2 || videoOrientation == 3){// Landscape
        if (0 == croppingPercent) {
            croppingRect.size.width = videosize.height;
            croppingRect.size.height = videosize.height;
            
            
            
            croppingRect.origin.x = 0 ;
            croppingRect.origin.y = 0 ;
        }
        //        else{
        //            croppingRect.size.width = videosize.width / 2.0;
        //            croppingRect.size.height = videosize.width / 2.0;
        //
        ////            NSLog(@"self.MainView.contentOffset.y    is %f", self.MainView.contentOffset.y);
        ////            NSLog(@"mainVieRect.origin.y             is %f", mainVieRect.origin.y);
        ////            NSLog(@"self.MainView.contentSize.height is %f", self.MainView.contentSize.height);
        //
        //            croppingRect.origin.x = (videosize.width / self.MainView.contentSize.width) * self.MainView.contentOffset.x ;
        //
        //            float f1 = self.MainView.contentSize.height - (mainVieRect.origin.y * self.MainView.zoomScale);
        //            float f2 = (videosize.height / f1 ) * self.MainView.contentOffset.y;
        //            float f3 = f2 - (mainVieRect.origin.y / croppingPercent);
        //
        //            float nf1 = (self.MainView.contentOffset.y / self.MainView.zoomScale) - mainVieRect.origin.y;
        //            //float nf2 = (videosize.height / f1 ) * self.MainView.contentOffset.y;
        //            //float nf3 = f2 - (mainVieRect.origin.y / croppingPercent);
        //
        //
        //            croppingRect.origin.y = (f1) * (f3) ;
        //            float newFormula = (videosize.height / (self.MainView.contentSize.height -(mainVieRect.origin.y * self.MainView.zoomScale))) - (mainVieRect.origin.y / croppingRect.origin.y) ;
        //            NSLog(@"newFormula is %f", newFormula);
        //            croppingRect.origin.y = nf1;
        //            // 0.37 * 428 - 426
        //
        //            //croppingRect.origin.y = (self.MainView.contentOffset.y  / (self.MainView.zoomScale ) - mainVieRect.origin.y) ;
        //
        //        }
        
        
        
        
    }
    else{
        NSLog(@"no supported orientation has been found in this video");
    }
    
    //CGFloat offsetRatio = self.MainView.contentSize.width / self.MainView.contentOffset.x ;
    // CGFloat offsetRatioy = self.MainView.contentSize.height / self.MainView.contentOffset.y ;
    
    //croppingRect.origin.x = videosize.width / offsetRatio ;
    //scroppingRect.origin.y = videosize.height / offsetRatioy ;
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd'T'HH:mm:ss'Z'";
    NSDate *date = [NSDate date];
    NSString *tempDir   = [NSTemporaryDirectory() stringByAppendingString:[formatter stringFromDate:date]];
    NSString *d         = [NSString stringWithFormat:@"%@%lld.mov", tempDir, arc4random() % 5000000000];
    NSURL    *outputURL = [NSURL fileURLWithPath:d];
    
    //    CGFloat nearestWidth = (int)croppingRect.size.width - ((int)croppingRect.size.width  % 16 );
    //    CGFloat nearestHeight = (int)croppingRect.size.height - ((int)croppingRect.size.height  % 16 );
    
    //CGRect tempRect = [self zoomRectForScrollView:self.MainView  withCenter:croppingRect.origin];
    
    if (videosize.height > videosize.width) {
        croppingRect = CGRectMake(croppingRect.origin.x, croppingRect.origin.y, videosize.width, videosize.width);
    }     else if (videosize.height < videosize.width)
    {
        croppingRect = CGRectMake(croppingRect.origin.x, croppingRect.origin.y, videosize.height, videosize.height);
    }
    else
    {
        croppingRect = CGRectMake(croppingRect.origin.x, croppingRect.origin.y, videosize.height, videosize.width);
    }
    
    
    
    
    //    AVAssetTrack *videoAsset3Track = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    //    CMTime duration3 = videoAsset3Track.timeRange.duration;
    
    [self applyCropToVideoWithAsset:asset AtRect:croppingRect OnTimeRange:CMTimeRangeMake(kCMTimeZero, [asset duration]) ExportToUrl:outputURL ExistingExportSession:nil WithCompletion:^(BOOL success, NSError *error, NSURL *videoUrl)  {
        if (success) {
            //[self cutFinalFrameVideoForURL:videoUrl];
            
            [SVProgressHUD dismiss];
            
            self.selectedURL = videoUrl;
            NSLog(@"%@",self.selectedURL);
            
            
            AVAsset *newAsset = [AVAsset assetWithURL:videoUrl] ;
            CGSize newVideoSize = [[[newAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] naturalSize] ;
            
            
            NSLog(@"New Video Width and Height = %f , %f" , newVideoSize.width , newVideoSize.height) ;
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [_recorder.session removeAllSegments];
                [videoClipPaths addObject:videoUrl];
                [allAlreadyArray addObject:videoUrl];
                
                
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"videosData"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                videoDataArray = [[NSMutableArray alloc ]init];
                
                
                for (NSURL *url in videoClipPaths) {
                    
                    //saving video data to user default
                    NSData *videoData = [NSData dataWithContentsOfURL:url];
                    [videoDataArray addObject:videoData];
                }
                
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                [userDefaults setObject:videoDataArray forKey:@"videosData"];
                [userDefaults synchronize];
                
                [Utils setArray:[videoClipPaths mutableCopy] Key:VidSegArray];
                
                [collViewMain performBatchUpdates:^{
                    [collViewMain reloadSections:[NSIndexSet indexSetWithIndex:0]];
                } completion:^(BOOL finished) {
                    if((int)CMTimeGetSeconds(currentTime)>10){
                        [_recorder stopRunning];
                    }
                }];
            });
        }
        else{
            [SVProgressHUD dismiss] ;
            NSLog(@"There is an error %@" , error.localizedDescription) ;
            
        }
    }];
}



- (AVAssetExportSession*)applyCropToVideoWithAsset:(AVAsset*)asset AtRect:(CGRect)cropRect OnTimeRange:(CMTimeRange)cropTimeRange ExportToUrl:(NSURL*)outputUrl ExistingExportSession:(AVAssetExportSession*)exporter WithCompletion:(void(^)(BOOL success, NSError* error, NSURL* videoUrl))completion
{
    
    //create an avassetrack with our asset
    
    AVAssetTrack *clipVideoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    
    //create a video composition and preset some settings
    AVMutableVideoComposition* videoComposition = [AVMutableVideoComposition videoComposition];
    
    //AVMutableCompositionTrack * videoComposition = [videoComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    
    videoComposition.frameDuration = CMTimeMake(1, 30);
    
    CGFloat cropOffX = cropRect.origin.x;
    CGFloat cropOffY = cropRect.origin.y;
    CGFloat cropWidth = cropRect.size.width;
    CGFloat cropHeight = cropRect.size.height;
    
    videoComposition.renderSize = CGSizeMake(cropWidth, cropHeight);
    
    //create a video instruction
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    instruction.timeRange = cropTimeRange;
    
    AVMutableVideoCompositionLayerInstruction* transformer = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:clipVideoTrack];
    
    UIImageOrientation videoOrientation = [self getVideoOrientationFromAsset:asset];
    
    CGAffineTransform t1 = CGAffineTransformIdentity;
    CGAffineTransform t2 = CGAffineTransformIdentity;
    
    switch (videoOrientation) {
        case UIImageOrientationUp:
            t1 = CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.height - cropOffX, 0 - cropOffY );
            t2 = CGAffineTransformRotate(t1, M_PI_2 );
            break;
        case UIImageOrientationDown:
            t1 = CGAffineTransformMakeTranslation(0 - cropOffX, clipVideoTrack.naturalSize.width - cropOffY ); // not fixed width is the real height in upside down
            t2 = CGAffineTransformRotate(t1, - M_PI_2 );
            break;
        case UIImageOrientationRight:
            t1 = CGAffineTransformMakeTranslation(0 - cropOffX, 0 - cropOffY );
            t2 = CGAffineTransformRotate(t1, 0 );
            break;
        case UIImageOrientationLeft:
            t1 = CGAffineTransformMakeTranslation(clipVideoTrack.naturalSize.width - cropOffX, clipVideoTrack.naturalSize.height - cropOffY );
            t2 = CGAffineTransformRotate(t1, M_PI  );
            break;
        default:
            NSLog(@"no supported orientation has been found in this video");
            break;
    }
    
    
    
    CGAffineTransform finalTransform = t2;
    [transformer setTransform:finalTransform atTime:kCMTimeZero];
    
    //add the transformer layer instructions, then add to video composition
    instruction.layerInstructions = [NSArray arrayWithObject:transformer];
    videoComposition.instructions = [NSArray arrayWithObject: instruction];
    
    //Remove any prevouis videos at that path
    [[NSFileManager defaultManager]  removeItemAtURL:outputUrl error:nil];
    
    if (!exporter){
        exporter = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetHighestQuality] ;
    }
    
    // assign all instruction for the video processing (in this case the transformation for cropping the video
    exporter.videoComposition = videoComposition;
    exporter.outputFileType = AVFileTypeQuickTimeMovie;
    
    if (outputUrl){
        
        exporter.outputURL = outputUrl;
        [exporter exportAsynchronouslyWithCompletionHandler:^{
            
            switch ([exporter status]) {
                case AVAssetExportSessionStatusFailed:
                    NSLog(@"crop Export failed: %@", [[exporter error] description]);
                    if (completion){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            completion(NO,[exporter error],nil);
                        });
                        return;
                    }
                    break;
                case AVAssetExportSessionStatusCancelled:
                    NSLog(@"crop Export canceled");
                    if (completion){
                        dispatch_async(dispatch_get_main_queue(), ^{
                            completion(NO,nil,nil);
                        });
                        return;
                    }
                    break;
                default:
                    break;
            }
            
            if (completion){
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(YES,nil,outputUrl);
                });
            }
            
        }];
    }
    
    return exporter;
}

-(UIImageOrientation)getVideoOrientationFromAsset:(AVAsset *)asset
{
    AVAssetTrack *videoTrack = [[asset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    CGSize size = [videoTrack naturalSize];
    CGAffineTransform txf = [videoTrack preferredTransform];
    
    
    //    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    //    CMTime time = CMTimeMake(1, 1);
    //    CGImageRef imageRef = [imageGenerator copyCGImageAtTime:time actualTime:NULL error:NULL];
    //    UIImage *thumbnail = [UIImage imageWithCGImage:imageRef];
    //    CGImageRelease(imageRef);  // CGImageRef won't be released by ARC
    //
    //    if (thumbnail.size.height > thumbnail.size.width) {
    //        // potrait video
    //        NSLog(@"portrait");
    //         return UIImageOrientationUp;
    //    } else {
    //        // landscape
    //        NSLog(@"landscape");
    //        return UIImageOrientationRight;
    //
    //    }
    if (size.width == txf.tx && size.height == txf.ty)
        return UIImageOrientationLeft; //return UIInterfaceOrientationLandscapeLeft;
    else if (txf.tx == 0 && txf.ty == 0)
        return UIImageOrientationRight; //return UIInterfaceOrientationLandscapeRight;
    else if (txf.tx == 0 && txf.ty == size.width)
        return UIImageOrientationDown; //return UIInterfaceOrientationPortraitUpsideDown;
    else
        return UIImageOrientationUp;  //return UIInterfaceOrientationPortrait;
}

-(void)MergeVideo
{
    [SVProgressHUD show];
    AVMutableComposition *mixComposition = [[AVMutableComposition alloc] init];
    AVMutableCompositionTrack *videoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo
                                                                        preferredTrackID:kCMPersistentTrackID_Invalid];
    AVMutableCompositionTrack *soundtrackTrack ;
    CMTime AllvideosTime = kCMTimeZero;
    
    for (int i = 0 ; i <  videoClipPaths.count ; i++)
    {
        id object = videoClipPaths[i];
        
        AVAsset *asset = [AVAsset assetWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",object]]];
        CMTime subtrahend = CMTimeMake(0, 1000);
        if([[asset tracksWithMediaType:AVMediaTypeVideo] count]){
            
            [videoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, CMTimeSubtract(asset.duration, subtrahend)) ofTrack:[[asset tracksWithMediaType:AVMediaTypeVideo] firstObject] atTime:AllvideosTime error:nil];
        }
        
        if([[asset tracksWithMediaType:AVMediaTypeAudio] count]){
            soundtrackTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
            [soundtrackTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, asset.duration) ofTrack:[[asset tracksWithMediaType:AVMediaTypeAudio] firstObject] atTime:AllvideosTime error:nil];
        }
        
        AllvideosTime = CMTimeAdd(AllvideosTime, asset.duration);
    }
    
    NSString* documentsDirectory= [self applicationDocumentsDirectory];
    NSString* myDocumentPath= [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"combined_video-%d.mov", arc4random() % 1000000]];
    NSURL* urlVideoMain = [[NSURL alloc] initFileURLWithPath: myDocumentPath];
    if([[NSFileManager defaultManager] fileExistsAtPath:myDocumentPath]){
        [[NSFileManager defaultManager] removeItemAtPath:myDocumentPath error:nil];
    }
    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
    exporter.outputURL = urlVideoMain;
    exporter.outputFileType = @"com.apple.quicktime-movie";
    exporter.shouldOptimizeForNetworkUse = YES;
    [exporter exportAsynchronouslyWithCompletionHandler:^{
        dispatch_async (dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            switch ([exporter status]){
                case AVAssetExportSessionStatusFailed:{
                    NSLog(@"Error ++++++ %@" , [[exporter error] description]) ;
                }
                    break;
                    
                case AVAssetExportSessionStatusCancelled:
                    break;
                    
                case AVAssetExportSessionStatusCompleted:{
                    
                    //goto next with exprote video
                    
                   
                    
                    AVURLAsset* avUrl = [[AVURLAsset alloc]initWithURL:exporter.outputURL options:nil];
                    watermarkedUrl = [avUrl URL];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [SVProgressHUD dismiss];
                    });
                    self.view.userInteractionEnabled = true;
                    self.navigationController.view.userInteractionEnabled = YES;
                    [self gotoNextViewWithUrl:watermarkedUrl];
                    
                    
                    
                    
                }
                    break;
                    
                default:
                    break;
            }
        });
    }];
    
}

#pragma mark - Alert

-(void)showErrorWithMessage:(NSString *)title message:(NSString *)message
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:title
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction* yesButton = [UIAlertAction
                               actionWithTitle:LOCALIZATION(@"discard")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                                {
                                   [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"videosData"];
                                   [[NSUserDefaults standardUserDefaults] synchronize];
                                   [self.navigationController popViewControllerAnimated:YES];
                               }];
    UIAlertAction* noButton = [UIAlertAction
                               actionWithTitle:LOCALIZATION(@"keep")
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action)
                               {
                                   [[NSUserDefaults standardUserDefaults] synchronize];
                                   [self.navigationController popViewControllerAnimated:YES];
                               }];
    [alert addAction:noButton];
    [alert addAction:yesButton];
    [self presentViewController:alert animated:YES completion:nil];
}


- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
@end





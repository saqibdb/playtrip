
#import "VideoDetailsViewController.h"
#import "DraftTourViewController.h"
#import "MapAttractionsViewController.h"
#import "MapViewController.h"
#import "LocalAttrTags.h"
#import "URLSchema.h"
#import "Info.h"
#import "VideoId.h"
#import "MyVideoTourViewController.h"
#import "ColorConstants.h"
#import "LocalVideoObject.h"
#import "SVProgressHUD.h"
#import "Utils.h"
#import "PlayTripManager.h"
#import "GTLR/AppAuth.h"
#import "GTLR/GTLRUtilities.h"
#import "GTLR/GTMSessionUploadFetcher.h"
#import "GTLR/GTMSessionFetcherLogging.h"
#import "GTLR/GTMAppAuth.h"
#import "AppDelegate.h"
#import "ActionSheetPicker.h"
//#import "VideoTour.h"
#import "VideoMapViewController.h"
#import "SearchData.h"
#import "KGModalWrapper.h"
#import "videoDetailCell.h"
#import "Attractions.h"
#import "Categori.h"
#import "DraftAttraction.h"
#import "DraftAttrData.h"
#import "DraftInfo.h"
#import "DraftVideoId.h"
#import "Localisator.h"


static NSString *const kSuccessURLString = @"http://openid.github.io/AppAuth-iOS/redirect/";
NSString *const kGTMAppAuthKeychainItemName = @"YouTubeSample: YouTube. GTMAppAuth";

@interface VideoDetailsViewController () {
    AVPlayer *avPlayer;
    AVPlayerLayer *videoLayer;
    NSString * searchTextGlobal;
}
@end

@implementation VideoDetailsViewController

+(VideoDetailsViewController *)initViewControllerWithURL:(NSURL *)vidUrl {
    VideoDetailsViewController * controller = [[VideoDetailsViewController alloc] initWithNibName:@"VideoDetailsViewController" bundle:nil];
    controller->vidUrl = vidUrl;
    controller.title = @"Post Attractions Video";
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.automaticallyAdjustsScrollViewInsets = YES;
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:LocationName];
    
    self.searchQuery = [HNKGooglePlacesAutocompleteQuery sharedQuery];
    scrollViewMain.contentSize = CGSizeMake(scrollViewMain.frame.size.width, 800);
    [self registerNibs];
    [self initLocManager];
    [self allocArrays];
    [self setPickerForTime];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = true;
    self.searchResults = [NSMutableArray new];
    [self setViewChanges];
    [self checkPickedLocation];
    [self addDoneToolBar];
    NSDate *now = [NSDate date];
    txtTime.text =[Utils timeFormatHHmm:now];
    tblViewAddedAttractions.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tblViewGoogle.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [Utils addBottomBorderForTextField:txtAttractions];
    [Utils addBottomBorderForTextField:txtDescription];
    [Utils addBottomBorderForTextField:txtTime];
    videoTourView.layer.cornerRadius = videoTourView.layer.frame.size.height/2;
    btnSave.layer.cornerRadius =  btnSave.layer.frame.size.height/2;
    [self setScrollViewContentSize];
    btnAddAttraction.backgroundColor = [ColorConstants appBrownColor];
    
    dTour = [DraftTour getDraft];
    lblVideotour.text = dTour.name;
    [self setArray];
    
    BOOL isAddedAttraction = [Utils getBOOLForKey:@"AddedAttraction"];
    if (isAddedAttraction) {
        [Utils setBOOL:NO Key:@"AddedAttraction"];
        [self saveClicked:self];
    }
}

-(void)allocArrays {
    arrPlan = [NSMutableArray new];
    arrPlanName = [NSMutableArray new];
    addedAttractionArray = [NSMutableArray new];
    arrCmsResponse = [NSMutableArray new];
    arrGoogleResponse = [NSMutableArray new];
    arrCmsNames = [NSMutableArray new];
    arrCmsNamesAll = [NSMutableArray new];
    attractionArray = [NSMutableArray new];
}

-(void)setArray {
    
    outerArray = [NSMutableArray new];
    NSMutableArray * localArray = [[dTour.draftAttractionsSet allObjects]mutableCopy];
    attractionArray = [NSMutableArray new];
    
    NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"day" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
    NSArray *sortedArray = [localArray sortedArrayUsingDescriptors:sortDescriptors];
    localArray = [sortedArray mutableCopy];
    attractionArray = localArray;
    
    for (int i = 1;i<=dTour.days.integerValue;i++) {
        NSMutableArray * tempArray = [NSMutableArray new];
        for (DraftAttraction * dAttrs in attractionArray) {
            if (dAttrs.day.integerValue == i) {
                for (DraftAttrData * dAD in [dAttrs.draftAttrDataSet allObjects]) {
                    [tempArray addObject:dAD];
                }
            }
        }
        if (tempArray.count > 0) {
            [outerArray setObject:tempArray atIndexedSubscript:outerArray.count];
        }
    }
    //NSLog(@"%@", outerArray);
    [tblVideotour reloadData];
}

-(void)setScrollViewContentSize {
    
    CGFloat tblHeight = (addedAttractionArray.count * 44)+20; //+ 20
    int a = 0;
    if (addedAttractionArray.count == 0) {
        a = 0;
    }
    
    tblViewAddedAttractions.frame = CGRectMake(tblViewAddedAttractions.frame.origin.x, tblViewAddedAttractions.frame.origin.y, tblViewAddedAttractions.frame.size.width, tblHeight+a); // tblViewAddedAttractions.frame.origin.y
    
    endView.frame = CGRectMake(endView.frame.origin.x, tblViewAddedAttractions.frame.origin.y + tblHeight+a + 20, endView.frame.size.width, endView.frame.size.height);
    
    CGFloat totalheight = endView.frame.size.height+endView.frame.origin.y+60;//tblHeight + topHeight + attrHeight + 120;
    scrollViewMain.contentSize = CGSizeMake(scrollViewMain.frame.size.width, totalheight);
}

-(void)registerNibs {
    UINib *cell = [UINib nibWithNibName:@"videoDetailCell" bundle:nil];
    [tblVideotour registerNib:cell forCellReuseIdentifier:@"videoDetailCell"];
}

-(void)addDoneToolBar {
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelToolbar)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithToolbar)],
                           nil];
    [numberToolbar sizeToFit];
    txtAttractions.inputAccessoryView = numberToolbar;
}

-(void)initLocManager {
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager requestAlwaysAuthorization];
    [locationManager startUpdatingLocation];
}

-(void)setViewChanges {
    UIColor *color = [ColorConstants appYellowColor];
    txtAttractions.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Type your Location" attributes:@{NSForegroundColorAttributeName: color}];
    txtAttractions.textColor = color;
    btnSave.backgroundColor = [ColorConstants appYellowColor];
    
    videoTourView.layer.borderWidth = 1.0;
    videoTourView.layer.borderColor = [ColorConstants appYellowColor].CGColor;
}

-(void)setPickerForTime {
    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
    datePicker.datePickerMode = UIDatePickerModeTime;
    [datePicker addTarget:self action:@selector(updateTime:) forControlEvents:UIControlEventValueChanged];
    [txtTime setInputView:datePicker];
}

-(void)updateTime:(id)sender {
    UIDatePicker *picker = (UIDatePicker*)txtTime.inputView;
    NSString * timeStr = [Utils getHourAndMinutesFromDate:[NSString stringWithFormat:@"%@",picker.date]];
    txtTime.text = timeStr;
}

-(void)checkPickedLocation {
    pickedLocation = [[NSUserDefaults standardUserDefaults] objectForKey:LocationName];
    if(pickedLocation){
        addedAttractionArray = [NSMutableArray new];
        [addedAttractionArray addObject:pickedLocation];
        [tblViewAddedAttractions reloadData];
        [tblViewGoogle setHidden:true];
        progressLine.hidden = NO;
        txtAttractions.text = @"";
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:LocationName];
        [self setScrollViewContentSize];
    }
}

-(void)cancelToolbar {
    [txtAttractions resignFirstResponder];
}

-(void)doneWithToolbar{
    [txtAttractions resignFirstResponder];
    if (txtAttractions.text.length <= 0) {
        return;
    }
    addedAttractionArray = [NSMutableArray new];
    [addedAttractionArray addObject:txtAttractions.text];
    [tblViewAddedAttractions reloadData];
    tblViewGoogle.hidden = YES;
    progressLine.hidden = NO;
    txtAttractions.text = @"";
    
    [self setScrollViewContentSize];
    [self checkPlaces];
}

- (IBAction)playClicked:(id)sender {
    if (waterMarkedUrl) {
        [self playVideoWithURL:waterMarkedUrl];
    } else {
        [self playVideoWithURL:vidUrl];
    }
}

- (IBAction)addAttractionClicked:(id)sender {
    [KGModalWrapper hideView];
//    MapAttractionsViewController * controller = [MapAttractionsViewController initViewController];
    MapViewController * controller = [MapViewController initViewController];
    [self.navigationController pushViewController:controller animated:YES];
//    [self.navigationController presentViewController:controller animated:YES completion:nil];
}

-(void)playVideoWithURL:(NSURL *)url {
    self.videoController = [[MPMoviePlayerController alloc] init];
    [self.videoController setContentURL:url];
    [self.videoController.view setFrame:CGRectMake(0, 0, videoView.frame.size.width, videoView.frame.size.height)];
    [self.videoController setControlStyle:MPMovieControlStyleNone];
    [videoView addSubview:self.videoController.view];
    //    self.videoController.view.transform = CGAffineTransformMakeRotation(M_PI/2);
    [self.videoController play];
    
    double delayInSeconds = 12.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        if(self.videoController.playbackState != MPMoviePlaybackStatePlaying) {
            [videoView bringSubviewToFront:btnPlay];
            [videoView bringSubviewToFront:tblViewGoogle];
        }
    });
    
    countDownTime = 10.0;
    [progressLine setProgress:0 animated:YES];
    timerCountDown = [NSTimer scheduledTimerWithTimeInterval:1  target:self selector:@selector(updateProgressLine) userInfo:nil repeats:YES];
    [videoView bringSubviewToFront:tblViewGoogle];
}

- (void)itemDidFinishPlaying:(NSNotification *)notification {
    AVPlayerItem *playerItem = [notification object];
    [playerItem seekToTime:kCMTimeZero];
    [videoLayer removeFromSuperlayer];
}

-(void)updateProgressLine {
    countDownTime--;
    if (countDownTime <= 0) {
        [timerCountDown invalidate];
    }
    float pro = (10 - countDownTime)/10.0;
    [progressLine setProgress:pro animated:YES];
}

#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == txtDescription) {// || !currentLocation
        return YES;
    }
    [tblViewGoogle setHidden:YES];
    progressLine.hidden = NO;
    [timer invalidate];
    
    arrCmsNames = [NSMutableArray new];
    
    searchTextGlobal =  [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSString * searchString = [NSString stringWithFormat:@"%@", searchTextGlobal];
    if(searchString.length > 0) {
        //        timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(getCMSData) userInfo:nil repeats:YES];
        [self getCMSData];
    } else {
        [tblViewGoogle setHidden:YES];
        progressLine.hidden = NO;
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [timer invalidate];
    searchTextGlobal = @"";
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == txtAttractions) {
        [self doneWithToolbar];
        return NO;
    }
    return YES;
}

- (IBAction)myLocationClicked:(id)sender {
    if (![[AppDelegate appDelegate] isReachable]) {
        [Utils showAlertWithMessage:@"Please connect with internet first"];
    }else {
        VideoMapViewController *controler = [VideoMapViewController initViewController];
        [self.navigationController pushViewController:controler animated:YES];
    }
}

-(void)MixVideoWithTextWithUrl:(NSURL *)vidUrl WithText:(NSString *)watermarkText {
    AVURLAsset* videoAsset = [[AVURLAsset alloc]initWithURL:self->vidUrl options:nil];
    AVMutableComposition* mixComposition = [AVMutableComposition composition];
    AVMutableCompositionTrack *compositionVideoTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    if ([videoAsset tracksWithMediaType:AVMediaTypeVideo].count <= 0) {
        self.view.userInteractionEnabled = true;
        self.navigationController.view.userInteractionEnabled = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        [Utils showAlertWithMessage:@"Video not saved properly. Try again"];
        return;
    }
    
    AVAssetTrack *clipVideoTrack = [[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    AVMutableCompositionTrack *compositionAudioTrack = [mixComposition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];
    if ([videoAsset tracksWithMediaType:AVMediaTypeAudio].count <= 0) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
        });
        [Utils showAlertWithMessage:@"Video not saved properly. Try again"];
        return;
    }
    
    AVAssetTrack *clipAudioTrack = [[videoAsset tracksWithMediaType:AVMediaTypeAudio] objectAtIndex:0];
    CMTime a = videoAsset.duration;
    
    [compositionVideoTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, a) ofTrack:clipVideoTrack atTime:kCMTimeZero error:nil];
    [compositionAudioTrack insertTimeRange:CMTimeRangeMake(kCMTimeZero, a) ofTrack:clipAudioTrack atTime:kCMTimeZero error:nil];
    
    CGSize sizeOfVideo=[[[videoAsset tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0] naturalSize];
    
    CATextLayer *textOfvideo=[[CATextLayer alloc] init];
    textOfvideo.string = watermarkText;
    [textOfvideo setFontSize:38];
    [textOfvideo setFrame:CGRectMake(0, 10, sizeOfVideo.width, sizeOfVideo.height/12)];
    [textOfvideo setAlignmentMode:kCAAlignmentCenter];
    [textOfvideo setForegroundColor:[[UIColor whiteColor] CGColor]];
    [textOfvideo setBackgroundColor:[[UIColor darkGrayColor] CGColor]];
    
    
    CALayer *optionalLayer=[CALayer layer];
    [optionalLayer addSublayer:textOfvideo];
    optionalLayer.frame=CGRectMake(0, 10, sizeOfVideo.width, sizeOfVideo.height);
    [optionalLayer setMasksToBounds:YES];
    
    CALayer *parentLayer = [CALayer layer];
    CALayer *videoLayer1 = [CALayer layer];
    parentLayer.frame = CGRectMake(0, 0, sizeOfVideo.width, sizeOfVideo.height);
    videoLayer1.frame = CGRectMake(0, 0, sizeOfVideo.width, sizeOfVideo.height);
    [parentLayer addSublayer:videoLayer1];
    [parentLayer addSublayer:optionalLayer];
    
    AVMutableVideoComposition* videoComp = [AVMutableVideoComposition videoComposition];
    videoComp.renderSize = sizeOfVideo;
    videoComp.frameDuration = CMTimeMake(1, 30);
    videoComp.animationTool = [AVVideoCompositionCoreAnimationTool videoCompositionCoreAnimationToolWithPostProcessingAsVideoLayer:videoLayer1 inLayer:parentLayer];
    
    // instruction
    AVMutableVideoCompositionInstruction *instruction = [AVMutableVideoCompositionInstruction videoCompositionInstruction];
    
    instruction.timeRange = CMTimeRangeMake(kCMTimeZero, [mixComposition duration]);
    AVAssetTrack *videoTrack = [[mixComposition tracksWithMediaType:AVMediaTypeVideo] objectAtIndex:0];
    AVMutableVideoCompositionLayerInstruction* layerInstruction = [AVMutableVideoCompositionLayerInstruction videoCompositionLayerInstructionWithAssetTrack:videoTrack];
    
    instruction.layerInstructions = [NSArray arrayWithObject:layerInstruction];
    videoComp.instructions = [NSArray arrayWithObject: instruction];
    
    AVAssetExportSession * _assetExport = [[AVAssetExportSession alloc] initWithAsset:mixComposition presetName:AVAssetExportPresetHighestQuality];
    _assetExport.videoComposition = videoComp;
    
    NSString * timeStamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
    NSString* videoName = [NSString stringWithFormat:@"%@.mp4", timeStamp];
    
    NSString *exportPath = [NSTemporaryDirectory() stringByAppendingPathComponent:videoName];
    NSURL *exportUrl = [NSURL fileURLWithPath:exportPath];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:exportPath]) {
        [[NSFileManager defaultManager] removeItemAtPath:exportPath error:nil];
    }
    
    _assetExport.outputFileType = AVFileTypeQuickTimeMovie;
    _assetExport.outputURL = exportUrl;
    _assetExport.shouldOptimizeForNetworkUse = YES;
    
    [_assetExport exportAsynchronouslyWithCompletionHandler:
     ^(void ) {
         dispatch_async(dispatch_get_main_queue(), ^{
             [SVProgressHUD dismiss];
             AVURLAsset* avUrl = [[AVURLAsset alloc]initWithURL:_assetExport.outputURL options:nil];
             self->waterMarkedUrl = [avUrl URL];
             dispatch_async(dispatch_get_main_queue(), ^{
                 [SVProgressHUD dismiss];
             });
             self.view.userInteractionEnabled = true;
             self.navigationController.view.userInteractionEnabled = YES;
//             [self playVideoWithURL:self->waterMarkedUrl];
             [self afterWaterMark];
         });
     }];
}

#pragma TableViewDelegate and DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == tblVideotour) {
        return outerArray.count;
    }else{
        return 1;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == tblVideotour){
        return [[outerArray objectAtIndex:section] count];
    }else if (tableView == tblViewGoogle) {
        if (self.searchResults.count <= 0) {
            tblViewGoogle.hidden = YES;
            progressLine.hidden = NO;
            return 0;
        } else {
            return self.searchResults.count;
        }
    } else {
        return addedAttractionArray.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    if(tableView == tblVideotour){
        videoDetailCell *videoCell = (videoDetailCell*)[tblVideotour dequeueReusableCellWithIdentifier:@"videoDetailCell" forIndexPath:indexPath];
        [videoCell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        [videoCell.btnCheck setSelected:NO];
        if (selectedIndex == indexPath) {
            [videoCell.btnCheck setSelected:YES];
        }
        DraftAttrData *ad = [[outerArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        
        
        videoCell.lblDesc.text = [NSString stringWithFormat:@"%@", ad.draftInfo.name];
        
        if (ad.draftVideoId.thumbnail.length > 0) {
            //NSLog(@"%@",ad.draftVideoId.file_name);
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,ad.draftVideoId.thumbnail];
            videoCell.imgView.url = [NSURL URLWithString:urlString];
        } else {
            videoCell.imgView.image = [UIImage imageNamed:@"no_video.png"];
        }
        
        return videoCell;
    } else if (tableView == tblViewGoogle) {
        if (self.searchResults.count > 0) {
            cell.textLabel.text = self.searchResults[indexPath.row];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }else {
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.textColor = [ColorConstants appYellowColor];
        cell.textLabel.text = [addedAttractionArray objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (tableView == tblVideotour) {
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 30)];
        
        UILabel *lblText = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, [[UIScreen mainScreen] bounds].size.width-100, 20)];
        lblText.textColor = [UIColor whiteColor];
        lblText.font = [UIFont systemFontOfSize:12];
        
        UILabel *lblSpot = [[UILabel alloc] initWithFrame:CGRectMake( 230, 5, 50, 20)];
        lblSpot.textColor = [UIColor whiteColor];
        lblSpot.font = [UIFont systemFontOfSize:12];
        if(attractionArray.count <=0){
            
            lblSpot.text =@"0 spots";
        }else{
//            DraftAttraction *a = [attractionArray objectAtIndex:section];
            lblSpot.text =[NSString stringWithFormat:@"%lu spots",[[outerArray objectAtIndex:section] count]];
        }
        
//        DraftAttraction * attr = [attractionArray objectAtIndex:section];
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        NSInteger dayCount = section;
        //                if (dayCount >= 1){
        //        dayCount = dayCount -1;
        //                }
        dayComponent.day = dayCount;
        NSCalendar *theCalendar = [NSCalendar currentCalendar];
        NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:dTour.from_date options:0];
        NSString *next = [Utils dateFormatyyyyMMdd:nextDate];
        NSString * day =[[Utils getDayFromDate:nextDate] substringToIndex:3];
        lblText.text =[NSString stringWithFormat:@"Day %ld - %@(%@)",(long)dayCount+1,next,day];
        
        [view addSubview:lblText];
        [view addSubview:lblSpot];
        
        
        [view setBackgroundColor:[UIColor darkGrayColor]];
        return view;
        
        
    }
    return [[UIView alloc]initWithFrame:CGRectZero];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (tableView == tblVideotour) {
        return 30;
    }
    return 0;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == tblViewAddedAttractions) {
        return;
    }
    if(tableView == tblVideotour){
        selectedIndex = nil;
        [tblVideotour reloadData];
        videoDetailCell *videCell = [tblVideotour cellForRowAtIndexPath:indexPath];
        [videCell.btnCheck setSelected:YES];
        selectedIndex = indexPath;
        DraftAttrData *ad = [[outerArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        selectedAttractionData = ad;
        return;
    }
    [txtAttractions resignFirstResponder];
    if (self.searchResults.count >= indexPath.row) {
        NSString * selectedPlace = [self.searchResults objectAtIndex:indexPath.row];
        addedAttractionArray = [NSMutableArray new];
        [addedAttractionArray addObject:selectedPlace];
    }
    [tblViewAddedAttractions reloadData];
    [tblViewGoogle setHidden:true];
    progressLine.hidden = NO;
    txtAttractions.text = @"";
    
    [self setScrollViewContentSize];
    [self checkPlaces];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == tblViewAddedAttractions) {
        return YES;
    } else {
        return NO;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [addedAttractionArray removeObjectAtIndex:indexPath.row];
        [tblViewAddedAttractions reloadData];
        [self setScrollViewContentSize];
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath{}

- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
    NSDictionary *section = [arrGoogleResponse objectAtIndex:sourceIndexPath.section];
    NSUInteger sectionCount = [[section valueForKey:@"content"] count];
    
    if (sourceIndexPath.section != proposedDestinationIndexPath.section) {
        NSUInteger rowInSourceSection =
        (sourceIndexPath.section > proposedDestinationIndexPath.section) ?
        0 : sectionCount - 1;
        return [NSIndexPath indexPathForRow:rowInSourceSection inSection:sourceIndexPath.section];
    } else if (proposedDestinationIndexPath.row >= sectionCount) {
        return [NSIndexPath indexPathForRow:sectionCount - 1 inSection:sourceIndexPath.section];
    }
    return proposedDestinationIndexPath;
}

-(void)getCMSData{
    arrCmsNames = [NSMutableArray new];
    arrCmsNamesAll = [NSMutableArray new];
    
    NSString * searchString = [NSString stringWithFormat:@"%@", searchTextGlobal];
    if (searchString.length <= 0) {
        [tblViewGoogle setHidden:YES];
        progressLine.hidden = NO;
        return;
    }
    arrCmsNames = [SearchData getByName:searchString];
    for (SearchData * sd in arrCmsNames) {
        [self.searchResults addObject:sd.name];
        [arrCmsNamesAll addObject:sd.name];
    }
    
    [tblViewGoogle setHidden:NO];
    progressLine.hidden = YES;
    [tblViewGoogle reloadData];
    
    if ([[AppDelegate appDelegate] isReachable]) {
        [self searchGooglePlacesWithString:searchString];
    }
}

-(void)searchGooglePlacesWithString:(NSString *)searchText {
    arrGoogleNames = [NSMutableArray new];
    self.searchResults = [NSMutableArray new];
    [self.searchQuery fetchPlacesForSearchQuery:searchText configurationBlock:^(HNKGooglePlacesAutocompleteQueryConfig *config) {
        config.latitude = currentLocation.coordinate.latitude;
        config.longitude = currentLocation.coordinate.longitude;
        config.searchRadius = 500;//meters
    } completion:^(NSArray *places, NSError *error) {
        if (error) {
            [self handleSearchError:error];
        } else {
            for (HNKGooglePlacesAutocompletePlace * p in places) {
                if (![self.searchResults containsObject:p.name]) {
                    [self.searchResults addObject:p.name];
                    [arrGoogleNames addObject:p.name];
                    [arrGoogleResponse addObject:p];
                }
            }
            if (arrGoogleResponse.count > 0) {
                [tblViewGoogle setHidden:NO];
                progressLine.hidden = YES;
            }
            [tblViewGoogle reloadData];
        }
    }];
}

-(void)checkPlaces {
    for (NSString * mainName in addedAttractionArray) {
        if ([arrCmsNamesAll containsObject:mainName]) {
        } else {
            for (HNKGooglePlacesAutocompletePlace * p in arrGoogleResponse) {
                if ([p.name isEqualToString:mainName]) {
                    [self getPlaceDetails:p.placeId andName:p.name];
                }
            }
            if (![arrGoogleNames containsObject:mainName]) {
                if ([[AppDelegate appDelegate] isReachable]) {
                    [self callAddTagApiWithName:mainName andAddress:nil andLat:nil andLng:nil andPlaceId:nil];
                } else {
                    NSString * timeStamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
                    LocalAttrTags * vd = [LocalAttrTags newEntity];
                    vd.entity_id = timeStamp;//[self getNewEntityIdforVidObj];
                    vd.tagName = mainName;
                    [LocalAttrTags saveEntity];
                    NSMutableArray * aArray = [LocalAttrTags getAll];
                   // NSLog(@"%@", aArray);
                }
            }
        }
    }
}

-(void)getPlaceDetails:(NSString *)placeId andName:(NSString *)placeName{
    NSString* urlAsString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?placeid=%@&key=AIzaSyBiYdF5vtwH0gdrXxG_OnyBr6rWaa2cHSc", placeId];
    NSURL *url = [[NSURL alloc] initWithString:urlAsString];
    
    [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:url] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if (error) {
        } else {
            NSMutableDictionary *parsedObject = [[NSJSONSerialization JSONObjectWithData:data options:0 error:nil]mutableCopy];
            NSMutableArray * aArray = [parsedObject valueForKey:@"result"];
            if ([aArray valueForKey:@"formatted_address"]) {
                NSString *address = [aArray valueForKey:@"formatted_address"];
                if ([aArray valueForKey:@"geometry"]) {
                    NSMutableDictionary * geoDict = [aArray valueForKey:@"geometry"];
                    if ([geoDict valueForKey:@"location"]) {
                        NSMutableDictionary * locDict = [geoDict valueForKey:@"location"];
                        NSString * lat = [locDict valueForKey:@"lat"];
                        NSString * lng = [locDict valueForKey:@"lng"];
                        [self callAddTagApiWithName:placeName andAddress:address andLat:lat andLng:lng andPlaceId:placeId];
                    }
                }
            }
        }
    }];
}

-(void) callAddTagApiWithName:(NSString *)name andAddress:(NSString *)address andLat:(NSString *)lat andLng:(NSString *)lng andPlaceId:(NSString *)placeId{
    [[PlayTripManager Instance] tagsAddEditWithName:name andAddress:address andPlaceId:placeId andLat:lat andLng:lng WithBlock:^(id result, NSString *error) {
    }];
}

- (void)handleSearchError:(NSError *)error {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}


- (IBAction)saveClicked:(id)sender {
    
    NSString * message = @"";
    
    if (addedAttractionArray.count <= 0) {
        message = @"Please add Attraction Tag";
    } else if (txtDescription.text.length <= 0) {
        message = @"Please enter description";
    } else if (txtTime.text.length <= 0) {
        message = @"Please select time";
    }
//    else if (!selectedPlan) {
//        message = @"Please select tour";
//    }
    
    if (message.length > 0) {
        [Utils showAlertWithMessage:message];
    } else {
        lblTopMain.text = lblVideotour.text;
        lblTopMain =  [Utils roundCornersOnView:lblTopMain onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];
        [tblVideotour reloadData];
        [KGModalWrapper showWithContentView:videotourPopView];
    }
}

- (IBAction)postClicked:(id)sender {
    [self uploadVideo];
}

-(void)getLatestVideoTour{
    arrPlan = [Plan getVideoTourDraft];
    for(int i= 0 ; i<arrPlan.count ; i++){
        Plan *v = [arrPlan objectAtIndex:i];
        [arrPlanName addObject:v.name];
    }
    if(arrPlan.count <=0){
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"There are No Video Tour for you" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:okayAction];
        [[[UIApplication sharedApplication] keyWindow].rootViewController presentViewController:alert animated:YES completion:^{
        }];
    } else {
        [self showPicker:btnVideotour];
    }
}

- (IBAction)selectVideoTourClicked:(id)sender {
    if (arrPlanName.count <= 0) {
        [self getLatestVideoTour];
    } else {
        [self showPicker:sender];
    }
}

-(void)showPicker:(id)sender {
    [ActionSheetStringPicker showPickerWithTitle:@"Select Video Tours" rows:arrPlanName initialSelection:0  doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedInd, id selectedValue) {
        lblVideotour.text =[arrPlanName objectAtIndex:selectedInd] ;
//        selectedPlan = [arrPlan objectAtIndex:selectedInd];
//        attractionArray = [[selectedPlan.attractionsSet allObjects]mutableCopy];
//        startDate = selectedPlan.from_date;
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    } origin:sender];
}


-(void)uploadVideo {
    [SVProgressHUD showWithStatus:@"Uploading..."];

    DraftAttrData * ad = selectedAttractionData;
    ad.desc = txtDescription.text;
    ad.time = txtTime.text;
    [DraftAttrData saveEntity];
    
    [[PlayTripManager Instance] uploadVideoWithVideoUrl:waterMarkedUrl WithAttrId:selectedAttractionData.entity_id WithBlock:^(id result, NSString *error) {
        [SVProgressHUD dismiss];
        if (result) {
            //NSLog(@"%@", result);
            
            DraftAttrData * dAttrData = selectedAttractionData;
            dAttrData.draftVideoId.file_name = [result valueForKey:@"file_name"];
            dAttrData.draftVideoId.entity_id = [result valueForKey:@"id"];
            dAttrData.draftVideoId.thumbnail = [result valueForKey:@"thumbnail"];
            dAttrData.draftVideoId.user = [result valueForKey:@"user"];
            [DraftAttrData saveEntity];
            [KGModalWrapper hideView];
            
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Video successfully saved and added to the attraction" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//                [self.navigationController popToRootViewControllerAnimated:true];
                DraftTourViewController * controller = [DraftTourViewController initViewController];
                [self.navigationController pushViewController:controller animated:YES];
            }];
            [alert addAction:okayAction];
            [self.navigationController presentViewController:alert animated:true completion:nil];
            
        } else {
            if (!error) {
                error = @"Upload failure. Please retry.";
            }
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:error preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            }];
            [alert addAction:okayAction];
            [self.navigationController presentViewController:alert animated:true completion:nil];
        }
    }];
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:true];
}

- (IBAction)cancleClicked:(id)sender{
    selectedAttractionData = nil;
    [KGModalWrapper hideView];
}

- (IBAction)okClicked:(id)sender{
    
    
    
    if (selectedAttractionData) {
        
        DraftAttrData * ad = selectedAttractionData;
        
        if (ad.draftVideoId.file_name.length > 0) {
            
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Video is already there. Are you sure you want to replace it?" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [SVProgressHUD showWithStatus:@"Please Wait..."];
                [KGModalWrapper hideView];
                
                [self MixVideoWithTextWithUrl:vidUrl WithText:[self createTrickyStringForAttractions]];
            }];
            UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:nil];
            
            [alert addAction:yesAction];
            [alert addAction:noAction];
            [[Utils getTopViewController] presentViewController:alert animated:YES completion:nil];
            
        } else {
            [SVProgressHUD showWithStatus:@"Please Wait..."];
            [KGModalWrapper hideView];

            [self MixVideoWithTextWithUrl:vidUrl WithText:[self createTrickyStringForAttractions]];
        }
    } else {
        [Utils showAlertWithMessage:@"Please add attraction to tour"];
    }
}

-(void) afterWaterMark {
    
    if ([[AppDelegate appDelegate] isReachable]) {
        [SVProgressHUD showWithStatus:@"Please Wait..."];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [self uploadVideo];
    } else {
        [self saveVideoObjInCoreData];
    }
}

-(void)uploadAttrData {
//    [[PlayTripManager Instance] createAttrDataWithId:selectedAttractionData.entity_id andTime:txtTime.text andDesc:txtDescription.text andVideoId:selectedPlan.entity_id WithBlock:^(id result, NSString *error) {
//        if (error) {
//            NSLog(@"%@", error);
//        } else {
//            [self uploadVideo];
//        }
//    }];
}

-(void)saveVideoObjInCoreData {
    
    [KGModalWrapper hideView];
    
    NSMutableArray * localArray = [LocalVideoObject getAll];
    
    BOOL isPresent = NO;
    NSString * existingVidObjId = @"";
    for (LocalVideoObject * locVid in localArray) {
        if ([locVid.attrDataId isEqualToString:selectedAttractionData.entity_id]) {
            existingVidObjId = locVid.entity_id;
            isPresent = YES;
        }
    }
    
    if (isPresent == YES) {
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Video is already there. Are you sure you want to replace it?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * replaceAction = [UIAlertAction actionWithTitle:@"Replace" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [self updateVideoDataWithEntityId:existingVidObjId];
        }];
        UIAlertAction * cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alert addAction:replaceAction];
        [alert addAction:cancelAction];
        [self.navigationController presentViewController:alert animated:YES completion:nil];
    } else {
        [self saveVideoData];
    }
}

-(void)updateVideoDataWithEntityId:(NSString *)entity_id {
    
    NSString * timeStamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
    LocalVideoObject * vd = [LocalVideoObject getByEntityId:entity_id];
    vd.entity_id = timeStamp;//[self getNewEntityIdforVidObj];
    vd.desc = txtDescription.text;
    vd.videoTourId = dTour.entity_id;
    vd.time = txtTime.text;
    vd.attrDataId = selectedAttractionData.entity_id;
//    vd.videoObj = [NSData dataWithContentsOfURL:vidUrl];
    vd.vidPath = [waterMarkedUrl path];
    vd.addedAttractions = [self createTrickyStringForAttractions];
    [LocalVideoObject saveEntity];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Video will be saved and added to the attraction" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController popToRootViewControllerAnimated:true];
        }];
        [alert addAction:okayAction];
        [self.navigationController presentViewController:alert animated:true completion:nil];
    });
}

-(void)saveVideoData {
    
    NSString * timeStamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
    
    
    LocalVideoObject * vd = [LocalVideoObject newEntity];
    vd.entity_id = timeStamp;
    vd.desc = txtDescription.text;
    vd.videoTourId = dTour.entity_id;
    vd.time = txtTime.text;
    vd.attrDataId = selectedAttractionData.entity_id;
    vd.vidPath = [waterMarkedUrl path];
    UIImage * img = [Utils generateThumbImageFromURL:vidUrl];
    
    NSData *imageData = UIImagePNGRepresentation(img);
    vd.thumbnail = imageData;
    
    vd.addedAttractions = [self createTrickyStringForAttractions];
    [LocalVideoObject saveEntity];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Video will be saved and added to the attraction" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController popToRootViewControllerAnimated:true];
        }];
        [alert addAction:okayAction];
        [self.navigationController presentViewController:alert animated:true completion:nil];
    });
}

-(NSString *)createTrickyStringForAttractions {
    NSString * finalString = @"";
    for (NSString * name in addedAttractionArray) {
        NSString * aStr = [NSString stringWithFormat:@"%@**", name];
        finalString = [finalString stringByAppendingString:aStr];
    }
    finalString = [finalString substringToIndex:[finalString length] - 2];
    return finalString;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = false;
    [self.videoController stop];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

/*
 #pragma mark - CLLocationManagerDelegate
 
 - (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
 }
 
 - (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
 if (!currentLocationName) {
 currentLocation = newLocation;
 [Utils getAddressFromLocation:newLocation complationBlock:^(NSString *name) {
 if (name) {
 currentLocationName = name;
 }
 }];
 }
 }
 */


/*
 
 //-(void)playVideoInView {
 //    avPlayer = [AVPlayer playerWithURL:vidUrl];
 //    avPlayer.actionAtItemEnd = AVPlayerActionAtItemEndNone;
 //
 //    videoLayer = [AVPlayerLayer playerLayerWithPlayer:avPlayer];
 //    videoLayer.frame = videoView.bounds;
 //    videoLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
 //    [videoView.layer addSublayer:videoLayer];
 //
 //    [avPlayer play];
 //    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:[avPlayer currentItem]];
 //}
 
 
 */



/*
 -------------------------------------YOUTUBE-------------------------------------
 
 
 
 
 -(void)uploadToYouTube {
 [self signInClicked:self];
 }
 
 #pragma YouTube Methods
 
 - (GTLRYouTubeService *)youTubeService {
 static GTLRYouTubeService *service;
 static dispatch_once_t onceToken;
 dispatch_once(&onceToken, ^{
 service = [[GTLRYouTubeService alloc] init];
 service.shouldFetchNextPages = YES;
 service.retryEnabled = YES;
 });
 return service;
 }
 
 - (NSString *)signedInUsername {
 // Get the email address of the signed-in user.
 id<GTMFetcherAuthorizationProtocol> auth = self.youTubeService.authorizer;
 BOOL isSignedIn = auth.canAuthorize;
 if (isSignedIn) {
 return auth.userEmail;
 } else {
 return nil;
 }
 }
 
 - (BOOL)isSignedIn {
 NSString *name = [self signedInUsername];
 return (name != nil);
 }
 
 - (void)signInClicked:(id)sender {
 if (![self isSignedIn]) {
 // Sign in.
 [self runSigninThenHandler:^{
 [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
 [SVProgressHUD showWithStatus:@"Uploading to YouTube..."];
 [self btnUploadClicked:self];
 }];
 } else {
 GTLRYouTubeService *service = self.youTubeService;
 [GTMAppAuthFetcherAuthorization
 removeAuthorizationFromKeychainForName:kGTMAppAuthKeychainItemName];
 service.authorizer = nil;
 [Utils showAlertWithMessage:@"def"];
 }
 }
 
 - (void)runSigninThenHandler:(void (^)(void))handler {
 NSString *clientID = @"1044395803922-ijglbgrb1r345jhh973s1lb5c7i8shte.apps.googleusercontent.com";
 NSString *clientSecret = @"";
 
 NSURL *localRedirectURI = [NSURL URLWithString:@"com.app.playtrip:/oidc_callback"];
 
 OIDServiceConfiguration *configuration =
 [GTMAppAuthFetcherAuthorization configurationForGoogle];
 NSArray<NSString *> *scopes = @[ kGTLRAuthScopeYouTube, OIDScopeEmail ];
 OIDAuthorizationRequest *request =
 [[OIDAuthorizationRequest alloc] initWithConfiguration:configuration clientId:clientID clientSecret:clientSecret scopes:scopes redirectURL:localRedirectURI responseType:OIDResponseTypeCode additionalParameters:nil];
 
 __weak __typeof(self) weakSelf = self;
 
 [AppDelegate appDelegate].currentAuthorizationFlow = [OIDAuthState authStateByPresentingAuthorizationRequest:request presentingViewController:self callback:^(OIDAuthState * _Nullable authState, NSError * _Nullable error) {
 if (authState) {
 GTMAppAuthFetcherAuthorization *gtmAuthorization =
 [[GTMAppAuthFetcherAuthorization alloc] initWithAuthState:authState];
 weakSelf.youTubeService.authorizer = gtmAuthorization;
 [GTMAppAuthFetcherAuthorization saveAuthorization:gtmAuthorization
 toKeychainForName:kGTMAppAuthKeychainItemName];
 if (handler) handler();
 }
 }];
 }
 
 - (void)btnUploadClicked:(id)sender {
 GTLRYouTube_VideoStatus *status = [GTLRYouTube_VideoStatus object];
 status.privacyStatus = @"private";
 GTLRYouTube_VideoSnippet *snippet = [GTLRYouTube_VideoSnippet object];
 snippet.title = @"PlayTrip Video";
 NSString * descText = txtDescription.text;
 if (descText.length <= 0) {
 descText = @"No Description";
 }
 NSString *desc = descText;
 if (desc.length > 0) {
 snippet.descriptionProperty = desc;
 }
 GTLRYouTube_Video *video = [GTLRYouTube_Video object];
 video.status = status;
 video.snippet = snippet;
 
 [self uploadVideoWithVideoObject:video resumeUploadLocationURL:nil];
 
 }
 
 - (void)uploadVideoWithVideoObject:(GTLRYouTube_Video *)video resumeUploadLocationURL:(NSURL *)locationURL {
 
 NSURL *fileToUploadURL = vidUrl;
 NSString *mimeType = @"video/mp4";
 GTLRUploadParameters *uploadParameters =
 [GTLRUploadParameters uploadParametersWithFileURL:fileToUploadURL MIMEType:mimeType];
 uploadParameters.uploadLocationURL = locationURL;
 
 GTLRYouTubeQuery_VideosInsert *query =
 [GTLRYouTubeQuery_VideosInsert queryWithObject:video part:@"snippet,status" uploadParameters:uploadParameters];
 
 query.executionParameters.uploadProgressBlock = ^(GTLRServiceTicket *ticket, unsigned long long numberOfBytesRead, unsigned long long dataLength) {
 };
 
 GTLRYouTubeService *service = self.youTubeService;
 _uploadFileTicket = [service executeQuery:query completionHandler:^(GTLRServiceTicket *callbackTicket, GTLRYouTube_Video *uploadedVideo, NSError *callbackError) {
 if (callbackError == nil) {
 [SVProgressHUD dismiss];
 UIAlertController * alert =  [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Successfully Uploaded" preferredStyle:UIAlertControllerStyleAlert];
 UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
 [self.navigationController popToRootViewControllerAnimated:true];
 }];
 [alert addAction:okayAction];
 [self.navigationController presentViewController:alert animated:true completion:nil];
 } else {
 [SVProgressHUD dismiss];
 [Utils showAlertWithMessage:@"Upload failure"];
 }
 }];
 }
 
 
 
 */

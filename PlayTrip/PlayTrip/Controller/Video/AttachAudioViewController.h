
#import <UIKit/UIKit.h>
#import <AVKit/AVPlayerViewController.h>
#import <AVKit/AVError.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MediaPlayer/MediaPlayer.h>

@interface AttachAudioViewController : UIViewController<UITableViewDelegate,UITableViewDataSource, AVAudioRecorderDelegate>{
    
    IBOutlet UIButton *btnPlay;
    NSURL * vidUrl;
    
    NSURL * recordedAudioUrl;
    NSURL * mergedAudioUrl;
    NSURL * nextViewUrl;
    
    IBOutlet UIButton *btnDone;
    
    IBOutlet UIButton *btnRecordAudio;
    IBOutlet UILabel *lblTime;

    IBOutlet UIProgressView *progressLine;
    int secondsRecordedVideo;
    
    int countDownTime;
    NSTimer * timerCountDown;
    
    UIProgressView *audioProgress;
    
    IBOutlet UIButton *btnMusicList;
    IBOutlet UIView *videoPreviewView;
    IBOutlet UIView *listView;
}

@property (strong, nonatomic) MPMoviePlayerController *videoController;
- (IBAction)cancelAudioClicked:(id)sender;
- (IBAction)playClicked:(id)sender;

- (IBAction)audioListClicked:(id)sender;
- (IBAction)doneClicked:(id)sender;
- (IBAction)recordAudioClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tblView;
+(AttachAudioViewController *)initViewControllerWithURL:(NSURL *)vidUrl withTime:(int)recordedVideoTime;
@end

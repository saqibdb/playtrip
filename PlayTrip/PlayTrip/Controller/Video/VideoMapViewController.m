
#import "VideoMapViewController.h"
#import "ActionSheetStringPicker.h"
#import "Account.h"
#import "AccountManager.h"
#import "ColorConstants.h"
#import "Utils.h"
#import "Categori.h"
#import "SVProgressHUD.h"
#import "PlayTripManager.h"
#import "URLSchema.h"
#import "Attractions.h"
#import "Info.h"
#import "KGModalWrapper.h"
#import "Country.h"
#import "DistrictModel.h"
#import "CityModel.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import "Localisator.h"
#import "AttractionDataModel.h"

@import AWSS3;


@interface VideoMapViewController () {
    BOOL isFirstTime;
    NSMutableArray *countriesArray;
    Country *selectedCountry;
    NSMutableArray *districtsArray;
    DistrictModel *selectedDistrict;
    NSMutableArray *citiesArray;
    CityModel *selectedCity;
    
    
    Country *previousSelectedCountry;


}
@end

@implementation VideoMapViewController

+(VideoMapViewController *)initViewController {
    VideoMapViewController * controller = [[VideoMapViewController alloc] initWithNibName:@"VideoMapViewController" bundle:nil];
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    isFirstTime = YES;

   // mapView = [GMSMapView new];
    
    [scrollviewPop setContentSize:CGSizeMake(280, 500)];
    
    
    isallocated = NO;
    
    mapView.myLocationEnabled = YES;
    mapView.delegate = self;
    mapView.settings.myLocationButton = NO;
    mapView.settings.compassButton = NO;
    
    
    //self.view = mapView;
     // btnAddAttr.frame = CGRectMake(10, [UIScreen mainScreen].bounds.size.height - 50, btnAddAttr.frame.size.width, btnAddAttr.frame.size.height);
    
    //[self.view addSubview:btnAddAttr];
    
    self.navigationController.navigationBarHidden = false;
    [self setNavBarItems];
    
    
    
    
    
    for (UIView *object in mapView.subviews) {
        
        for (UIView *obj in object.subviews) {
            if ([obj isKindOfClass:[UIButton class]]) {
                UIButton *button = (UIButton *)obj;
                
                
                NSString *name = button.accessibilityIdentifier ;
                if ([name containsString:@"my_location"]) {
                    NSLog(@"Found it");
                }
            }
            
        }
        
        
        
    }

    
    if (self.videoFirstFrameImage) {
        coverImage.image = self.videoFirstFrameImage;
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    [mapView addObserver:self forKeyPath:@"myLocation" options:NSKeyValueObservingOptionNew context:nil];

    
    btnAddAttr.layer.cornerRadius = btnAddAttr.frame.size.height / 2;
    
    btnPick.layer.cornerRadius = 5.0;
    [btnPick addTarget:self action:@selector(pickClicked) forControlEvents:UIControlEventTouchUpInside];

    [self setLocalizedStrings];
    [self getAttractions];
}

-(void)setLocalizedStrings {

    [lblTop setText:LOCALIZATION(@"add_attraction")];
    [lblLocType setText:LOCALIZATION(@"type")];
    [lblLocCountry setText:LOCALIZATION(@"country1")];
    [lblLocDistrict setText:LOCALIZATION(@"district")];
    [lblLocCity setText:LOCALIZATION(@"city")];
    
    [txtName setPlaceholder:LOCALIZATION(@"name")];
    [txtDiscription setPlaceholder:LOCALIZATION(@"description")];
    [txtPhoneNumber setPlaceholder:LOCALIZATION(@"phone_no")];
    
    [btnSave setTitle:LOCALIZATION(@"save") forState:UIControlStateNormal];
    [btnCancel setTitle:LOCALIZATION(@"cancel") forState:UIControlStateNormal];
    [btnPick setTitle:LOCALIZATION(@"Pick") forState:UIControlStateNormal];
    
    
    [lblImageUploadDescription setText:LOCALIZATION(@"image_upload_description")];

    
    /*
    lblTop.text = [NSString stringWithFormat:NSLocalizedString(@"add_attraction", nil)];
    //lblAddress.text = [NSString stringWithFormat:NSLocalizedString(@"Address", nil)];
    lblLocType.text = [NSString stringWithFormat:NSLocalizedString(@"type", nil)];
    lblLocCountry.text = [NSString stringWithFormat:NSLocalizedString(@"country", nil)];
    lblLocDistrict.text = [NSString stringWithFormat:NSLocalizedString(@"district", nil)];
    lblLocCity.text = [NSString stringWithFormat:NSLocalizedString(@"city", nil)];
    
    txtName.placeholder = [NSString stringWithFormat:NSLocalizedString(@"name", nil)];
    txtDiscription.placeholder = [NSString stringWithFormat:NSLocalizedString(@"description", nil)];
    txtPhoneNumber.placeholder = [NSString stringWithFormat:NSLocalizedString(@"phone_no", nil)];
    
    [btnSave setTitle:NSLocalizedString(@"save", nil) forState:UIControlStateNormal];
    [btnCancel setTitle:NSLocalizedString(@"cancel", nil) forState:UIControlStateNormal];
    [btnPick setTitle:NSLocalizedString(@"Pick", nil) forState:UIControlStateNormal];
    */
}

- (void)mapViewDidStartTileRendering:(GMSMapView *)mapView{
    NSLog(@"Map view Loaded");
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        for (UIView *object in mapView.subviews) {
            
            for (UIView *obj in object.subviews) {
                if ([obj isKindOfClass:[UIButton class]]) {
                    UIButton *button = (UIButton *)obj;
                    
                    
                    NSString *name = button.accessibilityIdentifier ;
                    if ([name containsString:@"my_location"]) {
                        NSLog(@"Found it");
                    }
                }
                
            }
        }
    });
    
    
    
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
//    [self changeMyLocationButton];
}

-(void)setNavBarItems {
    // Arun :: customizing the nav bar icons
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[ColorConstants appBrownColor]}];
    
    UIImage *buttonImage = [UIImage imageNamed:@"back.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(backClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
    
    /*
    UIBarButtonItem *pickButton = [[UIBarButtonItem alloc] initWithTitle:@"Pick" style:UIBarButtonItemStylePlain target:self action:@selector(pickClicked)];
    pickButton.tintColor = [UIColor redColor];
    self.navigationItem.rightBarButtonItem = pickButton;
    */
}

-(void)backClicked {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:LocationName];
    selectedLocationName = @"";
    [self.navigationController popViewControllerAnimated:true];
}

-(void)pickClicked {
    if (selectedLocationName.length > 0) {
        [KGModalWrapper hideView];
        [self.navigationController popViewControllerAnimated:true];
    } else {
        [Utils showAlertWithMessage:@"Pick a proper location"];
    }
}

- (void)mapView:(GMSMapView *)map didTapAtCoordinate:(CLLocationCoordinate2D)coord {
//    return;
   
    if (isAddAttrProcess) {
         [mapView clear];
        GMSMarker *finishMarker = [[GMSMarker alloc] init];
        finishMarker.title = @"Add Attraction";
        finishMarker.icon = [UIImage imageNamed:(@"yellow_marker.png")];
        finishMarker.position = coord;
        finishMarker.map = mapView;
        selectedLocation = [[CLLocation alloc] initWithLatitude:coord.latitude longitude:coord.longitude];
        [self getAddress];
    } else {

    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *currentLocation = [locations lastObject];
    
    if(!isFirstTime) {

    }
    else {
        GMSCameraPosition *cameraPosition = [GMSCameraPosition cameraWithLatitude:currentLocation.coordinate.latitude
                                                                        longitude:currentLocation.coordinate.longitude
                                                                             zoom:13.0];
        
        [mapView animateToCameraPosition:cameraPosition];
        isFirstTime = NO;
    }
}


-(void)getCountries {
    [SVProgressHUD showWithStatus:@"Fetching Place"];

    [[PlayTripManager Instance] getAllCountriesDataWithBlock:^(id result, NSString *error) {
        if (!error) {
            
            countriesArray = [[NSMutableArray alloc] init];
            
            NSDictionary *dataDict = (NSDictionary *)result;
            NSArray *coutriesDicts = [dataDict objectForKey:@"data"];
            
            for (NSDictionary *countryDict in coutriesDicts) {
                
                NSError *error;
                
                Country *country = [[Country alloc] initWithDictionary:countryDict error:&error];
                if (!error) {
                    [countriesArray addObject:country];
                }
                else{
                    NSLog(@"ERROR AT Country JSONMODEL = %@" , error.description);
                }
            }
            
            countriesArray = [[countriesArray sortedArrayUsingComparator:^NSComparisonResult(Country *a, Country *b) {
                return [a.name compare:b.name];
            }] mutableCopy];
            
        }
        else{
            [SVProgressHUD dismiss];
            [Utils showAlertWithMessage:@"Could Not Detect Your Location"];
        }
        
        [self getAddress];
        
        
    }];
}


-(void)getAddress {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:LocationName];
    self.title = @"";
    selectedLocationName = @"";
    Account * account = [AccountManager Instance].activeAccount;

    
    [Utils getPlaceFromLocation:selectedLocation complationBlock:^(CLPlacemark *place) {
        if (place) {
            
            NSLog(@"Country Name = %@" , place.country);
            
            NSString *name;
            
            if (!place.postalCode) {
                if ([place.name isEqualToString:place.locality]) {
                    name = [NSString stringWithFormat:@"%@", place.name];
                }
                else{
                    name = [NSString stringWithFormat:@"%@, %@", place.name, place.locality];
                }
            }
            else{
                if ([place.name isEqualToString:place.locality]) {
                    name = [NSString stringWithFormat:@"%@, %@", place.name, place.postalCode];
                }
                else{
                    name = [NSString stringWithFormat:@"%@, %@ %@", place.name, place.postalCode, place.locality];
                }
            }
            
            selectedLocationName = name;
            //self.title = name;
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:selectedLocationName forKey:LocationName];
            
            for (Country *country in countriesArray) {
                
                //Obtains the country code, that is the same whatever language you are working with (US, ES, IT ...)
                NSString *countryCode = [place ISOcountryCode];
                if (!countryCode) {
                    [Utils showAlertWithMessage:@"Could Not Detect Your Country"];
                    [SVProgressHUD dismiss];
                    return ;
                }
                //Obtains a locale identifier. This will handle every language
                //NSString *identifier = [NSLocale localeIdentifierFromComponents: [NSDictionary dictionaryWithObject: countryCode forKey: NSLocaleCountryCode]];
                //Obtains the country name from the locale BUT IN ENGLISH (you can set it as "en_UK" also)
                
                
                if ([countryCode isEqualToString:country.iso]) {
                    if([account.language  isEqualToString: @"sc"]){
                        lblCountry.text = country.name_sc;
                    }else if([account.language  isEqualToString: @"tc"]){
                        lblCountry.text = country.name_tc;
                    }
                    else{
                        lblCountry.text = country.name;
                    }
                    
                    selectedCountry = country;
                    [self getDistrictsOfTheCountry:country];
                    
                    break;
                }
                
                /*
                
                NSString *countryName = [[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] displayNameForKey: NSLocaleIdentifier value:identifier];
                if ([[countryName lowercaseString] containsString:@"hong kong"]) {
                    if ([[country.name lowercaseString] containsString:@"hong kong"]) {
                        //lblCountry.text = country.name;
                        
                        if([account.language  isEqualToString: @"sc"]){
                            lblCountry.text = country.name_sc;
                        }else if([account.language  isEqualToString: @"tc"]){
                            lblCountry.text = country.name_tc;
                        }
                        else{
                            lblCountry.text = country.name;
                        }
                        
                        selectedCountry = country;
                        [self getDistrictsOfTheCountry:country];
                        
                        break;
                    }
                }
                
                if ([[countryName lowercaseString] isEqualToString:[country.name lowercaseString]]) {
                    NSLog(@"SELECTED COUNTRY IS %@" , country.name);
                    
                    if([account.language  isEqualToString: @"sc"]){
                        lblCountry.text = country.name_sc;
                    }else if([account.language  isEqualToString: @"tc"]){
                        lblCountry.text = country.name_tc;
                    }
                    else{
                        lblCountry.text = country.name;
                    }                    selectedCountry = country;
                    [self getDistrictsOfTheCountry:country];
                    
                    break;
                */
            }
            [SVProgressHUD dismiss];

        }
        else{
            [SVProgressHUD dismiss];
        }
    }];
}

-(void)getDistrictsOfTheCountry :(Country *)country {
    [SVProgressHUD dismiss];
    [SVProgressHUD showWithStatus:@""];

    [[PlayTripManager Instance] getAllDistrictsOdCountryId:[NSString stringWithFormat:@"ob-%@",country._id] DataWithBlock:^(id result, NSString *error) {
        if (!error) {
            districtsArray = [[NSMutableArray alloc] init];
            
            NSDictionary *dataDict = (NSDictionary *)result;
            NSArray *districtsDicts = [dataDict objectForKey:@"data"];
            NSLog(@"Districts Count is %lu" , (unsigned long)districtsDicts.count);
            
            for (NSDictionary *districtDict in districtsDicts) {
                NSError *error;
                DistrictModel *district = [[DistrictModel alloc] initWithDictionary:districtDict error:&error];
                
                if (!error) {
                    if ([district.country isEqualToString:country._id]) {
                        [districtsArray addObject:district];
                    }
                }
                else{
                    NSLog(@"ERROR AT District JSONMODEL = %@" , error.description);
                }
            }
            districtsArray = [[districtsArray sortedArrayUsingComparator:^NSComparisonResult(DistrictModel *a, DistrictModel *b) {
                return [a.name compare:b.name];
            }] mutableCopy];
            
            [SVProgressHUD dismiss];

            
        }
        else{
            NSLog(@"ERROR GOTTEN = %@", error);
            [SVProgressHUD dismiss];
        }
    }];
}

-(void)getCitiesOfTheDistrict :(DistrictModel *)district {
    [SVProgressHUD dismiss];
    [SVProgressHUD showWithStatus:@"Getting Cities"];
    
    [[PlayTripManager Instance] getAllCitiesOfDistrictId:[NSString stringWithFormat:@"%@",district._id] DataWithBlock:^(id result, NSString *error) {
        if (!error) {
            citiesArray = [[NSMutableArray alloc] init];
            
            NSDictionary *dataDict = (NSDictionary *)result;
            NSArray *citiesDicts = [dataDict objectForKey:@"data"];
            
            NSLog(@"Cities are %lu",(unsigned long)citiesDicts.count);
            
            for (NSDictionary *cityDict in citiesDicts) {
                NSError *error;
                CityModel *city = [[CityModel alloc] initWithDictionary:cityDict error:&error];
                
                if (!error) {
                    if ([city.district isEqualToString:district._id]) {
                        [citiesArray addObject:city];
                    }
                }
                else{
                    NSLog(@"ERROR AT City JSONMODEL = %@" , error.description);
                }
            }
            
            citiesArray = [[citiesArray sortedArrayUsingComparator:^NSComparisonResult(CityModel *a, CityModel *b) {
                return [a.name compare:b.name];
            }] mutableCopy];
            [SVProgressHUD dismiss];
            
            
        }
        else{
            NSLog(@"ERROR GOTTEN = %@", error);
            [SVProgressHUD dismiss];
        }
    }];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if([keyPath isEqualToString:@"myLocation"]) {
        CLLocation *location = [object myLocation];
        target = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        if (isallocated == NO) {
            [self getAttractions];
            isallocated = YES;
        }
    }
}

-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {
    
    if (isAddAttrProcess) {
        return NO;
    } else {
        [KGModalWrapper showWithContentView:viewDetail];
        viewDetail.layer.borderColor = [ColorConstants appBrownColor].CGColor;
        
        for (Attractions *a in attrArray) {
            if ([marker.title isEqualToString:a.name]) {
                selectedAttractionId = a.entity_id;
                selectedAttraction = a;
                lblMainDetail.text = a.name;
                self.title = a.name;
                NSString *selectedCover;
                for (AttractionModel *attract in self.allAttractions) {
                    if ([a.entity_id isEqualToString:attract._id]) {
                        selectedCover = attract.cover_photo_url;
                        break;
                    }
                }
                
                [imgDetail sd_setShowActivityIndicatorView:YES];
                [imgDetail sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
                [imgDetail sd_setImageWithURL:[NSURL URLWithString:selectedCover]
                                   placeholderImage:[UIImage imageNamed:@"def_city_img.png"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                                       if (!error) {
                                           imgDetail.image = image;
                                       }
                                       
                                   }];
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:selectedAttractionId forKey:LocationId];
                selectedLocationName = a.name;
                NSString * fullStr = [NSString stringWithFormat:@"%@\n%@", a.address, a.phone_no];
                lblDescDetail.text = fullStr;
                break;
            }
        }
        return NO;
    }
}

+ (void)removeGMSBlockingGestureRecognizerFromMapView:(GMSMapView *)mapView {
    if([mapView.settings respondsToSelector:@selector(consumesGesturesInView)]) {
        mapView.settings.consumesGesturesInView = NO;
    } else {
        for (id gestureRecognizer in mapView.gestureRecognizers)  {
            if (![gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]]) {
                [mapView removeGestureRecognizer:gestureRecognizer];
            }
        }
    }
}

-(void)getAttractions {
    categoryArray = [NSMutableArray new];
    
    
    attrArray = [NSMutableArray new];
    
    
    [[PlayTripManager Instance] getAttractionsListWithBlock:^(id result, NSString *error) {
        
        NSLog(@"%@",result);
        
    }];
    
    
    
    
    
    
    
    
    attrArray = [Attractions getAll];
    
    
    
    for (Attractions *a in attrArray) {
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
        GMSMarker *finishMarker = [[GMSMarker alloc] init];
        finishMarker.title = a.name;
        finishMarker.icon = [UIImage imageNamed:(@"aaa.png")];
        finishMarker.position = CLLocationCoordinate2DMake(a.loc_lat.doubleValue, a.loc_long.doubleValue);
        finishMarker.map = mapView;
        bounds = [bounds includingCoordinate:finishMarker.position];
        
        if (newAddedAttr) {
            if ([a.entity_id isEqualToString:newAddedAttr]) {
                //[mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:0.0f]];
                
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:a.entity_id forKey:LocationId];

            }
        } else {
            //[mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:0.0f]];
        }
    }
    [self getCatApi];

}

-(void)getCatApi {
    [[PlayTripManager Instance]loadCategory:^(id result, NSString *error) {
        if(!error){
            
            if(result){
                categoryArrayWithLanguages = [[NSMutableArray alloc] init];
                for (NSDictionary *categoryDict in result) {
                    NSError *error;
                    CategoryModel *category = [[CategoryModel alloc] initWithDictionary:categoryDict error:&error];
                    if (!error) {
                        [categoryArrayWithLanguages addObject:category];
                    }
                }
            }
            categoryArray = [Categori getAll];
            [self myLocationAction:nil];
        }
    }];
}

- (IBAction)addAttrClicked:(id)sender {
    isAddAttrProcess = YES;
    
    lblTopMain =  [Utils roundCornersOnView:lblTopMain onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];
    viewType.layer.borderColor = [ColorConstants appBrownColor].CGColor;
    viewType.layer.cornerRadius = 10;
    viewType.layer.borderWidth = 1.0f;
    
    
    viewCountry.layer.borderColor = [ColorConstants appBrownColor].CGColor;
    viewCountry.layer.cornerRadius = 10;
    viewCountry.layer.borderWidth = 1.0f;
    
    
    viewCity.layer.borderColor = [ColorConstants appBrownColor].CGColor;
    viewCity.layer.cornerRadius = 10;
    viewCity.layer.borderWidth = 1.0f;
    
    
    viewDistrict.layer.borderColor = [ColorConstants appBrownColor].CGColor;
    viewDistrict.layer.cornerRadius = 10;
    viewDistrict.layer.borderWidth = 1.0f;
    
    
    
    
    
    
    
    [mapView clear];
    CGPoint point = mapView.center;
    GMSCameraUpdate *camera =[GMSCameraUpdate setTarget:[mapView.projection coordinateForPoint:point]];
    [mapView animateWithCameraUpdate:camera];
    CLLocationCoordinate2D coord = [mapView.projection coordinateForPoint:point];
    GMSMarker *finishMarker = [[GMSMarker alloc] init];
    finishMarker.title = @"Add Attraction";
    finishMarker.icon = [UIImage imageNamed:(@"yellow_marker.png")];
    finishMarker.position = coord;
    finishMarker.map = mapView;
    
    selectedLocation = [[CLLocation alloc] initWithLatitude:coord.latitude longitude:coord.longitude];
    [self getCountries];
    
    
    UIColor *color = [ColorConstants appYellowColor];
    UIButton *nxtButton = [UIButton buttonWithType:UIButtonTypeSystem];
    nxtButton.frame = CGRectMake(0, 0, 55, 25);
    [nxtButton setTitle:LOCALIZATION(@"next_capital") forState:UIControlStateNormal];
    [nxtButton setTitleColor:[UIColor colorWithRed:(102.0/255.0) green:(74.0/255.0) blue:(61.0/255.0) alpha:1.0] forState:UIControlStateNormal];
    nxtButton.backgroundColor = color;
    
    nxtButton.layer.cornerRadius = 5; // this value vary as per your desire
    nxtButton.clipsToBounds = YES;
    
    [nxtButton addTarget:self action:@selector(addAttraction) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:nxtButton];
    
    
//    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithTitle:@"NEXT" style:UIBarButtonItemStyleDone target:self action:@selector(addAttraction)];
//    addButton.tintColor = [UIColor blackColor];
//    self.navigationItem.rightBarButtonItem = addButton;
}

- (IBAction)cancelClicked:(id)sender {
    [KGModalWrapper hideView];
}

-(UIImage *)imageResize :(UIImage*)img andResizeTo:(CGSize)newSize
{
    CGFloat scale = [[UIScreen mainScreen]scale];
    /*You can remove the below comment if you dont want to scale the image in retina   device .Dont forget to comment UIGraphicsBeginImageContextWithOptions*/
    //UIGraphicsBeginImageContext(newSize);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, scale);
    [img drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (UIImage *)imageByCroppingImage:(UIImage *)image toSize:(CGSize)size
{
    double newCropWidth, newCropHeight;
    
    //=== To crop more efficently =====//
    if(image.size.width < image.size.height){
        if (image.size.width < size.width) {
            newCropWidth = size.width;
        }
        else {
            newCropWidth = image.size.width;
        }
        newCropHeight = (newCropWidth * size.height)/size.width;
    } else {
        if (image.size.height < size.height) {
            newCropHeight = size.height;
        }
        else {
            newCropHeight = image.size.height;
        }
        newCropWidth = (newCropHeight * size.width)/size.height;
    }
    //==============================//
    
    double x = image.size.width/2.0 - newCropWidth/2.0;
    double y = image.size.height/2.0 - newCropHeight/2.0;
    
    CGRect cropRect = CGRectMake(x, y, newCropWidth, newCropHeight);
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], cropRect);
    
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    return cropped;
}



- (IBAction)nextClicked:(id)sender {
    
    previousSelectedCountry = selectedCountry;

    
    NSLog(@"%@",[NSString stringWithFormat:@"%f", selectedLocation.coordinate.longitude]);
    
    if (txtName.text.length <= 0) {
        [Utils showAlertWithMessage:@"Please add name"];
    } else if (!selectedCategory) {
        [Utils showAlertWithMessage:@"Please select a type"];
    }
    else if (!selectedCountry) {
        [Utils showAlertWithMessage:@"Please add a Country"];
    }
    else if (!selectedDistrict) {
        [Utils showAlertWithMessage:@"Please add a District"];
    }
    else if (!selectedCity) {
        [Utils showAlertWithMessage:@"Please add a City"];
    }
    else {

        
        //TODO ISSUE HERE
        
       CGSize size = CGSizeMake(coverImage.image.size.width, coverImage.image.size.height);
        if (coverImage.image.size.width < coverImage.image.size.height) {
            size = CGSizeMake(coverImage.image.size.width, coverImage.image.size.width);
        }
        else{
            size = CGSizeMake(coverImage.image.size.height, coverImage.image.size.height);
        }
        
        UIImage *image = [self imageByCroppingImage:coverImage.image toSize:size];
        NSData *imageData = UIImagePNGRepresentation(image);
        if (imageData) {
            NSLog(@"DATA HEERERE");
            NSString *fileName = [[[NSProcessInfo processInfo] globallyUniqueString] stringByAppendingString:@".jpg"];
            NSString *filePath = [[NSTemporaryDirectory() stringByAppendingPathComponent:@""] stringByAppendingPathComponent:fileName];
            NSError *error;
            BOOL success = [imageData writeToFile:filePath options:0 error:&error];
            if (!success) {
                NSLog(@"writeToFile failed with error %@", error);
                [Utils showAlertWithMessage:@"File Writting Issue"];
                [SVProgressHUD dismiss];
                return;
            }
            
            
            AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
            AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
            uploadRequest.bucket = @"playtrip.medias";
            uploadRequest.key = fileName;
            uploadRequest.body = [NSURL fileURLWithPath:filePath];
            uploadRequest.contentType = @"image/jpg";
            //[uploadRequest setACL:AWSS3ObjectCannedACLPublicRead];
            
            [SVProgressHUD showProgress:0.0 status:@"Uploading..."];

            
            [[transferManager upload:uploadRequest] continueWithBlock:^id(AWSTask *task) {
                // Do something with the response
                if (task.error) {
                    NSLog(@"ERROR GOTTEN = %@" , task.error.description);
                    [SVProgressHUD dismiss];
                    
                }
                else{//https://s3-ap-southeast-1.amazonaws.com/playtrip.medias/abc1497457136113.jpg
                    NSLog(@"UPLOADED = https://playtrip.medias.s3.amazonaws.com/%@",fileName);
                    
                    NSString *photoUrl = [NSString stringWithFormat:@"https://s3-ap-southeast-1.amazonaws.com/playtrip.medias/%@",fileName];
                    
                    
                    NSMutableArray * locArray = [NSMutableArray new];
                    NSString * lng = [NSString stringWithFormat:@"%f", selectedLocation.coordinate.longitude];
                    NSString * lat = [NSString stringWithFormat:@"%f", selectedLocation.coordinate.latitude];
                    [locArray addObject:lat];
                    [locArray addObject:lng];
                    
                    
                    NSString *coutryId = [NSString stringWithFormat:@"%@", selectedCountry._id];
                    NSString *distrcitId = [NSString stringWithFormat:@"%@", selectedDistrict._id];
                    NSString *cityId = [NSString stringWithFormat:@"%@", selectedCity._id];
                    
                    Account * account = [AccountManager Instance].activeAccount;
                    
                    [[PlayTripManager Instance] addAttractionWithName:txtName.text andCatId:selectedCategory.entity_id AndAddress:addressText.text andLocArray:locArray andDescription:txtDiscription.text andPhoneNo:txtPhoneNumber.text andPhotoUrl:photoUrl andCountryId:coutryId andDistrictId:distrcitId andCityId:cityId forUserId:account.userId WithBlockNew:^(id result, NSString *error) {
                        
                        [SVProgressHUD dismiss];
                        NSLog(@"%@",result);
                        
                        [mapView clear];
                        self.title = @"";
                        NSDictionary *data = [result valueForKey:@"data"];
                        NSString * aStr = [data valueForKey:@"_id"];
                        
                        
                        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                        [defaults setObject:aStr forKey:LocationId];
                        
                        [defaults synchronize];
                        
                        newAddedAttr = aStr;
                        [SVProgressHUD dismiss];
                        
                        [self setNavBarItems];
                        [KGModalWrapper hideView];
                        
                        [[PlayTripManager Instance] getAttractionsListWithBlock:^(id result, NSString *error) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                //[self getAttractions];
                                @try{
                                    [mapView removeObserver:self forKeyPath:@"myLocation"];
                                }@catch(id anException){
                                    //do nothing, obviously it wasn't attached because an exception was thrown
                                }
                                [self.navigationController popViewControllerAnimated:YES];
                            });
                        }];
                    }];
                    
                    
                    
                    
                }
                
                return nil;
            }];
            uploadRequest.uploadProgress =  ^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend){
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                   // [SVProgressHUD showProgress:1.0 status:@"Uploading..."];

                    float progress = ((float)totalBytesSent/(float)totalBytesExpectedToSend) ;
                    [SVProgressHUD showProgress:progress status:@"Uploading..."];

                    NSLog(@"%lld %lld %f",totalBytesSent , totalBytesExpectedToSend , progress);
                    
                    // upload progress
                });
            };
            
        }
    }
    
    
    
    
    
    
    /*
    if (txtName.text.length <= 0) {
        [Utils showAlertWithMessage:@"Please add name"];
    } else if (!selectedCategory) {
        [Utils showAlertWithMessage:@"Please select a type"];
    } else {
        isAddAttrProcess = NO;
        NSMutableArray * localArray = [NSMutableArray new];
        [localArray addObject:selectedCategory.entity_id];
        NSMutableArray * locArray = [NSMutableArray new];
        NSString * lng = [NSString stringWithFormat:@"%f", selectedLocation.coordinate.longitude];
        NSString * lat = [NSString stringWithFormat:@"%f", selectedLocation.coordinate.latitude];
        [locArray addObject:lng];
        [locArray addObject:lat];
        Account * account = [AccountManager Instance].activeAccount;
        [SVProgressHUD showWithStatus:@"Please Wait..."];
        [[PlayTripManager Instance] addAttractionWithName:txtName.text andCatId:locArray AndAddress:self.title andLocArray:locArray forUserId:account.userId WithBlockNew:^(id result, NSString *error) {
            [mapView clear];
            self.title = @"";
            NSDictionary *data = [result valueForKey:@"data"];
            NSString * aStr = [data valueForKey:@"_id"];
            
            
            Attractions *newAttr = [[Attractions alloc] init];
            newAttr.address = [data valueForKey:@"address"];
            
            newAttr.address = [data valueForKey:@"address"];
            newAttr.bookmark_users_string = [data valueForKey:@"bookmark_users"];
            newAttr.create_date = [data valueForKey:@"create_date"];
            newAttr.day = [data valueForKey:@"address"];
            newAttr.detail = [data valueForKey:@"detail"];
            newAttr.entity_id = [data valueForKey:@"_id"];
            newAttr.has_remark = @0;
            NSArray *remarks = [data valueForKey:@"remarks"];
            
            if (remarks.count
                ) {
                newAttr.has_remark = @1;
            }
            
            
            newAttr.address = [data valueForKey:@"address"];
            newAttr.address = [data valueForKey:@"address"];
            newAttr.address = [data valueForKey:@"address"];
            newAttr.address = [data valueForKey:@"address"];
            newAttr.address = [data valueForKey:@"address"];

     
             + (NSString *)address;
             + (NSString *)bookmark_users_string;
             + (NSString *)create_date;
             + (NSString *)day;
             + (NSString *)detail;
             + (NSString *)entity_id;
             + (NSString *)has_remark;
             + (NSString *)last_updated;
             + (NSString *)loc_lat;
             + (NSString *)loc_long;
             + (NSString *)name;
             + (NSString *)phone_no;
             + (NSString *)plan;
             + (NSString *)total_bookmark;
             + (NSString *)total_share;
             + (NSString *)total_view;
             + (NSString *)web_url;
     
            
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:aStr forKey:LocationId];
            
            [defaults synchronize];
            
            newAddedAttr = aStr;
            [SVProgressHUD dismiss];
            
            [self setNavBarItems];
            [KGModalWrapper hideView];

            [[PlayTripManager Instance] getAttractionsListWithDistance:@"50" WithLattitude:target.latitude WithLongitude:target.longitude WithBlock:^(id result, NSString *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self getAttractions];
                    [self.navigationController popViewControllerAnimated:YES];
                });
            }];
            
        }];
    
    }
     */
}

-(void)moveBackToPrevious {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)selectTypeClicked:(id)sender {
    NSMutableArray * localArray = [NSMutableArray new];
    
    Account * account = [AccountManager Instance].activeAccount;
    
    for (Categori * cat in categoryArray) {
        CategoryModel *selectedCategoryFound;
        
        for (CategoryModel *category in categoryArrayWithLanguages) {
            if ([cat.entity_id isEqualToString:category._id]) {
                selectedCategoryFound = category;
                break;
            }
        }
        
        if([account.language  isEqualToString: @"sc"]){
            [localArray addObject:selectedCategoryFound.name_sc];

            
        }else if([account.language  isEqualToString: @"tc"]){
            [localArray addObject:selectedCategoryFound.name_tc];
        }
        else{
            [localArray addObject:cat.name];
        }
    }
    
    
    ActionSheetStringPicker* sp = [[ActionSheetStringPicker alloc] initWithTitle:LOCALIZATION(@"select_plans") rows:localArray initialSelection:0  doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        //        [_btnTour setTitle:[planName objectAtIndex:selectedIndex] forState:UIControlStateNormal];
        lblType.text =[localArray objectAtIndex:selectedIndex] ;
        selectedCategory = [categoryArray objectAtIndex:selectedIndex];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    } origin:sender];
    
    
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setTitle:LOCALIZATION(@"cancel") forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [cancelButton setFrame:CGRectMake(0, 0, 70, 50)];
    [sp setCancelButton:[[UIBarButtonItem alloc] initWithCustomView:cancelButton]];
    UIButton *doneButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [doneButton setTitle:LOCALIZATION(@"done") forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [doneButton setFrame:CGRectMake(0, 0, 70, 50)];
    [sp setDoneButton:[[UIBarButtonItem alloc] initWithCustomView:doneButton]];
    [sp showActionSheetPicker];

    
}

- (IBAction)myLocationAction:(UIButton *)sender {
    CLLocation *location = mapView.myLocation;
    if (location) {
        [mapView animateToLocation:location.coordinate];
    }
}

- (IBAction)selectCountryClicked:(id)sender {
    Account * account = [AccountManager Instance].activeAccount;
    NSMutableArray * localArray = [NSMutableArray new];
    for (Country * country in countriesArray) {
        
        if([account.language  isEqualToString: @"sc"]){
            [localArray addObject:country.name_sc];
        }else if([account.language  isEqualToString: @"tc"]){
            [localArray addObject:country.name_tc];
        }
        else{
            [localArray addObject:country.name];
        }
        
        
    }
    
    ActionSheetStringPicker* sp = [[ActionSheetStringPicker alloc] initWithTitle:LOCALIZATION(@"select_country") rows:localArray initialSelection:0  doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        //        [_btnTour setTitle:[planName objectAtIndex:selectedIndex] forState:UIControlStateNormal];
        lblCountry.text =[localArray objectAtIndex:selectedIndex] ;
        selectedCountry = [countriesArray objectAtIndex:selectedIndex];
        [self getDistrictsOfTheCountry:selectedCountry];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    } origin:sender];
    
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setTitle:LOCALIZATION(@"cancel") forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [cancelButton setFrame:CGRectMake(0, 0, 70, 50)];
    [sp setCancelButton:[[UIBarButtonItem alloc] initWithCustomView:cancelButton]];
    UIButton *doneButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [doneButton setTitle:LOCALIZATION(@"done") forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [doneButton setFrame:CGRectMake(0, 0, 70, 50)];
    [sp setDoneButton:[[UIBarButtonItem alloc] initWithCustomView:doneButton]];
    [sp showActionSheetPicker];
    
    
    
    
}

- (IBAction)selectCityClicked:(id)sender {
    NSMutableArray * localArray = [NSMutableArray new];
    for (CityModel * city in citiesArray) {
        [localArray addObject:city.name];
    }
    
    
    ActionSheetStringPicker* sp = [[ActionSheetStringPicker alloc] initWithTitle:LOCALIZATION(@"select_city") rows:localArray initialSelection:0  doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        //        [_btnTour setTitle:[planName objectAtIndex:selectedIndex] forState:UIControlStateNormal];
        lblCity.text =[localArray objectAtIndex:selectedIndex] ;
        selectedCity = [citiesArray objectAtIndex:selectedIndex];

    } cancelBlock:^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    } origin:sender];
    
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setTitle:LOCALIZATION(@"cancel") forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [cancelButton setFrame:CGRectMake(0, 0, 70, 50)];
    [sp setCancelButton:[[UIBarButtonItem alloc] initWithCustomView:cancelButton]];
    UIButton *doneButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [doneButton setTitle:LOCALIZATION(@"done") forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [doneButton setFrame:CGRectMake(0, 0, 70, 50)];
    [sp setDoneButton:[[UIBarButtonItem alloc] initWithCustomView:doneButton]];
    [sp showActionSheetPicker];
}

- (IBAction)selectDistrictClicked:(id)sender {
    NSMutableArray * localArray = [NSMutableArray new];
    for (DistrictModel * district in districtsArray) {
        [localArray addObject:district.name];
    }
    
    
    ActionSheetStringPicker* sp = [[ActionSheetStringPicker alloc] initWithTitle:LOCALIZATION(@"select_district") rows:localArray initialSelection:0  doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        //        [_btnTour setTitle:[planName objectAtIndex:selectedIndex] forState:UIControlStateNormal];
        lblDistrict.text =[localArray objectAtIndex:selectedIndex] ;
        selectedDistrict = [districtsArray objectAtIndex:selectedIndex];
        
        [self getCitiesOfTheDistrict:selectedDistrict];
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    } origin:sender];
    
    
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setTitle:LOCALIZATION(@"cancel") forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [cancelButton setFrame:CGRectMake(0, 0, 70, 50)];
    [sp setCancelButton:[[UIBarButtonItem alloc] initWithCustomView:cancelButton]];
    UIButton *doneButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [doneButton setTitle:LOCALIZATION(@"done") forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [doneButton setFrame:CGRectMake(0, 0, 70, 50)];
    [sp setDoneButton:[[UIBarButtonItem alloc] initWithCustomView:doneButton]];
    [sp showActionSheetPicker];
}

- (IBAction)coverCameraAction:(id)sender {
    //[KGModalWrapper hideView];
    [[KGModal sharedInstance] hideAnimated:YES withCompletionBlock:^{
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
        
        UIAlertAction *openCamrea = [UIAlertAction actionWithTitle:@"Open Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
                [Utils showAlertWithMessage:@"Device has no camera"];
            } else {
                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                picker.allowsEditing = YES;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:picker animated:YES completion:NULL];
            }
        }];
        
        UIAlertAction *openGallery = [UIAlertAction actionWithTitle:@"Open Gallery"  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:picker animated:YES completion:NULL];
        }];
        
        UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:openCamrea];
        [alert addAction:openGallery];
        [alert addAction:cancel];
        
        [alert setModalPresentationStyle:UIModalPresentationPopover];
        //    UIPopoverPresentationController *popPresenter = [alert popoverPresentationController];
        //    popPresenter.sourceView = img;
        //    popPresenter.sourceRect =img.bounds;
        [self presentViewController:alert animated:YES completion:nil];
    }];

    
    
    
   

    
    
    
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    //You can retrieve the actual UIImage
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    //Or you can get the image url from AssetsLibrary
    NSURL *path = [info valueForKey:UIImagePickerControllerReferenceURL];
    
    if (image) {
        coverImage.image = image;
    }
    
    [picker dismissViewControllerAnimated:YES completion:^{
        [KGModalWrapper showWithContentView:addAttractionView];
    }];
}

-(void)addAttraction {
    
    if (![previousSelectedCountry._id isEqualToString:selectedCountry._id]) {
        txtName.text = @"";
        lblType.text = [NSString stringWithFormat:@"(%@)", LOCALIZATION(@"choose_type")];
        lblCity.text = [NSString stringWithFormat:@"(%@)", LOCALIZATION(@"choose_city")];
        lblDistrict.text = [NSString stringWithFormat:@"(%@)", LOCALIZATION(@"choose_district")];
        selectedCategory = nil;
    }
  
    addressText.text = selectedLocationName;
    
    
    [KGModalWrapper showWithContentView:addAttractionView];
}

-(void)changeMyLocationButton {
    mapView.settings.myLocationButton = YES;
    mapView.myLocationEnabled = YES;
    mapView.padding = UIEdgeInsetsMake(0, 0, 50, 0);
}

- (void)dealloc {
    @try{
        [mapView removeObserver:self forKeyPath:@"myLocation"];
    }@catch(id anException){
        //do nothing, obviously it wasn't attached because an exception was thrown
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    @try{
        [mapView removeObserver:self forKeyPath:@"myLocation"];
    }@catch(id anException){
        //do nothing, obviously it wasn't attached because an exception was thrown
    }
    
    [mapView clear];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

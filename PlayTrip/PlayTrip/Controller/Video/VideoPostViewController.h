
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <AVKit/AVPlayerViewController.h>
#import <AVKit/AVError.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <MediaPlayer/MediaPlayer.h>
#import <Foundation/Foundation.h>
#import <WebKit/WebKit.h>
#import "Attractions.h"
#import "DraftTour.h"
#import "DraftAttraction.h"
#import "DraftAttrData.h"
#import "DraftInfo.h"
#import "DraftVideoId.h"
#import "AttractionModel.h"



@interface VideoPostViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, NSURLConnectionDelegate> {


    IBOutlet UIView *myLocationView;
    IBOutlet UIButton *btnLocation;
    IBOutlet UIView *topView;
    IBOutlet UIScrollView *scrollViewMain;
    IBOutlet UIView *videoPreviewView;
    IBOutlet UITableView *tblList;
    IBOutlet UITextField *txtAttractionName;
    IBOutlet UILabel *lblAttractionName;
    IBOutlet UIView *attrBoxView;
    IBOutlet UIButton *btnPlay;
    IBOutlet UIButton *btnDeleteAttr;
    IBOutlet UIView *bottomView;
    IBOutlet UITextField *txtDescription;
    IBOutlet UITextField *txtTime;
    IBOutlet UITextField *txtDate;
    IBOutlet UIProgressView *progressLine;
    IBOutlet UILabel *lblProgressTime;
    IBOutlet UIButton *btnPostAttractionVideo;
    IBOutlet UIView *viewAttributes;
    
    IBOutlet UILabel *lblAttraction;
    IBOutlet UILabel *lblDescription;
    IBOutlet UILabel *lblTime;
    IBOutlet UILabel *lblDate;
    
    
    IBOutlet UITextField *txtEndTime;
    IBOutlet UILabel *lblEndtime;
    
    
    
    IBOutlet NSLayoutConstraint *bottomTextFieldLineConstraint;
    
    IBOutlet NSLayoutConstraint *bottomTextFieldLineConstraint2;
    
    
    
    NSURL * vidUrl;
    NSURL * waterMarkedUrl;
    
    AVPlayer *avPlayer;
    AVPlayerLayer *videoLayer;

    NSMutableArray *arrAttr;
    NSTimer * timerCountDown;
    int countDownTime;

    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLLocationCoordinate2D target;

    BOOL isallocated;
    
    NSString * currentLocationName;
    NSString * pickedLocation;
    NSString * searchTextGlobal;

    Attractions * selectedAttraction;
    DraftTour * dTour;
    DraftAttrData * selectedAttractionData;
    
    
    
    NSMutableArray * allAttractions;
    NSMutableArray * allAttractionsOrg;
    AttractionModel * selectedAttractionModel;
    
    
    
    
    __weak IBOutlet UIButton *outsideBtn;

    IBOutlet UIView *popView;
    __weak IBOutlet UILabel *fontLabel;
    __weak IBOutlet UILabel *hkfontLabel;
    
    
    
    UIImage *videoFirstFrameImage;
}

+(VideoPostViewController *)initViewControllerWithURL:(NSURL *)vidUrl;

@property (strong, nonatomic) MPMoviePlayerController *videoController;

- (IBAction)postAttractionClicked:(id)sender;
- (IBAction)backClicked:(id)sender;
- (IBAction)closeClicked:(id)sender;
- (IBAction)playClicked:(id)sender;
- (IBAction)addAttractionClicked:(id)sender;
- (IBAction)deleteAttractionClicked:(id)sender;
- (IBAction)outSideAction:(UIButton *)sender;


@end

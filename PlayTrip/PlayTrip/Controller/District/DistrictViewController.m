
#import "DistrictViewController.h"
#import "DistrictCollectionCell.h"
#import "DistrictDetailViewController.h"
#import "District.h"
#import "URLSchema.h"
#import "ColorConstants.h"
#import "AppDelegate.h"
#import "Utils.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import "PlanModel.h"
#import <AWSS3/AWSS3.h>
#import "AccountManager.h"
#import "Localisator.h"

@interface DistrictViewController ()

@end

@implementation DistrictViewController

+(DistrictViewController *)initViewController {
    DistrictViewController * controller = [[DistrictViewController alloc] initWithNibName:@"DistrictViewController" bundle:nil];
    controller.title = LOCALIZATION(@"country");;
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.title = LOCALIZATION(@"country");

    
    [collView registerClass:[DistrictCollectionCell class] forCellWithReuseIdentifier:@"DistrictCollectionCell"];
    
    UINib *cellNib = [UINib nibWithNibName:@"DistrictCollectionCell" bundle:nil];
    [collView registerNib:cellNib forCellWithReuseIdentifier:@"DistrictCollectionCell"];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setNavBarItems];
    if (![[AppDelegate appDelegate] isReachable]) {
        [Utils showAlertWithMessage:kCheckInternet];
        return;
    }

    //districtsArray = [District getAll];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    sortView = [PopularVideoSort initWithNib];
    sortView.frame = CGRectMake(self.view.frame.size.width - sortView.frame.size.width, 0, sortView.frame.size.width, sortView.frame.size.height);
}

-(void)setNavBarItems {
    // Arun :: customizing the nav bar icons
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[ColorConstants appBrownColor]}];
    
    UIImage *buttonImage = [UIImage imageNamed:@"back.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(backClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
    
    UIImage *mapImage = [UIImage imageNamed:@"sort_descending.png"];
    UIButton *btnRight1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight1 setImage:mapImage forState:UIControlStateNormal];
    btnRight1.frame = CGRectMake(0, 0, mapImage.size.width, mapImage.size.height);
    [btnRight1 addTarget:self action:@selector(sortClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarItem1 = [[UIBarButtonItem alloc] initWithCustomView:btnRight1];
    self.navigationItem.rightBarButtonItem = rightBarItem1;
}

-(void)backClicked {
    [self.navigationController popViewControllerAnimated:true];
}

-(void)sortClicked {    
//    if (isSortShown) {
//        isSortShown = NO;
//        [sortView removeFromSuperview];
//    } else {
//        isSortShown = YES;
//        [self.view addSubview:sortView];
//    }
}

#pragma mark -<UICollectionViewDelegate,UICollectionViewDataSource>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    //return districtsArray.count;
    //NSArray *allCountriesDictTitles = [[_allCountriesDicts allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    //return allCountriesDictTitles.count;
    return self.uniqueCountries.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    DistrictCollectionCell *cell = (DistrictCollectionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"DistrictCollectionCell" forIndexPath:indexPath];
    
    Account * account = [AccountManager Instance].activeAccount;

    
    
    LocationModel *location = self.uniqueCountries[indexPath.row];
    cell.lblName.text = location.country_data.name;
    
    cell.lblCount.text = [NSString stringWithFormat:@"(%@)",location.count];
    
    
    cell.imgMain.image = [UIImage imageNamed:@"default_country_image"];
    
    if (location.country_data.img_url) {
        [cell.imgMain sd_setImageWithURL:[NSURL URLWithString:location.country_data.img_url]
                        placeholderImage:[UIImage imageNamed:@"default_country_image"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                            if (!error) {
                                cell.imgMain.image = image;
                            }
                            else{
                                cell.imgMain.image = [UIImage imageNamed:@"default_country_image"];
                            }
                            [cell layoutIfNeeded];
                        }];
    }
    
    if([account.language  isEqualToString: @"sc"]){
        cell.lblName.text = location.country_data.name_sc;
    }else if([account.language  isEqualToString: @"tc"]){
        cell.lblName.text = location.country_data.name_tc;
    }

    
    //TODO
    
    
    
    /*
    NSString *downloadingFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"0E33586E-CD6C-4439-8211-59AEAD7E4FF2-9250-00000C6234F3DB22.jpg"];
    NSURL *downloadingFileURL = [NSURL fileURLWithPath:downloadingFilePath];
    
    AWSS3TransferManagerDownloadRequest *downloadRequest = [AWSS3TransferManagerDownloadRequest new];
    
    downloadRequest.bucket = @"playtrip.medias";
    downloadRequest.key = @"0E33586E-CD6C-4439-8211-59AEAD7E4FF2-9250-00000C6234F3DB22.jpg";
    
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];

    
    [[transferManager download:downloadRequest ] continueWithExecutor:[AWSExecutor mainThreadExecutor] withBlock:^id(AWSTask *task) {
        if (task.error){
            if ([task.error.domain isEqualToString:AWSS3TransferManagerErrorDomain]) {
                switch (task.error.code) {
                    case AWSS3TransferManagerErrorCancelled:
                    case AWSS3TransferManagerErrorPaused:
                        break;
                        
                    default:
                        NSLog(@"Error: %@", task.error);
                        break;
                }
                
            } else {
                NSLog(@"Error: %@", task.error);
            }
        }
        
        if (task.result) {
            AWSS3TransferManagerDownloadOutput *downloadOutput = task.result;
            UIImage *image = [UIImage imageWithContentsOfFile:downloadingFilePath];
            if (image) {
                NSLog(@"Image Gotten");
                cell.imgMain.image = image;
                [cell layoutIfNeeded];
            }

        }
        return nil;
    }];
    
    
    
    
    
    [cell.imgMain sd_setShowActivityIndicatorView:YES];
    [cell.imgMain sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [cell.imgMain sd_setImageWithURL:[NSURL URLWithString:@"https://s3-ap-southeast-1.amazonaws.com/playtrip.medias/abc1497457136113.jpg"]
                    placeholderImage:[UIImage imageNamed:@"def_user_img.png"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                        if (!error) {
                            cell.imgMain.image = image;
                            [cell layoutIfNeeded];
                        }
                        else{
                            cell.imgMain.url = [NSURL URLWithString:@"https://s3-ap-southeast-1.amazonaws.com/playtrip.medias/abc1497457136113.jpg"];
                        }
                    }];
    
    */
    
    for (PlanModel *plan in location.video_tour) {
        for (AttractionModel *attraction in plan.attractions) {
            for (AttractionDataModel *attractionData in attraction.attr_data) {
                NSLog(@"URL = %@" , attractionData.info.cover_photo_url);
                
                //[cell.imgMain sd_setImageWithURL:[NSURL URLWithString:attractionData.info.cover_photo_url] placeholderImage:[UIImage imageNamed:@"def_city_img.png"]];
                break;
                break;
                break;
            }
        }
    }
    
    
    
    
    /*
    NSArray *allCountriesDictTitles = [[_allCountriesDicts allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    NSArray *countriesPlans = [_allCountriesDicts objectForKey:[allCountriesDictTitles objectAtIndex:indexPath.row]];
    
    cell.lblCount.text = [NSString stringWithFormat:@"(%lu)",(unsigned long)countriesPlans.count];
    for (PlanModel *plan in countriesPlans) {
        for (AttractionModel *attraction in plan.attractions) {
            for (AttractionDataModel *attractionData in attraction.attr_data) {
                cell.lblName.text = attractionData.info.country_id.name;
                [cell.imgMain sd_setImageWithURL:[NSURL URLWithString:attractionData.info.cover_photo_url]
                                placeholderImage:[UIImage imageNamed:@"def_city_img.png"]];
                break;
                break;
                break;
            }
        }
    }
    
    
    
    
    
    District * dist = [districtsArray objectAtIndex:indexPath.row];
    cell.lblName.text = dist.name;
    cell.lblCount.text = [NSString stringWithFormat:@"(%@)",dist.plan_cnt];
    
    if(![dist.image isEqualToString:@""]){
        NSString * imgId = dist.image;
        NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
        cell.imgMain.url = [NSURL URLWithString:urlString];
    }else{
//        cell.imgMain.image = [UIImage imageNamed:@"mexico.jpg"];
    }
     */
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    LocationModel *location = self.uniqueCountries[indexPath.row];
    DistrictDetailViewController * controller = [DistrictDetailViewController initViewControllerWithLocation:location];
    [self.navigationController pushViewController:controller animated:true];
    
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((collectionView.frame.size.width/2)-8, (collectionView.frame.size.width/2)-8);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

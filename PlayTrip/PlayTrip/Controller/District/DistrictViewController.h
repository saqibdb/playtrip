
#import <UIKit/UIKit.h>
#import "MenuButtonViewController.h"
#import "PopularVideoSort.h"

@interface DistrictViewController : MenuButtonViewController {
    IBOutlet UICollectionView *collView;
    BOOL isSortShown;
    PopularVideoSort * sortView;
    NSMutableArray * districtsArray;
    

    
}
@property (nonatomic) NSDictionary *allCountriesDicts;
@property (nonatomic) NSMutableArray *uniqueCountries;

@property (nonatomic) NSString *controllerTitle;



+(DistrictViewController *)initViewController;

@end

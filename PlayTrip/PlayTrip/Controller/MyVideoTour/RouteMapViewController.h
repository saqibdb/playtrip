
#import <UIKit/UIKit.h>
#import "Plan.h"
#import "LRouteController.h"
#import "DraftTour.h"
#import "DraftAttraction.h"
#import "DraftAttrData.h"
#import "DraftInfo.h"
#import "PlanModel.h"

@interface RouteMapViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource , GMSMapViewDelegate> {
    IBOutlet GMSMapView *viewMain;
    IBOutlet UICollectionView *collViewMain;
    
    NSMutableArray * catArrayMain;
    NSMutableArray * locArray;
    
    LRouteController *_routeController;
    GMSPolyline *polyline;
    GMSMarker *markerStart;
    GMSMarker *markerFinish;
    
    DraftTour * dTour;
    
    PlanModel *plan;

}

@property(nonatomic)Plan *planMain;

+(RouteMapViewController *)initViewController;
+(RouteMapViewController *)initViewController:(Plan *)pl;
+(RouteMapViewController *)initViewControllerWithPlanModel:(PlanModel *)pl;

@end


#import "MYTDViewController.h"
#import "CreateVideoTourViewController.h"
#import "LocalVideoObject.h"
#import "VideoId.h"
#import "KGModalWrapper.h"
#import "SVProgressHUD.h"
#import "ActionSheetPicker.h"
#import "Account.h"
#import "AccountManager.h"
#import "PlayTripManager.h"
#import "AirTicketCell.h"
#import "ItineraryTableViewCell.h"
#import "HotelBookingCell.h"
#import "ColorConstants.h"
#import "TotalView.h"
#import "Attractions.h"
#import "AttractionData.h"
#import "URLSchema.h"
#import "Categori.h"
#import "Utils.h"
#import "PlaceInformationViewController.h"
#import "Info.h"
#import "MyVideoTourCell.h"
#import "VideoTourSaveCell.h"
#import "ActionSheetPicker.h"
#import "ActionSheetDatePicker.h"
#import "RouteMapViewController.h"
#import "AppDelegate.h"
#import "Account.h"
#import "AccountManager.h"

@interface MYTDViewController ()

@end

@implementation MYTDViewController

+(MYTDViewController *)initController:(Plan *)pl {
    MYTDViewController * controller = [[MYTDViewController alloc] initWithNibName:@"MYTDViewController" bundle:nil];
    controller.title = @"My Video Tour (Draft)";
    controller->planMain = pl;
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    tblView1.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self registerNib];
    [tblView1 setHidden:NO];
    attractionArray = [NSMutableArray new];
    
    attractionArray = [[planMain.attractionsSet allObjects]mutableCopy];
    
    NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"day"
                                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
    NSArray *sortedArray = [attractionArray sortedArrayUsingDescriptors:sortDescriptors];
    attractionArray = [sortedArray mutableCopy];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    [self setNavBarItems];
}

-(void)setNavBarItems {
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[ColorConstants appBrownColor]}];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backClicked)];
    self.navigationItem.leftBarButtonItem = backButton;
    self.navigationController.navigationBar.tintColor = [ColorConstants appBrownColor];
    self.navigationController.navigationBar.translucent = NO;
    UIBarButtonItem * homeButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"home.png"] style:UIBarButtonItemStylePlain target:self action:@selector(homeClicked)];
    [self.navigationItem setRightBarButtonItem:homeButton];
}

-(void)homeClicked{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)backClicked{
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)registerNib{
    UINib *celliterery = [UINib nibWithNibName:@"ItineraryTableViewCell" bundle:nil];
    [tblView1 registerNib:celliterery forCellReuseIdentifier:@"ItineraryTableViewCell"];
    UINib *cell = [UINib nibWithNibName:@"MyVideoTourCell" bundle:nil];
    [tblView1 registerNib:cell forCellReuseIdentifier:@"MyVideoTourCell"];
    UINib *cell2 = [UINib nibWithNibName:@"VideoTourSaveCell" bundle:nil];
    [tblView1 registerNib:cell2 forCellReuseIdentifier:@"VideoTourSaveCell"];
}

- (IBAction)toDatePicker:(id)sender{
//    NSString * aStr = [NSString stringWithFormat:@"%ld", (long)[sender tag]];
//    
//    NSArray * tagArray = [aStr componentsSeparatedByString:@"99999"];
//    NSString * a = [NSString stringWithFormat:@"%@",[tagArray firstObject]];
//    
//    Attractions * attr = [attractionArray objectAtIndex:a.integerValue-1];
//    NSInteger days = attr.day.integerValue;
    NSDate * minDate;
    if (selectedFromDate) {
        minDate = [Utils addDays:planMain.days.integerValue-1 toDate:selectedFromDate];
    } else {
        minDate = [Utils addDays:planMain.days.integerValue-1 toDate:planMain.from_date];
    }
    
    [ActionSheetDatePicker showPickerWithTitle:@"Select Date" datePickerMode:UIDatePickerModeDate selectedDate:minDate minimumDate:minDate maximumDate:nil doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        selectedToDate = selectedDate;
        NSIndexPath * ip = [NSIndexPath indexPathForRow:0 inSection:0];
        MyVideoTourCell *topCell = (MyVideoTourCell *)[tblView1 cellForRowAtIndexPath:ip];
        topCell.lblToDate.text = [NSString stringWithFormat:@"%@(%@)",[Utils dateFormatMMddyyyy:selectedDate],[Utils getDayShortFromDate:selectedDate]];
        [tblView1 reloadRowsAtIndexPaths:@[ip] withRowAnimation:UITableViewRowAnimationFade];
    } cancelBlock:nil origin:sender];
}

- (IBAction)fromDatePicker:(id)sender{
    
//    NSString * aStr = [NSString stringWithFormat:@"%ld", (long)[sender tag]];
//    
//    NSArray * tagArray = [aStr componentsSeparatedByString:@"99999"];
//    NSString * a = [NSString stringWithFormat:@"%@",[tagArray firstObject]];
//    
//    Attractions * attr = [attractionArray objectAtIndex:a.integerValue-1];
//    NSInteger days = attr.day.integerValue;
    
    NSDate * minDate;
    if (selectedToDate) {
        minDate = [Utils reduceDays:planMain.days.integerValue-1 toDate:selectedToDate];
    } else {
        minDate = [Utils reduceDays:planMain.days.integerValue-1 toDate:planMain.to_date];
    }
    
    [ActionSheetDatePicker showPickerWithTitle:@"Select Date" datePickerMode:UIDatePickerModeDate selectedDate:planMain.from_date minimumDate:[NSDate date] maximumDate:minDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        selectedFromDate = selectedDate;
        NSIndexPath * ip = [NSIndexPath indexPathForRow:0 inSection:0];
        MyVideoTourCell *topCell = (MyVideoTourCell *)[tblView1 cellForRowAtIndexPath:ip];
        topCell.lblFromDate.text = [NSString stringWithFormat:@"%@(%@)",[Utils dateFormatMMddyyyy:selectedDate],[Utils getDayShortFromDate:selectedDate]];
        [tblView1 reloadRowsAtIndexPaths:@[ip] withRowAnimation:UITableViewRowAnimationFade];
    } cancelBlock:nil origin:sender];
}

- (void)editImageClicked:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *openCamrea = [UIAlertAction actionWithTitle:@"Open Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            [Utils showAlertWithMessage:@"Device has no camera"];
        } else {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:NULL];
        }
    }];
    
    UIAlertAction *openGallery = [UIAlertAction actionWithTitle:@"Open Gallery"  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
    }];
    
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:openCamrea];
    [alert addAction:openGallery];
    [alert addAction:cancel];
    
    [alert setModalPresentationStyle:UIModalPresentationPopover];
        //    UIPopoverPresentationController *popPresenter = [alert popoverPresentationController];
        //    popPresenter.sourceView = img;
        //    popPresenter.sourceRect =img.bounds;
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    chosenImage = info[UIImagePickerControllerEditedImage];
    isImageChanged = YES;
    NSIndexPath * ip = [NSIndexPath indexPathForRow:0 inSection:0];
    MyVideoTourCell *topCell = (MyVideoTourCell *)[tblView1 cellForRowAtIndexPath:ip];
    topCell.imgMain.image = chosenImage;
    [tblView1 reloadRowsAtIndexPaths:@[ip] withRowAnimation:UITableViewRowAnimationFade];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)openTimePicker:(id)sender {
    pickerTag = [NSString stringWithFormat:@"%ld",(long)[sender tag]];
    ActionSheetDatePicker *datePicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select a time" datePickerMode:UIDatePickerModeTime selectedDate:[NSDate date] target:self action:@selector(timeWasSelected:element:) origin:sender];
    [datePicker showActionSheetPicker];
}

-(void)timeWasSelected:(NSDate *)selectedTime element:(id)element {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"h:mm a"];
    NSString * aAtr = [dateFormatter stringFromDate:selectedTime];
    NSArray * tagArray = [pickerTag componentsSeparatedByString:@"99999"];
    NSString * a = [NSString stringWithFormat:@"%@",[tagArray firstObject]];
    NSString * b = [NSString stringWithFormat:@"%@",[tagArray lastObject]];
    
    Attractions * attr = [attractionArray objectAtIndex:a.integerValue-1];
    AttractionData *aData = [[[attr.attractionDataSet allObjects] mutableCopy] objectAtIndex:b.integerValue];
    aData.time = aAtr;
    
    [tblView1 reloadData];
}

-(void)routeClicked {
    RouteMapViewController * controller = [RouteMapViewController initViewController:planMain];
    [self.navigationController pushViewController:controller animated:YES];
}



#pragma mark -- Tableview Delegate and DataSource method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return attractionArray.count+2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0 || section == attractionArray.count+1){
        return 1;
    }else{
        if(attractionArray.count <=0){
            return 1;
        }else{
            Attractions * attr = [attractionArray objectAtIndex:section-1];
            return [[attr.attractionDataSet allObjects] count];
        }
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        MyVideoTourCell *firstCell = (MyVideoTourCell*)[tblView1 dequeueReusableCellWithIdentifier:@"MyVideoTourCell" forIndexPath:indexPath];
        firstCell.txtName.text = planMain.name;
        mainName = planMain.name;
        [firstCell.btnRoute addTarget:self action:@selector(routeClicked) forControlEvents:UIControlEventTouchUpInside];
        
        NSDate * fromD;
        NSDate * toD;
        fromD = planMain.from_date;
        toD = planMain.to_date;
        if(selectedFromDate!= nil){
            firstCell.lblFromDate.text = [NSString stringWithFormat:@"%@(%@)",[Utils dateFormatMMddyyyy:selectedFromDate],[Utils getDayShortFromDate:selectedFromDate]];
        }else{
            firstCell.lblFromDate.text = [NSString stringWithFormat:@"%@(%@)",[Utils dateFormatMMddyyyy:fromD],[Utils getDayShortFromDate:fromD]];
        }
        
        if(selectedToDate != nil){
            firstCell.lblToDate.text = [NSString stringWithFormat:@"%@(%@)",[Utils dateFormatMMddyyyy:selectedToDate],[Utils getDayShortFromDate:selectedToDate]];
        }else{
            firstCell.lblToDate.text = [NSString stringWithFormat:@"%@(%@)",[Utils dateFormatMMddyyyy:toD],[Utils getDayShortFromDate:toD]];
        }
        
        if(![planMain.thumbnail isEqualToString:@""]){
            NSString * imgId = planMain.thumbnail;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            firstCell.imgMain.url = [NSURL URLWithString:urlString];
        }
        
        if(chosenImage!=nil){
            firstCell.imgMain.image = chosenImage;
        }
        
        [firstCell.btnMainImage addTarget:self action:@selector(editImageClicked:) forControlEvents:UIControlEventTouchUpInside];
        [firstCell.btnToDate addTarget:self action:@selector(toDatePicker:) forControlEvents:UIControlEventTouchUpInside];
        [firstCell.btnFromDate addTarget:self action:@selector(fromDatePicker:) forControlEvents:UIControlEventTouchUpInside];
        firstCell.selectionStyle = UITableViewCellSelectionStyleNone;
        return firstCell;
    }else if (indexPath.section == attractionArray.count+1){
        VideoTourSaveCell *cell = (VideoTourSaveCell*)[tblView1 dequeueReusableCellWithIdentifier:@"VideoTourSaveCell" forIndexPath:indexPath];
        [cell.btnSaveToDevice addTarget:self action:@selector(saveToDeviceClicked) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnUpload addTarget:self action:@selector(createVideoTour) forControlEvents:UIControlEventTouchUpInside];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else {
        ItineraryTableViewCell *cell = (ItineraryTableViewCell*)[tblView1 dequeueReusableCellWithIdentifier:@"ItineraryTableViewCell" forIndexPath:indexPath];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.bookmarkView.hidden = YES;
        cell.viewsView.hidden = YES;
        cell.shareView.hidden = YES;
        NSDate * fromD;
        NSDate * toD;
        fromD = planMain.from_date;
        toD = planMain.to_date;
        
        [cell.btnTime addTarget:self action:@selector(openTimePicker:) forControlEvents:UIControlEventTouchUpInside];
        Attractions * attr = [attractionArray objectAtIndex:indexPath.section-1];
        AttractionData *aData = [[[attr.attractionDataSet allObjects] mutableCopy] objectAtIndex:indexPath.row];
        cell.lblTime.text = aData.time;
        
        NSString * tagStr = [NSString stringWithFormat:@"%ld99999%ld", (long)indexPath.section,(long)indexPath.row];
        cell.btnDesc.tag = tagStr.integerValue;
        cell.btnTime.tag = tagStr.integerValue;
        cell.btnGreyDelete.tag = tagStr.integerValue;
        cell.btnPlayVideo.tag = tagStr.integerValue;
        
        if (aData.videoId) {
            NSLog(@"%@",aData.videoId.file_name);
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,aData.videoId.thumbnail];
            cell.imgFull.url = [NSURL URLWithString:urlString];
        }else{
            cell.imgFull.image = [UIImage imageNamed:@"no_video.png"];
        }
        
        [cell.btnDesc addTarget:self action:@selector(editDesc:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnGreyDelete addTarget:self action:@selector(deleteAttr:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnPlayVideo addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
        cell.lblName.text = aData.info.name;
        cell.lblDescription.text = aData.info.address;
        NSMutableArray *catArray = [[NSMutableArray alloc] init];
        catArray = [[aData.info.categori allObjects] mutableCopy];
        if(catArray.count >0) {
            Categori *cat =[catArray objectAtIndex:0];
            NSString * imgId=cat.image;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            cell.imgCategory.url = [NSURL URLWithString:urlString];
            cell.imgCategory.layer.cornerRadius = cell.imgCategory.frame.size.width / 2;
            cell.imgCategory.layer.masksToBounds = YES;
        } else {
            cell.imgCategory.backgroundColor = [UIColor grayColor];
            cell.imgCategory.layer.cornerRadius = cell.imgCategory.frame.size.width / 2;
            cell.imgCategory.layer.masksToBounds = YES;
        }
        cell.imgCategory.hidden = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        return 310;
    } else if(indexPath.section == attractionArray.count+1){
        return 120;
    }else{
        return 80;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(section == 0  || section == attractionArray.count+1){
        return [[UIView alloc]initWithFrame:CGRectZero];
    }else{
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 30)];
        UIImageView *imgWall = [[UIImageView alloc] initWithFrame:CGRectMake(11,0,30,30)];
        imgWall.image = [UIImage imageNamed:@"circle"];
        [view addSubview:imgWall];
        UILabel *lblText = [[UILabel alloc] initWithFrame:CGRectMake(46, 5, [[UIScreen mainScreen] bounds].size.width-100, 20)];
        lblText.font = [UIFont systemFontOfSize:12];
        lblText.textColor = [ColorConstants appBrownColor];
        UIButton *btnSpot = [[UIButton alloc] initWithFrame:CGRectMake(230, 3, 50, 20)];
        [btnSpot setTitle:@"+Spot" forState:UIControlStateNormal];
        btnSpot.backgroundColor = [ColorConstants appYellowColor];
        [btnSpot setTitleColor:[ColorConstants appBrownColor] forState:UIControlStateNormal];
        btnSpot.layer.cornerRadius = btnSpot.layer.frame.size.height/2;
        btnSpot.titleLabel.font = [UIFont systemFontOfSize:12];
        UIButton *btnCancel = [[UIButton alloc] initWithFrame:CGRectMake(290, 1, 25, 25)];
        [btnCancel setTitle:@"X" forState:UIControlStateNormal];
        btnCancel.backgroundColor = [UIColor redColor];
        [btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnCancel.layer.cornerRadius = btnCancel.layer.frame.size.height/2;
        btnCancel.titleLabel.font = [UIFont systemFontOfSize:17];
        btnCancel.tag = section;
        [btnCancel addTarget:self action:@selector(deleteDay:) forControlEvents:UIControlEventTouchUpInside];
        NSDate * fromD;
        NSDate * toD;
        
        fromD = planMain.from_date;
        toD = planMain.to_date;
        
        Attractions * attr = [attractionArray objectAtIndex:section-1];
        
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        NSInteger dayCount = attr.day.integerValue;
        if (dayCount >= 1){
            dayCount = dayCount -1;
        }
        dayComponent.day = dayCount;
        NSCalendar *theCalendar = [NSCalendar currentCalendar];
        NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:fromD options:0];
        NSString *next = [Utils dateFormatyyyyMMdd:nextDate];
        NSString * day =[[Utils getDayFromDate:nextDate] substringToIndex:3];
        lblText.text =[NSString stringWithFormat:@"Day %ld - %@(%@)",(long)dayCount+1,next,day];// @"Day 1 2016-9-14 (WED)";
        
        
            // Arun :: Clinet wants to hide this button for now.
            //        [view addSubview:btnSpot];
            //        [view addSubview:btnCancel];
        [view addSubview:lblText];
        [view setBackgroundColor:[UIColor whiteColor]];
        return view;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0 || section == attractionArray.count+1){
        return 0;
    }else{
        return 30;
    }
}

-(void)playVideo:(id)sender {
    [SVProgressHUD showWithStatus:@"Buffering Video..."];

    
    NSString * aStr = [NSString stringWithFormat:@"%ld", (long)[sender tag]];
    NSArray * tagArray = [aStr componentsSeparatedByString:@"99999"];
    NSString * a = [NSString stringWithFormat:@"%@",[tagArray firstObject]];
    NSString * b = [NSString stringWithFormat:@"%@",[tagArray lastObject]];
    Attractions * attr = [attractionArray objectAtIndex:a.integerValue-1];
    AttractionData *aData = [[[attr.attractionDataSet allObjects] mutableCopy] objectAtIndex:b.integerValue];
    if (aData.videoId.file_name.length > 0) {
        NSLog(@"%@",aData.videoId.file_name);
        NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetVideo,aData.videoId.file_name];
        [self playVideoWithURL:[NSURL URLWithString:urlString]];
    } else {
        [SVProgressHUD dismiss];
    }
}

-(void)playVideoWithURL:(NSURL *)url {
    [SVProgressHUD dismiss];
    if (url) {
        self.videoController = [[MPMoviePlayerController alloc] initWithContentURL:url];
        
        [self.videoController.view setFrame:CGRectMake (0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.videoController.controlStyle = MPMovieControlStyleEmbedded;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoPlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:self.videoController];
       
        
//        self.videoController.movieSourceType = MPMovieSourceTypeStreaming;

        
        [self.videoController prepareToPlay];
        [self.view addSubview:self.videoController.view];
        [self.videoController play];
    }
}

- (void)videoPlayBackDidFinish:(NSNotification *)notification {
        //    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    [self.videoController stop];
    [self.videoController.view removeFromSuperview];
    self.videoController = nil;
}

- (IBAction)cancelDescClicked:(id)sender {
    [KGModalWrapper hideView];
}

- (IBAction)saveDescClicked:(id)sender {
    if (txtViewDesc.text.length <= 0) {
        [Utils showAlertWithMessage:@"Please enter description"];
    } else {
        NSArray * tagArray = [descTag componentsSeparatedByString:@"99999"];
        NSString * a = [NSString stringWithFormat:@"%@",[tagArray firstObject]];
        NSString * b = [NSString stringWithFormat:@"%@",[tagArray lastObject]];
        Attractions * attr = [attractionArray objectAtIndex:a.integerValue-1];
        AttractionData *aData = [[[attr.attractionDataSet allObjects] mutableCopy] objectAtIndex:b.integerValue];
        aData.info.address = txtViewDesc.text;
        [tblView1 reloadData];
        [KGModalWrapper hideView];
    }
}

-(void)editDesc:(id)sender {
    NSString * str = [NSString stringWithFormat:@"%ld",(long)[sender tag]];
    descTag = str;
    NSArray * tagArray = [str componentsSeparatedByString:@"99999"];
    NSString * a = [NSString stringWithFormat:@"%@",[tagArray firstObject]];
    NSString * b = [NSString stringWithFormat:@"%@",[tagArray lastObject]];
    Attractions * attr = [attractionArray objectAtIndex:a.integerValue-1];
    AttractionData *aData = [[[attr.attractionDataSet allObjects] mutableCopy] objectAtIndex:b.integerValue];
    txtViewDesc.text = aData.info.address;
    lblManiDes =  [Utils roundCornersOnView:lblManiDes onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];
    [KGModalWrapper showWithContentView:EditDescView];
}


-(void)deleteAttr:(id)sender {
    NSString * str = [NSString stringWithFormat:@"%ld",(long)[sender tag]];
    
    NSArray * tagArray = [str componentsSeparatedByString:@"99999"];
    NSString * a = [NSString stringWithFormat:@"%@",[tagArray firstObject]];
    NSString * b = [NSString stringWithFormat:@"%@",[tagArray lastObject]];
    Attractions * attr = [attractionArray objectAtIndex:a.integerValue-1];
    AttractionData *aData = [[[attr.attractionDataSet allObjects] mutableCopy] objectAtIndex:b.integerValue];
    
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Are you sure you want to delete this attraction?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [SVProgressHUD showWithStatus:@"Updating..."];
        [[PlayTripManager Instance] deleteAttractionWithId:aData.entity_id andplanId:planMain.entity_id andDay:attr.day.stringValue WithBlock:^(id result, NSString *error) {
            if (error) {
                NSLog(@"%@", error);
            } else {
                //NSLog(@"%@", result);
            }
            [self loadAllPlansOfUser];
        }];
    }];
    
    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:nil];
    
    [alert addAction:yesAction];
    [alert addAction:noAction];
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}


-(void)deleteDay:(id)sender {
    NSString * str = [NSString stringWithFormat:@"%ld",(long)[sender tag]];
    NSArray * tagArray = [str componentsSeparatedByString:@"99999"];
    NSString * a = [NSString stringWithFormat:@"%@",[tagArray firstObject]];
        //    NSString * days = [NSString stringWithFormat:@"%ld",a.integerValue +1];
    
    Attractions *atr = [attractionArray objectAtIndex:a.integerValue-1];
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Are you sure you want to delete this Day?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [SVProgressHUD showWithStatus:@"Updating..."];
        [[PlayTripManager Instance] deleteDayForPlanId:planMain.entity_id andDay:atr.entity_id WithBlock:^(id result, NSString *error) {
            if (error) {
                NSLog(@"%@", error);
            } else {
                //NSLog(@"%@", result);
            }
            [self loadAllPlansOfUser];
        }];
    }];
    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:yesAction];
    [alert addAction:noAction];
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}


-(void)loadAllPlansOfUser {
    Account * account = [AccountManager Instance].activeAccount;
    [[PlayTripManager Instance] loadPlanByUserID:account.userId WithBlock:^(id result, NSString *error) {
        Plan *updatedPlan = [Plan getByEntityId:planMain.entity_id];
        planMain = updatedPlan;
        
        attractionArray = [[planMain.attractionsSet allObjects]mutableCopy];
        NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"day" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
        NSArray *sortedArray = [attractionArray sortedArrayUsingDescriptors:sortDescriptors];
        attractionArray = [sortedArray mutableCopy];
        [tblView1 reloadData];
        
        [SVProgressHUD dismiss];
    }];
}

-(void)saveToDeviceClicked {
    [SVProgressHUD showWithStatus:@"Updating..."];
    [self saveAttrDatainServer];
}

-(void)saveAttrDatainServer {
    NSMutableArray * finalAttrDataArray = [NSMutableArray new];
    
    for (Attractions * at in attractionArray) {
        NSMutableDictionary * inDict = [NSMutableDictionary new];
        for (AttractionData * ad in [[at.attractionDataSet allObjects]mutableCopy]) {
            [inDict setValue:ad.entity_id forKey:@"_id"];
            [inDict setValue:ad.info.address forKey:@"desc"];
            [inDict setValue:ad.time forKey:@"time"];
            [finalAttrDataArray addObject:inDict];
        }
    }
    [[PlayTripManager Instance] updateMultipleAttrData:finalAttrDataArray ForPlanId:planMain.entity_id WithBlock:^(id result, NSString *error) {
        if (error) {
            NSLog(@"%@", error);
        } else {
            //NSLog(@"%@", result);
        }
        [self saveVideoTourInServer];
    }];
}

-(void)saveVideoTourInServer{
    NSDate * toDate;
    if (selectedToDate) {
        toDate = selectedToDate;
    } else {
        toDate = planMain.to_date;
    }
    
    NSDate * fromDate;
    if (selectedFromDate) {
        fromDate = selectedFromDate;
    } else {
        fromDate = planMain.to_date;
    }
    
    NSIndexPath * ip = [NSIndexPath indexPathForRow:0 inSection:0];
    MyVideoTourCell *firstCell = (MyVideoTourCell*)[tblView1 cellForRowAtIndexPath:ip];
    NSString * travelTips =  firstCell.txtTips.text;
    NSString * from_date = [Utils strFromDate:fromDate];
    NSString * to_date = [Utils strFromDate:toDate];
    NSString * name = mainName;// firstCell.txtName.text;
    NSString * days = [Utils getDaysBetweenFromDate:fromDate andToDate:toDate];
    
    BOOL self_drive = false;
    if ([firstCell.btnYes.currentImage isEqual:[UIImage imageNamed:@"RadioButton-Selected.png"]]) {
        self_drive = true;
    }
    
    [[PlayTripManager Instance] editPlanWithId:planMain.entity_id WithName:name WithTravelTip:travelTips WithFromDate:from_date WithToDate:to_date WithDays:days andSelfDrive:self_drive WithBlock:^(id result, NSString *error) {
        if (error) {
            NSLog(@"%@", error);
        } else {
            //NSLog(@"%@", result);
        }
        if (isImageChanged) {
            [self saveThumbnailInServer];
        }else {
            [self goBack];
        }
    }];
}

-(void)saveThumbnailInServer {
    [[PlayTripManager Instance] uploadCoverPhoto:chosenImage ForPlanId:planMain.entity_id WithBlock:^(id result, NSString *error) {
        if (error) {
            NSLog(@"%@", error);
        } else {
            //NSLog(@"%@", result);
        }
        [self goBack];
    }];
}


-(void)createVideoTour {
    Account *account = [AccountManager Instance].activeAccount;
    
    if (![[AppDelegate appDelegate] isReachable]) {
        [Utils showAlertWithMessage:kCheckInternet];
        return;
    }else{
        if([account.wifiOnly boolValue]){
            if (![[AppDelegate appDelegate] isReachableViaWifi]) {
                [Utils showAlertWithMessage:@"Please check your wifi connection"];
                return;
                
            }else{
                [self videoTourDataUpload];
            }
        }else{
            [self videoTourDataUpload];
        }
        
        
    }
}
-(void)videoTourDataUpload{
    NSInteger days = [planMain.days integerValue];
    NSInteger j = 0;
    
    for (NSInteger i = 1; i<= days; i++) {
        for (Attractions * atr in attractionArray) {
            if (atr.day.integerValue == i) {
                j++;
            }
        }
    }
    NSLog(@"j = %ld, days = %ld", (long)j, (long)days);
    
    BOOL isVideoAbsent = false;
    if (j != days) {
        [Utils showAlertWithMessage:@"Attractions is not found in all days"];
    } else {
        for (Attractions * atr in attractionArray) {
            for (AttractionData * aData in [[atr.attractionDataSet allObjects] mutableCopy]) {
                if (aData.videoId == nil) {
                    LocalVideoObject * vidOb = [LocalVideoObject getByEntityId:aData.entity_id];
                    if (vidOb == nil) {
                        isVideoAbsent = true;
                    }
                }
            }
        }
        if (isVideoAbsent) {
            [Utils showAlertWithMessage:@"Video is not found in all attractions"];
        } else {
            CreateVideoTourViewController * controller = [CreateVideoTourViewController initViewControllerWithPlan:planMain];
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
    
}

-(void)goBack {
    [SVProgressHUD dismiss];
    Account * account = [AccountManager Instance].activeAccount;
    [[PlayTripManager Instance] loadPlanByUserID:account.userId WithBlock:^(id result, NSString *error) {
    }];
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Saved Successfully" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
    [alert addAction:okayAction];
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.videoController stop];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

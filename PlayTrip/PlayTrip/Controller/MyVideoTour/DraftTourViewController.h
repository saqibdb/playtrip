
#import <UIKit/UIKit.h>
#import "ItineraryTableViewCell.h"
#import "CarRentalTableViewCell.h"
#import "InsuranceTableViewCell.h"
#import "DragAndDropTableView.h"
#import <MediaPlayer/MediaPlayer.h>
#import "DraftTour.h"
#import "DraftAttraction.h"
#import "DraftAttrData.h"
#import "DraftInfo.h"
#import "DraftVideoId.h"

@interface DraftTourViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate, UITextFieldDelegate, UIImagePickerControllerDelegate, UIGestureRecognizerDelegate> {
    
    NSString *timeString;
    NSString *catImage;
    NSString *mainName;
    
    IBOutlet UILabel *lblManiDes;
    BOOL selfDrive;
    BOOL isImageChanged;
    
    
    NSString * pickerTag;
    NSString * descTag;
    
    UIImage *chosenImage;
    NSDate *selectedFromDate;
    NSDate *selectedToDate;
    
    NSMutableArray *attractionArray;
    NSMutableArray *atArray;
    
    NSMutableArray *outerArray;
    
    NSMutableArray *languagesArray;
    NSMutableArray *categoryListAttr;
    
    IBOutlet UITextView *txtViewDesc;
    IBOutlet UIView *EditDescView;
    
    IBOutlet UITableView *tblMain;
    
    DraftTour * dTour;
    
    
    IBOutlet UIView *popUpView;

}

@property (strong, nonatomic) MPMoviePlayerController *videoController;

- (IBAction)cancelDescClicked:(id)sender;
- (IBAction)saveDescClicked:(id)sender;

+(DraftTourViewController *)initViewController;

@end

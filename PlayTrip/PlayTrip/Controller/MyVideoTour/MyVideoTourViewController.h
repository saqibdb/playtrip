
#import <UIKit/UIKit.h>
#import "HMSegmentedControl.h"
#import "ItineraryTableViewCell.h"
#import "Plan.h"
#import "CarRentalTableViewCell.h"
#import "InsuranceTableViewCell.h"
#import "VideoTour.h"
#import "DragAndDropTableView.h"
#import <MediaPlayer/MediaPlayer.h>

@interface MyVideoTourViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIGestureRecognizerDelegate> {
    
    NSString *timeString;
    NSString *catImage;
    NSString *mainName;
    
    IBOutlet UILabel *lblManiDes;
    BOOL selfDrive;
    BOOL isImageChanged;
    
    NSMutableArray * outerArray;
    
    NSString * pickerTag;
    NSString * descTag;
    
    UIImage *chosenImage;
    NSDate *selectedFromDate;
    NSDate *selectedToDate;
    
    NSMutableArray *attractionArray;
    NSMutableArray *languagesArray;
    NSMutableArray *categoryListAttr;
    NSMutableArray *attDataArray;
    
    
    
    IBOutlet UITextView *txtViewDesc;
    IBOutlet UIView *EditDescView;
}

- (IBAction)cancelDescClicked:(id)sender;
- (IBAction)saveDescClicked:(id)sender;

//@property(nonatomic)Plan *planMain;
@property (strong, nonatomic) MPMoviePlayerController *videoController;

@property (strong, nonatomic) IBOutlet UITableView *tblView1;

+(MyVideoTourViewController *)initController:(Plan *)pl;
+(MyVideoTourViewController *)initController;
@end

/*
 //@property (weak, nonatomic) IBOutlet UIView *routeView;
 //@property (weak, nonatomic) IBOutlet UIButton *btnCreateVideotour;
 
 //    IBOutlet UIImageView *img;
 
 //    IBOutlet UILabel *lblDays;
 //    IBOutlet UILabel *lblDate;
 //    IBOutlet UITextField *txtTourName;
 //    IBOutlet UIImageView *imgLanguage;
 //    IBOutlet UIView *travelTipView;
 //
 //    IBOutlet UIView *footerView;
 //    IBOutlet UIButton *btnNo;
 //    IBOutlet UIButton *btnYes;
 //    IBOutlet UILabel *toDate;
 //    IBOutlet UILabel *fromDate;
 //    IBOutlet UIView *fromView;
 //
 //    IBOutlet UIButton *btnUpload;
 //    IBOutlet UIButton *btnSave;
 //    IBOutlet UIView *toView;
 
 */

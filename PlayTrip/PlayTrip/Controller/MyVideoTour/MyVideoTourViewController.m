
#import "MyVideoTourViewController.h"
#import "CreateVideoTourViewController.h"
#import "LocalVideoObject.h"
#import "VideoId.h"
#import "KGModalWrapper.h"
#import "SVProgressHUD.h"
#import "ActionSheetPicker.h"
#import "Account.h"
#import "AccountManager.h"
#import "PlayTripManager.h"
#import "AirTicketCell.h"
#import "ItineraryTableViewCell.h"
#import "HotelBookingCell.h"
#import "ColorConstants.h"
#import "TotalView.h"
#import "Attractions.h"
#import "AttractionData.h"
#import "URLSchema.h"
#import "Categori.h"
#import "Utils.h"
#import "PlaceInformationViewController.h"
#import "Info.h"
#import "MyVideoTourCell.h"
#import "VideoTourSaveCell.h"
#import "ActionSheetPicker.h"
#import "ActionSheetDatePicker.h"
#import "RouteMapViewController.h"
#import "AppDelegate.h"
#import "Account.h"
#import "AccountManager.h"
#import "DraftTour.h"

@interface MyVideoTourViewController () {
    DraftTour * dTour;
}
@end

@implementation MyVideoTourViewController

+(MyVideoTourViewController *)initController{
    MyVideoTourViewController * controller = [[MyVideoTourViewController alloc] initWithNibName:@"MyVideoTourViewController" bundle:nil];
    controller.title = @"My Video Tour (Draft)";
    return controller;
}

+(MyVideoTourViewController *)initController:(Plan *)pl {
    MyVideoTourViewController * controller = [[MyVideoTourViewController alloc] initWithNibName:@"MyVideoTourViewController" bundle:nil];
    controller.title = @"My Video Tour (Draft)";
//    controller.planMain = pl;
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _tblView1.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self registerNib];
    [_tblView1 setHidden:NO];
    attractionArray = [NSMutableArray new];
    [self setArray];
    dTour = [DraftTour getDraft];
}

-(void)setArray {
    NSMutableArray * localArray = [[dTour.attractionsSet allObjects]mutableCopy];
    attractionArray = localArray;

    NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"day"
                                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
    NSArray *sortedArray = [localArray sortedArrayUsingDescriptors:sortDescriptors];
    localArray = [sortedArray mutableCopy];
    attractionArray = localArray;
    outerArray = [NSMutableArray new];
    for (Attractions * attr in localArray) {
        [outerArray addObject:[[attr.attractionDataSet allObjects] mutableCopy]];
    }
    NSLog(@"%@", outerArray);
    [_tblView1 reloadData];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    [self setNavBarItems];
}

-(void)setNavBarItems {
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[ColorConstants appBrownColor]}];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backClicked)];
    self.navigationItem.leftBarButtonItem = backButton;
    self.navigationController.navigationBar.tintColor = [ColorConstants appBrownColor];
    self.navigationController.navigationBar.translucent = NO;
    UIBarButtonItem * homeButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"home.png"] style:UIBarButtonItemStylePlain target:self action:@selector(homeClicked)];
    [self.navigationItem setRightBarButtonItem:homeButton];
}

-(void)homeClicked{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)backClicked{
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)registerNib{
    UINib *celliterery = [UINib nibWithNibName:@"ItineraryTableViewCell" bundle:nil];
    [_tblView1 registerNib:celliterery forCellReuseIdentifier:@"ItineraryTableViewCell"];
    UINib *cell = [UINib nibWithNibName:@"MyVideoTourCell" bundle:nil];
    [_tblView1 registerNib:cell forCellReuseIdentifier:@"MyVideoTourCell"];
    UINib *cell2 = [UINib nibWithNibName:@"VideoTourSaveCell" bundle:nil];
    [_tblView1 registerNib:cell2 forCellReuseIdentifier:@"VideoTourSaveCell"];
}

- (IBAction)toDatePicker:(id)sender{
//    NSInteger days = [_planMain.days integerValue];
    NSInteger days = outerArray.count;
    NSDate * minDate;
    if (selectedFromDate) {
        minDate = [Utils addDays:days toDate:selectedFromDate];
    } else {
        minDate = [Utils addDays:days toDate:dTour.from_date];
    }
    
    [ActionSheetDatePicker showPickerWithTitle:@"Select Date" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] minimumDate:minDate maximumDate:nil doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        selectedToDate = selectedDate;
        NSIndexPath * ip = [NSIndexPath indexPathForRow:0 inSection:0];
        MyVideoTourCell *topCell = (MyVideoTourCell *)[_tblView1 cellForRowAtIndexPath:ip];
        topCell.lblToDate.text = [NSString stringWithFormat:@"%@(%@)",[Utils dateFormatMMddyyyy:selectedDate],[Utils getDayShortFromDate:selectedDate]];
        [_tblView1 reloadRowsAtIndexPaths:@[ip] withRowAnimation:UITableViewRowAnimationFade];
    } cancelBlock:nil origin:sender];
}

- (IBAction)fromDatePicker:(id)sender{
    
//    NSInteger days = [_planMain.days integerValue];
    NSInteger days = outerArray.count;
    NSDate * minDate;
    if (selectedToDate) {
        minDate = [Utils reduceDays:days toDate:selectedToDate];
    } else {
        minDate = [Utils reduceDays:days toDate:dTour.to_date];
    }
    
    [ActionSheetDatePicker showPickerWithTitle:@"Select Date" datePickerMode:UIDatePickerModeDate selectedDate:dTour.from_date minimumDate:[NSDate date] maximumDate:minDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        selectedFromDate = selectedDate;
        NSIndexPath * ip = [NSIndexPath indexPathForRow:0 inSection:0];
        MyVideoTourCell *topCell = (MyVideoTourCell *)[_tblView1 cellForRowAtIndexPath:ip];
        topCell.lblFromDate.text = [NSString stringWithFormat:@"%@(%@)",[Utils dateFormatMMddyyyy:selectedDate],[Utils getDayShortFromDate:selectedDate]];
        [_tblView1 reloadRowsAtIndexPaths:@[ip] withRowAnimation:UITableViewRowAnimationFade];
    } cancelBlock:nil origin:sender];
}

- (void)editImageClicked:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *openCamrea = [UIAlertAction actionWithTitle:@"Open Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            [Utils showAlertWithMessage:@"Device has no camera"];
        } else {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:NULL];
        }
    }];
    
    UIAlertAction *openGallery = [UIAlertAction actionWithTitle:@"Open Gallery"  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
    }];
    
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:openCamrea];
    [alert addAction:openGallery];
    [alert addAction:cancel];
    
    [alert setModalPresentationStyle:UIModalPresentationPopover];
    //    UIPopoverPresentationController *popPresenter = [alert popoverPresentationController];
    //    popPresenter.sourceView = img;
    //    popPresenter.sourceRect =img.bounds;
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    chosenImage = info[UIImagePickerControllerEditedImage];
    isImageChanged = YES;
    NSIndexPath * ip = [NSIndexPath indexPathForRow:0 inSection:0];
    MyVideoTourCell *topCell = (MyVideoTourCell *)[_tblView1 cellForRowAtIndexPath:ip];
    topCell.imgMain.image = chosenImage;
    [_tblView1 reloadRowsAtIndexPaths:@[ip] withRowAnimation:UITableViewRowAnimationFade];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)openTimePicker:(id)sender {
    pickerTag = [NSString stringWithFormat:@"%ld",(long)[sender tag]];
    ActionSheetDatePicker *datePicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select a time" datePickerMode:UIDatePickerModeTime selectedDate:[NSDate date] target:self action:@selector(timeWasSelected:element:) origin:sender];
    [datePicker showActionSheetPicker];
}

-(void)timeWasSelected:(NSDate *)selectedTime element:(id)element {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"h:mm a"];
    NSString * aAtr = [dateFormatter stringFromDate:selectedTime];
    NSArray * tagArray = [pickerTag componentsSeparatedByString:@"99999"];
    NSString * a = [NSString stringWithFormat:@"%@",[tagArray firstObject]];
    NSString * b = [NSString stringWithFormat:@"%@",[tagArray lastObject]];
    AttractionData *aData = [[outerArray objectAtIndex:a.integerValue-1] objectAtIndex:b.integerValue];
    aData.time = aAtr;
    
    [_tblView1 reloadData];
}

-(void)routeClicked {
//    RouteMapViewController * controller = [RouteMapViewController initViewController:_planMain];
//    [self.navigationController pushViewController:controller animated:YES];
}

#pragma mark -- Tableview Delegate and DataSource method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return outerArray.count+2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0 || section == outerArray.count+1){
        return 1;
    }else{
        if(outerArray.count <=0){
            return 1;
        }else{
            return [[outerArray objectAtIndex:section-1] count];
        }
    }
}


- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
        //remove any editing accessories (AKA) delete button
    return UITableViewCellEditingStyleNone;
}

- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath*)sourceIndexPath
       toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath{
    if (proposedDestinationIndexPath.section == 0  || proposedDestinationIndexPath.section == outerArray.count+1) {
        return sourceIndexPath;
    } else {
        return proposedDestinationIndexPath;
    }
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    [_tblView1 setEditing:NO animated:YES];
    AttractionData * obj = [[outerArray objectAtIndex:sourceIndexPath.section-1] objectAtIndex:sourceIndexPath.row];
    [[outerArray objectAtIndex:sourceIndexPath.section-1] removeObjectAtIndex:sourceIndexPath.row];
    [[outerArray objectAtIndex:destinationIndexPath.section-1] insertObject:obj atIndex:destinationIndexPath.row];
    [_tblView1 reloadData];
}

/*
-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

-(void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    Attractions * atrs = [attractionArray objectAtIndex:sourceIndexPath.section-1];
    AttractionData * obj = [[[atrs.attractionDataSet allObjects] mutableCopy] objectAtIndex:sourceIndexPath.row];
    
    [[[atrs.attractionDataSet allObjects] mutableCopy] removeObjectAtIndex:sourceIndexPath.row];
    [[[atrs.attractionDataSet allObjects] mutableCopy] insertObject:obj atIndex:destinationIndexPath.row];
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if(UITableViewCellEditingStyleInsert == editingStyle) {
        // inserts are always done at the end

        [tableView beginUpdates];
        [attractionArray addObject:[NSMutableArray array]];
        [tableView insertSections:[NSIndexSet indexSetWithIndex:[attractionArray count]-1] withRowAnimation:UITableViewRowAnimationAutomatic];
        [tableView endUpdates];

    } else if(UITableViewCellEditingStyleDelete == editingStyle) {
        // check if we are going to delete a row or a section
        [tableView beginUpdates];
        Attractions * atrs = [attractionArray objectAtIndex:indexPath.section-1];

        if([[atrs.attractionDataSet allObjects] count] == 0)
        {
            [tableView deleteSections:[NSIndexSet indexSetWithIndex:indexPath.section-1] withRowAnimation:UITableViewRowAnimationAutomatic];
            [attractionArray removeObjectAtIndex:indexPath.section-1];
        }
        else
        {
            Attractions * atrs = [attractionArray objectAtIndex:indexPath.section-1];
            
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            // Delete the row from the data source.
            [[[atrs.attractionDataSet allObjects] mutableCopy] removeObjectAtIndex:indexPath.row];
        }
        [tableView endUpdates];
    }
}
 
 */

/*
#pragma mark DragAndDropTableViewDataSource

-(BOOL)canCreateNewSection:(NSInteger)section {
    return YES;
}

#pragma mark -

#pragma mark DragAndDropTableViewDelegate

-(void)tableView:(UITableView *)tableView willBeginDraggingCellAtIndexPath:(NSIndexPath *)indexPath placeholderImageView:(UIImageView *)placeHolderImageView {
    // this is the place to edit the snapshot of the moving cell
    // add a shadow
    placeHolderImageView.layer.shadowOpacity = .3;
    placeHolderImageView.layer.shadowRadius = 1;
}

-(void)tableView:(DragAndDropTableView *)tableView didEndDraggingCellAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)toIndexPath placeHolderView:(UIImageView *)placeholderImageView {
    // The cell has been dropped. Remove all empty sections (if you want to)
    NSMutableIndexSet *indexSet = [NSMutableIndexSet indexSet];
    for(int i = 0; i < attractionArray.count; i++) {
        Attractions * ats = [attractionArray objectAtIndex:i];
        if([[ats.attractionDataSet allObjects] count] == 0)
            [indexSet addIndex:i];
    }

    [tableView beginUpdates];
    [tableView deleteSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
    [attractionArray removeObjectsAtIndexes:indexSet];
    [tableView endUpdates];
}


*/









//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if(indexPath.section == 0){
//        MyVideoTourCell *firstCell = (MyVideoTourCell*)[_tblView1 dequeueReusableCellWithIdentifier:@"MyVideoTourCell" forIndexPath:indexPath];
//        if (_planMain) {
//            firstCell.txtName.text = _planMain.name;
//            mainName = _planMain.name;
//        } else {
//            firstCell.txtName.text = _videotour.name;
//             mainName = _videotour.name;
//        }
//        [firstCell.btnRoute addTarget:self action:@selector(routeClicked) forControlEvents:UIControlEventTouchUpInside];
//        
//        NSDate * fromD;
//        NSDate * toD;
//        if (_planMain) {
//            fromD = _planMain.from_date;
//            toD = _planMain.to_date;
//        } else {
//            fromD = _videotour.from_date;
//            toD = _videotour.to_date;
//        }
//        if(selectedFromDate!= nil){
//            firstCell.lblFromDate.text = [NSString stringWithFormat:@"%@(%@)",[Utils dateFormatMMddyyyy:selectedFromDate],[Utils getDayShortFromDate:selectedFromDate]];
//        }else{
//            firstCell.lblFromDate.text = [NSString stringWithFormat:@"%@(%@)",[Utils dateFormatMMddyyyy:fromD],[Utils getDayShortFromDate:fromD]];
//        }
//        
//        if(selectedToDate != nil){
//            firstCell.lblToDate.text = [NSString stringWithFormat:@"%@(%@)",[Utils dateFormatMMddyyyy:selectedToDate],[Utils getDayShortFromDate:selectedToDate]];
//        }else{
//            firstCell.lblToDate.text = [NSString stringWithFormat:@"%@(%@)",[Utils dateFormatMMddyyyy:toD],[Utils getDayShortFromDate:toD]];
//        }
//        
//        if(![_planMain.thumbnail isEqualToString:@""]){
//            NSString * imgId = _planMain.thumbnail;
//            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
//            firstCell.imgMain.url = [NSURL URLWithString:urlString];
//        }
//        
//        if(chosenImage!=nil){
//            firstCell.imgMain.image = chosenImage;
//        }
//        
//        [firstCell.btnMainImage addTarget:self action:@selector(editImageClicked:) forControlEvents:UIControlEventTouchUpInside];
//        [firstCell.btnToDate addTarget:self action:@selector(toDatePicker:) forControlEvents:UIControlEventTouchUpInside];
//        [firstCell.btnFromDate addTarget:self action:@selector(fromDatePicker:) forControlEvents:UIControlEventTouchUpInside];
//        firstCell.selectionStyle = UITableViewCellSelectionStyleNone;
//        return firstCell;
//    }else if (indexPath.section == attractionArray.count+1){
//        VideoTourSaveCell *cell = (VideoTourSaveCell*)[_tblView1 dequeueReusableCellWithIdentifier:@"VideoTourSaveCell" forIndexPath:indexPath];
//        [cell.btnSaveToDevice addTarget:self action:@selector(saveToDeviceClicked) forControlEvents:UIControlEventTouchUpInside];
//        [cell.btnUpload addTarget:self action:@selector(createVideoTour) forControlEvents:UIControlEventTouchUpInside];
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        return cell;
//    } else {
//        ItineraryTableViewCell *cell = (ItineraryTableViewCell*)[_tblView1 dequeueReusableCellWithIdentifier:@"ItineraryTableViewCell" forIndexPath:indexPath];
//        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
//        cell.bookmarkView.hidden = YES;
//        cell.viewsView.hidden = YES;
//        cell.shareView.hidden = YES;
//        NSDate * fromD;
//        NSDate * toD;
//        if (_planMain) {
//            fromD = _planMain.from_date;
//            toD = _planMain.to_date;
//        } else {
//            fromD = _videotour.from_date;
//            toD = _videotour.to_date;
//        }
//        
//        [cell.btnTime addTarget:self action:@selector(openTimePicker:) forControlEvents:UIControlEventTouchUpInside];
//        Attractions * attr = [outerArray objectAtIndex:indexPath.section-1];
//        AttractionData *aData = [[[attr.attractionDataSet allObjects] mutableCopy] objectAtIndex:indexPath.row];
//        cell.lblTime.text = aData.time;
//        
//        NSString * tagStr = [NSString stringWithFormat:@"%ld99999%ld", (long)indexPath.section,(long)indexPath.row];
//        cell.btnDesc.tag = tagStr.integerValue;
//        cell.btnTime.tag = tagStr.integerValue;
//        cell.btnGreyDelete.tag = tagStr.integerValue;
//        cell.btnPlayVideo.tag = tagStr.integerValue;
//        
//        if (aData.videoId) {
//            NSLog(@"%@",aData.videoId.file_name);
//            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,aData.videoId.thumbnail];
//            cell.imgFull.url = [NSURL URLWithString:urlString];
//        }else{
//            cell.imgFull.image = [UIImage imageNamed:@"no_video.png"];
//        }
//        
//        [cell.btnDesc addTarget:self action:@selector(editDesc:) forControlEvents:UIControlEventTouchUpInside];
//        [cell.btnGreyDelete addTarget:self action:@selector(deleteAttr:) forControlEvents:UIControlEventTouchUpInside];
//        [cell.btnPlayVideo addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
//        cell.lblName.text = aData.info.name;
//        cell.lblDescription.text = aData.info.address;
//        NSMutableArray *catArray = [[NSMutableArray alloc] init];
//        catArray = [[aData.info.categori allObjects] mutableCopy];
//        if(catArray.count >0) {
//            Categori *cat =[catArray objectAtIndex:0];
//            NSString * imgId=cat.image;
//            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
//            cell.imgCategory.url = [NSURL URLWithString:urlString];
//            cell.imgCategory.layer.cornerRadius = cell.imgCategory.frame.size.width / 2;
//            cell.imgCategory.layer.masksToBounds = YES;
//        } else {
//            cell.imgCategory.backgroundColor = [UIColor grayColor];
//            cell.imgCategory.layer.cornerRadius = cell.imgCategory.frame.size.width / 2;
//            cell.imgCategory.layer.masksToBounds = YES;
//        }
//        cell.imgCategory.hidden = YES;
//        
//        UILongPressGestureRecognizer* longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture)];
//        longPressGesture.minimumPressDuration = .5; //seconds
//        longPressGesture.delegate = self;
//        [cell addGestureRecognizer:longPressGesture];
//        
//        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        return cell;
//    }
//}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        MyVideoTourCell *firstCell = (MyVideoTourCell*)[_tblView1 dequeueReusableCellWithIdentifier:@"MyVideoTourCell" forIndexPath:indexPath];
        firstCell.txtName.text = dTour.name;
        mainName = dTour.name;
        [firstCell.btnRoute addTarget:self action:@selector(routeClicked) forControlEvents:UIControlEventTouchUpInside];
        
        [firstCell.txtTips setTextColor:[UIColor blackColor]];
        firstCell.txtTips.text = dTour.travel_tip;
        
        NSDate * fromD;
        NSDate * toD;
        
        fromD = dTour.from_date;
        toD = dTour.to_date;
        
        if (dTour.self_drive.boolValue) {
            [firstCell yesClicked:self];
        } else {
            [firstCell noClicked:self];
        }
        
        if(selectedFromDate!= nil){
            firstCell.lblFromDate.text = [NSString stringWithFormat:@"%@(%@)",[Utils dateFormatMMddyyyy:selectedFromDate],[Utils getDayShortFromDate:selectedFromDate]];
        }else{
            firstCell.lblFromDate.text = [NSString stringWithFormat:@"%@(%@)",[Utils dateFormatMMddyyyy:fromD],[Utils getDayShortFromDate:fromD]];
        }
        
        if(selectedToDate != nil){
            firstCell.lblToDate.text = [NSString stringWithFormat:@"%@(%@)",[Utils dateFormatMMddyyyy:selectedToDate],[Utils getDayShortFromDate:selectedToDate]];
        }else{
            firstCell.lblToDate.text = [NSString stringWithFormat:@"%@(%@)",[Utils dateFormatMMddyyyy:toD],[Utils getDayShortFromDate:toD]];
        }
        
        if(![dTour.thumbnail isEqualToString:@""]){
            NSString * imgId = dTour.thumbnail;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            firstCell.imgMain.url = [NSURL URLWithString:urlString];
        }
        
        if(chosenImage!=nil){
            firstCell.imgMain.image = chosenImage;
        }
        
        [firstCell.btnMainImage addTarget:self action:@selector(editImageClicked:) forControlEvents:UIControlEventTouchUpInside];
        [firstCell.btnToDate addTarget:self action:@selector(toDatePicker:) forControlEvents:UIControlEventTouchUpInside];
        [firstCell.btnFromDate addTarget:self action:@selector(fromDatePicker:) forControlEvents:UIControlEventTouchUpInside];
        firstCell.selectionStyle = UITableViewCellSelectionStyleNone;
        return firstCell;
    }else if (indexPath.section == outerArray.count+1){
        VideoTourSaveCell *cell = (VideoTourSaveCell*)[_tblView1 dequeueReusableCellWithIdentifier:@"VideoTourSaveCell" forIndexPath:indexPath];
        [cell.btnSaveToDevice addTarget:self action:@selector(saveToDeviceClicked) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnUpload addTarget:self action:@selector(createVideoTour) forControlEvents:UIControlEventTouchUpInside];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else {
        ItineraryTableViewCell *cell = (ItineraryTableViewCell*)[_tblView1 dequeueReusableCellWithIdentifier:@"ItineraryTableViewCell" forIndexPath:indexPath];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.bookmarkView.hidden = YES;
        cell.viewsView.hidden = YES;
        cell.shareView.hidden = YES;
        NSDate * fromD;
        NSDate * toD;
        
        fromD = dTour.from_date;
        toD = dTour.to_date;
        
        [cell.btnTime addTarget:self action:@selector(openTimePicker:) forControlEvents:UIControlEventTouchUpInside];
//        Attractions * attr = [outerArray objectAtIndex:indexPath.section-1];
        AttractionData *aData = [[outerArray objectAtIndex:indexPath.section-1] objectAtIndex:indexPath.row];
        cell.lblTime.text = aData.time;
        
        NSString * tagStr = [NSString stringWithFormat:@"%ld99999%ld", (long)indexPath.section,(long)indexPath.row];
        cell.btnDesc.tag = tagStr.integerValue;
        cell.btnTime.tag = tagStr.integerValue;
        cell.btnGreyDelete.tag = tagStr.integerValue;
        cell.btnPlayVideo.tag = tagStr.integerValue;
        
        if (aData.videoId) {
            NSLog(@"%@",aData.videoId.file_name);
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,aData.videoId.thumbnail];
            cell.imgFull.url = [NSURL URLWithString:urlString];
        }else{
            cell.imgFull.image = [UIImage imageNamed:@"no_video.png"];
        }
        
        [cell.btnDesc addTarget:self action:@selector(editDesc:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnGreyDelete addTarget:self action:@selector(deleteAttr:) forControlEvents:UIControlEventTouchUpInside];
//        [cell.btnPlayVideo addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
        cell.lblName.text = aData.info.name;
        cell.lblDescription.text = aData.info.address;
        NSMutableArray *catArray = [[NSMutableArray alloc] init];
        catArray = [[aData.info.categori allObjects] mutableCopy];
        if(catArray.count >0) {
            Categori *cat =[catArray objectAtIndex:0];
            NSString * imgId=cat.image;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            cell.imgCategory.url = [NSURL URLWithString:urlString];
            cell.imgCategory.layer.cornerRadius = cell.imgCategory.frame.size.width / 2;
            cell.imgCategory.layer.masksToBounds = YES;
        } else {
            cell.imgCategory.backgroundColor = [UIColor grayColor];
            cell.imgCategory.layer.cornerRadius = cell.imgCategory.frame.size.width / 2;
            cell.imgCategory.layer.masksToBounds = YES;
        }
        cell.imgCategory.hidden = YES;
        
        UILongPressGestureRecognizer* longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture)];
        longPressGesture.minimumPressDuration = .5; //seconds
        longPressGesture.delegate = self;
        [cell addGestureRecognizer:longPressGesture];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        return 310;
    } else if(indexPath.section == outerArray.count+1){
        return 120;
    }else{
        return 80;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(section == 0  || section == outerArray.count+1){
        return [[UIView alloc]initWithFrame:CGRectZero];
    }else{
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 30)];
        UIImageView *imgWall = [[UIImageView alloc] initWithFrame:CGRectMake(11,0,30,30)];
        imgWall.image = [UIImage imageNamed:@"circle"];
        [view addSubview:imgWall];
        UILabel *lblText = [[UILabel alloc] initWithFrame:CGRectMake(46, 5, [[UIScreen mainScreen] bounds].size.width-100, 20)];
        lblText.font = [UIFont systemFontOfSize:12];
        lblText.textColor = [ColorConstants appBrownColor];
        UIButton *btnSpot = [[UIButton alloc] initWithFrame:CGRectMake(230, 3, 50, 20)];
        [btnSpot setTitle:@"+Spot" forState:UIControlStateNormal];
        btnSpot.backgroundColor = [ColorConstants appYellowColor];
        [btnSpot setTitleColor:[ColorConstants appBrownColor] forState:UIControlStateNormal];
        btnSpot.layer.cornerRadius = btnSpot.layer.frame.size.height/2;
        btnSpot.titleLabel.font = [UIFont systemFontOfSize:12];
        UIButton *btnCancel = [[UIButton alloc] initWithFrame:CGRectMake(290, 1, 25, 25)];
        [btnCancel setTitle:@"X" forState:UIControlStateNormal];
        btnCancel.backgroundColor = [UIColor redColor];
        [btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnCancel.layer.cornerRadius = btnCancel.layer.frame.size.height/2;
        btnCancel.titleLabel.font = [UIFont systemFontOfSize:17];
        btnCancel.tag = section;
        [btnCancel addTarget:self action:@selector(deleteDay:) forControlEvents:UIControlEventTouchUpInside];
        NSDate * fromD;
        NSDate * toD;
        
        fromD = dTour.from_date;
        toD = dTour.to_date;
        
        Attractions * attr = [attractionArray objectAtIndex:section-1];
        
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        NSInteger dayCount = attr.day.integerValue;
        if (dayCount >= 1){
            dayCount = dayCount -1;
        }
        dayComponent.day = dayCount;
        NSCalendar *theCalendar = [NSCalendar currentCalendar];
        NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:fromD options:0];
        NSString *next = [Utils dateFormatyyyyMMdd:nextDate];
        NSString * day =[[Utils getDayFromDate:nextDate] substringToIndex:3];
        lblText.text =[NSString stringWithFormat:@"Day %ld - %@(%@)",(long)dayCount+1,next,day];// @"Day 1 2016-9-14 (WED)";
        
        
            // Arun :: Clinet wants to hide this button for now.
//        [view addSubview:btnSpot];
//        [view addSubview:btnCancel];
        [view addSubview:lblText];
        [view setBackgroundColor:[UIColor whiteColor]];
        return view;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0 || section == outerArray.count+1){
        return 0;
    }else{
        return 30;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}


-(void)handleLongPressGesture {
    [_tblView1 setEditing:YES animated:YES];
//    [_tblView1 reloadData];
}

-(void)playVideo:(id)sender {
    NSString * aStr = [NSString stringWithFormat:@"%ld", (long)[sender tag]];
    NSArray * tagArray = [aStr componentsSeparatedByString:@"99999"];
    NSString * a = [NSString stringWithFormat:@"%@",[tagArray firstObject]];
    NSString * b = [NSString stringWithFormat:@"%@",[tagArray lastObject]];
//    Attractions * attractions = [outerArray objectAtIndex:a.integerValue-1];
    AttractionData *aData = [[outerArray objectAtIndex:a.integerValue-1] objectAtIndex:b.integerValue];
    if (aData.videoId) {
        NSLog(@"%@",aData.videoId.file_name);
        NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetVideo,aData.videoId.file_name];
        [self playVideoWithURL:[NSURL URLWithString:urlString]];
    }
}

-(void)playVideoWithURL:(NSURL *)url {
    if (url) {
        self.videoController = [[MPMoviePlayerController alloc] init];
        [self.videoController setContentURL:url];
        [self.videoController.view setFrame:CGRectMake (0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.videoController.controlStyle = MPMovieControlStyleFullscreen;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoPlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:self.videoController];
        [self.view addSubview:self.videoController.view];
        [self.videoController play];
    }
}

- (void)videoPlayBackDidFinish:(NSNotification *)notification {
    //    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    [self.videoController stop];
    [self.videoController.view removeFromSuperview];
    self.videoController = nil;
}

- (IBAction)cancelDescClicked:(id)sender {
    [KGModalWrapper hideView];
}

- (IBAction)saveDescClicked:(id)sender {
    if (txtViewDesc.text.length <= 0) {
        [Utils showAlertWithMessage:@"Please enter description"];
    } else {
        NSArray * tagArray = [descTag componentsSeparatedByString:@"99999"];
        NSString * a = [NSString stringWithFormat:@"%@",[tagArray firstObject]];
        NSString * b = [NSString stringWithFormat:@"%@",[tagArray lastObject]];
        
        AttractionData *aData = [[outerArray objectAtIndex:a.integerValue-1] objectAtIndex:b.integerValue];
        aData.info.address = txtViewDesc.text;
        [_tblView1 reloadData];
        [KGModalWrapper hideView];
    }
}

-(void)editDesc:(id)sender {
    NSString * str = [NSString stringWithFormat:@"%ld",(long)[sender tag]];
    descTag = str;
    NSArray * tagArray = [str componentsSeparatedByString:@"99999"];
    NSString * a = [NSString stringWithFormat:@"%@",[tagArray firstObject]];
    NSString * b = [NSString stringWithFormat:@"%@",[tagArray lastObject]];
    AttractionData *aData = [[outerArray objectAtIndex:a.integerValue-1] objectAtIndex:b.integerValue];
    txtViewDesc.text = aData.info.address;
     lblManiDes =  [Utils roundCornersOnView:lblManiDes onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];
    [KGModalWrapper showWithContentView:EditDescView];
}


-(void)deleteAttr:(id)sender {
    NSString * str = [NSString stringWithFormat:@"%ld",(long)[sender tag]];
    
    NSArray * tagArray = [str componentsSeparatedByString:@"99999"];
    NSString * a = [NSString stringWithFormat:@"%@",[tagArray firstObject]];
    NSString * b = [NSString stringWithFormat:@"%@",[tagArray lastObject]];
    Attractions * attractions = [attractionArray objectAtIndex:a.integerValue-1];
    AttractionData *aData = [[outerArray objectAtIndex:a.integerValue-1] objectAtIndex:b.integerValue];
    
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Are you sure you want to delete this attraction?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [SVProgressHUD showWithStatus:@"Updating..."];
        
        NSString * dayStr = [NSString stringWithFormat:@"%@", attractions.day];
        [[PlayTripManager Instance] deleteAttractionWithId:aData.entity_id andplanId:dTour.entity_id andDay:dayStr WithBlock:^(id result, NSString *error) {
            if (error) {
                NSLog(@"%@", error);
            } else {
                //NSLog(@"%@", result);
            }
            [self loadAllPlansOfUser];
        }];
    }];
    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:yesAction];
    [alert addAction:noAction];
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}


-(void)deleteDay:(id)sender {
    NSString * str = [NSString stringWithFormat:@"%ld",(long)[sender tag]];
    NSArray * tagArray = [str componentsSeparatedByString:@"99999"];
    NSString * a = [NSString stringWithFormat:@"%@",[tagArray firstObject]];
    //    NSString * days = [NSString stringWithFormat:@"%ld",a.integerValue +1];
    
    Attractions *atr = [attractionArray objectAtIndex:a.integerValue-1];
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Are you sure you want to delete this Day?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [SVProgressHUD showWithStatus:@"Updating..."];
        [[PlayTripManager Instance] deleteDayForPlanId:dTour.entity_id andDay:atr.entity_id WithBlock:^(id result, NSString *error) {
            if (error) {
                NSLog(@"%@", error);
            } else {
                //NSLog(@"%@", result);
            }
            [self loadAllPlansOfUser];
        }];
    }];
    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:yesAction];
    [alert addAction:noAction];
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}


-(void)loadAllPlansOfUser {
    Account * account = [AccountManager Instance].activeAccount;
    [[PlayTripManager Instance] loadPlanByUserID:account.userId WithBlock:^(id result, NSString *error) {
//        Plan *updatedPlan = [Plan getByEntityId:_planMain.entity_id];
//        _planMain = updatedPlan;
//        attractionArray = [[_planMain.attractionsSet allObjects]mutableCopy];
//        NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"day"                                                                     ascending:YES];
//        NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
//        NSArray *sortedArray = [attractionArray sortedArrayUsingDescriptors:sortDescriptors];
//        attractionArray = [sortedArray mutableCopy];
        [self setArray];
        [SVProgressHUD dismiss];
    }];
}

-(void)saveToDeviceClicked {
//    [SVProgressHUD showWithStatus:@"Updating..."];
//    [self saveAttrDatainServer];
    
    NSIndexPath * ip = [NSIndexPath indexPathForRow:0 inSection:0];
    MyVideoTourCell *firstCell = (MyVideoTourCell*)[_tblView1 cellForRowAtIndexPath:ip];

    if (firstCell.txtName.text.length <= 0) {
        [Utils showAlertWithMessage:@"Please enter a valid tour name"];
        return;
    }
    
    NSDate * toDate;
    if (selectedToDate) {
        toDate = selectedToDate;
    } else {
        toDate = dTour.to_date;
    }
    
    NSDate * fromDate;
    if (selectedFromDate) {
        fromDate = selectedFromDate;
    } else {
        fromDate = dTour.from_date;
    }
    
    NSString * days = [Utils getDaysBetweenFromDate:fromDate andToDate:toDate];
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *daysNumber = [f numberFromString:days];
    
    
    
    dTour.name = firstCell.txtName.text;
    dTour.from_date = fromDate;
    dTour.to_date = toDate;
    dTour.travel_tip =  firstCell.txtTips.text;
    dTour.days = daysNumber;
    
    if ([firstCell.btnYes.currentImage isEqual:[UIImage imageNamed:@"RadioButton-Selected.png"]]) {
        dTour.self_drive = [NSNumber numberWithBool:YES];
    } else {
        dTour.self_drive = [NSNumber numberWithBool:NO];
    }
    
    [DraftTour saveEntity];
    [self.navigationController popToRootViewControllerAnimated:YES];
    [Utils showAlertWithMessage:@"Draft saved successfully"];
}

-(void)saveAttrDatainServer {
    NSMutableArray * finalAttrDataArray = [NSMutableArray new];
    for (Attractions * at in outerArray) {
        NSMutableDictionary * inDict = [NSMutableDictionary new];
        for (AttractionData * ad in [[at.attractionDataSet allObjects]mutableCopy]) {
            [inDict setValue:ad.entity_id forKey:@"_id"];
            [inDict setValue:ad.info.address forKey:@"desc"];
            [inDict setValue:ad.time forKey:@"time"];
            [finalAttrDataArray addObject:inDict];
        }
    }
    [[PlayTripManager Instance] updateMultipleAttrData:finalAttrDataArray ForPlanId:dTour.entity_id WithBlock:^(id result, NSString *error) {
        if (error) {
            NSLog(@"%@", error);
        } else {
            //NSLog(@"%@", result);
        }
        [self saveVideoTourInServer];
    }];
}

-(void)saveVideoTourInServer{
    NSDate * toDate;
    if (selectedToDate) {
        toDate = selectedToDate;
    } else {
        toDate = dTour.to_date;
    }
    
    NSDate * fromDate;
    if (selectedFromDate) {
        fromDate = selectedFromDate;
    } else {
        fromDate = dTour.from_date;
    }
    
    NSIndexPath * ip = [NSIndexPath indexPathForRow:0 inSection:0];
    MyVideoTourCell *firstCell = (MyVideoTourCell*)[_tblView1 cellForRowAtIndexPath:ip];
    NSString * travelTips =  firstCell.txtTips.text;
    NSString * from_date = [Utils strFromDate:fromDate];
    NSString * to_date = [Utils strFromDate:toDate];
    NSString * name = mainName;// firstCell.txtName.text;
    NSString * days = [Utils getDaysBetweenFromDate:fromDate andToDate:toDate];
    
    BOOL self_drive = false;
    if ([firstCell.btnYes.currentImage isEqual:[UIImage imageNamed:@"RadioButton-Selected.png"]]) {
        self_drive = true;
    }
    
    [[PlayTripManager Instance] editPlanWithId:dTour.entity_id WithName:name WithTravelTip:travelTips WithFromDate:from_date WithToDate:to_date WithDays:days andSelfDrive:self_drive WithBlock:^(id result, NSString *error) {
        if (error) {
            NSLog(@"%@", error);
        } else {
            //NSLog(@"%@", result);
        }
        if (isImageChanged) {
            [self saveThumbnailInServer];
        }else {
            [self goBack];
        }
    }];
}

-(void)saveThumbnailInServer {
    [[PlayTripManager Instance] uploadCoverPhoto:chosenImage ForPlanId:dTour.entity_id WithBlock:^(id result, NSString *error) {
        if (error) {
            NSLog(@"%@", error);
        } else {
            //NSLog(@"%@", result);
        }
        [self goBack];
    }];
}


-(void)createVideoTour {
    Account *account = [AccountManager Instance].activeAccount;
    
    if (![[AppDelegate appDelegate] isReachable]) {
        [Utils showAlertWithMessage:kCheckInternet];
        return;
    }else{
        if([account.wifiOnly boolValue]){
            if (![[AppDelegate appDelegate] isReachableViaWifi]) {
                [Utils showAlertWithMessage:@"Please check your wifi connection"];
                return;
                
            }else{
                [self videoTourDataUpload];
            }
        }else{
            [self videoTourDataUpload];
        }
    }
}

-(void)videoTourDataUpload{
    NSInteger days = [dTour.days integerValue];
    NSInteger j = 0;
    for (NSInteger i = 1; i<= days; i++) {
        for (Attractions * atr in outerArray) {
            if (atr.day.integerValue == i) {
                j++;
            }
        }
    }
    
    BOOL isVideoAbsent = false;
    if (j != days) {
        [Utils showAlertWithMessage:@"Attractions is not found in all days"];
    } else {
        for (Attractions * atr in outerArray) {
            for (AttractionData * aData in [[atr.attractionDataSet allObjects] mutableCopy]) {
                if (aData.videoId == nil) {
                    LocalVideoObject * vidOb = [LocalVideoObject getByEntityId:aData.entity_id];
                    if (vidOb == nil) {
                        isVideoAbsent = true;
                    }
                }
            }
        }
        if (isVideoAbsent) {
            [Utils showAlertWithMessage:@"Video is not found in all attractions"];
        } else {
//            CreateVideoTourViewController * controller = [CreateVideoTourViewController initViewControllerWithPlan:_planMain];
//            [self.navigationController pushViewController:controller animated:YES];
        }
    }

}

-(void)goBack {
    [SVProgressHUD dismiss];
    Account * account = [AccountManager Instance].activeAccount;
    [[PlayTripManager Instance] loadPlanByUserID:account.userId WithBlock:^(id result, NSString *error) {
    }];
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Saved Successfully" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }];
    [alert addAction:okayAction];
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end


/*
 
 - (IBAction)noClicked:(id)sender {
 [btnYes setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
 [btnNo setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
 selfDrive = true;
 }
 
 - (IBAction)uploadClicked:(id)sender {
 }
 
 - (IBAction)saveClicked:(id)sender {
 NSIndexPath * ip = [NSIndexPath indexPathForRow:0 inSection:0];
 MyVideoTourCell *topCell = (MyVideoTourCell *)[_tblView1 cellForRowAtIndexPath:ip];
 if(topCell.txtName.text.length <=0) {
 [Utils showAlertWithMessage:@"Please Enter Name"];
 } else if(topCell.lblToDate.text.length <=0) {
 [Utils showAlertWithMessage:@"Please Select Date"];
 } else if(topCell.lblFromDate.text.length <=0) {
 [Utils showAlertWithMessage:@"Please Select Date"];
 } else {
 if (_planMain) {
 _planMain.name  = topCell.txtName.text;
 _planMain.from_date = selectedFromDate;
 _planMain.to_date = selectedToDate;
 } else {
 _videotour.name  = topCell.txtName.text;
 _videotour.from_date = selectedFromDate;
 _videotour.to_date = selectedToDate;
 }
 }
 }
 
 - (IBAction)yesClicked:(id)sender {
 [btnNo setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
 [btnYes setImage:[UIImage imageNamed:@"RadioButton-Selected.png"]
 forState:UIControlStateNormal];
 selfDrive = true;
 }
 
 
 viewdidload
 
 fromView.layer.borderColor = [ColorConstants appBrownColor].CGColor;
 fromView.layer.borderWidth = 1;
 fromView.layer.cornerRadius = fromView.layer.frame.size.height/2;
 toView.layer.borderColor = [ColorConstants appBrownColor].CGColor;
 toView.layer.borderWidth = 1;
 toView.layer.cornerRadius = toView.layer.frame.size.height/2;
 
 travelTipView.layer.borderColor = [ColorConstants appBrownColor].CGColor;
 travelTipView.layer.borderWidth = 1;
 travelTipView.layer.cornerRadius = 8;
 
 
 viewwillappear
 btnSave.layer.cornerRadius = btnSave.frame.size.height / 2;
 btnUpload.layer.cornerRadius = btnUpload.frame.size.height / 2;
 
 */


#import "DraftTourViewController.h"
#import "CreateVideoTourViewController.h"
#import "LocalVideoObject.h"
#import "VideoId.h"
#import "KGModalWrapper.h"
#import "SVProgressHUD.h"
#import "Account.h"
#import "AccountManager.h"
#import "PlayTripManager.h"
#import "AirTicketCell.h"
#import "ItineraryTableViewCell.h"
#import "HotelBookingCell.h"
#import "ColorConstants.h"
#import "TotalView.h"
#import "URLSchema.h"
#import "Categori.h"
#import "Utils.h"
#import "MyVideoTourCell.h"
#import "VideoTourSaveCell.h"
#import "ActionSheetPicker.h"
#import "ActionSheetDatePicker.h"
#import "RouteMapViewController.h"
#import "AppDelegate.h"
#import "MapAttractionsViewController.h"
#import "VideoViewController.h"
#import "MapViewController.h"
#import "ProfileViewController.h"
#import "Localisator.h"

@interface DraftTourViewController ()
@end

@implementation DraftTourViewController

+(DraftTourViewController *)initViewController{
    DraftTourViewController * controller = [[DraftTourViewController alloc] initWithNibName:@"DraftTourViewController" bundle:nil];
    [controller setTitle:LOCALIZATION(@"my_video_tour_draft")];
    //controller.title = [NSString stringWithFormat:NSLocalizedString(@"my_video_tour_draft", nil)];

    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    tblMain.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self registerNib];
    [tblMain setHidden:NO];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopVideoPlay) name:@"HideKGView" object:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    [self setNavBarItems];
    dTour = [DraftTour getDraft];
    [self setArray];
}


-(void)stopVideoPlay {
    [self.videoController stop];
}

-(void)setArray {
    
    outerArray = [NSMutableArray new];
    atArray = [NSMutableArray new];
    
    NSMutableArray * localArray = [[dTour.draftAttractionsSet allObjects]mutableCopy];
    attractionArray = [NSMutableArray new];
    
    NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"day" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
    NSArray *sortedArray = [localArray sortedArrayUsingDescriptors:sortDescriptors];
    localArray = [sortedArray mutableCopy];
    attractionArray = localArray;
    
    for (int i = 1;i<=dTour.days.integerValue;i++) {
        NSMutableArray * tempArray = [NSMutableArray new];
        
        
        
        
        for (DraftAttraction * dAttrs in attractionArray) {
            if (dAttrs.day.integerValue == i) {
                for (DraftAttrData * dAD in [dAttrs.draftAttrDataSet allObjects]) {
                    
                    
                    //[tempArray addObject:@"123"];
                    
                    NSLog(@"Entity Description is %@" , dAD.desc);
                    
                    
                    [tempArray addObject:dAD];
                }
            }
        }
        
        
        
        tempArray = [[tempArray sortedArrayUsingComparator:^NSComparisonResult(DraftAttrData *a, DraftAttrData *b) {
            return [a.sequence intValue] > [b.sequence intValue];
        }] mutableCopy];
        
        /*
        
        for (DraftAttraction * dAttrs in attractionArray) {
            if (dAttrs.day.integerValue == i) {
                
                for (DraftAttrData * dAD in [dAttrs.draftAttrDataSet allObjects]) {
                    if ([dAD.sequence intValue] != 0) {
                        if ([[tempArray objectAtIndex:[dAD.sequence intValue] - 1] isKindOfClass:[NSString class]]) {
                            [tempArray replaceObjectAtIndex:[dAD.sequence intValue] - 1 withObject:dAD];
                            
                        }
                    }
                    else{
                        dAD.sequence = [NSNumber numberWithInteger:tempArray.count];
                        [tempArray addObject:dAD];
                    }
                }
                
                [tempArray removeObject:@"123"];
                
            }
        }
         */
        NSLog(@"tempArray Count is %lu" , (unsigned long)tempArray.count);

        
        /*
         for (DraftAttrData * dAD in [dAttrs.draftAttrDataSet allObjects]) {
         if ([dAD.sequence intValue] != 0) {
         if ([[tempArray objectAtIndex:[dAD.sequence intValue] - 1] isKindOfClass:[NSString class]]) {
         [tempArray replaceObjectAtIndex:[dAD.sequence intValue] - 1 withObject:dAD];
         
         }
         }
         else{
         dAD.sequence = [NSNumber numberWithInteger:tempArray.count];
         [tempArray addObject:dAD];
         }
         }
         
         [tempArray removeObject:@"123"];
         */
        
//        if (tempArray.count > 0) {
            [outerArray setObject:tempArray atIndexedSubscript:outerArray.count];
//        }
    }
    
    for (int i = 1;i<=dTour.days.integerValue;i++) {
        for (DraftAttraction * dAttrs in attractionArray) {
            if (dAttrs.day.integerValue == i) {
                [atArray addObject:dAttrs];
                break;
            }
        }
    }
    
    //NSLog(@"%@", outerArray);
    [tblMain reloadData];
}

-(void)setNavBarItems {
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[ColorConstants appBrownColor]}];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backClicked)];
    self.navigationItem.leftBarButtonItem = backButton;
    self.navigationController.navigationBar.tintColor = [ColorConstants appBrownColor];
    self.navigationController.navigationBar.translucent = NO;
    UIBarButtonItem * homeButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"home.png"] style:UIBarButtonItemStylePlain target:self action:@selector(homeClicked)];
    [self.navigationItem setRightBarButtonItem:homeButton];
}

-(void)homeClicked{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)backClicked{
    UIViewController * aController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
    if ([aController isKindOfClass:[ProfileViewController class]]) {
        [self.navigationController popViewControllerAnimated:NO];
    } else {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (void)registerNib{
    UINib *celliterery = [UINib nibWithNibName:@"ItineraryTableViewCell" bundle:nil];
    [tblMain registerNib:celliterery forCellReuseIdentifier:@"ItineraryTableViewCell"];
    UINib *cell = [UINib nibWithNibName:@"MyVideoTourCell" bundle:nil];
    [tblMain registerNib:cell forCellReuseIdentifier:@"MyVideoTourCell"];
    UINib *cell2 = [UINib nibWithNibName:@"VideoTourSaveCell" bundle:nil];
    [tblMain registerNib:cell2 forCellReuseIdentifier:@"VideoTourSaveCell"];
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.tag == 1) {
        mainName = textField.text;
    }
}

- (IBAction)toDatePicker:(id)sender{
    //    NSInteger days = [_planMain.days integerValue];
    NSInteger days = atArray.count-1;
    NSDate * minDate;
    NSDate * maxDate;

    NSCalendar *cal = [NSCalendar currentCalendar];

    
    if (selectedFromDate) {
        minDate = [Utils addDays:days toDate:selectedFromDate];
        
        

        
    } else {
        minDate = [Utils addDays:days toDate:dTour.from_date];
        
        
        
    }
    
    
    
    NSDate *max3Months = [cal dateByAddingUnit:NSCalendarUnitMonth value:6 toDate:minDate options:0];
    
    maxDate = max3Months;
    
    [ActionSheetDatePicker showPickerWithTitle:@"Select Date" datePickerMode:UIDatePickerModeDate selectedDate:[NSDate date] minimumDate:minDate maximumDate:maxDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        selectedToDate = selectedDate;
        NSIndexPath * ip = [NSIndexPath indexPathForRow:0 inSection:0];
        MyVideoTourCell *topCell = (MyVideoTourCell *)[tblMain cellForRowAtIndexPath:ip];
        topCell.lblToDate.text = [NSString stringWithFormat:@"%@(%@)",[Utils dateFormatMMddyyyy:selectedDate],[Utils getDayShortFromDate:selectedDate]];
        [tblMain reloadRowsAtIndexPaths:@[ip] withRowAnimation:UITableViewRowAnimationFade];
        [self saveTour];
        [self setArray];
    } cancelBlock:nil origin:sender];
}

- (IBAction)fromDatePicker:(id)sender{
    
    NSCalendar *cal = [NSCalendar currentCalendar];

    
    
NSInteger days = dTour.days.integerValue-1;
//        NSInteger days = atArray.count;
    NSDate * maxDate;
    if (selectedToDate) {
        maxDate = [Utils reduceDays:days toDate:selectedToDate];
    } else {
        maxDate = [Utils reduceDays:days toDate:dTour.to_date];
    }
    
    NSDate *min3Months = [cal dateByAddingUnit:NSCalendarUnitMonth value:-6 toDate:maxDate options:0];

    
    [ActionSheetDatePicker showPickerWithTitle:@"Select Date" datePickerMode:UIDatePickerModeDate selectedDate:dTour.from_date minimumDate:min3Months maximumDate:maxDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        selectedFromDate = selectedDate;
        NSIndexPath * ip = [NSIndexPath indexPathForRow:0 inSection:0];
        MyVideoTourCell *topCell = (MyVideoTourCell *)[tblMain cellForRowAtIndexPath:ip];
        topCell.lblFromDate.text = [NSString stringWithFormat:@"%@(%@)",[Utils dateFormatMMddyyyy:selectedDate],[Utils getDayShortFromDate:selectedDate]];
        [tblMain reloadRowsAtIndexPaths:@[ip] withRowAnimation:UITableViewRowAnimationFade];
    } cancelBlock:nil origin:sender];
}

- (IBAction)languagePicker:(id)sender{
    
    NSInteger days = dTour.days.integerValue-1;
    //        NSInteger days = atArray.count;
    NSDate * maxDate;
    if (selectedToDate) {
        maxDate = [Utils reduceDays:days toDate:selectedToDate];
    } else {
        maxDate = [Utils reduceDays:days toDate:dTour.to_date];
    }
    int alreadySelected = 0;
    
    NSArray *langArray = [NSArray arrayWithObjects:@"English",@"粵",@"普", nil];
    
    if([dTour.language  isEqualToString: @"sc"]){
        alreadySelected = 2;
    }else if([dTour.language  isEqualToString: @"tc"]){
        alreadySelected = 1;
    }else{
        alreadySelected = 0;
    }
    [ActionSheetStringPicker showPickerWithTitle:LOCALIZATION(@"select_language") rows:langArray initialSelection:alreadySelected  doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        if (selectedIndex == 0) {
            dTour.language = @"en";
        }
        else if (selectedIndex == 1) {
            dTour.language = @"tc";
        }
        else {
            dTour.language = @"sc";
        }
        
        
        
        NSIndexPath * ip = [NSIndexPath indexPathForRow:0 inSection:0];
        MyVideoTourCell *firstCell = (MyVideoTourCell *)[tblMain cellForRowAtIndexPath:ip];
        
        if([dTour.language  isEqualToString: @"sc"]){
            [firstCell.imgSpeaker setImage:[UIImage imageNamed:@"speaker_.png"]];
        }else if([dTour.language  isEqualToString: @"tc"]){
            [firstCell.imgSpeaker setImage:[UIImage imageNamed:@"speaker_2.png"]];
        }else{
            [firstCell.imgSpeaker setImage:[UIImage imageNamed:@"speaker_Eng.png"]];
        }
        [tblMain reloadRowsAtIndexPaths:@[ip] withRowAnimation:UITableViewRowAnimationFade];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        NSLog(@"Block Picker Canceled");
    } origin:sender];

}



-(void)openTimePicker:(id)sender {
    pickerTag = [NSString stringWithFormat:@"%ld",(long)[sender tag]];
    ActionSheetDatePicker *datePicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select a Start time" datePickerMode:UIDatePickerModeTime selectedDate:[NSDate date] target:self action:@selector(timeWasSelected:element:) origin:sender];
    [datePicker showActionSheetPicker];
}



-(void)openEndTimePicker:(id)sender {
    pickerTag = [NSString stringWithFormat:@"%ld",(long)[sender tag]];
    ActionSheetDatePicker *datePicker = [[ActionSheetDatePicker alloc] initWithTitle:@"Select an End time" datePickerMode:UIDatePickerModeTime selectedDate:[NSDate date] target:self action:@selector(endTimeWasSelected:element:) origin:sender];
    [datePicker showActionSheetPicker];
}


-(void)timeWasSelected:(NSDate *)selectedTime element:(id)element {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"h:mm a"];
    NSString * aAtr = [dateFormatter stringFromDate:selectedTime];
    NSArray * tagArray = [pickerTag componentsSeparatedByString:@"99999"];
    NSString * a = [NSString stringWithFormat:@"%@",[tagArray firstObject]];
    NSString * b = [NSString stringWithFormat:@"%@",[tagArray lastObject]];
    DraftAttrData *aData = [[outerArray objectAtIndex:a.integerValue-1] objectAtIndex:b.integerValue];
    NSArray *bothDates = [aData.time componentsSeparatedByString:@"-"];
    if (bothDates.count == 2) {
        NSString *endDate = [bothDates lastObject];
        aData.time = [NSString stringWithFormat:@"%@ - %@",aAtr , endDate ];
    }
    else{
        aData.time = aAtr;
    
    }
    [tblMain reloadData];
}

-(void)endTimeWasSelected:(NSDate *)selectedTime element:(id)element {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"h:mm a"];
    NSString * aAtr = [dateFormatter stringFromDate:selectedTime];
    NSArray * tagArray = [pickerTag componentsSeparatedByString:@"99999"];
    NSString * a = [NSString stringWithFormat:@"%@",[tagArray firstObject]];
    NSString * b = [NSString stringWithFormat:@"%@",[tagArray lastObject]];
    DraftAttrData *aData = [[outerArray objectAtIndex:a.integerValue-1] objectAtIndex:b.integerValue];
    NSArray *bothDates = [aData.time componentsSeparatedByString:@"-"];
    if (bothDates.count == 2) {
        NSString *startDate = [bothDates firstObject];
        aData.time = [NSString stringWithFormat:@"%@ to%@", startDate, aAtr ];
    }
    else{
        aData.time = aAtr;
        
    }
    
    [tblMain reloadData];
}

- (void)editImageClicked:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *openCamrea = [UIAlertAction actionWithTitle:@"Open Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            [Utils showAlertWithMessage:@"Device has no camera"];
        } else {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:NULL];
        }
    }];
    
    UIAlertAction *openGallery = [UIAlertAction actionWithTitle:@"Open Gallery"  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
    }];
    
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:openCamrea];
    [alert addAction:openGallery];
    [alert addAction:cancel];
    
    [alert setModalPresentationStyle:UIModalPresentationPopover];
    //UIPopoverPresentationController *popPresenter = [alert popoverPresentationController];
    //popPresenter.sourceView = [[UIView alloc]initWithFrame:CGRectMake(50, 100, 100, 200)];
    //popPresenter.sourceRect = CGRectMake(50, 100, 100, 200);
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    chosenImage = info[UIImagePickerControllerEditedImage];
    
    NSString *docDirPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath =  [docDirPath stringByAppendingPathComponent:@"draftThumbnail.png"];
    NSLog (@"File Path = %@", filePath);
    
    // Get PNG data from following method
    NSData *myData =     UIImagePNGRepresentation(chosenImage);
    [myData writeToFile:filePath atomically:YES];
    dTour.thumbnail = filePath;
    [DraftTour saveEntity];
    
    isImageChanged = YES;
    NSIndexPath * ip = [NSIndexPath indexPathForRow:0 inSection:0];
    MyVideoTourCell *topCell = (MyVideoTourCell *)[tblMain cellForRowAtIndexPath:ip];
    topCell.imgMain.image = chosenImage;
    [tblMain reloadRowsAtIndexPaths:@[ip] withRowAnimation:UITableViewRowAnimationFade];
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)routeClicked {
    //[Utils showAlertWithMessage:@"Under Construction!!!"];
    
    PlanModel* pm = [[PlanModel alloc]initWithDraftTour:dTour];
    RouteMapViewController* vc = [RouteMapViewController initViewControllerWithPlanModel:pm];
    [self.navigationController pushViewController:vc animated:true];
}

#pragma mark -- Tableview Delegate and DataSource method

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return outerArray.count+2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0 || section == outerArray.count+1){
        return 1;
    }else{
        if(outerArray.count <=0){
            return 1;
        }else{
            return [[outerArray objectAtIndex:section-1] count];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        MyVideoTourCell *firstCell = (MyVideoTourCell*)[tblMain dequeueReusableCellWithIdentifier:@"MyVideoTourCell" forIndexPath:indexPath];
        firstCell.txtName.text = dTour.name;
        firstCell.txtName.tag = 1;
        firstCell.txtName.delegate = self;
        mainName = dTour.name;
        [firstCell.btnRoute addTarget:self action:@selector(routeClicked) forControlEvents:UIControlEventTouchUpInside];
        
        if(![dTour.travel_tip isEqualToString:@""] && dTour.travel_tip != nil){
            firstCell.txtTips.text = dTour.travel_tip;
            [firstCell.txtTips setTextColor:[UIColor blackColor]];
        }
            
        NSDate * fromD;
        NSDate * toD;
        
        fromD = dTour.from_date;
        toD = dTour.to_date;
        
        if (dTour.self_drive.boolValue) {
            [firstCell yesClicked:self];
        } else {
            [firstCell noClicked:self];
        }
        
        if(selectedFromDate!= nil){
            firstCell.lblFromDate.text = [NSString stringWithFormat:@"%@(%@)",[Utils dateFormatMMddyyyy:selectedFromDate],[Utils getDayShortFromDate:selectedFromDate]];
        }else{
            firstCell.lblFromDate.text = [NSString stringWithFormat:@"%@(%@)",[Utils dateFormatMMddyyyy:fromD],[Utils getDayShortFromDate:fromD]];
        }
        
        if(selectedToDate != nil){
            firstCell.lblToDate.text = [NSString stringWithFormat:@"%@(%@)",[Utils dateFormatMMddyyyy:selectedToDate],[Utils getDayShortFromDate:selectedToDate]];
        }else{
            firstCell.lblToDate.text = [NSString stringWithFormat:@"%@(%@)",[Utils dateFormatMMddyyyy:toD],[Utils getDayShortFromDate:toD]];
        }
        
        if(dTour.thumbnail.length > 0){
            firstCell.imgMain.image = [UIImage imageWithContentsOfFile:dTour.thumbnail];
            chosenImage = [UIImage imageWithContentsOfFile:dTour.thumbnail];
        }
        
        if(chosenImage!=nil){
            firstCell.imgMain.image = chosenImage;
        }
        
        [firstCell.btnMainImage addTarget:self action:@selector(editImageClicked:) forControlEvents:UIControlEventTouchUpInside];
        [firstCell.btnToDate addTarget:self action:@selector(toDatePicker:) forControlEvents:UIControlEventTouchUpInside];
        [firstCell.btnFromDate addTarget:self action:@selector(fromDatePicker:) forControlEvents:UIControlEventTouchUpInside];
        firstCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        
        if([dTour.language  isEqualToString: @"sc"]){
            [firstCell.imgSpeaker setImage:[UIImage imageNamed:@"speaker_.png"]];
        }else if([dTour.language  isEqualToString: @"tc"]){
            [firstCell.imgSpeaker setImage:[UIImage imageNamed:@"speaker_2.png"]];
        }else{
            [firstCell.imgSpeaker setImage:[UIImage imageNamed:@"speaker_Eng.png"]];
        }
        
        
        [firstCell.btnSpeaker addTarget:self action:@selector(languagePicker:) forControlEvents:UIControlEventTouchUpInside];

        
        
        // Localizable String
        [firstCell.lblRouteMap setText:LOCALIZATION(@"route_map")];
        [firstCell.lblDate setText:LOCALIZATION(@"date_with_colon")];
        [firstCell.lblTo setText:LOCALIZATION(@"to")];
        [firstCell.lblSelfDrive setText:LOCALIZATION(@"self_drive")];
        [firstCell.lblYes setText:LOCALIZATION(@"yes")];
        [firstCell.lblNo setText:LOCALIZATION(@"no")];
        [firstCell.lblTraveltips setText:LOCALIZATION(@"travel_tip")];
        [firstCell.lblItinerary setText:LOCALIZATION(@"tab_itinarey")];
        
        /*
        firstCell.lblRouteMap.text = [NSString stringWithFormat:NSLocalizedString(@"route_map", nil)];
        firstCell.lblDate.text = [NSString stringWithFormat:NSLocalizedString(@"date_with_colon", nil)];
        firstCell.lblTo.text = [NSString stringWithFormat:NSLocalizedString(@"to", nil)];
        firstCell.lblSelfDrive.text = [NSString stringWithFormat:NSLocalizedString(@"self_drive", nil)];
        firstCell.lblYes.text = [NSString stringWithFormat:NSLocalizedString(@"yes", nil)];
        firstCell.lblNo.text = [NSString stringWithFormat:NSLocalizedString(@"no", nil)];
        firstCell.lblTraveltips.text = [NSString stringWithFormat:NSLocalizedString(@"travel_tip", nil)];
        firstCell.lblItinerary.text = [NSString stringWithFormat:NSLocalizedString(@"tab_itinarey", nil)];
        */
        
        return firstCell;
    }
    else if (indexPath.section == outerArray.count+1) {
        VideoTourSaveCell *cell = (VideoTourSaveCell*)[tblMain dequeueReusableCellWithIdentifier:@"VideoTourSaveCell" forIndexPath:indexPath];
        [cell.btnContinueVideoTour addTarget:self action:@selector(continueVideoTourClicked) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnDeleteDraft addTarget:self action:@selector(deleteDraftClicked) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnSaveToDevice addTarget:self action:@selector(saveToDeviceClicked) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnUpload addTarget:self action:@selector(createVideoTour) forControlEvents:UIControlEventTouchUpInside];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        
        // Localizable String
        
        [cell.lblContinueVidTour setTitle:LOCALIZATION(@"continue_video") forState:UIControlStateNormal];
        [cell.btnSaveToDevice setTitle:LOCALIZATION(@"save_draft") forState:UIControlStateNormal];
        [cell.btnDeleteDraft setTitle:LOCALIZATION(@"delete_draft") forState:UIControlStateNormal];
        [cell.btnUpload setTitle:LOCALIZATION(@"create_video_tour") forState:UIControlStateNormal];

        return cell;
    }
    else {
        ItineraryTableViewCell *cell = (ItineraryTableViewCell*)[tblMain dequeueReusableCellWithIdentifier:@"ItineraryTableViewCell" forIndexPath:indexPath];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.bookmarkView.hidden = YES;
        cell.viewsView.hidden = YES;
        cell.shareView.hidden = YES;
        NSDate * fromD;
        NSDate * toD;
        
        fromD = dTour.from_date;
        toD = dTour.to_date;
        
        [cell.btnTime addTarget:self action:@selector(openTimePicker:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btnEndTime addTarget:self action:@selector(openEndTimePicker:) forControlEvents:UIControlEventTouchUpInside];

        //        Attractions * attr = [outerArray objectAtIndex:indexPath.section-1];
        DraftAttrData *aData = [[outerArray objectAtIndex:indexPath.section-1] objectAtIndex:indexPath.row];
        cell.lblTime.text = aData.time;
        
        
        if ([aData.time containsString:@"to"]) {
            NSArray *bothDates = [aData.time componentsSeparatedByString:@"-"];
            if (bothDates.count == 2) {
                NSString *startDate = [bothDates firstObject];
                NSString *endDate = [bothDates lastObject];

                startDate = [startDate stringByReplacingOccurrencesOfString:@" " withString:@""];
                endDate = [endDate stringByReplacingOccurrencesOfString:@" " withString:@""];

                NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                [formatter setDateFormat:@"hh:mma"];
                
                
                NSDate *startDateNew = [formatter dateFromString:startDate];
                NSDate *endDateNew = [formatter dateFromString:endDate];
                
                
                
                NSDateFormatter *newFormatter = [[NSDateFormatter alloc]init];
                [newFormatter setDateFormat:@"HH:mm"];
                
                NSString *newStringTime = [NSString stringWithFormat:@"%@ %@ %@" ,[newFormatter stringFromDate:startDateNew], LOCALIZATION(@"to"), [newFormatter stringFromDate:endDateNew]];

                NSLog(@"%@" , newStringTime);
                cell.lblTime.text = newStringTime;
                
                
            }
        }
        
        
        
        
        
        
        NSString * tagStr = [NSString stringWithFormat:@"%ld99999%ld", (long)indexPath.section,(long)indexPath.row];
        cell.btnDesc.tag = tagStr.integerValue;
        cell.btnTime.tag = tagStr.integerValue;
        cell.btnEndTime.tag = tagStr.integerValue;

        cell.btnGreyDelete.tag = tagStr.integerValue;
        cell.btnPlayVideo.tag = tagStr.integerValue;
        cell.bookMarkButton.hidden = YES;
        if (aData.draftVideoId.thumbnail.length > 0) {
            //NSLog(@"%@",aData.draftVideoId.file_name);
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,aData.draftVideoId.thumbnail];
            cell.imgFull.url = [NSURL URLWithString:urlString];
        }else{
            cell.imgFull.image = [UIImage imageNamed:@"no_video.png"];
        }
        
        [cell.btnDesc addTarget:self action:@selector(editDesc:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnGreyDelete addTarget:self action:@selector(deleteAttr:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnPlayVideo addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.lblName.text = aData.draftInfo.name;
        cell.lblDescription.text = aData.desc;
//        NSMutableArray *catArray = [[NSMutableArray alloc] init];
//        catArray = [[aData.draftInfo.categori allObjects] mutableCopy];
//        if(catArray.count >0) {
//            Categori *cat =[catArray objectAtIndex:0];
//            NSString * imgId=cat.image;
//            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
//            cell.imgCategory.url = [NSURL URLWithString:urlString];
//            cell.imgCategory.layer.cornerRadius = cell.imgCategory.frame.size.width / 2;
//            cell.imgCategory.layer.masksToBounds = YES;
//        } else {
//            cell.imgCategory.backgroundColor = [UIColor grayColor];
//            cell.imgCategory.layer.cornerRadius = cell.imgCategory.frame.size.width / 2;
//            cell.imgCategory.layer.masksToBounds = YES;
//        }
        cell.imgCategory.hidden = YES;
        
        UILongPressGestureRecognizer* longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPressGesture)];
        longPressGesture.minimumPressDuration = .5; //seconds
        longPressGesture.delegate = self;
        [cell addGestureRecognizer:longPressGesture];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        return 410;
    } else if(indexPath.section == outerArray.count+1){
        return 200;
    }else{
        return 80;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if(section == 0  || section == outerArray.count+1){
        return [[UIView alloc]initWithFrame:CGRectZero];
    }else{
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 30)];
        UIImageView *imgWall = [[UIImageView alloc] initWithFrame:CGRectMake(11,0,30,30)];
        imgWall.image = [UIImage imageNamed:@"circle"];
        [view addSubview:imgWall];
        UILabel *lblText = [[UILabel alloc] initWithFrame:CGRectMake(46, 5, [[UIScreen mainScreen] bounds].size.width-100, 20)];
        lblText.font = [UIFont systemFontOfSize:12];
        lblText.textColor = [ColorConstants appBrownColor];
        UIButton *btnSpot = [[UIButton alloc] initWithFrame:CGRectMake(230, 3, 50, 20)];
        [btnSpot setTitle:@"+Spot" forState:UIControlStateNormal];
        btnSpot.backgroundColor = [ColorConstants appYellowColor];
        [btnSpot setTitleColor:[ColorConstants appBrownColor] forState:UIControlStateNormal];
        btnSpot.layer.cornerRadius = btnSpot.layer.frame.size.height/2;
        btnSpot.titleLabel.font = [UIFont systemFontOfSize:12];
        UIButton *btnCancel = [[UIButton alloc] initWithFrame:CGRectMake(290, 1, 25, 25)];
        [btnCancel setTitle:@"X" forState:UIControlStateNormal];
        btnCancel.backgroundColor = [UIColor redColor];
        [btnCancel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        btnCancel.layer.cornerRadius = btnCancel.layer.frame.size.height/2;
        btnCancel.titleLabel.font = [UIFont systemFontOfSize:17];
        btnCancel.tag = section;
        [btnCancel addTarget:self action:@selector(deleteDay:) forControlEvents:UIControlEventTouchUpInside];
        NSDate * fromD;
        NSDate * toD;
        
        fromD = dTour.from_date;
        toD = dTour.to_date;
        
//        DraftAttraction * attr = [attractionArray objectAtIndex:section-1];
        
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
//        NSInteger dayCount = attr.day.integerValue;
        NSInteger dayCount = section-1;
//        if (dayCount < 1){
//            dayCount = dayCount -1;
//        }
        dayComponent.day = dayCount;
        NSCalendar *theCalendar = [NSCalendar currentCalendar];
        NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:fromD options:0];
        NSString *next = [Utils dateFormatyyyyMMdd:nextDate];
        NSString * day =[[Utils getDayFromDate:nextDate] substringToIndex:3];
        lblText.text =[NSString stringWithFormat:@"Day %ld - %@(%@)",(long)dayCount+1,next,day];// @"Day 1 2016-9-14 (WED)";
        
        
        // Arun :: Clinet wants to hide this button for now.
        //        [view addSubview:btnSpot];
        //        [view addSubview:btnCancel];
        [view addSubview:lblText];
        [view setBackgroundColor:[UIColor whiteColor]];
        return view;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0 || section == outerArray.count+1){
        return 0;
    }else{
        return 30;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}

- (NSIndexPath *)tableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath*)sourceIndexPath
       toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath{
    
    
    if (proposedDestinationIndexPath.section == 0  || proposedDestinationIndexPath.section == outerArray.count+1) {
        return sourceIndexPath;
    } else {
        return proposedDestinationIndexPath;
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 || indexPath.section == outerArray.count + 1) {
        return NO;
    }
    return YES;
}

-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleNone;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    [tblMain setEditing:NO animated:YES];
    //NSLog(@"moveRowAtIndexPath  %lu",(unsigned long)outerArray.count);
    DraftAttrData * obj = [[outerArray objectAtIndex:sourceIndexPath.section-1] objectAtIndex:sourceIndexPath.row];
    
    
    
    if (sourceIndexPath.section == destinationIndexPath.section) {
        DraftAttrData * objOther = [[outerArray objectAtIndex:destinationIndexPath.section-1] objectAtIndex:destinationIndexPath.row];

        objOther.sequence = [NSNumber numberWithInteger:sourceIndexPath.row + 1];
        obj.sequence = [NSNumber numberWithInteger:destinationIndexPath.row + 1];
    }
    else{
        
        
        
        
        
    }
    
    
    
    
    
    [[outerArray objectAtIndex:sourceIndexPath.section-1] removeObjectAtIndex:sourceIndexPath.row];
    [[outerArray objectAtIndex:destinationIndexPath.section-1] insertObject:obj atIndex:destinationIndexPath.row];
    
    
    
    

    
//    DraftAttraction * dAttrS = [attractionArray objectAtIndex:sourceIndexPath.section-1];
    //    [dAttrS removeDraftAttrDataObject:obj];
    //    [DraftAttraction saveEntity];
    
   /* DraftAttraction * dAttrD = [atArray objectAtIndex:destinationIndexPath.section-1];
    
    [dAttrD addDraftAttrDataObject:obj];
    [DraftAttraction saveEntity];

    [obj setDraftAttractions:dAttrD];
    [DraftAttrData saveEntity];
    
    [self setArray];*/
    // I just commented code because send IPA today , we have to store in Database.

//    [tblMain reloadData];
}

-(void)handleLongPressGesture {
    [tblMain setEditing:YES animated:YES];
    //    [_tblView1 reloadData];
}

-(void)playVideo:(id)sender {
    NSString * aStr = [NSString stringWithFormat:@"%ld", (long)[sender tag]];
    NSArray * tagArray = [aStr componentsSeparatedByString:@"99999"];
    NSString * a = [NSString stringWithFormat:@"%@",[tagArray firstObject]];
    NSString * b = [NSString stringWithFormat:@"%@",[tagArray lastObject]];
    //    Attractions * attractions = [outerArray objectAtIndex:a.integerValue-1];
    DraftAttrData *aData = [[outerArray objectAtIndex:a.integerValue-1] objectAtIndex:b.integerValue];
    if (aData.draftVideoId.file_name.length > 0) {
        //NSLog(@"%@",aData.draftVideoId.file_name);
        
        NSString * imgId = aData.draftVideoId.file_name;
        
        if (imgId) {
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetMergedVideo,imgId];
            
            
            [self playVideoInPopUpViewWithURL:[NSURL URLWithString:urlString]];
        }
        else{
            [Utils showAlertWithMessage:@"No Video Found"];
        }
        
    }
}



-(void)playVideoWithURL:(NSURL *)url {
    if (url) {
        [SVProgressHUD dismiss];
        self.videoController = [[MPMoviePlayerController alloc] init];
        [self.videoController setContentURL:url];
        [self.videoController.view setFrame:CGRectMake (0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.videoController.controlStyle = MPMovieControlStyleDefault;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoPlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:self.videoController];
        [self.view addSubview:self.videoController.view];
        [self.videoController play];
    }
}

-(void)playVideoInPopUpViewWithURL:(NSURL *)url {
    if (url) {
        self.videoController = [[MPMoviePlayerController alloc] init];
        [self.videoController setContentURL:url];
        [self.videoController.view setFrame:CGRectMake (0, 0, popUpView.frame.size.width, popUpView.frame.size.height)];
        self.videoController.controlStyle = MPMovieControlStyleNone;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoPlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:self.videoController];
        [popUpView addSubview:self.videoController.view];
        [KGModalWrapper showWithContentView:popUpView];
        [self.videoController setControlStyle:MPMovieControlStyleNone];
        [self.videoController play];
    }
}

- (void)videoPlayBackDidFinish:(NSNotification *)notification {
    //    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    [self.videoController stop];
    [self.videoController.view removeFromSuperview];
    self.videoController = nil;
    [KGModalWrapper hideView];

}

-(void)editDesc:(id)sender {
    NSString * str = [NSString stringWithFormat:@"%ld",(long)[sender tag]];
    descTag = str;
    NSArray * tagArray = [str componentsSeparatedByString:@"99999"];
    NSString * a = [NSString stringWithFormat:@"%@",[tagArray firstObject]];
    NSString * b = [NSString stringWithFormat:@"%@",[tagArray lastObject]];
    DraftAttrData *aData = [[outerArray objectAtIndex:a.integerValue-1] objectAtIndex:b.integerValue];
    txtViewDesc.text = aData.draftInfo.detail;
    lblManiDes =  [Utils roundCornersOnView:lblManiDes onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];
    [KGModalWrapper showWithContentView:EditDescView];
}


- (IBAction)cancelDescClicked:(id)sender {
    [KGModalWrapper hideView];
}

- (IBAction)saveDescClicked:(id)sender {
    if (txtViewDesc.text.length <= 0) {
        [Utils showAlertWithMessage:@"Please enter description"];
    } else {
        NSArray * tagArray = [descTag componentsSeparatedByString:@"99999"];
        NSString * a = [NSString stringWithFormat:@"%@",[tagArray firstObject]];
        NSString * b = [NSString stringWithFormat:@"%@",[tagArray lastObject]];
        
        DraftAttrData *aData = [[outerArray objectAtIndex:a.integerValue-1] objectAtIndex:b.integerValue];
        aData.draftInfo.detail = txtViewDesc.text;
        [tblMain reloadData];
        [KGModalWrapper hideView];
    }
}

-(void)deleteAttr:(id)sender {
    NSString * str = [NSString stringWithFormat:@"%ld",(long)[sender tag]];
    
    NSArray * tagArray = [str componentsSeparatedByString:@"99999"];
    NSString * a = [NSString stringWithFormat:@"%@",[tagArray firstObject]];
    NSString * b = [NSString stringWithFormat:@"%@",[tagArray lastObject]];
//    DraftAttraction * attractions = [outerArray objectAtIndex:a.integerValue-1];
    DraftAttrData *aData = [[outerArray objectAtIndex:a.integerValue-1] objectAtIndex:b.integerValue];
    
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Are you sure you want to delete this attraction?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [DraftAttrData deleteObj:aData];
        [DraftAttrData saveEntity];
        dTour = [DraftTour getDraft];
        [self setArray];
        [tblMain reloadData];
    }];
    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:yesAction];
    [alert addAction:noAction];
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}

-(void)deleteDay:(id)sender {
    NSString * str = [NSString stringWithFormat:@"%ld",(long)[sender tag]];
    NSArray * tagArray = [str componentsSeparatedByString:@"99999"];
    NSString * a = [NSString stringWithFormat:@"%@",[tagArray firstObject]];
    //    NSString * days = [NSString stringWithFormat:@"%ld",a.integerValue +1];
    
    DraftAttraction *atr = [outerArray objectAtIndex:a.integerValue-1];
    
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Are you sure you want to delete this Day?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [SVProgressHUD showWithStatus:@"Updating..."];
        [DraftAttraction deleteObj:atr];
        [self setArray];
        [tblMain reloadData];
    }];
    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:yesAction];
    [alert addAction:noAction];
    [self.navigationController presentViewController:alert animated:YES completion:nil];
}

-(void)continueVideoTourClicked{
VideoViewController * controller = [VideoViewController initViewController];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)deleteDraftClicked {

    NSString * msg1 = [NSString stringWithFormat:@"Are you sure you want to delete %@", dTour.name];
    UIAlertController * alert1 = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:msg1 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * yesAction1 = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
        NSString * msg2 = [NSString stringWithFormat:@"Really? This process is irreversible!"];
        UIAlertController * alert2 = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:msg2 preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * yesAction2 = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [DraftVideoId deleteAll];
            [DraftInfo deleteAll];
            [DraftAttrData deleteAll];
            [DraftAttraction deleteAll];
            [DraftTour deleteObj:dTour];
            [self.navigationController popToRootViewControllerAnimated:YES];
        }];
        UIAlertAction * noAction2 = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:nil];
        [alert2 addAction:yesAction2];
        [alert2 addAction:noAction2];
        
        [self.navigationController presentViewController:alert2 animated:YES completion:nil];
    }];
    UIAlertAction * noAction1 = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:nil];
    [alert1 addAction:yesAction1];
    [alert1 addAction:noAction1];
    
    [self.navigationController presentViewController:alert1 animated:YES completion:nil];
    
}

-(void) addAttractionClicked {
//    MapAttractionsViewController * controller = [MapAttractionsViewController initViewController];
    MapViewController * controller = [MapViewController initViewController];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)saveToDeviceClicked {
    [self saveTour];
    [self.navigationController popToRootViewControllerAnimated:YES];
    [Utils showAlertWithMessage:@"Draft saved successfully"];
}

-(void)saveOuterArray {
    for (NSMutableArray * aArray in outerArray) {
        //NSLog(@"%@", aArray);
        for (DraftAttrData * dAttrData in aArray) {
            //NSLog(@"%@", dAttrData);
            NSInteger indexA = [outerArray indexOfObject:aArray];
            //NSLog(@"%ld", (long)indexA);
            if (dAttrData.draftAttractions.day.integerValue == indexA+1) {
                //NSLog(@"yes day");
            } else {
                DraftAttraction * dAttr = dAttrData.draftAttractions;
                [dAttr setDay:[NSNumber numberWithInteger:indexA+1]];
                [DraftAttraction saveEntity];
                
                //NSLog(@"no day");
            }
        }
    }
}

-(void)saveTour{
    [self saveOuterArray];
    NSIndexPath * ip = [NSIndexPath indexPathForRow:0 inSection:0];
    MyVideoTourCell *firstCell = (MyVideoTourCell*)[tblMain cellForRowAtIndexPath:ip];
    
    if (mainName.length <= 0) {
        [Utils showAlertWithMessage:@"Please enter a valid tour name"];
        return;
    }
    
    NSDate * toDate;
    if (selectedToDate) {
        toDate = selectedToDate;
    } else {
        toDate = dTour.to_date;
    }
    
    NSDate * fromDate;
    if (selectedFromDate) {
        fromDate = selectedFromDate;
    } else {
        fromDate = dTour.from_date;
    }
    
    NSString * days = [Utils getDaysBetweenFromDate:fromDate andToDate:toDate];
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSNumber *daysNumber = [f numberFromString:days];
    
    dTour.name = mainName;
    dTour.from_date = fromDate;
    dTour.to_date = toDate;
    dTour.travel_tip =  firstCell.txtTips.text;
    dTour.days = daysNumber;
    
    if ([firstCell.btnYes.currentImage isEqual:[UIImage imageNamed:@"RadioButton-Selected.png"]]) {
        dTour.self_drive = [NSNumber numberWithBool:YES];
    } else {
        dTour.self_drive = [NSNumber numberWithBool:NO];
    }
    
    [DraftTour saveEntity];
}



-(void)createVideoTour {
    if (!chosenImage) {
        [Utils showAlertWithMessage:@"Please set cover photo"];
        return;
    }
    
    Account *account = [AccountManager Instance].activeAccount;
    
    if (![[AppDelegate appDelegate] isReachable]) {
        [Utils showAlertWithMessage:kCheckInternet];
        return;
    }else{
        if([account.wifiOnly boolValue]){
            if (![[AppDelegate appDelegate] isReachableViaWifi]) {
                [Utils showAlertWithMessage:@"Please check your wifi connection"];
                return;
            }else{
                [self videoTourDataUpload];
            }
        }else{
            [self videoTourDataUpload];
        }
    }
}

-(void)videoTourDataUpload{
    NSInteger days = [dTour.days integerValue];
    NSInteger j = 0;
    for (NSInteger i = 1; i<= days; i++) {
        for (DraftAttraction * atr in attractionArray) {
            if (atr.day.integerValue == i) {
                j++;
            }
        }
    }
    
    BOOL isVideoAbsent = false;
    if (j < days) {
        [Utils showAlertWithMessage:@"Attractions is not found in all days"];
    } else {
        for (DraftAttraction * atr in attractionArray) {
            if ([[atr.draftAttrDataSet allObjects] count] <= 0) {
                isVideoAbsent = true;
            } else {
                for (DraftAttrData * aData in [[atr.draftAttrDataSet allObjects] mutableCopy]) {
                    if (aData.draftVideoId.file_name.length <= 0) {
                        LocalVideoObject * vidOb = [LocalVideoObject getByEntityId:aData.entity_id];
                        if (vidOb == nil) {
                            isVideoAbsent = true;
                        }
                    }
                }
            }
        }
        if (isVideoAbsent) {
            [Utils showAlertWithMessage:@"Video is not found in all attractions"];
        } else {
            [self saveTour];
            CreateVideoTourViewController * controller = [CreateVideoTourViewController initViewControllerWithImage:chosenImage];
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

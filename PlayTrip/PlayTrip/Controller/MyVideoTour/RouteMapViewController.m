
#import "RouteMapViewController.h"
#import "CategoryCell.h"
#import "Categori.h"
#import "Attractions.h"
#import "AttractionData.h"
#import "Info.h"
#import "URLSchema.h"
#import "ColorConstants.h"
#import "PlayTripManager.h"
#import "SVProgressHUD.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import "AttractionModel.h"
#import "AttractionDataModel.h"
#import "ColorConstants.h"

@interface RouteMapViewController (){
    NSMutableArray *allCategories;
    
    NSMutableArray *dummyLocationsArray;
}
@end

@implementation RouteMapViewController

+(RouteMapViewController *)initViewController {
    RouteMapViewController * controller = [[RouteMapViewController alloc] initWithNibName:@"RouteMapViewController" bundle:nil];
    controller.title = @"My Video Tour (Draft) Route";
    return controller;
}

+(RouteMapViewController *)initViewController:(Plan *)pl {
    RouteMapViewController * controller = [[RouteMapViewController alloc] initWithNibName:@"RouteMapViewController" bundle:nil];
    controller.title = @"My Video Tour (Draft) Route";
    controller.planMain = pl;
    return controller;
}

+(RouteMapViewController *)initViewControllerWithPlanModel:(PlanModel *)pl {
    RouteMapViewController * controller = [[RouteMapViewController alloc] initWithNibName:@"RouteMapViewController" bundle:nil];
    controller.title = pl.name;
    controller->plan = pl;
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = NO;
    catArrayMain = [NSMutableArray new];
    [collViewMain registerClass:[CategoryCell class] forCellWithReuseIdentifier:@"CategoryCell"];
    [collViewMain registerNib:[UINib nibWithNibName:@"CategoryCell" bundle:nil] forCellWithReuseIdentifier:@"CategoryCell"];
    
    locArray = [NSMutableArray new];
    _routeController = [LRouteController new];
    
    CLLocation *loc1 = [[CLLocation alloc] initWithLatitude:22.264544 longitude:70.719166];
    CLLocation *loc2 = [[CLLocation alloc] initWithLatitude:22.296631 longitude:70.788174];
    CLLocation *loc3 = [[CLLocation alloc] initWithLatitude:22.325852 longitude:70.869198];
    CLLocation *loc4 = [[CLLocation alloc] initWithLatitude:22.359830 longitude:70.938549];

    
    dummyLocationsArray = [[NSMutableArray alloc] initWithObjects:loc1,loc2,loc3,loc4, nil];
    viewMain.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    [self setNavBarItems];

    [self getAllCategories];
}

-(void)backClicked {
    [self.navigationController popViewControllerAnimated:true];
}

-(void)setNavBarItems {
    // Arun :: customizing the nav bar icons
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[ColorConstants appBrownColor]}];
    
    UIImage *buttonImage = [UIImage imageNamed:@"back.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(backClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
}

-(void)getAllCategories {
    
    allCategories = [[NSMutableArray alloc] init];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Searching Categories..."];
    
    
    [[PlayTripManager Instance] getAllCategoriesWithBlock:^(id result, NSString *error) {
        [SVProgressHUD dismiss];
        if (!error) {
            NSError* errorData;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                 options:kNilOptions
                                                                   error:&errorData];
            if (errorData) {
                NSLog(@"ERROR GOTTEN = %@" ,errorData.description);
            }
            NSArray* categoriesDicts = [json objectForKey:@"data"];
            allCategories = [[NSMutableArray alloc] init];
            for (NSDictionary *categoryDict in categoriesDicts) {
                NSError *error;
                CategoryModel *categoryModel = [[CategoryModel alloc] initWithDictionary:categoryDict error:&error];
                if (error) {
                    NSLog(@"ERROR GOTTEN AT JSONMODEL = %@" ,error.description);
                }
                if (![allCategories containsObject:categoryModel]) {
                    [allCategories addObject:categoryModel];
                }
            }
            NSLog(@"Total Categories Gotten = %lu" ,(unsigned long)allCategories.count);
            //allLatestPlans = [[[allLatestPlans reverseObjectEnumerator] allObjects] mutableCopy];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [collViewMain reloadData];
                
                for (AttractionModel *attr in plan.attractions) {
                    attr.attr_data = [[attr.attr_data sortedArrayUsingComparator:^NSComparisonResult(AttractionDataModel *a, AttractionDataModel *b) {
                        return [a.sequence intValue] > [b.sequence intValue];
                    }] mutableCopy];
                }
                
                if (plan.attractions.count) {
                    AttractionModel *attraction = [plan.attractions firstObject];
                    if (attraction.attr_data.count) {
                        AttractionDataModel *attractionData = [attraction.attr_data firstObject];
                        if (attractionData.info.loc.count) {
                            CLLocationDegrees latitude = [attractionData.info.loc[0] doubleValue];
                            CLLocationDegrees longitude = [attractionData.info.loc[1] doubleValue];
                            GMSCameraPosition *cameraPosition = [GMSCameraPosition cameraWithLatitude:latitude longitude:longitude zoom:10.0];
                            [viewMain animateToCameraPosition:cameraPosition];
                            
                        }
                        else if([attractionData.info.loc_lat doubleValue] && [attractionData.info.loc_long doubleValue]){
                            CLLocationDegrees latitude = [attractionData.info.loc_lat doubleValue];
                            CLLocationDegrees longitude = [attractionData.info.loc_long doubleValue];
                            GMSCameraPosition *cameraPosition = [GMSCameraPosition cameraWithLatitude:latitude longitude:longitude zoom:10.0];
                            [viewMain animateToCameraPosition:cameraPosition];
                            
                            
                        }
                    }
                }
                
                
                NSMutableArray *arraysOfArrayOfLocations = [[NSMutableArray alloc] init];
                __block CLLocation *lastLocation;

                for (AttractionModel *attraction in plan.attractions) {
                    __block NSMutableArray *locationsArray = [[NSMutableArray alloc] init];

                    
                    for (AttractionDataModel *attractionData in attraction.attr_data) {
                        __block UIColor *selectedColor = [ColorConstants categoryRedColor];

                        if (attractionData.info.loc.count) {
                            CLLocationDegrees latitude = [attractionData.info.loc[0] doubleValue];
                            CLLocationDegrees longitude = [attractionData.info.loc[1] doubleValue];
                            CLLocation *loc = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
                            GMSMarker *marker = [[GMSMarker alloc] init];
                            marker.position = loc.coordinate;
                            marker.title = attraction.name;
                            marker.snippet = attractionData.info.name;
                            UIImage *image = [UIImage imageNamed:@"redPin"];
                            CategoryModel *category = [attractionData.info.category firstObject];
                            if ([category._id isEqualToString:@"58d136a72e0fbe7e5867e363"]) {
                                image = [UIImage imageNamed:@"yellowPin"];
                                selectedColor = [ColorConstants categoryYellowColor];
                            }
                            if ([category._id isEqualToString:@"58d136a62e0fbe7e5867e343"]) {
                                image = [UIImage imageNamed:@"redPin"];
                                selectedColor = [ColorConstants categoryRedColor];
                            }
                            if ([category._id isEqualToString:@"58d136a62e0fbe7e5867e341"]) {
                                image = [UIImage imageNamed:@"greenPin"];
                                selectedColor = [ColorConstants categoryGreenColor];
                            }
                            if ([category._id isEqualToString:@"58d136a62e0fbe7e5867e33f"]) {
                                image = [UIImage imageNamed:@"redPin"];
                                selectedColor = [ColorConstants categoryBlueColor];
                            }
                            //marker.icon = image;
                            marker.icon = [GMSMarker markerImageWithColor:selectedColor];

                            marker.map = viewMain;
                            [locationsArray addObject:loc];
                            
                        }
                        else if([attractionData.info.loc_lat doubleValue] && [attractionData.info.loc_long doubleValue]){
                            CLLocationDegrees latitude = [attractionData.info.loc_lat doubleValue];
                            CLLocationDegrees longitude = [attractionData.info.loc_long doubleValue];
                            
                            CLLocation *loc = [[CLLocation alloc] initWithLatitude:latitude longitude:longitude];
                            
                            GMSMarker *marker = [[GMSMarker alloc] init];
                            marker.position = loc.coordinate;
                            marker.title = attraction.name;
                            marker.snippet = attractionData.info.name;
                            UIImage *image = [UIImage imageNamed:@"redPin"];
                            CategoryModel *category = [attractionData.info.category firstObject];
                            if ([category._id isEqualToString:@"58d136a72e0fbe7e5867e363"]) {
                                image = [UIImage imageNamed:@"yellowPin"];
                                selectedColor = [ColorConstants categoryYellowColor];
                            }
                            if ([category._id isEqualToString:@"58d136a62e0fbe7e5867e343"]) {
                                image = [UIImage imageNamed:@"redPin"];
                                selectedColor = [ColorConstants categoryRedColor];
                            }
                            if ([category._id isEqualToString:@"58d136a62e0fbe7e5867e341"]) {
                                image = [UIImage imageNamed:@"greenPin"];
                                selectedColor = [ColorConstants categoryGreenColor];
                            }
                            if ([category._id isEqualToString:@"58d136a62e0fbe7e5867e33f"]) {
                                image = [UIImage imageNamed:@"redPin"];
                                selectedColor = [ColorConstants categoryBlueColor];
                            }
                            //marker.icon = image;
                            marker.icon = [GMSMarker markerImageWithColor:selectedColor];
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            
                            marker.map = viewMain;
                            [locationsArray addObject:loc];
                            
                            
                            
                            
                            if (lastLocation) {
                                
                                
                                
                                
                                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                                    NSLog(@"CALLED");
                                    
                                    
                                    if ([lastLocation isEqual:loc]) {
                                        NSLog(@"MAJOR ISSUE HERE");

                                    }
                                    LRouteController *newrouteController = [LRouteController new];

                                    [newrouteController getPolylineWithLocations:[NSArray arrayWithObjects:lastLocation , loc, nil] andCompletitionBlock:^(GMSPolyline *pLine, NSError *error) {
                                        if (error) {
                                            NSLog(@"%@", error);
                                        } else if (!pLine) {
                                            NSLog(@"No route");
                                            [locArray removeAllObjects];
                                        } else{
                                            NSLog(@"GOT A route");

                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                markerStart.position = [[dummyLocationsArray objectAtIndex:0] coordinate];
                                                markerStart.map = viewMain;
                                                markerFinish.position = [[dummyLocationsArray lastObject] coordinate];
                                                markerFinish.map = viewMain;
                                                polyline = pLine;
                                                polyline.strokeWidth = 6;
                                                polyline.strokeColor = selectedColor;
                                                polyline.map = viewMain;
                                            });
                                            
                                            
                                            
                                        }
                                    }];
                                    lastLocation = nil;
                                    lastLocation = [loc copy];
                                });
                                
                                
                            }
                            else{
                                lastLocation = [loc copy];
                            }
                            
                            
                            
                            
                            
                            
                            
                        }
                    }
                    [arraysOfArrayOfLocations addObject:locationsArray];
                }
                
                /*
                for (int i = 0; i < arraysOfArrayOfLocations.count; i++) {
                    NSMutableArray *insideArray = arraysOfArrayOfLocations[i];
                    if (i+1 < arraysOfArrayOfLocations.count) {
                        NSMutableArray *insideArrayNext = arraysOfArrayOfLocations[i+1];
                        if (insideArrayNext.count) {
                            [insideArray addObject:[insideArrayNext firstObject]];
                        }

                    }
                    
                    if (insideArray.count) {
                        NSLog(@"Count of array = %lu", (unsigned long)insideArray.count);
                        [_routeController getPolylineWithLocations:insideArray travelMode:TravelModeDriving andCompletitionBlock:^(GMSPolyline *pLine, NSError *error) {
                            if (error) {
                                NSLog(@"%@", error);
                            } else if (!pLine) {
                                NSLog(@"No route");
                                [locArray removeAllObjects];
                            } else{
                                markerStart.position = [[dummyLocationsArray objectAtIndex:0] coordinate];
                                markerStart.map = viewMain;
                                markerFinish.position = [[dummyLocationsArray lastObject] coordinate];
                                markerFinish.map = viewMain;
                                polyline = pLine;
                                polyline.strokeWidth = 6;
                                polyline.strokeColor = [UIColor purpleColor];
                                polyline.map = viewMain;
                            }
                        }];
                    }
                }
                
                
                
                */
                
                
                
                
                
                /*
                if (locationsArray.count) {
                    NSLog(@"Count of array = %lu", (unsigned long)locationsArray.count);
                    
                    
                    
                    
                    for (int i = 0; i + 1 < locationsArray.count ; i++) {
                        NSArray *twoPointsArray = [NSArray arrayWithObjects:locationsArray[i],locationsArray[i + 1] ,nil];
                        
                        
                        [_routeController getPolylineWithLocations:twoPointsArray andCompletitionBlock:^(GMSPolyline *pLine, NSError *error) {
                            if (error) {
                                NSLog(@"%@", error);
                            } else if (!pLine) {
                                NSLog(@"No route");
                                [locArray removeAllObjects];
                            } else{
                                markerStart.position = [[dummyLocationsArray objectAtIndex:0] coordinate];
                                markerStart.map = viewMain;
                                markerFinish.position = [[dummyLocationsArray lastObject] coordinate];
                                markerFinish.map = viewMain;
                                polyline = pLine;
                                polyline.strokeWidth = 6;
                                polyline.strokeColor = [UIColor orangeColor];
                                polyline.map = viewMain;
                            }
                        }];

                        
                    }
                    
                 
                    
                }
                 */
                
            });
        }
    }];

}



-(void)getAllCats {
    NSMutableArray *arrayLoc = [NSMutableArray new];
    NSMutableArray *arrDays = [NSMutableArray new];
    NSMutableArray * arrayAttract = [[dTour.draftAttractionsSet allObjects]mutableCopy];
    for (DraftAttraction * attractObj in arrayAttract) {
        NSMutableArray * arrayAttrData = [[attractObj.draftAttrData allObjects] mutableCopy];
       
        for (DraftAttrData * attrDataObj in arrayAttrData) {
//            NSMutableArray * arrayCat = [[attrDataObj.draftInfo.categoriSet allObjects] mutableCopy];
            NSMutableDictionary * dd = [[NSMutableDictionary alloc]init];
            
            if (attrDataObj.draftInfo.loc_lat == 0) {
                [dd setObject:@23.0365 forKey:@"latitude"];
                [dd setObject:@72.5611 forKey:@"longitude"];
            } else {
                [dd setObject:attrDataObj.draftInfo.loc_lat forKey:@"latitude"];
                [dd setObject:attrDataObj.draftInfo.loc_long forKey:@"longitude"];
            }
            [arrayLoc addObject:dd];
             [arrDays addObject:[NSString stringWithFormat:@"Day-%d",attractObj.dayValue] ];
            
//            for (Categori * catObj in arrayCat) {
//                [catArrayMain addObject:catObj];
//            }
        }
    }
    [collViewMain reloadData];
    [self allAnotationFocus:arrayLoc WittDay:arrDays];
}

- (void) allAnotationFocus:(NSMutableArray *) array WittDay:(NSMutableArray *)dayArray{
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    CLLocationCoordinate2D location;
    for (int index = 0; index < array.count; index ++) {
        NSDictionary *dictionary = [array objectAtIndex:index];
        location.latitude = [[dictionary objectForKey:@"latitude"]doubleValue];
        location.longitude = [[dictionary objectForKey:@"longitude"]doubleValue];
        
        if (index <= 7) {
            [locArray addObject:[[CLLocation alloc] initWithLatitude:location.latitude longitude:location.longitude]];
            

                GMSMarker *startMarker = [[GMSMarker alloc] init];
           
            startMarker.title =[dayArray objectAtIndex:index];                startMarker.icon = [UIImage imageNamed:(@"Map_Pin.png")];
                startMarker.position = CLLocationCoordinate2DMake(location.latitude, location.longitude);
                startMarker.map = viewMain;
                bounds = [bounds includingCoordinate:startMarker.position];
        
        }
    }
    [viewMain animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:30.0f]];
    if ([locArray count] > 1) {
        [_routeController getPolylineWithLocations:locArray travelMode:TravelModeDriving andCompletitionBlock:^(GMSPolyline *pLine, NSError *error) {
            if (error) {
                NSLog(@"%@", error);
            } else if (!pLine) {
                NSLog(@"No route");
                [locArray removeAllObjects];
            } else{
                markerStart.position = [[locArray objectAtIndex:0] coordinate];
                markerStart.map = viewMain;
                markerFinish.position = [[locArray lastObject] coordinate];
                markerFinish.map = viewMain;
                polyline = pLine;
                polyline.strokeWidth = 3;
                polyline.strokeColor = [UIColor blueColor];
                polyline.map = viewMain;
            }
        }];
    }
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return allCategories.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CategoryCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CategoryCell" forIndexPath:indexPath];
    CategoryModel *category = allCategories[indexPath.row];
    
    
    [cell.imgMain sd_setShowActivityIndicatorView:YES];
    [cell.imgMain sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [cell.imgMain sd_setImageWithURL:[NSURL URLWithString:category.image]
                       placeholderImage:[UIImage imageNamed:@"="] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                           if (!error) {
                               cell.imgMain.image = image;
                               [cell layoutIfNeeded];
                           } 
                       }];
    
    /*
    Categori * catObj = [catArrayMain objectAtIndex:indexPath.row];
    if (catObj.image.length > 0) {
        NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,catObj.image];
        cell.imgMain.url = [NSURL URLWithString:urlString];
    } else {
        cell.imgMain.image = [UIImage imageNamed:@"restaurant.png"];
    }
     */
    
    return cell;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

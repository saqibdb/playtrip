
#import <UIKit/UIKit.h>
#import "ItineraryTableViewCell.h"
#import "Plan.h"
#import "CarRentalTableViewCell.h"
#import "InsuranceTableViewCell.h"
#import "VideoTour.h"
#import "DragAndDropTableView.h"
#import <MediaPlayer/MediaPlayer.h>

@interface MYTDViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,UINavigationControllerDelegate, UIImagePickerControllerDelegate> {
    NSString *timeString;
    NSString *catImage;
    NSString *mainName;
    
    BOOL selfDrive;
    BOOL isImageChanged;
        
    NSString * pickerTag;
    NSString * descTag;
    
    UIImage *chosenImage;
    NSDate *selectedFromDate;
    NSDate *selectedToDate;
    
    Plan *planMain;
    
    NSMutableArray *attractionArray;
    NSMutableArray *languagesArray;
    NSMutableArray *categoryListAttr;
    NSMutableArray *attDataArray;
    
    IBOutlet UITableView *tblView1;
    
    IBOutlet UITextView *txtViewDesc;
    IBOutlet UIView *EditDescView;
    IBOutlet UILabel *lblManiDes;
}


- (IBAction)cancelDescClicked:(id)sender;
- (IBAction)saveDescClicked:(id)sender;

@property (strong, nonatomic) MPMoviePlayerController *videoController;

+(MYTDViewController *)initController:(Plan *)pl;

@end

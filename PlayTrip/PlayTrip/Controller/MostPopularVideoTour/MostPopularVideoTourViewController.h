
#import <UIKit/UIKit.h>
#import "MenuButtonViewController.h"
#import "PopularVideoSort.h"

@interface MostPopularVideoTourViewController : MenuButtonViewController <UICollectionViewDelegate,UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource> {
    
    IBOutlet UICollectionView * collviewMain;
    BOOL isSortShown;
    PopularVideoSort * sortView;
    UIView * subMenuView;
    
    int lastSortedIndex;
    BOOL lastSortedUp;
}

+ (MostPopularVideoTourViewController *) initViewController;

@end


#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "MenuButtonViewController.h"
#import "AttractionData.h"
#import <HNKGooglePlacesAutocomplete/HNKGooglePlacesAutocomplete.h>
#import "CLPlacemark+HNKAdditions.h"
#import <CoreLocation/CoreLocation.h>

#import "AttractionModel.h"

@interface MapViewController : MenuButtonViewController<GMSMapViewDelegate, CLLocationManagerDelegate,UICollectionViewDelegate,UICollectionViewDataSource, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate> {
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    NSString * currentLocationName;
    
    
    IBOutlet UITableView *tblViewGoogle;
    NSMutableArray * placesArray;
    NSMutableArray * arrGoogleResponse;
    NSTimer * timer;
    NSString * searchString;
    
    IBOutlet UIView *bottomView;
    IBOutlet GMSMapView *mapView;
    CLLocationCoordinate2D target;
    BOOL isallocated;
    IBOutlet UIButton *btnDetail;
    IBOutlet UIButton *btnAddPlan;
    IBOutlet UILabel *lblDetails;
    IBOutlet UILabel *lblName;
    IBOutlet UIImageView *detailImage;
    IBOutlet UIView *detailView;
    NSString * selectedAttractionId;
    Attractions *selectedAttraction;
    
    
    AttractionModel *selectedAttractionNew;
    
    
    AttractionDataModel *selectedAttractionNewWithData;

    
    IBOutlet UITextField *txtSearch;
    IBOutlet UISegmentedControl *segment;
    IBOutlet UICollectionView *collView;
}
@property (strong, nonatomic) HNKGooglePlacesAutocompleteQuery *searchQuery;
@property (strong, nonatomic) NSMutableArray *searchResults;

- (IBAction)addPlanClicked:(id)sender;
- (IBAction)detailClicked:(id)sender;
- (IBAction)segmentChanged:(id)sender;

+(MapViewController *)initViewController;

@end

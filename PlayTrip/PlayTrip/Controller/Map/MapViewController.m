
#import "MapViewController.h"
#import "PlayTripManager.h"
#import "ItineraryViewController.h"
#import "Categori.h"
#import "Attractions.h"
#import "KGModalWrapper.h"
#import "AddAttractionToMyPlanView.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "AttractionData.h"
#import "ColorConstants.h"
#import "PlaceInformationViewController.h"
#import "MapViewCell.h"
#import "Info.h"
#import "AddAttractionToDraftView.h"
#import "VideoDetailsViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import "Localisator.h"

@interface MapViewController (){
    NSMutableArray *categoryList;
    NSMutableArray *attrArray;
    NSMutableArray *allAttractions;
    NSMutableArray *allFilteredAttractions;
    
    
    NSMutableArray *allAttractionsWithData;
    NSMutableArray *allFilteredAttractionsWithData;


}
@end

@implementation MapViewController
+(MapViewController *)initViewController {
    MapViewController * controller = [[MapViewController alloc] initWithNibName:@"MapViewController" bundle:nil];
    [controller setTitle:LOCALIZATION(@"map")];
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    mapView = [GMSMapView new];
    isallocated = NO;
    mapView.myLocationEnabled = YES;
    mapView.delegate = self;
    mapView.settings.myLocationButton = TRUE;
    mapView.settings.compassButton = YES;
    
    self.view = mapView;
    
    bottomView.hidden = YES;
    [collView registerClass:[MapViewCell class] forCellWithReuseIdentifier:@"MapViewCell"];
    UINib *cellNib = [UINib nibWithNibName:@"MapViewCell" bundle:nil];
    [collView registerNib:cellNib forCellWithReuseIdentifier:@"MapViewCell"];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    bottomView.hidden = YES;
    self.navigationController.navigationBarHidden = false;
    [self setNavBarItems];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager startUpdatingLocation];
    [mapView addObserver:self forKeyPath:@"myLocation" options:NSKeyValueObservingOptionNew context:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideBottomView) name:@"HideKGView" object:nil];
}


-(void)viewDidAppear:(BOOL)animated {
    bottomView.hidden = YES;
    if(bottomView.hidden){
        self.leftButton.frame = CGRectMake(0.0, self.view.frame.size.height-50, 50.0, 50.0);
        self.rightButton.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 50, self.view.frame.size.height-50, 50.0, 50.0);
        
        CGRect mf = self.view.frame;
        self.menuView1.frame = CGRectMake(mf.size.width-250,mf.size.height - 110, self.menuView1.frame.size.width, self.menuView1.frame.size.height);
        //self.menuView1.frame = CGRectMake(0, self.view.frame.size.height-110, self.menuView1.frame.size.width, self.menuView1.frame.size.height);
    }
    [self setTextField];
    [self changeMyLocationButton];
    [self getAllAttractionsNew];
}

-(void)setTextField {
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleDone target:self action:@selector(cancelKeyboard)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Search" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithKeyboard)],
                           nil];
    [numberToolbar sizeToFit];
    txtSearch.inputAccessoryView = numberToolbar;
    
    txtSearch.frame = CGRectMake(30, 50, self.view.frame.size.width-60, 30);
    tblViewGoogle.frame = CGRectMake(30, 88, self.view.frame.size.width-60, 185);
    [self.view addSubview:txtSearch];
    [self.view addSubview:tblViewGoogle];
    [self.view bringSubviewToFront:txtSearch];
    [self.view bringSubviewToFront:tblViewGoogle];
    [MapViewController removeGMSBlockingGestureRecognizerFromMapView:mapView];
}

-(void)setUpBottomView{
    bottomView.frame =CGRectMake(0.0, mapView.frame.size.height-bottomView.frame.size.height,bottomView.frame.size.width, bottomView.frame.size.height);
    [mapView addSubview:bottomView];
    
    self.leftButton.frame = CGRectMake(0.0, mapView.frame.size.height-bottomView.frame.size.height-50, 50.0, 50.0);
    self.rightButton.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 50, mapView.frame.size.height-bottomView.frame.size.height-50, 50.0, 50.0);
    CGRect mf = self.view.frame;
    self.menuView1.frame = CGRectMake(mf.size.width-250, mapView.frame.size.height-bottomView.frame.size.height-110, self.menuView1.frame.size.width, self.menuView1.frame.size.height);
}

-(void)hideBottomView {
    [bottomView removeFromSuperview];
    
    self.leftButton.frame = CGRectMake(0.0, mapView.frame.size.height-50, 50.0, 50.0);
    self.rightButton.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 50, mapView.frame.size.height-50, 50.0, 50.0);
    
    self.menuView1.frame = CGRectMake(self.view.frame.size.width-250, mapView.frame.size.height-110, self.menuView1.frame.size.width, self.menuView1.frame.size.height);
}

-(void)setNavBarItems {
    // Arun :: customizing the nav bar icons
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[ColorConstants appBrownColor]}];
    
    UIImage *buttonImage = [UIImage imageNamed:@"back.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(backClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
}

-(void)backClicked {
    
    UIViewController * aController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];

    if ([aController isKindOfClass:[VideoDetailsViewController class]]) {
        [Utils setBOOL:YES Key:@"AddedAttraction"];
        [self.navigationController popViewControllerAnimated:true];
    } else {
        [self.navigationController popViewControllerAnimated:true];
    }
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    if (!currentLocationName) {
        currentLocation = newLocation;
        [Utils getAddressFromLocation:newLocation complationBlock:^(NSString *name) {
            if (name) {
                currentLocationName = name;
            }
        }];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self gotoGoogleMapWithDestination:txtSearch.text];
    [textField resignFirstResponder];
    return YES;
}

-(void)cancelKeyboard {
    [txtSearch resignFirstResponder];
}

-(void)doneWithKeyboard {
    [self gotoGoogleMapWithDestination:txtSearch.text];
    [txtSearch resignFirstResponder];
}

-(void)gotoGoogleMapWithDestination:(NSString *)destination{
    if (currentLocationName) {
        NSString* addr = [NSString stringWithFormat:@"http://maps.google.com/maps?daddr=%@ Location&saddr=%@",destination, currentLocationName];
        NSURL* url = [[NSURL alloc] initWithString:[addr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [[UIApplication sharedApplication] openURL:url];
    }
}



#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    [tblViewGoogle setHidden:YES];
    
    [timer invalidate];
    
    NSString * searchText =  [textField.text stringByReplacingCharactersInRange:range withString:string];
    NSLog(@"SEARCHED STRING = %@", searchText);
    allFilteredAttractionsWithData = [[NSMutableArray alloc] init];
    if (searchText.length) {
        [tblViewGoogle setHidden:NO];
        for (AttractionDataModel *attraction in allAttractionsWithData) {
            if ([[attraction.info.name lowercaseString] containsString:[searchText lowercaseString]]) {
                [allFilteredAttractionsWithData addObject:attraction];
            }
        }
    }
    else{
        [tblViewGoogle setHidden:YES];

    }
    [tblViewGoogle reloadData];
    /*
    
    arrGoogleResponse = [NSMutableArray new];
    
    NSString * searchText =  [textField.text stringByReplacingCharactersInRange:range withString:string];
    searchString = [NSString stringWithFormat:@"%@", searchText];
    if(searchString.length > 0) {
        timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(callCMSAPi) userInfo:nil repeats:YES];
    } else {
        [tblViewGoogle setHidden:YES];
    }
     */
    return YES;
}

-(void)callCMSAPi {
    [self searchGooglePlacesWithString:searchString];
    [timer invalidate];
}

-(void)searchGooglePlacesWithString:(NSString *)searchText {
    //self.searchResults = [NSMutableArray new];
    [[HNKGooglePlacesAutocompleteQuery sharedQuery] fetchPlacesForSearchQuery:searchText completion:^(NSArray *places, NSError *error)  {
        if (error) {
            NSLog(@"ERROR: %@", error);
        } else {
            for (HNKGooglePlacesAutocompletePlace * p in places) {
                if (![self.searchResults containsObject:p.name]) {
                    [self.searchResults addObject:p.name];
                    [arrGoogleResponse addObject:p];
                }
            }
            if (arrGoogleResponse.count > 0) {
                [tblViewGoogle setHidden:NO];
            }
            [tblViewGoogle reloadData];
        }
    }];
}

- (void)handleSearchError:(NSError *)error {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (allFilteredAttractionsWithData.count <= 0) {
        tblViewGoogle.hidden = YES;
        return 0;
    } else {
        return allFilteredAttractionsWithData.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    if (allFilteredAttractionsWithData.count > 0) {
        
        AttractionDataModel *attraction = allFilteredAttractionsWithData[indexPath.row];
        
        
        cell.textLabel.text = attraction.info.name;
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [txtSearch resignFirstResponder];
    
    AttractionDataModel *attraction = allFilteredAttractionsWithData[indexPath.row];
    
    [mapView animateToLocation:CLLocationCoordinate2DMake(attraction.info.loc_lat.doubleValue, attraction.info.loc_long.doubleValue)];
    
    
    [tableView setHidden:YES];
    
    /*
    if (self.searchResults.count >= indexPath.row) {
        NSString * selectedPlace = [self.searchResults objectAtIndex:indexPath.row];
        [tblViewGoogle setHidden:true];
        [self gotoGoogleMapWithDestination:selectedPlace];
    }
     */
}


-(void)getAllCategory{
    categoryList = [NSMutableArray new];
    [[PlayTripManager Instance]loadCategory:^(id result, NSString *error) {
        if(!error){
            categoryList = [Categori getAll];
        }
    }];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if([keyPath isEqualToString:@"myLocation"]) {
        CLLocation *location = [object myLocation];
        target = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude);
        if (isallocated == NO) {
            [self getAllAttractionsNew];
            isallocated = YES;
        }
    }
}
-(void)getAttractions {
    //    return;
    categoryList = [NSMutableArray new];
    attrArray = [NSMutableArray new];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Loading..."];
    [[PlayTripManager Instance] getAttractionsListWithDistance:@"50" WithLattitude:target.latitude WithLongitude:target.longitude WithBlock:^(id result, NSString *error) {
        if (error) {
            [SVProgressHUD dismiss];
        } else {
            [SVProgressHUD dismiss];
            attrArray = [Attractions getAll];
            NSLog(@"%lu", (unsigned long)attrArray.count);
            for (Attractions *a in attrArray) {
                GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
                GMSMarker *finishMarker = [[GMSMarker alloc] init];
                finishMarker.title = a.name;
                finishMarker.icon = [UIImage imageNamed:(@"aaa.png")];
                finishMarker.position = CLLocationCoordinate2DMake(a.loc_lat.doubleValue, a.loc_long.doubleValue);
                finishMarker.map = mapView;
                
                bounds = [bounds includingCoordinate:finishMarker.position];
                [mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:30.0f]];
            }
            
        }
    }];
}
-(void)getAllAttractionsNew {
    if (!allAttractionsWithData) {
        allAttractionsWithData = [[NSMutableArray alloc] init];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD showWithStatus:@"Getting Attractions..."];
    }
    
    [[PlayTripManager Instance] loadAttractionsWithDataWithBlockNew:^(id result, NSString *error) {
        [SVProgressHUD dismiss];
        if (!error) {
            NSError* error;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                 options:kNilOptions
                                                                   error:&error];
            
            NSArray* attractionsDicts = [json objectForKey:@"data"];
            
            
            
            for (NSDictionary *attractionDict in attractionsDicts) {
                NSError* modelError;
                
                AttractionDataModel *newDataModel = [[AttractionDataModel alloc] initWithDictionary:attractionDict error:&modelError];
                if (modelError) {
                    NSLog(@"ERROR GOTTEN = %@" , modelError.description);
                }
                else{
                    if (![allAttractionsWithData containsObject:newDataModel]) {
                        [allAttractionsWithData addObject:newDataModel];
                    }
                }
            }
            
            NSLog(@"TOTAL ATTRACTIONS = %lu" , (unsigned long)allAttractionsWithData.count);
            /*
            dispatch_async(dispatch_get_main_queue(), ^{
                for (AttractionDataModel *newDataModel in allAttractionsWithData) {
                    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
                    GMSMarker *finishMarker = [[GMSMarker alloc] init];
                    finishMarker.title = newDataModel.info.name;
                    finishMarker.icon = [UIImage imageNamed:(@"aaa.png")];
                    finishMarker.position = CLLocationCoordinate2DMake(newDataModel.info.loc_lat.doubleValue, newDataModel.info.loc_long.doubleValue);
                    finishMarker.map = mapView;
                    
                    bounds = [bounds includingCoordinate:finishMarker.position];
                    [mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:30.0f]];
                }
            });
            */
            
            
            for (AttractionDataModel *newDataModel in allAttractionsWithData) {
                
                
                
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
                    GMSMarker *finishMarker = [[GMSMarker alloc] init];
                    finishMarker.title = newDataModel.info.name;
                    finishMarker.icon = [UIImage imageNamed:(@"aaa.png")];
                    finishMarker.position = CLLocationCoordinate2DMake(newDataModel.info.loc_lat.doubleValue, newDataModel.info.loc_long.doubleValue);
                    finishMarker.map = mapView;
                    
                    bounds = [bounds includingCoordinate:finishMarker.position];
                    [mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:30.0f]];
                });
                
                
                
                
            }
            
            
        }
        else{
            
        }
    }];
}


-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {
    //[self setUpBottomView];
    bottomView.hidden = NO;
    [collView reloadData];
    [KGModalWrapper showWithContentView:detailView];
    detailView.layer.borderColor = [ColorConstants appBrownColor].CGColor;
    btnAddPlan.layer.cornerRadius =  btnAddPlan.layer.frame.size.height / 2;
    btnDetail.layer.cornerRadius = btnDetail.layer.frame.size.height / 2;
    
    
    
    for (AttractionDataModel *newDataModel in allAttractionsWithData) {
        if ([marker.title isEqualToString:newDataModel.info.name]) {
            selectedAttractionId = newDataModel.info._id;
            //selectedAttraction = selectedAttractionNew;
            selectedAttractionNewWithData = newDataModel;
            lblName.text = newDataModel.info.name;
            NSString * fullStr = [NSString stringWithFormat:@"%@\n%@", newDataModel.info.address, newDataModel.info.phone_no];
            lblDetails.text = fullStr;
            
            
            
            [detailImage sd_setShowActivityIndicatorView:YES];
            [detailImage sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            [detailImage sd_setImageWithURL:[NSURL URLWithString:newDataModel.info.cover_photo_url]
                               placeholderImage:[UIImage imageNamed:@"def_city_img.png"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                                   if (!error) {
                                       [detailImage setContentMode:UIViewContentModeScaleAspectFill];

                                       detailImage.image = image;
                                       [detailView layoutIfNeeded];
                                   }
                               }];
            
            
            
            break;
        }
        
    }
    
    
    /*
    for (Attractions *a in attrArray) {
        if ([marker.title isEqualToString:a.name]) {
            selectedAttractionId = a.entity_id;
            selectedAttraction = a;
            lblName.text = a.name;
            NSString * fullStr = [NSString stringWithFormat:@"%@\n%@", a.address, a.phone_no];
            lblDetails.text = fullStr;
            break;
        }
    }
     */
    return NO;
}

+ (void)removeGMSBlockingGestureRecognizerFromMapView:(GMSMapView *)mapView {
    if([mapView.settings respondsToSelector:@selector(consumesGesturesInView)]) {
        mapView.settings.consumesGesturesInView = NO;
    } else {
        for (id gestureRecognizer in mapView.gestureRecognizers)  {
            if (![gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]]) {
                [mapView removeGestureRecognizer:gestureRecognizer];
            }
        }
    }
}

- (IBAction)addPlanClicked:(id)sender {
//    AddAttractionToMyPlanView *view = [AddAttractionToMyPlanView initWithNibWithAttractionId:selectedAttractionId];
//    [KGModalWrapper showWithContentView:view];
   // AddAttractionToDraftView *view = [AddAttractionToDraftView initWithNibAttractionId:selectedAttractionId];
    //[KGModalWrapper showWithContentView:view];
    AddAttractionToMyPlanView *view = [AddAttractionToMyPlanView initWithNibWithAttractionDataModel:selectedAttractionNewWithData];
    [KGModalWrapper showWithContentView:view];
    
    
}

- (IBAction)detailClicked:(id)sender {
//    return;
    [KGModalWrapper hideView];
     bottomView.hidden = NO;
    PlaceInformationViewController * controller = [PlaceInformationViewController initViewControllerWithAttrModel:nil andAttractionDataModel:selectedAttractionNewWithData];
    [self.navigationController pushViewController:controller animated:YES];
}

-(void)changeMyLocationButton {
    mapView.settings.myLocationButton = YES;
    mapView.myLocationEnabled = YES;
    mapView.padding = UIEdgeInsetsMake(0, 0, 50, 0);
}

- (IBAction)segmentChanged:(id)sender {
    [collView reloadData];
}

-(void)viewWillDisappear:(BOOL)animated {
    @try{
        [mapView removeObserver:self forKeyPath:@"myLocation"];
    }@catch(id anException){
        //do nothing, obviously it wasn't attached because an exception was thrown
    }
}

- (void)dealloc {
    @try{
        [mapView removeObserver:self forKeyPath:@"myLocation"];
    }@catch(id anException){
        //do nothing, obviously it wasn't attached because an exception was thrown
    }
}


#pragma mark -<UICollectionViewDelegate,UICollectionViewDataSource>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MapViewCell *cell = (MapViewCell*)[collView dequeueReusableCellWithReuseIdentifier:@"MapViewCell" forIndexPath:indexPath];
    
    if(segment.selectedSegmentIndex == 0){
        cell.img.image = [UIImage imageNamed:@"def_video_img.png"];
        cell.img.layer.cornerRadius = 0;
        
    }else{
        cell.img.image = [UIImage imageNamed:@"avatarPlacHolder"];
        cell.img.layer.cornerRadius = cell.img.frame.size.width / 2;
        cell.img.layer.masksToBounds = YES;
    }
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(segment.selectedSegmentIndex == 0){
        //Itinarary
        
    }else{
        //UserDetail
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.leftButton.hidden= YES;
    self.rightButton.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end


#import <UIKit/UIKit.h>
#import "PopularVideoSort.h"
#import "District.h"
#import "MenuButtonViewController.h"
#import "Country.h"
#import "LocationModel.h"
@interface DistrictDetailViewController : MenuButtonViewController<UICollectionViewDelegate , UICollectionViewDataSource> {
    
    BOOL isSortShown;
    PopularVideoSort * sortView;
    
    IBOutlet UIButton *btnDistrict;
    IBOutlet UILabel *lblCountryName;
    IBOutlet UICollectionView *collViewMain;
    NSString * districtId;
    NSMutableArray * arrVideoTour;
    District *districts;
    Country *country;
    NSMutableArray *allDistricts;
    NSMutableArray *allCities;
    
    LocationModel *location;
    
    
    
    IBOutlet UILabel *lblCity;
    IBOutlet UILabel *lblDistrcit;
}

+(DistrictDetailViewController *)initViewControllerWithDistrict:(District *)districts;
+(DistrictDetailViewController *)initViewControllerWithCountry:(Country *)country;
+(DistrictDetailViewController *)initViewControllerWithLocation:(LocationModel *)location;

@property (weak, nonatomic) IBOutlet UICollectionView *districtCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *cityCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *districtLbl;
@property (weak, nonatomic) IBOutlet UILabel *cityLbl;

- (IBAction)districtClicked:(id)sender;

@end

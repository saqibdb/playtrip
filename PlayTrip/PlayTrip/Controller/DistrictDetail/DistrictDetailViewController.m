
#import "DistrictDetailViewController.h"
#import "LatestVideoTourCollectionViewCell.h"
#import "MapViewController.h"
#import "ItineraryViewController.h"
#import "PlayTripManager.h"
#import "VideoTour.h"
#import "Attractions.h"
#import "AttractionData.h"
#import "AccountManager.h"
#import "Account.h"
#import "CommonUser.h"
#import "URLSchema.h"
#import "ColorConstants.h"
#import "Info.h"
#import "DistrictCityCountCell.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import "Localisator.h"


@interface DistrictDetailViewController (){
    NSMutableArray * uniqueDistricts;
    NSMutableArray *allSelectedDistricts;
    NSMutableArray * uniqueCities;
    NSMutableArray *allSelectedCities;
    NSMutableArray *allFinalTours;

    NSMutableArray *alreadyPlans;
}
@end

@implementation DistrictDetailViewController

+(DistrictDetailViewController *)initViewControllerWithDistrict:(District *)districts {
    DistrictDetailViewController * controller = [[DistrictDetailViewController alloc] initWithNibName:@"DistrictDetailViewController" bundle:nil];
    controller.title = districts.name;
    controller->districts = districts;
    return controller;
}
+(DistrictDetailViewController *)initViewControllerWithCountry:(Country *)country{
    DistrictDetailViewController * controller = [[DistrictDetailViewController alloc] initWithNibName:@"DistrictDetailViewController" bundle:nil];
    controller.title = country.name;
    controller->country = country;
    return controller;
}
+(DistrictDetailViewController *)initViewControllerWithLocation:(LocationModel *)location{
    DistrictDetailViewController * controller = [[DistrictDetailViewController alloc] initWithNibName:@"DistrictDetailViewController" bundle:nil];
    controller.title = location.country_data.name;
    controller->location = location;
    return controller;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self registerNibs];
    alreadyPlans = [[NSMutableArray alloc] init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveLanguageChangedNotification:)
                                                 name:kNotificationLanguageChanged
                                               object:nil];

    
    [self.districtCollectionView setDelegate:self];
    [self.districtCollectionView setDataSource:self];
    [self.cityCollectionView setDelegate:self];
    [self.cityCollectionView setDataSource:self];
    
    allDistricts = [[NSMutableArray alloc] initWithObjects:@"ABC",@"XYZ", nil];
    allCities = [[NSMutableArray alloc] initWithObjects:@"123",@"321", nil];
    allSelectedDistricts = [[NSMutableArray alloc] init];
    allSelectedCities = [[NSMutableArray alloc] init];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self getAllDistricts];
    [self registerNibs];
    [self setNavBarItems];
    
    [self setLocalizedStrings];
    /*
    arrVideoTour = [NSMutableArray new];
    btnDistrict.layer.cornerRadius = btnDistrict.layer.frame.size.height / 2;
    lblCountryName.text = [NSString stringWithFormat:@"%@(0)",districts.name];
    
    [self getPlans];
    
    UIFont * f = [UIFont systemFontOfSize:15.0];
    CGFloat strWidth = [Utils widthOfString:lblCountryName.text withFont:f];
    lblCountryName.frame = CGRectMake(lblCountryName.frame.origin.x,lblCountryName.frame.origin.y, strWidth + 20, lblCountryName.frame.size.height);
    btnDistrict.frame = CGRectMake(lblCountryName.frame.size.width, btnDistrict.frame.origin.y, btnDistrict.frame.size.width, btnDistrict.frame.size.height);
     */
}

-(void)setLocalizedStrings {
    
    [lblDistrcit setText:LOCALIZATION(@"district_colon")];
    [lblCity setText:LOCALIZATION(@"city_colon")];
    
    /*
    lblDistrcit.text = [NSString stringWithFormat:NSLocalizedString(@"district_colon", nil)];
    lblCity.text = [NSString stringWithFormat:NSLocalizedString(@"city_colon", nil)];
    */
}

- (void) receiveLanguageChangedNotification:(NSNotification *) notification
{
    if ([notification.name isEqualToString:@"languageChanged"])
    {
        [self setLocalizedStrings];
    }
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    sortView = [PopularVideoSort initWithNib];
    sortView.frame = CGRectMake(self.view.frame.size.width - sortView.frame.size.width, 0, sortView.frame.size.width, sortView.frame.size.height);
}

-(void)registerNibs {
    
    // Arun :: registering collection view cells
    
    [collViewMain registerClass:[LatestVideoTourCollectionViewCell class] forCellWithReuseIdentifier:@"LatestVideoTourCollectionViewCell"];
    UINib *cellNib = [UINib nibWithNibName:@"LatestVideoTourCollectionViewCell" bundle:nil];
    [collViewMain registerNib:cellNib forCellWithReuseIdentifier:@"LatestVideoTourCollectionViewCell"];
    
    
    [self.districtCollectionView registerClass:[DistrictCityCountCell class] forCellWithReuseIdentifier:@"DistrictCityCountCell"];
    UINib *cellNib1 = [UINib nibWithNibName:@"DistrictCityCountCell" bundle:nil];
    [self.districtCollectionView registerNib:cellNib1 forCellWithReuseIdentifier:@"DistrictCityCountCell"];
    
    
    
    [self.cityCollectionView registerClass:[DistrictCityCountCell class] forCellWithReuseIdentifier:@"DistrictCityCountCell"];
    [self.cityCollectionView registerNib:cellNib1 forCellWithReuseIdentifier:@"DistrictCityCountCell"];
    
}

-(void)setNavBarItems {
    // Arun :: customizing the nav bar icons
   
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[ColorConstants appBrownColor]}];
    
    UIImage *buttonImage = [UIImage imageNamed:@"back.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(backClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
    
    UIImage *mapImage = [UIImage imageNamed:@"map_search.png"];
    UIButton *btnRight1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight1 setImage:mapImage forState:UIControlStateNormal];
    btnRight1.frame = CGRectMake(0, 0, mapImage.size.width, mapImage.size.height);
    [btnRight1 addTarget:self action:@selector(mapClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarItem1 = [[UIBarButtonItem alloc] initWithCustomView:btnRight1];
    
    UIImage *filterImage = [UIImage imageNamed:@"sort_descending.png"];
    UIButton *btnRight2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight2 setImage:filterImage forState:UIControlStateNormal];
    btnRight2.frame = CGRectMake(0, 0, filterImage.size.width, filterImage.size.height);
    [btnRight2 addTarget:self action:@selector(sortClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnRight2];
    
    self.navigationItem.rightBarButtonItems = @[rightBarItem2, rightBarItem1];
}

-(void)backClicked {
    [self.navigationController popViewControllerAnimated:true];
}
- (IBAction)districtClicked:(id)sender {
}

-(void)getPlans {
    NSMutableDictionary *where = [NSMutableDictionary new];
    [where setObject:districts.entity_id forKey:@"district"];
    [where setObject:@"true" forKey:@"is_published"];
    [where setObject:[NSNumber numberWithBool:false] forKey:@"is_deleted"];
    
    [[PlayTripManager Instance] getVideoTourWithDistrictIdDict:where WithBlock:^(id result, NSString *error) {
        if(!error){
            arrVideoTour = [VideoTour getAll];
            lblCountryName.text = [NSString stringWithFormat:@"%@(%lu)",districts.name,(unsigned long)arrVideoTour.count];
            [collViewMain reloadData];
        }
    }];
    
    if(arrVideoTour.count <= 0){
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"No VideoTour for this District" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:actionOk];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}

-(void)mapClicked {
    MapViewController * controller = [MapViewController initViewController];
    [self.navigationController pushViewController:controller animated:true];
}

-(void)sortClicked {
     // Arun :: Waiting for client confirmation
//    if (isSortShown) {
//        isSortShown = NO;
//        [sortView removeFromSuperview];
//    } else {
//        isSortShown = YES;
//        [self.view addSubview:sortView];
//    }
}

#pragma mark -<UICollectionViewDelegate,UICollectionViewDataSource>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{

    if (collectionView == self.districtCollectionView) {
        return uniqueDistricts.count;
    }
    else if (collectionView == self.cityCollectionView) {
        return uniqueCities.count;
    }
    else{
        if (allSelectedDistricts.count == 0 && allSelectedCities.count == 0) {
            return location.video_tour.count;
        }
        else{
            
            if (allSelectedCities.count) {
                int count = 0;
                alreadyPlans = [[NSMutableArray alloc] init];
                for (LocationModel *locationM in allSelectedCities) {
                    
                    for (PlanModel *plan in locationM.video_tour) {
                        NSArray *filteredArray = [alreadyPlans filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF._id == %@", plan._id]];
                        if (!filteredArray.count) {
                            [alreadyPlans addObject:plan];
                        }
                    }
                    
                    count = count + (int)locationM.video_tour.count;
                }
                return alreadyPlans.count;
            }
            else{
                int count = 0;
                alreadyPlans = [[NSMutableArray alloc] init];
                for (LocationModel *locationMod in uniqueCities) {
                    
                    
                    
                    
                    
                    for (PlanModel *plan in locationMod.video_tour) {
                        
                        
                        NSArray *filteredArray = [alreadyPlans filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF._id == %@", plan._id]];
                        if (!filteredArray.count) {
                            [alreadyPlans addObject:plan];
                        }
                    }
                    count = count + (int)locationMod.video_tour.count;
                }
                return alreadyPlans.count;
            }
            
            
        }
        
        return arrVideoTour.count;
    }
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.districtCollectionView) {
        DistrictCityCountCell *cell = (DistrictCityCountCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"DistrictCityCountCell" forIndexPath:indexPath];
        
        LocationModel *loc = uniqueDistricts[indexPath.row];
        
     
        
        NSString *title = [NSString stringWithFormat:@"%@ (%lu)",loc.district_data.name,(unsigned long)loc.video_tour.count];
        
        if ([allSelectedDistricts containsObject:loc]) {
            [cell.titleButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [cell.titleButton setBackgroundColor:[ColorConstants appBrownColor]];
            cell.titleButton.tag = 1;
        }
        else{
            [cell.titleButton setTitleColor:[ColorConstants appBrownColor] forState:UIControlStateNormal];
            [cell.titleButton setBackgroundColor:[UIColor whiteColor]];
            cell.titleButton.tag = 0;
        }
        
        
        [cell.titleButton setTitle:title forState:UIControlStateNormal];
        //cell.titleButton.tag = 0;
        
        

        
        
        
        
        [cell.titleButton addTarget:self action:@selector(btnSelected:) forControlEvents:UIControlEventTouchUpInside];

        cell.outerBorderView.tag = indexPath.row;
        return cell;
    }
    else if (collectionView == self.cityCollectionView) {
        DistrictCityCountCell *cell = (DistrictCityCountCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"DistrictCityCountCell" forIndexPath:indexPath];
        LocationModel *loc = [uniqueCities objectAtIndex:indexPath.row];
        NSString *title = [NSString stringWithFormat:@"%@ (%lu)",loc.city_data.name,(unsigned long)loc.video_tour.count];
        [cell.titleButton setTitle:title forState:UIControlStateNormal];
        
        
        if ([allSelectedCities containsObject:loc]) {
            [cell.titleButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [cell.titleButton setBackgroundColor:[ColorConstants appBrownColor]];
            cell.titleButton.tag = 1;
        }
        else{
            [cell.titleButton setTitleColor:[ColorConstants appBrownColor] forState:UIControlStateNormal];
            [cell.titleButton setBackgroundColor:[UIColor whiteColor]];
            cell.titleButton.tag = 0;
        }
        
        [cell.titleButton addTarget:self action:@selector(btnSelectedCities:) forControlEvents:UIControlEventTouchUpInside];
        
        cell.outerBorderView.tag = indexPath.row;
        
        return cell;
    }
    else{
        
        LatestVideoTourCollectionViewCell *cell = (LatestVideoTourCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"LatestVideoTourCollectionViewCell" forIndexPath:indexPath];
        Account * account = [AccountManager Instance].activeAccount;
        
        PlanModel *plan ;
        if (allSelectedDistricts.count == 0 && allSelectedCities.count == 0) {
            plan = location.video_tour[indexPath.row];
        }
        else {
            NSMutableArray *allTour = [[NSMutableArray alloc] init];
            
            if (allSelectedCities.count) {
                for (LocationModel *locationM in allSelectedCities) {
                    [allTour addObjectsFromArray:locationM.video_tour];
                }
                if (allTour.count > indexPath.row) {
                    plan = allTour[indexPath.row];
                }
            }
            else{
                for (LocationModel *locationMod in uniqueDistricts) {
                    [allTour addObjectsFromArray:locationMod.video_tour];
                }
                if (allTour.count > indexPath.row) {
                    plan = allTour[indexPath.row];
                }
            }
            
            
            
        }
        if (plan == nil || [plan.name isEqualToString:@""]) {
            cell.hidden = YES;
            cell.alpha = 0.0;
        }
        else{
            cell.hidden = NO;
            cell.alpha = 1.0;
        }
        
        
        cell.lblTime.text = @"00:00";
        

        cell.lblUserName.text = plan.user.full_name;
        
        [cell.imgUserImg sd_setShowActivityIndicatorView:YES];
        [cell.imgUserImg sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [cell.imgUserImg sd_setImageWithURL:[NSURL URLWithString:plan.user.avatar]
                           placeholderImage:[UIImage imageNamed:@"avatarPlacHolder"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                               if (!error) {
                                   cell.imgUserImg.image = image;
                                   [cell layoutIfNeeded];
                               }
                           }];
        cell.lblFromDate.text = ([plan.create_date length]>10 ? [plan.create_date substringToIndex:10] : plan.create_date);
        
        
        if(![plan.cover_photo isEqualToString:@""]){
            NSString * imgId = plan.cover_photo;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            //cell.imgThumbnail.url = [NSURL URLWithString:urlString];
            [cell.imgThumbnail sd_setShowActivityIndicatorView:YES];
            [cell.imgThumbnail sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            [cell.imgThumbnail sd_setImageWithURL:[NSURL URLWithString:urlString]
                               placeholderImage:[UIImage imageNamed:@"def_video_img.png"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                                   if (!error) {
                                       cell.imgThumbnail.image = image;
                                       [cell layoutIfNeeded];
                                   }
                               }];
        }
        cell.lblLanguage.text = plan.language;
        cell.lblVideoName.text = plan.name;
        
        cell.lblCountry.hidden = YES;
        cell.lblDistrict.hidden = YES;
        cell.lblCity.hidden = YES;
        
        for (AttractionModel *attraction in plan.attractions) {
            if (attraction.attr_data.count) {
                AttractionDataModel *attratctionData = [attraction.attr_data objectAtIndex:0];
                cell.lblTime.text = attratctionData.time;
                if (attratctionData.info.country_id) {
                    cell.lblCountry.text = attratctionData.info.country_id.name;
                    cell.lblDistrict.text = attratctionData.info.district_id.name;
                    cell.lblCity.text = attratctionData.info.city_id.name;
                    cell.lblCountry.hidden = NO;
                    cell.lblDistrict.hidden = NO;
                    cell.lblCity.hidden = NO;
                    
                    break;
                }
            }
        }
        cell.lblBookmarkCount.text = [NSString stringWithFormat:@"%@",plan.total_bookmark];
        cell.lblViewCount.text = [NSString stringWithFormat:@"%@",plan.total_view];
        cell.lblShareCount.text = [NSString stringWithFormat:@"%@",plan.total_share];
        cell.lblRevertCount.text = [NSString stringWithFormat:@"%@",plan.remuneration_amount];
        
        [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
        cell.btnBookMark.selected = NO;
        if (account) {
            for (NSString *user in plan.bookmark_users) {
                if ([user isEqualToString:account.userId]) {
                    [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                    cell.btnBookMark.selected = YES;
                }
            }
        }
        
        
        
        
        
        
        
        
        
        
        /*
        VideoTour * v = [arrVideoTour objectAtIndex:indexPath.row];
        NSMutableArray *attraction = [NSMutableArray new];
        NSMutableArray *attractionData = [NSMutableArray new];
        Attractions *a;
        AttractionData *aData;
        attraction = [[v.attractionsSet allObjects] mutableCopy];
        if(attraction.count>0){
            a =[attraction objectAtIndex:0];
        }
        if(attractionData.count>0){
            aData = [attractionData objectAtIndex:0];
        }
        
        if (account) {
            if ([v.bookmark_users_string containsString:account.userId]) {
                [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                cell.btnBookMark.selected = YES;
            } else {
                [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
                cell.btnBookMark.selected = NO;
            }
        } else {
            [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
            cell.btnBookMark.selected = NO;
        }
        attractionData = [[a.attractionDataSet allObjects] mutableCopy];
        if(attractionData.count>0){
            aData = [attractionData objectAtIndex:0];
            cell.lblDescription.text = [NSString stringWithFormat:@"[%@ days] %@",v.days, aData.info.address];
        } else {
            cell.lblDescription.text = [NSString stringWithFormat:@"[%@ days]",v.days];
        }
        
        cell.lblFromDate.text = [Utils dateFormatyyyyMMdd:v.from_date];
        cell.lblVideoName.text = v.name;
        cell.lblUserName.text = v.user.full_name;
        
        if(![v.user.avatar isEqualToString:@""]){
            cell.imgUserImg.layer.cornerRadius = cell.imgUserImg.frame.size.width / 2;
            cell.imgUserImg.layer.masksToBounds = true;
            NSString * imgId = v.user.avatar;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            cell.imgUserImg.url = [NSURL URLWithString:urlString];
        }else{
            cell.imgUserImg.backgroundColor = [UIColor blueColor];
        }
        cell.btnBookMark.tag = indexPath.row;
        [cell.btnBookMark addTarget:self action:@selector(bookmarkClickedVideoTour:) forControlEvents:UIControlEventTouchUpInside];
        cell.lblBookmarkCount.text = [NSString stringWithFormat:@"%@",v.total_bookmark];
        cell.lblViewCount.text = [NSString stringWithFormat:@"%@",v.total_view];
        cell.lblRevertCount.text = [NSString stringWithFormat:@"%@",v.remuneration_amount];
        cell.lblShareCount.text = [NSString stringWithFormat:@"%@",v.total_share];
         */
        return cell;
    }
    
    
    
    
    
    


    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == self.districtCollectionView) {
        DistrictCityCountCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DistrictCityCountCell" forIndexPath:indexPath];
        
        if (cell.tag == 0) {
            cell.titleText.textColor = [UIColor whiteColor];
            cell.outerBorderView.backgroundColor = [ColorConstants appBrownColor];
            cell.tag = 1;
        }
        else{
            cell.outerBorderView.backgroundColor = [UIColor whiteColor];
            cell.titleText.textColor = [ColorConstants appBrownColor];
            cell.tag = 0;
        }
        [cell layoutIfNeeded];

    }
    else{
        
        PlanModel *plan ;
        if (allSelectedDistricts.count == 0 && allSelectedCities.count == 0) {
            plan = location.video_tour[indexPath.row];
        }
        else {
            NSMutableArray *allTour = [[NSMutableArray alloc] init];
            
            if (allSelectedCities.count) {
                for (LocationModel *locationM in allSelectedCities) {
                    [allTour addObjectsFromArray:locationM.video_tour];
                }
                if (allTour.count > indexPath.row) {
                    plan = allTour[indexPath.row];
                }
            }
            else{
                for (LocationModel *locationMod in uniqueDistricts) {
                    [allTour addObjectsFromArray:locationMod.video_tour];
                }
                if (allTour.count > indexPath.row) {
                    plan = allTour[indexPath.row];
                }
            }
            
            
            
        }
        
        if (plan) {
            ItineraryViewController * controller = [ItineraryViewController initViewControllerWithPlan:plan];
            [self.navigationController  pushViewController:controller animated:YES];
        }
        else{
            NSLog(@"ISSUE HERE>>>>>>>>>>>>");
        }
        
        
        
        
    }
    

    
    
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //return CGSizeMake(175, 20);
    [self.view layoutIfNeeded];
    NSString *tagTitle;
    if (collectionView == self.districtCollectionView) {
        LocationModel *loc = [uniqueDistricts objectAtIndex:indexPath.row];
        NSString *title = [NSString stringWithFormat:@"%@ (%lu)",loc.district_data.name,(unsigned long)loc.video_tour.count];

        tagTitle = title;
    }
    else if (collectionView == self.cityCollectionView) {
        LocationModel *loc = [uniqueCities objectAtIndex:indexPath.row];
        NSString *title = [NSString stringWithFormat:@"%@ (%lu)",loc.city_data.name,(unsigned long)loc.video_tour.count];

        tagTitle = title;
    }
    else{
        //150 250
        return CGSizeMake((collectionView.frame.size.width/2)-10, (collectionView.frame.size.height/2)-10);
    }
    
    
    self.districtLbl.text = tagTitle;
    
    
    
    CGSize maxSize = CGSizeMake(MAXFLOAT, 20.0);
    
    
    CGRect labelRect = [self.districtLbl.text boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.districtLbl.font} context:nil];
    self.districtLbl.text = @"District : ";
    
    return CGSizeMake(labelRect.size.width + 20 , 20);
    
    
        
    

    
    
    
}
-(void)bookmarkClickedVideoTour:(id)sender {
    VideoTour * v = [arrVideoTour objectAtIndex:[sender tag]];
    [self commanApiWithModel:ModelPlan WithId:v.entity_id WithType:TypeBookmark];
}

-(void)commanApiWithModel:(NSString *)modelValue WithId:(NSString *)idValue WithType:(NSString *)typeValue {
    [[PlayTripManager Instance] addCountForModel:modelValue withType:typeValue forId:idValue WithBlock:^(id result, NSString *error) {
        if(!error){
            if(modelValue == ModelPlan ){
              [[NSNotificationCenter defaultCenter] postNotificationName:@"LatestVideoTourUpdated" object:nil];
            }
        }
    }];
}


#pragma mark - New Server Calls Methods
-(void)getAllDistricts{
    if (!uniqueDistricts) {
        uniqueDistricts = [[NSMutableArray alloc] init];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD showWithStatus:@"Getting Districts..."];
    }
    NSString *countryId = location.country_data._id;
    NSDictionary *parameters = @{ @"where": @{ @"country_id": countryId } };

    
    [[PlayTripManager Instance] getAllLocationWithDictionary:parameters andBlockNew:^(id result, NSString *error) {
        [SVProgressHUD dismiss];
        if (!error) {
            NSError* errorData;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                 options:kNilOptions
                                                                   error:&errorData];
            if (errorData) {
                NSLog(@"ERROR GOTTEN = %@" ,errorData.description);
            }
            NSArray* locationDicts = [json objectForKey:@"data"];
            uniqueDistricts = [[NSMutableArray alloc] init];
            for (NSDictionary *locationDict in locationDicts) {
                NSError *error;
                LocationModel *locationModel = [[LocationModel alloc] initWithDictionary:locationDict error:&error];
                if (error) {
                    NSLog(@"ERROR GOTTEN AT JSONMODEL = %@" ,error.description);
                }
                if (![uniqueDistricts containsObject:locationModel]) {
                    [uniqueDistricts addObject:locationModel];
                }
            }
            NSLog(@"Total Locations Gotten = %lu" ,(unsigned long)uniqueDistricts.count);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [_districtCollectionView reloadData];
            });
        }
    }];
}
-(void)getSelectedCities{
        uniqueCities = [[NSMutableArray alloc] init];
        
    
    for (LocationModel *locationMod in allSelectedDistricts) {
        NSString *districtd = locationMod.district_data._id;
        NSString *countryId = location.country_data._id;
        
        if (districtd != nil || countryId != nil) {
            
            NSDictionary *parameters = @{ @"where": @{ @"country_id": countryId, @"district_id": districtd } };
            [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
            [SVProgressHUD showWithStatus:@"Getting Cities..."];
            
            [[PlayTripManager Instance] getAllLocationWithDictionary:parameters andBlockNew:^(id result, NSString *error) {
                [SVProgressHUD dismiss];
                if (!error) {
                    NSError* errorData;
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                         options:kNilOptions
                                                                           error:&errorData];
                    if (errorData) {
                        NSLog(@"ERROR GOTTEN = %@" ,errorData.description);
                    }
                    NSArray* locationDicts = [json objectForKey:@"data"];
                    
                    for (NSDictionary *locationDict in locationDicts) {
                        NSError *error;
                        LocationModel *locationModel = [[LocationModel alloc] initWithDictionary:locationDict error:&error];
                        if (error) {
                            NSLog(@"ERROR GOTTEN AT JSONMODEL = %@" ,error.description);
                        }
                        if (![uniqueCities containsObject:locationModel]) {
                            [uniqueCities addObject:locationModel];
                        }
                    }
                    NSLog(@"Total Cities Gotten = %lu" ,(unsigned long)uniqueCities.count);
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [_cityCollectionView reloadData];
                        [collViewMain reloadData];
                    });
                }
            }];
        }
    }
   
}



-(void)getTourFromSelectedCity{
    allFinalTours = [[NSMutableArray alloc] init];
    
    
    for (LocationModel *locationMod in allSelectedCities) {
        NSString *cityId = locationMod.city_data._id;
        NSDictionary *parameters = @{ @"where": @{ @"city_id": cityId } };
        
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD showWithStatus:@"Getting Tours..."];
        
        [[PlayTripManager Instance] getAllLocationWithDictionary:parameters andBlockNew:^(id result, NSString *error) {
            [SVProgressHUD dismiss];
            if (!error) {
                NSError* errorData;
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                     options:kNilOptions
                                                                       error:&errorData];
                if (errorData) {
                    NSLog(@"ERROR GOTTEN = %@" ,errorData.description);
                }
                NSArray* locationDicts = [json objectForKey:@"data"];
                
                for (NSDictionary *locationDict in locationDicts) {
                    NSError *error;
                    LocationModel *locationModel = [[LocationModel alloc] initWithDictionary:locationDict error:&error];
                    if (error) {
                        NSLog(@"ERROR GOTTEN AT JSONMODEL = %@" ,error.description);
                    }
                    if (![allFinalTours containsObject:locationModel]) {
                        [allFinalTours addObject:locationModel];
                    }
                }
                NSLog(@"Total Cities Gotten = %lu" ,(unsigned long)allFinalTours.count);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [collViewMain reloadData];
                });
            }
        }];
    }
    
    
    
    
    
}

-(void)btnSelected :(UIButton *)btn {
    LocationModel *locationModel = uniqueDistricts[btn.superview.tag];
    
    NSLog(@"Selected District = %@", locationModel.district_data.name);
    
    if (btn.tag == 0) {
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setBackgroundColor:[ColorConstants appBrownColor]];
        btn.tag = 1;
        [allSelectedDistricts removeAllObjects];
        [allSelectedDistricts addObject:locationModel];
    }
    else{
        [btn setTitleColor:[ColorConstants appBrownColor] forState:UIControlStateNormal];
        [btn setBackgroundColor:[UIColor whiteColor]];
        btn.tag = 0;
        [allSelectedDistricts removeObject:locationModel];
    }
    
    [self.districtCollectionView reloadData];
    
    
    if (allSelectedDistricts.count) {
        [self getSelectedCities];
    }
    else{
        [uniqueCities removeAllObjects];
        [self.cityCollectionView reloadData];
        [collViewMain reloadData];
    }
}

-(void)btnSelectedCities :(UIButton *)btn {
    LocationModel *locationModel = uniqueCities[btn.superview.tag];
    
    NSLog(@"Selected City = %@", locationModel.city_data.name);
    
    if (btn.tag == 0) {
        [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btn setBackgroundColor:[ColorConstants appBrownColor]];
        btn.tag = 1;
        [allSelectedCities removeAllObjects];
        [allSelectedCities addObject:locationModel];
    }
    else{
        [btn setTitleColor:[ColorConstants appBrownColor] forState:UIControlStateNormal];
        [btn setBackgroundColor:[UIColor whiteColor]];
        btn.tag = 0;
        [allSelectedCities removeObject:locationModel];
    }
    
    [self.cityCollectionView reloadData];

    
    
    if (allSelectedCities.count) {
        
        //[self getTourFromSelectedCity];
        [collViewMain reloadData];
    }
    else{
        [collViewMain reloadData];
    }
}

-(void)filterWithSelectedCities {
    allFinalTours = [[NSMutableArray alloc] init];
    
    for (LocationModel *locationModel in allSelectedCities) {
        for (LocationModel *location in uniqueCities) {
            if ([locationModel.city_data._id isEqualToString:location.city_data._id]) {
                
            }
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

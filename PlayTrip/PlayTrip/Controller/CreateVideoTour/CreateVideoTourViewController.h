
#import <UIKit/UIKit.h>
#import "Plan.h"
#import <MediaPlayer/MediaPlayer.h>
#import "DraftTour.h"
#import "DraftAttraction.h"
#import "DraftAttrData.h"
#import "DraftInfo.h"
#import "DraftVideoId.h"

@interface CreateVideoTourViewController : UIViewController {
    IBOutlet UIImageView *imgMain;
    IBOutlet UIProgressView *progressMain;
    Plan * planMain;
    DraftTour * dTour;
    NSMutableDictionary * finalDict;
    NSMutableDictionary * planDict;
    NSMutableArray* aArray;
    UIImage * imgThumbnail;
}

@property (strong, nonatomic) MPMoviePlayerController *videoController;
+(CreateVideoTourViewController *)initViewControllerWithImage:(UIImage *)img;
+(CreateVideoTourViewController *)initViewControllerWithPlan:(Plan *)plan;

@end

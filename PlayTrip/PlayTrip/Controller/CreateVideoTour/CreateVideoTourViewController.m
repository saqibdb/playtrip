
#import "CreateVideoTourViewController.h"
#import "ItineraryViewController.h"
#import "ColorConstants.h"
#import "LocalVideoObject.h"
#import "PlayTripManager.h"
#import "Constants.h"
#import "URLSchema.h"
#import "VideoTour.h"
#import "Account.h"
#import "AccountManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>


@interface CreateVideoTourViewController ()
@end

static float progress = 0.0f;

@implementation CreateVideoTourViewController

+(CreateVideoTourViewController *)initViewControllerWithImage:(UIImage *)img {
    CreateVideoTourViewController * controller = [[CreateVideoTourViewController alloc] initWithNibName:@"CreateVideoTourViewController" bundle:nil];
    controller->imgThumbnail = img;
    controller.title = @"Create Video Your";
    return controller;
}

+(CreateVideoTourViewController *)initViewControllerWithPlan:(Plan *)plan {
    CreateVideoTourViewController * controller = [[CreateVideoTourViewController alloc] initWithNibName:@"CreateVideoTourViewController" bundle:nil];
    controller.title = @"Create Video Your";
    controller -> planMain = plan;
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    /*
    Account * account = [AccountManager Instance].activeAccount;
    if(account){
        if(![account.cover_pic isEqualToString:@""]){
            
            NSString * imgId = account.cover_pic;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            [imgMain sd_setImageWithURL:[NSURL URLWithString:urlString]
                            placeholderImage:[UIImage imageNamed:@"default_country_image"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                                if (!error) {
                                    imgMain.image = image;
                                }
                                else{
                                    imgMain.image = [UIImage imageNamed:@"default_country_image"];
                                }
                            }];
            
            
        }
        else{
            imgMain.image = [UIImage imageNamed:@"default_country_image"];
        }
    
    }
    else{
        imgMain.image = [UIImage imageNamed:@"default_country_image"];
    }
     */
    
    
    
    
    
    if (imgThumbnail) {
        imgMain.image = imgThumbnail;
    }
     
    
    
    
    dTour = [DraftTour getDraft];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    
    progress = 0.0f;
    progressMain.progress = progress;
    [self performSelector:@selector(increaseProgress) withObject:nil afterDelay:0.3];
    [self checkLocalObjects];
}

-(void)increaseProgress {
    progress+=0.05f;
    progressMain.progress = progress;
    if(progress < 1.0f){
        
        [self performSelector:@selector(increaseProgress) withObject:nil afterDelay:0.3];
    } else {
        progress = 0.0f;
        progressMain.progress = progress;
        [self performSelector:@selector(increaseProgress) withObject:nil afterDelay:0.3];
    }
}

-(void)checkLocalObjects {
    NSMutableArray * localArray = [LocalVideoObject getAll];
    if (localArray.count > 0) {
        for (LocalVideoObject * vidObj in localArray) {
            
            NSURL *movieUrl = [NSURL fileURLWithPath:vidObj.vidPath];
            
            [[PlayTripManager Instance] uploadVideoWithVideoUrl:movieUrl WithAttrId:vidObj.attrDataId WithBlock:^(id result, NSString *error) {
                [self createTour];
            }];
        }
        [LocalVideoObject MR_truncateAll];
    } else {
        [self arrangeValues];
    }
}

-(void)arrangeValues {
    
    Account * account = [AccountManager Instance].activeAccount;
    
    finalDict = [NSMutableDictionary new];
    aArray = [NSMutableArray new];
    
    NSMutableArray* attractionsArray = [NSMutableArray new];
    
    NSMutableArray * localArray = [[dTour.draftAttractionsSet allObjects]mutableCopy];
    NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"day" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
    NSArray *sortedArray = [localArray sortedArrayUsingDescriptors:sortDescriptors];
    localArray = [sortedArray mutableCopy];
    attractionsArray = localArray;
    
    for (int i = 1;i<=dTour.days.integerValue;i++) {
        NSMutableDictionary * attractionDict = [NSMutableDictionary new];
        NSMutableArray * attrArray = [NSMutableArray new];
        int sequence = 1;

        for (DraftAttraction * dAttrs in attractionsArray) {
            if (dAttrs.day.integerValue == i) {
                
                for (DraftAttrData * dAD in [dAttrs.draftAttrDataSet allObjects]) {
                    NSMutableDictionary * infoDict = [NSMutableDictionary new];
                    [infoDict setValue:dAD.draftInfo.entity_id forKey:@"info"];
                    [infoDict setValue:dAD.time forKey:@"time"];
                    [infoDict setValue:dAD.desc forKey:@"desc"];
                    [infoDict setValue:dAD.draftVideoId.entity_id forKey:@"video_id"];
                    
                    [infoDict setValue:[NSString stringWithFormat:@"%@" , dAD.sequence] forKey:@"sequence"];
                    sequence = sequence + 1;
                    //TODO SEQUENCE HERE
                    
                    
                    [attrArray addObject:infoDict];
                }
                [attractionDict setValue:[NSNumber numberWithInteger:dAttrs.day.integerValue] forKey:@"day"];
                [attractionDict setValue:attrArray forKey:@"attr"];
                [attractionDict setValue:account.userId forKey:@"user"];
            }
        }
        [aArray addObject:attractionDict];
    }
    NSString * dateStr = [Utils dateFormatMMddyyyy:dTour.from_date];
    
    planDict = [NSMutableDictionary new];
    [planDict setValue:account.userId forKey:@"user"];
    [planDict setValue:dTour.name forKey:@"name"];
    [planDict setValue:dateStr forKey:@"from_date"];
    [planDict setValue:dTour.travel_tip forKey:@"travel_tip"];
    [planDict setValue:dTour.days forKey:@"days"];
    [planDict setValue:dTour.language forKey:@"language"];
    [planDict setValue:dTour.self_drive forKey:@"self_drive"];
    
    NSLog(@"Self Drive = %@",dTour.self_drive);

    [finalDict setValue:planDict forKey:@"plan"];
    [finalDict setValue:aArray forKey:@"attractions"];
    
    [self createTour];
}


-(void)createTour {
    [[PlayTripManager Instance] createVideoTourWithAllDetails:planDict WithAttractionArray:aArray WithBlock:^(id result, NSString *error) {
        if (error) {
            NSLog(@"%@", error);
            [Utils showAlertWithMessage:error];
            [self.navigationController popToRootViewControllerAnimated:true];
        } else {
            //NSLog(@"%@", result);
            NSString * idStr = [result valueForKey:@"_id"];
            [self saveThumbnailInServerWithId:idStr];
        }
    }];
}

-(void)saveThumbnailInServerWithId:(NSString *)planId {
    [[PlayTripManager Instance] uploadCoverPhoto:imgThumbnail ForPlanId:planId WithBlock:^(id result, NSString *error) {
        if (error) {
            NSLog(@"%@", error);
        } else {
            //NSLog(@"%@", result);
            [self createVideoTourWithId:planId];
        }
    }];
}

-(void)createVideoTourWithId:(NSString *)planId {
    [[PlayTripManager Instance] createVideoTourForPlan:planId WithBlock:^(id result, NSString *error) {
        if (error) {
            NSLog(@"%@", error);
            [self.navigationController popToRootViewControllerAnimated:true];
            [Utils showAlertWithMessage:error];
        } else {
            //NSLog(@"%@", result);
            
            
            NSString * idStr = [result valueForKey:@"_id"];
            [DraftTour deleteObj:dTour];
            [DraftAttraction deleteAll];
            [DraftAttrData deleteAll];
            [DraftInfo deleteAll];
            [DraftVideoId deleteAll];
            
            NSError *error;
            PlanModel *plan = [[PlanModel alloc] initWithDictionary:result error:&error];
            if (error) {
                NSLog(@"ERROR GOTTEN %@",error.description);
            }
            
            ItineraryViewController * controller = [ItineraryViewController initViewControllerWithPlan:plan];
            [self.navigationController pushViewController:controller animated:YES];
            
            //[self getLatestVideoTourWithId:idStr];
        }
    }];
}

-(void)backClicked {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

-(void)playVideoWithURL:(NSURL *)url {
    if (url) {
        self.videoController = [[MPMoviePlayerController alloc] init];
        [self.videoController setContentURL:url];
        [self.videoController.view setFrame:CGRectMake (0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.videoController.controlStyle = MPMovieControlStyleNone;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoPlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:self.videoController];
        [self.view addSubview:self.videoController.view];
        [self.videoController play];
    }
}

- (void)videoPlayBackDidFinish:(NSNotification *)notification {
    [self.videoController stop];
    [self.videoController.view removeFromSuperview];
    self.videoController = nil;
    [self backClicked];
}

-(void)getLatestVideoTourWithId:(NSString *)idStr{
    NSMutableDictionary *where = [NSMutableDictionary new];
    [where setObject:@"true" forKey:@"is_published"];
    [where setObject:[NSNumber numberWithBool:false] forKey:@"is_deleted"];
    NSMutableDictionary *sort = [NSMutableDictionary new];
    [sort setObject:@"-1" forKey:@"create_date"];
    
    [[PlayTripManager Instance] getLatestVideoTourWithWhere:where WithSort:sort WithBlock:^(id result, NSString *error) {
        if(!error){
            VideoTour * vidTour = [VideoTour getByEntityId:idStr];
            if (vidTour) {
                ItineraryViewController * controller = [ItineraryViewController initViewController:vidTour];
                [self.navigationController pushViewController:controller animated:YES];
            }
        }
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end


#import <UIKit/UIKit.h>
#import "MenuButtonViewController.h"
#import "AttractionData.h"
#import "MostPopularAttractions.h"
#import "BookmarkAttractions.h"
#import "Attractions.h"
#import "AttractionModel.h"
#import <MediaPlayer/MediaPlayer.h>

@interface PlaceInformationViewController : MenuButtonViewController <UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource> {
    
    NSMutableArray * reportsArray;
    
    IBOutlet UIButton *btnBookMarkmain;
    IBOutlet UIButton *btnBookMarkVideoSource;
    IBOutlet UIButton *btnMoreAttr;
    IBOutlet UIButton *btnMoreVideo;
    
    NSString * selectedSpamId;
    
    IBOutlet UICollectionView *collViewVideoTour;
    
    IBOutlet UICollectionView *collViewAttr;
    
    IBOutlet UILabel *lblBookmark;
    IBOutlet UILabel *lblAddress;
    IBOutlet UILabel *lblDetails;
    
    IBOutlet UIImageView *imgBookmark;
    IBOutlet UILabel *lblhrsInfo;
    IBOutlet UILabel *lblwebUrl;
    IBOutlet UILabel *lblPhoneNum;
    IBOutlet UILabel *lblattractionName;
    
    __weak IBOutlet UIImageView *phoneIcon;
    
    
    __weak IBOutlet UIImageView *coverImage;
    
    IBOutlet UIView *popUpView;

    NSString *attId;
    NSString *userId;
}
@property (weak, nonatomic) Attractions *attrs;
@property (weak, nonatomic) AttractionData *attData;
@property (weak, nonatomic) MostPopularAttractions *mostAttData;
@property (weak, nonatomic) BookmarkAttractions *bookmarkAttData;

@property (weak, nonatomic) AttractionModel *attModel;
@property (weak, nonatomic) AttractionDataModel *dataModel;

@property (weak, nonatomic) AttractionModel *attractionModelOnly;


@property (strong, nonatomic) MPMoviePlayerController *videoController;


+(PlaceInformationViewController *)initViewController:(Attractions *)atts;
+(PlaceInformationViewController *)initViewControllerWithAttrData:(AttractionData *)attData ;
+(PlaceInformationViewController *)initController:(MostPopularAttractions *)mostAttData;
+(PlaceInformationViewController *)initControllerBookmark:(BookmarkAttractions *)bookmarkAttData;
+(PlaceInformationViewController *)initViewControllerWithAttrModel:(AttractionModel *)attModel andAttractionDataModel:(AttractionDataModel *)dataModel;
+(PlaceInformationViewController *)initViewControllerWithAttractionModelOnly:(AttractionModel *)attractionModel;

- (IBAction)moreAttrClicked:(id)sender;
- (IBAction)moreVideoClicked:(id)sender;

- (IBAction)backClicked:(id)sender;
- (IBAction)reportClicked:(id)sender;
- (IBAction)shareClicked:(id)sender;
- (IBAction)bookmarkClicked:(id)sender;
- (IBAction)addToMyPlanClicked:(id)sender;

@end

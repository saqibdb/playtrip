
#import "PlaceInformationViewController.h"
#import "ColorConstants.h"
#import "Reason.h"
#import "RelatedCollectionCell.h"
#import "RelatedVideoTourViewController.h"
#import "RelatedAttractionsVideoViewController.h"
#import "ReportView.h"
#import "ReportCell.h"
#import "KGModalWrapper.h"
#import "Account.h"
#import "AccountManager.h"
#import "SVProgressHUD.h"
#import "PlayTripManager.h"
#import "CommonUser.h"
#import "AddAttractionToMyPlanView.h"
#import "Info.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import "URLSchema.h"

@interface PlaceInformationViewController ()
@end

@implementation PlaceInformationViewController

+(PlaceInformationViewController *)initViewControllerWithAttrData:(AttractionData *)attData {
    PlaceInformationViewController * controller = [[PlaceInformationViewController alloc] initWithNibName:@"PlaceInformationViewController" bundle:nil];
//    controller.attData = attData;
    return controller;
}

+(PlaceInformationViewController *)initViewController:(Attractions *)atts {
    PlaceInformationViewController * controller = [[PlaceInformationViewController alloc] initWithNibName:@"PlaceInformationViewController" bundle:nil];
    controller.attrs = atts;
    return controller;
}


+(PlaceInformationViewController *)initController:(MostPopularAttractions *)mostAttData{
    PlaceInformationViewController * controller = [[PlaceInformationViewController alloc] initWithNibName:@"PlaceInformationViewController" bundle:nil];
    controller.mostAttData = mostAttData;
    return controller;
}

+(PlaceInformationViewController *)initControllerBookmark:(BookmarkAttractions *)bookmarkAttData{
    PlaceInformationViewController * controller = [[PlaceInformationViewController alloc] initWithNibName:@"PlaceInformationViewController" bundle:nil];
    controller.bookmarkAttData = bookmarkAttData;
    return controller;

}


+(PlaceInformationViewController *)initViewControllerWithAttrModel:(AttractionModel *)attModel andAttractionDataModel:(AttractionDataModel *)dataModel {
    PlaceInformationViewController * controller = [[PlaceInformationViewController alloc] initWithNibName:@"PlaceInformationViewController" bundle:nil];
    controller.attModel = attModel;
    controller.dataModel = dataModel;
    return controller;
}
+(PlaceInformationViewController *)initViewControllerWithAttractionModelOnly:(AttractionModel *)attractionModel {
    PlaceInformationViewController * controller = [[PlaceInformationViewController alloc] initWithNibName:@"PlaceInformationViewController" bundle:nil];
    controller.attractionModelOnly = attractionModel;
    return controller;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopVideoPlay) name:@"HideKGView" object:nil];

}

-(void)stopVideoPlay{
    [self.videoController stop];
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:false];
    self.navigationController.navigationBarHidden = true;
    
    reportsArray = [NSMutableArray new];
    reportsArray = [Reason getAll];
    
    btnMoreAttr.layer.cornerRadius = btnMoreAttr.frame.size.height / 2;
    btnMoreVideo.layer.cornerRadius = btnMoreVideo.frame.size.height / 2;
    btnBookMarkmain.layer.cornerRadius = btnBookMarkmain.frame.size.height / 2;
    btnBookMarkVideoSource.layer.cornerRadius = btnBookMarkVideoSource.frame.size.height / 2;
    [self registerNibs];
     [self showAttractionData];
    [self showBookmark];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:false];
    self.leftButton.frame = CGRectMake(0.0, self.view.frame.size.height - 50, 50.0, 50.0);
    self.rightButton.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 50, self.view.frame.size.height - 50, 50.0, 50.0);
    
    CGRect mf = self.view.frame;
    self.menuView1.frame = CGRectMake(mf.size.width-250,mf.size.height - 110, self.menuView1.frame.size.width, self.menuView1.frame.size.height);
   
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showAttractionData {
    if (_attrs) {
        lblattractionName.text = _attrs.name;
        lblDetails.text = _attrs.detail;
        lblAddress.text = _attrs.address;
        lblwebUrl.text = _attrs.web_url;
        lblPhoneNum.text = _attrs.phone_no;
    } else if(_attData){
        lblattractionName .text = _attData.info.name;
        lblDetails.text = _attData.info.detail;
        lblAddress.text = _attData.info.address;
        lblwebUrl.text = _attData.info.web_url;
        lblPhoneNum.text = _attData.info.phone_no;

    }else if(_mostAttData){
        lblattractionName .text = _mostAttData.name;
        lblDetails.text = _mostAttData.detail;
        lblAddress.text = _mostAttData.address;
        lblwebUrl.text = _mostAttData.web_url;
        lblPhoneNum.text = _mostAttData.phone_no;

    }else if (_bookmarkAttData){
        lblattractionName .text = _bookmarkAttData.name;
        lblDetails.text = _bookmarkAttData.detail;
        lblAddress.text = _bookmarkAttData.address;
        lblwebUrl.text = _bookmarkAttData.web_url;
        lblPhoneNum.text = _bookmarkAttData.phone_no;
    }
    else if (_attModel || _dataModel){
        lblattractionName.text = _dataModel.info.name;
        lblDetails.text = _dataModel.info.detail;
        lblAddress.text = _dataModel.info.address;
        lblwebUrl.text = _attModel.web_url;
        if (_dataModel.info.phone_no.length) {
            phoneIcon.hidden = NO;
            lblPhoneNum.text = _dataModel.info.phone_no;
        }
        else{
            //phoneIcon.hidden = YES;
        }
        NSLog(@"%@",_dataModel.info.cover_photo_url);
        [coverImage sd_setShowActivityIndicatorView:YES];
        [coverImage sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [coverImage sd_setImageWithURL:[NSURL URLWithString:_dataModel.info.cover_photo_url]
                           placeholderImage:[UIImage imageNamed:@"def_video_img.png"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                               if (!error) {
                                   coverImage.image = image;
                                   [self.view layoutIfNeeded];
                               }
                           }];
    }
    else if (_attractionModelOnly){
        lblattractionName.text = _attractionModelOnly.name;
        lblDetails.text = _attractionModelOnly.detail;
        lblAddress.text = _attractionModelOnly.address;
        lblwebUrl.text = _attractionModelOnly.web_url;
        if (_attractionModelOnly.phone_no.length) {
            phoneIcon.hidden = NO;
            lblPhoneNum.text = _attractionModelOnly.phone_no;
        }
        else{
            //phoneIcon.hidden = YES;
        }
        NSLog(@"%@",_attractionModelOnly.cover_photo_url);
        [coverImage sd_setShowActivityIndicatorView:YES];
        [coverImage sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [coverImage sd_setImageWithURL:[NSURL URLWithString:_attractionModelOnly.cover_photo_url]
                      placeholderImage:[UIImage imageNamed:@"def_video_img.png"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                          if (!error) {
                              coverImage.image = image;
                              [self.view layoutIfNeeded];
                          }
                      }];

    }
    //    lblhrsInfo.text = _attData.hours_info; //we have to show 00:00 - 00:00 data
}


- (IBAction)reportClicked:(id)sender {
    ReportView * view = [ReportView initWithNib];
    view.tblView.delegate = self;
    view.tblView.dataSource = self;
    [view.tblView reloadData];
    [view.btnSend addTarget:self action:@selector(sendReport) forControlEvents:UIControlEventTouchUpInside];
    [KGModalWrapper showWithContentView:view];
}

-(void)sendReport{
    if (!selectedSpamId) {
        [Utils showAlertWithMessage:@"First select a reason"];
        return;
    }
    [[PlayTripManager Instance] addSpam:selectedSpamId forPlan:@"" WithBlock:^(id result, NSString *error) {
        if (error) {
            [KGModalWrapper hideView];
            [Utils showAlertWithMessage:error];
        } else {
            [KGModalWrapper hideView];
            [Utils showAlertWithMessage:@"Reported successfully"];
        }
    }];
}

- (IBAction)shareClicked:(id)sender {
    NSString *textToShare = @"Play Trip!";
    NSURL *myWebsite = [NSURL URLWithString:@"http://www.playtrip.com/"];
    NSURL *androidLink = [NSURL URLWithString:@"https://play.google.com/store/apps/details?id=com.appone.playtrip&hl=en"];
    NSURL *appLink = [NSURL URLWithString:@"https://itunes.apple.com/us/app/playtrip/id1223364669?ls=1&mt=8"];
    
    NSArray *objectsToShare = @[textToShare, myWebsite, androidLink, appLink];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}

-(void)showBookmark{
    Account * account = [AccountManager Instance].activeAccount;
    if(account){
        if (account.isSkip == true) {
            lblBookmark.text = @"Bookmark";
            imgBookmark.image = [UIImage imageNamed:@"bookmark_tag_s.png"];
            
        }else {
            if(_attrs){
                if ([_attrs.bookmark_users_string containsString:account.userId]){
                    lblBookmark.text = @"Bookmarked";
                    imgBookmark.image = [UIImage imageNamed:@"bookmark_tag_s_brown.png"];
                } else {
                    lblBookmark.text = @"Bookmark";
                    imgBookmark.image = [UIImage imageNamed:@"bookmark_tag_us_brown.png"];
                }
                
            } else if(_attData){
                if ([_attData.info.bookmark_users_string containsString:account.userId]) {
                    lblBookmark.text = @"Bookmarked";
                    imgBookmark.image = [UIImage imageNamed:@"bookmark_tag_s_brown.png"];
                } else {
                    lblBookmark.text = @"Bookmark";
                    imgBookmark.image = [UIImage imageNamed:@"bookmark_tag_us_brown.png"];
                }
            }else if(_bookmarkAttData){
                if ([_bookmarkAttData.bookmark_users_string containsString:account.userId]){
                    lblBookmark.text = @"Bookmarked";
                    imgBookmark.image = [UIImage imageNamed:@"bookmark_tag_s_brown.png"];
                } else {
                    lblBookmark.text = @"Bookmark";
                    imgBookmark.image = [UIImage imageNamed:@"bookmark_tag_us_brown.png"];
                }
                
            }else{
                if ([_mostAttData.bookmark_users_string containsString:account.userId]) {
                    lblBookmark.text = @"Bookmarked";
                    imgBookmark.image = [UIImage imageNamed:@"bookmark_tag_s_brown.png"];
                } else {
                    lblBookmark.text = @"Bookmark";
                    imgBookmark.image = [UIImage imageNamed:@"bookmark_tag_us_brown.png"];
                }
            }
            

        }
    } else {
        lblBookmark.text = @"Bookmark";
        imgBookmark.image = [UIImage imageNamed:@"bookmark_tag_s.png"];
    }
}


- (IBAction)bookmarkClicked:(id)sender {
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    if ([lblBookmark.text isEqualToString:@"Bookmarked"]) {
        [SVProgressHUD showWithStatus:@"Removing Bookmark.."];
    } else {
        [SVProgressHUD showWithStatus:@"Bookmarking.."];
    }
    if(_attrs){
        attId = _attrs.entity_id;
    } else if(_attData){
        attId = _attData.entity_id;
    }else if(_mostAttData){
        attId = _mostAttData.entity_id;
    }else{
         attId = _bookmarkAttData.entity_id;
    }
    
    [[PlayTripManager Instance] addCountForModel:ModelAttraction withType:TypeBookmark forId:attId WithBlock:^(id result, NSString *error) {
        if(!error){
                if ([lblBookmark.text isEqualToString:@"Bookmarked"]) {
                lblBookmark.text = @"Bookmark";
                imgBookmark.image = [UIImage imageNamed:@"bookmark_tag_s.png"];
            } else {
                lblBookmark.text = @"Bookmarked";
                imgBookmark.image = [UIImage imageNamed:@"bookmark_tag_s_brown.png"];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"MostPopAttrUpdated" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"AttractionsUpdated" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BookmarkDetailsUpdated" object:nil];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"ProfileReloadTable" object:nil];
        }
        [SVProgressHUD dismiss];
    }];

}

- (IBAction)addToMyPlanClicked:(id)sender {
    
    if(_attData){
        attId = _attData.entity_id;
    }else if(_mostAttData){
        attId = _mostAttData.entity_id;
    }else{
        attId = _bookmarkAttData.entity_id;
    }

    
    
    
    AddAttractionToMyPlanView *view = [AddAttractionToMyPlanView initWithNibWithAttractionDataModel:_dataModel];
    [KGModalWrapper showWithContentView:view];
    
       
}


-(void)registerNibs {
    [collViewAttr registerClass:[RelatedCollectionCell class] forCellWithReuseIdentifier:@"RelatedCollectionCell"];
    
    [collViewAttr registerNib:[UINib nibWithNibName:@"RelatedCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"RelatedCollectionCell"];
    
    [collViewVideoTour registerClass:[RelatedCollectionCell class] forCellWithReuseIdentifier:@"RelatedCollectionCell"];
    
    [collViewVideoTour registerNib:[UINib nibWithNibName:@"RelatedCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"RelatedCollectionCell"];
}

#pragma mark -<UICollectionViewDelegate,UICollectionViewDataSource>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if ([collectionView isEqual:collViewAttr]) {
        return self.attModel.attr_data.count;
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    RelatedCollectionCell *cell = (RelatedCollectionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"RelatedCollectionCell" forIndexPath:indexPath];
    if ([collectionView isEqual:collViewAttr]) {
        AttractionDataModel *dataModel = self.attModel.attr_data[indexPath.row];
        
        NSString * imgId = dataModel.video_id.thumbnail;
        NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
        
        [cell.imgMain sd_setShowActivityIndicatorView:YES];
        [cell.imgMain sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [cell.imgMain sd_setImageWithURL:[NSURL URLWithString:urlString]
                      placeholderImage:[UIImage imageNamed:@"def_video_img.png"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                          if (!error) {
                              cell.imgMain.image = image;
                              [cell layoutIfNeeded];
                          }
                      }];
    }

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    AttractionDataModel *dataModel = self.attModel.attr_data[indexPath.row];

    NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,@"/uploads/",dataModel.video_id.file_name];
    NSLog(@"%@",urlString);
    [self playVideoInPopUpViewWithURL:[NSURL URLWithString:urlString]];
    
}

-(void)playVideoInPopUpViewWithURL:(NSURL *)url {
    if (url) {
        self.videoController = [[MPMoviePlayerController alloc] init];
        [self.videoController setContentURL:url];
        [self.videoController.view setFrame:CGRectMake (0, 0, popUpView.frame.size.width, popUpView.frame.size.height)];
        self.videoController.controlStyle = MPMovieControlStyleNone;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoPlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:self.videoController];
        [popUpView addSubview:self.videoController.view];
        [KGModalWrapper showWithContentView:popUpView];
        [self.videoController setControlStyle:MPMovieControlStyleNone];
        [self.videoController play];
    }
}
- (void)videoPlayBackDidFinish:(NSNotification *)notification {
    [self.videoController stop];
    [self.videoController.view removeFromSuperview];
    self.videoController = nil;
    [KGModalWrapper hideView];
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(80,80);
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    /*
     this function is used for set number of rows in tableview
     */
    
    return reportsArray.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    /*
     this method is used to set cell for table view
     */
    
    
    ReportCell * cell = (ReportCell *)[tableView dequeueReusableCellWithIdentifier:@"ReportCell"];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ReportCell" owner:self options:nil];
        for (id currentObject in topLevelObjects) {
            if ([currentObject isKindOfClass:[UITableViewCell class]]) {
                cell =  (ReportCell *) currentObject;
                break;
            }
        }
    }
    Reason * reas = [reportsArray objectAtIndex:indexPath.row];
    cell.lblTitle.text = reas.title;
    cell.lblDescription.text = reas.desc;
    cell.lblDescription.textColor = [UIColor blackColor];
    cell.lblTitle.textColor = [UIColor blackColor];
    cell.contentView.backgroundColor = [UIColor whiteColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //    [tableView reloadData];
    
    ReportCell * cell = (ReportCell *)[tableView cellForRowAtIndexPath:indexPath];
    //        cell.backBrownView.hidden = false;
    cell.contentView.layer.backgroundColor = [ColorConstants appBrownColor].CGColor;
    cell.lblTitle.textColor = [UIColor whiteColor];
    cell.lblDescription.textColor = [UIColor whiteColor];
    
    Reason * reas = [reportsArray objectAtIndex:indexPath.row];
    selectedSpamId = reas.entity_id;
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ReportCell * cell = (ReportCell *)[tableView cellForRowAtIndexPath:indexPath];
    //        cell.backBrownView.hidden = false;
    cell.contentView.layer.backgroundColor = [UIColor clearColor].CGColor;
    cell.lblTitle.textColor = [UIColor blackColor];
    cell.lblDescription.textColor = [UIColor blackColor];
    
}



- (IBAction)moreAttrClicked:(id)sender {
    RelatedAttractionsVideoViewController * controller = [RelatedAttractionsVideoViewController initViewController];
    controller.attModel = self.attModel;
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)moreVideoClicked:(id)sender {
    RelatedVideoTourViewController * controller = [RelatedVideoTourViewController initViewController];
    [self.navigationController pushViewController:controller animated:true];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

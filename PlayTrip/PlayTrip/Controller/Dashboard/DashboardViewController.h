
#import <UIKit/UIKit.h>
#import "MenuButtonViewController.h"
#import "PlayTripManager.h"
#import <CoreLocation/CoreLocation.h>
#import "MostPopularVideoTourViewController.h"
#import "AttractionModel.h"
#import "Account.h"
#import <MediaPlayer/MediaPlayer.h>



@interface DashboardViewController : MenuButtonViewController <UISearchResultsUpdating,UICollectionViewDelegate, UICollectionViewDataSource,CLLocationManagerDelegate,UISearchBarDelegate, CLLocationManagerDelegate, AccountAuthenticateDelegate> {
    
    IBOutlet UIView *topView;
    IBOutlet UIView *middleView;
    IBOutlet UIView *bottomView;

    IBOutlet UILabel *lblDistrictData;
    IBOutlet UILabel *lblVideoTourData;
    IBOutlet UILabel *lblUserData;


    IBOutlet UIButton *btnDistrictMore;
    IBOutlet UIButton *btnVideoTourMore;

    IBOutlet UIButton *btnUserMore;
    IBOutlet UIScrollView *scrollViewMain;

    IBOutlet UICollectionView *collDistrictView;
    IBOutlet UICollectionView *collLatestView;
    IBOutlet UICollectionView *collRecommendedView;
    IBOutlet UICollectionView *collMostPopularVideoTour;
    IBOutlet UICollectionView *collAttrView;
    
    IBOutlet UIView *viewLatest;
    IBOutlet UIView *viewPopularTour;
    IBOutlet UIView *viewPopularAttr;
    
    
    IBOutlet UILabel *lblTitleLatest;
    IBOutlet UILabel *lblTitlePopularTour;
    IBOutlet UILabel *lblTitlePopularAttr;
    
    //    IBOutlet UILabel *lblPlanData;
    
    IBOutlet UILabel *lblNoLatest;
    IBOutlet UILabel *lblNoPopularTour;
    IBOutlet UILabel *lblNoPopularAttr;
    
    int selIndex;
    
    NSMutableArray * arrDistricts;
    NSMutableArray * arrUsers;
    NSMutableArray * arrVideoTour;
    NSMutableArray * arrMostPopularPlan;
    NSMutableArray * arrMostPopularVideoTour;
    NSMutableArray * arrMostPopularAttractions;

    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    CLLocationCoordinate2D target;
    
    int districtCount;
    int videoTourCount;
    int usersCount;

    UISearchBar *searchBar;
    
    
    IBOutlet UILabel *lblDistrict;
    IBOutlet UILabel *lblRecommendedUser;
    
    
    IBOutlet UIButton *btnMore;
    IBOutlet UIButton *btnMore2;
    IBOutlet UIButton *btnMore22;
    IBOutlet UIButton *btnMore222;
    IBOutlet UIButton *btnMore3;
    
    NSMutableArray * allAttractions;
    NSMutableArray * uniqueCountries;
    
    
    
    NSMutableArray * allLatestPlans;
    //NSMutableArray * allMostPopularPlans;
    NSMutableArray * allMostPopularAttractions;
    NSMutableArray * allRecomendedUsers;

    NSDictionary *allCountriesDicts;
    
    IBOutlet UIView *popUpView;

    
}
- (IBAction)districtMoreClicked:(id)sender;
- (IBAction)latestVideoMoreClicked:(id)sender;
- (IBAction)recommendedUsersMoreClicked:(id)sender;
- (IBAction)sortClicked:(id)sender;
- (IBAction)popularTourMoreClicked:(id)sender;
- (IBAction)popularAttrMoreClicked:(id)sender;
@property (strong, nonatomic) MPMoviePlayerController *videoController;
+(DashboardViewController *)initViewController;

@end

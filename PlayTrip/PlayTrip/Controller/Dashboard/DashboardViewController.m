
#import "DashboardViewController.h"
#import "AppDelegate.h"
#import "VideoToBeUploadedViewController.h"
#import "SearchData.h"
#import "LocalAttrTags.h"
#import "LocalVideoObject.h"
#import "VideoViewController.h"
#import "SearchTabViewController.h"
#import "MostPopularPlan.h"
#import "MostPopularVideoTour.h"
#import "ActionSheetStringPicker.h"
#import "AttractionData.h"
#import "Attractions.h"
#import "DistrictCollectionCell.h"
#import "LatestVideoTourCollectionViewCell.h"
#import "DistrictDetailViewController.h"
#import "AttractionsVideoCell.h"
#import "UserTabViewController.h"
#import "LatestVideoTourViewController.h"
#import "MostPopularAttractionsVideoViewController.h"
#import "MHTabBarController.h"
#import "CommonUser.h"
#import "Users.h"
#import "District.h"
#import "UserCollectionViewCell.h"
#import "URLSchema.h"
#import "PlayTripManager.h"
#import "Plan.h"
#import "AccountManager.h"
#import "VideoTour.h"
#import "ItineraryViewController.h"
#import "VideoDetailsViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "DistrictViewController.h"
#import "UserDetailViewController.h"
#import "ColorConstants.h"
#import "MostPopularAttractions.h"
#import "Info.h"
#import "PlaceInformationViewController.h"
#import "VideoPostViewController.h"
#import "SettingsViewController.h"
#import "PlanModel.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import <LinqToObjectiveC/LinqToObjectiveC.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import "LocationModel.h"
#import "Localisator.h"
#import "KGModalWrapper.h"

@interface DashboardViewController (){
    BOOL isAttractionPickerSelected ;
}

@property NSArray * arrayOfLanguages;

@end

@implementation DashboardViewController

+(DashboardViewController *)initViewController {
    DashboardViewController * controller = [[DashboardViewController alloc] initWithNibName:@"DashboardViewController" bundle:nil];
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.arrayOfLanguages = [[[Localisator sharedInstance] availableLanguagesArray] copy];

    [self registerNibs];
    self.navigationController.navigationBarHidden = false;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveLanguageChangedNotification:)
                                                 name:kNotificationLanguageChanged
                                               object:nil];

    //NSLog (@"Font families: %@", [UIFont familyNames]);

    
   // [self gotoVideoTemp];
   // return;
    [self setNavBarItems];
    
    lblUserData.hidden = YES;
    lblDistrictData.hidden = YES;
    lblVideoTourData.hidden = YES;
    lblNoLatest.hidden = YES;
    
    arrDistricts = [NSMutableArray new];
    arrUsers = [NSMutableArray new];
    arrVideoTour = [NSMutableArray new];
    arrMostPopularPlan = [NSMutableArray new];
    arrMostPopularVideoTour = [NSMutableArray new];
    locationManager = [[CLLocationManager alloc] init];
    //[self gotoVideoViewController];
    isAttractionPickerSelected = NO;
}

- (void) receiveLanguageChangedNotification:(NSNotification *) notification
{
    if ([notification.name isEqualToString:@"languageChanged"])
    {
        [self setLocalizedStrings];
    }
}


-(void)setLocalizedStrings {
    

    [lblDistrict setText:LOCALIZATION(@"country")];
    [lblRecommendedUser setText:LOCALIZATION(@"recommended_users")];
    [lblDistrictData setText:LOCALIZATION(@"no_data_found")];
    [lblNoLatest setText:LOCALIZATION(@"no_data_found")];
    [lblNoPopularTour setText:LOCALIZATION(@"no_data_found")];
    [lblNoPopularAttr setText:LOCALIZATION(@"no_data_found")];
    [lblUserData setText:LOCALIZATION(@"no_data_found")];
    
    [lblTitleLatest setText:LOCALIZATION(@"lastest_video_tour")];
    [lblTitlePopularTour setText:LOCALIZATION(@"mostpopular_video_tour")];
    [lblTitlePopularAttr setText:LOCALIZATION(@"mostpopular_attracttion_video")];
    
    [btnMore setTitle:LOCALIZATION(@"more") forState:UIControlStateNormal];
    [btnMore2 setTitle:LOCALIZATION(@"more") forState:UIControlStateNormal];
    [btnMore22 setTitle:LOCALIZATION(@"more") forState:UIControlStateNormal];
    [btnMore222 setTitle:LOCALIZATION(@"more") forState:UIControlStateNormal];
    [btnMore3 setTitle:LOCALIZATION(@"more") forState:UIControlStateNormal];
    
    //TODO delete
    
    
    NSLog(@"MORE is %@", LOCALIZATION(@"more"));
    
    [self.view layoutIfNeeded];
}

-(void)gotoVideoViewController{
    UIAlertController * alert  = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Do you want to continue with last session?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        //yes pressed
    
        VideoViewController * controller = [VideoViewController initViewController];
        [self.navigationController pushViewController:controller animated:true];

    }];
    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        // no pressed
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"videosData"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        
        
        
        
    }];

    [alert addAction:yesAction];
    [alert addAction:noAction];
    [self.navigationController presentViewController:alert animated:true completion:nil];
}







-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = false;
    [self setNavBarItems];
    
    
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager requestAlwaysAuthorization];
    [locationManager startUpdatingLocation];
    
    viewLatest.hidden = false;
    viewPopularTour.hidden = true;
    viewPopularAttr.hidden = true;
    searchBar.text = @"";
    
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getLatestVideoTour) name:@"LatestVideoTourUpdated" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMostPopularVideoTour) name:@"MostPopularVideoTourUpdated" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getMostPopularattractions) name:@"MostPopAttrUpdated" object:nil];
    
         
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getRecommendedUsers) name:@"RecommendedUsersUpdated" object:nil];
     
    
    BOOL isBookmarkUserUpd = [Utils getBOOLForKey:@"BookmarkUserUpd"];
    if (isBookmarkUserUpd) {
        [self getRecommendedUsers];
        [Utils setBOOL:NO Key:@"BookmarkUserUpd"];
    }
    
    [self setLocalizedStrings];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    btnDistrictMore.layer.cornerRadius = 15;
    btnVideoTourMore.layer.cornerRadius = 15;
    btnUserMore.layer.cornerRadius = 15;
    
    [self addBottomBorderForView:topView];
    [self addBottomBorderForView:middleView];
    
    if (![[AppDelegate appDelegate] isReachable]) {
        [Utils showAlertWithMessage:kCheckInternet];
        return;
    }
    
    [self getAllCountries];
    [self getMostRecomendedUsersNew];

    
    Account * account = [AccountManager Instance].activeAccount;
    if (account) {
        if (account.isSkip == true || account.isLoggedIn == true) {
            if (account.isLoggedIn == true) {
                
                Account * account = [AccountManager Instance].activeAccount;
                [account getUserDetails:self];
               
            }
            
            if (lblTitleLatest.tag == 0) {
                [self getLatestVideoTourNew];
            }
            else if(lblTitleLatest.tag == 1){
                [self getMostPopularVideoTourNew];
            }
            else{
                isAttractionPickerSelected = YES;
                [self getMostPopularVideoTourByAttractionNew];
            }
            //[self loadDummyUsers];
            
            
            if (arrDistricts.count <= 0) {
                
                //[self getDistricts];//TODO//
                //[self checkLocalVideoObjects];//TODO//
            }
        }
    }
    scrollViewMain.contentSize = CGSizeMake(scrollViewMain.frame.size.width, 600);
    
    collMostPopularVideoTour.hidden = true;
    collAttrView.hidden = true;
    collLatestView.hidden = false;
}

-(void)registerNibs {
    // Arun :: registering nib files
    [collDistrictView registerClass:[DistrictCollectionCell class] forCellWithReuseIdentifier:@"DistrictCollectionCell"];
    UINib *cellNib = [UINib nibWithNibName:@"DistrictCollectionCell" bundle:nil];
    [collDistrictView registerNib:cellNib forCellWithReuseIdentifier:@"DistrictCollectionCell"];
    [collLatestView registerClass:[LatestVideoTourCollectionViewCell class] forCellWithReuseIdentifier:@"LatestVideoTourCollectionViewCell"];
    [collLatestView registerNib:[UINib nibWithNibName:@"LatestVideoTourCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"LatestVideoTourCollectionViewCell"];
    [collMostPopularVideoTour registerClass:[LatestVideoTourCollectionViewCell class] forCellWithReuseIdentifier:@"LatestVideoTourCollectionViewCell"];
    [collMostPopularVideoTour registerNib:[UINib nibWithNibName:@"LatestVideoTourCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"LatestVideoTourCollectionViewCell"];
    [collAttrView registerClass:[AttractionsVideoCell class] forCellWithReuseIdentifier:@"AttractionsVideoCell"];
    [collAttrView registerNib:[UINib nibWithNibName:@"AttractionsVideoCell" bundle:nil] forCellWithReuseIdentifier:@"AttractionsVideoCell"];
    [collRecommendedView registerClass:[UserCollectionViewCell class] forCellWithReuseIdentifier:@"UserCollectionViewCell"];
    [collRecommendedView registerNib:[UINib nibWithNibName:@"UserCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"UserCollectionViewCell"];
}

#pragma mark - New Server Calls Methods
-(void)getAllCountries{
    if (!uniqueCountries) {
        uniqueCountries = [[NSMutableArray alloc] init];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD showWithStatus:@"Getting Countries..."];
    }
    [[PlayTripManager Instance] getAllLocationWithDictionary:nil andBlockNew:^(id result, NSString *error) {
        [SVProgressHUD dismiss];
        if (!error) {
            NSError* errorData;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                 options:kNilOptions
                                                                   error:&errorData];
            if (errorData) {
                NSLog(@"ERROR GOTTEN = %@" ,errorData.description);
            }
            
            if ([[json objectForKey:@"data"] isEqual:[NSNull null]]) {
                return;
            }
            
            
            NSArray* locationDicts = [json objectForKey:@"data"];
            uniqueCountries = [[NSMutableArray alloc] init];
            for (NSDictionary *locationDict in locationDicts) {
                NSError *error;
                LocationModel *locationModel = [[LocationModel alloc] initWithDictionary:locationDict error:&error];
                if (error) {
                    NSLog(@"ERROR GOTTEN AT JSONMODEL = %@" ,error.description);
                }
                else{
                    if (![uniqueCountries containsObject:locationModel]) {
                        [uniqueCountries addObject:locationModel];
                    }
                }
                
            }
            uniqueCountries = [[uniqueCountries sortedArrayUsingComparator:^NSComparisonResult(LocationModel *a, LocationModel *b) {
                int aValue = [a.count intValue];
                int bValue = [b.count intValue];
                return aValue < bValue;
            }] mutableCopy];
            NSLog(@"Total Locations Gotten = %lu" ,(unsigned long)uniqueCountries.count);

            dispatch_async(dispatch_get_main_queue(), ^{
    
                [collDistrictView reloadData];
            });
        }
    }];
}




-(void)getMostRecomendedUsersNew{
    if (!allRecomendedUsers) {
        allRecomendedUsers = [[NSMutableArray alloc] init];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD showWithStatus:@"Getting Users..."];
    }
    [[PlayTripManager Instance] getRecommendedUsersWithBlockNew:^(id result, NSString *error) {
        [SVProgressHUD dismiss];
        if (!error) {
            NSError* errorData;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                 options:kNilOptions
                                                                   error:&errorData];
            if (errorData) {
                NSLog(@"ERROR GOTTEN = %@" ,errorData.description);
            }
            NSArray* userDicts = [json objectForKey:@"data"];
            allRecomendedUsers = [[NSMutableArray alloc] init];
            for (NSDictionary *userDict in userDicts) {
                NSError *error;
                UserModel *userModel = [[UserModel alloc] initWithDictionary:userDict error:&error];
                if (error) {
                    NSLog(@"ERROR GOTTEN AT JSONMODEL = %@" ,error.description);
                }
                if (![allRecomendedUsers containsObject:userModel]) {
                    [allRecomendedUsers addObject:userModel];
                }
            }
            NSLog(@"Total Usrs Gotten = %lu" ,(unsigned long)allRecomendedUsers.count);
            //allRecomendedUsers = [[[allRecomendedUsers reverseObjectEnumerator] allObjects] mutableCopy];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if (!allRecomendedUsers.count) {
                    //[self loadDummyUsers];
                }
                
                
                [collRecommendedView reloadData];
            });
        }
    }];
}

-(void)loadDummyUsers {
    UserModel *userModel = [[UserModel alloc] init];
    userModel.full_name = @"saqib";
    [allRecomendedUsers addObject:userModel];
    userModel = [[UserModel alloc] init];
    userModel.full_name = @"123";
    [allRecomendedUsers addObject:userModel];
    userModel = [[UserModel alloc] init];
    userModel.full_name = @"saqib7777";
    [allRecomendedUsers addObject:userModel];
    userModel = [[UserModel alloc] init];
    userModel.full_name = @"11222333";
    [allRecomendedUsers addObject:userModel];
    [collRecommendedView reloadData];

}


-(void)getAllVideoTourNew{
    

    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Getting Attractionss..."];
    
    NSDictionary *parameters = @{ @"sort": @{ @"create_date": @-1 },
                                  @"limit": @{ @"skip": @"", @"limit": @20 },
                                  @"where": @{ @"type": @"plan"}};
    [[PlayTripManager Instance] getLatestVideoTourWithParameters:parameters WithBlockNew:^(id result, NSString *error) {
        [SVProgressHUD dismiss];
        if (!error) {
            NSError* errorData;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                 options:kNilOptions
                                                                   error:&errorData];
            if (errorData) {
                NSLog(@"ERROR GOTTEN = %@" ,errorData.description);
            }
            NSArray* plansDicts = [json objectForKey:@"data"];
            NSMutableArray *allPlans = [[NSMutableArray alloc] init];
            for (NSDictionary *planDict in plansDicts) {
                NSError *error;
                PlanModel *planModel = [[PlanModel alloc] initWithDictionary:planDict error:&error];
                if (error) {
                    NSLog(@"ERROR GOTTEN AT JSONMODEL = %@" ,error.description);
                }
                if (planModel.attractions.count > 0) {
                    for (AttractionModel *attraction in planModel.attractions) {
                        if (attraction.attr_data.count) {
                            for ( AttractionDataModel *attractionData in attraction.attr_data) {
                                if (attractionData.info.country_id) {
                                    if (![allPlans containsObject:planModel]) {
                                        [allPlans addObject:planModel];
                                    }
                                }
                            }
                        }
                    }
                    
                    
                    
                }
            }
            NSLog(@"Total Plans Gotten = %lu" ,(unsigned long)allPlans.count);
            
            
            allCountriesDicts = [allPlans linq_groupBy:^id(PlanModel *plan) {
                AttractionModel *attraction = [plan.attractions objectAtIndex:0];
                return attraction.country_id.name;
            }];
            NSArray *allCountriesDictTitles = [[allCountriesDicts allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
            NSLog(@"Single Oils Dictionary Titles Count= %lu" , (unsigned long)allCountriesDictTitles.count) ;
            
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [collDistrictView reloadData];
            });
        }
    }];
}




-(void)getMostPopularVideoTourNew{
    if (!allLatestPlans) {
        allLatestPlans = [[NSMutableArray alloc] init];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD showWithStatus:@"Getting Attractionss..."];
    }
    
    NSDictionary *parameters = @{ @"where": @{ @"name": @"" },
                                  @"limit": @{ @"skip": @"", @"limit": @20 } };
    
    [[PlayTripManager Instance] getMostPopularPlansWithParameters:parameters WithBlockNew:^(id result, NSString *error) {
        [SVProgressHUD dismiss];
        if (!error) {
            NSError* errorData;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                 options:kNilOptions
                                                                   error:&errorData];
            if (errorData) {
                NSLog(@"ERROR GOTTEN = %@" ,errorData.description);
            }
            NSNumber * isSuccessNumber = (NSNumber *)[json objectForKey: @"is_error"];
            allLatestPlans = [[NSMutableArray alloc] init];

            if ([isSuccessNumber intValue] == 1) {
                
                NSLog(@"ERROR GOTTEN AT is_error = %@" ,[json objectForKey: @"message"]);

            }
            else{
                NSArray* plansDicts = [json objectForKey:@"data"];
                for (NSDictionary *planDict in plansDicts) {
                    NSError *error;
                    PlanModel *planModel = [[PlanModel alloc] initWithDictionary:planDict error:&error];
                    if (error) {
                        NSLog(@"ERROR GOTTEN AT JSONMODEL = %@" ,error.description);
                    }
                    if (![allLatestPlans containsObject:planModel]) {
                        [allLatestPlans addObject:planModel];
                    }
                }
                NSLog(@"Total Plans Gotten = %lu" ,(unsigned long)allLatestPlans.count);
            }
            
            
            //allLatestPlans = [[[allLatestPlans reverseObjectEnumerator] allObjects] mutableCopy];
            dispatch_async(dispatch_get_main_queue(), ^{
                [collLatestView reloadData];
            });
        }
    }];
}

-(void)getMostPopularVideoTourByAttractionNew{
    if (!allMostPopularAttractions) {
        allMostPopularAttractions = [[NSMutableArray alloc] init];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD showWithStatus:@"Getting Attractions..."];
    }
    
    
    [[PlayTripManager Instance] getMostPopularAttractionVideosWithBlockNew:^(id result, NSString *error) {
        [SVProgressHUD dismiss];
        if (!error) {
            NSError* errorData;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                 options:kNilOptions
                                                                   error:&errorData];
            if (errorData) {
                NSLog(@"ERROR GOTTEN = %@" ,errorData.description);
            }
            
            NSArray* attractionsDicts = [json objectForKey:@"data"];

            allMostPopularAttractions = [[NSMutableArray alloc] init];

            for (NSDictionary *attractionDict in attractionsDicts) {
                NSError *error;
                AttractionDataModel *attractionData = [[AttractionDataModel alloc] initWithDictionary:attractionDict error:&error];
                if (error) {
                    NSLog(@"ERROR GOTTEN AT JSONMODEL = %@" ,error.description);
                }
                if (![allMostPopularAttractions containsObject:attractionData]) {
                    [allMostPopularAttractions addObject:attractionData];
                }
            }
            NSLog(@"Total Attractions Video Gotten = %lu" ,(unsigned long)allMostPopularAttractions.count);
            dispatch_async(dispatch_get_main_queue(), ^{
                [collLatestView reloadData];
            });

            
            
            
            
            /*
            NSArray* plansDicts = [json objectForKey:@"data"];
            allLatestPlans = [[NSMutableArray alloc] init];
            for (NSDictionary *planDict in plansDicts) {
                NSError *error;
                PlanModel *planModel = [[PlanModel alloc] initWithDictionary:planDict error:&error];
                if (error) {
                    NSLog(@"ERROR GOTTEN AT JSONMODEL = %@" ,error.description);
                }
                if (![allLatestPlans containsObject:planModel]) {
                    [allLatestPlans addObject:planModel];
                }
            }
            NSLog(@"Total Plans Gotten = %lu" ,(unsigned long)allLatestPlans.count);
            //allLatestPlans = [[[allLatestPlans reverseObjectEnumerator] allObjects] mutableCopy];
            dispatch_async(dispatch_get_main_queue(), ^{
                [collLatestView reloadData];
            });
             
             */
        }
    }];
}


-(void)getLatestVideoTourNew{
    if (!allLatestPlans) {
        allLatestPlans = [[NSMutableArray alloc] init];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD showWithStatus:@"Getting Attractions..."];
    }
    NSDictionary *parameters = @{ @"sort": @{ @"create_date": @-1 },
                                  @"limit": @{ @"skip": @"", @"limit": @20 },
                                  @"where": @{ @"type": @"videotour"}};
    [[PlayTripManager Instance] getLatestVideoTourWithParameters:parameters WithBlockNew:^(id result, NSString *error) {
        [SVProgressHUD dismiss];
        if (!error) {
            NSError* errorData;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                 options:kNilOptions
                                                                   error:&errorData];
            if (errorData) {
                NSLog(@"ERROR GOTTEN = %@" ,errorData.description);
            }
            NSArray* plansDicts = [json objectForKey:@"data"];
            allLatestPlans = [[NSMutableArray alloc] init];
            for (NSDictionary *planDict in plansDicts) {
                NSError *error;
                PlanModel *planModel = [[PlanModel alloc] initWithDictionary:planDict error:&error];
                if (error) {
                    NSLog(@"ERROR GOTTEN AT JSONMODEL = %@" ,error.description);
                }
                if (![allLatestPlans containsObject:planModel]) {
                    [allLatestPlans addObject:planModel];
                }
            }
            NSLog(@"Total Plans Gotten = %lu" ,(unsigned long)allLatestPlans.count);
            //allLatestPlans = [[[allLatestPlans reverseObjectEnumerator] allObjects] mutableCopy];

            dispatch_async(dispatch_get_main_queue(), ^{
                [collLatestView reloadData];
            });
        }
    }];
}

-(void)getAllAttractionsNew {
    if (!allAttractions) {
        allAttractions = [[NSMutableArray alloc] init];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD showWithStatus:@"Getting Attractionss..."];
    }
    
    [[PlayTripManager Instance] getAttractionListWithBlockNew:^(id result, NSString *error) {
        [SVProgressHUD dismiss];
        if (!error) {
            NSError* error;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                 options:kNilOptions
                                                                   error:&error];
            
            NSArray* attractionsDicts = [json objectForKey:@"data"];
            
            
            
            for (NSDictionary *attractionDict in attractionsDicts) {
                NSError* modelError;
                
                AttractionModel *newModel = [[AttractionModel alloc] initWithDictionary:attractionDict error:&modelError];
                if (modelError) {
                    NSLog(@"ERROR GOTTEN = %@" , modelError.description);
                }
                else{
                    if (![allAttractions containsObject:newModel]) {
                        [allAttractions addObject:newModel];
                        NSPredicate *bPredicate = [NSPredicate predicateWithFormat:@"SELF.country_id._id contains[cd] %@",newModel.country_id._id];
                        
                        NSArray *filteredCountriesArray = [uniqueCountries filteredArrayUsingPredicate:bPredicate];
                        if (!filteredCountriesArray.count) {
                            [uniqueCountries addObject:newModel];
                        }
                    }
                }
                uniqueCountries = [[uniqueCountries sortedArrayUsingComparator:^NSComparisonResult(LocationModel *a, LocationModel *b) {
                    int aValue = [a.count intValue];
                    int bValue = [b.count intValue];
                    return aValue > bValue;
                }] mutableCopy];

            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [collDistrictView reloadData];
            });
            
        }
        else{
            
        }
    }];
}



-(void)gotoVideoTemp{
//    VideoDetailsViewController * controller = [VideoDetailsViewController initViewControllerWithURL:nil];
    VideoPostViewController * controller = [VideoPostViewController initViewControllerWithURL:nil];
    [self.navigationController pushViewController:controller animated:true];
}

-(void)addBottomBorderForView:(UIView *)view {
    CALayer *bottomBorder = [CALayer layer];
    bottomBorder.frame = CGRectMake(0.0f, view.frame.size.height, view.frame.size.width, 1.0f);
    bottomBorder.backgroundColor = [UIColor colorWithWhite:0.8f alpha:1.0f].CGColor;
    [view.layer addSublayer:bottomBorder];
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    currentLocation = (CLLocation *)[locations lastObject];
    target = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude);
}

-(void)setNavBarItems {
    // Arun :: customizing the nav bar icons
    self.navigationController.navigationBar.backgroundColor = [ColorConstants appYellowColor];
    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,45)];
    searchBar.delegate = self;
    searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
    searchBar.barTintColor = [ColorConstants appYellowColor];
    searchBar.translucent = NO;
    searchBar.opaque = NO;
    
    self.navigationItem.titleView = searchBar;
    
    UIImage *buttonImage = [UIImage imageNamed:@"share_brown.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(shareButton:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
    
    UIButton *btnRight = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight setTitle:LOCALIZATION(@"advance_search") forState:UIControlStateNormal];
    btnRight.titleLabel.font = [UIFont systemFontOfSize:10];
    btnRight.backgroundColor = [ColorConstants appBrownColor];
    [btnRight addTarget:self action:@selector(advancedSearchClicked) forControlEvents:UIControlEventTouchUpInside];
    btnRight.frame = CGRectMake(0, 0, 100, 30);
    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithCustomView:btnRight];
    self.navigationItem.rightBarButtonItem = rightBarItem;
}

-(void)commanApiWithModel:(NSString *)modelValue WithId:(NSString *)idValue WithType:(NSString *)typeValue {
    Account * account = [AccountManager Instance].activeAccount;
    if(account){
        if (account.isSkip == true) {
            [[AppDelegate appDelegate] showLogin:nil];
            return;
        }
    }
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Please wait.."];
    [[PlayTripManager Instance] addCountForModel:modelValue withType:typeValue forId:idValue WithBlock:^(id result, NSString *error) {
        [SVProgressHUD dismiss];
        if(!error){
            if (modelValue == ModelPlan) {
                if (!collLatestView.hidden) {
                    [self getAllVideoTourNew];
                } else if (!collMostPopularVideoTour.hidden) {
                    [self getMostPopularVideoTourNew];
                }
            }else if (modelValue == ModelAttraction){
                
                [self getMostPopularVideoTourByAttractionNew];
                
            }else {
                [self getMostRecomendedUsersNew];
            }
        }
    }];
}



-(void)getDistricts {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Please wait.."];
    [[PlayTripManager Instance] getDistrictListWithBlock:^(id result, NSString *error) {
        if (!error) {
            arrDistricts = [District getAll];
            
            
            for (District *district in arrDistricts) {
                //NSLog(@"District Entity Id is %@", district.entity_id) ;
                
                Attractions *vt = [Attractions getByEntityId:district.entity_id];
                if (vt) {
                    //NSLog(@"Video  is %@", vt.name) ;
                }
                
            }
            
            
            
            
            [collDistrictView reloadData];
            
            
            
        }
        [SVProgressHUD dismiss];
        if (arrVideoTour.count <= 0) {
            [self getLatestVideoTour];
        }
    }];
}

-(void)getLatestVideoTour{
    
    
    NSMutableDictionary *where = [NSMutableDictionary new];
    [where setObject:@"true" forKey:@"is_published"];
//    [where setObject:[NSNumber numberWithBool:false] forKey:@"is_deleted"];
    NSMutableDictionary *sort = [NSMutableDictionary new];
    [sort setObject:@"-1" forKey:@"create_date"];
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Please wait.."];
    
    [[PlayTripManager Instance] getLatestVideoTourWithWhere:where WithSort:sort WithBlock:^(id result, NSString *error) {
        if(!error){
            arrVideoTour = [VideoTour getAll];
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"create_date" ascending:NO];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            arrVideoTour = (NSMutableArray *)[arrVideoTour sortedArrayUsingDescriptors:sortDescriptors];
            [collLatestView reloadData];
        }
        [SVProgressHUD dismiss];
        if (arrUsers.count <= 0) {
            [self getRecommendedUsers];
        }
    }];
     
}

-(void)getMostPopularVideoTour {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Please wait.."];
    [[PlayTripManager Instance] getMostPopularVideoTourWithBlock:^(id result, NSString *error) {
        if(!error){
            arrMostPopularVideoTour = [MostPopularVideoTour getAll];
            [collMostPopularVideoTour reloadData];
        }
        [SVProgressHUD dismiss];
        
    }];
}

-(void)getMostPopularattractions {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Please wait.."];
    [[PlayTripManager Instance]getMostPopularAttractionsWithBlock:^(id result, NSString *error) {
        if(!error){
            arrMostPopularAttractions = [MostPopularAttractions getAll];
            [collAttrView reloadData];
            if(arrMostPopularAttractions.count <= 0){
                lblNoPopularAttr.hidden = NO;
            }
        }
        [SVProgressHUD dismiss];
        if (arrUsers.count <= 0) {
            [self getRecommendedUsers];
        }
    }];
}

-(void)getRecommendedUsers {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Please wait.."];
    [[PlayTripManager Instance]getRecommendedUsersListWithBlock:^(id result, NSString *error) {
        if (!error) {
            arrUsers = [Users getAll];
            
            
            NSArray *sortedArray;
            sortedArray = [arrUsers sortedArrayUsingComparator:^NSComparisonResult(Users *a, Users *b) {
                float firstValue = ([a.total_view integerValue] + [a.total_bookmark integerValue]) / 2;
                float secondalue = ([b.total_view integerValue] + [b.total_bookmark integerValue]) / 2;
                
                //float firstValue = ([a.total_bookmark integerValue]) / 1;
                //float secondalue = ([b.total_bookmark integerValue]) / 1;

                if ( firstValue > secondalue ) {
                    return (NSComparisonResult)NSOrderedAscending;
                } else if ( firstValue < secondalue ) {
                    return (NSComparisonResult)NSOrderedDescending;
                } else {
                    return (NSComparisonResult)NSOrderedSame;
                }
            }];
            arrUsers = [[NSMutableArray alloc] initWithArray:sortedArray];
            [collRecommendedView reloadData];
        }
        
        
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        NSArray *vidData = [userDefaults objectForKey:@"videosData"];
        if (vidData.count>0) {
             [self gotoVideoViewController];
        }
        
        
       
        [SVProgressHUD dismiss];
        [self getReports];
    }];
}

-(void)getReports {
    [[PlayTripManager Instance] getReportsListWithBlock:^(id result, NSString *error) {
        [SVProgressHUD dismiss];
        [self loadAllPlansOfUser];//TODO//
    }];
}

-(void)loadAllPlansOfUser {
    Account * account = [AccountManager Instance].activeAccount;
    [[PlayTripManager Instance] loadPlanByUserID:account.userId WithBlock:^(id result, NSString *error) {
        [self getAttractions];
    }];
}

-(void)getAttractions {
    [[PlayTripManager Instance] getAttractionsListWithDistance:@"50" WithLattitude:target.latitude WithLongitude:target.longitude WithBlock:^(id result, NSString *error) {
        [self checkVidSegments];
    }];
}

-(void)checkVidSegments {
    NSMutableArray * localArray = [Utils getArrayForKey:VidSegArray];
    NSMutableArray * localArray1 = [NSMutableArray new];
    if (localArray.count > 0) {
       // NSLog(@"%@",localArray);
        for (NSData * aData in localArray) {
            NSString* newStr = [NSKeyedUnarchiver unarchiveObjectWithData:aData];
            [localArray1 addObject:newStr];
        }
       // NSLog(@"%@",localArray1);
    } else {
        NSLog(@"no elements");
    }
    [self getCmsData];
}

-(void)getCmsData{
    [[PlayTripManager Instance]getCmsDataWithSearchString:@"" WithLattitude:23.123 WithLongitude:72.123 WithBlock:^(id result, NSString *error) {
        if(!error){
            NSMutableArray * localArray = [SearchData getAll];
           // NSLog(@"%@",localArray);
        }else{
            NSLog(@"%@",error);
        }
    }];
}

-(void)checkLocalVideoObjects {
    NSMutableArray * localArray =  [LocalVideoObject getAll];
    for (LocalVideoObject * vidObj in localArray) {
        if (vidObj.isUploading.length > 0) {
            vidObj.isUploading = @"";
            [LocalVideoObject saveEntity];
        }
    }
    if (localArray.count > 0) {
        UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"There are some videos saved in local. You want to upload now?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            VideoToBeUploadedViewController * controller = [VideoToBeUploadedViewController initViewController];
            [self.navigationController pushViewController:controller animated:YES];
        }];
        UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alert addAction:yesAction];
        [alert addAction:noAction];
        [self.navigationController presentViewController:alert animated:YES completion:nil];
    }
    [self checkLocalTags];
}


-(void)checkLocalTags {
    NSMutableArray * localArray =  [LocalAttrTags getAll];
    for (LocalAttrTags * tag in localArray) {
        [[PlayTripManager Instance] tagsAddEditWithName:tag.tagName andAddress:nil andPlaceId:nil andLat:nil andLng:nil WithBlock:nil];
    }
    [LocalAttrTags MR_truncateAll];
}

-(void)getAllAttrData {
    [[PlayTripManager Instance]getAllAttrDataWithBlock:^(id result, NSString *error) {
        if(!error){
            NSMutableArray * localArray = [AttractionData getAll];
            //NSLog(@"%@",localArray);
        }else{
            NSLog(@"%@",error);
        }
        [self checkLocalTags];
    }];
}

- (IBAction)shareButton:(UIBarButtonItem *)sender {
    NSString *textToShare = @"Play Trip!";
    NSURL *myWebsite = [NSURL URLWithString:@"http://www.playtrip.com/"];
    NSURL *androidLink = [NSURL URLWithString:@"https://play.google.com/store/apps/details?id=com.appone.playtrip&hl=en"];
    NSURL *appLink = [NSURL URLWithString:@"https://itunes.apple.com/us/app/playtrip/id1223364669?ls=1&mt=8"];
    
    NSArray *objectsToShare = @[textToShare, myWebsite, androidLink, appLink];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
   activityVC.excludedActivityTypes = excludeActivities;
    
    [self.view.window.rootViewController presentViewController:activityVC animated:YES completion:nil];
}

-(void)homeClicked {
    [self.navigationController popViewControllerAnimated:true];
}

-(void) advancedSearchClicked {
    SearchTabViewController * controller = [SearchTabViewController initViewController];
    [self.navigationController pushViewController:controller animated:true];
}

#pragma mark -<UICollectionViewDelegate,UICollectionViewDataSource>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == collDistrictView) {

        if(uniqueCountries.count <= 0){
            lblDistrictData.hidden = NO;
            collDistrictView.hidden = YES;
            return 0;
        }else{
            lblDistrictData.hidden = YES;
            collDistrictView.hidden = NO;
            return uniqueCountries.count;
        }
        /*
        
        if(arrDistricts.count <= 0){
            lblDistrictData.hidden = NO;
            collDistrictView.hidden = YES;
            return 0;
        }else{
            lblDistrictData.hidden = YES;
            collDistrictView.hidden = NO;
            return arrDistricts.count;
        }
         */
        
    } else if (collectionView == collLatestView) {
        
        if (!isAttractionPickerSelected) {
            if(allLatestPlans.count <= 0){
                lblNoLatest.hidden = NO;
                collLatestView.hidden = YES;
                return 0;
            }else{
                collLatestView.hidden = NO;
                lblNoLatest.hidden = YES;
                return allLatestPlans.count;
            }
        }
        else{
            if(allMostPopularAttractions.count <= 0){
                lblNoLatest.hidden = NO;
                collLatestView.hidden = YES;
                return 0;
            }else{
                collLatestView.hidden = NO;
                lblNoLatest.hidden = YES;
                return allMostPopularAttractions.count;
            }
        }
        
        
        
    } else if (collectionView == collMostPopularVideoTour) {
        if(allLatestPlans.count <= 0){
            lblNoPopularTour.hidden = NO;
            collMostPopularVideoTour.hidden = YES;
            return 0;
        }else{
            collMostPopularVideoTour.hidden = NO;
            lblNoPopularTour.hidden = YES;
            return allLatestPlans.count;
        }
        
    }else if (collectionView == collAttrView) {
        if(arrMostPopularAttractions.count <= 0){
            lblNoPopularAttr.hidden = NO;
            collAttrView.hidden = YES;
            return 0;
        }else{
            collAttrView.hidden = NO;
            lblNoPopularAttr.hidden = YES;
            return arrMostPopularAttractions.count;
        }
    }else  {
        if(allRecomendedUsers.count <= 0){
            lblUserData.hidden = NO;
            collRecommendedView.hidden = YES;
            return 0;
        }else{
            collRecommendedView.hidden = NO;
            lblUserData.hidden = YES;
            return allRecomendedUsers.count;
        }
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    Account * account = [AccountManager Instance].activeAccount;
    
    if (collectionView == collDistrictView) {
        DistrictCollectionCell *cell = (DistrictCollectionCell*)[collDistrictView dequeueReusableCellWithReuseIdentifier:@"DistrictCollectionCell" forIndexPath:indexPath];
        
        LocationModel *location = uniqueCountries[indexPath.row];
        cell.lblName.text = location.country_data.name;

        cell.lblCount.text = [NSString stringWithFormat:@"(%@)",location.count];
        cell.imgMain.image = [UIImage imageNamed:@"default_country_image"];

        if (location.country_data.img_url) {
            [cell.imgMain sd_setImageWithURL:[NSURL URLWithString:location.country_data.img_url]
                            placeholderImage:[UIImage imageNamed:@"default_country_image"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                                if (!error) {
                                    cell.imgMain.image = image;
                                }
                                else{
                                    cell.imgMain.image = [UIImage imageNamed:@"default_country_image"];
                                }
                                [cell layoutIfNeeded];
                            }];
        }

        if([account.language  isEqualToString: @"sc"]){
            cell.lblName.text = location.country_data.name_sc;
        }else if([account.language  isEqualToString: @"tc"]){
            cell.lblName.text = location.country_data.name_tc;
        }
        
        

        /*
        District * dist = [arrDistricts objectAtIndex:indexPath.row];
        cell.lblName.text = dist.name;
        cell.lblCount.hidden = YES;
        NSLog(@"%@", dist.plan_cnt);
        
        if(![dist.image isEqualToString:@""]){
            NSString * imgId = dist.image;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            cell.imgMain.url = [NSURL URLWithString:urlString];
        }else{
            cell.imgMain.image = [UIImage imageNamed:@"def_city_img.png"];
        }
         */
        return cell;
    }
    else if (collectionView == collLatestView) {
        LatestVideoTourCollectionViewCell *cell = (LatestVideoTourCollectionViewCell*)[collLatestView dequeueReusableCellWithReuseIdentifier:@"LatestVideoTourCollectionViewCell" forIndexPath:indexPath];
        cell.lblTime.text = @"00:00";

        
        
        
        
        
        if (!isAttractionPickerSelected) {
            if (indexPath.row >= allLatestPlans.count) {
                return cell;
            }
            PlanModel *plan = [allLatestPlans objectAtIndex:indexPath.row];
            cell.lblUserName.text = plan.user.full_name;
            cell.imgUserImg.image = [UIImage imageNamed:@"avatarPlacHolder"];
            if (plan.duration) {
                
                int durationInt = [plan.duration intValue];
                
                cell.lblTime.text = [NSString stringWithFormat:@"%@",[self formatTimeFromSeconds:durationInt]];
            }
            
            NSString * imgId = plan.user.avatar;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            
            
            [cell.imgUserImg sd_setShowActivityIndicatorView:YES];
            [cell.imgUserImg sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            [cell.imgUserImg sd_setImageWithURL:[NSURL URLWithString:urlString]
                               placeholderImage:[UIImage imageNamed:@"avatarPlacHolder"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                                   if (!error) {
                                       cell.imgUserImg.image = image;
                                       [cell layoutIfNeeded];
                                   }
                                   else{
                                       cell.imgUserImg.url = [NSURL URLWithString:urlString];
                                   }
                               }];
            cell.lblFromDate.text = ([plan.create_date length]>10 ? [plan.create_date substringToIndex:10] : plan.create_date);
            cell.imgThumbnail.image = [UIImage imageNamed:@"def_video_img.png"];
            
            if(![plan.cover_photo isEqualToString:@""]){
                NSString * imgId = plan.cover_photo;
                NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
                
                [cell.imgThumbnail sd_setShowActivityIndicatorView:YES];
                [cell.imgThumbnail sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
                [cell.imgThumbnail sd_setImageWithURL:[NSURL URLWithString:urlString]
                                     placeholderImage:[UIImage imageNamed:@"def_video_img.png"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                                         if (!error) {
                                             cell.imgThumbnail.image = image;
                                             [cell layoutIfNeeded];
                                         }
                                         else{
                                             cell.imgThumbnail.url = [NSURL URLWithString:urlString];
                                         }
                                     }];
                
                //cell.imgThumbnail.url = [NSURL URLWithString:urlString];
            }
            cell.lblLanguage.text = plan.language;
            
            cell.lblLanguage.hidden = YES;
            if([plan.language  isEqualToString: @"sc"]){
                cell.languageImage.image = [UIImage imageNamed:@"speaker_.png"];
            }else if([plan.language  isEqualToString: @"tc"]){
                cell.languageImage.image = [UIImage imageNamed:@"speaker_2.png"];
            }else{
                cell.languageImage.image = [UIImage imageNamed:@"speaker_Eng.png"];
            }
            
            
            cell.lblVideoName.text = plan.name;
            
            cell.lblCountry.hidden = YES;
            cell.lblDistrict.hidden = YES;
            cell.lblCity.hidden = YES;
            
            for (AttractionModel *attraction in plan.attractions) {
                if (attraction.attr_data.count) {
                    AttractionDataModel *attratctionData = [attraction.attr_data objectAtIndex:0];
                    //cell.lblTime.text = attratctionData.time;
                    if (attratctionData.info.country_id) {
                        cell.lblCountry.text = attratctionData.info.country_id.name;
                        cell.lblDistrict.text = attratctionData.info.district_id.name;
                        cell.lblCity.text = attratctionData.info.city_id.name;
                        cell.lblCountry.hidden = NO;
                        cell.lblDistrict.hidden = NO;
                        cell.lblCity.hidden = NO;
                        
                        break;
                    }
                }
            }
            cell.lblBookmarkCount.text = [NSString stringWithFormat:@"%@",plan.total_bookmark];
            cell.lblViewCount.text = [NSString stringWithFormat:@"%@",plan.total_view];
            cell.lblShareCount.text = [NSString stringWithFormat:@"%@",plan.total_share];
            cell.lblRevertCount.text = [NSString stringWithFormat:@"%@",plan.remuneration_amount];
            
            [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
            cell.btnBookMark.selected = NO;
            if (account) {
                for (NSString *user in plan.bookmark_users) {
                    if ([user isEqualToString:account.userId]) {
                        [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                        cell.btnBookMark.selected = YES;
                    }
                }
            }
            cell.btnBookMark.tag = indexPath.row;
            [cell.btnBookMark addTarget:self action:@selector(bookmarkClickedVideoTour:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        }
        else{
            if (indexPath.row >= allMostPopularAttractions.count) {
                return cell;
            }
            AttractionDataModel *plan = [allMostPopularAttractions objectAtIndex:indexPath.row];
            cell.lblUserName.text = plan.info.user.full_name;
            cell.imgUserImg.image = [UIImage imageNamed:@"avatarPlacHolder"];
            if (plan.time) {
                
                int durationInt = [plan.time intValue];
                
                cell.lblTime.text = [NSString stringWithFormat:@"%@",[self formatTimeFromSeconds:durationInt]];
            }
            
            NSString * imgId = plan.info.user.avatar;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            
            
            [cell.imgUserImg sd_setShowActivityIndicatorView:YES];
            [cell.imgUserImg sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            [cell.imgUserImg sd_setImageWithURL:[NSURL URLWithString:urlString]
                               placeholderImage:[UIImage imageNamed:@"avatarPlacHolder"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                                   if (!error) {
                                       cell.imgUserImg.image = image;
                                       [cell layoutIfNeeded];
                                   }
                                   else{
                                       cell.imgUserImg.url = [NSURL URLWithString:urlString];
                                   }
                               }];
            cell.lblFromDate.text = ([plan.create_date length]>10 ? [plan.create_date substringToIndex:10] : plan.create_date);
            cell.imgThumbnail.image = [UIImage imageNamed:@"def_video_img.png"];
            
            if(![plan.info.cover_photo_url isEqualToString:@""]){
                NSString * imgId = plan.info.cover_photo_url;
                NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
                
                [cell.imgThumbnail sd_setShowActivityIndicatorView:YES];
                [cell.imgThumbnail sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
                [cell.imgThumbnail sd_setImageWithURL:[NSURL URLWithString:urlString]
                                     placeholderImage:[UIImage imageNamed:@"def_video_img.png"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                                         if (!error) {
                                             cell.imgThumbnail.image = image;
                                             [cell layoutIfNeeded];
                                         }
                                         else{
                                             cell.imgThumbnail.url = [NSURL URLWithString:urlString];
                                         }
                                     }];
                
                //cell.imgThumbnail.url = [NSURL URLWithString:urlString];
            }

            
            cell.lblLanguage.hidden = YES;
          
            
            
            cell.lblVideoName.text = plan.info.name;
            
            cell.lblCountry.hidden = YES;
            cell.lblDistrict.hidden = YES;
            cell.lblCity.hidden = YES;
            
           
            cell.lblBookmarkCount.text = [NSString stringWithFormat:@"%@",plan.total_bookmark];
            cell.lblViewCount.text = [NSString stringWithFormat:@"%@",plan.total_view];
            cell.lblShareCount.text = [NSString stringWithFormat:@"%@",plan.total_share];
            cell.lblRevertCount.hidden = YES;
            
            [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
            cell.btnBookMark.selected = NO;
            if (account) {
                for (NSString *user in plan.bookmark_users) {
                    if ([user isEqualToString:account.userId]) {
                        [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                        cell.btnBookMark.selected = YES;
                    }
                }
            }
            cell.btnBookMark.tag = indexPath.row;
            [cell.btnBookMark addTarget:self action:@selector(bookmarkClickedVideoTour:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        }
        
        
        
    }
    else if (collectionView == collMostPopularVideoTour) {
        LatestVideoTourCollectionViewCell *cell = (LatestVideoTourCollectionViewCell*)[collMostPopularVideoTour dequeueReusableCellWithReuseIdentifier:@"LatestVideoTourCollectionViewCell" forIndexPath:indexPath];
        if (indexPath.row >= arrMostPopularVideoTour.count) {
            return cell;
        }
        
        MostPopularVideoTour * v = [arrMostPopularVideoTour objectAtIndex:indexPath.row];
        NSMutableArray *attraction = [NSMutableArray new];
        NSMutableArray *attractionData = [NSMutableArray new];
        Attractions *a;
        AttractionData *aData;
        attraction = [[v.attractionsSet allObjects] mutableCopy];
        if(attraction.count>0){
            a =[attraction objectAtIndex:0];
        }
        
        if (account) {
            if ([v.bookmark_users_string containsString:account.userId]) {
                [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                cell.btnBookMark.selected = YES;
            } else {
                [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
                cell.btnBookMark.selected = NO;
            }
        } else {
            [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
            cell.btnBookMark.selected = NO;
        }
        
        if(attractionData.count>0){
            aData = [attractionData objectAtIndex:0];
            cell.lblDescription.text = [NSString stringWithFormat:@"[%@ days] %@",v.days, aData.info.address];
        } else {
            cell.lblDescription.text = [NSString stringWithFormat:@"[%@ days]",v.days];
        }
        
        cell.lblFromDate.text = [Utils dateFormatyyyyMMdd:v.from_date];
        cell.lblVideoName.text = v.name;
        cell.lblUserName.text = v.user1.full_name;
        
        if(![v.user1.avatar isEqualToString:@""]){
            NSString * imgId = v.user1.avatar;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            cell.imgUserImg.url = [NSURL URLWithString:urlString];
            cell.imgUserImg.layer.cornerRadius = cell.imgUserImg.frame.size.width / 2;
            cell.imgUserImg.layer.masksToBounds = YES;
        }else{
            cell.imgUserImg.backgroundColor = [UIColor blueColor];
            cell.imgUserImg.layer.cornerRadius = cell.imgUserImg.frame.size.width / 2;
            cell.imgUserImg.layer.masksToBounds = YES;
            
        }
        if(![v.thumbnail isEqualToString:@""]){
            NSString * imgId = v.thumbnail;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            cell.imgThumbnail.url = [NSURL URLWithString:urlString];
        }else{
            //cell.imgThumbnail.image = [UIImage imageNamed:@"mexico.jpg"];
        }
        
        
        cell.btnBookMark.tag = indexPath.row;
        [cell.btnBookMark addTarget:self action:@selector(bookmarkClickedVideoTour:) forControlEvents:UIControlEventTouchUpInside];
        cell.lblBookmarkCount.text = [NSString stringWithFormat:@"%@",v.total_bookmark];
        cell.lblViewCount.text = [NSString stringWithFormat:@"%@",v.total_view];
        cell.lblRevertCount.text = [NSString stringWithFormat:@"%@",v.remuneration_amount];
        cell.lblShareCount.text = [NSString stringWithFormat:@"%@",v.total_share];
        return cell;
    }
    else if (collectionView == collAttrView) {
        AttractionsVideoCell *cell = (AttractionsVideoCell*)[collAttrView dequeueReusableCellWithReuseIdentifier:@"AttractionsVideoCell" forIndexPath:indexPath];
        MostPopularAttractions * att = [arrMostPopularAttractions objectAtIndex:indexPath.row];
        if (account) {
            if ([att.bookmark_users_string containsString:account.userId]) {
                [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                cell.btnBookMark.selected = YES;
            } else {
                [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
                cell.btnBookMark.selected = NO;
            }
        } else {
            [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
            cell.btnBookMark.selected = NO;
        }        
        cell.lblFromDate.text = [Utils dateFormatyyyyMMdd:att.create_date];
        cell.lblVideoName.text = att.name;
        cell.lblUserName.text = att.userMostPopularAttractionData.full_name;
        
        if(![att.userMostPopularAttractionData.avatar isEqualToString:@""]){
            NSString * imgId = att.userMostPopularAttractionData.avatar;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            cell.imgUserImg.url = [NSURL URLWithString:urlString];
            cell.imgUserImg.layer.cornerRadius = cell.imgUserImg.frame.size.width / 2;
            cell.imgUserImg.layer.masksToBounds = YES;
        }else{
            cell.imgUserImg.backgroundColor = [UIColor blueColor];
            cell.imgUserImg.layer.cornerRadius = cell.imgUserImg.frame.size.width / 2;
            cell.imgUserImg.layer.masksToBounds = YES;
            
        }
        //        if(![att.thumbnail isEqualToString:@""]){
        //            NSString * imgId = v.thumbnail;
        //            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
        //            cell.imgThumbnail.url = [NSURL URLWithString:urlString];
        //        }else{
        //            //            cell.imgThumbnail.image = [UIImage imageNamed:@"mexico.jpg"];
        //        }
        
        
        cell.btnBookMark.tag = indexPath.row;
        [cell.btnBookMark addTarget:self action:@selector(bookmarkClickedMostPopularAttraction:) forControlEvents:UIControlEventTouchUpInside];
        cell.lblTTotalBookMark.text = [NSString stringWithFormat:@"%@",att.total_bookmark];
        cell.lblTTotalView.text = [NSString stringWithFormat:@"%@",att.total_view];
        cell.lblTTotalShare.text = [NSString stringWithFormat:@"%@",att.total_share];
    
        return cell;
    }
    else {
        UserCollectionViewCell *cell = (UserCollectionViewCell*)[collRecommendedView dequeueReusableCellWithReuseIdentifier:@"UserCollectionViewCell" forIndexPath:indexPath];
        
        
        UserModel *user = allRecomendedUsers[indexPath.row];
        if(user.avatar.length > 0){
            cell.imgUser.layer.cornerRadius = cell.imgUser.frame.size.width / 2;
            NSString * imgId = user.avatar;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            cell.imgUser.url = [NSURL URLWithString:urlString];
        }else{
            cell.imgUser.image = [UIImage imageNamed:@"avatarPlacHolder"];
        }
        cell.lblName.text = user.full_name;
        cell.lblPlay.text = [NSString stringWithFormat:@"%@",user.total_share];
        
        if (user.credits) {
            cell.lblMoney.text =[NSString stringWithFormat:@"%@",user.credits];;
        }
        else{
            cell.lblMoney.text =[NSString stringWithFormat:@"%@",@"0"];;
        }
        if (user.total_bookmark) {
            cell.lblBookMark.text = [NSString stringWithFormat:@"%@",user.total_bookmark];
        }
        else{
            cell.lblBookMark.text = [NSString stringWithFormat:@"%@",@"0"];
        }
        if (user.total_view) {
            cell.lblViews.text = [NSString stringWithFormat:@"%@",user.total_view];
        }
        else{
            cell.lblViews.text = [NSString stringWithFormat:@"%@",@"0"];
        }
        if (user.total_share) {
            cell.lblShares.text = [NSString stringWithFormat:@"%@",user.total_share];
        }
        else{
            cell.lblShares.text = [NSString stringWithFormat:@"%@",@"0"];
        }
        cell.btnBookmark.tag = indexPath.row;
        
        [cell.btnBookmark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
        cell.btnBookmark.selected = NO;
        if (account) {
            for (NSString *userBookmarked in user.bookmark_users) {
                if ([userBookmarked isEqualToString:account.userId]) {
                    [cell.btnBookmark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                    cell.btnBookmark.selected = YES;
                }
            }
        }
        
        

        [cell.btnBookmark addTarget:self action:@selector(bookmarkClickedUser:) forControlEvents:UIControlEventTouchUpInside];
        
        
      
        return cell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (collectionView == collDistrictView) {
        if (indexPath.row < uniqueCountries.count) {
            LocationModel *location = uniqueCountries[indexPath.row];
            DistrictDetailViewController * controller = [DistrictDetailViewController initViewControllerWithLocation:location];
            [self.navigationController pushViewController:controller animated:true];
        }
        
        
        
    }
    else if (collectionView == collLatestView) {
        if (!isAttractionPickerSelected) {
            if (indexPath.row < allLatestPlans.count) {
                PlanModel *plan = [allLatestPlans objectAtIndex:indexPath.row];
                //VideoTour *v = [arrVideoTour objectAtIndex:indexPath.row];
                ItineraryViewController * controller = [ItineraryViewController initViewControllerWithPlan:plan];
                [self.navigationController  pushViewController:controller animated:YES];
            }
        }
        else{
            if (indexPath.row < allMostPopularAttractions.count) {
                
                AttractionDataModel *dataModel = allMostPopularAttractions[indexPath.row];
                NSString *imgId = dataModel.video_id.file_name;
                if (imgId) {
                    NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,@"/uploads/",imgId];
                    NSLog(@"%@",urlString);
                    [self playVideoInPopUpViewWithURL:[NSURL URLWithString:urlString]];
                }
            }
        }
        
        
    }
    else if (collectionView == collMostPopularVideoTour) {
        [Utils showAlertWithMessage:@"Under Construction"];
        return;
        
        MostPopularVideoTour *v = [arrMostPopularVideoTour objectAtIndex:indexPath.row];
        ItineraryViewController * controller = [ItineraryViewController initController:v];
        [self.navigationController  pushViewController:controller animated:false];
        
    }
    else if (collectionView == collRecommendedView) {
       //[Utils showAlertWithMessage:@"Under Construction"];
        if (indexPath.row < allRecomendedUsers.count) {
            UserModel *userModel = allRecomendedUsers[indexPath.row];
            UserDetailViewController * controller = [UserDetailViewController initViewControllerNew:userModel];
            [self.navigationController pushViewController:controller animated:YES];
            return;
        }
        
        
    }
    else if (collectionView == collAttrView) {
        [Utils showAlertWithMessage:@"Under Construction"];
        return;
        
        MostPopularAttractions * att = [arrMostPopularAttractions objectAtIndex:indexPath.row];
        PlaceInformationViewController  * controller = [PlaceInformationViewController initController:att];
        [self.navigationController pushViewController:controller animated:false];
    }
}
-(void)bookmarkClickedMostPopularAttraction:(UIButton *)sender {
    MostPopularAttractions * att = [arrMostPopularAttractions objectAtIndex:[sender tag]];
    [self commanApiWithModel:ModelAttraction WithId:att.entity_id WithType:TypeBookmark];
}
-(void)bookmarkClickedVideoTour:(UIButton *)sender {
    
    PlanModel *plan = [allLatestPlans objectAtIndex:[sender tag]];

    
    //VideoTour * v = [arrVideoTour objectAtIndex:[sender tag]];
    [self commanApiWithModel:ModelPlan WithId:plan._id WithType:TypeBookmark];
}
-(void)bookmarkClickedUser:(UIButton *)sender {
    UserModel *user = allRecomendedUsers[[sender tag]];
    //Users * v = [arrUsers objectAtIndex:[sender tag]];
    [self commanApiWithModel:ModelUser WithId:user._id WithType:TypeBookmark];
}
-(void)bookmarkClickedMostPopularPlan:(UIButton *)sender {
    //MostPopularPlan * v = [arrMostPopularPlan objectAtIndex:[sender tag]];
    PlanModel *plan = [allLatestPlans objectAtIndex:[sender tag]];
    [self commanApiWithModel:ModelPlan WithId:plan._id WithType:TypeBookmark];
}

-(void)bookmarkClickedMostPopularVideoTour:(UIButton *)sender {
    PlanModel *plan = [allLatestPlans objectAtIndex:[sender tag]];
    //MostPopularVideoTour * v = [arrMostPopularVideoTour objectAtIndex:[sender tag]];
    [self commanApiWithModel:ModelPlan WithId:plan._id WithType:TypeBookmark];
}

- (void)updateSearchResultsForSearchController:(UISearchController *)searchController {
}

- (IBAction)districtMoreClicked:(id)sender {
    DistrictViewController * controller = [DistrictViewController initViewController];
    controller.uniqueCountries = uniqueCountries;
    //controller.allCountriesDicts = allCountriesDicts;
   
    
    
    [self.navigationController pushViewController:controller animated:true];
}

- (IBAction)latestVideoMoreClicked:(id)sender {
    LatestVideoTourViewController * controller = [LatestVideoTourViewController initViewController];
    controller.allLatestPlans = allLatestPlans;
    if (lblTitleLatest.tag == 0) {
        controller.title = @"Latest Video Tour";
    }
    else if(lblTitleLatest.tag == 1){
        controller.title = @"Most Popular Video Tour";
    }
    else{
        controller.title = @"Most Popular Video Tour By Attraction";
    }
    [self.navigationController  pushViewController:controller animated:true];
}

- (IBAction)popularTourMoreClicked:(id)sender {
    MostPopularVideoTourViewController * controller = [MostPopularVideoTourViewController initViewController];
    [self.navigationController  pushViewController:controller animated:true];
}

- (IBAction)popularAttrMoreClicked:(id)sender {
    MostPopularAttractionsVideoViewController * controller = [MostPopularAttractionsVideoViewController initViewController];
    [self.navigationController  pushViewController:controller animated:true];
}

- (IBAction)recommendedUsersMoreClicked:(id)sender {
    UserTabViewController * controller = [UserTabViewController initViewController];
    controller.allRecomendedUsers = allRecomendedUsers;
    [self.navigationController pushViewController:controller animated:true];
}

- (IBAction)sortClicked:(id)sender {
    NSMutableArray * strArray = [[NSMutableArray alloc] init];
    
    NSString *str1 = LOCALIZATION(@"lastest_video_tour");
    
    
    //[NSString stringWithFormat:NSLocalizedString(@"lastest_video_tour", nil)];
    NSString *str2 = LOCALIZATION(@"mostpopular_video_tour");
    
    //[NSString stringWithFormat:NSLocalizedString(@"mostpopular_video_tour", nil)];
    NSString *str3 = LOCALIZATION(@"mostpopular_attracttion_video");
    

    [strArray addObject:str1];
    [strArray addObject:str2];
    [strArray addObject:str3];
    
    
    ActionSheetStringPicker* sp = [[ActionSheetStringPicker alloc]initWithTitle:ALERT_TITLE rows:strArray initialSelection:lblTitleLatest.tag doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        selIndex = (int)selectedIndex;
        if (selectedIndex == 0) {
            //[self getLatestVideoTour];
            allLatestPlans = nil;
            isAttractionPickerSelected = NO;
            [self getLatestVideoTourNew];
            lblTitleLatest.text = [NSString stringWithFormat:@"%@",LOCALIZATION(@"lastest_video_tour")];
            lblTitleLatest.tag = 0;
            
            viewLatest.hidden = false;
            viewPopularTour.hidden = true;
            viewPopularAttr.hidden =true;
        } else if (selectedIndex == 1) {
            //[self getMostPopularVideoTour];
            lblTitleLatest.text = LOCALIZATION(@"mostpopular_video_tour");
            lblTitleLatest.tag = 1;
            
            
            allLatestPlans = nil;
            isAttractionPickerSelected = NO;
            [self getMostPopularVideoTourNew];
            //viewLatest.hidden = true;
            //viewPopularTour.hidden = false;
            //viewPopularAttr.hidden =true;
        }else if (selectedIndex == 2) {
            //[self getMostPopularattractions];
            
            lblTitleLatest.text = LOCALIZATION(@"mostpopular_attracttion_video");
            lblTitleLatest.tag = 2;
            
            allLatestPlans = nil;
            isAttractionPickerSelected = YES;
            [self getMostPopularVideoTourByAttractionNew];
            //viewLatest.hidden = true;
            //viewPopularTour.hidden = true;
            //viewPopularAttr.hidden =false;
        }
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        viewLatest.hidden = false;
        viewPopularTour.hidden = true;
        viewPopularAttr.hidden =true;
    } origin:sender];
    UIButton *cancelButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelButton setTitle:LOCALIZATION(@"cancel") forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [cancelButton setFrame:CGRectMake(0, 0, 70, 50)];
    [sp setCancelButton:[[UIBarButtonItem alloc] initWithCustomView:cancelButton]];
    UIButton *doneButton =  [UIButton buttonWithType:UIButtonTypeCustom];
    [doneButton setTitle:LOCALIZATION(@"done") forState:UIControlStateNormal];
    [doneButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [doneButton setFrame:CGRectMake(0, 0, 70, 50)];
    [sp setDoneButton:[[UIBarButtonItem alloc] initWithCustomView:doneButton]];
    [sp showActionSheetPicker];
    
    /*[ActionSheetStringPicker showPickerWithTitle:ALERT_TITLE rows:strArray initialSelection:lblTitleLatest.tag  doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        selIndex = (int)selectedIndex;
        if (selectedIndex == 0) {
            //[self getLatestVideoTour];
            allLatestPlans = nil;
            isAttractionPickerSelected = NO;
            [self getLatestVideoTourNew];
            lblTitleLatest.text = [NSString stringWithFormat:@"%@",LOCALIZATION(@"lastest_video_tour")];
            lblTitleLatest.tag = 0;
            
            viewLatest.hidden = false;
            viewPopularTour.hidden = true;
            viewPopularAttr.hidden =true;
        } else if (selectedIndex == 1) {
            //[self getMostPopularVideoTour];
            lblTitleLatest.text = LOCALIZATION(@"mostpopular_video_tour");
            lblTitleLatest.tag = 1;
            
            
            allLatestPlans = nil;
            isAttractionPickerSelected = NO;
            [self getMostPopularVideoTourNew];
            //viewLatest.hidden = true;
            //viewPopularTour.hidden = false;
            //viewPopularAttr.hidden =true;
        }else if (selectedIndex == 2) {
            //[self getMostPopularattractions];
            
            lblTitleLatest.text = LOCALIZATION(@"mostpopular_attracttion_video");
            lblTitleLatest.tag = 2;
            
            allLatestPlans = nil;
            isAttractionPickerSelected = YES;
            [self getMostPopularVideoTourByAttractionNew];
            //viewLatest.hidden = true;
            //viewPopularTour.hidden = true;
            //viewPopularAttr.hidden =false;
        }
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        viewLatest.hidden = false;
        viewPopularTour.hidden = true;
        viewPopularAttr.hidden =true;
    } origin:sender];*/
}

#pragma Searchbar Delegate

- (void)searchBarTextDidBeginEditing:(UISearchBar *)sBar {
    [sBar setShowsCancelButton:YES animated:YES];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)sBar{
    [sBar resignFirstResponder];
    if (searchBar.text.length) {
        allLatestPlans = [[NSMutableArray alloc] init];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD showWithStatus:@"Searching Attractions..."];
        
        NSDictionary *parameters = @{ @"sort": @{ @"create_date": @-1 },
                                      @"where": @{ @"type": @"videotour",
                                                   @"name": searchBar.text}};
        [[PlayTripManager Instance] getLatestVideoTourWithParameters:parameters WithBlockNew:^(id result, NSString *error) {
            [SVProgressHUD dismiss];
            if (!error) {
                NSError* errorData;
                NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                     options:kNilOptions
                                                                       error:&errorData];
                if (errorData) {
                    NSLog(@"ERROR GOTTEN = %@" ,errorData.description);
                }
                NSArray* plansDicts = [json objectForKey:@"data"];
                allLatestPlans = [[NSMutableArray alloc] init];
                for (NSDictionary *planDict in plansDicts) {
                    NSError *error;
                    PlanModel *planModel = [[PlanModel alloc] initWithDictionary:planDict error:&error];
                    if (error) {
                        NSLog(@"ERROR GOTTEN AT JSONMODEL = %@" ,error.description);
                    }
                    if (![allLatestPlans containsObject:planModel]) {
                        [allLatestPlans addObject:planModel];
                    }
                }
                NSLog(@"Total Plans Gotten = %lu" ,(unsigned long)allLatestPlans.count);
                //allLatestPlans = [[[allLatestPlans reverseObjectEnumerator] allObjects] mutableCopy];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [collLatestView reloadData];
                });
            }
        }];
    }
    else{
        [self getLatestVideoTourNew];
    }
    
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)sBar{
    [sBar resignFirstResponder];
    [sBar setShowsCancelButton:NO animated:YES];
    sBar.text = @"";
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Getting Attractions..."];
    [self getLatestVideoTourNew];
}
- (void)searchBar:(UISearchBar *)sBar textDidChange:(NSString *)searchText {
    /*
    arrDistricts =[[NSMutableArray alloc]init];
    arrVideoTour =[[NSMutableArray alloc]init];
    arrUsers = [[NSMutableArray alloc] init];
    arrMostPopularPlan = [NSMutableArray new];
    arrMostPopularVideoTour = [NSMutableArray new];
    arrMostPopularAttractions = [NSMutableArray new];
    
    
    NSMutableArray * aarDistrict = [District getAll];
    NSMutableArray * aarVideoTour = [VideoTour getAll];
    NSMutableArray * aarUsers = [Users getAll];
    NSMutableArray * aarMostPopularPlan = [MostPopularPlan getAll];
    NSMutableArray * aarMostPopularVideoTour = [MostPopularVideoTour getAll];
    NSMutableArray * aarMostPopularAttraction = [MostPopularAttractions getAll];
    
    if([searchText length] != 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name BEGINSWITH[c] %@", searchText];
        arrDistricts = [[aarDistrict filteredArrayUsingPredicate:predicate]mutableCopy];
        
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"name BEGINSWITH[c] %@", searchText];
        arrVideoTour = [[aarVideoTour filteredArrayUsingPredicate:predicate1]mutableCopy];
        
        NSPredicate *predicate11 = [NSPredicate predicateWithFormat:@"name BEGINSWITH[c] %@", searchText];
        arrMostPopularPlan = [[aarMostPopularPlan filteredArrayUsingPredicate:predicate11]mutableCopy];
        
        NSPredicate *predicate12 = [NSPredicate predicateWithFormat:@"name BEGINSWITH[c] %@", searchText];
        arrMostPopularVideoTour = [[aarMostPopularVideoTour filteredArrayUsingPredicate:predicate12]mutableCopy];
        
        NSPredicate *predicate123 = [NSPredicate predicateWithFormat:@"name BEGINSWITH[c] %@", searchText];
        arrMostPopularAttractions= [[aarMostPopularAttraction filteredArrayUsingPredicate:predicate123]mutableCopy];
        
        NSPredicate *predicateCity = [NSPredicate predicateWithFormat:@"user_name BEGINSWITH[c] %@", searchText];
        arrUsers = [[aarUsers filteredArrayUsingPredicate:predicateCity]mutableCopy];
    } else {
        [sBar resignFirstResponder];
        arrDistricts =[District getAll];
        arrVideoTour = [VideoTour getAll];
        arrUsers = [Users getAll];
        arrMostPopularVideoTour = [MostPopularVideoTour getAll];
        arrMostPopularPlan = [MostPopularPlan getAll];
        arrMostPopularAttractions = [MostPopularAttractions getAll];
        
    }
    
    [collRecommendedView reloadData];
    [collDistrictView reloadData];
    [collLatestView reloadData];
    [collMostPopularVideoTour reloadData];
    [collAttrView reloadData];
     */
}

-(void)viewWillDisappear:(BOOL)animated {
    [self stopVideoPlay];
    [self searchBarCancelButtonClicked:searchBar];
    self.menuView1.hidden = true;
    [SVProgressHUD dismiss];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


-(NSString *)formatTimeFromSeconds:(int)numberOfSeconds
{
    
    int seconds = numberOfSeconds % 60;
    int minutes = (numberOfSeconds / 60) % 60;
    
    if (minutes < 10) {
        return [NSString stringWithFormat:@"0%d:%02d", minutes, seconds];
    }
    return [NSString stringWithFormat:@"%d:%02d", minutes, seconds];

}

-(void)accountAuthenticatedWithAccount:(Account*) account{
    [AccountManager Instance].activeAccount = account;

    if ([account.language isEqualToString:@"tc"]) {
        if (![[Localisator sharedInstance].currentLanguage isEqualToString:self.arrayOfLanguages[0]]) {
            [[Localisator sharedInstance] setLanguage:self.arrayOfLanguages[0]];
        }
    }
    else if([account.language isEqualToString:@"sc"]) {
        if (![[Localisator sharedInstance].currentLanguage isEqualToString:self.arrayOfLanguages[1]]) {
            [[Localisator sharedInstance] setLanguage:self.arrayOfLanguages[1]];
        }
    }
    else{
        if (![[Localisator sharedInstance].currentLanguage isEqualToString:self.arrayOfLanguages[2]]) {
            [[Localisator sharedInstance] setLanguage:self.arrayOfLanguages[2]];
        }
    }
    [self setLocalizedStrings];
    
    
}

-(void)accountDidFailAuthentication:(NSString*) error{
    
    
    
}

-(void)playVideoInPopUpViewWithURL:(NSURL *)url {
    if (url) {
        self.videoController = [[MPMoviePlayerController alloc] init];
        [self.videoController setContentURL:url];
        [self.videoController.view setFrame:CGRectMake (0, 0, popUpView.frame.size.width, popUpView.frame.size.height)];
        self.videoController.controlStyle = MPMovieControlStyleNone;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoPlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:self.videoController];
        [popUpView addSubview:self.videoController.view];
        [KGModalWrapper showWithContentView:popUpView];
        [self.videoController setControlStyle:MPMovieControlStyleNone];
        [self.videoController play];
    }
}
- (void)videoPlayBackDidFinish:(NSNotification *)notification {
    
    [self.videoController stop];
    [self.videoController.view removeFromSuperview];
    self.videoController = nil;
    [KGModalWrapper hideView];
}

-(void)stopVideoPlay{
    [self.videoController stop];
}

@end

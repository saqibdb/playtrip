
#import <UIKit/UIKit.h>

@interface TutorialTopViewController : UIViewController

+(TutorialTopViewController *)initViewConrtoller;

@property (assign, nonatomic) NSInteger index;
@property (strong, nonatomic) IBOutlet UIButton *btnGo;

- (IBAction)btnGoClicked:(id)sender;

@property (strong, nonatomic) IBOutlet UIImageView *imgMain;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControlMain;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblDescription;

@end

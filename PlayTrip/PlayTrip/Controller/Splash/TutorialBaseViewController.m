
#import "TutorialBaseViewController.h"
#import "TutorialTopViewController.h"
#import "LanguageSelectionViewController.h"

@interface TutorialBaseViewController ()
@end

@implementation TutorialBaseViewController

+(TutorialBaseViewController *)initViewConrtoller {
    TutorialBaseViewController * controller = [[TutorialBaseViewController alloc] initWithNibName:@"TutorialBaseViewController" bundle:nil];
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = true;
    
    self.pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
    self.pageController.view.bounds = self.view.bounds;
    
    self.pageController.dataSource = self;
//    self.pageController.
//    self.pageController.view.frame = [UIScreen mainScreen].bounds;
    
    TutorialTopViewController *initialViewController = [self viewControllerAtIndex:0];
    

    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    
    NSArray *subviews = self.pageController.view.subviews;
    UIPageControl *thisControl = nil;
    for (int i=0; i<[subviews count]; i++) {
        if ([[subviews objectAtIndex:i] isKindOfClass:[UIPageControl class]]) {
            thisControl = (UIPageControl *)[subviews objectAtIndex:i];
        }
    }
    
    thisControl.hidden = true;
    self.pageController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height+40);

    
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    [self addChildViewController:self.pageController];
    [[self view] addSubview:[self.pageController view]];
    [self.pageController didMoveToParentViewController:self];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissView) name:@"DismissTutorial" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showLanguageView) name:@"ShowLanguageView" object:nil];
}

-(void)dismissView {
    [self.navigationController dismissViewControllerAnimated:true completion:nil];
    [self.navigationController popViewControllerAnimated:true];
}

-(void)showLanguageView {
    LanguageSelectionViewController * controller = [LanguageSelectionViewController initViewController];
//    UINavigationController * navController = [[UINavigationController alloc] initWithRootViewController:controller];
    
//    [self.navigationController presentViewController:navController animated:true completion:nil];
    [self.navigationController pushViewController:controller animated:true];
}

- (TutorialTopViewController *)viewControllerAtIndex:(NSUInteger)index {
    TutorialTopViewController *childViewController = [TutorialTopViewController initViewConrtoller];
    childViewController.index = index;
    return childViewController;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSUInteger index = [(TutorialTopViewController *)viewController index];
    
    if (index == 0) {
        return nil;
    }
    
    index--;
    
    return [self viewControllerAtIndex:index];
    
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    NSUInteger index = [(TutorialTopViewController *)viewController index];
    index++;
    if (index == 4) {
        return nil;
    }
    return [self viewControllerAtIndex:index];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController {
    return 4;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController {
    return 0;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

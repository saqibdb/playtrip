
#import "TutorialTopViewController.h"
#import "ColorConstants.h"
#import "Localisator.h"

@interface TutorialTopViewController ()

@end

@implementation TutorialTopViewController

+(TutorialTopViewController *)initViewConrtoller {
    TutorialTopViewController * controller = [[TutorialTopViewController alloc] initWithNibName:@"TutorialTopViewController" bundle:nil];
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pageControlMain.numberOfPages = 4;
    self.pageControlMain.tintColor = [ColorConstants appYellowColor];
    
    self.pageControlMain.currentPage = self.index;
    
    if (self.index == 3) {
        _btnGo.hidden = false;
    } else {
        _btnGo.hidden = true;
    }
    
    if (self.index == 0) {
        self.imgMain.image = [UIImage imageNamed:@"splash_1.png"];
        [self.lblTitle setText:LOCALIZATION(@"customizable_itinerary")];
        [self.lblDescription setText:LOCALIZATION(@"refer_to_user")];
    } else if (self.index == 1) {
        self.imgMain.image = [UIImage imageNamed:@"splash_2.png"];
        [self.lblTitle setText:LOCALIZATION(@"connect_with_booking_platforms")];
        [self.lblDescription setText:LOCALIZATION(@"book_with_popular")];
    } else if (self.index == 2) {
        self.imgMain.image = [UIImage imageNamed:@"splash_3.png"];
        [self.lblTitle setText:LOCALIZATION(@"film_your_own")];
        [self.lblDescription setText:LOCALIZATION(@"share_your_travel")];
    } else if (self.index == 3) {
        self.imgMain.image = [UIImage imageNamed:@"splash_4.png"];
        [self.lblTitle setText:LOCALIZATION(@"exclusive_financial_rewards")];
        [self.lblDescription setText:LOCALIZATION(@"receive_financial_rewards")];
    }
    
    _btnGo.layer.cornerRadius = _btnGo.layer.frame.size.height / 2;
}

/*
 "refer_to_user" = "Refer to user's existing itinerary and readjust if needed.";
 "book_with_popular" = "Book with popular travel websites! Get everything beforehand!";
 "share_your_travel" = "Share your travel memories with others through our instant video taking platform.";
 "receive_financial_rewards" = "Receive financial rewards upon videos being quoted!";

 */

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];    
    self.navigationController.navigationBarHidden = true;
    [_btnGo setTitle:LOCALIZATION(@"go") forState:UIControlStateNormal];
}

- (IBAction)btnGoClicked:(id)sender {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowLanguageView" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

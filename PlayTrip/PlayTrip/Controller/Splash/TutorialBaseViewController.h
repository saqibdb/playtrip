
#import <UIKit/UIKit.h>

@interface TutorialBaseViewController : UIViewController <UIPageViewControllerDataSource>

+(TutorialBaseViewController *)initViewConrtoller;

@property (strong, nonatomic) UIPageViewController *pageController;

@end

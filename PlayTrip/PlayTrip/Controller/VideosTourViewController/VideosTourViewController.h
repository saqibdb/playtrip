
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "MenuButtonViewController.h"

@interface VideosTourViewController : MenuButtonViewController <UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate> {
    
    IBOutlet UICollectionView *collViewMain;
    IBOutlet UICollectionView *collViewTags;
    
    NSMutableArray * videoTourArray;
    int videoTourCount;
    
    NSString * searchText;
    NSString * sDate;
    NSString * eDate;
    BOOL selfDrive;
    NSMutableArray * arrLanguage;
    BOOL isShowTemp;
}

@property (nonatomic) UISearchBar *bar;

+(VideosTourViewController *)initViewControllerWithSearchText:(NSString *)searchText andLanguageArray:(NSMutableArray *)langArray andStartDate:(NSString *)sDate andEndDate:(NSString *)eDate withSelfDrive:(BOOL)selfDrive;

@end

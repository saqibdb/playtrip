
#import "VideosTourViewController.h"
#import "PlayTripManager.h"
#import "LatestVideoTourCollectionViewCell.h"
#import "TagsCollectionCell.h"
#import "VideoTour.h"
#import "Utils.h"
#import "AttractionData.h"
#import "Account.h"
#import "AccountManager.h"
#import "PlayTripManager.h"
#import "CommonUser.h"
#import "URLSchema.h"
#import "SVProgressHUD.h"
#import "Attractions.h"
#import "ColorConstants.h"
#import "ItineraryViewController.h"
#import "Info.h"

#define langChi  @"ch"
#define langMnd  @"md"
#define langEng  @"en"

@interface VideosTourViewController ()

@end

@implementation VideosTourViewController

+(VideosTourViewController *)initViewControllerWithSearchText:(NSString *)searchText andLanguageArray:(NSMutableArray *)langArray andStartDate:(NSString *)sDate andEndDate:(NSString *)eDate withSelfDrive:(BOOL)selfDrive {
    
    VideosTourViewController * controller = [[VideosTourViewController alloc] initWithNibName:@"VideosTourViewController" bundle:nil];
    controller.title = @"Video Tour";
    controller->arrLanguage = langArray;
    controller->sDate = sDate;
    controller->eDate = eDate;
    controller->searchText = searchText;
    controller->selfDrive = selfDrive;
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    videoTourArray = [NSMutableArray new];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self registerNibs];
    [self setNavBarItems];
    videoTourArray = [VideoTour getAll];
    [collViewMain reloadData];
    self.bar.showsCancelButton = YES;
    self.bar.delegate = self;
}

-(void)viewDidAppear:(BOOL)animated {
    self.leftButton.frame = CGRectMake(0.0, self.view.frame.size.height - 50, 50.0, 50.0);
    self.rightButton.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 50, self.view.frame.size.height - 50, 50.0, 50.0);
    
    CGRect mf = self.view.frame;
    self.menuView1.frame = CGRectMake(mf.size.width-250,mf.size.height - 110, self.menuView1.frame.size.width, self.menuView1.frame.size.height);
}

-(void)registerNibs {
    
    // Arun :: registering collection view cells
    
    [collViewMain registerClass:[LatestVideoTourCollectionViewCell class] forCellWithReuseIdentifier:@"LatestVideoTourCollectionViewCell"];
    [collViewMain registerNib:[UINib nibWithNibName:@"LatestVideoTourCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"LatestVideoTourCollectionViewCell"];
    
    [collViewTags registerClass:[TagsCollectionCell class] forCellWithReuseIdentifier:@"TagsCollectionCell"];
    [collViewTags registerNib:[UINib nibWithNibName:@"TagsCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"TagsCollectionCell"];
}

-(void)setNavBarItems {
    // Arun :: customizing the nav bar icons
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[ColorConstants appBrownColor]}];
    UIImage *buttonImage = [UIImage imageNamed:@"back"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(backClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
    
    UIImage *filterImage = [UIImage imageNamed:@"sort_descending.png"];
    UIButton *btnRight2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight2 setImage:filterImage forState:UIControlStateNormal];
    btnRight2.frame = CGRectMake(0, 0, filterImage.size.width, filterImage.size.height);
    [btnRight2 addTarget:self action:@selector(backClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnRight2];
    
    self.navigationItem.rightBarButtonItem = rightBarItem2;
}

-(void)backClicked {
    [self.navigationController popViewControllerAnimated:true];
}

-(void)bookmarkClickedVideoTour:(id)sender {
    VideoTour * v = [videoTourArray objectAtIndex:[sender tag]];
    [self commanApiWithModel:ModelPlan WithId:v.entity_id WithType:TypeBookmark];
}

-(void)commanApiWithModel:(NSString *)modelValue WithId:(NSString *)idValue WithType:(NSString *)typeValue {
    [[PlayTripManager Instance] addCountForModel:modelValue withType:typeValue forId:idValue WithBlock:^(id result, NSString *error) {
        if(!error){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LatestVideoTourUpdated" object:nil];
        }
    }];
}

#pragma mark -<UICollectionViewDelegate,UICollectionViewDataSource>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (collectionView == collViewMain) {
        return videoTourArray.count;
    } else {
        int total = 0;//end date and search text;
        if (sDate.length > 0 && eDate.length > 0 && eDate.intValue > 0) {
            total++;
        }
        if (searchText.length > 0) {
            total++;
        }
        if (selfDrive) {
            total++;
        }
        total = total + (int)arrLanguage.count;
        return total;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == collViewTags ) {
        TagsCollectionCell *cell = (TagsCollectionCell*)[collViewTags dequeueReusableCellWithReuseIdentifier:@"TagsCollectionCell" forIndexPath:indexPath];
        
        cell.layer.borderWidth=1.0f;
        cell.layer.cornerRadius = cell.frame.size.height / 2;
        cell.layer.borderColor=[UIColor blackColor].CGColor;
        
        cell.btnClose.tag = indexPath.row;
        [cell.btnClose addTarget:self action:@selector(btnCloseClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        if (sDate.length > 0 && eDate.length > 0 && eDate.intValue > 0) {
            if (indexPath.row == 0) {
                NSString * aString = [NSString stringWithFormat:@"%@-%@ days", sDate, eDate];
                cell.lblMain.text = aString;
            } else {
                if (searchText.length > 0) {
                    if (indexPath.row == 1) {
                        cell.lblMain.text = searchText;
                    } else {
                        if (selfDrive) {
                            if (indexPath.row == 2) {
                                cell.lblMain.text = @"Self-Drive";
                            } else {
                                
                                if ([[arrLanguage objectAtIndex:indexPath.row-3] isEqualToString:langChi]) {
                                    cell.lblMain.text = @"Chineese";
                                } else if ([[arrLanguage objectAtIndex:indexPath.row-3] isEqualToString:langMnd]) {
                                    cell.lblMain.text = @"Mandarin";
                                } else{
                                    cell.lblMain.text = @"English";
                                }
                                
                            }
                        } else {
                            cell.lblMain.text = [arrLanguage objectAtIndex:indexPath.row - 2];
                        }
                    }
                } else {
                    if (selfDrive) {
                        if (indexPath.row == 1) {
                            cell.lblMain.text = @"Self-Drive";
                        } else {
                            //                             cell.lblMain.text = [arrLanguage objectAtIndex:indexPath.row - 2];
                            if ([[arrLanguage objectAtIndex:indexPath.row-2] isEqualToString:langChi]) {
                                cell.lblMain.text = @"Chineese";
                            } else if ([[arrLanguage objectAtIndex:indexPath.row-2] isEqualToString:langMnd]) {
                                cell.lblMain.text = @"Mandarin";
                            } else{
                                cell.lblMain.text = @"English";
                            }
                        }
                    } else {
                        //                         cell.lblMain.text = [arrLanguage objectAtIndex:indexPath.row - 1];
                        if ([[arrLanguage objectAtIndex:indexPath.row-1] isEqualToString:langChi]) {
                            cell.lblMain.text = @"Chineese";
                        } else if ([[arrLanguage objectAtIndex:indexPath.row-1] isEqualToString:langMnd]) {
                            cell.lblMain.text = @"Mandarin";
                        } else{
                            cell.lblMain.text = @"English";
                        }
                    }
                }
            }
        } else {
            if (searchText.length > 0) {
                if (indexPath.row == 0) {
                    cell.lblMain.text = searchText;
                } else {
                    if (selfDrive) {
                        if (indexPath.row == 1) {
                            cell.lblMain.text = @"Self-Drive";
                        } else {
                            cell.lblMain.text = [arrLanguage objectAtIndex:indexPath.row - 2];
                        }
                    } else {
                        cell.lblMain.text = [arrLanguage objectAtIndex:indexPath.row - 1];
                    }
                }
            } else {
                if (selfDrive) {
                    if (indexPath.row == 0) {
                        cell.lblMain.text = @"Self-Drive";
                    } else {
                        cell.lblMain.text = [arrLanguage objectAtIndex:indexPath.row - 1];
                    }
                } else {
                    cell.lblMain.text = [arrLanguage objectAtIndex:indexPath.row - 0];
                }
            }
        }
        return cell;
    } else {
        LatestVideoTourCollectionViewCell *cell = (LatestVideoTourCollectionViewCell*)[collViewMain dequeueReusableCellWithReuseIdentifier:@"LatestVideoTourCollectionViewCell" forIndexPath:indexPath];
        VideoTour * v = [videoTourArray objectAtIndex:indexPath.row];
        NSMutableArray *attraction = [NSMutableArray new];
        NSMutableArray *attractionData = [NSMutableArray new];
        Attractions *a;
        AttractionData *aData;
        attraction = [[v.attractionsSet allObjects] mutableCopy];
        if(attraction.count>0){
            a =[attraction objectAtIndex:0];
        }
        
        Account * account = [AccountManager Instance].activeAccount;
        if (account) {
            if ([v.bookmark_users_string containsString:account.userId]) {
                [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                cell.btnBookMark.selected = YES;
            } else {
                [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
                cell.btnBookMark.selected = NO;
            }
        } else {
            [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
            cell.btnBookMark.selected = NO;
        }
        
        attractionData = [[a.attractionDataSet allObjects] mutableCopy];
        if(attractionData.count>0){
            aData = [attractionData objectAtIndex:0];
            cell.lblDescription.text = [NSString stringWithFormat:@"[%@ days] %@",v.days, aData.info.address];
        } else {
            cell.lblDescription.text = [NSString stringWithFormat:@"[%@ days]",v.days];
        }
        
        cell.lblFromDate.text = [Utils dateFormatyyyyMMdd:v.from_date];
        cell.lblVideoName.text = v.name;
        cell.lblUserName.text = v.user.full_name;
        
        if(![v.user.avatar isEqualToString:@""]){
            
            NSString * imgId = v.user.avatar;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            cell.imgUserImg.url = [NSURL URLWithString:urlString];
            cell.imgUserImg.layer.cornerRadius = cell.imgUserImg.frame.size.width / 2;
            cell.imgUserImg.layer.masksToBounds = YES;
        }else{
            cell.imgUserImg.backgroundColor = [UIColor blueColor];
            cell.imgUserImg.layer.cornerRadius = cell.imgUserImg.frame.size.width / 2;
            cell.imgUserImg.layer.masksToBounds = YES;
        }
        
        cell.btnBookMark.tag = indexPath.row;
        [cell.btnBookMark addTarget:self action:@selector(bookmarkClickedVideoTour:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (collectionView == collViewTags ) {
    }else{
        ItineraryViewController *controller = [ItineraryViewController initViewController:[videoTourArray objectAtIndex:indexPath.row]];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView != collViewTags) {
        return CGSizeMake((collectionView.frame.size.width/2)-5, (collectionView.frame.size.height)+35);
    } else {
        NSString * lblString;
        if (sDate.length > 0 && eDate.length > 0 && eDate.intValue > 0) {
            if (indexPath.row == 0) {
                NSString * aString = [NSString stringWithFormat:@"%@-%@ days", sDate, eDate];
                lblString = aString;
            } else {
                if (searchText.length > 0) {
                    if (indexPath.row == 1) {
                        lblString = searchText;
                    } else {
                        if (selfDrive) {
                            if (indexPath.row == 2) {
                                lblString = @"Self-Drive";
                            } else {
                                //                                lblString = [arrLanguage objectAtIndex:indexPath.row - 3];
                                if ([[arrLanguage objectAtIndex:indexPath.row-3] isEqualToString:langChi]) {
                                    lblString = @"Chineese";
                                } else if ([[arrLanguage objectAtIndex:indexPath.row-3] isEqualToString:langMnd]) {
                                    lblString = @"Mandarin";
                                } else{
                                    lblString = @"English";
                                }
                            }
                        } else {
                            //                            lblString = [arrLanguage objectAtIndex:indexPath.row - 2];
                            if ([[arrLanguage objectAtIndex:indexPath.row-2] isEqualToString:langChi]) {
                                lblString = @"Chineese";
                            } else if ([[arrLanguage objectAtIndex:indexPath.row-2] isEqualToString:langMnd]) {
                                lblString = @"Mandarin";
                            } else{
                                lblString = @"English";
                            }
                        }
                    }
                } else {
                    if (selfDrive) {
                        if (indexPath.row == 1) {
                            lblString = @"Self-Drive";
                        } else {
                            //                            lblString = [arrLanguage objectAtIndex:indexPath.row - 2];
                            if ([[arrLanguage objectAtIndex:indexPath.row-2] isEqualToString:langChi]) {
                                lblString = @"Chineese";
                            } else if ([[arrLanguage objectAtIndex:indexPath.row-2] isEqualToString:langMnd]) {
                                lblString = @"Mandarin";
                            } else{
                                lblString = @"English";
                            }
                        }
                    } else {
                        //                        lblString = [arrLanguage objectAtIndex:indexPath.row - 1];
                        if ([[arrLanguage objectAtIndex:indexPath.row-1] isEqualToString:langChi]) {
                            lblString = @"Chineese";
                        } else if ([[arrLanguage objectAtIndex:indexPath.row-1] isEqualToString:langMnd]) {
                            lblString = @"Mandarin";
                        } else{
                            lblString = @"English";
                        }
                    }
                }
            }
        } else {
            if (searchText.length > 0) {
                if (indexPath.row == 0) {
                    lblString = searchText;
                } else {
                    if (selfDrive) {
                        if (indexPath.row == 1) {
                            lblString = @"Self-Drive";
                        } else {
                            lblString = [arrLanguage objectAtIndex:indexPath.row - 2];
                        }
                    } else {
                        lblString = [arrLanguage objectAtIndex:indexPath.row - 1];
                    }
                }
            } else {
                if (selfDrive) {
                    if (indexPath.row == 0) {
                        lblString = @"Self-Drive";
                    } else {
                        lblString = [arrLanguage objectAtIndex:indexPath.row - 1];
                    }
                } else {
                    lblString = [arrLanguage objectAtIndex:indexPath.row - 0];
                }
            }
        }
        
        UIFont *font = [UIFont systemFontOfSize:14.0];
        CGFloat strWidth = [Utils widthOfString:lblString withFont:font];
        
        return CGSizeMake(strWidth+70,32);
        
        
    }
}

-(void)btnCloseClicked:(id)sender {
    
    [SVProgressHUD showWithStatus:@"Updating..."];
    NSInteger Tag = [sender tag];
    
    if (sDate.length > 0 && eDate.length > 0 && eDate.intValue > 0) {
        if (Tag == 0) {
            sDate = @"";
            eDate = @"";
        } else {
            if (searchText.length > 0) {
                if (Tag == 1) {
                    searchText = @"";
                } else {
                    if (selfDrive) {
                        if (Tag == 2) {
                            selfDrive = false;
                        } else {
                            if (arrLanguage.count >= Tag -3) {
                                [arrLanguage removeObjectAtIndex:Tag - 3];
                            }
                        }
                    } else {
                        if (arrLanguage.count >= Tag -2) {
                            [arrLanguage removeObjectAtIndex:Tag - 2];
                        }
                    }
                }
            } else {
                if (selfDrive) {
                    if (Tag == 1) {
                        selfDrive = false;
                    } else {
                        if (arrLanguage.count >= Tag -2) {
                            [arrLanguage removeObjectAtIndex:Tag - 2];
                        }
                    }
                } else {
                    if (arrLanguage.count >= Tag - 1) {
                        [arrLanguage removeObjectAtIndex:Tag - 1];
                    }
                }
            }
        }
    } else {
        if (searchText.length > 0) {
            if (Tag == 0) {
                searchText = @"";
            } else {
                if (selfDrive) {
                    if (Tag == 1) {
                        selfDrive = false;
                    } else {
                        if (arrLanguage.count >= Tag - 2) {
                            [arrLanguage removeObjectAtIndex:Tag - 2];
                        }
                    }
                } else {
                    if (arrLanguage.count >= Tag - 1) {
                        [arrLanguage removeObjectAtIndex:Tag - 1];
                    }
                }
            }
        } else {
            if (selfDrive) {
                if (Tag == 0) {
                    selfDrive = false;
                } else {
                    if (arrLanguage.count >= Tag - 1) {
                        [arrLanguage removeObjectAtIndex:Tag - 1];
                    }
                }
            } else {
                if (arrLanguage.count >= Tag) {
                    [arrLanguage removeObjectAtIndex:Tag];
                }
            }
        }
    }
    
    
    
    [[PlayTripManager Instance] searchVideoTourWithName:searchText andLanguageArray:arrLanguage andStartDate:nil andEndDate:nil andSelfDrive:selfDrive WithBlock:^(id result, NSString *error) {
        if (!error) {
            videoTourArray = [VideoTour getAll];
            [collViewMain reloadData];
            isShowTemp = false;
            [collViewTags reloadData];
        }
        [SVProgressHUD dismiss];
    }];
}

#pragma Searchbar Delegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)sBar {
    [sBar setShowsCancelButton:YES animated:YES];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)sBar{
    [sBar resignFirstResponder];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)sBar{
    [sBar resignFirstResponder];
    [sBar setShowsCancelButton:NO animated:YES];
    sBar.text = @"";
}

- (void)searchBar:(UISearchBar *)sBar textDidChange:(NSString *)text {
    videoTourArray =[NSMutableArray new];
    NSMutableArray * aarVideoTour = [VideoTour getAll];
    
    if([text length] != 0) {
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"name BEGINSWITH[c] %@", text];
        videoTourArray = [[aarVideoTour filteredArrayUsingPredicate:predicate1]mutableCopy];
    } else {
        [sBar resignFirstResponder];
        videoTourArray = [VideoTour getAll];
    }
    
    videoTourCount = (int)videoTourArray.count;
    [collViewMain reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

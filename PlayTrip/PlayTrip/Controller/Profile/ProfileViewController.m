
#import "ProfileViewController.h"
#import "DraftTourViewController.h"
#import "MyVideoTourViewController.h"
#import "MYTDViewController.h"
#import "ItineraryViewController.h"
#import "MyPlanTableViewCell.h"
#import "UserInformationTableViewCell.h"
#import "MyBookMarkTableViewCell.h"
#import "SettingsViewController.h"
#import "ColorConstants.h"
#import "PlayTripManager.h"
#import "AppDelegate.h"
#import "URLSchema.h"
#import "Plan.h"
#import "EditMyPlanViewController.h"
#import "BookmarkUser.h"
#import "BookmarkAttractions.h"
#import "BookmarkLocation.h"
#import "BookmarkVideotour.h"
#import "BookmarkDetail.h"
#import "CommonUser.h"
#import "Attractions.h"
#import "AttractionData.h"
#import "VideoTour.h"
#import "UIAlertController+Blocks.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "ProfileTopViewCell.h"
#import "Info.h"
#import "DraftTour.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import "Localisator.h"


@interface ProfileViewController (){
    NSMutableArray *planArray;
//    NSMutableArray *videoTourDraftArray;
    DraftTour * dTour;
    NSMutableArray *videoTourArray;
    
    BOOL isMyPlanExpanded;
    BOOL isMyVideoTourDraftExpanded;
    BOOL isMyVideoTourExpanded;
    BOOL isMyBookmarksExpanded;

    NSMutableArray *myPlansArray;
    NSMutableArray *myVideoToursArray;

    
}
@end

@implementation ProfileViewController

+(ProfileViewController *)initViewController {
    ProfileViewController * controller = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
    return controller;
}

- (BOOL)prefersStatusBarHidden {
    return NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerNib];
    
    self.leftButton.frame = CGRectMake(0.0, self.view.frame.size.height - 50, 50.0, 50.0);
    self.rightButton.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 50, self.view.frame.size.height - 50, 50.0, 50.0);
    CGRect mf = self.view.frame;
    self.menuView1.frame = CGRectMake(mf.size.width-250,mf.size.height - 110, self.menuView1.frame.size.width, self.menuView1.frame.size.height);
    //self.menuView1.frame = CGRectMake(0, self.view.frame.size.height - 110, self.menuView1.frame.size.width, self.menuView1.frame.size.height);
    
    planArray = [NSMutableArray new];
//    videoTourDraftArray = [NSMutableArray new];
    dTour = [DraftTour getDraft];
    videoTourArray = [NSMutableArray new];
   
    
    self.automaticallyAdjustsScrollViewInsets = NO;// Arun :: for status bar
    
    Account * account = [AccountManager Instance].activeAccount;
    if(account){
        if (account.isSkip == true) {
            //TODO//[[AppDelegate appDelegate] showLogin:self];
            return;
        }
    }
    if (![[AppDelegate appDelegate] isReachable]) {
        [Utils showAlertWithMessage:kCheckInternet];
        return;
    }
    [self getPlanByUserApiNew];
    [self getVideoToursByUserApiNew];
    
    UIView* pv = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
    [pv setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:pv];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = true;
    
    
    
    isMyPlanExpanded = YES;
    isMyVideoTourDraftExpanded = YES;
    isMyVideoTourExpanded = YES;
    isMyBookmarksExpanded = YES;
    
    
    Account * account = [AccountManager Instance].activeAccount;
    if(account){
        if (account.isSkip == true) {
            [self.navigationController popToRootViewControllerAnimated:NO];
            [[AppDelegate appDelegate] showLogin:self];

            
            
        }
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(LoadBookmarkDetails) name:@"BookmarkDetailsUpdated" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadingTable) name:@"ProfileReloadTable" object:nil];
   

}

-(void)viewDidAppear:(BOOL)animated {
    self.leftButton.frame = CGRectMake(0.0, self.view.frame.size.height - 50, 50.0, 50.0);
    self.rightButton.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 50, self.view.frame.size.height - 50, 50.0, 50.0);
    CGRect mf = self.view.frame;
    self.menuView1.frame = CGRectMake(mf.size.width-250,mf.size.height - 110, self.menuView1.frame.size.width, self.menuView1.frame.size.height);
    //self.menuView1.frame = CGRectMake(0, self.view.frame.size.height - 110, self.menuView1.frame.size.width, self.menuView1.frame.size.height);
}

-(void)reloadingTable {
    [self LoadBookmarkDetails];
}

-(void)LoadBookmarkDetails{
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Please wait.."];
    
    Account * account = [AccountManager Instance].activeAccount;
    if(account){
        if (account.isSkip == true) {
            return;
        }
    }
    
    [[PlayTripManager Instance] LoadBookmarkDetails:^(id result, NSString *error) {
        if(!error){
            BookmarkDetail * detail = [BookmarkDetail getFirst];
            arrData_users = [[NSMutableArray alloc] init];
            arrData_locations = [[NSMutableArray alloc] init];
            arrData_attractions = [[NSMutableArray alloc] init];
            arrData_video_tours = [[NSMutableArray alloc] init];
            arrData_users = [[detail.bookmarkUser allObjects] mutableCopy];
            arrData_locations = [[detail.bookmarkLocation allObjects] mutableCopy];
            arrData_attractions = [[detail.bookmarkAttractions allObjects] mutableCopy];
            arrData_video_tours = [[detail.bookmarkVideotour allObjects] mutableCopy];
            [tblView reloadData];
        }
        [SVProgressHUD dismiss];
        //[[NSNotificationCenter defaultCenter] postNotificationName:@"RecommendedUsersUpdated" object:nil];
    }];
}

-(void) backClicked{
    [self.navigationController popViewControllerAnimated:true];
}

-(void)settingsClicked{
    SettingsViewController *controller = [SettingsViewController initViewController];
    [self.navigationController pushViewController:controller animated:YES];
}


-(void)getPlanByUserApiNew {
    Account *account = [AccountManager Instance].activeAccount;
    if(account){
        if (account.isSkip == true) {
            [[AppDelegate appDelegate] showLogin:self];
        } else {
            [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
            [SVProgressHUD showWithStatus:@"Loading My Plans..."];
            
            
            [[PlayTripManager Instance] loadPlanByUserID:account.userId WithBlockNew:^(id result, NSString *error) {
                if(!error){
                    NSError* errorData;
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                         options:kNilOptions
                                                                           error:&errorData];
                    if (errorData) {
                        NSLog(@"ERROR GOTTEN = %@" ,errorData.description);
                    }
                    NSArray* plansDicts = [json objectForKey:@"data"];
                    myPlansArray = [[NSMutableArray alloc] init];
                    for (NSDictionary *planDict in plansDicts) {
                        NSError *error;
                        PlanModel *planModel = [[PlanModel alloc] initWithDictionary:planDict error:&error];
                        if (error) {
                            NSLog(@"ERROR GOTTEN AT JSONMODEL = %@" ,error.description);
                        }
                        [myPlansArray addObject:planModel];
                    }
                    
                    NSLog(@"TOTAL MY PLANS ARE = %lu" ,(unsigned long)myPlansArray.count);

                    
                }
                [SVProgressHUD dismiss];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [tblView reloadData];
                });
                
            }];
        }
    }
}




-(void)getVideoToursByUserApiNew {
    Account *account = [AccountManager Instance].activeAccount;
    if(account){
        if (account.isSkip == true) {
            [[AppDelegate appDelegate] showLogin:self];
        } else {
            [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
            [SVProgressHUD showWithStatus:@"Loading Video Tours..."];
            
            
            [[PlayTripManager Instance] loadVideoToursByUserID:account.userId WithBlockNew:^(id result, NSString *error) {
                if(!error){
                    NSError* errorData;
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                         options:kNilOptions
                                                                           error:&errorData];
                    if (errorData) {
                        NSLog(@"ERROR GOTTEN = %@" ,errorData.description);
                    }
                    NSArray* plansDicts = [json objectForKey:@"data"];
                    myVideoToursArray = [[NSMutableArray alloc] init];
                    for (NSDictionary *planDict in plansDicts) {
                        NSError *error;
                        PlanModel *planModel = [[PlanModel alloc] initWithDictionary:planDict error:&error];
                        if (error) {
                            NSLog(@"ERROR GOTTEN AT JSONMODEL = %@" ,error.description);
                        }
                        [myVideoToursArray addObject:planModel];
                    }
                    
                    NSLog(@"TOTAL MY PLANS ARE = %lu" ,(unsigned long)myVideoToursArray.count);
                    
                    
                }
                [SVProgressHUD dismiss];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [tblView reloadData];
                });
                
            }];
        }
    }
}



/*get specific plan api call*/
-(void)getPlanByUserApi {
    Account *account = [AccountManager Instance].activeAccount;
    if(account){
        if (account.isSkip == true) {
            [[AppDelegate appDelegate] showLogin:self];
        } else {
            [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
            [SVProgressHUD showWithStatus:@"Please wait.."];
           
            
            [[PlayTripManager Instance] loadPlanByUserID:account.userId WithBlock:^(id result, NSString *error) {
                if(!error){
                    planArray = [Plan getPlans];
                    NSMutableArray *commonUsers = (NSMutableArray *)[CommonUser getUserById:account.userId];
                    CommonUser *commonUser ;
                    if (commonUsers.count) {
                        commonUser = [commonUsers objectAtIndex:0];
                    }
                    NSLog(@"Common User is %@" , commonUser.email);
                    videoTourArray = [VideoTour getAll];
                    NSMutableArray *filteredVideoTours = [[NSMutableArray alloc] init];
                    for (VideoTour *videoTour in videoTourArray) {
                        if ([commonUser isEqual:videoTour.user]) {
                            [filteredVideoTours addObject:videoTour];
                        }
                    }
                    videoTourArray = [[NSMutableArray alloc] initWithArray:filteredVideoTours];
//                    draftsArray = [Plan getVideoTourDraft];
//                    NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"from_date" ascending:YES];
//                    NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
//                    NSArray *sortedArray = [draftsArray sortedArrayUsingDescriptors:sortDescriptors];
//                    draftsArray = [sortedArray mutableCopy];
                    dTour = [DraftTour getDraft];
                    
                    [tblView reloadData];
                    
                    [self LoadBookmarkDetails];
//
                    
                }
                [SVProgressHUD dismiss];
                
            }];
        }
    }
}

- (void)editImageClicked:(id)sender {
    if([sender tag] == 100){
        select = true;
    }else{
        select = false;
    }
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *openCamrea = [UIAlertAction actionWithTitle:@"Open Camera"
                                                         style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
                                                             
                                                             if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
                                                                 
                                                                 UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error"message:@"Device has no camera"delegate:nil cancelButtonTitle:@"OK"otherButtonTitles: nil];
                                                                 
                                                                 [myAlertView show];
                                                             }
                                                             else{
                                                                 UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                                                 
                                                                 picker.delegate = self;
                                                                 picker.allowsEditing = YES;
                                                                 picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                                                                 
                                                                 [self presentViewController:picker animated:YES completion:NULL];
                                                             }
                                                         }];
    UIAlertAction *openGallery = [UIAlertAction actionWithTitle:@"Open Gallery"
                                                          style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                  {
                                      UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                                      picker.delegate = self;
                                      picker.allowsEditing = YES;
                                      picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                                      
                                      [self presentViewController:picker animated:YES completion:NULL];
                                  }];
    
    UIAlertAction *cancel=[UIAlertAction actionWithTitle:@"Cancel"
                                                   style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){}];
    [alert addAction:openCamrea];
    [alert addAction:openGallery];
    [alert addAction:cancel];
    [alert setModalPresentationStyle:UIModalPresentationPopover];
    
    UIPopoverPresentationController *popPresenter = [alert popoverPresentationController];
    popPresenter.sourceView = img;
    popPresenter.sourceRect =img.bounds;
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    
    [picker dismissViewControllerAnimated:YES completion:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            chosenImage = info[UIImagePickerControllerEditedImage];
            //    [self->btnProfile setImage:chosenImage forState:UIControlStateNormal];
            NSIndexPath * ip = [NSIndexPath indexPathForRow:0 inSection:0];
            ProfileTopViewCell *topCell = (ProfileTopViewCell *)[tblView cellForRowAtIndexPath:ip];
            chosenImage = [self imageWithImage:chosenImage scaledToWidth:100];
            if(select == false){
                topCell.imgCover.image = chosenImage;
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    [self uploadCoverPic:chosenImage];
                });
            }else{
                topCell.imgAvatar.image=chosenImage ;
                topCell.imgAvatar.layer.cornerRadius = topCell.imgAvatar.frame.size.width/2;
                topCell.imgAvatar.layer.masksToBounds = YES;
                topCell.imgAvatar.backgroundColor = [UIColor orangeColor];
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    [self uploadAvtar:chosenImage andImageURL:[info objectForKey:UIImagePickerControllerReferenceURL]];
                });
            }
        }); 
    }];
    
}

-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

-(void)uploadCoverPic:(UIImage *)coverPic {
    [SVProgressHUD showWithStatus:@"Uploading..."];
    //Account * account = [AccountManager Instance].activeAccount;
    //[account uploadCoverPic:coverPic WithDelegate:self];
    [[PlayTripManager Instance] uploadCoverWithImage:coverPic WithBlockNew:^(id result, NSString *error) {
        [SVProgressHUD dismiss];
        if (!error) {
            [self showAlertWithMessage:@"Avatar Uploaded"];
        }else{
            [self showAlertWithMessage:error.description];
        }
    }];
}

-(void)uploadAvtar:(UIImage *)img1 andImageURL :(NSURL *)imageUrl {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Uploading..."];
    //Account * account = [AccountManager Instance].activeAccount;
    
    [[PlayTripManager Instance] uploadAvatarWithImage:img1 WithBlockNew:^(id result, NSString *error) {
        [SVProgressHUD dismiss];
        if (!error) {
            [self showAlertWithMessage:@"Avatar Uploaded"];
        }else{
            [self showAlertWithMessage:error.description];
        }
    }];
    
    
    //[account uploadAvtar:img1 WithDelegate:self];
}

- (void)registerNib{
    
    [tblView registerNib:[UINib nibWithNibName:@"ProfileTopViewCell" bundle:nil] forCellReuseIdentifier:@"ProfileTopViewCell"];
    
    
    UINib *cellMyPlan = [UINib nibWithNibName:@"MyPlanTableViewCell" bundle:nil];
    [tblView registerNib:cellMyPlan forCellReuseIdentifier:@"MyPlanTableViewCell"];
    
    UINib *cellVideoTour = [UINib nibWithNibName:@"UserInformationTableViewCell" bundle:nil];
    [tblView registerNib:cellVideoTour forCellReuseIdentifier:@"UserInformationTableViewCell"];
    
    UINib *cellMyBookMarkTableViewCell = [UINib nibWithNibName:@"MyBookMarkTableViewCell" bundle:nil];
    [tblView registerNib:cellMyBookMarkTableViewCell forCellReuseIdentifier:@"MyBookMarkTableViewCell"];
}

#pragma mark -- TableView Delegate and DataSource Method

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 1){
        if(myPlansArray.count <= 0){
            return 0;
        }else{
            if (!isMyPlanExpanded) {
                return 0;
            }
            else{
                return myPlansArray.count;
            }
        }
    }else if(section == 2){
        if(!dTour){
            return 0;
        }else{
            if (!isMyVideoTourDraftExpanded) {
                return 0;
            }
            else{
                return 1;
            }
            
        }
    }else if(section == 3){
        if(myVideoToursArray.count <= 0){
            return 0;
        }else{
            if (!isMyVideoTourExpanded) {
                return 0;
            }
            else{
                return myVideoToursArray.count;
            }
        }
    }
    else if(section == 4){
        if (!isMyBookmarksExpanded) {
            return 0;
        }
        else{
            return 1;
        }
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        ProfileTopViewCell *topCell = (ProfileTopViewCell *)[tblView dequeueReusableCellWithIdentifier:@"ProfileTopViewCell"];
        if (topCell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ProfileTopViewCell" owner:nil options:nil];
            topCell = (ProfileTopViewCell *)[nib objectAtIndex:0];
        }
        [topCell setSelectionStyle:UITableViewCellSelectionStyleNone];
        topCell.btnImg.tag = 100;
        [topCell.btnImg addTarget:self action:@selector(editImageClicked:) forControlEvents:UIControlEventTouchUpInside];
        [topCell.btnCover addTarget:self action:@selector(editImageClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [topCell.btnBack addTarget:self action:@selector(backClicked) forControlEvents:UIControlEventTouchUpInside];
        [topCell.btnSettings addTarget:self action:@selector(settingsClicked) forControlEvents:UIControlEventTouchUpInside];
        
        
        Account * account = [AccountManager Instance].activeAccount;
        if(account){
            if (account.isSkip != true) {
                topCell.lblName.text = account.full_name;
                topCell.lblBookmarkCount.text = account.totalBookmark;
                
                
                
                topCell.lblBookmark.text = account.totalBookmark;
                topCell.lblViews.text = account.totalViews;
                topCell.lblShares.text = account.totalShares;
                topCell.lblMoney.text = @"0";
                
                
                if(![account.avatar isEqualToString:@""]){
                    
                    NSString * imgId = account.avatar;
                    NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
                    topCell.imgAvatar.url = [NSURL URLWithString:urlString];
                    topCell.imgAvatar.layer.cornerRadius = topCell.imgAvatar.frame.size.width/2;
                    topCell.imgAvatar.layer.masksToBounds = YES;
                }else{
                    //  imgProfile.image = [UIImage imageNamed:@"mexico.jpg"];
                    topCell.imgAvatar.layer.cornerRadius = topCell.imgAvatar.frame.size.width/2;
                    topCell.imgAvatar.layer.masksToBounds = YES;
                }
                if(![account.cover_pic isEqualToString:@""]){
                    
                    NSString * imgId = account.cover_pic;
                    NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
                    topCell.imgCover.url = [NSURL URLWithString:urlString];
                    
                    topCell.imgCover.layer.masksToBounds = YES;
                }else{
                    topCell.imgCover.backgroundColor = [ColorConstants appYellowColor];
                }
            }
        }
        return topCell;
    } else if(indexPath.section == 1){
        if (myPlansArray.count <= 0) {
            UITableViewCell *planCell =  [tableView dequeueReusableCellWithIdentifier:@"PlanCell"];
            if (!planCell) {
                planCell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"PlanCell"];
            }
            planCell.textLabel.text = LOCALIZATION(@"no_plan");
            //planCell.textLabel.text = @"No Plan Found";
            [planCell.textLabel setTextAlignment:NSTextAlignmentCenter];
            [planCell setUserInteractionEnabled:NO];
            return planCell;
        }
        static NSString *planCellIdentifier = @"MyPlanTableViewCell";
        MyPlanTableViewCell *planCell = (MyPlanTableViewCell *)[tblView dequeueReusableCellWithIdentifier:planCellIdentifier];
        if (planCell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:planCellIdentifier owner:nil options:nil];
            planCell = (MyPlanTableViewCell *)[nib objectAtIndex:0];
        }
        [planCell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        
        PlanModel *plan = myPlansArray[indexPath.row];
        planCell.lblPlanName.text = plan.name;
        planCell.lblPlanDays.text = [NSString stringWithFormat:@"%@ %@",plan.days, LOCALIZATION(@"plan_day")];
        
        planCell.lblCreateDay.hidden = NO;
        planCell.lblLastEdited.hidden = NO;

        
        planCell.lblCreateDay.text = [NSString stringWithFormat:@"Date created : %@",([plan.create_date length]>10 ? [plan.create_date substringToIndex:10] : plan.create_date)];
        
        NSString *lastUpdated = ([plan.last_updated length]>16 ? [plan.last_updated substringToIndex:16] : plan.last_updated);
        lastUpdated = [lastUpdated stringByReplacingOccurrencesOfString:@"T" withString:@" "];
        
        planCell.lblLastEdited.text =[NSString stringWithFormat:@"Last Edited : %@",lastUpdated];
        planCell.lblLanguage.text = plan.language;
        
        planCell.tag1.hidden = YES;
        planCell.tag2.hidden = YES;
        planCell.tag3.hidden = YES;
        planCell.tag4.hidden = YES;
        planCell.tag5.hidden = YES;
        planCell.lblDescription.hidden = YES;
        
        if (plan.attractions.count) {
            
        }
        

        
        /*

        Plan * p = [planArray objectAtIndex:indexPath.row];
        planCell.lblPlanName.text = p.name;
        planCell.lblPlanDays.text = [NSString stringWithFormat:@"%@ days",p.days];
        NSMutableArray *attraction = [NSMutableArray new];
        NSMutableArray *attractionData = [NSMutableArray new];
        Attractions *a;
        AttractionData *aData;
        attraction = [[p.attractionsSet allObjects] mutableCopy];
        
        if(attraction.count>0){
            a =[attraction objectAtIndex:0];
        }
        attractionData = [[a.attractionDataSet allObjects] mutableCopy];
        if(attractionData.count>0){
            aData = [attractionData objectAtIndex:0];
            planCell.lblDescription.text = [NSString stringWithFormat:@"[%@ days] %@",p.days, aData.info.address];
        } else {
            planCell.lblDescription.text = [NSString stringWithFormat:@"[%@ days]",p.days];
        }
        
        planCell.lblCreateDay.text = [NSString stringWithFormat:@"Create Day : %@",[Utils dateFormatyyyyMMdd:p.create_date]];
        planCell.lblLastEdited.text =[NSString stringWithFormat:@"Last Edited : %@",[Utils dateFormatyyyyMMddHHmm:p.last_updated]];
        NSString * fromDay = [NSString stringWithFormat:@"%@", [Utils getDayShortFromDate:p.from_date]];
        NSString * fromDate = [NSString stringWithFormat:@"%@", [Utils dateFormatyyyyMMdd:p.from_date]];
        
        NSString * fromString = [NSString stringWithFormat:@"%@(%@)", fromDate, fromDay];
        NSDate * toDate = [Utils addDays:[p.days integerValue] toDate:p.from_date];
        NSString * toString = [NSString stringWithFormat:@"%@(%@)", [Utils dateFormatyyyyMMdd:toDate], [Utils getDayShortFromDate:toDate]];
        
        NSString * totalString = [NSString stringWithFormat:@"%@ to %@", fromString, toString];
        planCell.lblPlanDates.numberOfLines = 2;
        planCell.lblPlanDates.text = [NSString stringWithFormat:@"%@",totalString];
        planCell.btnDelete.tag = indexPath.row;
        [planCell.btnDelete addTarget:self action:@selector(deleteClicked:) forControlEvents:UIControlEventTouchUpInside];
         */
        return planCell;
    }else if(indexPath.section == 2){
        if (!dTour) {
            UITableViewCell *draftCell =  [tableView dequeueReusableCellWithIdentifier:@"DraftCell"];
            if (!draftCell) {
                draftCell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"DraftCell"];
            }
            draftCell.textLabel.text = @"No VideoTour(Draft) Found";
            [draftCell.textLabel setTextAlignment:NSTextAlignmentCenter];
            [draftCell setUserInteractionEnabled:NO];
            return draftCell;
        }
        static NSString *draftCellIdentifier = @"MyPlanTableViewCell";
        MyPlanTableViewCell *draftCell = (MyPlanTableViewCell *)[tblView dequeueReusableCellWithIdentifier:draftCellIdentifier];
        if (draftCell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:draftCellIdentifier owner:nil options:nil];
            draftCell = (MyPlanTableViewCell *)[nib objectAtIndex:0];
        }
        [draftCell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
//        Plan * p = [draftsArray objectAtIndex:indexPath.row];
        
        draftCell.lblPlanName.text = dTour.name;
        draftCell.lblPlanDays.text = [NSString stringWithFormat:@"%@ days",dTour.days];
        
        NSMutableArray *attraction = [NSMutableArray new];
        NSMutableArray *attractionData = [NSMutableArray new];
        Attractions *a;
        AttractionData *aData;
        attraction = [[dTour.attractionsSet allObjects] mutableCopy];
        if(attraction.count>0){
            a =[attraction objectAtIndex:0];
        }
        attractionData = [[a.attractionDataSet allObjects] mutableCopy];
        if(attractionData.count>0){
            aData = [attractionData objectAtIndex:0];
            draftCell.lblDescription.text = [NSString stringWithFormat:@"[%@ days] %@",dTour.days, aData.info.address];
        } else {
            draftCell.lblDescription.text = [NSString stringWithFormat:@"[%@ days]",dTour.days];
        }
        draftCell.lblCreateDay.text = [NSString stringWithFormat:@"Create Day : %@",[Utils dateFormatyyyyMMdd:dTour.create_date]];
        draftCell.lblLastEdited.text =[NSString stringWithFormat:@"Last Edited : %@",[Utils dateFormatyyyyMMdd:dTour.last_updated]];
        draftCell.lblPlanDates.text = [Utils dateFormatyyyyMMdd:dTour.from_date];
        draftCell.btnDelete.tag = indexPath.row;
        [draftCell.btnDelete addTarget:self action:@selector(deleteDraftClicked:) forControlEvents:UIControlEventTouchUpInside];
        return draftCell;
    }else if(indexPath.section == 3){
        if (myVideoToursArray.count <= 0) {
            UITableViewCell *videoTourCell =  [tableView dequeueReusableCellWithIdentifier:@"VideoTourCell"];
            if (!videoTourCell) {
                videoTourCell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"VideoTourCell"];
            }
            videoTourCell.textLabel.text = @"No VideoTour Found";
            [videoTourCell.textLabel setTextAlignment:NSTextAlignmentCenter];
            [videoTourCell setUserInteractionEnabled:NO];
            return videoTourCell;
        }
        static NSString *videoTourCellIdentifier = @"UserInformationTableViewCell";
        UserInformationTableViewCell *videoTourCell = (UserInformationTableViewCell *)[tblView dequeueReusableCellWithIdentifier:videoTourCellIdentifier];
        if (videoTourCell == nil) {
            NSArray *nib = [[NSBundle mainBundle] loadNibNamed:videoTourCellIdentifier owner:nil options:nil];
            videoTourCell = (UserInformationTableViewCell *)[nib objectAtIndex:0];
        }
        [videoTourCell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        
        
        videoTourCell.btnList.hidden = YES;
        
        PlanModel *plan = myVideoToursArray[indexPath.row];
        
        
        videoTourCell.lblVideoTourName.text = plan.name;
        videoTourCell.lblDescription.text = plan.description;
        videoTourCell.lblCreateDate.text = ([plan.create_date length]>10 ? [plan.create_date substringToIndex:10] : plan.create_date);
        
        if(![plan.cover_photo isEqualToString:@""]){
            NSString * imgId = plan.cover_photo;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            //NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];

            //videoTourCell.imgVideoTour.url = [NSURL URLWithString:urlString];
            
            
            [videoTourCell.imgVideoTour sd_setShowActivityIndicatorView:YES];
            [videoTourCell.imgVideoTour sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            [videoTourCell.imgVideoTour sd_setImageWithURL:[NSURL URLWithString:urlString]
                                 placeholderImage:[UIImage imageNamed:@"def_video_img.png"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                                     if (!error) {
                                         videoTourCell.imgVideoTour.image = image;
                                         [videoTourCell layoutIfNeeded];
                                     }
                                     else{
                                         videoTourCell.imgVideoTour.url = [NSURL URLWithString:urlString];
                                     }
                                 }];

            
            
        }
        videoTourCell.lblLanguge.text = plan.language;
        videoTourCell.lblCountry.hidden = YES;
        videoTourCell.lblDistrict.hidden = YES;
        videoTourCell.lblCity.hidden = YES;
        
        for (AttractionModel *attraction in plan.attractions) {
            if (attraction.attr_data.count) {
                AttractionDataModel *attratctionData = [attraction.attr_data objectAtIndex:0];
                videoTourCell.lblTime.text = attratctionData.time;
                if (attratctionData.info.country_id) {
                    videoTourCell.lblCountry.text = attratctionData.info.country_id.name;
                    videoTourCell.lblDistrict.text = attratctionData.info.district_id.name;
                    videoTourCell.lblCity.text = attratctionData.info.city_id.name;
                    videoTourCell.lblCountry.hidden = NO;
                    videoTourCell.lblDistrict.hidden = NO;
                    videoTourCell.lblCity.hidden = NO;
                    
                    
                    break;
                }
            }
        }
        videoTourCell.lblBookmarks.text = [NSString stringWithFormat:@"%@",plan.total_bookmark];
        videoTourCell.lblViews.text = [NSString stringWithFormat:@"%@",plan.total_view];
        videoTourCell.lblShares.text = [NSString stringWithFormat:@"%@",plan.total_share];
        videoTourCell.lblRevert.text = [NSString stringWithFormat:@"%@",plan.remuneration_amount];
        
        videoTourCell.btnBookmark.hidden = YES;

        
    
        
        videoTourCell.lblLanguge.hidden = YES;
        if([plan.language  isEqualToString: @"sc"]){
            videoTourCell.languageImage.image = [UIImage imageNamed:@"speaker_.png"];
        }else if([plan.language  isEqualToString: @"tc"]){
            videoTourCell.languageImage.image = [UIImage imageNamed:@"speaker_2.png"];
        }else{
            videoTourCell.languageImage.image = [UIImage imageNamed:@"speaker_Eng.png"];
        }

        
        
        /*
        VideoTour * v = [videoTourArray objectAtIndex:indexPath.row];
        
        videoTourCell.lblTime.text = [NSString stringWithFormat:@"%.2f", v.duration.floatValue];

        
        NSMutableArray *attraction = [NSMutableArray new];
        NSMutableArray *attractionData = [NSMutableArray new];
        Attractions *a;
        AttractionData *aData;
        attraction = [[v.attractionsSet allObjects] mutableCopy];
        if(attraction.count>0){
            a =[attraction objectAtIndex:0];
        }
        attractionData = [[a.attractionDataSet allObjects] mutableCopy];
        if(attractionData.count>0){
            aData = [attractionData objectAtIndex:0];
            videoTourCell.lblDescription.text = [NSString stringWithFormat:@"[%@ days] %@",v.days, aData.info.address];
        } else {
            videoTourCell.lblDescription.text = [NSString stringWithFormat:@"[%@ days]",v.days];
        }
        videoTourCell.lblCreateDate.text = [Utils dateFormatyyyyMMdd:v.from_date];
        videoTourCell.lblVideoTourName.text = v.name;
        
        
        if(v.thumbnail != nil){
            
            NSString * imgId = v.thumbnail;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            videoTourCell.imgVideoTour.url = [NSURL URLWithString:urlString];
        }
        videoTourCell.lblBookmarks.text = [NSString stringWithFormat:@"%@",v.total_bookmark];
        videoTourCell.lblViews.text = [NSString stringWithFormat:@"%@",v.total_view];
        videoTourCell.lblRevert.text = [NSString stringWithFormat:@"%@",v.remuneration_amount];
        videoTourCell.lblShares.text = [NSString stringWithFormat:@"%@",v.total_share];
        videoTourCell.btnBookmark.hidden = YES;
        videoTourCell.btnDelete.tag = indexPath.row;
        [videoTourCell.btnDelete addTarget:self action:@selector(videoTourDeleteClicked:) forControlEvents:UIControlEventTouchUpInside];
        videoTourCell.btnEdit.tag = indexPath.row;
        [videoTourCell.btnEdit addTarget:self action:@selector(editClicked) forControlEvents:UIControlEventTouchUpInside];
        */
        return videoTourCell;
    }else{
        MyBookMarkTableViewCell *cell = (MyBookMarkTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"MyBookMarkTableViewCell" forIndexPath:indexPath];
        cell.arrData_users = arrData_users;
        cell.arrData_locations = arrData_locations;
        cell.arrData_attractions = arrData_attractions;
        cell.arrData_video_tours = arrData_video_tours;
        cell.tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        [cell.tblView reloadData];
        [cell.collectionview reloadData];
        return cell;
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tblView.frame.size.width, 30)];
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, 200, 20)];
    UIImageView *imgViewNoExpand = [[UIImageView alloc] initWithFrame:CGRectMake(view.frame.size.width-20, 5, 15, 15)];

    lblTitle.textColor = [UIColor darkGrayColor];
    lblTitle.font = [UIFont systemFontOfSize:13];
    //    lblTitle.backgroundColor = [ColorConstants appYellowColor];
    if (section == 0) {
        return nil;
    } else if (section == 1) {
        [lblTitle setText:LOCALIZATION(@"my_plan")];
        if(!isMyPlanExpanded) {
            imgViewNoExpand.image = [UIImage imageNamed:@"Y_Line_xx.png"];
        }
        else {
            imgViewNoExpand.image = [UIImage imageNamed:@"Y_Linee.png"];
        }
        
    }else if (section == 2){
        
        [lblTitle setText:LOCALIZATION(@"my_video_tour_draft")];
        if(!isMyVideoTourDraftExpanded) {
            imgViewNoExpand.image = [UIImage imageNamed:@"Y_Line_xx.png"];
        }
        else {
            imgViewNoExpand.image = [UIImage imageNamed:@"Y_Linee.png"];
        }
    }else if (section == 3){
        
        NSString *firstStr = LOCALIZATION(@"my_video_tour");
        lblTitle.text = [NSString stringWithFormat:@"%@ (%lu)",firstStr,(unsigned long)myVideoToursArray.count];
        
        if(!isMyVideoTourExpanded) {
            imgViewNoExpand.image = [UIImage imageNamed:@"Y_Line_xx.png"];
        }
        else {
            imgViewNoExpand.image = [UIImage imageNamed:@"Y_Linee.png"];
        }
    }else{
        NSString *firstStr = LOCALIZATION(@"my_bookmark");
        NSUInteger allBookMarks = arrData_video_tours.count + arrData_attractions.count + arrData_locations.count + arrData_users.count;
        
        lblTitle.text = [NSString stringWithFormat:@"%@ (%lu)",firstStr,(unsigned long)allBookMarks];
        if(!isMyBookmarksExpanded) {
            imgViewNoExpand.image = [UIImage imageNamed:@"Y_Line_xx.png"];
        }
        else {
            imgViewNoExpand.image = [UIImage imageNamed:@"Y_Linee.png"];
        }
    }
    
    //UIImage *img1 = [UIImage imageNamed:@"Y_Line.png"];
    
    UIImage *img1 = [UIImage imageNamed:@"Y_LineYELLOWw.png"];
    view.layer.contents = (__bridge id _Nullable)(img1.CGImage);
    
    [view addSubview:lblTitle];
    [view addSubview:imgViewNoExpand];
    
    
    UIButton *cellButton = [[UIButton alloc] initWithFrame:view.frame];
    cellButton.tag = section;
    [cellButton addTarget:self action:@selector(expandableClicked:) forControlEvents:UIControlEventTouchUpInside];

    [view addSubview:cellButton];

    
    return view;
}

-(void)expandableClicked:(UIButton *)sender{
    if (sender.tag == 1) {
        isMyPlanExpanded = !isMyPlanExpanded;
    }
    else if (sender.tag == 2) {
        isMyVideoTourDraftExpanded = !isMyVideoTourDraftExpanded;
    }
    else if (sender.tag == 3) {
        isMyVideoTourExpanded = !isMyVideoTourExpanded;
    }
    else if (sender.tag == 4) {
        isMyBookmarksExpanded = !isMyBookmarksExpanded;
    }
    
    
    [tblView beginUpdates];
    [tblView reloadSections:[NSIndexSet indexSetWithIndex:sender.tag] withRowAnimation:UITableViewRowAnimationFade];
    [tblView endUpdates];
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return 0;
    }
    return 30;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1.0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return 240.0;
    } else if (indexPath.section < 3) {
        return 100.0;
    }else if (indexPath.section == 3){
        return 108.0;
    }else{
        return 293;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1) {
        PlanModel *plan = [myPlansArray objectAtIndex:indexPath.row];
        EditMyPlanViewController *controller = [EditMyPlanViewController initViewControllerWithPlanModel:plan];
        [self.navigationController pushViewController:controller animated:YES];
    } else if (indexPath.section == 2) {
        if (![[AppDelegate appDelegate] isReachable]) {
            [Utils showAlertWithMessage:kCheckInternet];
            return;
        }
//        draftsArray = [Plan getVideoTourDraft];
//        
//        
//        NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"from_date" ascending:YES];
//        NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
//        NSArray *sortedArray = [draftsArray sortedArrayUsingDescriptors:sortDescriptors];
//        draftsArray = [sortedArray mutableCopy];
        
//        Plan * pl = [draftsArray objectAtIndex:indexPath.row];
        DraftTourViewController * controller = [DraftTourViewController initViewController];
//        MYTDViewController * controller = [MYTDViewController initController:pl];
        [self.navigationController pushViewController:controller animated:YES];
    } else if (indexPath.section == 3) {
        
        PlanModel *plan = myVideoToursArray[indexPath.row];

        //VideoTour * v = [videoTourArray objectAtIndex:indexPath.row];
        ItineraryViewController * controller = [ItineraryViewController initViewControllerWithPlan:plan];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

-(void)editClicked{}//Dont delete

-(void)videoTourDeleteClicked:(id)sender {
    VideoTour * v = [videoTourArray objectAtIndex:[sender tag]];
    [UIAlertController showAlertInViewController:self withTitle:ALERT_TITLE message:@"Are you sure you want to delete?" cancelButtonTitle:@"Yes" destructiveButtonTitle:@"No" otherButtonTitles:nil tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
        if (buttonIndex == controller.cancelButtonIndex) {
            [self deleteVideoTourWithId:v.entity_id];
        }
    }];
}

-(void)deleteVideoTourWithId:(NSString *)vId{
    [[PlayTripManager Instance]deleteTourWithId:vId WithBlock:^(id result, NSString *error) {
        if(!error){
            [UIAlertController showAlertInViewController:self withTitle:@"PlayTrip" message:@"VideoTour deleted" cancelButtonTitle:@"Ok" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                if (buttonIndex == controller.cancelButtonIndex) {
                    [self getPlanByUserApi];
                }
            }];
        }else{
            [UIAlertController showAlertInViewController:self withTitle:@"PlayTrip" message:error cancelButtonTitle:@"Ok" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:nil];
        }
    }];
    
}

-(void)deleteClicked:(id)sender {
    Plan * p = [planArray objectAtIndex:[sender tag]];
    [UIAlertController showAlertInViewController:self withTitle:@"PlayTrip" message:@"Are you sure you want to delete?" cancelButtonTitle:@"Yes" destructiveButtonTitle:@"No" otherButtonTitles:nil tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
        if (buttonIndex == controller.cancelButtonIndex) {
            [self deletePlanWithId:p.entity_id];
            
        }
    }];
}


-(void)deleteDraftClicked:(id)sender {
    
    NSString * msg1 = [NSString stringWithFormat:@"Are you sure you want to delete %@", dTour.name];
    UIAlertController * alert1 = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:msg1 preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * yesAction1 = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
        NSString * msg2 = [NSString stringWithFormat:@"Really? This process is irreversible!"];
        UIAlertController * alert2 = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:msg2 preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction * yesAction2 = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
            [DraftVideoId deleteAll];
            [DraftInfo deleteAll];
            [DraftAttrData deleteAll];
            [DraftAttraction deleteAll];
            [DraftTour deleteObj:dTour];
            dTour = nil;
            [tblView reloadData];
        }];
        UIAlertAction * noAction2 = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:nil];
        [alert2 addAction:yesAction2];
        [alert2 addAction:noAction2];
        
        [self.navigationController presentViewController:alert2 animated:YES completion:nil];
    }];
    UIAlertAction * noAction1 = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDefault handler:nil];
    [alert1 addAction:yesAction1];
    [alert1 addAction:noAction1];
    
    [self.navigationController presentViewController:alert1 animated:YES completion:nil];
    
    
    
    
    
//    Plan * p = [draftsArray objectAtIndex:[sender tag]];
//    [UIAlertController showAlertInViewController:self withTitle:@"PlayTrip" message:@"Are you sure you want to delete?" cancelButtonTitle:@"Yes" destructiveButtonTitle:@"No" otherButtonTitles:nil tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
//        if (buttonIndex == controller.cancelButtonIndex) {
//            [self deletePlanWithId:p.entity_id];
//            
//        }
//    }];
}

-(void)deletePlanWithId:(NSString *)pId{
    [[PlayTripManager Instance]deleteTourWithId:pId WithBlock:^(id result, NSString *error) {
        if(!error){
            [UIAlertController showAlertInViewController:self withTitle:@"PlayTrip" message:@"VideoTour deleted" cancelButtonTitle:@"Ok" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:^(UIAlertController *controller, UIAlertAction *action, NSInteger buttonIndex){
                if (buttonIndex == controller.cancelButtonIndex) {
                    [self getPlanByUserApi];
                }
            }];
        }else{
            [UIAlertController showAlertInViewController:self withTitle:@"PlayTrip" message:error cancelButtonTitle:@"Ok" destructiveButtonTitle:nil otherButtonTitles:nil tapBlock:nil];
        }
    }];
    
}



- (void)showAlertWithMessage:(NSString *)message{
    /*
     this method is used for showing message Dialog
     */
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                       style:UIAlertActionStyleDefault
                                                     handler:nil];
    [alertController addAction:actionOk];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark -- Account Manager Delegate

-(void)accountAuthenticatedWithAccount:(Account*) account{
    [SVProgressHUD dismiss];
    [self showAlertWithMessage:@"Image uploaded successfully"];
    NSIndexPath * ip = [NSIndexPath indexPathForRow:0 inSection:0];
    [tblView reloadRowsAtIndexPaths:@[ip] withRowAnimation:UITableViewRowAnimationFade];
    [tblView reloadData];
}

-(void)accountDidFailAuthentication:(NSString*) error{
    [SVProgressHUD dismiss];
    [self showAlertWithMessage:error];
}

#pragma Login Delegate Method

- (void) loginSuccessfully{
    
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = false;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

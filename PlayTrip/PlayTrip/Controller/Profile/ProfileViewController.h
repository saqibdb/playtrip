
#import <UIKit/UIKit.h>
#import "LoginDelegate.h"
#import "URLImageView.h"
#import "Account.h"
#import "AccountManager.h"
#import "MenuButtonViewController.h"

@interface ProfileViewController : MenuButtonViewController <UITableViewDataSource,UITableViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, AccountAuthenticateDelegate, LoginDelegate>{
    
    NSMutableArray *arrData_video_tours;
    NSMutableArray *arrData_users;
    NSMutableArray *arrData_attractions;
    NSMutableArray *arrData_locations;
    
//    NSMutableArray * draftsArray;
    
    IBOutlet UIImageView *img;
    UIImage *chosenImage;
    IBOutlet UITableView *tblView;
    BOOL select;
}

+(ProfileViewController *)initViewController ;

@end

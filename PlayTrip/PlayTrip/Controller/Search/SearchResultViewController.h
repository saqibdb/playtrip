
#import <UIKit/UIKit.h>
#import "MenuButtonViewController.h"
#import "PopularVideoSort.h"

@interface SearchResultViewController : MenuButtonViewController <UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource> {
    
    PopularVideoSort * sortView;
    UIView * subMenuView;
    BOOL isSortShown;

    IBOutlet UIButton *btnVideo;
    IBOutlet UIButton *btnAttr;
    
    
    IBOutlet UIView *viewVideo;
    IBOutlet UIView *viewAttr;
    
    IBOutlet UICollectionView *collViewTagsVideo;
    IBOutlet UICollectionView *collViewMainVideo;
    
    IBOutlet UICollectionView *collViewTagsAttr;
    IBOutlet UICollectionView *collViewMainAttr;
    
    int lastSortedVideoIndex;
    int lastSortedAttrIndex;
    
    BOOL lastSortedVideoUp;
    BOOL lastSortedAttrUp;
    
    //Video Segment
    NSMutableArray * videoTourArray;
    int videoTourCount;
    
    NSString * searchTextVideo;
    NSString * sDate;
    NSString * eDate;
    BOOL selfDrive;
    NSMutableArray * arrLanguageVideo;
    BOOL isShowTemp;
    
    BOOL isVideoSearched;
    
    IBOutlet UILabel *lblNoDataFound;
    
    //Attr Segment
    NSString * searchTextAttr;
    NSMutableArray * arrLanguageAttr;
    NSMutableArray * arrCategory;
    
    NSMutableArray * arrAttractionData;
    
    NSMutableArray * searchResultVideoTours;
    NSMutableArray * searchResultAttraction;

    
    
}

@property (nonatomic) UISearchBar *searchBar;
- (IBAction)btnAttrClicked:(id)sender;
- (IBAction)btnVideoClicked:(id)sender;

+(SearchResultViewController *)initViewControllerWithSearchText:(NSString *)searchText andLanguageArray:(NSMutableArray *)langArray andStartDate:(NSString *)sDate andEndDate:(NSString *)eDate withSelfDrive:(BOOL)selfDrive;

+(SearchResultViewController *)initViewControllerWithSearchText:(NSString *)searchText andLangArray:(NSMutableArray *)LangArray andCatArray:(NSMutableArray *)catArray;

+(SearchResultViewController *)initViewControllerWithSearchResultsArray:(NSMutableArray *)resultsArray WithSearchText:(NSString *)searchText andLanguageArray:(NSMutableArray *)langArray andStartDate:(NSString *)sDate andEndDate:(NSString *)eDate withSelfDrive:(BOOL)selfDrive ;
+(SearchResultViewController *)initViewControllerWithSearchText:(NSString *)searchText andLangArray:(NSMutableArray *)LangArray andCatArray:(NSMutableArray *)catArray andSearchResult:(NSMutableArray *)searchResults;

@end

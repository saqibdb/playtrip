
#import "SearchTabViewController.h"
#import "SearchResultViewController.h"
#import "Attractions.h"
#import "ColorConstants.h"
#import "PlayTripManager.h"
#import "VideoTour.h"
#import "MostPopularSearchVideoTour.h"
#import "LanguageTableViewCell.h"
#import "DayTableViewCell.h"
#import "PopularTableViewCell.h"
#import "SVProgressHUD.h"
#import "AttractionData.h"
#import "URLSchema.h"
#import "MostPopularSearchAttraction.h"
#import "Categori.h"
#import "LanguageTableViewCell.h"
#import "WhatToDoTableViewCell.h"
#import "PopularTableViewCell.h"
#import "Localisator.h"
#import "Account.h"
#import "AccountManager.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>

#define langChi  @"ch"
#define langMnd  @"md"
#define langEng  @"en"

@interface SearchTabViewController (){
    NSMutableArray *seachedTours;
    NSMutableArray *seachedAttractions;
    NSMutableArray *categoryArrayWithLanguages;
}

@end

@implementation SearchTabViewController

+(SearchTabViewController *)initViewController {
    SearchTabViewController * controller = [[SearchTabViewController alloc] initWithNibName:@"SearchTabViewController" bundle:nil];
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    lblVideo.backgroundColor = [ColorConstants appYellowColor];
    lblAttr.backgroundColor = [ColorConstants appYellowColor];
    
    tblVideo.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tblAttr.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    //Video Segmemnt
    arrMostSearchVideo = [NSMutableArray new];
    arrLanguageVideo = [NSMutableArray new];
    self.searchBar.showsCancelButton = YES;
    self.searchBar.delegate= self;
    //[self getApiResponseVideo];
    
    //Attraction Segment
    arrMostSearchAttr = [NSMutableArray new];
    arrCategoryAttr = [NSMutableArray new];
    arrLanguageAttr = [NSMutableArray new];
    

    [self registerNIB];
    [self setNavBarItems];
    [self setLocalizedStrings];
}

-(void)setLocalizedStrings {
    [self.lblSelfDrive setText:LOCALIZATION(@"self_drive")];
    [self.lblYes setText:LOCALIZATION(@"yes")];
    [self.lblNo setText:LOCALIZATION(@"no")];
    [btnVideoTour setTitle:LOCALIZATION(@"video_tour") forState:UIControlStateNormal];
    [btnAttr setTitle:LOCALIZATION(@"attraction_video") forState:UIControlStateNormal];

    /*
    self.lblSelfDrive.text = [NSString stringWithFormat:NSLocalizedString(@"self_drive", nil)];
    self.lblYes.text = [NSString stringWithFormat:NSLocalizedString(@"yes", nil)];
    self.lblNo.text = [NSString stringWithFormat:NSLocalizedString(@"no", nil)];
    [btnVideoTour setTitle:NSLocalizedString(@"video_tour", nil) forState:UIControlStateNormal];
    [btnAttr setTitle:NSLocalizedString(@"attraction_video", nil) forState:UIControlStateNormal];
     */
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.searchBar.showsCancelButton = YES;
    self.searchBar.delegate = self;
    [self videoClicked:self];
    
    btnAttr.backgroundColor = [ColorConstants appYellowColor];
    btnVideoTour.backgroundColor = [ColorConstants appYellowColor];

    selfDrive = true;
    [self getAllCategory];

}

- (void)registerNIB{
    [tblVideo registerNib:[UINib nibWithNibName:@"LanguageTableViewCell" bundle:nil] forCellReuseIdentifier:@"LanguageTableViewCell"];
    [tblVideo registerNib:[UINib nibWithNibName:@"DayTableViewCell" bundle:nil] forCellReuseIdentifier:@"DayTableViewCell"];
    [tblVideo registerNib:[UINib nibWithNibName:@"PopularTableViewCell" bundle:nil] forCellReuseIdentifier:@"PopularTableViewCell"];
    
    [tblAttr registerNib:[UINib nibWithNibName:@"LanguageTableViewCell" bundle:nil] forCellReuseIdentifier:@"LanguageTableViewCell"];
    [tblAttr registerNib:[UINib nibWithNibName:@"WhatToDoTableViewCell" bundle:nil] forCellReuseIdentifier:@"WhatToDoTableViewCell"];
    [tblAttr registerNib:[UINib nibWithNibName:@"PopularTableViewCell" bundle:nil] forCellReuseIdentifier:@"PopularTableViewCell"];
}

-(void)setNavBarItems {
    // Arun :: customizing the nav bar icons
    self.navigationController.navigationBar.backgroundColor = [ColorConstants appYellowColor];
    _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,45)];
    _searchBar.delegate = self;
    //    searchBar.showsCancelButton = YES;
    _searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    _searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
    //    [self set_searchBar:searchBar];
    self.navigationItem.titleView = _searchBar;
    UIImage *buttonImage = [UIImage imageNamed:@"home_brown.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(homeClicked) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
    
}

-(void)getApiResponseVideo {
    [[PlayTripManager Instance] getMostPopularSearchDataForVideoTour:^(id result, NSString *error) {
        if (!error) {
         
            arrMostSearchVideo = [MostPopularSearchVideoTour getAll];
            [tblVideo reloadData];
        }
        [self getAllCategory];
    }];
}

-(void)getAllCategory {
    
    categoryListAttr = [NSMutableArray new];

    [[PlayTripManager Instance]loadCategory:^(id result, NSString *error) {
        if(!error){
            
            if(result){
                categoryArrayWithLanguages = [[NSMutableArray alloc] init];
                for (NSDictionary *categoryDict in result) {
                    NSError *error;
                    CategoryModel *category = [[CategoryModel alloc] initWithDictionary:categoryDict error:&error];
                    if (!error) {
                        [categoryArrayWithLanguages addObject:category];
                    }
                }
            }
            [[PlayTripManager Instance]loadCategory:^(id result, NSString *error) {
                if(!error){
                    
                    categoryListAttr = [Categori getAll];
                    [tblAttr reloadData];
                    [self getApiResponseAttr];
                }
            }];
        }
    }];
    
    
    
    
    
    
    
   
}

-(void)getApiResponseAttr {
    [[PlayTripManager Instance] getMostPopularSearchDataForAttraction:^(id result, NSString *error) {
        if (!error) {
       
            arrMostSearchAttr = [MostPopularSearchAttraction getAll];
            [tblAttr reloadData];
        }
    }];
}

- (IBAction)videoClicked:(id)sender {
    
    [btnVideoTour setBackgroundImage:[UIImage imageNamed:@"brown_uLine.png"] forState:UIControlStateNormal];
    [btnAttr setBackgroundImage:nil forState:UIControlStateNormal];
    tblAttr.hidden = true;
    tblVideo.hidden = false;
}

- (IBAction)attrClicked:(id)sender {
    [btnAttr setBackgroundImage:[UIImage imageNamed:@"brown_uLine.png"] forState:UIControlStateNormal];
    [btnVideoTour setBackgroundImage:nil forState:UIControlStateNormal];

    tblAttr.hidden = false;
    tblVideo.hidden = true;
}

#pragma mark -- Video Segement Functions
-(void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.tag == 1) {
        leftDate = textField.text;
    } else if (textField.tag == 2) {
        rightDate = textField.text;
    }
}

-(void)checkedClickedVideo:(id)sender{
    if ([sender tag] == 0) {
        if (chSelectedVideo) {
            chSelectedVideo = false;
            if ([arrLanguageVideo containsObject:langChi]) {
                [arrLanguageVideo removeObject:langChi];
            }
        } else {
            chSelectedVideo = true;
            [arrLanguageVideo addObject:langChi];
        }
    } else if ([sender tag] == 1) {
        if (mnSelectedVideo) {
            mnSelectedVideo = false;
            if ([arrLanguageVideo containsObject:langMnd]) {
                [arrLanguageVideo removeObject:langMnd];
            }
        } else {
            mnSelectedVideo = true;
            [arrLanguageVideo addObject:langMnd];
        }
    } else if ([sender tag] == 2) {
        if (enSelectedVideo) {
            enSelectedVideo = false;
            if ([arrLanguageVideo containsObject:langEng]) {
                [arrLanguageVideo removeObject:langEng];
            }
        } else {
            enSelectedVideo = true;
            [arrLanguageVideo addObject:langEng];
        }
    }
    [self updateLanguageSelectedVideo:btnHeaderCheckVideo];
    [tblVideo reloadData];
    
}

-(void)updateLanguageSelectedVideo:(id)sender {
    if (chSelectedVideo && mnSelectedVideo && enSelectedVideo) {
        fullSelectedVideo = true;
    } else {
        fullSelectedVideo = false;
    }
    [sender setImage:[UIImage imageNamed:((fullSelectedVideo) ? @"square_s.png" : @"square_us.png")] forState:UIControlStateNormal];
}

-(void)languageSelectedVideo:(id)sender {
    
    if (fullSelectedVideo) {
        fullSelectedVideo = false;
        chSelectedVideo = false;
        mnSelectedVideo = false;
        enSelectedVideo = false;
        for (int i = 0; i<3; i++) {
            NSIndexPath * ip = [NSIndexPath indexPathForRow:i inSection:0];
            
            [tblVideo reloadRowsAtIndexPaths:[NSArray arrayWithObjects:ip, nil] withRowAnimation:UITableViewRowAnimationNone];
        }
        if ([arrLanguageVideo containsObject:langChi]) {
            [arrLanguageVideo removeObject:langChi];
        }
        if ([arrLanguageVideo containsObject:langMnd]) {
            [arrLanguageVideo removeObject:langMnd];
        }
        if ([arrLanguageVideo containsObject:langEng]) {
            [arrLanguageVideo removeObject:langEng];
        }
    } else {
        fullSelectedVideo = true;
        chSelectedVideo = true;
        mnSelectedVideo = true;
        enSelectedVideo = true;
        for (int i = 0; i<3; i++) {
            NSIndexPath * ip = [NSIndexPath indexPathForRow:i inSection:0];
            [tblVideo reloadRowsAtIndexPaths:[NSArray arrayWithObjects:ip, nil] withRowAnimation:UITableViewRowAnimationNone];
        }
        if (![arrLanguageVideo containsObject:langChi]) {
            [arrLanguageVideo addObject:langChi];
        }
        if (![arrLanguageVideo containsObject:langMnd]) {
            [arrLanguageVideo addObject:langMnd];
        }
        if (![arrLanguageVideo containsObject:langEng]) {
            [arrLanguageVideo addObject:langEng];
        }
    }
    [sender setImage:[UIImage imageNamed:((fullSelectedVideo) ? @"square_s.png" : @"square_us.png")] forState:UIControlStateNormal];
}

- (IBAction)yesClicked:(id)sender {
    [btnNo setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [btnYes setImage:[UIImage imageNamed:@"RadioButton-Selected.png"]
            forState:UIControlStateNormal];
    selfDrive = true;
}

- (IBAction)NoClicked:(id)sender {
    [btnYes setImage:[UIImage imageNamed:@"RadioButton-Unselected.png"] forState:UIControlStateNormal];
    [btnNo setImage:[UIImage imageNamed:@"RadioButton-Selected.png"] forState:UIControlStateNormal];
    selfDrive = false;
}

-(void)leftPlusClicked {
    int a = leftDate.intValue;
    a++;
    leftDate = [NSString stringWithFormat:@"%d",a];
}

-(void)leftMinusClicked {
    int a = leftDate.intValue;
    if (a > 1) {
        a--;
    }
    leftDate = [NSString stringWithFormat:@"%d",a];
}

-(void)rightPlusClicked {
    int a = rightDate.intValue;
    a++;
    rightDate = [NSString stringWithFormat:@"%d",a];
}

-(void)rightMinusClicked {
    int a = rightDate.intValue;
    if (a > 1) {
        a--;
    }
    rightDate = [NSString stringWithFormat:@"%d",a];
}


#pragma mark -- Attraction Segement Functions

-(void)checkedClickedAttr:(id)sender{
    
    if ([sender tag] == 0) {
        if (chSelectedAttr) {
            chSelectedAttr = false;
            if ([arrLanguageAttr containsObject:langChi]) {
                [arrLanguageAttr removeObject:langChi];
            }
        } else {
            chSelectedAttr = true;
            [arrLanguageAttr addObject:langChi];
        }
    } else if ([sender tag] == 1) {
        if (mnSelectedAttr) {
            mnSelectedAttr = false;
            if ([arrLanguageAttr containsObject:langMnd]) {
                [arrLanguageAttr removeObject:langMnd];
            }
        } else {
            mnSelectedAttr = true;
            [arrLanguageAttr addObject:langMnd];
        }
    } else if ([sender tag] == 2) {
        if (enSelectedAttr) {
            enSelectedAttr = false;
            if ([arrLanguageAttr containsObject:langEng]) {
                [arrLanguageAttr removeObject:langEng];
            }
        } else {
            enSelectedAttr = true;
            [arrLanguageAttr addObject:langEng];
        }
    }
    [self updateLanguageSelectedAttr:btnHeaderCheckAttr];
    [tblAttr reloadData];
}

-(void)updateLanguageSelectedAttr:(id)sender {
    if (chSelectedAttr && mnSelectedAttr && enSelectedAttr) {
        fullSelectedAttr = true;
    } else {
        fullSelectedAttr = false;
    }
    [sender setImage:[UIImage imageNamed:((fullSelectedAttr) ? @"square_s.png" : @"square_us.png")] forState:UIControlStateNormal];
}

-(void)languageSelectedAttr:(id)sender {
    if (fullSelectedAttr) {
        fullSelectedAttr = false;
        chSelectedAttr = false;
        mnSelectedAttr = false;
        enSelectedAttr = false;
        for (int i = 0; i<3; i++) {
            NSIndexPath * ip = [NSIndexPath indexPathForRow:i inSection:1];
            
            [tblAttr reloadRowsAtIndexPaths:[NSArray arrayWithObjects:ip, nil] withRowAnimation:UITableViewRowAnimationNone];
        }
        if ([arrLanguageAttr containsObject:langChi]) {
            [arrLanguageAttr removeObject:langChi];
        }
        if ([arrLanguageAttr containsObject:langMnd]) {
            [arrLanguageAttr removeObject:langMnd];
        }
        if ([arrLanguageAttr containsObject:langEng]) {
            [arrLanguageAttr removeObject:langEng];
        }
    } else {
        fullSelectedAttr = true;
        chSelectedAttr = true;
        mnSelectedAttr = true;
        enSelectedAttr = true;
        for (int i = 0; i<3; i++) {
            NSIndexPath * ip = [NSIndexPath indexPathForRow:i inSection:1];
            [tblAttr reloadRowsAtIndexPaths:[NSArray arrayWithObjects:ip, nil] withRowAnimation:UITableViewRowAnimationNone];
        }
        if (![arrLanguageAttr containsObject:langChi]) {
            [arrLanguageAttr addObject:langChi];
        }
        if (![arrLanguageAttr containsObject:langMnd]) {
            [arrLanguageAttr addObject:langMnd];
        }
        if (![arrLanguageAttr containsObject:langEng]) {
            [arrLanguageAttr addObject:langEng];
        }
    }
    [sender setImage:[UIImage imageNamed:((fullSelectedAttr) ? @"square_s.png" : @"square_us.png")] forState:UIControlStateNormal];
}

-(void)allLanSelectedVideo:(id)sender{
    [btnHeaderCheckVideo sendActionsForControlEvents:UIControlEventTouchUpInside];
}

-(void)allLanSelectedAttr:(id)sender{
    [btnHeaderCheckAttr sendActionsForControlEvents:UIControlEventTouchUpInside];
}

-(void)customSearchClickedVideo:(id)sender {
    UIButton * btn = sender;
    self.searchBar.text = btn.titleLabel.text;
    [self searchVideoClicked];
    }

-(void)customSearchClickedAttr:(id)sender {
    UIButton * btn = sender;
    self.searchBar.text = btn.titleLabel.text;
    [self searchAttrClicked];
}


#pragma mark -- TablerView Datasource and Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3; // Arun :: for both table
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    if (tableView == tblVideo) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(5, 5, 200, 50)];
        UILabel *lblText = [[UILabel alloc] initWithFrame:CGRectMake(8,4, 200, 20)];
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,1)];
        line.backgroundColor = [UIColor lightGrayColor];
        UIButton * btnEmpty = [[UIButton alloc] initWithFrame:CGRectMake(0, 0,320, 50)];
        [btnEmpty setTitle:@"" forState:UIControlStateNormal];
        [btnEmpty addTarget:self action:@selector(allLanSelectedVideo:)forControlEvents:UIControlEventTouchUpInside];
        if (section == 0) {
            
            [lblText setText:LOCALIZATION(@"language")];
            //lblText.text = [NSString stringWithFormat:NSLocalizedString(@"language", nil)];
            
            btnHeaderCheckVideo = [UIButton buttonWithType:UIButtonTypeCustom];
            
            btnHeaderCheckVideo.frame = CGRectMake(self.view.frame.size.width-30, 4, 15, 15);//(292, 0, 20, 20)
            [btnHeaderCheckVideo addTarget:self action:@selector(languageSelectedVideo:)forControlEvents:UIControlEventTouchUpInside];
            
            if(fullSelectedVideo){
                [btnHeaderCheckVideo setImage:[UIImage imageNamed:@"square_s.png"] forState:UIControlStateNormal];
            }else{
                [btnHeaderCheckVideo setImage:[UIImage imageNamed:@"square_us.png"] forState:UIControlStateNormal];
            }
            [view addSubview:btnHeaderCheckVideo];
            [view addSubview:btnEmpty];
            
        }else if (section == 1){
            lblText = [[UILabel alloc] initWithFrame:CGRectMake(8, 10, 200, 20)];
            [lblText setText:LOCALIZATION(@"day")];
            //lblText.text = [NSString stringWithFormat:NSLocalizedString(@"day", nil)];
            [view addSubview:line];
            
        }else if (section == 2){
            [lblText setText:LOCALIZATION(@"most_popular_search")];
            //lblText.text = [NSString stringWithFormat:NSLocalizedString(@"most_popular_search", nil)];
            [view addSubview:line];
        }
        [view addSubview:lblText];
        [view setBackgroundColor:[UIColor whiteColor]];
        return view;
    } else {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(5, 5, 200, 25)];
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width,1)];
        line.backgroundColor = [UIColor lightGrayColor];
        
        UIButton * btnEmpty = [[UIButton alloc] initWithFrame:CGRectMake(0, 0,320, 50)];
        [btnEmpty setTitle:@"" forState:UIControlStateNormal];
        [btnEmpty addTarget:self action:@selector(allLanSelectedAttr:)forControlEvents:UIControlEventTouchUpInside];
        
        UILabel *lblText = [[UILabel alloc] initWithFrame:CGRectMake(8, 5, 200, 25)];
        if (section == 0) {
            [lblText setText:LOCALIZATION(@"what_to_do")];
            //lblText.text = [NSString stringWithFormat:NSLocalizedString(@"what_to_do", nil)];
        }else if (section == 1){
            [lblText setText:LOCALIZATION(@"language")];
            //lblText.text = [NSString stringWithFormat:NSLocalizedString(@"language", nil)];
            btnHeaderCheckAttr = [UIButton buttonWithType:UIButtonTypeCustom];
            
            if(fullSelectedAttr){
                [btnHeaderCheckAttr setImage:[UIImage imageNamed:@"square_s.png"] forState:UIControlStateNormal];
            } else {
                [btnHeaderCheckAttr setImage:[UIImage imageNamed:@"square_us.png"] forState:UIControlStateNormal];
            }
            
            
            btnHeaderCheckAttr.frame = CGRectMake(self.view.frame.size.width-30, 8, 15, 15);//(292, 0, 20, 20)
            [btnHeaderCheckAttr addTarget:self action:@selector(languageSelectedAttr:)forControlEvents:UIControlEventTouchUpInside];
            [view addSubview:btnHeaderCheckAttr];
            [view addSubview:btnEmpty];
            [view addSubview:line];
        }else if (section == 2){
            lblText.text = @"Most Popular Search";
            [view addSubview:line];
        }
        [view addSubview:lblText];
        [view setBackgroundColor:[UIColor whiteColor]];
        return view;
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if (tableView == tblVideo) {
        if (section == 1) {
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,17, tblVideo.frame.size.width, 120)];
            [view addSubview:selfDriveView];
            UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 5, self.view.frame.size.width,1)];
            line.backgroundColor = [UIColor lightGrayColor];
            [view addSubview:line];
            
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(selfDriveView.frame.size.height-25, selfDriveView.frame.size.height+10, tblVideo.frame.size.width-50, 30)];
            
            [btn setTitle:LOCALIZATION(@"search") forState:UIControlStateNormal];
            [btn setBackgroundColor:[ColorConstants appYellowColor]];
            btn.layer.cornerRadius = 18;
            btn.clipsToBounds = YES;
            [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            [view addSubview:btn];
            [btn addTarget:self action:@selector(searchVideoClicked) forControlEvents:UIControlEventTouchUpInside];
            [view setBackgroundColor:[UIColor whiteColor]];
            return view;
        }
        return [[UIView alloc] initWithFrame:CGRectMake(0, 0, 1, 1)];
    } else {
        if (section == 1) {
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tblAttr.frame.size.width, 40)];
            UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(30, 10, tblAttr.frame.size.width-50, 30)];
            [btn setTitle:LOCALIZATION(@"search") forState:UIControlStateNormal];
            [btn setBackgroundColor:[ColorConstants appYellowColor]];
            btn.layer.cornerRadius = 18;
            btn.clipsToBounds = YES;
            [btn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
            [btn addTarget:self action:@selector(searchAttrClicked) forControlEvents:UIControlEventTouchUpInside];
            view.backgroundColor = [UIColor whiteColor];
            [view addSubview:btn];
            return view;
        }
        return [[UIView alloc] initWithFrame:CGRectZero];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == tblVideo) {
        if (section == 0) {
            return 3;
        }else if (section == 1){
            return 1;
        }else{
            if (arrMostSearchVideo.count > 0) {
                return 1;
            } else {
                return 0;
            }
        }
    } else {
        if (section == 0) {
            return categoryListAttr.count;
        }else if (section == 1){
            return 3;
        }else{
            if (arrMostSearchAttr.count > 0) {
                return 1;
            } else {
                return 0;
            };
        }
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == tblVideo) {
        if (indexPath.section == 0) {
            LanguageTableViewCell *cell = (LanguageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"LanguageTableViewCell" forIndexPath:indexPath];
            if (indexPath.row == 0) {
                cell.lblLanguage.text = @"廣東話";
                if (!chSelectedVideo) {
                    [cell.btnCheck setImage:[UIImage imageNamed:@"square_us.png"] forState:UIControlStateNormal];
                } else {
                    [cell.btnCheck setImage:[UIImage imageNamed:@"square_s.png"] forState:UIControlStateNormal];
                }
            } else if (indexPath.row == 1) {
                cell.lblLanguage.text = @"普通話";
                if (!mnSelectedVideo) {
                    [cell.btnCheck setImage:[UIImage imageNamed:@"square_us.png"] forState:UIControlStateNormal];
                } else {
                    [cell.btnCheck setImage:[UIImage imageNamed:@"square_s.png"] forState:UIControlStateNormal];
                }
            } else if (indexPath.row == 2) {
                cell.lblLanguage.text = @"English";
                if (!enSelectedVideo) {
                    [cell.btnCheck setImage:[UIImage imageNamed:@"square_us.png"] forState:UIControlStateNormal];
                } else {
                    [cell.btnCheck setImage:[UIImage imageNamed:@"square_s.png"] forState:UIControlStateNormal];
                }
                cell.lblLine.hidden = true;
            }
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.btnCheck.tag = indexPath.row;
            
            [cell.btnCheck addTarget:self action:@selector(checkedClickedVideo:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        }else if (indexPath.section == 1){
            DayTableViewCell *cell = (DayTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"DayTableViewCell" forIndexPath:indexPath];
            
            cell.txtLeftDigit.tag = 1;
            cell.txtRightDigit.tag = 2;
            
            rightDate = cell.txtRightDigit.text;
            leftDate = cell.txtLeftDigit.text;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            //cell.lblTo.text = [NSString stringWithFormat:NSLocalizedString(@"to", nil)];
            [cell.lblTo setText:LOCALIZATION(@"to")];
            
            [cell.btnLeftPlus addTarget:self action:@selector(leftPlusClicked) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnLeftMinus addTarget:self action:@selector(leftMinusClicked) forControlEvents:UIControlEventTouchUpInside];
            
            [cell.btnRightPlus addTarget:self action:@selector(rightPlusClicked) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnRightMinus addTarget:self action:@selector(rightMinusClicked) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        } else {
            PopularTableViewCell *cell = (PopularTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"PopularTableViewCell" forIndexPath:indexPath];
            UIFont *font = [UIFont systemFontOfSize:17.0];
            
            if (arrMostSearchVideo.count >= 3) {
                MostPopularSearchVideoTour * aMost = [arrMostSearchVideo objectAtIndex:0];
                MostPopularSearchVideoTour * bMost = [arrMostSearchVideo objectAtIndex:1];
                MostPopularSearchVideoTour * cMost = [arrMostSearchVideo objectAtIndex:2];
                
                [cell.btn1 setTitle:aMost.search_str forState:UIControlStateNormal];
                [cell.btn2 setTitle:bMost.search_str forState:UIControlStateNormal];
                [cell.btn3 setTitle:cMost.search_str forState:UIControlStateNormal];
                
               
                CGFloat strWidth1 = [Utils widthOfString:aMost.search_str withFont:font];
                CGFloat strWidth2 = [Utils widthOfString:bMost.search_str withFont:font];
                CGFloat strWidth3 = [Utils widthOfString:cMost.search_str withFont:font];
                
                cell.btn1.frame = CGRectMake(cell.btn1.frame.origin.x, cell.btn1.frame.origin.y, strWidth1+10, cell.btn1.frame.size.height);
                cell.btn2.frame = CGRectMake(cell.btn1.frame.origin.x + cell.btn1.frame.size.width +10 , cell.btn2.frame.origin.y, strWidth2+10, cell.btn2.frame.size.height);
                cell.btn3.frame = CGRectMake(cell.btn2.frame.origin.x + cell.btn2.frame.size.width +10 , cell.btn2.frame.origin.y, strWidth3+10, cell.btn2.frame.size.height);
                
            } else if (arrMostSearchVideo.count == 2) {
                MostPopularSearchVideoTour * aMost = [arrMostSearchVideo objectAtIndex:0];
                MostPopularSearchVideoTour * bMost = [arrMostSearchVideo objectAtIndex:1];
                [cell.btn1 setTitle:aMost.search_str forState:UIControlStateNormal];
                [cell.btn2 setTitle:bMost.search_str forState:UIControlStateNormal];
                CGFloat strWidth1 = [Utils widthOfString:aMost.search_str withFont:font];
                CGFloat strWidth2 = [Utils widthOfString:bMost.search_str withFont:font];
                
                cell.btn1.frame = CGRectMake(cell.btn1.frame.origin.x, cell.btn1.frame.origin.y, strWidth1+10, cell.btn1.frame.size.height);
                cell.btn2.frame = CGRectMake(cell.btn1.frame.origin.x + cell.btn1.frame.size.width +10 , cell.btn2.frame.origin.y, strWidth2+10, cell.btn2.frame.size.height);

            } else if (arrMostSearchVideo.count == 1) {
                MostPopularSearchVideoTour * aMost = [arrMostSearchVideo objectAtIndex:0];
                [cell.btn1 setTitle:aMost.search_str forState:UIControlStateNormal];
                
                CGFloat strWidth1 = [Utils widthOfString:aMost.search_str withFont:font];
                cell.btn1.frame = CGRectMake(cell.btn1.frame.origin.x, cell.btn1.frame.origin.y, strWidth1+10, cell.btn1.frame.size.height);
            }
           
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            cell.btn1.layer.cornerRadius = 5;
            cell.btn1.layer.borderWidth = 1;
            cell.btn1.layer.borderColor = [ColorConstants appBrownColor].CGColor;
            
            cell.btn2.layer.cornerRadius = 5;
            cell.btn2.layer.borderWidth = 1;
            cell.btn2.layer.borderColor = [ColorConstants appBrownColor].CGColor;
            
            cell.btn3.layer.cornerRadius = 5;
            cell.btn3.layer.borderWidth = 1;
            cell.btn3.layer.borderColor = [ColorConstants appBrownColor].CGColor;
            
            [cell.btn1 addTarget:self action:@selector(customSearchClickedVideo:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btn2 addTarget:self action:@selector(customSearchClickedVideo:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btn3 addTarget:self action:@selector(customSearchClickedVideo:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        }
    } else {
        if (indexPath.section == 1) {
            LanguageTableViewCell *cell = (LanguageTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"LanguageTableViewCell" forIndexPath:indexPath];
            if (indexPath.row == 0) {
                cell.lblLanguage.text = @"Chineese";
                if (!chSelectedAttr) {
                    [cell.btnCheck setImage:[UIImage imageNamed:@"square_us.png"] forState:UIControlStateNormal];
                } else {
                    [cell.btnCheck setImage:[UIImage imageNamed:@"square_s.png"] forState:UIControlStateNormal];
                }
            } else if (indexPath.row == 1) {
                cell.lblLanguage.text = @"Mandarin";
                if (!mnSelectedAttr) {
                    [cell.btnCheck setImage:[UIImage imageNamed:@"square_us.png"] forState:UIControlStateNormal];
                } else {
                    [cell.btnCheck setImage:[UIImage imageNamed:@"square_s.png"] forState:UIControlStateNormal];
                }
            } else if (indexPath.row == 2) {
                cell.lblLanguage.text = @"English";
                if (!enSelectedAttr) {
                    [cell.btnCheck setImage:[UIImage imageNamed:@"square_us.png"] forState:UIControlStateNormal];
                } else {
                    [cell.btnCheck setImage:[UIImage imageNamed:@"square_s.png"] forState:UIControlStateNormal];
                }
                cell.lblLine.hidden = true;
            }
            cell.btnCheck.tag = indexPath.row;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell.btnCheck addTarget:self action:@selector(checkedClickedAttr:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        }
        else if (indexPath.section == 0) {
            WhatToDoTableViewCell *cell = (WhatToDoTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"WhatToDoTableViewCell" forIndexPath:indexPath];
            Categori * cat = [categoryListAttr objectAtIndex: indexPath.row];
            
            
            Account * account = [AccountManager Instance].activeAccount;
            
            
            
            
            
            CategoryModel *selectedCategoryFound;
            for (CategoryModel *category in categoryArrayWithLanguages) {
                if ([cat.entity_id isEqualToString:category._id]) {
                    selectedCategoryFound = category;
                    break;
                }
            }
            if (selectedCategoryFound) {
                if([account.language  isEqualToString: @"sc"]){
                    cell.lblText.text = selectedCategoryFound.name_sc;
                    
                    
                }else if([account.language  isEqualToString: @"tc"]){
                    cell.lblText.text = selectedCategoryFound.name_tc;
                }
                else{
                    cell.lblText.text = selectedCategoryFound.name;
                }
            }
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,cat.image];

            
            [cell.imgIcon sd_setShowActivityIndicatorView:YES];
            [cell.imgIcon sd_setIndicatorStyle:UIActivityIndicatorViewStyleGray];
            [cell.imgIcon sd_setImageWithURL:[NSURL URLWithString:selectedCategoryFound.image]
                            placeholderImage:[UIImage imageNamed:@"="] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                                if (!error) {
                                    cell.imgIcon.image = image;
                                    [cell layoutIfNeeded];
                                }
                            }];
            
            
            //cell.imgIcon.url = [NSURL URLWithString:urlString];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            if (indexPath.row == categoryListAttr.count-1) {
                cell.lblLine.hidden = true;
            } else {
                cell.lblLine.hidden = false;
            }
            return cell;
        }
        else{
            PopularTableViewCell *cell = (PopularTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"PopularTableViewCell" forIndexPath:indexPath];
            UIFont *font = [UIFont systemFontOfSize:17.0];

            if (arrMostSearchAttr.count >= 3) {
                MostPopularSearchAttraction * aMost = [arrMostSearchAttr objectAtIndex:0];
                MostPopularSearchAttraction * bMost = [arrMostSearchAttr objectAtIndex:1];
                MostPopularSearchAttraction * cMost = [arrMostSearchAttr objectAtIndex:2];
                [cell.btn1 setTitle:aMost.search_str forState:UIControlStateNormal];
                [cell.btn2 setTitle:bMost.search_str forState:UIControlStateNormal];
                [cell.btn3 setTitle:cMost.search_str forState:UIControlStateNormal];
                CGFloat strWidth1 = [Utils widthOfString:aMost.search_str withFont:font];
                CGFloat strWidth2 = [Utils widthOfString:bMost.search_str withFont:font];
                CGFloat strWidth3 = [Utils widthOfString:cMost.search_str withFont:font];
                cell.btn1.frame = CGRectMake(cell.btn1.frame.origin.x, cell.btn1.frame.origin.y, strWidth1+10, cell.btn1.frame.size.height);
                cell.btn2.frame = CGRectMake(cell.btn1.frame.origin.x + cell.btn1.frame.size.width +10 , cell.btn2.frame.origin.y, strWidth2+10, cell.btn2.frame.size.height);
                cell.btn3.frame = CGRectMake(cell.btn2.frame.origin.x + cell.btn2.frame.size.width +10 , cell.btn2.frame.origin.y, strWidth3+10, cell.btn2.frame.size.height);
            }
            else if (arrMostSearchAttr.count == 2) {
                MostPopularSearchAttraction * aMost = [arrMostSearchAttr objectAtIndex:0];
                MostPopularSearchAttraction * bMost = [arrMostSearchAttr objectAtIndex:1];
                [cell.btn1 setTitle:aMost.search_str forState:UIControlStateNormal];
                [cell.btn2 setTitle:bMost.search_str forState:UIControlStateNormal];
                CGFloat strWidth1 = [Utils widthOfString:aMost.search_str withFont:font];
                CGFloat strWidth2 = [Utils widthOfString:bMost.search_str withFont:font];
                cell.btn1.frame = CGRectMake(cell.btn1.frame.origin.x, cell.btn1.frame.origin.y, strWidth1+10, cell.btn1.frame.size.height);
                cell.btn2.frame = CGRectMake(cell.btn1.frame.origin.x + cell.btn1.frame.size.width +10 , cell.btn2.frame.origin.y, strWidth2+10, cell.btn2.frame.size.height);
            }
            else if (arrMostSearchAttr.count == 1) {
                MostPopularSearchAttraction * aMost = [arrMostSearchAttr objectAtIndex:0];
                [cell.btn1 setTitle:aMost.search_str forState:UIControlStateNormal];
                CGFloat strWidth1 = [Utils widthOfString:aMost.search_str withFont:font];
                cell.btn1.frame = CGRectMake(cell.btn1.frame.origin.x, cell.btn1.frame.origin.y, strWidth1+10, cell.btn1.frame.size.height);
            }
            
            cell.btn1.layer.cornerRadius = 5;
            cell.btn1.layer.borderWidth = 1;
            cell.btn1.layer.borderColor = [ColorConstants appBrownColor].CGColor;
            
            cell.btn2.layer.cornerRadius = 5;
            cell.btn2.layer.borderWidth = 1;
            cell.btn2.layer.borderColor = [ColorConstants appBrownColor].CGColor;
            
            cell.btn3.layer.cornerRadius = 5;
            cell.btn3.layer.borderWidth = 1;
            cell.btn3.layer.borderColor = [ColorConstants appBrownColor].CGColor;
            
            [cell.btn1 addTarget:self action:@selector(customSearchClickedAttr:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btn2 addTarget:self action:@selector(customSearchClickedAttr:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btn3 addTarget:self action:@selector(customSearchClickedAttr:) forControlEvents:UIControlEventTouchUpInside];
            
            return cell;
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == tblVideo) {
        if (indexPath.section == 0) {
            LanguageTableViewCell *cell = (LanguageTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
            [self checkedClickedVideo:cell.btnCheck];
        }
    } else {
        if (indexPath.section == 0) {
            WhatToDoTableViewCell *cell = (WhatToDoTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
            Categori * cat = [categoryListAttr objectAtIndex: indexPath.row];
            [arrCategoryAttr addObject:cat.entity_id];
            cell.imgCheck.image = [UIImage imageNamed:@"square_s.png"];
        } else if (indexPath.section == 1) {
            LanguageTableViewCell *cell = (LanguageTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
            [self checkedClickedAttr:cell.btnCheck];
        }
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == tblVideo) {
        if (indexPath.section == 0) {
            LanguageTableViewCell *cell = (LanguageTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
            [self checkedClickedVideo:cell.btnCheck];
        }
    } else {
        if (indexPath.section == 0) {
            WhatToDoTableViewCell *cell = (WhatToDoTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
            Categori * cat = [categoryListAttr objectAtIndex: indexPath.row];
            if ([arrCategoryAttr containsObject:cat.entity_id]) {
                [arrCategoryAttr removeObject:cat.entity_id];
            }
            cell.imgCheck.image = [UIImage imageNamed:@"square_us.png"];
        } else if (indexPath.section == 1) {
            LanguageTableViewCell *cell = (LanguageTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
            [self checkedClickedAttr:cell.btnCheck];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (tableView == tblVideo) {
        if (section == 1) {
            return 120;
        }
        return CGFLOAT_MIN;
    } else {
        if (section == 1) {
            return 60;
        }
        return CGFLOAT_MIN;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 35;
}

#pragma End of table view delegate and datasource

-(void)searchVideoClicked {
    
    NSIndexPath * ip = [NSIndexPath indexPathForRow:0 inSection:1];
    DayTableViewCell *cell = (DayTableViewCell*)[tblVideo cellForRowAtIndexPath:ip];
                                                 
    NSString * sDate = cell.txtLeftDigit.text;
    NSString * eDate = cell.txtRightDigit.text;
    
    if (sDate.intValue > eDate.intValue) {
        [Utils showAlertWithMessage:@"Set to date higher than from date"];
        return;
    }
    if (sDate.length <= 0 || eDate.length <= 0) {
        [Utils showAlertWithMessage:@"Set proper days"];
        return;
    }
    
    NSString *daysString = [NSString stringWithFormat:@"%i",sDate.intValue];
    
    for (int i = sDate.intValue + 1; i <= eDate.intValue ; i++) {
        daysString = [NSString stringWithFormat:@"%@,%i",daysString,i];
    }
    
    
    NSString *selfDriveString = @"false";
    if (selfDrive) {
        selfDriveString = @"true";
    }
   
    
    NSLog(@"%lu",(unsigned long)arrLanguageVideo.count);
    
    NSDictionary *parameters;
    
    if (self.searchBar.text.length) {
        if (arrLanguageVideo.count) {
            NSString *languageString = [NSString stringWithFormat:@"%@",arrLanguageVideo[0]];
            for (int i = 1; i < arrLanguageVideo.count ; i++) {
                languageString = [NSString stringWithFormat:@"%@,%@",languageString,arrLanguageVideo[i]];
            }
            parameters = @{ @"where": @{ @"language": languageString,
                                         @"name": self.searchBar.text,
                                         @"self_drive": selfDriveString,
                                         @"days": daysString } };
            
        }
        else{
            parameters = @{ @"where": @{ @"name": self.searchBar.text,
                                         @"self_drive": selfDriveString,
                                         @"days": daysString } };
        }
    }
    else{
        if (arrLanguageVideo.count) {
            NSString *languageString = [NSString stringWithFormat:@"%@",arrLanguageVideo[0]];
            for (int i = 1; i < arrLanguageVideo.count ; i++) {
                languageString = [NSString stringWithFormat:@"%@,%@",languageString,arrLanguageVideo[i]];
            }
            parameters = @{ @"where": @{ @"name": self.searchBar.text,
                                         @"self_drive": selfDriveString,
                                         @"days": daysString } };
            
        }
        else{
            parameters = @{ @"where": @{ @"self_drive": selfDriveString,
                                         @"days": daysString } };
        }
    }
    
    seachedTours = [[NSMutableArray alloc] init];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Searching Video Tours..."];
  
    
    
    [[PlayTripManager Instance] getLatestVideoTourWithParameters:parameters WithBlockNew:^(id result, NSString *error) {
        [SVProgressHUD dismiss];
        if (!error) {
            NSError* errorData;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                 options:kNilOptions
                                                                   error:&errorData];
            if (errorData) {
                NSLog(@"ERROR GOTTEN = %@" ,errorData.description);
            }
            NSArray* plansDicts = [json objectForKey:@"data"];
            seachedTours = [[NSMutableArray alloc] init];
            for (NSDictionary *planDict in plansDicts) {
                NSError *error;
                PlanModel *planModel = [[PlanModel alloc] initWithDictionary:planDict error:&error];
                if (error) {
                    NSLog(@"ERROR GOTTEN AT JSONMODEL = %@" ,error.description);
                }
                if (![seachedTours containsObject:planModel]) {
                    [seachedTours addObject:planModel];
                }
            }
            NSLog(@"Total Plans Gotten = %lu" ,(unsigned long)seachedTours.count);
            //allLatestPlans = [[[allLatestPlans reverseObjectEnumerator] allObjects] mutableCopy];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //[collLatestView reloadData];
                
                if (seachedTours.count > 0) {
                   
                    
                    NSString * searchStr = self.searchBar.text;
                    
                    NSIndexPath * ip = [NSIndexPath indexPathForRow:0 inSection:1];
                    DayTableViewCell *cell = (DayTableViewCell*)[tblVideo cellForRowAtIndexPath:ip];
                    
                    NSString * sDate = cell.txtLeftDigit.text;
                    NSString * eDate = cell.txtRightDigit.text;
                    
                    if (searchStr.length > 0) {
                        SearchResultViewController * controller = [SearchResultViewController initViewControllerWithSearchResultsArray:seachedTours WithSearchText:searchStr andLanguageArray:arrLanguageVideo andStartDate:sDate andEndDate:eDate withSelfDrive:selfDrive];
                        if (!alreadyPushed) {
                            alreadyPushed = true;
                            [self.navigationController pushViewController:controller animated:true];
                        }
                        
                    }
                    else{
                        SearchResultViewController * controller = [SearchResultViewController initViewControllerWithSearchResultsArray:seachedTours WithSearchText:nil andLanguageArray:arrLanguageVideo andStartDate:sDate andEndDate:eDate withSelfDrive:selfDrive];
                        if (!alreadyPushed) {
                            alreadyPushed = true;
                            [self.navigationController pushViewController:controller animated:true];
                        }
                    }
                    
                } else {
                    [Utils showAlertWithMessage:@"No data found"];
                }    
            });
        }
    }];
    
    
    
/*NSDictionary *parameters = @{ @"where": @{ @"language": @"en", @"name": @"June15", @"self_drive": @"false", @"days": @"1" } };
    
    
 
    
    
    /*
    
    [[PlayTripManager Instance] searchVideoTourWithName:self.searchBar.text andLanguageArray:arrLanguageVideo andStartDate:sDate andEndDate:eDate andSelfDrive:selfDrive WithBlock:^(id result, NSString *error) {
        if (error) {
            [Utils showAlertWithMessage:@"No data found"];
            [SVProgressHUD dismiss];
        } else {
            [SVProgressHUD dismiss];
            videoTourArray = [VideoTour getAll];
            if (videoTourArray.count > 0) {
                [self goToResultViewVideo];
            } else {
                [Utils showAlertWithMessage:@"No data found"];
            }
            if (self.searchBar.text.length > 0) {
                [[PlayTripManager Instance] saveSearchDataForModel:@"video_tour" WithString:self.searchBar.text WithBlock:nil];
            }
        }
    }];
     */
}

-(void)searchAttrClicked {

    NSDictionary *parameters;
    
    if (self.searchBar.text.length) {
        if (arrLanguageAttr.count) {
            NSString *languageString = [NSString stringWithFormat:@"%@",arrLanguageVideo[0]];
            for (int i = 1; i < arrLanguageAttr.count ; i++) {
                languageString = [NSString stringWithFormat:@"%@,%@",languageString,arrLanguageAttr[i]];
            }
            
            if (categoryListAttr.count) {
                NSString *categoryStr = arrCategoryAttr[0];
                for (int i = 1; i < arrCategoryAttr.count ; i++) {
                    categoryStr = [NSString stringWithFormat:@"%@,%@",categoryStr,arrCategoryAttr[i]];
                }
                                         
                parameters = @{ @"where": @{ @"language": languageString,
                                             @"name": self.searchBar.text,
                                             @"category": categoryStr } };
            }
            else{
                parameters = @{ @"where": @{ @"language": languageString,
                                             @"name": self.searchBar.text } };
            }
            
            
        }
        else{
            if (categoryListAttr.count) {
                NSString *categoryStr = arrCategoryAttr[0];
                for (int i = 1; i < arrCategoryAttr.count ; i++) {
                    categoryStr = [NSString stringWithFormat:@"%@,%@",categoryStr,arrCategoryAttr[i]];
                }
                parameters = @{ @"where": @{ @"name": self.searchBar.text,
                                             @"category": categoryStr } };
            }
            else{
                parameters = @{ @"where": @{ @"name": self.searchBar.text} };
            }
            
        }
    }
    else{
        if (arrLanguageAttr.count) {
            NSString *languageString = [NSString stringWithFormat:@"%@",arrLanguageAttr[0]];
            for (int i = 1; i < arrLanguageAttr.count ; i++) {
                languageString = [NSString stringWithFormat:@"%@,%@",languageString,arrLanguageAttr[i]];
            }
            if (arrCategoryAttr.count) {
                NSString *categoryStr = arrCategoryAttr[0];
                for (int i = 1; i < arrCategoryAttr.count ; i++) {
                    categoryStr = [NSString stringWithFormat:@"%@,%@",categoryStr,arrCategoryAttr[i]];
                }
                
                parameters = @{ @"where": @{ @"language": languageString,
                                             @"category": categoryStr } };
            }
            else{
                parameters = @{ @"where": @{ @"language": languageString} };
            }
            
            
            
        }
        else{
            if (arrCategoryAttr.count) {
                NSString *categoryStr = arrCategoryAttr[0];
                for (int i = 1; i < arrCategoryAttr.count ; i++) {
                    categoryStr = [NSString stringWithFormat:@"%@,%@",categoryStr,arrCategoryAttr[i]];
                }
                parameters = @{ @"where": @{ @"category": categoryStr } };
            }
            else{
                parameters = @{ @"where": @{  } };

            }
        }
    }
    
    
    
    seachedAttractions = [[NSMutableArray alloc] init];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Searching Attractions..."];
    
    
    
    [[PlayTripManager Instance] searchAttractionWithDictionary:parameters WithBlock:^(id result, NSString *error) {
        
        [SVProgressHUD dismiss];
        if (!error) {
            NSError* errorData;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                 options:kNilOptions
                                                                   error:&errorData];
            if (errorData) {
                NSLog(@"ERROR GOTTEN = %@" ,errorData.description);
            }
            NSArray* attractionDicts = [json objectForKey:@"data"];
            seachedAttractions = [[NSMutableArray alloc] init];
            for (NSDictionary *attractionDict in attractionDicts) {
                NSError *error;
                AttractionModel *attractionModel = [[AttractionModel alloc] initWithDictionary:attractionDict error:&error];
                if (error) {
                    NSLog(@"ERROR GOTTEN AT JSONMODEL = %@" ,error.description);
                }
                if (![seachedAttractions containsObject:attractionModel]) {
                    [seachedAttractions addObject:attractionModel];
                }
            }
            NSLog(@"Total Attractions Gotten Gotten = %lu" ,(unsigned long)seachedAttractions.count);
            //allLatestPlans = [[[allLatestPlans reverseObjectEnumerator] allObjects] mutableCopy];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //[collLatestView reloadData];
                
                if (seachedAttractions.count > 0) {
                    [self goToResultViewAttr];
                } else {
                    [Utils showAlertWithMessage:@"No data found"];
                }
            });
        }
    }];
    
    
    
    
    
    
    
    //    //NSDictionary *parameters = @{ @"where": @{ @"name": @"", @"category": @"58d136a72e0fbe7e5867e363" } };

    
    /*
    [[PlayTripManager Instance] searchAttractionWithCatArray:arrCategoryAttr andLanguageArray:arrLanguageAttr andSearchStr:self.searchBar.text WithBlock:^(id result, NSString *error) {
        if (error) {
            [SVProgressHUD dismiss];
           } else {
            NSMutableArray * attrArray = [AttractionData getAll];
            if (attrArray.count > 0) {
                [self goToResultViewAttr];
            } else {
                [Utils showAlertWithMessage:@"No data found"];
            }
               
            NSMutableArray * attrArray1 = [Attractions getAll];
               for (Attractions * atr in attrArray1) {
                   //NSLog(@"%@", atr);
               }
            [SVProgressHUD dismiss];
        }
    }];
     */
}

-(void)goToResultViewVideo {
    NSString * searchStr = self.searchBar.text;
    
    NSIndexPath * ip = [NSIndexPath indexPathForRow:0 inSection:1];
    DayTableViewCell *cell = (DayTableViewCell*)[tblVideo cellForRowAtIndexPath:ip];
    
    NSString * sDate = cell.txtLeftDigit.text;
    NSString * eDate = cell.txtRightDigit.text;
    
    if (searchStr.length > 0) {
        SearchResultViewController * controller = [SearchResultViewController initViewControllerWithSearchText:searchStr andLanguageArray:arrLanguageVideo andStartDate:sDate andEndDate:eDate withSelfDrive:selfDrive];
        if (!alreadyPushed) {
            alreadyPushed = true;
            [self.navigationController pushViewController:controller animated:true];
        }
    } else {
        SearchResultViewController * controller = [SearchResultViewController initViewControllerWithSearchText:nil andLanguageArray:arrLanguageVideo andStartDate:sDate andEndDate:eDate withSelfDrive:selfDrive];
        if (!alreadyPushed) {
            alreadyPushed = true;
            [self.navigationController pushViewController:controller animated:true];
        }
        
    }
    
    
}

-(void)goToResultViewAttr {
    NSString * searchStr = self.searchBar.text;
    
    if (searchStr.length > 0) {
        SearchResultViewController * controller = [SearchResultViewController initViewControllerWithSearchText:searchStr andLangArray:arrLanguageAttr andCatArray:arrCategoryAttr andSearchResult:seachedAttractions];
        
        [self.navigationController pushViewController:controller animated:true];
    } else {
        SearchResultViewController * controller = [SearchResultViewController initViewControllerWithSearchText:nil andLangArray:arrLanguageAttr andCatArray:arrCategoryAttr andSearchResult:seachedAttractions];
        
        [self.navigationController pushViewController:controller animated:true];
    }
   
}

-(void)homeClicked {
    [self.navigationController popToRootViewControllerAnimated:true];
}

#pragma Searchbar Delegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)sBar {
    [sBar setShowsCancelButton:YES animated:YES];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)sBar{
    [sBar resignFirstResponder];
    [self searchVideoClicked];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)sBar{
    [sBar resignFirstResponder];
    [sBar setShowsCancelButton:NO animated:YES];
    sBar.text = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end

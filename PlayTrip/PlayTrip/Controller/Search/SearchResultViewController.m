
#import "SearchResultViewController.h"
#import "ItineraryViewController.h"
#import "VideoSortTableViewCell.h"
#import "PlayTripManager.h"
#import "LatestVideoTourCollectionViewCell.h"
#import "TagsCollectionCell.h"
#import "VideoTour.h"
#import "Utils.h"
#import "AttractionData.h"
#import "Account.h"
#import "AccountManager.h"
#import "PlayTripManager.h"
#import "CommonUser.h"
#import "URLSchema.h"
#import "SVProgressHUD.h"
#import "Attractions.h"
#import "AttractionData.h"
#import "Categori.h"
#import "ColorConstants.h"
#import "AttractionsVideoCell.h"
#import "PlaceInformationViewController.h"
#import "Info.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>


#define langChi  @"ch"
#define langMnd  @"md"
#define langEng  @"en"

@interface SearchResultViewController ()
@end

@implementation SearchResultViewController

+(SearchResultViewController *)initViewControllerWithSearchText:(NSString *)searchText andLanguageArray:(NSMutableArray *)langArray andStartDate:(NSString *)sDate andEndDate:(NSString *)eDate withSelfDrive:(BOOL)selfDrive {
    SearchResultViewController * controller = [[SearchResultViewController alloc] initWithNibName:@"SearchResultViewController" bundle:nil];
    controller->arrLanguageVideo = langArray;
    controller->sDate = sDate;
    controller->eDate = eDate;
    controller->searchTextVideo = searchText;
    controller->selfDrive = selfDrive;
    controller->isVideoSearched = true;
    return controller;
}

+(SearchResultViewController *)initViewControllerWithSearchText:(NSString *)searchText andLangArray:(NSMutableArray *)LangArray andCatArray:(NSMutableArray *)catArray {
    SearchResultViewController * controller = [[SearchResultViewController alloc] initWithNibName:@"SearchResultViewController" bundle:nil];
    controller->searchTextAttr = searchText;
    controller->arrLanguageAttr = LangArray;
    controller->arrCategory = catArray;
    controller->isVideoSearched = false;
    return controller;
}
+(SearchResultViewController *)initViewControllerWithSearchResultsArray:(NSMutableArray *)resultsArray WithSearchText:(NSString *)searchText andLanguageArray:(NSMutableArray *)langArray andStartDate:(NSString *)sDate andEndDate:(NSString *)eDate withSelfDrive:(BOOL)selfDrive {
    SearchResultViewController * controller = [[SearchResultViewController alloc] initWithNibName:@"SearchResultViewController" bundle:nil];

    controller->searchResultVideoTours = resultsArray;
    controller->isVideoSearched = true;
    
    controller->arrLanguageVideo = langArray;
    controller->sDate = sDate;
    controller->eDate = eDate;
    controller->searchTextVideo = searchText;
    controller->selfDrive = selfDrive;
    
    return controller;
}

+(SearchResultViewController *)initViewControllerWithSearchText:(NSString *)searchText andLangArray:(NSMutableArray *)LangArray andCatArray:(NSMutableArray *)catArray andSearchResult:(NSMutableArray *)searchResults {
    SearchResultViewController * controller = [[SearchResultViewController alloc] initWithNibName:@"SearchResultViewController" bundle:nil];
    controller->searchTextAttr = searchText;
    controller->arrLanguageAttr = LangArray;
    controller->arrCategory = catArray;
    controller->isVideoSearched = false;
    controller->searchResultAttraction = searchResults;

    
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    videoTourArray = [NSMutableArray new];
    arrAttractionData = [NSMutableArray new];
    
    if (isVideoSearched) {
        [self btnVideoClicked:self];
        videoTourArray = [VideoTour getAll];
        [collViewTagsVideo reloadData];
        [collViewMainVideo reloadData];
    } else {
        [self btnAttrClicked:self];
        arrAttractionData = [AttractionData getAll];
        [collViewTagsAttr reloadData];
        [collViewMainAttr reloadData];
    }
    
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self registerNibs];
    [self setNavBarItems];
    
    self.navigationController.navigationBarHidden = false;
    
    btnVideo.backgroundColor = [ColorConstants appYellowColor];
    btnAttr.backgroundColor = [ColorConstants appYellowColor];
    
    self.searchBar.showsCancelButton = YES;
    self.searchBar.delegate = self;
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getAttractions) name:@"AttractionsUpdated" object:nil];

}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    sortView = [PopularVideoSort initWithNib];
    sortView.tblView.delegate = self;
    sortView.tblView.dataSource = self;
    if (searchTextVideo) {
        self.searchBar.text = searchTextVideo;
    }
}

-(void)registerNibs {
    
    // Arun :: registering collection view cells
    
    [collViewMainVideo registerClass:[LatestVideoTourCollectionViewCell class] forCellWithReuseIdentifier:@"LatestVideoTourCollectionViewCell"];
    [collViewMainVideo registerNib:[UINib nibWithNibName:@"LatestVideoTourCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"LatestVideoTourCollectionViewCell"];
    
    [collViewTagsVideo registerClass:[TagsCollectionCell class] forCellWithReuseIdentifier:@"TagsCollectionCell"];
    [collViewTagsVideo registerNib:[UINib nibWithNibName:@"TagsCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"TagsCollectionCell"];
    
    [collViewMainAttr registerClass:[AttractionsVideoCell class] forCellWithReuseIdentifier:@"AttractionsVideoCell"];
    
    UINib *cellNib = [UINib nibWithNibName:@"AttractionsVideoCell" bundle:nil];
    [collViewMainAttr registerNib:cellNib forCellWithReuseIdentifier:@"AttractionsVideoCell"];
    
    [collViewTagsAttr registerClass:[TagsCollectionCell class] forCellWithReuseIdentifier:@"TagsCollectionCell"];
    [collViewTagsAttr registerNib:[UINib nibWithNibName:@"TagsCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"TagsCollectionCell"];
}

-(void)setNavBarItems {
    // Arun :: customizing the nav bar icons
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[ColorConstants appBrownColor]}];
    UIImage *buttonImage = [UIImage imageNamed:@"home_brown.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(homeClicked) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
    
    self.navigationController.navigationBar.backgroundColor = [ColorConstants appYellowColor];
    _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,45)];
    _searchBar.delegate = self;
    //    searchBar.showsCancelButton = YES;
    _searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    _searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
    //    [self set_searchBar:searchBar];
    self.navigationItem.titleView = _searchBar;
    
    UIImage *filterImage = [UIImage imageNamed:@"sort_descending.png"];
    UIButton *btnRight2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight2 setImage:filterImage forState:UIControlStateNormal];
    btnRight2.frame = CGRectMake(0, 0, filterImage.size.width, filterImage.size.height);
    [btnRight2 addTarget:self action:@selector(sortClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnRight2];
    
    self.navigationItem.rightBarButtonItem = rightBarItem2;
}

-(void)homeClicked {
    [self.navigationController popToRootViewControllerAnimated:true];
}

-(void)backClicked {
    [self.navigationController popViewControllerAnimated:true];
}

-(void)sortClicked {
    if (isSortShown) {
        isSortShown = NO;
        [subMenuView removeFromSuperview];
    } else {
        isSortShown = YES;
        CGFloat height;
        if (!viewVideo.hidden) {
            height = (7 * 40); // Arun :: number of rows * height of row
            subMenuView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - sortView.frame.size.width, 0, sortView.frame.size.width, height)];
        } else {
            height = (5 * 40);// Arun :: number of rows * height of row
             subMenuView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - sortView.frame.size.width, 0, sortView.frame.size.width, height)];
        }
        subMenuView.layer.borderWidth = 0.5;
        subMenuView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        [subMenuView addSubview:sortView];
        subMenuView.clipsToBounds = YES;
        [self.view addSubview:subMenuView];
    }
    [sortView.tblView reloadData];
}

- (IBAction)btnAttrClicked:(id)sender {
    [btnAttr setBackgroundImage:[UIImage imageNamed:@"brown_uLine.png"] forState:UIControlStateNormal];
    [btnVideo setBackgroundImage:nil forState:UIControlStateNormal];
    viewVideo.hidden = true;
    if (isVideoSearched) {
        viewAttr.hidden = true;
        lblNoDataFound.hidden= false;
    } else {
        viewAttr.hidden = false;
        lblNoDataFound.hidden= true;
    }
    [sortView.tblView reloadData];
}

- (IBAction)btnVideoClicked:(id)sender {
    [btnVideo setBackgroundImage:[UIImage imageNamed:@"brown_uLine.png"] forState:UIControlStateNormal];
    [btnAttr setBackgroundImage:nil forState:UIControlStateNormal];
    viewAttr.hidden = true;
    if (isVideoSearched) {
        viewVideo.hidden = false;
        lblNoDataFound.hidden= true;
    } else {
        viewVideo.hidden = true;
        lblNoDataFound.hidden= false;
    }
    [sortView.tblView reloadData];
}

-(void)bookmarkClickedVideoTour:(id)sender {
    VideoTour * v = [videoTourArray objectAtIndex:[sender tag]];
    [self commanApiWithModel:ModelPlan WithId:v.entity_id WithType:TypeBookmark];
}

-(void)commanApiWithModel:(NSString *)modelValue WithId:(NSString *)idValue WithType:(NSString *)typeValue {
   
    [[PlayTripManager Instance] addCountForModel:modelValue withType:typeValue forId:idValue WithBlock:^(id result, NSString *error) {
        if(!error){
      
            if(modelValue == ModelPlan){
                [self getVideoTour];
            }else{
                [self getAttractions];
            }
        }
    }];
}

#pragma mark -<UICollectionViewDelegate,UICollectionViewDataSource>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    if (collectionView == collViewMainVideo) {
        return searchResultVideoTours.count;
    } else if (collectionView == collViewTagsVideo) {
        int total = 0;//end date and search text;
        if (sDate.length > 0 && eDate.length > 0 && eDate.intValue > 0) {
            total++;
        }
        if (searchTextVideo.length > 0) {
            total++;
        }
        if (selfDrive) {
            total++;
        }
        total = total + (int)arrLanguageVideo.count;
        return total;
    } else if (collectionView == collViewTagsAttr) {
        int total = 0;
        if (searchTextAttr.length > 0) {
            total++;
        }
        total = total + (int)arrLanguageAttr.count;
        total = total + (int)arrCategory.count;
        return total;
    } else {
        return searchResultAttraction.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (collectionView == collViewTagsVideo ) {
        TagsCollectionCell *cell = (TagsCollectionCell*)[collViewTagsVideo dequeueReusableCellWithReuseIdentifier:@"TagsCollectionCell" forIndexPath:indexPath];
        
        cell.layer.borderWidth=1.0f;
        cell.layer.cornerRadius = cell.frame.size.height / 2;
        cell.layer.borderColor=[UIColor blackColor].CGColor;
        
        cell.btnClose.tag = indexPath.row;
        [cell.btnClose addTarget:self action:@selector(btnCloseClickedVideo:) forControlEvents:UIControlEventTouchUpInside];
        
        if (sDate.length > 0 && eDate.length > 0 && eDate.intValue > 0) {
            if (indexPath.row == 0) {
                NSString * aString = [NSString stringWithFormat:@"%@-%@ days", sDate, eDate];
                cell.lblMain.text = aString;
            } else {
                if (searchTextVideo.length > 0) {
                    if (indexPath.row == 1) {
                        cell.lblMain.text = searchTextVideo;
                    } else {
                        if (selfDrive) {
                            if (indexPath.row == 2) {
                                cell.lblMain.text = @"Self-Drive";
                            } else {
                                
                                if ([[arrLanguageVideo objectAtIndex:indexPath.row-3] isEqualToString:langChi]) {
                                    cell.lblMain.text = @"Chineese";
                                } else if ([[arrLanguageVideo objectAtIndex:indexPath.row-3] isEqualToString:langMnd]) {
                                    cell.lblMain.text = @"Mandarin";
                                } else{
                                    cell.lblMain.text = @"English";
                                }
                                
                            }
                        } else {
                            cell.lblMain.text = [arrLanguageVideo objectAtIndex:indexPath.row - 2];
                        }
                    }
                } else {
                    if (selfDrive) {
                        if (indexPath.row == 1) {
                            cell.lblMain.text = @"Self-Drive";
                        } else {
                            //                             cell.lblMain.text = [arrLanguage objectAtIndex:indexPath.row - 2];
                            if ([[arrLanguageVideo objectAtIndex:indexPath.row-2] isEqualToString:langChi]) {
                                cell.lblMain.text = @"Chineese";
                            } else if ([[arrLanguageVideo objectAtIndex:indexPath.row-2] isEqualToString:langMnd]) {
                                cell.lblMain.text = @"Mandarin";
                            } else{
                                cell.lblMain.text = @"English";
                            }
                        }
                    } else {
                        //                         cell.lblMain.text = [arrLanguage objectAtIndex:indexPath.row - 1];
                        if ([[arrLanguageVideo objectAtIndex:indexPath.row-1] isEqualToString:langChi]) {
                            cell.lblMain.text = @"Chineese";
                        } else if ([[arrLanguageVideo objectAtIndex:indexPath.row-1] isEqualToString:langMnd]) {
                            cell.lblMain.text = @"Mandarin";
                        } else{
                            cell.lblMain.text = @"English";
                        }
                    }
                }
            }
        } else {
            if (searchTextVideo.length > 0) {
                if (indexPath.row == 0) {
                    cell.lblMain.text = searchTextVideo;
                } else {
                    if (selfDrive) {
                        if (indexPath.row == 1) {
                            cell.lblMain.text = @"Self-Drive";
                        } else {
                            cell.lblMain.text = [arrLanguageVideo objectAtIndex:indexPath.row - 2];
                        }
                    } else {
                        cell.lblMain.text = [arrLanguageVideo objectAtIndex:indexPath.row - 1];
                    }
                }
            } else {
                if (selfDrive) {
                    if (indexPath.row == 0) {
                        cell.lblMain.text = @"Self-Drive";
                    } else {
                        cell.lblMain.text = [arrLanguageVideo objectAtIndex:indexPath.row - 1];
                    }
                } else {
                    cell.lblMain.text = [arrLanguageVideo objectAtIndex:indexPath.row - 0];
                }
            }
        }
        return cell;
    } else if (collectionView == collViewMainVideo ) {
        LatestVideoTourCollectionViewCell *cell = (LatestVideoTourCollectionViewCell*)[collViewMainVideo dequeueReusableCellWithReuseIdentifier:@"LatestVideoTourCollectionViewCell" forIndexPath:indexPath];
        
        Account * account = [AccountManager Instance].activeAccount;

        
        cell.lblTime.text = @"00:00";
        
        PlanModel *plan = [searchResultVideoTours objectAtIndex:indexPath.row];
        cell.lblUserName.text = plan.user.full_name;
        cell.imgUserImg.image = [UIImage imageNamed:@"avatarPlacHolder"];
        
        NSString * imgId = plan.user.avatar;
        NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
        
        
        [cell.imgUserImg sd_setShowActivityIndicatorView:YES];
        [cell.imgUserImg sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [cell.imgUserImg sd_setImageWithURL:[NSURL URLWithString:urlString]
                           placeholderImage:[UIImage imageNamed:@"avatarPlacHolder"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                               if (!error) {
                                   cell.imgUserImg.image = image;
                                   [cell layoutIfNeeded];
                               }
                               else{
                                   cell.imgUserImg.url = [NSURL URLWithString:urlString];
                               }
                           }];
        cell.lblFromDate.text = ([plan.create_date length]>10 ? [plan.create_date substringToIndex:10] : plan.create_date);
        cell.imgThumbnail.image = [UIImage imageNamed:@"def_video_img.png"];
        
        if(![plan.cover_photo isEqualToString:@""]){
            NSString * imgId = plan.cover_photo;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            
            [cell.imgThumbnail sd_setShowActivityIndicatorView:YES];
            [cell.imgThumbnail sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            [cell.imgThumbnail sd_setImageWithURL:[NSURL URLWithString:urlString]
                                 placeholderImage:[UIImage imageNamed:@"def_video_img.png"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                                     if (!error) {
                                         cell.imgThumbnail.image = image;
                                         [cell layoutIfNeeded];
                                     }
                                     else{
                                         cell.imgThumbnail.url = [NSURL URLWithString:urlString];
                                     }
                                 }];
            
            //cell.imgThumbnail.url = [NSURL URLWithString:urlString];
        }
        cell.lblLanguage.text = plan.language;
        
        cell.lblLanguage.hidden = YES;
        if([plan.language  isEqualToString: @"sc"]){
            cell.languageImage.image = [UIImage imageNamed:@"speaker_.png"];
        }else if([plan.language  isEqualToString: @"tc"]){
            cell.languageImage.image = [UIImage imageNamed:@"speaker_2.png"];
        }else{
            cell.languageImage.image = [UIImage imageNamed:@"speaker_Eng.png"];
        }
        
        
        cell.lblVideoName.text = plan.name;
        
        cell.lblCountry.hidden = YES;
        cell.lblDistrict.hidden = YES;
        cell.lblCity.hidden = YES;
        
        for (AttractionModel *attraction in plan.attractions) {
            if (attraction.attr_data.count) {
                AttractionDataModel *attratctionData = [attraction.attr_data objectAtIndex:0];
                cell.lblTime.text = attratctionData.time;
                if (attratctionData.info.country_id) {
                    cell.lblCountry.text = attratctionData.info.country_id.name;
                    cell.lblDistrict.text = attratctionData.info.district_id.name;
                    cell.lblCity.text = attratctionData.info.city_id.name;
                    cell.lblCountry.hidden = NO;
                    cell.lblDistrict.hidden = NO;
                    cell.lblCity.hidden = NO;
                    
                    break;
                }
            }
        }
        cell.lblBookmarkCount.text = [NSString stringWithFormat:@"%@",plan.total_bookmark];
        cell.lblViewCount.text = [NSString stringWithFormat:@"%@",plan.total_view];
        cell.lblShareCount.text = [NSString stringWithFormat:@"%@",plan.total_share];
        cell.lblRevertCount.text = [NSString stringWithFormat:@"%@",plan.remuneration_amount];
        
        [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
        cell.btnBookMark.selected = NO;
        if (account) {
            for (NSString *user in plan.bookmark_users) {
                if ([user isEqualToString:account.userId]) {
                    [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                    cell.btnBookMark.selected = YES;
                }
            }
        }
        cell.btnBookMark.tag = indexPath.row;
        [cell.btnBookMark addTarget:self action:@selector(bookmarkClickedVideoTour:) forControlEvents:UIControlEventTouchUpInside];
        
        /*
        VideoTour * v = [videoTourArray objectAtIndex:indexPath.row];
        NSMutableArray *attraction = [NSMutableArray new];
        NSMutableArray *attractionData = [NSMutableArray new];
        Attractions *a;
        AttractionData *aData;
        attraction = [[v.attractionsSet allObjects] mutableCopy];
        if(attraction.count>0){
            a =[attraction objectAtIndex:0];
        }
        
        Account * account = [AccountManager Instance].activeAccount;
        if (account) {
            if ([v.bookmark_users_string containsString:account.userId]) {
                [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                cell.btnBookMark.selected = YES;
            } else {
                [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
                cell.btnBookMark.selected = NO;
            }
        } else {
            [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
            cell.btnBookMark.selected = NO;
        }
        
        attractionData = [[a.attractionDataSet allObjects] mutableCopy];
        if(attractionData.count>0){
            aData = [attractionData objectAtIndex:0];
            cell.lblDescription.text = [NSString stringWithFormat:@"[%@ days] %@",v.days, aData.info.address];
        } else {
            cell.lblDescription.text = [NSString stringWithFormat:@"[%@ days]",v.days];
        }
        
        cell.lblFromDate.text = [Utils dateFormatyyyyMMdd:v.from_date];
        cell.lblVideoName.text = v.name;
        cell.lblUserName.text = v.user.full_name;
        
        if(![v.user.avatar isEqualToString:@""]){
            
            NSString * imgId = v.user.avatar;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            cell.imgUserImg.url = [NSURL URLWithString:urlString];
            cell.imgUserImg.layer.cornerRadius = cell.imgUserImg.frame.size.width / 2;
            cell.imgUserImg.layer.masksToBounds = YES;
        }else{
            cell.imgUserImg.backgroundColor = [UIColor blueColor];
            cell.imgUserImg.layer.cornerRadius = cell.imgUserImg.frame.size.width / 2;
            cell.imgUserImg.layer.masksToBounds = YES;
        }
        
        cell.btnBookMark.tag = indexPath.row;
        [cell.btnBookMark addTarget:self action:@selector(bookmarkClickedVideoTour:) forControlEvents:UIControlEventTouchUpInside];
        */
        
        return cell;
    } else if (collectionView == collViewTagsAttr ) {
        TagsCollectionCell *cell = (TagsCollectionCell*)[collViewTagsAttr dequeueReusableCellWithReuseIdentifier:@"TagsCollectionCell" forIndexPath:indexPath];
        
        cell.layer.borderWidth=1.0f;
        cell.layer.cornerRadius = cell.frame.size.height / 2;
        cell.layer.borderColor=[UIColor blackColor].CGColor;
        
        cell.btnClose.tag = indexPath.row;
        [cell.btnClose addTarget:self action:@selector(btnCloseClickedAttr:) forControlEvents:UIControlEventTouchUpInside];
        
        if (searchTextAttr.length > 0) {
            if (indexPath.row == 0) {
                cell.lblMain.text = searchTextAttr;
            } else {
                if (arrLanguageAttr.count > 0) {
                    if (indexPath.row < arrLanguageAttr.count+1) {
                        if ([[arrLanguageAttr objectAtIndex:indexPath.row-1] isEqualToString:langChi]) {
                            cell.lblMain.text = @"Chineese";
                        } else if ([[arrLanguageAttr objectAtIndex:indexPath.row-1] isEqualToString:langMnd]) {
                            cell.lblMain.text = @"Mandarin";
                        } else{
                            cell.lblMain.text = @"English";
                        }
                        
                    } else {
                        if (arrCategory.count > 0) {
                            NSString * eId = [arrCategory objectAtIndex:indexPath.row - arrLanguageAttr.count -1];
                            Categori * cat = [Categori getByEntityId:eId];
                            if (cat) {
                                cell.lblMain.text = cat.name;
                            }
                        }
                    }
                } else {
                    if (arrCategory.count > 0) {
                        NSString * eId = [arrCategory objectAtIndex:indexPath.row -1];
                        Categori * cat = [Categori getByEntityId:eId];
                        if (cat) {
                            cell.lblMain.text = cat.name;
                        }
                    }
                }
            }
        } else {
            if (arrLanguageAttr.count > 0) {
                if (indexPath.row < arrLanguageAttr.count) {
                    if ([[arrLanguageAttr objectAtIndex:indexPath.row] isEqualToString:langChi]) {
                        cell.lblMain.text = @"Chineese";
                    } else if ([[arrLanguageAttr objectAtIndex:indexPath.row] isEqualToString:langMnd]) {
                        cell.lblMain.text = @"Mandarin";
                    } else{
                        cell.lblMain.text = @"English";
                    }
                    
                } else {
                    if (arrCategory.count > 0) {
                        NSString * eId = [arrCategory objectAtIndex:indexPath.row - arrLanguageAttr.count];
                        Categori * cat = [Categori getByEntityId:eId];
                        if (cat) {
                            cell.lblMain.text = cat.name;
                        }
                    }
                }
            } else {
                if (arrCategory.count > 0) {
                    NSString * eId = [arrCategory objectAtIndex:indexPath.row];
                    Categori * cat = [Categori getByEntityId:eId];
                    if (cat) {
                        cell.lblMain.text = cat.name;
                    }
                }
            }
        }
        return cell;
    } else {
        AttractionsVideoCell *cell = (AttractionsVideoCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"AttractionsVideoCell" forIndexPath:indexPath];
        
        

        AttractionModel *attraction = searchResultAttraction[indexPath.row];
        
        NSString * imgId = attraction.user.avatar;
        NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
        
        
        
        [cell.imgUserImg sd_setShowActivityIndicatorView:YES];
        [cell.imgUserImg sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [cell.imgUserImg sd_setImageWithURL:[NSURL URLWithString:urlString]
                           placeholderImage:[UIImage imageNamed:@"avatarPlacHolder"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                               if (!error) {
                                   cell.imgUserImg.image = image;
                                   [cell layoutIfNeeded];
                               }
                           }];
        
        
        cell.lblUserName.text = attraction.user.full_name;
        cell.lblFromDate.text = ([attraction.create_date length]>10 ? [attraction.create_date substringToIndex:10] : attraction.create_date);
        
        cell.lblVideoName.text = attraction.name;
        
        cell.lblTTotalView.text = [NSString stringWithFormat:@"%@",attraction.total_view];
        cell.lblTTotalBookMark.text = [NSString stringWithFormat:@"%@",attraction.total_bookmark];
        cell.lblTTotalShare.text = [NSString stringWithFormat:@"%@",attraction.total_share];
        
        
        [cell.imgThumbnail sd_setShowActivityIndicatorView:YES];
        [cell.imgThumbnail sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [cell.imgThumbnail sd_setImageWithURL:[NSURL URLWithString:attraction.cover_photo_url]
                             placeholderImage:[UIImage imageNamed:@"def_video_img.png"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                                 if (!error) {
                                     cell.imgThumbnail.image = image;
                                     [cell layoutIfNeeded];
                                 }
                             }];
        Account * account = [AccountManager Instance].activeAccount;
        [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
        cell.btnBookMark.selected = NO;
        if (account) {
            for (UserModel *user in attraction.bookmark_users) {
                if ([user._id isEqualToString:account.userId]) {
                    [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                    cell.btnBookMark.selected = YES;
                }
            }
        }
        cell.imgUserImg.layer.cornerRadius = cell.imgUserImg.frame.size.width / 2;
        cell.imgUserImg.layer.masksToBounds = YES;
        /*
        if([plan.language  isEqualToString: @"sc"]){
            cell.languageImage.image = [UIImage imageNamed:@"speaker_.png"];
        }else if([plan.language  isEqualToString: @"tc"]){
            cell.languageImage.image = [UIImage imageNamed:@"speaker_2.png"];
        }else{
            cell.languageImage.image = [UIImage imageNamed:@"speaker_Eng.png"];
        }
        
         */
        
        /*
        AttractionData * attrData = [arrAttractionData objectAtIndex: indexPath.row];
          Account * account = [AccountManager Instance].activeAccount;
        if (account) {
            if ([attrData.info.bookmark_users_string containsString:account.userId]) {
                [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                cell.btnBookMark.selected = YES;
            } else {
                [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
                cell.btnBookMark.selected = NO;
            }
        } else {
            [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
            cell.btnBookMark.selected = NO;
        }
        
        
       
        cell.lblUserName.text = attrData.info.userAttractionData.full_name;
        cell.lblVideoName.text = attrData.info.name;
        cell.lblTTotalView.text = [NSString stringWithFormat:@"%@", attrData.info.total_view];
        cell.lblTTotalShare.text = [NSString stringWithFormat:@"%@", attrData.info.total_share];
        cell.lblTTotalBookMark.text = [NSString stringWithFormat:@"%@", attrData.info.total_bookmark];
        cell.lblFromDate.text = [Utils dateFormatyyyyMMdd:attrData.info.create_date];
        if(![attrData.info.userAttractionData.avatar isEqualToString:@""]){
            NSString * imgId = attrData.info.userAttractionData.avatar;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            cell.imgUserImg.url = [NSURL URLWithString:urlString];
            cell.imgUserImg.layer.cornerRadius = cell.imgUserImg.frame.size.width / 2;
            cell.imgUserImg.layer.masksToBounds = YES;
        }else{
            cell.imgUserImg.backgroundColor = [UIColor blueColor];
            cell.imgUserImg.layer.cornerRadius = cell.imgUserImg.frame.size.width / 2;
            cell.imgUserImg.layer.masksToBounds = YES;
            
        }
        cell.btnBookMark.tag = indexPath.row;
        [cell.btnBookMark addTarget:self action:@selector(bookmarkClickedAttraction:) forControlEvents:UIControlEventTouchUpInside];
        */
        
        return cell;
    }
}
-(void)bookmarkClickedAttraction:(UIButton *)sender {
    AttractionData * att = [arrAttractionData objectAtIndex:[sender tag]];
    [self commanApiWithModel:ModelAttraction WithId:att.entity_id WithType:TypeBookmark];
}




- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == collViewMainVideo) {
        return CGSizeMake((collectionView.frame.size.width/2)-5, (collectionView.frame.size.height/2)+35);
    } else if (collectionView == collViewTagsVideo) {
        NSString * lblString;
        if (sDate.length > 0 && eDate.length > 0 && eDate.intValue > 0) {
            if (indexPath.row == 0) {
                NSString * aString = [NSString stringWithFormat:@"%@-%@ days", sDate, eDate];
                lblString = aString;
            } else {
                if (searchTextVideo.length > 0) {
                    if (indexPath.row == 1) {
                        lblString = searchTextVideo;
                    } else {
                        if (selfDrive) {
                            if (indexPath.row == 2) {
                                lblString = @"Self-Drive";
                            } else {
                                //                                lblString = [arrLanguage objectAtIndex:indexPath.row - 3];
                                if ([[arrLanguageVideo objectAtIndex:indexPath.row-3] isEqualToString:langChi]) {
                                    lblString = @"Chineese";
                                } else if ([[arrLanguageVideo objectAtIndex:indexPath.row-3] isEqualToString:langMnd]) {
                                    lblString = @"Mandarin";
                                } else{
                                    lblString = @"English";
                                }
                            }
                        } else {
                            //                            lblString = [arrLanguage objectAtIndex:indexPath.row - 2];
                            if ([[arrLanguageVideo objectAtIndex:indexPath.row-2] isEqualToString:langChi]) {
                                lblString = @"Chineese";
                            } else if ([[arrLanguageVideo objectAtIndex:indexPath.row-2] isEqualToString:langMnd]) {
                                lblString = @"Mandarin";
                            } else{
                                lblString = @"English";
                            }
                        }
                    }
                } else {
                    if (selfDrive) {
                        if (indexPath.row == 1) {
                            lblString = @"Self-Drive";
                        } else {
                            //                            lblString = [arrLanguage objectAtIndex:indexPath.row - 2];
                            if ([[arrLanguageVideo objectAtIndex:indexPath.row-2] isEqualToString:langChi]) {
                                lblString = @"Chineese";
                            } else if ([[arrLanguageVideo objectAtIndex:indexPath.row-2] isEqualToString:langMnd]) {
                                lblString = @"Mandarin";
                            } else{
                                lblString = @"English";
                            }
                        }
                    } else {
                        //                        lblString = [arrLanguage objectAtIndex:indexPath.row - 1];
                        if ([[arrLanguageVideo objectAtIndex:indexPath.row-1] isEqualToString:langChi]) {
                            lblString = @"Chineese";
                        } else if ([[arrLanguageVideo objectAtIndex:indexPath.row-1] isEqualToString:langMnd]) {
                            lblString = @"Mandarin";
                        } else{
                            lblString = @"English";
                        }
                    }
                }
            }
        } else {
            if (searchTextVideo.length > 0) {
                if (indexPath.row == 0) {
                    lblString = searchTextVideo;
                } else {
                    if (selfDrive) {
                        if (indexPath.row == 1) {
                            lblString = @"Self-Drive";
                        } else {
                            lblString = [arrLanguageVideo objectAtIndex:indexPath.row - 2];
                        }
                    } else {
                        lblString = [arrLanguageVideo objectAtIndex:indexPath.row - 1];
                    }
                }
            } else {
                if (selfDrive) {
                    if (indexPath.row == 0) {
                        lblString = @"Self-Drive";
                    } else {
                        lblString = [arrLanguageVideo objectAtIndex:indexPath.row - 1];
                    }
                } else {
                    lblString = [arrLanguageVideo objectAtIndex:indexPath.row - 0];
                }
            }
        }
        UIFont *font = [UIFont systemFontOfSize:14.0];
        CGFloat strWidth = [Utils widthOfString:lblString withFont:font];
        return CGSizeMake(strWidth+50,25);
    } else if (collectionView == collViewTagsAttr) {
        NSString * lblString;
        if (searchTextAttr.length > 0) {
            if (indexPath.row == 0) {
                lblString = searchTextAttr;
            } else {
                if (arrLanguageAttr.count > 0) {
                    if (indexPath.row < arrLanguageAttr.count+1) {
                        if ([[arrLanguageAttr objectAtIndex:indexPath.row-1] isEqualToString:langChi]) {
                            lblString = @"Chineese";
                        } else if ([[arrLanguageAttr objectAtIndex:indexPath.row-1] isEqualToString:langMnd]) {
                            lblString = @"Mandarin";
                        } else{
                            lblString = @"English";
                        }
                    } else {
                        if (arrCategory.count > 0) {
                            NSString * eId = [arrCategory objectAtIndex:indexPath.row - arrLanguageAttr.count -1];
                            Categori * cat = [Categori getByEntityId:eId];
                            if (cat) {
                                lblString = cat.name;
                            }
                        }
                    }
                } else {
                    if (arrCategory.count > 0) {
                        NSString * eId = [arrCategory objectAtIndex:indexPath.row -1];
                        Categori * cat = [Categori getByEntityId:eId];
                        if (cat) {
                            lblString = cat.name;
                        }
                    }
                }
            }
        } else {
            if (arrLanguageAttr.count > 0) {
                if (indexPath.row < arrLanguageAttr.count) {
                    if ([[arrLanguageAttr objectAtIndex:indexPath.row] isEqualToString:langChi]) {
                        lblString = @"Chineese";
                    } else if ([[arrLanguageAttr objectAtIndex:indexPath.row] isEqualToString:langMnd]) {
                        lblString = @"Mandarin";
                    } else{
                        lblString = @"English";
                    }
                    
                } else {
                    if (arrCategory.count > 0) {
                        NSString * eId = [arrCategory objectAtIndex:indexPath.row - arrLanguageAttr.count];
                        Categori * cat = [Categori getByEntityId:eId];
                        if (cat) {
                            lblString = cat.name;
                        }
                    }
                }
            } else {
                if (arrCategory.count > 0) {
                    NSString * eId = [arrCategory objectAtIndex:indexPath.row];
                    Categori * cat = [Categori getByEntityId:eId];
                    if (cat) {
                        lblString = cat.name;
                    }
                }
            }
        }
        
        if (lblString) {
            UIFont *font = [UIFont systemFontOfSize:14.0];
            CGFloat strWidth = [Utils widthOfString:lblString withFont:font];
            
            return CGSizeMake(strWidth+50,32);
        } else {
            return CGSizeMake(0,32);
        }
        
    } else if (collectionView == collViewMainAttr) {
        //+51 for bottom
        return CGSizeMake((collectionView.frame.size.width/2)-5, (collectionView.frame.size.height/2)+35);
    } else { // This condition is not possible. But still.
        return CGSizeMake((collectionView.frame.size.width/2)-5, (collectionView.frame.size.height/2)+35);
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == collViewMainVideo) {
        PlanModel *plan = [searchResultVideoTours objectAtIndex:indexPath.row];
        ItineraryViewController * controller = [ItineraryViewController initViewControllerWithPlan:plan];
        [self.navigationController  pushViewController:controller animated:YES];
        
    }else if(collectionView == collViewMainAttr ){
        AttractionModel *attractionModel = searchResultAttraction[indexPath.row];
        PlaceInformationViewController *controller = [PlaceInformationViewController initViewControllerWithAttractionModelOnly:attractionModel];
        [self.navigationController pushViewController:controller animated:YES];
    }
}

-(void)btnCloseClickedVideo:(id)sender {
    
    [SVProgressHUD showWithStatus:@"Updating..."];
    NSInteger Tag = [sender tag];
    
    if (sDate.length > 0 && eDate.length > 0 && eDate.intValue > 0) {
        if (Tag == 0) {
            sDate = @"";
            eDate = @"";
        } else {
            if (searchTextVideo.length > 0) {
                if (Tag == 1) {
                    searchTextVideo = @"";
                    self.searchBar.text = @"";
                } else {
                    if (selfDrive) {
                        if (Tag == 2) {
                            selfDrive = false;
                        } else {
                            if (arrLanguageVideo.count >= Tag -3) {
                                [arrLanguageVideo removeObjectAtIndex:Tag - 3];
                            }
                        }
                    } else {
                        if (arrLanguageVideo.count >= Tag -2) {
                            [arrLanguageVideo removeObjectAtIndex:Tag - 2];
                        }
                    }
                }
            } else {
                if (selfDrive) {
                    if (Tag == 1) {
                        selfDrive = false;
                    } else {
                        if (arrLanguageVideo.count >= Tag -2) {
                            [arrLanguageVideo removeObjectAtIndex:Tag - 2];
                        }
                    }
                } else {
                    if (arrLanguageVideo.count >= Tag - 1) {
                        [arrLanguageVideo removeObjectAtIndex:Tag - 1];
                    }
                }
            }
        }
    } else {
        if (searchTextVideo.length > 0) {
            if (Tag == 0) {
                searchTextVideo = @"";
                self.searchBar.text = @"";
            } else {
                if (selfDrive) {
                    if (Tag == 1) {
                        selfDrive = false;
                    } else {
                        if (arrLanguageVideo.count >= Tag - 2) {
                            [arrLanguageVideo removeObjectAtIndex:Tag - 2];
                        }
                    }
                } else {
                    if (arrLanguageVideo.count >= Tag - 1) {
                        [arrLanguageVideo removeObjectAtIndex:Tag - 1];
                    }
                }
            }
        } else {
            if (selfDrive) {
                if (Tag == 0) {
                    selfDrive = false;
                } else {
                    if (arrLanguageVideo.count >= Tag - 1) {
                        [arrLanguageVideo removeObjectAtIndex:Tag - 1];
                    }
                }
            } else {
                if (arrLanguageVideo.count >= Tag) {
                    [arrLanguageVideo removeObjectAtIndex:Tag];
                }
            }
        }
    }
    [self getVideoTour];
    
   
}
-(void)getVideoTour{
    
    NSString *daysString = [NSString stringWithFormat:@"%i",sDate.intValue];
    
    for (int i = sDate.intValue + 1; i <= eDate.intValue ; i++) {
        daysString = [NSString stringWithFormat:@"%@,%i",daysString,i];
    }
    
    
    NSString *selfDriveString = @"false";
    if (selfDrive) {
        selfDriveString = @"true";
    }
    
    
    NSLog(@"%lu",(unsigned long)arrLanguageVideo.count);
    
    NSMutableDictionary *parameters;
    
    if (self.searchBar.text.length) {
        if (arrLanguageVideo.count) {
            NSString *languageString = [NSString stringWithFormat:@"%@",arrLanguageVideo[0]];
            for (int i = 1; i < arrLanguageVideo.count ; i++) {
                languageString = [NSString stringWithFormat:@"%@,%@",languageString,arrLanguageVideo[i]];
            }
            parameters = [@{ @"where": @{ @"language": languageString,
                                         @"name": self.searchBar.text,
                                         @"self_drive": selfDriveString,
                                         @"days": daysString } } mutableCopy];
            
        }
        else{
            parameters = [@{ @"where": @{ @"name": self.searchBar.text,
                                         @"self_drive": selfDriveString,
                                         @"days": daysString } } mutableCopy];
        }
    }
    else{
        if (arrLanguageVideo.count) {
            NSString *languageString = [NSString stringWithFormat:@"%@",arrLanguageVideo[0]];
            for (int i = 1; i < arrLanguageVideo.count ; i++) {
                languageString = [NSString stringWithFormat:@"%@,%@",languageString,arrLanguageVideo[i]];
            }
            parameters = [@{ @"where": @{ @"name": self.searchBar.text,
                                         @"self_drive": selfDriveString,
                                         @"days": daysString } } mutableCopy];
            
        }
        else{
            parameters = [@{ @"where": @{ @"self_drive": selfDriveString,
                                         @"days": daysString } } mutableCopy];
        }
    }
    
    if (sDate.length == 0 || eDate.length == 0) {
        NSMutableDictionary *whereDict = [[parameters objectForKey:@"where"] mutableCopy];
        
        [whereDict removeObjectForKey:@"days"];
        [parameters setObject:whereDict forKey:@"where"];
    }

    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Searching Video Tours..."];
  
    
    [[PlayTripManager Instance] getLatestVideoTourWithParameters:parameters WithBlockNew:^(id result, NSString *error) {
        [SVProgressHUD dismiss];
        if (!error) {
            NSError* errorData;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                 options:kNilOptions
                                                                   error:&errorData];
            if (errorData) {
                NSLog(@"ERROR GOTTEN = %@" ,errorData.description);
            }
            NSArray* plansDicts = [json objectForKey:@"data"];
            searchResultVideoTours = [[NSMutableArray alloc] init];
            for (NSDictionary *planDict in plansDicts) {
                NSError *error;
                PlanModel *planModel = [[PlanModel alloc] initWithDictionary:planDict error:&error];
                if (error) {
                    NSLog(@"ERROR GOTTEN AT JSONMODEL = %@" ,error.description);
                }
                if (![searchResultVideoTours containsObject:planModel]) {
                    [searchResultVideoTours addObject:planModel];
                }
            }
            NSLog(@"Total Plans Gotten = %lu" ,(unsigned long)searchResultVideoTours.count);
            //allLatestPlans = [[[allLatestPlans reverseObjectEnumerator] allObjects] mutableCopy];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [collViewMainVideo reloadData];
                [collViewTagsVideo reloadData];
                
            });
        }
    }];

    
    
    /*
    [[PlayTripManager Instance] searchVideoTourWithName:searchTextVideo andLanguageArray:arrLanguageVideo andStartDate:nil andEndDate:nil andSelfDrive:selfDrive WithBlock:^(id result, NSString *error) {
        if (!error) {
       
            videoTourArray = [VideoTour getAll];
            [collViewMainVideo reloadData];
            isShowTemp = false;
            [collViewTagsVideo reloadData];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LatestVideoTourUpdated" object:nil];
        }
        [SVProgressHUD dismiss];
    }];
     */
}

-(void)btnCloseClickedAttr:(id)sender {
    NSInteger Tag = [sender tag];
    [SVProgressHUD showWithStatus:@"Updating..."];
    if (searchTextAttr.length > 0) {
        if (Tag == 0) {
            searchTextAttr = @"";
        } else {
            if (arrLanguageAttr.count > 0) {
                if (Tag < arrLanguageAttr.count+1) {
                    [arrLanguageAttr removeObjectAtIndex:Tag-1];
                } else {
                    if (arrCategory.count > 0) {
                        [arrCategory removeObjectAtIndex:Tag - arrLanguageAttr.count -1];
                    }
                }
            } else {
                if (arrCategory.count > 0) {
                    [arrCategory removeObjectAtIndex:Tag - arrLanguageAttr.count -1];
                }
            }
        }
        
    } else {
        if (arrLanguageAttr.count > 0) {
            if (Tag < arrLanguageAttr.count) {
                [arrLanguageAttr removeObjectAtIndex:Tag];
            } else {
                if (arrCategory.count > 0) {
                    [arrCategory removeObjectAtIndex:Tag - arrLanguageAttr.count];
                }
            }
        } else {
            if (arrCategory.count > 0) {
                [arrCategory removeObjectAtIndex:Tag - arrLanguageAttr.count];
            }
        }
    }
    
    
    NSDictionary *parameters;
    
    if (self.searchBar.text.length) {
        if (arrLanguageAttr.count) {
            NSString *languageString = [NSString stringWithFormat:@"%@",arrLanguageAttr[0]];
            for (int i = 1; i < arrLanguageAttr.count ; i++) {
                languageString = [NSString stringWithFormat:@"%@,%@",languageString,arrLanguageAttr[i]];
            }
            
            if (arrCategory.count) {
                NSString *categoryStr = arrCategory[0];
                for (int i = 1; i < arrCategory.count ; i++) {
                    categoryStr = [NSString stringWithFormat:@"%@,%@",categoryStr,arrCategory[i]];
                }
                
                parameters = @{ @"where": @{ @"language": languageString,
                                             @"name": self.searchBar.text,
                                             @"category": categoryStr } };
            }
            else{
                parameters = @{ @"where": @{ @"language": languageString,
                                             @"name": self.searchBar.text } };
            }
            
            
        }
        else{
            if (arrCategory.count) {
                NSString *categoryStr = arrCategory[0];
                for (int i = 1; i < arrCategory.count ; i++) {
                    categoryStr = [NSString stringWithFormat:@"%@,%@",categoryStr,arrCategory[i]];
                }
                parameters = @{ @"where": @{ @"name": self.searchBar.text,
                                             @"category": categoryStr } };
            }
            else{
                parameters = @{ @"where": @{ @"name": self.searchBar.text} };
            }
            
        }
    }
    else{
        if (arrLanguageAttr.count) {
            NSString *languageString = [NSString stringWithFormat:@"%@",arrLanguageAttr[0]];
            for (int i = 1; i < arrLanguageAttr.count ; i++) {
                languageString = [NSString stringWithFormat:@"%@,%@",languageString,arrLanguageAttr[i]];
            }
            if (arrCategory.count) {
                NSString *categoryStr = arrCategory[0];
                for (int i = 1; i < arrCategory.count ; i++) {
                    categoryStr = [NSString stringWithFormat:@"%@,%@",categoryStr,arrCategory[i]];
                }
                
                parameters = @{ @"where": @{ @"language": languageString,
                                             @"category": categoryStr } };
            }
            else{
                parameters = @{ @"where": @{ @"language": languageString} };
            }
            
            
            
        }
        else{
            if (arrCategory.count) {
                NSString *categoryStr = arrCategory[0];
                for (int i = 1; i < arrCategory.count ; i++) {
                    categoryStr = [NSString stringWithFormat:@"%@,%@",categoryStr,arrCategory[i]];
                }
                parameters = @{ @"where": @{ @"category": categoryStr } };
            }
            else{
                parameters = @{ @"where": @{  } };
                
            }
        }
    }
    
    
    
    searchResultAttraction = [[NSMutableArray alloc] init];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Searching Attractions..."];
    
    
    
    [[PlayTripManager Instance] searchAttractionWithDictionary:parameters WithBlock:^(id result, NSString *error) {
        
        [SVProgressHUD dismiss];
        if (!error) {
            NSError* errorData;
            NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                 options:kNilOptions
                                                                   error:&errorData];
            if (errorData) {
                NSLog(@"ERROR GOTTEN = %@" ,errorData.description);
            }
            NSArray* attractionDicts = [json objectForKey:@"data"];
            searchResultAttraction = [[NSMutableArray alloc] init];
            for (NSDictionary *attractionDict in attractionDicts) {
                NSError *error;
                AttractionModel *attractionModel = [[AttractionModel alloc] initWithDictionary:attractionDict error:&error];
                if (error) {
                    NSLog(@"ERROR GOTTEN AT JSONMODEL = %@" ,error.description);
                }
                if (![searchResultAttraction containsObject:attractionModel]) {
                    [searchResultAttraction addObject:attractionModel];
                }
            }
            NSLog(@"Total Attractions Gotten Gotten = %lu" ,(unsigned long)searchResultAttraction.count);
            //allLatestPlans = [[[allLatestPlans reverseObjectEnumerator] allObjects] mutableCopy];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //[collLatestView reloadData];
                
                if (searchResultAttraction.count > 0) {
                    [collViewTagsAttr reloadData];
                    [collViewMainAttr reloadData];
                } else {
                    [Utils showAlertWithMessage:@"No data found"];
                }
            });
        }
    }];
    
    
    
    //[self getAttractions];
    
    
    
}
-(void)getAttractions{
    [[PlayTripManager Instance] searchAttractionWithCatArray:arrCategory andLanguageArray:arrLanguageAttr andSearchStr:searchTextAttr WithBlock:^(id result, NSString *error) {
        if (!error) {
  
            arrAttractionData = [AttractionData getAll];
            [collViewMainAttr reloadData];
            [collViewTagsAttr reloadData];
            
        }
        [SVProgressHUD dismiss];
    }];
}

#pragma Searchbar Delegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)sBar {
    [sBar setShowsCancelButton:YES animated:YES];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)sBar{
    if (sBar.text.length) {
        searchTextVideo = sBar.text;
        [self getVideoTour];
    }
    
    [sBar resignFirstResponder];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)sBar{
    [sBar resignFirstResponder];
    [sBar setShowsCancelButton:NO animated:YES];
    sBar.text = @"";
    videoTourArray = [VideoTour getAll];
     [collViewMainVideo reloadData];
    arrAttractionData = [AttractionData getAll];
    [collViewMainAttr reloadData];
    
}

- (void)searchBar:(UISearchBar *)sBar textDidChange:(NSString *)text {
    /*
    videoTourArray =[NSMutableArray new];
    NSMutableArray * aarVideoTour = [VideoTour getAll];
    
    if([text length] != 0) {
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"name BEGINSWITH[c] %@", text];
        videoTourArray = [[aarVideoTour filteredArrayUsingPredicate:predicate1]mutableCopy];
    } else {
        [sBar resignFirstResponder];
        videoTourArray = [VideoTour getAll];
    }
    
    videoTourCount = (int)videoTourArray.count;
    [collViewMainVideo reloadData];
    
    arrAttractionData =[NSMutableArray new];
    NSMutableArray * aarAttr = [AttractionData getAll];
    if([text length] != 0) {
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"name BEGINSWITH[c] %@", text];
        arrAttractionData = [[aarAttr filteredArrayUsingPredicate:predicate1]mutableCopy];
    } else {
        [sBar resignFirstResponder];
        arrAttractionData = [AttractionData getAll];
    }
    [collViewMainAttr reloadData];
     */
}

#pragma Sort View - Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (!viewVideo.hidden) {
        return 7;
    } else {
        return 5;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!viewVideo.hidden) {
        VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"VideoSortTableViewCell" forIndexPath:indexPath];
        cell.btnFirst.layer.cornerRadius = 5.0;
        cell.btnSecond.layer.cornerRadius = 5.0;
        
        if (indexPath.row == 0) {
            cell.lblText.text = @"Time";
        }else if (indexPath.row == 1){
            cell.lblText.text = @"Bookmark";
        }else if (indexPath.row == 2){
            cell.lblText.text = @"View";
        }else if (indexPath.row == 3){
            cell.lblText.text = @"Share";
        }else if (indexPath.row == 4){
            cell.lblText.text = @"Rewards";
        }else if (indexPath.row == 5){
            cell.lblText.text = @"Attractions";
        }else if (indexPath.row == 6){
            cell.lblText.text = @"Add to My Plan";
        }
        
       
        
        [cell.btnFirst setImage:[UIImage imageNamed:@"up_us"] forState:UIControlStateNormal];
        [cell.btnSecond setImage:[UIImage imageNamed:@"down_us"] forState:UIControlStateNormal];
        
        cell.btnFirst.tag = indexPath.row;
        cell.btnSecond.tag = indexPath.row;
        
        [cell.btnFirst addTarget:self action:@selector(btnFirstClickedSort:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnSecond addTarget:self action:@selector(btnSecondClickedSort:) forControlEvents:UIControlEventTouchUpInside];
        
        if (lastSortedVideoIndex) {
            if ((int)indexPath.row == lastSortedVideoIndex-1) {
                if (lastSortedVideoUp) {
                     [cell.btnFirst setImage:[UIImage imageNamed:@"up_s"] forState:UIControlStateNormal];
                } else {
                 [cell.btnSecond setImage:[UIImage imageNamed:@"down_s"] forState:UIControlStateNormal];
                }
            }
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else {
        VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"VideoSortTableViewCell" forIndexPath:indexPath];
        cell.btnFirst.layer.cornerRadius = 5.0;
        cell.btnSecond.layer.cornerRadius = 5.0;
        
        if (indexPath.row == 0) {
            cell.lblText.text = @"Time";
        }else if (indexPath.row == 1){
            cell.lblText.text = @"Bookmark";
        }else if (indexPath.row == 2){
            cell.lblText.text = @"View";
        }else if (indexPath.row == 3){
            cell.lblText.text = @"Share";
        }else if (indexPath.row == 4){
            cell.lblText.text = @"Add to My Plan";
        }
        
        [cell.btnFirst setImage:[UIImage imageNamed:@"up_us"] forState:UIControlStateNormal];
        cell.btnFirst.tag = indexPath.row;
        cell.btnSecond.tag = indexPath.row;
        
        [cell.btnFirst addTarget:self action:@selector(btnFirstClickedSort:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnSecond addTarget:self action:@selector(btnSecondClickedSort:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btnSecond setImage:[UIImage imageNamed:@"down_us"] forState:UIControlStateNormal];
        
        if (lastSortedAttrIndex) {
            if ((int)indexPath.row == lastSortedAttrIndex-1) {
                if (lastSortedAttrUp) {
                    [cell.btnFirst setImage:[UIImage imageNamed:@"up_s"] forState:UIControlStateNormal];
                } else {
                    [cell.btnSecond setImage:[UIImage imageNamed:@"down_s"] forState:UIControlStateNormal];
                }
            }
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

-(void)btnFirstClickedSort:(id)sender {
    int rowNumber = (int)[sender tag]+1;
    NSIndexPath * ip = [NSIndexPath indexPathForRow:rowNumber inSection:0];
    
    if (!viewVideo.hidden) {
        lastSortedVideoIndex = rowNumber;
        lastSortedVideoUp = true;
        for (int i = 0; i<7; i++) {
            NSIndexPath * ip1 = [NSIndexPath indexPathForRow:i inSection:0];
             VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[sortView.tblView cellForRowAtIndexPath:ip1];
            //[cell.btnFirst setImage:[UIImage imageNamed:@"up_us"] forState:UIControlStateNormal];
            //[cell.btnSecond setImage:[UIImage imageNamed:@"down_us"] forState:UIControlStateNormal];
        }
        VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[sortView.tblView cellForRowAtIndexPath:ip];
        //[cell.btnFirst setImage:[UIImage imageNamed:@"up_s"] forState:UIControlStateNormal];
        
        if ([sender tag] == 0) {
            [self sortVideoToursWithTime:YES];
            //videoTourArray = [VideoTour getAllByTimeAsc:true];
        } else if ([sender tag] == 1) {
            //videoTourArray = [VideoTour getAllByBoookmarkAsc:true];
            [self sortVideoToursWithBookMarks:YES];
        } else if ([sender tag] == 2) {
            //videoTourArray = [VideoTour getAllByViewAsc:true];
            [self sortVideoToursWithViews:YES];
        } else if ([sender tag] == 3) {
            //videoTourArray = [VideoTour getAllByShareAsc:true];
            [self sortVideoToursWithShare:YES];
        } else if ([sender tag] == 4) {
            //videoTourArray = [VideoTour getAllByAmountAsc:true];
            [self sortVideoToursWithReward:YES];
        } else if ([sender tag] == 5) {
            //videoTourArray = [VideoTour getAllByAttrCountAsc:true];
            [self sortVideoToursWithAttractions:YES];
        } else if ([sender tag] == 6) {
            //videoTourArray = [VideoTour getAllByAddToPlanAsc:true];
        }
        //[collViewMainVideo reloadData];
    } else {
        lastSortedAttrIndex = (int)[sender tag]+1;
        lastSortedAttrUp = true;
        for (int i = 0; i<5; i++) {
            NSIndexPath * ip1 = [NSIndexPath indexPathForRow:i inSection:0];
            VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[sortView.tblView cellForRowAtIndexPath:ip1];
            [cell.btnFirst setImage:[UIImage imageNamed:@"up_us"] forState:UIControlStateNormal];
            [cell.btnSecond setImage:[UIImage imageNamed:@"down_us"] forState:UIControlStateNormal];
        }
        VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[sortView.tblView cellForRowAtIndexPath:ip];
        [cell.btnFirst setImage:[UIImage imageNamed:@"up_s"] forState:UIControlStateNormal];
        
        if ([sender tag] == 0) {
            [self sortAttractionsWithTime:YES];
            //videoTourArray = [VideoTour getAllByTimeAsc:true];
        } else if ([sender tag] == 1) {
            //videoTourArray = [VideoTour getAllByBoookmarkAsc:true];
            [self sortAttractionsWithBookMarks:YES];
        } else if ([sender tag] == 2) {
            //videoTourArray = [VideoTour getAllByViewAsc:true];
            [self sortAttractionsWithViews:YES];
        } else if ([sender tag] == 3) {
            //videoTourArray = [VideoTour getAllByShareAsc:true];
            [self sortAttractionsWithShare:YES];
        } else if ([sender tag] == 4) {
            //videoTourArray = [VideoTour getAllByAmountAsc:true];
        }
        //[collViewMainAttr reloadData];
    }
    isSortShown = NO;
    [subMenuView removeFromSuperview];
}

-(void)btnSecondClickedSort:(id)sender {
    int rowNumber = (int)[sender tag]+1;
    NSIndexPath * ip = [NSIndexPath indexPathForRow:rowNumber inSection:0];
    if (!viewVideo.hidden) {
        lastSortedVideoIndex = rowNumber;
        lastSortedVideoUp = false;
        for (int i = 0; i<7; i++) {
            NSIndexPath * ip1 = [NSIndexPath indexPathForRow:i inSection:0];
            VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[sortView.tblView cellForRowAtIndexPath:ip1];
            [cell.btnFirst setImage:[UIImage imageNamed:@"up_us"] forState:UIControlStateNormal];
            [cell.btnSecond setImage:[UIImage imageNamed:@"down_us"] forState:UIControlStateNormal];
        }
        VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[sortView.tblView cellForRowAtIndexPath:ip];
        [cell.btnSecond setImage:[UIImage imageNamed:@"down_s"] forState:UIControlStateNormal];
        
        if ([sender tag] == 0) {
            [self sortVideoToursWithTime:NO];
            //videoTourArray = [VideoTour getAllByTimeAsc:true];
        } else if ([sender tag] == 1) {
            //videoTourArray = [VideoTour getAllByBoookmarkAsc:true];
            [self sortVideoToursWithBookMarks:NO];
        } else if ([sender tag] == 2) {
            //videoTourArray = [VideoTour getAllByViewAsc:true];
            [self sortVideoToursWithViews:NO];
        } else if ([sender tag] == 3) {
            //videoTourArray = [VideoTour getAllByShareAsc:true];
            [self sortVideoToursWithShare:NO];
        } else if ([sender tag] == 4) {
            //videoTourArray = [VideoTour getAllByAmountAsc:true];
        } 
        //[collViewMainVideo reloadData];
    } else {
        int rowNumber = (int)[sender tag]+1;
        lastSortedAttrIndex = rowNumber;
        lastSortedAttrUp = false;
        for (int i = 0; i<5; i++) {
            NSIndexPath * ip1 = [NSIndexPath indexPathForRow:i inSection:0];
            VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[sortView.tblView cellForRowAtIndexPath:ip1];
            [cell.btnFirst setImage:[UIImage imageNamed:@"up_us"] forState:UIControlStateNormal];
            [cell.btnSecond setImage:[UIImage imageNamed:@"down_us"] forState:UIControlStateNormal];
        }
        VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[sortView.tblView cellForRowAtIndexPath:ip];
        [cell.btnSecond setImage:[UIImage imageNamed:@"down_s"] forState:UIControlStateNormal];
        
        if ([sender tag] == 0) {
            [self sortAttractionsWithTime:NO];
            //videoTourArray = [VideoTour getAllByTimeAsc:true];
        } else if ([sender tag] == 1) {
            //videoTourArray = [VideoTour getAllByBoookmarkAsc:true];
            [self sortAttractionsWithBookMarks:NO];
        } else if ([sender tag] == 2) {
            //videoTourArray = [VideoTour getAllByViewAsc:true];
            [self sortAttractionsWithViews:NO];
        } else if ([sender tag] == 3) {
            //videoTourArray = [VideoTour getAllByShareAsc:true];
            [self sortAttractionsWithShare:NO];
        } else if ([sender tag] == 4) {
            //videoTourArray = [VideoTour getAllByAmountAsc:true];
            [self sortAttractionsWithReward:NO];
        } else if ([sender tag] == 5) {
            //videoTourArray = [VideoTour getAllByAttrCountAsc:true];
            [self sortAttractionsWithAttractions:NO];
        } else if ([sender tag] == 6) {
            //videoTourArray = [VideoTour getAllByAddToPlanAsc:true];
        }
        //[collViewMainAttr reloadData];
    }
    isSortShown = NO;
    [subMenuView removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)sortVideoToursWithBookMarks :(BOOL)isAscending {
    searchResultVideoTours = [[searchResultVideoTours sortedArrayUsingComparator:^NSComparisonResult(PlanModel *a, PlanModel *b) {
        int aBooks = [a.total_bookmark intValue];
        int bBooks = [b.total_bookmark intValue];
        
        if (isAscending) {
            return aBooks > bBooks;
        }
        else{
            return aBooks < bBooks;
        }
    }] mutableCopy];
    [collViewMainVideo reloadData];
}
-(void)sortVideoToursWithViews :(BOOL)isAscending {
    searchResultVideoTours = [[searchResultVideoTours sortedArrayUsingComparator:^NSComparisonResult(PlanModel *a, PlanModel *b) {
        int aBooks = [a.total_view intValue];
        int bBooks = [b.total_view intValue];
        
        if (isAscending) {
            return aBooks > bBooks;
        }
        else{
            return aBooks < bBooks;
        }
    }] mutableCopy];
    [collViewMainVideo reloadData];
}
-(void)sortVideoToursWithShare :(BOOL)isAscending {
    searchResultVideoTours = [[searchResultVideoTours sortedArrayUsingComparator:^NSComparisonResult(PlanModel *a, PlanModel *b) {
        int aBooks = [a.total_share intValue];
        int bBooks = [b.total_share intValue];
        
        if (isAscending) {
            return aBooks > bBooks;
        }
        else{
            return aBooks < bBooks;
        }
    }] mutableCopy];
    [collViewMainVideo reloadData];
}
-(void)sortVideoToursWithReward :(BOOL)isAscending {
    searchResultVideoTours = [[searchResultVideoTours sortedArrayUsingComparator:^NSComparisonResult(PlanModel *a, PlanModel *b) {
        int aBooks = [a.remuneration_amount intValue];
        int bBooks = [b.remuneration_amount intValue];
        
        if (isAscending) {
            return aBooks > bBooks;
        }
        else{
            return aBooks < bBooks;
        }
    }] mutableCopy];
    [collViewMainVideo reloadData];
}
-(void)sortVideoToursWithAttractions :(BOOL)isAscending {
    searchResultVideoTours = [[searchResultVideoTours sortedArrayUsingComparator:^NSComparisonResult(PlanModel *a, PlanModel *b) {
        int aBooks = [a.total_attractions intValue];
        int bBooks = [b.total_attractions intValue];
        
        if (isAscending) {
            return aBooks > bBooks;
        }
        else{
            return aBooks < bBooks;
        }
    }] mutableCopy];
    [collViewMainVideo reloadData];
}
-(void)sortVideoToursWithTime :(BOOL)isAscending {
    searchResultVideoTours = [[searchResultVideoTours sortedArrayUsingComparator:^NSComparisonResult(PlanModel *a, PlanModel *b) {
        NSString *afromDateStr = ([a.create_date length]>19 ? [a.create_date substringToIndex:19] : a.create_date);
        NSString *bfromDateStr = ([b.create_date length]>19 ? [b.create_date substringToIndex:19] : b.create_date);
        
        //2017-06-21T07:24:32.570Z
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        
        NSDate* afromDateNew = [dateFormatter dateFromString:afromDateStr];
        NSDate* bfromDateNew = [dateFormatter dateFromString:bfromDateStr];
        
        
        
        if (isAscending) {
            return [afromDateNew compare:bfromDateNew];
        }
        else{
            return [bfromDateNew compare:afromDateNew];
        }
    }] mutableCopy];
    [collViewMainVideo reloadData];
}



-(void)sortAttractionsWithBookMarks :(BOOL)isAscending {
    searchResultAttraction = [[searchResultAttraction sortedArrayUsingComparator:^NSComparisonResult(AttractionModel *a, AttractionModel *b) {
        int aBooks = [a.total_bookmark intValue];
        int bBooks = [b.total_bookmark intValue];
        
        if (isAscending) {
            return aBooks > bBooks;
        }
        else{
            return aBooks < bBooks;
        }
    }] mutableCopy];
    [collViewMainAttr reloadData];
}
-(void)sortAttractionsWithViews :(BOOL)isAscending {
    searchResultAttraction = [[searchResultAttraction sortedArrayUsingComparator:^NSComparisonResult(AttractionModel *a, AttractionModel *b) {
        int aBooks = [a.total_view intValue];
        int bBooks = [b.total_view intValue];
        
        if (isAscending) {
            return aBooks > bBooks;
        }
        else{
            return aBooks < bBooks;
        }
    }] mutableCopy];
    [collViewMainAttr reloadData];
}
-(void)sortAttractionsWithShare :(BOOL)isAscending {
    searchResultAttraction = [[searchResultAttraction sortedArrayUsingComparator:^NSComparisonResult(AttractionModel *a, AttractionModel *b) {
        int aBooks = [a.total_share intValue];
        int bBooks = [b.total_share intValue];
        
        if (isAscending) {
            return aBooks > bBooks;
        }
        else{
            return aBooks < bBooks;
        }
    }] mutableCopy];
    [collViewMainAttr reloadData];
}
-(void)sortAttractionsWithReward :(BOOL)isAscending {
    searchResultAttraction = [[searchResultAttraction sortedArrayUsingComparator:^NSComparisonResult(AttractionModel *a, AttractionModel *b) {
        int aBooks = [a.remuneration_amount intValue];
        int bBooks = [b.remuneration_amount intValue];
        
        if (isAscending) {
            return aBooks > bBooks;
        }
        else{
            return aBooks < bBooks;
        }
    }] mutableCopy];
    [collViewMainAttr reloadData];
}
-(void)sortAttractionsWithAttractions :(BOOL)isAscending {
    searchResultAttraction = [[searchResultAttraction sortedArrayUsingComparator:^NSComparisonResult(AttractionModel *a, AttractionModel *b) {
        int aBooks = [a.total_attractions intValue];
        int bBooks = [b.total_attractions intValue];
        
        if (isAscending) {
            return aBooks > bBooks;
        }
        else{
            return aBooks < bBooks;
        }
    }] mutableCopy];
    [collViewMainAttr reloadData];
}
-(void)sortAttractionsWithTime :(BOOL)isAscending {
    searchResultAttraction = [[searchResultAttraction sortedArrayUsingComparator:^NSComparisonResult(AttractionModel *a, AttractionModel *b) {
        
        NSString *afromDateStr = ([a.create_date length]>19 ? [a.create_date substringToIndex:19] : a.create_date);
        NSString *bfromDateStr = ([b.create_date length]>19 ? [b.create_date substringToIndex:19] : b.create_date);

        //2017-06-21T07:24:32.570Z
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        
        NSDate* afromDateNew = [dateFormatter dateFromString:afromDateStr];
        NSDate* bfromDateNew = [dateFormatter dateFromString:bfromDateStr];

    
        
        if (isAscending) {
            return [afromDateNew compare:bfromDateNew];
        }
        else{
            return [bfromDateNew compare:afromDateNew];
        }
    }] mutableCopy];
    [collViewMainAttr reloadData];
}
@end


#import <UIKit/UIKit.h>
#import "MenuButtonViewController.h"

@interface SearchTabViewController : MenuButtonViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate> {
    
    IBOutlet UILabel *lblVideo;
    IBOutlet UILabel *lblAttr;
    
    IBOutlet UITableView *tblVideo;
    IBOutlet UITableView *tblAttr;
    BOOL alreadyPushed;
    // Video Segment
    
    IBOutlet UIView *selfDriveView;
    IBOutlet UIButton *btnNo;
    IBOutlet UIButton *btnYes;
    
    IBOutlet UIButton *btnAttr;
    IBOutlet UIButton *btnVideoTour;
    
    NSMutableArray * arrLanguageVideo;
    NSMutableArray * arrMostSearchVideo;
    NSMutableArray * videoTourArray;
    
    NSString * leftDate;
    NSString * rightDate;
    
    UIButton *btnHeaderCheckVideo;
    
    BOOL selfDrive;
    BOOL chSelectedVideo, mnSelectedVideo,enSelectedVideo, fullSelectedVideo;
    
    
    
    //Attraction Segment
    NSMutableArray * arrLanguageAttr;
    NSMutableArray * arrCategoryAttr;
    NSMutableArray * arrMostSearchAttr;
    NSMutableArray * categoryListAttr;
    
    BOOL chSelectedAttr, mnSelectedAttr,enSelectedAttr, fullSelectedAttr;
    UIButton *btnHeaderCheckAttr;
}

- (IBAction)attrClicked:(id)sender;
- (IBAction)videoClicked:(id)sender;


- (IBAction)yesClicked:(id)sender;
- (IBAction)NoClicked:(id)sender;

@property (nonatomic) UISearchBar *searchBar;

+(SearchTabViewController *)initViewController;

@property (strong, nonatomic) IBOutlet UILabel *lblSelfDrive;
@property (strong, nonatomic) IBOutlet UILabel *lblYes;
@property (strong, nonatomic) IBOutlet UILabel *lblNo;





@end

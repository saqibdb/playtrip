
#import <UIKit/UIKit.h>
#import "PopularVideoSort.h"
#import "AttractionModel.h"

@interface RelatedAttractionsVideoViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDelegate, UITableViewDataSource> {
    
    IBOutlet UICollectionView *collViewMain;
    BOOL isSortShown;
    UIView * subMenuView;
    PopularVideoSort * sortView;
    int lastSortedIndex;
    BOOL lastSortedUp;
}

+(RelatedAttractionsVideoViewController *)initViewController;
@property (weak, nonatomic) AttractionModel *attModel;

@end

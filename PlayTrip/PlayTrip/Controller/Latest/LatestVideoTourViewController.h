
#import <UIKit/UIKit.h>
#import "MenuButtonViewController.h"

@interface LatestVideoTourViewController : MenuButtonViewController <UICollectionViewDelegate,UICollectionViewDataSource>{
    IBOutlet UICollectionView * collectionview;

}

+ (LatestVideoTourViewController *) initViewController;
@property (nonatomic) NSMutableArray * allLatestPlans;

@end

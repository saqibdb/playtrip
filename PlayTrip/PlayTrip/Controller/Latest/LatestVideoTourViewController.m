#import "LatestVideoTourViewController.h"
#import "ItineraryViewController.h"
#import "LatestVideoTourCollectionViewCell.h"
#import "SVProgressHUD.h"
#import "AccountManager.h"
#import "Account.h"
#import "VideoTour.h"
#import "PlayTripManager.h"
#import "Constant.h"
#import "Attractions.h"
#import "AttractionData.h"
#import "CommonUser.h"
#import "URLSchema.h"
#import "ColorConstants.h"
#import "AppDelegate.h"
#import "Info.h"
#import "PlanModel.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>

@interface LatestVideoTourViewController (){
    NSMutableArray *videoTourArray;
}
@end

@implementation LatestVideoTourViewController

+ (LatestVideoTourViewController *)initViewController{
    LatestVideoTourViewController * controller = [[LatestVideoTourViewController alloc]initWithNibName:@"LatestVideoTourViewController" bundle:nil];
    //controller.title = @"Latest Video Tour";
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (![[AppDelegate appDelegate] isReachable]) {
        [Utils showAlertWithMessage:kCheckInternet];
        return;
    }

    //videoTourArray = [[NSMutableArray alloc]init];
    //[self getLatestVideoTour];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = false;
    [self registerNibs];
    [self setNavBarItems];
}

-(void)registerNibs {
    [collectionview registerClass:[LatestVideoTourCollectionViewCell class] forCellWithReuseIdentifier:@"LatestVideoTourCollectionViewCell"];
    
    UINib *cellNib = [UINib nibWithNibName:@"LatestVideoTourCollectionViewCell" bundle:nil];
    [collectionview registerNib:cellNib forCellWithReuseIdentifier:@"LatestVideoTourCollectionViewCell"];
}

-(void)setNavBarItems {
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[ColorConstants appBrownColor]}];
    
    UIImage *buttonImage = [UIImage imageNamed:@"back.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(backClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
}

-(void)backClicked {
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark -<UICollectionViewDelegate,UICollectionViewDataSource>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.allLatestPlans.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    LatestVideoTourCollectionViewCell *cell = (LatestVideoTourCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"LatestVideoTourCollectionViewCell" forIndexPath:indexPath];
    Account * account = [AccountManager Instance].activeAccount;

    PlanModel * v = [self.allLatestPlans objectAtIndex:indexPath.row];
    NSMutableArray *attraction = [NSMutableArray new];
    NSMutableArray *attractionData = [NSMutableArray new];
    Attractions *a;
    AttractionData *aData;
    
    
    
    [cell.imgUserImg sd_setShowActivityIndicatorView:YES];
    [cell.imgUserImg sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [cell.imgUserImg sd_setImageWithURL:[NSURL URLWithString:v.user.avatar]
                       placeholderImage:[UIImage imageNamed:@"placeholder.png"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                           if (!error) {
                               
                           }
                       }];
    cell.lblFromDate.text = ([v.create_date length]>10 ? [v.create_date substringToIndex:10] : v.create_date);
    
    if(![v.cover_photo isEqualToString:@""]){
        NSString * imgId = v.cover_photo;
        NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
        cell.imgThumbnail.url = [NSURL URLWithString:urlString];
    }
    cell.lblLanguage.text = v.language;
    cell.lblVideoName.text = v.name;
    cell.lblUserName.text = v.user.full_name;

    cell.lblCountry.hidden = YES;
    cell.lblDistrict.hidden = YES;
    cell.lblCity.hidden = YES;
    
    for (AttractionModel *attraction in v.attractions) {
        if (attraction.attr_data.count) {
            AttractionDataModel *attratctionData = [attraction.attr_data objectAtIndex:0];
            cell.lblTime.text = attratctionData.time;
            if (attratctionData.info.country_id) {
                cell.lblCountry.text = attratctionData.info.country_id.name;
                cell.lblDistrict.text = attratctionData.info.district_id.name;
                cell.lblCity.text = attratctionData.info.city_id.name;
                cell.lblCountry.hidden = NO;
                cell.lblDistrict.hidden = NO;
                cell.lblCity.hidden = NO;
                
                break;
            }
        }
    }
    cell.lblBookmarkCount.text = [NSString stringWithFormat:@"%@",v.total_bookmark];
    cell.lblViewCount.text = [NSString stringWithFormat:@"%@",v.total_view];
    cell.lblShareCount.text = [NSString stringWithFormat:@"%@",v.total_share];
    cell.lblRevertCount.text = [NSString stringWithFormat:@"%@",v.remuneration_amount];
    
    [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
    cell.btnBookMark.selected = NO;
    if (account) {
        for (NSString *user in v.bookmark_users) {
            if ([user isEqualToString:account.userId]) {
                [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                cell.btnBookMark.selected = YES;
            }
        }
    }
    
    /*
    attraction = [[v.attractionsSet allObjects] mutableCopy];
    if(attraction.count>0){
        a =[attraction objectAtIndex:0];
    }
    attractionData = [[a.attractionDataSet allObjects] mutableCopy];
    if(attractionData.count>0){
        aData = [attractionData objectAtIndex:0];
    }
    
    if (account) {
        if ([v.bookmark_users_string containsString:account.userId]) {
            [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
            cell.btnBookMark.selected = YES;
        } else {
            [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
            cell.btnBookMark.selected = NO;
        }
    } else {
        [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
        cell.btnBookMark.selected = NO;
    }
     
    attractionData = [[a.attractionDataSet allObjects] mutableCopy];
    if(attractionData.count>0){
        aData = [attractionData objectAtIndex:0];
        cell.lblDescription.text = [NSString stringWithFormat:@"[%@ days] %@",v.days, aData.info.address];
    } else {
        cell.lblDescription.text = [NSString stringWithFormat:@"[%@ days]",v.days];
    }

    cell.lblFromDate.text = [Utils dateFormatyyyyMMdd:v.from_date];
    cell.lblVideoName.text = v.name;
    cell.lblUserName.text = v.user.full_name;
   
    if(![v.user.avatar isEqualToString:@""]){
        cell.imgUserImg.layer.cornerRadius = cell.imgUserImg.frame.size.width / 2;
        cell.imgUserImg.layer.masksToBounds = true;
        NSString * imgId = v.user.avatar;
        NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
        cell.imgUserImg.url = [NSURL URLWithString:urlString];
    }else{
        cell.imgUserImg.backgroundColor = [UIColor blueColor];
    }
    cell.btnBookMark.tag = indexPath.row;
    [cell.btnBookMark addTarget:self action:@selector(bookmarkClickedVideoTour:) forControlEvents:UIControlEventTouchUpInside];
    cell.lblBookmarkCount.text = [NSString stringWithFormat:@"%@",v.total_bookmark];
    cell.lblViewCount.text = [NSString stringWithFormat:@"%@",v.total_view];
    cell.lblRevertCount.text = [NSString stringWithFormat:@"%@",v.remuneration_amount];
    cell.lblShareCount.text = [NSString stringWithFormat:@"%@",v.total_share];
     */
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    PlanModel *plan = [_allLatestPlans objectAtIndex:indexPath.row];
    //VideoTour *v = [arrVideoTour objectAtIndex:indexPath.row];
    ItineraryViewController * controller = [ItineraryViewController initViewControllerWithPlan:plan];
    [self.navigationController  pushViewController:controller animated:YES];
    
    //VideoTour *v = [videoTourArray objectAtIndex:indexPath.row];
    //ItineraryViewController * controller = [ItineraryViewController initViewController:v];
    //[self.navigationController  pushViewController:controller animated:YES];
}

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    return CGSizeMake((collectionView.frame.size.width/2)-8, (collectionView.frame.size.height/2)-8);
//}

-(void)bookmarkClickedVideoTour:(id)sender {
    VideoTour * v = [videoTourArray objectAtIndex:[sender tag]];
    [self commanApiWithModel:ModelPlan WithId:v.entity_id WithType:TypeBookmark];
}


-(void)commanApiWithModel:(NSString *)modelValue WithId:(NSString *)idValue WithType:(NSString *)typeValue {
    [SVProgressHUD showWithStatus:@"Updating Bookmark"];

    [[PlayTripManager Instance] addCountForModel:modelValue withType:typeValue forId:idValue WithBlock:^(id result, NSString *error) {
        if(!error){
            [self getLatestVideoTour];
           [[NSNotificationCenter defaultCenter] postNotificationName:@"LatestVideoTourUpdated" object:nil];
        }
        [SVProgressHUD dismiss];
    }];
}

-(void)getLatestVideoTour{
    NSMutableDictionary *where = [NSMutableDictionary new];
    [where setObject:@"true" forKey:@"is_published"];
    [where setObject:[NSNumber numberWithBool:false] forKey:@"is_deleted"];
    NSMutableDictionary *sort = [NSMutableDictionary new];
    [sort setObject:@"-1" forKey:@"create_date"];
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Please wait.."];
    
    [[PlayTripManager Instance] getLatestVideoTourWithWhere:where WithSort:sort WithBlock:^(id result, NSString *error) {
        if(!error){
            
            videoTourArray = [VideoTour getAll];
            [collectionview reloadData];
        }
        [SVProgressHUD dismiss];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

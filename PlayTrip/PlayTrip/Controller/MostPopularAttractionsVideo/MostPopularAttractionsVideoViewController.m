
#import "MostPopularAttractionsVideoViewController.h"
#import "SVProgressHUD.h"
#import "CommonUser.h"
#import "AttractionsVideoCell.h"
#import "MostPopularAttractions.h"
#import "LatestVideoTourCollectionViewCell.h"
#import "VideoSortTableViewCell.h"
#import "ColorConstants.h"
#import "AccountManager.h"
#import "Account.h"
#import "URLSchema.h"
#import "Attractions.h"
#import "AttractionData.h"
#import "VideoTour.h"
#import "PlayTripManager.h"
#import "Constants.h"
#import "AppDelegate.h"

@interface MostPopularAttractionsVideoViewController (){
    NSMutableArray *videoTourArray;
}
@end

@implementation MostPopularAttractionsVideoViewController

+ (MostPopularAttractionsVideoViewController *)initViewController{
    MostPopularAttractionsVideoViewController * controller = [[MostPopularAttractionsVideoViewController alloc]initWithNibName:@"MostPopularAttractionsVideoViewController" bundle:nil];
    controller.title = @"Most Popular Attractions Video";
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];

}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = false;
    
    [collViewMain registerClass:[AttractionsVideoCell class] forCellWithReuseIdentifier:@"AttractionsVideoCell"];
    [collViewMain registerNib:[UINib nibWithNibName:@"AttractionsVideoCell" bundle:nil] forCellWithReuseIdentifier:@"AttractionsVideoCell"];
    [self setNavBarItems];
    
    if (![[AppDelegate appDelegate] isReachable]) {
        [Utils showAlertWithMessage:kCheckInternet];
        return;
    }
    [self getMostPopularattractions];
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    sortView = [PopularVideoSort initWithNib];
    sortView.tblView.delegate = self;
    sortView.tblView.dataSource = self;
}

-(void) setNavBarItems {
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[ColorConstants appBrownColor]}];
    UIImage *buttonImage = [UIImage imageNamed:@"back.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(backClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
    
    self.navigationController.navigationBar.barTintColor = [ColorConstants appYellowColor];
    self.navigationController.navigationBar.translucent = NO;
    
    UIImage *mapImage = [UIImage imageNamed:@"sort_descending.png"];
    UIButton *btnRight1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight1 setImage:mapImage forState:UIControlStateNormal];
    btnRight1.frame = CGRectMake(0, 0, mapImage.size.width, mapImage.size.height);
    [btnRight1 addTarget:self action:@selector(sortClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarItem1 = [[UIBarButtonItem alloc] initWithCustomView:btnRight1];
    self.navigationItem.rightBarButtonItem = rightBarItem1;
   
    
}

-(void)sortClicked {
    if (isSortShown) {
        isSortShown = NO;
        [subMenuView removeFromSuperview];
    } else {
        isSortShown = YES;
        CGFloat height;
        height = (4 * 40);// Arun :: number of rows * height of row
        subMenuView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - sortView.frame.size.width, 0, sortView.frame.size.width, height)];
        
        subMenuView.layer.borderWidth = 0.5;
        subMenuView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        [subMenuView addSubview:sortView];
        subMenuView.clipsToBounds = YES;
        [self.view addSubview:subMenuView];
    }
    [sortView.tblView reloadData];
}

-(void)getMostPopularattractions {
    arrMostPopularAttractions=[NSMutableArray new];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Please wait.."];
    [[PlayTripManager Instance]getMostPopularAttractionsWithBlock:^(id result, NSString *error) {
        
        if(!error){
        arrMostPopularAttractions = [MostPopularAttractions getAll];
            [collViewMain reloadData];
        }
        [SVProgressHUD dismiss];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MostPopAttrUpdated" object:nil];
        
    }];
 
}

-(void)backClicked {
//    [self.navigationController dismissViewControllerAnimated:true completion:nil];
    [self.navigationController popToRootViewControllerAnimated:true];
}

-(void)bookmarkClickedMostPopularAttraction:(UIButton *)sender {
    MostPopularAttractions * att = [arrMostPopularAttractions objectAtIndex:[sender tag]];
    [self commanApiWithModel:ModelAttraction WithId:att.entity_id WithType:TypeBookmark];
}

#pragma mark -<UICollectionViewDelegate,UICollectionViewDataSource>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return arrMostPopularAttractions.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    Account * account = [AccountManager Instance].activeAccount;
    
    AttractionsVideoCell *cell = (AttractionsVideoCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"AttractionsVideoCell" forIndexPath:indexPath];
    MostPopularAttractions * att = [arrMostPopularAttractions objectAtIndex:indexPath.row];
    if (account) {
        if ([att.bookmark_users_string containsString:account.userId]) {
            [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
            cell.btnBookMark.selected = YES;
        } else {
            [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
            cell.btnBookMark.selected = NO;
        }
    } else {
        [cell.btnBookMark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
        cell.btnBookMark.selected = NO;
    }
    
    cell.lblFromDate.text = [Utils dateFormatyyyyMMdd:att.create_date];
    cell.lblVideoName.text = att.name;
    cell.lblUserName.text = att.userMostPopularAttractionData.full_name;
    
    if(![att.userMostPopularAttractionData.avatar isEqualToString:@""]){
        NSString * imgId = att.userMostPopularAttractionData.avatar;
        NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
        cell.imgUserImg.url = [NSURL URLWithString:urlString];
        cell.imgUserImg.layer.cornerRadius = cell.imgUserImg.frame.size.width / 2;
        cell.imgUserImg.layer.masksToBounds = YES;
    }else{
        cell.imgUserImg.backgroundColor = [UIColor blueColor];
        cell.imgUserImg.layer.cornerRadius = cell.imgUserImg.frame.size.width / 2;
        cell.imgUserImg.layer.masksToBounds = YES;
        
    }
    //        if(![att.thumbnail isEqualToString:@""]){
    //            NSString * imgId = v.thumbnail;
    //            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
    //            cell.imgThumbnail.url = [NSURL URLWithString:urlString];
    //        }else{
    //            //            cell.imgThumbnail.image = [UIImage imageNamed:@"mexico.jpg"];
    //        }
    
    
    cell.btnBookMark.tag = indexPath.row;
    [cell.btnBookMark addTarget:self action:@selector(bookmarkClickedMostPopularAttraction:) forControlEvents:UIControlEventTouchUpInside];
    cell.lblTTotalBookMark.text = [NSString stringWithFormat:@"%@",att.total_bookmark];
    cell.lblTTotalView.text = [NSString stringWithFormat:@"%@",att.total_view];
    cell.lblTTotalShare.text = [NSString stringWithFormat:@"%@",att.total_share];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
}

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    return CGSizeMake((collectionView.frame.size.width/2)-10, (collectionView.frame.size.height/2)-10);
//}
-(void)bookmarkClickedVideoTour:(id)sender {
    VideoTour * v = [videoTourArray objectAtIndex:[sender tag]];
    [self commanApiWithModel:ModelPlan WithId:v.entity_id WithType:TypeBookmark];
}
-(void)commanApiWithModel:(NSString *)modelValue WithId:(NSString *)idValue WithType:(NSString *)typeValue {
    [[PlayTripManager Instance] addCountForModel:modelValue withType:typeValue forId:idValue WithBlock:^(id result, NSString *error) {
        if(!error){
            [self getMostPopularattractions];
        
        }
    }];
}

#pragma Sort View - Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"VideoSortTableViewCell" forIndexPath:indexPath];
    cell.btnFirst.layer.cornerRadius = 5.0;
    cell.btnSecond.layer.cornerRadius = 5.0;
    
    if (indexPath.row == 0) {
        cell.lblText.text = @"Title";
    }else if (indexPath.row == 1){
        cell.lblText.text = @"Time";
    }else if (indexPath.row == 2){
        cell.lblText.text = @"Bookmark";
    }else if (indexPath.row == 3){
        cell.lblText.text = @"Rewards";
    }else if (indexPath.row == 4){
        cell.lblText.text = @"Add to My Plan";
    }
    
    if (indexPath.row == 0) {
        [cell.btnFirst setImage:[UIImage imageNamed:@"a_z_us"] forState: UIControlStateNormal];
    }else {
        [cell.btnFirst setImage:[UIImage imageNamed:@"up_us"] forState: UIControlStateNormal];
    }
    cell.btnFirst.tag = indexPath.row;
    cell.btnSecond.tag = indexPath.row;
    
    [cell.btnFirst addTarget:self action:@selector(btnFirstClickedSort:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnSecond addTarget:self action:@selector(btnSecondClickedSort:) forControlEvents:UIControlEventTouchUpInside];
    
    if (indexPath.row == 0) {
        [cell.btnSecond setImage:[UIImage imageNamed:@"z_a_us"] forState:UIControlStateNormal];
    }else {
        [cell.btnSecond setImage:[UIImage imageNamed:@"down_us"] forState:UIControlStateNormal];
    }
    
    if (lastSortedIndex) {
        if ((int)indexPath.row == lastSortedIndex-1) {
            if (lastSortedUp) {
                if (indexPath.row == 0) {
                    [cell.btnFirst setImage:[UIImage imageNamed:@"a_z_s"] forState: UIControlStateNormal];
                }else {
                    [cell.btnFirst setImage:[UIImage imageNamed:@"up_s"] forState: UIControlStateNormal];
                }
            } else {
                if (indexPath.row == 0) {
                    [cell.btnSecond setImage:[UIImage imageNamed:@"z_a_s"] forState:UIControlStateNormal];
                }else {
                    [cell.btnSecond setImage:[UIImage imageNamed:@"down_s"] forState:UIControlStateNormal];
                }
            }
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)btnFirstClickedSort:(id)sender {
    NSIndexPath * ip = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    
    lastSortedIndex = (int)[sender tag]+1;
    lastSortedUp = true;
    
    for (int i = 0; i<5; i++) {
        NSIndexPath * ip1 = [NSIndexPath indexPathForRow:i inSection:0];
        VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[sortView.tblView cellForRowAtIndexPath:ip1];
        if (i == 0) {
            [cell.btnFirst setImage:[UIImage imageNamed:@"a_z_us"] forState:UIControlStateNormal];
            [cell.btnSecond setImage:[UIImage imageNamed:@"z_a_us"] forState:UIControlStateNormal];
        }else {
            [cell.btnFirst setImage:[UIImage imageNamed:@"up_us"] forState:UIControlStateNormal];
            [cell.btnSecond setImage:[UIImage imageNamed:@"down_us"] forState:UIControlStateNormal];
        }
        
    }
    VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[sortView.tblView cellForRowAtIndexPath:ip];
    if ([sender tag] == 0) {
        [cell.btnFirst setImage:[UIImage imageNamed:@"a_z_s"] forState:UIControlStateNormal];
    }else {
        [cell.btnFirst setImage:[UIImage imageNamed:@"up_s"] forState:UIControlStateNormal];
    }
    
    
//    if ([sender tag] == 0) {
//        videoTourArray = [MostPopularVideoTour getAllByTitleAsc:true];
//    } else if ([sender tag] == 1) {
//        videoTourArray = [MostPopularVideoTour getAllByTimeAsc:true];
//    } else if ([sender tag] == 2) {
//        videoTourArray = [MostPopularVideoTour getAllByBoookmarkAsc:true];
//    } else if ([sender tag] == 3) {
//        videoTourArray = [MostPopularVideoTour getAllByRewardsAsc:true];
//    } else if ([sender tag] == 4) {
//        videoTourArray = [MostPopularVideoTour getAllByAddToPlanAsc:true];
//    }
    
    [collViewMain reloadData];
    isSortShown = NO;
    [subMenuView removeFromSuperview];
}

-(void)btnSecondClickedSort:(id)sender {
    NSIndexPath * ip = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    
    lastSortedIndex = (int)[sender tag]+1;
    lastSortedUp = false;
    for (int i = 0; i<5; i++) {
        NSIndexPath * ip1 = [NSIndexPath indexPathForRow:i inSection:0];
        VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[sortView.tblView cellForRowAtIndexPath:ip1];
        if (i == 0) {
            [cell.btnFirst setImage:[UIImage imageNamed:@"a_z_us"] forState:UIControlStateNormal];
            [cell.btnSecond setImage:[UIImage imageNamed:@"z_a_us"] forState:UIControlStateNormal];
        }else {
            [cell.btnFirst setImage:[UIImage imageNamed:@"up_us"] forState:UIControlStateNormal];
            [cell.btnSecond setImage:[UIImage imageNamed:@"down_us"] forState:UIControlStateNormal];
        }
        
    }
    VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[sortView.tblView cellForRowAtIndexPath:ip];
    if ([sender tag] == 0) {
        [cell.btnSecond setImage:[UIImage imageNamed:@"z_a_s"] forState:UIControlStateNormal];
    }else {
        [cell.btnSecond setImage:[UIImage imageNamed:@"down_s"] forState:UIControlStateNormal];
    }
    
    
//    if ([sender tag] == 0) {
//        videoTourArray = [MostPopularVideoTour getAllByTitleAsc:false];
//    } else if ([sender tag] == 1) {
//        videoTourArray = [MostPopularVideoTour getAllByTimeAsc:false];
//    } else if ([sender tag] == 2) {
//        videoTourArray = [MostPopularVideoTour getAllByBoookmarkAsc:false];
//    } else if ([sender tag] == 3) {
//        videoTourArray = [MostPopularVideoTour getAllByRewardsAsc:false];
//    } else if ([sender tag] == 4) {
//        videoTourArray = [MostPopularVideoTour getAllByAddToPlanAsc:false];
//    }
    [collViewMain reloadData];
    isSortShown = NO;
    [subMenuView removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end


#import <UIKit/UIKit.h>
#import "PopularVideoSort.h"

@interface MostPopularAttractionsVideoViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UITableViewDataSource, UITableViewDelegate> {
    IBOutlet UICollectionView *collViewMain;
    PopularVideoSort *sortView;
    BOOL isSortShown;
    UIView * subMenuView;
    NSMutableArray * arrMostPopularAttractions;

    int lastSortedIndex;
    BOOL lastSortedUp;
}

+ (MostPopularAttractionsVideoViewController *)initViewController;

@end

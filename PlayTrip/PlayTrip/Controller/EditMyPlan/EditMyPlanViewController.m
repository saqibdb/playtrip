
#import "EditMyPlanViewController.h"
#import "ItineraryTableViewCell.h"
#import "ColorConstants.h"
#import "PlayTripManager.h"
#import "Account.h"
#import "AccountManager.h"
#import "Plan.h"
#import "ActionSheetPicker.h"
#import "ActionSheetDatePicker.h"
#import "Utils.h"
#import "Attractions.h"
#import "EditMyPlanSecondCell.h"
#import "EditMyPlanFirstCell.h"
#import "Constant.h"
#import "URLSchema.h"
#import "EditFirstDay.h"
#import "KGModalWrapper.h"
#import "Categori.h"
#import "PlaceInformationViewController.h"
#import "Info.h"
#import "Localisator.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <SDWebImage/UIButton+WebCache.h>

static char myDateKey;
static char myVideoKey;



@interface EditMyPlanViewController (){
    NSMutableArray *attractionArray;
    NSMutableArray *languagesArray;
    NSMutableArray *categoryListAttr;
    NSMutableArray *attDataArray;
    
    BOOL isChanged ;
    int segmentViewSelected;
}

@end

@implementation EditMyPlanViewController

+(EditMyPlanViewController *)initViewController:(Plan *)plan {
    EditMyPlanViewController * controller = [[EditMyPlanViewController alloc] initWithNibName:@"EditMyPlanViewController" bundle:nil];
    controller.objplan = plan;
    controller.title = LOCALIZATION(@"edit_my_plan");
    
     controller->lblTopMain1 =  [Utils roundCornersOnView:controller->lblTopMain1 onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];
    controller->lblTopMain2 =  [Utils roundCornersOnView:controller->lblTopMain2 onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];
    controller->lblTopMain3 =  [Utils roundCornersOnView:controller->lblTopMain3 onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];
    
    return controller;
}

+(EditMyPlanViewController *)initViewControllerWithPlanModel:(PlanModel *)plan {
    EditMyPlanViewController * controller = [[EditMyPlanViewController alloc] initWithNibName:@"EditMyPlanViewController" bundle:nil];
    controller.planModel = plan;
    controller.title = LOCALIZATION(@"edit_my_plan");
    
    controller->lblTopMain1 =  [Utils roundCornersOnView:controller->lblTopMain1 onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];
    controller->lblTopMain2 =  [Utils roundCornersOnView:controller->lblTopMain2 onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];
    controller->lblTopMain3 =  [Utils roundCornersOnView:controller->lblTopMain3 onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];
    
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavBarItems];
    
    segmentViewSelected = 0;
    webViewOuterView.hidden = YES;
    webViewHotelOuterView.hidden = YES;
    
    //https://secure.rentalcars.com/WidgetSearch.do?affiliateCode=rentalcars&preflang=en&pickupIATACode=MAN&results=3&pickupMonth=8&pickupDate=5&pickupYear=2015&puHour=9&pickupHour=9&pickupMinute=15&returnDate=13&returnMonth=8&returnYear=2015&returnHour=9&returnMinute=15&maxOccupancy=3
    Account * account = [AccountManager Instance].activeAccount;
    if(account){
    
        NSString *address = @"https://secure.rentalcars.com/WidgetSearch.do?";
        
        NSString *params1 = [NSString stringWithFormat: @"affiliateCode=playtrip&preflang=%@&pickupIATACode=HKG&results=5&prefcurrency=USD",account.language];
        
        // URL encode the problematic part of the url.
        NSString *params2 = [NSString stringWithFormat:@"&pickupMonth=10&pickupDate=5&pickupYear=2017&puHour=9&pickupHour=9&pickupMinute=15&returnDate=13&returnMonth=8&returnYear=2015&returnHour=9&returnMinute=15&maxOccupancy=3"];
        params2 = [self escape:params2];
        
        // Build the url and loadRequest
        NSString *urlString = [NSString stringWithFormat:@"%@%@%@",address,params1,params2];
        NSLog(@"URL = %@", urlString);
        [carRentalWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
        
        
        //https://www.hotelscombined.com/?a_aid=181392&brandid=536404&languageCode=EN
    [hotelWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"https://www.hotelscombined.com/?a_aid=181392&brandid=536404&languageCode=EN"]]];
    }
    
    
   
    
    carRentalWebView.delegate = self;
    hotelWebView.delegate = self;
    

        languagesArray =[NSMutableArray arrayWithObjects:@"Chineese",
                     @"Mandarin",@"English", nil];
   
    self.navigationController.navigationBar.barTintColor = [ColorConstants appYellowColor];
    
    tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    topTblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    UILongPressGestureRecognizer *lpgr
    = [[UILongPressGestureRecognizer alloc]
       initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.delegate = self;
    lpgr.delaysTouchesBegan = YES;
    [tblView addGestureRecognizer:lpgr];
    
    self.navigationController.navigationBarHidden = false;
    
    [self registerNib];
    
   
    isChanged = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopVideoPlay) name:@"HideKGView" object:nil];

}


- (void)webViewDidStartLoad:(UIWebView *)webView{
    NSLog(@"LOAD STARTED");
    if ([webView isEqual:carRentalWebView]) {
        [webLoader startAnimating];
        webLoader.hidden = NO;
    }
    
    if ([webView isEqual:hotelWebView]) {
        [webHotelLoader startAnimating];
        webHotelLoader.hidden = NO;
    }
    
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    NSLog(@"LOAD FINISHED");
    
    if ([webView isEqual:carRentalWebView]) {
        [webLoader stopAnimating];
        webLoader.hidden = YES;
    }
    
    if ([webView isEqual:hotelWebView]) {
        [webHotelLoader stopAnimating];
        webHotelLoader.hidden = YES;
    }
    
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    NSLog(@"LOAD FAILED");
    if ([webView isEqual:carRentalWebView]) {
        [webLoader stopAnimating];
        webLoader.hidden = YES;
    }
    
    if ([webView isEqual:hotelWebView]) {
        [webHotelLoader stopAnimating];
        webHotelLoader.hidden = YES;
    }
}


- (NSString *)escape:(NSString *)text
{
    return (__bridge NSString *)CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                        (__bridge CFStringRef)text, NULL,
                                                                        (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                        kCFStringEncodingUTF8);
}

-(void)stopVideoPlay {
    [self.videoController stop];
}


-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = false;

    btnOneDay.layer.cornerRadius =btnOneDay.layer.frame.size.height / 2;
    Account * account = [AccountManager Instance].activeAccount;
    if(account){
        lblUserName.text = account.full_name;
        if(![account.avatar isEqualToString:@""]){
            NSString * imgId = account.avatar;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            userImage.url = [NSURL URLWithString:urlString];
            userImage.layer.cornerRadius = userImage.frame.size.width/2;
            userImage.layer.masksToBounds = YES;
        }else{
            userImage.layer.cornerRadius = userImage.frame.size.width/2;
            userImage.layer.masksToBounds = YES;
        }
    }
    
    
    
    /*
    
   
    fromDate = self.objplan.from_date; //[NSDate date];
    toDate = self.objplan.to_date; //[NSDate date];
    planDays = self.objplan.days;
    fromDay = [Utils getDayFromDate:fromDate];
    toDay = [Utils getDayFromDate:toDate];
     timeString = [Utils timeFormatHHmm:fromDate];
    
    
    
    self.navigationController.navigationBarHidden = false;
    [self getAllCategory];
    
    //    arrattractions  = [[NSMutableArray alloc] init];
    //    arrattractions = [(NSMutableArray *) [self.objplan.attractionsSet allObjects] mutableCopy];
    //    arrData = [[NSMutableArray alloc] init];
    */
}
-(void)setNavBarItems {
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[ColorConstants appBrownColor]}];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backClicked)];
    self.navigationItem.leftBarButtonItem = backButton;
    self.navigationController.navigationBar.tintColor = [ColorConstants appBrownColor];
    self.navigationController.navigationBar.translucent = NO;
    
    UIBarButtonItem *saveButton = [[UIBarButtonItem alloc]initWithTitle:LOCALIZATION(@"save") style:UIBarButtonItemStylePlain target:self action:@selector(saveClicked:)];
    saveButton.tintColor = [ColorConstants appBrownColor];
    [self.navigationItem setRightBarButtonItem:saveButton];
}

-(void)backClicked {
    if (isChanged) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Unsaved Changes" message:@"Do you want to Discard the unsaved changed?" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"YES" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [alertController addAction:actionOk];
        
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }];
        [alertController addAction:cancelAction];
        
        [[Utils getTopViewController] presentViewController:alertController animated:YES completion:nil];
    }
    else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)oneDayClicked:(id)sender {
    
    int value = [planDays intValue];
    planDays = [NSNumber numberWithInt:value + 1];
    
    //    planDays = planDays+1;
    [tblView reloadData];
}

- (IBAction)cancleClicked:(id)sender {
    [KGModalWrapper hideView];
}

- (IBAction)viewSaveClicked:(id)sender {
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
//    fromDate = [dateFormatter dateFromString:dateString];
    if(fromDate){
        fromDay = [Utils getDayFromDate:fromDate];
        [topTblView reloadData];
    }
    
    [KGModalWrapper hideView];
}

- (IBAction)datepickerClicked:(id)sender {
    
    [ActionSheetDatePicker showPickerWithTitle:@"Select Date" datePickerMode:UIDatePickerModeDate selectedDate:fromDate minimumDate:nil maximumDate:toDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        fromDate = selectedDate;
        dateString=[Utils dateFormatMMddyyyy:selectedDate];
        _lblViewMonth.text=[Utils dateFormatMMMyyyy:selectedDate];
        NSArray *dateArray = [dateString componentsSeparatedByString:@"/"];
        _lblViewDay.text = [dateArray objectAtIndex:1];
        _lblViewDayName.text =[NSString stringWithFormat:@"(%@)", [Utils getDayFromDate:selectedDate]];
    } cancelBlock:nil origin:sender];
}

- (IBAction)addNoteSaveAction:(id)sender {
    [KGModalWrapper hideView];
}

- (IBAction)addNoteCancelAction:(id)sender {
    [KGModalWrapper hideView];
}

- (void)timepickerClicked:(id)sender {
    NSString *myData =
    (NSString *)objc_getAssociatedObject(sender, &myDateKey);
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
    NSDate *dateSent = [dateFormatter dateFromString:myData];
    

    if (dateSent) {
        [ActionSheetDatePicker showPickerWithTitle:@"Select Time" datePickerMode:UIDatePickerModeTime selectedDate:dateSent minimumDate:nil maximumDate:nil doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
            timeString=[Utils timeFormatHHmm:selectedDate];
            [tblView reloadData];
            
        } cancelBlock:nil origin:sender];
    }
    
}


-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer{
    [tblView setEditing:YES animated:true];
}
- (void)registerNib{
    /*
     this function is used for register all NIB for tableView
     */
    
    UINib *celliterery = [UINib nibWithNibName:@"ItineraryTableViewCell" bundle:nil];
    [tblView registerNib:celliterery forCellReuseIdentifier:@"ItineraryTableViewCell"];
    UINib *cellSecond = [UINib nibWithNibName:@"EditMyPlanSecondCell" bundle:nil];
    [tblView registerNib:cellSecond forCellReuseIdentifier:@"EditMyPlanSecondCell"];
    UINib *cellFirst = [UINib nibWithNibName:@"EditMyPlanFirstCell" bundle:nil];
    [tblView registerNib:cellFirst forCellReuseIdentifier:@"EditMyPlanFirstCell"];
}

- (IBAction)saveClicked:(id)sender{
    
    
    Account * account = [AccountManager Instance].activeAccount;
    if(account){
        if (account.isSkip == true) {
            [[AppDelegate appDelegate] showLogin:self];
        }else {
            //[self editPlan];
            
            [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
            [SVProgressHUD showWithStatus:@"Saving Plan..."];
            
            [[PlayTripManager Instance] saveEditedPlanForPlan:self.planModel withBlockNew:^(id result, NSString *error) {
                [SVProgressHUD dismiss];
                if (!error) {
                    NSError* errorData;
                    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:result
                                                                         options:kNilOptions
                                                                           error:&errorData];
                    if (errorData) {
                        NSLog(@"ERROR GOTTEN = %@" ,errorData.description);
                    }
                    NSArray* locationDicts = [json objectForKey:@"data"];
                    NSLog(@"Saved Plan = %lu",(unsigned long)locationDicts.count);
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.navigationController popViewControllerAnimated:YES];
                    });

                }
                else{
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Error Saving" message:@"There is an Error saving Plan. Please Try again Later." preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
                    [alertController addAction:actionOk];
                
                    [[Utils getTopViewController] presentViewController:alertController animated:YES completion:nil];
                }
            }];
        }
    }else{
        [[AppDelegate appDelegate] showLogin:self];
    }
}

-(void)editPlan {
    Account *account = [AccountManager Instance].activeAccount;
    [[PlayTripManager Instance]editMyPlanWithPlanId:@"" WithUser:account.userId withName:txtPlanName.text withFromDate:[Utils dateFormatMMddyyyy:fromDate] withDays:@"" withBlock:^(id result, NSString *error) {
        
    }];
}
- (void)editClicked:(id)sender {
    
    [KGModalWrapper showWithContentView:firstDayView];
    _dayView.layer.cornerRadius = _dayView.layer.frame.size.height / 2;
    _dayView.layer.borderColor= [ColorConstants appBrownColor].CGColor;
    _dayView.layer.borderWidth=1;
    
    _monthView.layer.cornerRadius = _monthView.layer.frame.size.height / 2;
    _monthView.layer.borderColor= [ColorConstants appBrownColor].CGColor;
    _monthView.layer.borderWidth=1;
    
    _lblViewMonth.text=[Utils dateFormatMMMyyyy:fromDate];
    NSArray *dateArray = [[Utils dateFormatMMddyyyy:fromDate] componentsSeparatedByString:@"/"];
    _lblViewDay.text = [dateArray objectAtIndex:1];
    _lblViewDayName.text =[NSString stringWithFormat:@"(%@)", [Utils getDayFromDate:fromDate]];
}


- (void)languageClicked:(id)sender {
    [KGModalWrapper showWithContentView:editLanguageView];
    
    [_tblLanguage reloadData];
    [tblView reloadData];
}


- (void) checkClicked:(id)sender{
    if(checked){
        checked = false;
    } else {
        checked = true;
    }
    [sender setImage:[UIImage imageNamed:((checked) ? @"circle_s.png" : @"circle_us.png")] forState:UIControlStateNormal];
}

- (void)categoryClicked:(id)sender {
    [KGModalWrapper showWithContentView:editAttractionTypeview];
    
    
}
-(void)getAllCategory {
    categoryListAttr = [NSMutableArray new];
    [[PlayTripManager Instance]loadCategory:^(id result, NSString *error) {
        if(!error){
            categoryListAttr = [Categori getAll];
            [tblViewAttType reloadData];
        }
    }];
}

#pragma mark - <LoginDelegate>
-(void)loginSuccessfully {
    [self editPlan];
}


#pragma mark -- TableView Delegate and dataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if(tableView == _tblLanguage){
        return 1;
    }else if(tableView == tblViewAttType){
        return 1;
    }else if(tableView == topTblView){
        return 1;
    }else{
        return [self.planModel.days intValue];
    }
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == _tblLanguage){
        return 0;
    }else if(tableView == tblViewAttType){
        return 3;
    }else if(tableView == topTblView){
        return 2;
    }else{
        if(self.planModel.attractions.count <=0){
            return 0;
        }

        for (AttractionModel *attraction in self.planModel.attractions) {
            if ([attraction.day intValue] == section+1) {
                return attraction.attr_data.count;
                break;
            }
        }
        
        return 0;
        //return 0;

    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    if(tableView == _tblLanguage){
        static NSString *languageCellIdentifier = @"Cell";
        
        UITableViewCell *languageCell = [tableView dequeueReusableCellWithIdentifier:languageCellIdentifier];
        if (languageCell == nil) {
            languageCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:languageCellIdentifier];
        }
        [languageCell setSelectionStyle:UITableViewCellSelectionStyleNone];

        //languageCell.textLabel.text = [languagesArray objectAtIndex:indexPath.row];
        return languageCell;
    }else if(tableView == tblViewAttType){
        static NSString *typeCellIdentifier = @"Cell";
        
        UITableViewCell *typeCell = [tableView dequeueReusableCellWithIdentifier:typeCellIdentifier];
        if (typeCell == nil) {
            typeCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:typeCellIdentifier];
        }
         [typeCell setSelectionStyle:UITableViewCellSelectionStyleNone];
        //Categori * cat = [categoryListAttr objectAtIndex: indexPath.row];
        
        //typeCell.textLabel.text = cat.name;
        return typeCell;
        
    }else if(tableView == topTblView){
        if(indexPath.row == 0){
            
            EditMyPlanFirstCell *topFirstCell = (EditMyPlanFirstCell *)[tblView dequeueReusableCellWithIdentifier:@"EditMyPlanFirstCell"];
            [topFirstCell setSelectionStyle:UITableViewCellSelectionStyleNone];
            if (topFirstCell == nil) {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EditMyPlanFirstCell" owner:nil options:nil];
                topFirstCell = (EditMyPlanFirstCell *)[nib objectAtIndex:0];
            }
            
            topFirstCell.txtPlanName.text = self.planModel.name;
            [topFirstCell.txtPlanName addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
            
            NSString *fromDateStr = ([self.planModel.from_date length]>19 ? [self.planModel.from_date substringToIndex:19] : self.planModel.from_date);
            
            NSString *toDateStr = ([self.planModel.to_date length]>19 ? [self.planModel.to_date substringToIndex:19] : self.planModel.to_date);

            
            
            //2017-06-21T07:24:32.570Z
            

            
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
            
            NSDate* fromDateNew = [dateFormatter dateFromString:fromDateStr];
            NSDate* toDateNew = [dateFormatter dateFromString:toDateStr];
            
            
            NSDateFormatter *dateFormatterButton = [[NSDateFormatter alloc] init];
            [dateFormatterButton setDateFormat:@"dd MMM yyy"];

            
            [topFirstCell.fromButton setTitle:[dateFormatterButton stringFromDate:fromDateNew] forState:UIControlStateNormal];
            
            [topFirstCell.toButton setTitle:[dateFormatterButton stringFromDate:toDateNew] forState:UIControlStateNormal];
            
            topFirstCell.fromButton.tag = 1;
            [topFirstCell.fromButton addTarget:self action:@selector(dateSelected:) forControlEvents:UIControlEventTouchUpInside];
            topFirstCell.toButton.tag = 2;
            [topFirstCell.toButton addTarget:self action:@selector(dateSelected:) forControlEvents:UIControlEventTouchUpInside];
            
            
            
            topFirstCell.lblDays.text = [NSString stringWithFormat:@"%@ %@",_planModel.days, LOCALIZATION(@"plan_day")];
            topFirstCell.totf.text = LOCALIZATION(@"to");
            topFirstCell.lblRouteMap.text = LOCALIZATION(@"route_map");
            
            [topFirstCell.btnEdit addTarget:self action:@selector(editClicked:) forControlEvents:UIControlEventTouchUpInside];

            
            /*
            topFirstCell.txtPlanName.text = self.objplan.name;
            
            topFirstCell.lblDate.text = [NSString stringWithFormat:@"%@(%@) to %@(%@)",[Utils dateFormatMMddyyyy:fromDate],[Utils getDayShortFromDate:fromDate],[Utils dateFormatMMddyyyy:toDate],[Utils getDayShortFromDate:toDate]];
            topFirstCell.lblDays.text = [NSString stringWithFormat:@"%@ Days",planDays];
            
            if([selectedLanguage  isEqual: @"sc"]){
                [topFirstCell.btnLanguage setImage:[UIImage imageNamed:@"speaker_.png"] forState:UIControlStateNormal];
            }else if([selectedLanguage  isEqual: @"tc"]){
                [topFirstCell.btnLanguage setImage:[UIImage imageNamed:@"speaker_2.png"] forState:UIControlStateNormal];
            }else{
                [topFirstCell.btnLanguage setImage:[UIImage imageNamed:@"speaker_Eng.png"] forState:UIControlStateNormal];
            }
            
            
            [topFirstCell.btnEdit addTarget:self action:@selector(editClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            [topFirstCell.btnLanguage addTarget:self action:@selector(languageClicked:) forControlEvents:UIControlEventTouchUpInside];
            */
            return topFirstCell;
            
        }else{
            EditMyPlanSecondCell *topCell = (EditMyPlanSecondCell *)[tblView dequeueReusableCellWithIdentifier:@"EditMyPlanSecondCell"];
            
            topCell.iteneraryBtn.tag = 0;
            topCell.airTickerBtn.tag = 0;
            topCell.hotelBtn.tag = 0;
            topCell.carBtn.tag = 0;
            
            [topCell.iteneraryBtn setBackgroundImage:[UIImage imageNamed:@"icone_1_US.png"] forState:UIControlStateNormal];
            [topCell.iteneraryBtn setBackgroundColor:[UIColor clearColor]];
            
            [topCell.airTickerBtn setBackgroundImage:[UIImage imageNamed:@"icone_2_US.png"] forState:UIControlStateNormal];
            [topCell.airTickerBtn setBackgroundColor:[UIColor clearColor]];
            
            [topCell.hotelBtn setBackgroundImage:[UIImage imageNamed:@"icone_3_US.png"] forState:UIControlStateNormal];
            [topCell.hotelBtn setBackgroundColor:[UIColor clearColor]];
            
            [topCell.carBtn setBackgroundImage:[UIImage imageNamed:@"icone_4_US.png"] forState:UIControlStateNormal];
            [topCell.carBtn setBackgroundColor:[UIColor clearColor]];
            
            
            if (segmentViewSelected == 0) {
                [topCell.iteneraryBtn setBackgroundImage:[UIImage imageNamed:@"icone_1_S.png"] forState:UIControlStateNormal];
                [topCell.iteneraryBtn setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
            }
            else if (segmentViewSelected == 1){
                [topCell.airTickerBtn setBackgroundImage:[UIImage imageNamed:@"icone_2_S.png"] forState:UIControlStateNormal];
                [topCell.airTickerBtn setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
            }
            else if (segmentViewSelected == 2){
                [topCell.hotelBtn setBackgroundImage:[UIImage imageNamed:@"icone_3_S.png"] forState:UIControlStateNormal];
                [topCell.hotelBtn setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
            }
            else if (segmentViewSelected == 3){
                [topCell.carBtn setBackgroundImage:[UIImage imageNamed:@"icone_4_S.png"] forState:UIControlStateNormal];
                [topCell.carBtn setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
            }

            
            [topCell.iteneraryBtn addTarget:self action:@selector(iteneraryClicked:) forControlEvents:UIControlEventTouchUpInside];
            [topCell.airTickerBtn addTarget:self action:@selector(airTickerClicked:) forControlEvents:UIControlEventTouchUpInside];
            [topCell.hotelBtn addTarget:self action:@selector(hotelClicked:) forControlEvents:UIControlEventTouchUpInside];
            [topCell.carBtn addTarget:self action:@selector(carClicked:) forControlEvents:UIControlEventTouchUpInside];

            /*
             
             
             @property (weak, nonatomic) IBOutlet UIButton *iteneraryBtn;
             @property (weak, nonatomic) IBOutlet UIButton *airTickerBtn;
             @property (weak, nonatomic) IBOutlet UIButton *hotelBtn;
             @property (weak, nonatomic) IBOutlet UIButton *carBtn;
             
             */
            
            return topCell;
        }
        
    }else{
        ItineraryTableViewCell *cell = (ItineraryTableViewCell*)[tblView dequeueReusableCellWithIdentifier:@"ItineraryTableViewCell" forIndexPath:indexPath];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        cell.bookmarkView.hidden = YES;
        cell.viewsView.hidden = YES;
        cell.shareView.hidden = YES;
        cell.btnCategory.tag = indexPath.row;
        [cell.btnCategory addTarget:self action:@selector(categoryClicked:) forControlEvents:UIControlEventTouchUpInside];
        //cell.lblTime.text = timeString;
        
        
        cell.bookMarkButton.hidden = YES;
        
        AttractionModel *gottenAttraction ;
        
        for (AttractionModel *attraction in self.planModel.attractions) {
            if ([attraction.day intValue] == indexPath.section + 1) {
                gottenAttraction = attraction;
                break;
            }
        }
        
        if (gottenAttraction) {
            AttractionDataModel *attractionData = gottenAttraction.attr_data[indexPath.row];
            
            cell.lblName.text = attractionData.info.name;
            cell.lblDescription.text = attractionData.info.address;
            NSString * imgId = attractionData.video_id.thumbnail;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            
            [cell.imgFull sd_setShowActivityIndicatorView:YES];
            [cell.imgFull sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            [cell.imgFull sd_setImageWithURL:[NSURL URLWithString:urlString]
                            placeholderImage:[UIImage imageNamed:@"placeholder.png"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                                if (!error) {
                                    cell.imgFull.image = image;
                                    [cell layoutIfNeeded];
                                }
                            }];
            cell.lblTime.text = attractionData.time;
            
            NSString *myData = attractionData.time;
            objc_setAssociatedObject (cell.btnTime, &myDateKey, myData,
                                      OBJC_ASSOCIATION_RETAIN);
            
            
            [cell.btnTime addTarget:self action:@selector(timepickerClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            
            NSString *myVideoData = attractionData.video_id.file_name;
            objc_setAssociatedObject (cell.btnPlayVideo, &myVideoKey, myVideoData,
                                      OBJC_ASSOCIATION_RETAIN);
            
            
            [cell.btnPlayVideo addTarget:self action:@selector(playAttractionVideo:) forControlEvents:UIControlEventTouchUpInside];
            
            
            cell.dragButton.hidden = NO;
            
            //[cell.dragButton addTarget:self action:@selector(moveTableRow:) forControlEvents:UIControlEventTouchDown];
            
            UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(moveTableRow:)];
            longPress.minimumPressDuration = 0.2;
            [cell.dragButton addGestureRecognizer:longPress];
            
            
            CategoryModel *cat = [attractionData.info.category firstObject];
            
            
            [cell.btnCategory sd_setBackgroundImageWithURL:[NSURL URLWithString:cat.image] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@""] options:SDWebImageContinueInBackground completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                [cell.btnCategory setBackgroundImage:image forState:UIControlStateNormal];
                [cell layoutIfNeeded];
            }];
            
            
        }
        
       /*Attractions * attractions = [attractionArray objectAtIndex:indexPath.section];
        AttractionData *aData = [[attractions.attractionData allObjects] objectAtIndex:indexPath.row];
       
        
        cell.lblName.text = aData.info.name;
        cell.lblDescription.text = aData.info.address;
        NSMutableArray *catArray = [[NSMutableArray alloc] init];
        catArray = [[aData.info.categori allObjects] mutableCopy];
        if(catArray.count >0){
            Categori *cat =[catArray objectAtIndex:0];
            NSString * imgId;
            if(catImage){
               imgId = catImage;
            }else{
                imgId = cat.image;

            }
           
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            cell.imgCategory.url = [NSURL URLWithString:urlString];
            cell.imgCategory.layer.cornerRadius = cell.imgCategory.frame.size.width / 2;
            cell.imgCategory.layer.masksToBounds = YES;
        }else{
            if(catImage){
                NSString * imgId = catImage;
                NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
                cell.imgCategory.url = [NSURL URLWithString:urlString];
                cell.imgCategory.layer.cornerRadius = cell.imgCategory.frame.size.width / 2;
                cell.imgCategory.layer.masksToBounds = YES;
            }else{
                //            cell.imgCategory.backgroundColor = [UIColor grayColor];
                cell.imgCategory.layer.cornerRadius = cell.imgCategory.frame.size.width / 2;
                cell.imgCategory.layer.masksToBounds = YES;

            }
        }
        
        
//        [cell.btnCategory setImage:[UIImage imageNamed:cat.image] forState:UIControlStateNormal];
        */
        return cell;
        
        
        
    }
    
    
}

-(void)moveTableRow:(id)sender {
    
    UILongPressGestureRecognizer *longPress = (UILongPressGestureRecognizer *)sender;
    UIGestureRecognizerState state = longPress.state;
    
    CGPoint location = [longPress locationInView:tblView];
    NSIndexPath *indexPath = [tblView indexPathForRowAtPoint:location];
    
    static UIView       *snapshot = nil;        ///< A snapshot of the row user is moving.
    static NSIndexPath  *sourceIndexPath = nil; ///< Initial index path, where gesture begins.
    
    switch (state) {
        case UIGestureRecognizerStateBegan: {
            if (indexPath) {
                sourceIndexPath = indexPath;
                
                UITableViewCell *cell = [tblView cellForRowAtIndexPath:indexPath];
                
                // Take a snapshot of the selected row using helper method.
                snapshot = [self customSnapshoFromView:cell];
                
                // Add the snapshot as subview, centered at cell's center...
                __block CGPoint center = cell.center;
                snapshot.center = center;
                snapshot.alpha = 0.0;
                [tblView addSubview:snapshot];
                [UIView animateWithDuration:0.25 animations:^{
                    
                    // Offset for gesture location.
                    center.y = location.y;
                    snapshot.center = center;
                    snapshot.transform = CGAffineTransformMakeScale(1.05, 1.05);
                    snapshot.alpha = 0.98;
                    cell.alpha = 0.0;
                    cell.hidden = YES;
                    
                }];
            }
            break;
        }
            
        case UIGestureRecognizerStateChanged: {
            CGPoint center = snapshot.center;
            center.y = location.y;
            snapshot.center = center;
            
            // Is destination valid and is it different from source?
            if (indexPath && ![indexPath isEqual:sourceIndexPath]) {
               
                
                
                
                
                NSLog(@"Now index = %ld Previous Index = %ld",(long)indexPath.row ,(long)sourceIndexPath.row);
                NSLog(@"Now Section = %ld Previous Section = %ld",(long)indexPath.section ,(long)sourceIndexPath.section);
                AttractionModel *nowAttraction ;
                AttractionDataModel *nowAttractionData ;
                AttractionModel *previousAttraction ;
                AttractionDataModel *previousAttractionData ;
                for (AttractionModel *attraction in self.planModel.attractions) {
                    if ([attraction.day intValue] == indexPath.section+1) {
                        nowAttraction = attraction;
                    }
                    if ([attraction.day intValue] == sourceIndexPath.section+1) {
                        previousAttraction = attraction;
                    }
                }
                if (nowAttraction) {
                    nowAttractionData = nowAttraction.attr_data[indexPath.row];
                }
                if (previousAttraction) {
                    previousAttractionData = previousAttraction.attr_data[sourceIndexPath.row];
                }
                if (nowAttractionData && previousAttractionData) {
                    [previousAttraction.attr_data removeObject:previousAttractionData];
                    [nowAttraction.attr_data insertObject:previousAttractionData atIndex:sourceIndexPath.row];
                }
              /*
                for (AttractionModel *attraction in self.planModel.attractions) {
                    if ([attraction.day intValue] == section+1) {
                        return attraction.attr_data.count;
                        break;
                    }
                }
                
               
               
               
                */
                // ... update data source.
                //TODO[resourceDetailsArray exchangeObjectAtIndex:indexPath.row withObjectAtIndex:sourceIndexPath.row];
                
                // ... move the rows.
                [tblView moveRowAtIndexPath:sourceIndexPath toIndexPath:indexPath];
                
                // ... and update source so it is in sync with UI changes.
                sourceIndexPath = indexPath;
            }
            break;
        }
            
        default: {
            // Clean up.
            UITableViewCell *cell = [tblView cellForRowAtIndexPath:sourceIndexPath];
            cell.alpha = 0.0;
            
            [UIView animateWithDuration:0.25 animations:^{
                
                snapshot.center = cell.center;
                snapshot.transform = CGAffineTransformIdentity;
                snapshot.alpha = 0.0;
                cell.alpha = 1.0;
                
            } completion:^(BOOL finished) {
                
                cell.hidden = NO;
                sourceIndexPath = nil;
                [snapshot removeFromSuperview];
                snapshot = nil;
                
            }];
            
            break;
        }
    }
}


/** @brief Returns a customized snapshot of a given view. */
- (UIView *)customSnapshoFromView:(UIView *)inputView {
    
    // Make an image from the input view.
    UIGraphicsBeginImageContextWithOptions(inputView.bounds.size, NO, 0);
    [inputView.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Create an image view.
    UIView *snapshot = [[UIImageView alloc] initWithImage:image];
    snapshot.layer.masksToBounds = NO;
    snapshot.layer.cornerRadius = 0.0;
    snapshot.layer.shadowOffset = CGSizeMake(-5.0, 0.0);
    snapshot.layer.shadowRadius = 5.0;
    snapshot.layer.shadowOpacity = 0.4;
    
    return snapshot;
}



-(void)playAttractionVideo :(UIButton *)btn {
    NSString *myData =
    (NSString *)objc_getAssociatedObject(btn, &myVideoKey);
    NSString * imgId = myData;
    NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,@"/uploads/",imgId];
    NSLog(@"%@",urlString);
    [self playVideoInPopUpViewWithURL:[NSURL URLWithString:urlString]];
    //http://54.254.206.27:9000/uploads/
}
-(void)playVideoInPopUpViewWithURL:(NSURL *)url {
    if (url) {
        self.videoController = [[MPMoviePlayerController alloc] init];
        [self.videoController setContentURL:url];
        [self.videoController.view setFrame:CGRectMake (0, 0, popUpView.frame.size.width, popUpView.frame.size.height)];
        self.videoController.controlStyle = MPMovieControlStyleNone;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoPlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:self.videoController];
        [popUpView addSubview:self.videoController.view];
        [KGModalWrapper showWithContentView:popUpView];
        [self.videoController setControlStyle:MPMovieControlStyleNone];
        [self.videoController play];
    }
}
- (void)videoPlayBackDidFinish:(NSNotification *)notification {
    [self.videoController stop];
    [self.videoController.view removeFromSuperview];
    self.videoController = nil;
    [KGModalWrapper hideView];
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView == _tblLanguage){
        return 44;
    }else if(tableView == tblViewAttType){
        return 44;
    }else if(tableView == topTblView){
        if(indexPath.row == 0){
            return 70;
        }else{
            return 70;
        }
    }else{
        return 80;
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    /*
     this method is used for set header in first segment
     */
    if(tableView == _tblLanguage){
        return nil;
    }else if(tableView == tblViewAttType){
        return nil;
    }else if(tableView == topTblView){
        return nil;
    }else{
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 30)];
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0,0, [[UIScreen mainScreen] bounds].size.width, 1)];
        line.backgroundColor = [UIColor lightGrayColor];
        //[view addSubview:line];
        UIImageView *imgWall = [[UIImageView alloc] initWithFrame:CGRectMake(12,0,30,30)];
        
        imgWall.image = [UIImage imageNamed:@"circle"];
        [view addSubview:imgWall];
        UILabel *lblText = [[UILabel alloc] initWithFrame:CGRectMake(46, 5, [[UIScreen mainScreen] bounds].size.width-100, 20)];
        lblText.font = [UIFont systemFontOfSize:12];
        lblText.textColor = [ColorConstants appBrownColor];
        UIButton *noteBtn = [[UIButton alloc] initWithFrame:CGRectMake([[UIScreen mainScreen] bounds].size.width - 100, 5, 80, 20)];

        if (section == 0) {
            noteBtn.backgroundColor = [ColorConstants appYellowColor];
            [noteBtn setTitle:LOCALIZATION(@"add_note") forState:UIControlStateNormal];
            [noteBtn setTitleColor:[ColorConstants appBrownColor] forState:UIControlStateNormal];
            noteBtn.titleLabel.font = [UIFont systemFontOfSize:12];
            noteBtn.layer.cornerRadius = 10; // this value vary as per your desire
            noteBtn.clipsToBounds = YES;
            [noteBtn addTarget:self action:@selector(addNoteAction:) forControlEvents:UIControlEventTouchUpInside];

            [view addSubview:noteBtn];
        }
        else{
            [noteBtn removeFromSuperview];
        }
        
        
        NSString *fromDateStr = ([self.planModel.from_date length]>19 ? [self.planModel.from_date substringToIndex:19] : self.planModel.from_date);
        
        
        
        
        //2017-06-21T07:24:32.570Z
        
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
        
        NSDate* fromDateNew = [dateFormatter dateFromString:fromDateStr];
        
        
        
        
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        dayComponent.day = section;
        
        NSCalendar *theCalendar = [NSCalendar currentCalendar];
        NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:fromDateNew options:0];
        NSString *next = [Utils dateFormatyyyyMMdd:nextDate];
        NSString * day =[[Utils getDayFromDate:nextDate] substringToIndex:3];
        
        
        int bSec = (int)section+1;
        lblText.text =[NSString stringWithFormat:@"Day %d - %@(%@)",bSec,next,day];// @"Day 1 2016-9-14 (WED)";
        
        
        UIButton *btnAddRemark = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnAddRemark addTarget:self action:@selector(remarkSelected:) forControlEvents:UIControlEventTouchUpInside];
        [btnAddRemark setTitle:@"+add remark" forState:UIControlStateNormal];
        btnAddRemark.frame = CGRectMake(self.view.frame.size.width-80,5, 75.0, 20.0);
        btnAddRemark.backgroundColor = [ColorConstants appYellowColor];
        [btnAddRemark setTitleColor:[ColorConstants appBrownColor] forState:UIControlStateNormal];
        btnAddRemark.titleLabel.font =[UIFont systemFontOfSize:11];
        btnAddRemark.layer.cornerRadius =btnAddRemark.frame.size.height / 2;
        //[view addSubview:btnAddRemark];
        
        
        [view addSubview:lblText];
        [view setBackgroundColor:[UIColor whiteColor]];
        return view;
        
        
    }
    
    
}
-(void)remarkSelected:(id)sender{
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(tableView == _tblLanguage){
        return 0;
    }else if(tableView == tblViewAttType){
        return 0;
    }else if(tableView == topTblView){
        return 0;
    }else{
        return 30;
    }
}


- (UITableViewCellEditingStyle) tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == tblView){
        return UITableViewCellEditingStyleNone;
    }else{
        return UITableViewCellEditingStyleNone;

    }
    
    
    
}

- (BOOL)moveTableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    if(tableView == tblView){
        return YES;
    }else{
        return NO;
    }
}

- (NSIndexPath *)moveTableView:(UITableView *)tableView targetIndexPathForMoveFromRowAtIndexPath:(NSIndexPath *)sourceIndexPath toProposedIndexPath:(NSIndexPath *)proposedDestinationIndexPath {
    if(tableView == tblView){
        return proposedDestinationIndexPath;
    }else{
        return 0;
    }
}


- (void) tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    if(tableView == tblView){
        [tblView setEditing:NO animated:true];
        [tblView reloadData];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == _tblLanguage){
        selectedLanguage = [languagesArray objectAtIndex:indexPath.row];
        if([[languagesArray objectAtIndex:indexPath.row] isEqual:@"English"]){
            selectedLanguage= @"en";
        }else if([[languagesArray objectAtIndex:indexPath.row] isEqual:@"Chineese"]){
            selectedLanguage= @"sc";
        }else{
            selectedLanguage= @"tc";
        }
    }else if(tableView == tblViewAttType){
        Categori *cat = [categoryListAttr objectAtIndex:indexPath.row];
        catImage = cat.image;
        
//        ItineraryTableViewCell *cell = (ItineraryTableViewCell*)[tblView dequeueReusableCellWithIdentifier:@"ItineraryTableViewCell" forIndexPath:indexPath];
        
        index = [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section];
        
    }else if(tableView == topTblView){
        

        //        EditMyPlanSecondCell *cell = (EditMyPlanSecondCell*)[tableView cellForRowAtIndexPath:indexPath];
        //        cell.imgSelected.image=[UIImage imageNamed:@"circle_s.png"];
    }else{
        AttractionModel *gottenAttraction ;
        
        for (AttractionModel *attraction in self.planModel.attractions) {
            if ([attraction.day intValue] == indexPath.section + 1) {
                gottenAttraction = attraction;
                break;
            }
        }
        
        if (gottenAttraction) {
            AttractionModel *attractionModel = gottenAttraction;
            AttractionDataModel *dataModel = attractionModel.attr_data[indexPath.row];
            
            PlaceInformationViewController *controller = [PlaceInformationViewController initViewControllerWithAttrModel:attractionModel andAttractionDataModel:dataModel];
            [self.navigationController pushViewController:controller animated:YES];
        }
        
        
        
        
        
        
    }
}

- (void)tableView:(UITableView *)tableView didDeSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(tableView == _tblLanguage){
    }else if(tableView == tblViewAttType){
        
    }else{
        //        EditMyPlanSecondCell *cell = (EditMyPlanSecondCell*)[tableView cellForRowAtIndexPath:indexPath];
        //        cell.imgSelected.image=[UIImage imageNamed:@"circle_us.png"];
    }
}

- (IBAction)cancleLanguageClicked:(id)sender {
    [KGModalWrapper hideView];
}

- (IBAction)saveLanguageClicked:(id)sender {
    
    [topTblView reloadData];
    
    [KGModalWrapper hideView];
}

- (IBAction)cancleAttTypeClicked:(id)sender {
    [KGModalWrapper hideView];
}

- (IBAction)saveAttTypeClicked:(id)sender {
//    [tblView reloadData];
    [tblView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
    [KGModalWrapper hideView];
}
- (void)dateSelected:(UIButton *)sender {
    NSString *title;
    NSDate *selectedDate;
    
    
    NSString *fromDateStr = ([self.planModel.from_date length]>19 ? [self.planModel.from_date substringToIndex:19] : self.planModel.from_date);
    NSString *toDateStr = ([self.planModel.to_date length]>19 ? [self.planModel.to_date substringToIndex:19] : self.planModel.to_date);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate* fromDateNew = [dateFormatter dateFromString:fromDateStr];
    NSDate* toDateNew = [dateFormatter dateFromString:toDateStr];
    
    
    NSDate *minimumDate;
    NSDate *maximumDate;

    NSCalendar *cal = [NSCalendar currentCalendar];

    
    if (sender.tag == 1) {
        title = @"Select From Date";
        selectedDate = fromDateNew;
        maximumDate = toDateNew;
        
        
        NSDate *min3Months = [cal dateByAddingUnit:NSCalendarUnitMonth value:-6 toDate:maximumDate options:0];
        
        minimumDate = min3Months;
    }
    else{
        title = @"Select To Date";
        selectedDate = toDateNew;
        minimumDate = fromDateNew;
        
        NSDate *max3Months = [cal dateByAddingUnit:NSCalendarUnitMonth value:6 toDate:minimumDate options:0];

        maximumDate = max3Months;

    }
    
    
    [ActionSheetDatePicker showPickerWithTitle:title datePickerMode:UIDatePickerModeDate selectedDate:selectedDate minimumDate:minimumDate maximumDate:maximumDate doneBlock:^(ActionSheetDatePicker *picker, id selectedDate, id origin) {
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];

        if (sender.tag == 1) {
            self.planModel.from_date = [dateFormatter stringFromDate:(NSDate *)selectedDate];
            
            NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                                fromDate:(NSDate *)selectedDate
                                                                  toDate:toDateNew
                                                                 options:0];
            self.planModel.days = [NSNumber numberWithInt:((int)[components day] + 1)];
            
        }
        else{
            self.planModel.to_date = [dateFormatter stringFromDate:(NSDate *)selectedDate];
            
            NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                                fromDate:fromDateNew
                                                                  toDate:(NSDate *)selectedDate
                                                                 options:0];
            self.planModel.days = [NSNumber numberWithInt:((int)[components day] + 1)];
        }
        
        [topTblView reloadData];
        [tblView reloadData];
        isChanged = YES;
    } cancelBlock:nil origin:sender];

}

- (void)iteneraryClicked:(UIButton *)sender {
    segmentViewSelected = 0;
    webViewOuterView.hidden = YES;
    webViewHotelOuterView.hidden = YES;
    [self reloadSegmentCell];
    /*
    if (sender.tag == 0) {
        [sender setBackgroundImage:[UIImage imageNamed:@"icone_1_S.png"] forState:UIControlStateNormal];
        [sender setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
        sender.tag = 1;
    }
    else{
        [sender setBackgroundImage:[UIImage imageNamed:@"icone_1_US.png"] forState:UIControlStateNormal];
        [sender setBackgroundColor:[UIColor clearColor]];
        sender.tag = 0;
    }*/
}

- (void)airTickerClicked:(UIButton *)sender {
    segmentViewSelected = 1;
    webViewOuterView.hidden = YES;
    webViewHotelOuterView.hidden = YES;
    [self reloadSegmentCell];
    /*
    if (sender.tag == 0) {
        [sender setBackgroundImage:[UIImage imageNamed:@"icone_2_S.png"] forState:UIControlStateNormal];
        [sender setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
        sender.tag = 1;
    }
    else{
        [sender setBackgroundImage:[UIImage imageNamed:@"icone_2_US.png"] forState:UIControlStateNormal];
        [sender setBackgroundColor:[UIColor clearColor]];
        sender.tag = 0;
    }*/
}


- (void)hotelClicked:(UIButton *)sender {
    segmentViewSelected = 2;
    webViewOuterView.hidden = YES;
     webViewHotelOuterView.hidden = NO;
    [self reloadSegmentCell];
    /*
    if (sender.tag == 0) {
        [sender setBackgroundImage:[UIImage imageNamed:@"icone_3_S.png"] forState:UIControlStateNormal];
        [sender setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
        sender.tag = 1;
    }
    else{
        [sender setBackgroundImage:[UIImage imageNamed:@"icone_3_US.png"] forState:UIControlStateNormal];
        [sender setBackgroundColor:[UIColor clearColor]];
        sender.tag = 0;
    }
     */
}

- (void)carClicked:(UIButton *)sender {
    segmentViewSelected = 3;
    webViewOuterView.hidden = NO;
    webViewHotelOuterView.hidden = YES;
    [self reloadSegmentCell];
    /*
    
    if (sender.tag == 0) {
        [sender setBackgroundImage:[UIImage imageNamed:@"icone_4_S.png"] forState:UIControlStateNormal];
        [sender setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
        sender.tag = 1;
    }
    else{
        [sender setBackgroundImage:[UIImage imageNamed:@"icone_4_US.png"] forState:UIControlStateNormal];
        [sender setBackgroundColor:[UIColor clearColor]];
        sender.tag = 0;
    }
     */
}


-(void)reloadSegmentCell {
    [topTblView reloadData];
    /*
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:1];
    [tblView beginUpdates];
    [tblView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    [tblView endUpdates];
     */
}






-(void)textFieldDidChange :(UITextField *)theTextField{
    isChanged = YES;
    self.planModel.name = theTextField.text;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


- (void)addNoteAction:(id)sender {
    
    [KGModalWrapper showWithContentView:addNoteView];
    
    /*
    _dayView.layer.cornerRadius = _dayView.layer.frame.size.height / 2;
    _dayView.layer.borderColor= [ColorConstants appBrownColor].CGColor;
    _dayView.layer.borderWidth=1;
    
    _monthView.layer.cornerRadius = _monthView.layer.frame.size.height / 2;
    _monthView.layer.borderColor= [ColorConstants appBrownColor].CGColor;
    _monthView.layer.borderWidth=1;
    
    _lblViewMonth.text=[Utils dateFormatMMMyyyy:fromDate];
    NSArray *dateArray = [[Utils dateFormatMMddyyyy:fromDate] componentsSeparatedByString:@"/"];
    _lblViewDay.text = [dateArray objectAtIndex:1];
    _lblViewDayName.text =[NSString stringWithFormat:@"(%@)", [Utils getDayFromDate:fromDate]];
     */
}

@end

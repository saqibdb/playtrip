
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "Plan.h"
#import "AttractionData.h"
#import "ItineraryTableViewCell.h"
#import "URLImageView.h"
#import "PlanModel.h"
#import <MediaPlayer/MediaPlayer.h>


@interface EditMyPlanViewController : UIViewController<UITableViewDataSource, UITabBarDelegate, LoginDelegate ,UIGestureRecognizerDelegate, UIWebViewDelegate> {
    
    IBOutlet UITableView *topTblView;
    IBOutlet UITextField *txtPlanName;
    IBOutlet UITableView *tblView;
    IBOutlet UILabel *lblDay;
    IBOutlet UILabel *lblDate;
    
    IBOutlet UILabel *lblTopMain1;
    IBOutlet UILabel *lblTopMain2;
    IBOutlet UILabel *lblTopMain3;
    
    IBOutlet UIButton *btnOneDay;
    
    NSMutableArray *arrVideoList;
    
    NSMutableArray *arrattractions,*arrData;
    
    NSDate * fromDate;
    NSDate * toDate;
    BOOL dateSelected;
    
    IBOutlet URLImageView *userImage;
    
    IBOutlet UIView *firstDayView;
    
    IBOutlet UIView *editAttractionTypeview;
    
    IBOutlet UITableView *tblViewAttType;
    IBOutlet UIView *editLanguageView;
    IBOutlet UILabel *lblUserName;
    NSString *fromDay;
    NSString *toDay;
    NSDate *startDate;
    NSString *dateString;
    NSString *timeString;
    NSString *selectedLanguage;
    BOOL checked;
    NSNumber *planDays;
    NSString *catImage;
    NSIndexPath *index;
    
    IBOutlet UIView *popUpView;

    
    IBOutlet UIView *addNoteView;
    
    
    IBOutlet UITextView *addNoteTextView;
    
    
    
    
    IBOutlet UIView *webViewOuterView;
    
    IBOutlet UIWebView *carRentalWebView;
    
    IBOutlet UIActivityIndicatorView *webLoader;
    
    
    IBOutlet UIView *webViewHotelOuterView;
    
    IBOutlet UIWebView *hotelWebView;
    
    IBOutlet UIActivityIndicatorView *webHotelLoader;
}

@property(nonatomic)Plan *objplan;


@property(nonatomic)PlanModel *planModel;



+(EditMyPlanViewController *)initViewController:(Plan *)plan;
+(EditMyPlanViewController *)initViewControllerWithPlanModel:(PlanModel *)plan;
- (IBAction)oneDayClicked:(id)sender;
- (IBAction)cancleClicked:(id)sender;
- (IBAction)viewSaveClicked:(id)sender;
- (IBAction)datepickerClicked:(id)sender;

- (IBAction)addNoteSaveAction:(id)sender;
- (IBAction)addNoteCancelAction:(id)sender;




@property (weak, nonatomic) IBOutlet UILabel *lblViewDay;
@property (weak, nonatomic) IBOutlet UILabel *lblViewMonth;
@property (weak, nonatomic) IBOutlet UIView *dayView;
@property (weak, nonatomic) IBOutlet UIView *monthView;

@property (weak, nonatomic) IBOutlet UILabel *lblViewDayName;
- (IBAction)cancleLanguageClicked:(id)sender;
- (IBAction)saveLanguageClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tblLanguage;
@property (strong, nonatomic) MPMoviePlayerController *videoController;

- (IBAction)cancleAttTypeClicked:(id)sender;
- (IBAction)saveAttTypeClicked:(id)sender;

@end

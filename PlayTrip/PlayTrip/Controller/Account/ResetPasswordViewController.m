
#import "ResetPasswordViewController.h"
#import "AppDelegate.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "Constants.h"
#import "Localisator.h"

@interface ResetPasswordViewController ()
@end

@implementation ResetPasswordViewController

+(ResetPasswordViewController *)initViewController {
    ResetPasswordViewController * controller = [[ResetPasswordViewController alloc] initWithNibName:@"ResetPasswordViewController" bundle:nil];
    return controller;
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController dismissViewControllerAnimated:true completion:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveLanguageChangedNotification:)
                                                 name:kNotificationLanguageChanged
                                               object:nil];
    
    
    [self setLocalizedStrings];
}

- (void) receiveLanguageChangedNotification:(NSNotification *) notification
{
    if ([notification.name isEqualToString:@"languageChanged"])
    {
        [self setLocalizedStrings];
    }
}

-(void)setLocalizedStrings {
    
    [self.lblResetPass setText:LOCALIZATION(@"reset_password")];
    [self.txtResetEmail setPlaceholder:LOCALIZATION(@"email")];
    [self.btnReset setTitle:LOCALIZATION(@"reset") forState:UIControlStateNormal];
    
    /*
    self.lblResetPass.text = [NSString stringWithFormat:NSLocalizedString(@"reset_password", nil)];
    _txtResetEmail.placeholder = [NSString stringWithFormat:NSLocalizedString(@"email", nil)];
    [self.btnReset setTitle:NSLocalizedString(@"reset", nil) forState:UIControlStateNormal];
     */
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = true;
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = false;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    UIFont * f = [UIFont systemFontOfSize:15.0];
    CGFloat width = [Utils widthOfString:newString withFont:f];
    
    if (width > 200) {
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        textField.leftView = paddingView;
        textField.leftViewMode = UITextFieldViewModeAlways;
        
    } else {
        textField.leftViewMode = UITextFieldViewModeNever;
    }
    
    return YES;
}

- (IBAction)actionReset:(id)sender {
    if (_txtResetEmail.text.length == 0) {
        [self showAlertWithMessage:@"Please Enter Email"];
        return;
    } else if (![Utils validateEmailWithString:_txtResetEmail.text]) {
        [self showAlertWithMessage:@"Please Enter Valid Email"];
        return;
    }
    
    if(![[AppDelegate appDelegate] isReachable]){
        [self showAlertWithMessage:kCheckInternet];
        return;
    }
    Account *account=[[Account alloc]init];
    account.delegate = self;
    account.email = _txtResetEmail.text;
    [SVProgressHUD showWithStatus:@"Please wait..."];
    [account changePasswordWithDelegate:self];
}

- (void)showAlertWithMessage:(NSString *)message{
    
    /*
     this method is used for showing message Dialog
     */
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:ALERT_TITLE
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Ok"
                                                       style:UIAlertActionStyleDefault
                                                     handler:nil];
    [alertController addAction:actionOk];
    [self presentViewController:alertController animated:YES completion:nil];
}



-(void)accountAuthenticatedWithAccount:(Account*) account{
    /*
     Delegate method for Account.Here we get success Response of Registration
     */
    [SVProgressHUD dismiss];
    NSString *message = @"Reset password link is sent to your email address";
    [self showAlertWithMessage:message];
}
-(void)accountDidFailAuthentication:(NSString*) error{
    /*
     Delegate method for Account.Here we get Failure Response of Registration
     */
    [SVProgressHUD dismiss];
    [self showAlertWithMessage:error];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

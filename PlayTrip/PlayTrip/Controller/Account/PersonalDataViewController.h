
#import <UIKit/UIKit.h>

@interface PersonalDataViewController : UIViewController {
    
    IBOutlet UITextField *txtAccountName;
    IBOutlet UITextField *txtAccountNumber;
}

+(PersonalDataViewController *) initViewController;

@end

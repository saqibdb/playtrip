
#import <UIKit/UIKit.h>
#import "AccountManager.h"
#import "Account.h"

@interface SettingsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, AccountAuthenticateDelegate, UITextFieldDelegate> {
    IBOutlet UITableView *tblView;
    BOOL isSave;
    
    NSString * mobileNumber;
    NSString * email;
    NSString * bankName;
    NSString * bankAccount;
    NSString * holderName;
    
//    NSString * language;
//    NSString * currency;
    
    BOOL reservation, receiveRewards, updates, received, wifiOnly;
    
    NSString * changedLanguage;
    NSString * changedCurrency;
}

+(SettingsViewController *)initViewController;


@end

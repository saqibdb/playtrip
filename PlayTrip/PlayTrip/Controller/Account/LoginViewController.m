
#import "LoginViewController.h"
#import "ColorConstants.h"
#import "KGModalWrapper.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "AppDelegate.h"
#import "Constants.h"
#import "TutorialBaseViewController.h"
#import "ResetPasswordViewController.h"
#import "RegisterViewController.h"
#import "SettingsViewController.h"
#import "TutorialBaseViewController.h"
#import "NotificationViewController.h"
#import "DistrictViewController.h"
#import "LanguageSelectionViewController.h"
#import "PersonalDataViewController.h"
#import "UserTabViewController.h"
#import "CreateVideoTourViewController.h"
#import "LocationViewController.h"
#import "ProfileViewController.h"
#import "MapViewController.h"
#import "PopularVideoSort.h"
#import "Localisator.h"

@interface LoginViewController (){
    NSString *imageUrl;
}
@end

@implementation LoginViewController

+(LoginViewController *)initViewController {
    LoginViewController * controller = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
     controller->lblTopMain =  [Utils roundCornersOnView:controller->lblTopMain onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveLanguageChangedNotification:)
                                                 name:kNotificationLanguageChanged
                                               object:nil];
    
    BOOL isTutorialShown = [Utils getBOOLForKey:@"TutorialShown"];
    if (!isTutorialShown) {
        [Utils setBOOL:true Key:@"TutorialShown"];
        [self showTutorial];
    }
    locationManager = [[CLLocationManager alloc] init];
   
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = true;
    [self setNavBarItems];
    [self getTermsAndConditions];
    locArray = [NSMutableArray new];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [locationManager requestAlwaysAuthorization];
    [locationManager startUpdatingLocation];
    lblTopMain =  [Utils roundCornersOnView:lblTopMain onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];
    
    [self setLocalizedStrings];
}

- (void) receiveLanguageChangedNotification:(NSNotification *) notification
{
    if ([notification.name isEqualToString:@"languageChanged"])
    {
        [self setLocalizedStrings];
    }
}

-(void)setLocalizedStrings {
    
    [self.lblLogin setText:LOCALIZATION(@"login_playTrip")];
    [self.lblOr setText:LOCALIZATION(@"or")];
    
    [self.btnForgotPassword setTitle:LOCALIZATION(@"forgot_password") forState:UIControlStateNormal];
    [self.btnRegister setTitle:LOCALIZATION(@"register") forState:UIControlStateNormal];
    [self.btnSkip setTitle:LOCALIZATION(@"skip") forState:UIControlStateNormal];
    [self.btnLogin setTitle:LOCALIZATION(@"login") forState:UIControlStateNormal];
    
    [txtUserName setPlaceholder:LOCALIZATION(@"username")];
    [txtPassword setPlaceholder:LOCALIZATION(@"password")];
    
    // T & C Popup
    [lblTopMain setText:LOCALIZATION(@"terms_condtions")];
    [lblAgreeText setText:LOCALIZATION(@"agree_text")];
    [btnAgree setTitle:LOCALIZATION(@"understand") forState:UIControlStateNormal];

}

-(void)setNavBarItems {
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_white.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backClicked:)];
    self.navigationItem.leftBarButtonItem = backButton;
}

-(void)showTutorial {
    TutorialBaseViewController * controller = [TutorialBaseViewController initViewConrtoller];
    [self.navigationController pushViewController:controller animated:true];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    UIFont * f = [UIFont systemFontOfSize:15.0];
    CGFloat width = [Utils widthOfString:newString withFont:f];
    
    if (width > 200) {
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        textField.leftView = paddingView;
        textField.leftViewMode = UITextFieldViewModeAlways;
      
    } else {
        textField.leftViewMode = UITextFieldViewModeNever;
    }
    return YES;
}


-(void)getTermsAndConditions {
    
    
    
    /*
    [[PlayTripManager Instance] getTermsAndConditionsWithBlock:^(id result, NSString *error){
        if (!error) {
            NSMutableDictionary * res = result;
            NSArray * desc = [res valueForKey:@"description"];
            if(desc.count>0){
                [descView setText:[desc objectAtIndex:0]];
            }
            
        }
    }];*/
}

-(IBAction)skipClicked:(id)sender{
    [self termsClicked:self];
}

- (IBAction)backClicked:(id)sender {
    LanguageSelectionViewController * controller = [LanguageSelectionViewController initViewController];
    [self.navigationController pushViewController:controller animated:true];
}

- (IBAction)forgotPasswordClicked:(id)sender {
    ResetPasswordViewController * controller = [ResetPasswordViewController initViewController];
    UINavigationController * navController = [[UINavigationController alloc] initWithRootViewController:controller];
    [self.navigationController presentViewController:navController animated:true completion:nil];
}

- (IBAction)registerClicked:(id)sender {
    RegisterViewController * controller = [RegisterViewController initViewController];
    UINavigationController * navController = [[UINavigationController alloc] initWithRootViewController:controller];
    [self.navigationController presentViewController:navController animated:true completion:nil];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
  
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    if (!currentLocation) {
        currentLocation = newLocation;
        double lng = currentLocation.coordinate.longitude;
        NSString * lngString = [NSString stringWithFormat:@"%f", lng];
        
        double lat = currentLocation.coordinate.latitude;
        NSString * latString = [NSString stringWithFormat:@"%f", lat];
        [locArray addObject:lngString];
        [locArray addObject:latString];
    }
}

- (IBAction)btnGoogleClicked:(id)sender {
    if(![[AppDelegate appDelegate] isReachable]){
        [Utils showAlertWithMessage:kCheckInternet];
        return;
    }
    GIDSignIn* signIn = [GIDSignIn sharedInstance];
    signIn.scopes = @[ @"profile", @"email" ];
    signIn.delegate = self;
    signIn.uiDelegate = self;
    
    [[GIDSignIn sharedInstance] signIn];
}

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    /*
     in this method we can get response from google
     */
    if (error == nil) {
        
        CGSize thumbSize=CGSizeMake(100, 100);
        if ([GIDSignIn sharedInstance].currentUser.profile.hasImage)
        {
            NSUInteger dimension = round(thumbSize.width * [[UIScreen mainScreen] scale]);
            NSURL *imageURL = [user.profile imageURLWithDimension:dimension];
            imageUrl = imageURL.absoluteString;
        }
        
        
        Account *account=[[Account alloc]init];
        account.email = user.profile.email;
        account.first_name =user.profile.name;
        account.last_name = user.profile.givenName;
        account.image = [user.profile imageURLWithDimension:200].absoluteString;
        account.provider = @"Google";
        account.username = user.profile.name;
        NSString *selectedLanguage = [Utils getValueForKey:kUpdatedLanguage];
        if ([selectedLanguage isEqualToString:Language_Simple_Chineese]) {
            account.language = @"sc";
        }
        else if ([selectedLanguage isEqualToString:Language_Traditional_Chineese]){
            account.language = @"tc";
        }
        else{
            account.language = @"en";
        }
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD showWithStatus:@"Loading..."];
        [account registerWithDelegate:self];
    }else{
        [Utils showAlertWithMessage:error.localizedDescription];
    }
}

- (void)signIn:(GIDSignIn *)signIn didDisconnectWithUser:(GIDGoogleUser *)user withError:(NSError *)error {
}

- (IBAction)btnFbClicked:(id)sender {
    if(![[AppDelegate appDelegate] isReachable]){
        [Utils showAlertWithMessage:kCheckInternet];
        return;
    }
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile",@"email",@"user_birthday"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
       if (result.isCancelled) {
            NSLog(@"Token: %@", result.token);
        } else {
            if ([FBSDKAccessToken currentAccessToken]) {
                FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]initWithGraphPath:@"me" parameters:@{@"fields" : @"id,name,email,first_name,last_name,picture"                                                                                                            }];
                [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                    if (result) {
                        Account *account=[[Account alloc]init];
                        account.email = [NSString stringWithFormat:@"%@",[result objectForKey:@"email"]];
                        account.first_name = [NSString stringWithFormat:@"%@",[result objectForKey:@"first_name"]];
                        account.provider = @"facebook";
                        account.loginTypeObject = result;
                        account.username = [result objectForKey:@"first_name"];
                        account.last_name = [result objectForKey:@"last_name"];
                        NSString *imageStringOfLoginUser = [[[result valueForKey:@"picture"] valueForKey:@"data"] valueForKey:@"url"];
                        account.avatar = imageStringOfLoginUser;
                        imageUrl = imageStringOfLoginUser;
                        
                        NSString *selectedLanguage = [Utils getValueForKey:kUpdatedLanguage];
                        if ([selectedLanguage isEqualToString:Language_Simple_Chineese]) {
                            account.language = @"sc";
                        }
                        else if ([selectedLanguage isEqualToString:Language_Traditional_Chineese]){
                            account.language = @"tc";
                        }
                        else{
                            account.language = @"en";
                        }
                       
                        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
                        [SVProgressHUD showWithStatus:@"Signin..."];
                        [account registerWithDelegate:self];
                        
                    }
                }];
            }
        }
    }];
}

- (IBAction)loginClicked:(id)sender {
    
    /*
     this function is used for checking login credential and login request
     */
    
    NSString *strMessage;
    if (txtUserName.text.length == 0 ) {
        strMessage = @"Please Enter Username";
    }else if (txtPassword.text.length == 0){
        strMessage = @"Please Enter Password";
    }
    else if (txtPassword.text.length < 8) {
        strMessage = @"Password must be atleast 8 characters";
    }
    if (strMessage.length > 0) {
        [Utils showAlertWithMessage:strMessage];
        return;
    }
    
    if (![[AppDelegate appDelegate] isReachable]) {
        [Utils showAlertWithMessage:kCheckInternet];
        return;
    }
    
    Account *account=[[Account alloc]init];
    account.username = txtUserName.text;
    account.password = txtPassword.text;
    NSString *selectedLanguage = [Utils getValueForKey:kUpdatedLanguage];
    if ([selectedLanguage isEqualToString:Language_Simple_Chineese]) {
        account.language = @"sc";
    }
    else if ([selectedLanguage isEqualToString:Language_Traditional_Chineese]){
        account.language = @"tc";
    }
    else{
        account.language = @"en";
    }
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Logging In..."];
    if (locArray.count == 2) {
        [account authenticateWithLoc:locArray WithDelegate:self]; // Arun :: sending lat,lng need to check after running in device.
    } else {
        [account authenticateWithDelegate:self];
    }
    
}

#pragma mark -- Account Manager Delegate

-(void)accountAuthenticatedWithAccount:(Account*) account{
    /*
     Success CallBack of Account API Like Login
     */
    account.isSkip = false;
    account.isLoggedIn = true;
    [AccountManager Instance].activeAccount = account;
    [SVProgressHUD dismiss];
    [[AppDelegate appDelegate] userDidLogin];
    [account saveLanguage:nil];
    if (imageUrl) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSURL *imageURL = [NSURL URLWithString:imageUrl];
            
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            
            UIImage *image = [UIImage imageWithData:imageData];
            
            [account uploadAvtar:image WithDelegate:nil];
            
            
        });
    }
    
    
}

-(void)accountDidFailAuthentication:(NSString*) error{
    [SVProgressHUD dismiss];
    [Utils showAlertWithMessage:error];
}

- (IBAction)termsClicked:(id)sender {
    btnAcceptTerms.selected = true;
    imgCheckBox.image = [UIImage imageNamed:@"checked_black.png"];
    [KGModalWrapper showWithContentView:termsView];
}

- (IBAction)acceptTermsClicked:(id)sender {
    if ([btnAcceptTerms isSelected]) {
        btnAcceptTerms.selected = false;
        imgCheckBox.image = [UIImage imageNamed:@"unchecked_black.png"];
    } else {
        btnAcceptTerms.selected = true;
        imgCheckBox.image = [UIImage imageNamed:@"checked_black.png"];
    }
}

- (IBAction)understandClicked:(id)sender {
    if ([btnAcceptTerms isSelected]) {
        [KGModalWrapper hideView];
        Account * account = [[Account alloc] init];
        account.isSkip = true;
        account.isLoggedIn = false;
        NSString *selectedLanguage = [Utils getValueForKey:kUpdatedLanguage];
        if ([selectedLanguage isEqualToString:Language_Simple_Chineese]) {
            account.language = @"sc";
        }
        else if ([selectedLanguage isEqualToString:Language_Traditional_Chineese]){
            account.language = @"tc";
        }
        else{
            account.language = @"en";
        }
        [AccountManager Instance].activeAccount = account;
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

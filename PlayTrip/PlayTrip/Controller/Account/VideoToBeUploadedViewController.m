
#import "VideoToBeUploadedViewController.h"
#import "VideoUploadedCell.h"
#import "ColorConstants.h"
#import "Utils.h"
#import "Plan.h"
#import "PlayTripManager.h"
#import "SVProgressHUD.h"
#import "Attractions.h"
#import "AttractionData.h"
#import "Info.h"
#import "Account.h"
#import "AccountManager.h"
#import "VideoSyncHelper.h"

@interface VideoToBeUploadedViewController ()
@end

@implementation VideoToBeUploadedViewController

+(VideoToBeUploadedViewController *)initViewController {
    VideoToBeUploadedViewController * controller = [[VideoToBeUploadedViewController alloc] initWithNibName:@"VideoToBeUploadedViewController" bundle:nil];
    controller.title = @"Videos to be Uploaded";
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    mainArray = [NSMutableArray new];
    selectedArray = [NSMutableArray new];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = false;
    [self registerNibs];
    [self setNavBarItems];
    [self getLocalVids];
}

-(void)registerNibs {
    [tblView registerNib:[UINib nibWithNibName:@"VideoUploadedCell" bundle:nil] forCellReuseIdentifier:@"VideoUploadedCell"];
}

-(void)setNavBarItems {
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[ColorConstants appBrownColor]}];
    
    UIImage *buttonImage = [UIImage imageNamed:@"back.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(backClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
    
    UIBarButtonItem * clearButton = [[UIBarButtonItem alloc] initWithTitle:@"Upload" style:UIBarButtonItemStylePlain target:self action:@selector(uploadVideoObj:)];
    clearButton.tintColor = [ColorConstants appBrownColor];
    self.navigationItem.rightBarButtonItem = clearButton;
}

-(void)backClicked {
    [self.navigationController popViewControllerAnimated:true];
}

-(void)getLocalVids {
    mainArray = [NSMutableArray new];
    NSMutableArray * localArray = [NSMutableArray new];
    localArray = [LocalVideoObject getAll];
    
    for (LocalVideoObject* vidObj in localArray) {
        if (vidObj.isUploading.length <= 0) { // Arun :: Because we dont have to show the currently uploading videos in the list.
            [mainArray addObject:vidObj];
        }
    }
    [tblView reloadData];
}

#pragma UITableViewDelegate,UITableViewDataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return mainArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VideoUploadedCell * cell = (VideoUploadedCell *)[tblView dequeueReusableCellWithIdentifier:@"VideoUploadedCell"];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"VideoUploadedCell" owner:self options:nil];
        for (id currentObject in topLevelObjects) {
            if ([currentObject isKindOfClass:[UITableViewCell class]]) {
                cell =  (VideoUploadedCell *) currentObject;
                break;
            }
        }
    }
    [cell.btnCheck setSelected:NO];
    NSString * rowStr = [NSString stringWithFormat:@"%ld", (long)indexPath.row];
    if ([selectedArray containsObject:rowStr]) {
        [cell.btnCheck setSelected:YES];
    }
    LocalVideoObject * vidObj = [mainArray objectAtIndex:indexPath.row];
    cell.lblTime.text = [NSString stringWithFormat:@"Taken on : %@", vidObj.time];
    
    NSData * videoData = [NSData dataWithContentsOfURL:[NSURL fileURLWithPath:vidObj.vidPath]];
    
    cell.lblSize.text = [NSString stringWithFormat:@"Size: %.2f MB",(float)videoData.length/1024.0f/1024.0f];

    cell.btnPlay.tag = indexPath.row;
    [cell.btnPlay addTarget:self action:@selector(playTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UIImage * img = [UIImage imageWithData:vidObj.thumbnail];
    if (img) {
        cell.imgVideo.image = img;
    }
    
    Plan * pl = [Plan getByEntityId:vidObj.videoTourId];
    AttractionData * attr = [AttractionData getByEntityId:vidObj.attrDataId];
    
    if (pl) {
        cell.lblName.text = [NSString stringWithFormat:@"%@ - %@", pl.name, attr.info.name];
    }
    
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString * rowStr = [NSString stringWithFormat:@"%ld", (long)indexPath.row];
    [tblView reloadData];
    LocalVideoObject * videObj = [mainArray objectAtIndex:indexPath.row];
    VideoUploadedCell *cell = [tblView cellForRowAtIndexPath:indexPath];
    if ([selectedArray containsObject:rowStr]) {
        [cell.btnCheck setSelected:NO];
        videObj.isSelected = @"No"; // Arun :: Incase we have to upload the videos through Instance class
        [selectedArray removeObject:rowStr];
    } else {
        [cell.btnCheck setSelected:YES];
        videObj.isSelected = @"Yes"; // Arun :: Incase we have to upload the videos through Instance class
        [selectedArray addObject:rowStr];
    }
    [LocalVideoObject saveEntity];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        LocalVideoObject * vidObj = [mainArray objectAtIndex:indexPath.row];
        [LocalVideoObject deleteObj:vidObj];
        [self getLocalVids];
    }
}

-(void)playTapped:(id)sender {
    NSInteger t = [sender tag];
    LocalVideoObject * a = [mainArray objectAtIndex:t];
    NSURL *movieUrl = [NSURL fileURLWithPath:a.vidPath];
    [self playVideoWithURL:movieUrl];
}

-(void)playVideoWithURL:(NSURL *)url {
    if (url) {
        self.videoController = [[MPMoviePlayerController alloc] init];
        [self.videoController setContentURL:url];
        [self.videoController.view setFrame:CGRectMake (0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.videoController.controlStyle = MPMovieControlStyleFullscreen;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoPlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:self.videoController];
        [self.view addSubview:self.videoController.view];
        [self.videoController play];
    }
}

- (void)videoPlayBackDidFinish:(NSNotification *)notification {
    [self.videoController stop];
    [self.videoController.view removeFromSuperview];
    self.videoController = nil;
}

-(void)uploadVideoObj:(id)sender {
    [self.videoController stop];
    [self.videoController.view removeFromSuperview];
    self.videoController = nil;
    Account * account = [AccountManager Instance].activeAccount;

    if (selectedArray.count <= 0) {
        [Utils showAlertWithMessage:@"Please select videos to upload"];
    } else {
        if (account.wifiOnly.boolValue) {
            for (NSString * aStr in selectedArray) {
                NSInteger a = aStr.integerValue;
                [self uploadForIndex:a];
                return;
            }
        } else {
            [[VideoSyncHelper Instance] startSync];
            UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Video will be saved and added to the attraction" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction * okayAction= [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [self.navigationController popViewControllerAnimated:true];
            }];
            [alert addAction:okayAction];
            [self.navigationController presentViewController:alert animated:YES completion:nil];
        }
    }
}

-(void) uploadForIndex:(NSInteger)index{
    [SVProgressHUD showWithStatus:@"Please wait..."];
    LocalVideoObject * vidObj = [mainArray objectAtIndex:index];
    NSURL *movieUrl = [NSURL fileURLWithPath:vidObj.vidPath];
    [[PlayTripManager Instance] uploadVideoWithVideoUrl:movieUrl WithAttrId:vidObj.attrDataId WithBlock:^(id result, NSString *error) {
        [LocalVideoObject deleteObj:vidObj];
        NSString * aStr = [NSString stringWithFormat:@"%ld", (long)index];
        [selectedArray removeObject:aStr];
        for (NSString * aStr in selectedArray) {
            NSInteger a = aStr.integerValue;
            [self uploadForIndex:a];
        }
        [SVProgressHUD dismiss];
    }];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.videoController stop];
    [self.videoController.view removeFromSuperview];
    self.videoController = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

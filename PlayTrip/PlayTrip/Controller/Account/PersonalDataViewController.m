
#import "PersonalDataViewController.h"
#import "ColorConstants.h"

@interface PersonalDataViewController ()
@end

@implementation PersonalDataViewController

+(PersonalDataViewController *) initViewController {
    PersonalDataViewController * controller = [[PersonalDataViewController alloc] initWithNibName:@"PersonalDataViewController" bundle:nil];
    controller.title = @"Personal Data";
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setNavBarItems];
    
    [self setBottomBorderForTextField:txtAccountName];
    [self setBottomBorderForTextField:txtAccountNumber];
}

-(void)setNavBarItems {
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[ColorConstants appBrownColor]}];
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleDone target:self action:@selector(backClicked)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    UIBarButtonItem * saveButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStyleDone target:self action:@selector(backClicked)];
    self.navigationItem.rightBarButtonItem = saveButton;
    
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.shadowImage = [UIImage new];
//    self.navigationController.view.backgroundColor = [UIColo];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

-(void)backClicked {
    [self.navigationController dismissViewControllerAnimated:true completion:nil];
}

-(void)saveClicked {
    [self.navigationController dismissViewControllerAnimated:true completion:nil];
}

-(void)setBottomBorderForTextField:(UITextField *)textField {
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 2;
    border.borderColor = [UIColor darkGrayColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

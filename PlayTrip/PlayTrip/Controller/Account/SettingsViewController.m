
#import "SettingsViewController.h"
#import "ColorConstants.h"
#import "SettingUserCell.h"
#import "SettingLanguageCell.h"
#import "SettingNotificationCell.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "VideoToBeUploadedViewController.h"
#import "Localisator.h"


#define MobileTextTag        @10;
#define EmailTextTag         @20;
#define BankNameTextTag      @30;
#define BankAccountTextTag   @40;
#define HolderNameTextTag    @50;

@interface SettingsViewController () {
    int selectedLanguageIndex;
}

@property NSArray * arrayOfLanguages;

@end

@implementation SettingsViewController

+(SettingsViewController *)initViewController {
    SettingsViewController * controller = [[SettingsViewController alloc] initWithNibName:@"SettingsViewController" bundle:nil];
    controller.title = LOCALIZATION(@"settings");
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.arrayOfLanguages = [[[Localisator sharedInstance] availableLanguagesArray] copy];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveLanguageChangedNotification:)
                                                 name:@"languageChanged"
                                               object:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = false;
    [self setNavBarItems];
    [self getUserDetails];
    isSave = false;
}

- (void) receiveLanguageChangedNotification:(NSNotification *) notification
{
    if ([notification.name isEqualToString:@"languageChanged"]){
        [self setLocalizedStrings];
    }
}
-(void)setLocalizedStrings {
    self.title = LOCALIZATION(@"settings");
    
    UIBarButtonItem * rightButton = [[UIBarButtonItem alloc] initWithTitle:LOCALIZATION(@"save") style:UIBarButtonItemStyleDone target:self action:@selector(saveUserDetails)];
    // saveUserDetails
    [rightButton setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys: [ColorConstants appBrownColor], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    
    [tblView reloadData];

}


-(void)registerNibs {
    [tblView registerNib: [UINib nibWithNibName:@"SettingUserCell" bundle:nil] forCellReuseIdentifier:@"SettingUserCell"];
    [tblView registerNib: [UINib nibWithNibName:@"SettingLanguageCell" bundle:nil] forCellReuseIdentifier:@"SettingLanguageCell"];
    [tblView registerNib: [UINib nibWithNibName:@"SettingNotificationCell" bundle:nil] forCellReuseIdentifier:@"SettingNotificationCell"];
}

-(void)setNavBarItems {
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[ColorConstants appBrownColor]}];
    self.navigationController.navigationBar.barTintColor = [ColorConstants appYellowColor];
    self.navigationController.navigationBar.translucent = NO;
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor blackColor]}];
    
    UIImage *buttonImage = [UIImage imageNamed:@"back.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(backClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
    
    UIBarButtonItem * rightButton = [[UIBarButtonItem alloc] initWithTitle:LOCALIZATION(@"save") style:UIBarButtonItemStyleDone target:self action:@selector(saveUserDetails)];
    // saveUserDetails
    [rightButton setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys: [ColorConstants appBrownColor], NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = rightButton;
 
    [self.navigationController.navigationBar setTitleTextAttributes: @{NSForegroundColorAttributeName:[ColorConstants appBrownColor]}];
}

-(void)getUserDetails {
    Account * account = [AccountManager Instance].activeAccount;
    [account getUserDetails:self];
}

-(void)saveData {
    Account * account = [AccountManager Instance].activeAccount;
    mobileNumber = account.phone_number;
    email = account.email;
    bankName = account.bank_name;
    bankAccount = account.bank_account;
    holderName = account.holder_name;
    
    changedLanguage = account.language;
    changedCurrency = account.currency;
    
    reservation = [account.notification_made_reservation boolValue];
    receiveRewards = [account.notification_get_reward boolValue];
    updates = [account.notification_bookmark_user_update boolValue];
    received = [account.notification_reward_information boolValue];
    wifiOnly = [account.wifiOnly boolValue];
}

-(void)saveUserDetails {
    isSave = true;
    
    if ([[Localisator sharedInstance] setLanguage:self.arrayOfLanguages[selectedLanguageIndex]])
    {
        if ([self.arrayOfLanguages[selectedLanguageIndex] isEqualToString:@"en"]) {
            changedLanguage = @"en";
        }
        else if ([self.arrayOfLanguages[selectedLanguageIndex] isEqualToString:@"zh-Hans"]) {
            changedLanguage = @"sc";
        }
        else{
            changedLanguage = @"tc";
        }
    }

    
    
    
    Account * account = [AccountManager Instance].activeAccount;
    if (mobileNumber.length > 0) {
        account.phone_number = mobileNumber;
    }
    if (email.length > 0) {
        account.email = email;
    }
    if (bankName.length > 0) {
        account.bank_name = bankName;
    }
    if (bankAccount.length > 0) {
        account.bank_account = bankAccount;
    }
    if (holderName.length > 0) {
        account.holder_name = holderName;
    }
    if (changedLanguage.length > 0) {
        account.language = changedLanguage;
    }
    if (changedCurrency.length > 0) {
        account.currency = changedCurrency;
    }
    
    account.notification_made_reservation = [NSNumber numberWithBool:reservation];
    account.notification_get_reward = [NSNumber numberWithBool:receiveRewards];
    account.notification_bookmark_user_update = [NSNumber numberWithBool:updates];
    account.notification_reward_information = [NSNumber numberWithBool:received];
    
    account.wifiOnly = [NSNumber numberWithBool:wifiOnly];
    [account updateUserDetails:self];
}

-(void)uploadUserAvtar {
    Account * account = [AccountManager Instance].activeAccount;
    UIImage * image = [UIImage imageNamed:@"hi.jpg"];
    [account uploadAvtar:image WithDelegate:self];
}

-(void)backClicked {
    [self.navigationController popViewControllerAnimated:NO];
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.tag == 1) {
        mobileNumber = textField.text;
    } else if (textField.tag == 2) {
        email = textField.text;
    } else if (textField.tag == 3) {
        bankName = textField.text;
    } else if (textField.tag == 4) {
        bankAccount = textField.text;
    } else if (textField.tag == 5) {
        holderName = textField.text;
    }
}

-(void)langChiSelected {

    selectedLanguageIndex = 0;

    /*
    changedLanguage = Language_Traditional_Chineese;
    [Utils setBOOL:true Key:kIsUpdatedLanguage];
    [Utils setValue:Language_Traditional_Chineese Key:kUpdatedLanguage];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.navigationController popToRootViewControllerAnimated:true];
    
    
    
    if ([[Localisator sharedInstance] setLanguage:self.arrayOfLanguages[0]])
    {
        
    }
     */
    
}
-(void)langSimSelected {
    
    selectedLanguageIndex = 1;

    /*
    changedLanguage = Language_Simple_Chineese;
    [Utils setBOOL:true Key:kIsUpdatedLanguage];
    [Utils setValue:Language_Simple_Chineese Key:kUpdatedLanguage];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //[self.navigationController popToRootViewControllerAnimated:true];
     

    if ([[Localisator sharedInstance] setLanguage:self.arrayOfLanguages[1]])
    {
        
    }
     */
}
-(void)langEngSelected {
    
    selectedLanguageIndex = 2;

    /*
    changedLanguage = Language_English;s
    [Utils setBOOL:true Key:kIsUpdatedLanguage];
    [Utils setValue:Language_English Key:kUpdatedLanguage];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //[self.navigationController popToRootViewControllerAnimated:true];
     

    if ([[Localisator sharedInstance] setLanguage:self.arrayOfLanguages[2]])
    {
        
    }
     */
}

-(void)currHKDSelected {
    changedCurrency = Currency_HKD;
}
-(void)currRMBSelected {
    changedCurrency = Currency_RMB;
}
-(void)currUSDSelected {
    changedCurrency = Currency_USD;
}

- (void)setSwitchState:(id)sender {
    
    UISwitch * switchA = (UISwitch *) sender;
    if ([sender tag] == 1) {
        if ([switchA isOn]) {
            reservation = true;
        } else {
            reservation = false;
        }
    } else if ([sender tag] == 2) {
        if ([switchA isOn]) {
            receiveRewards = true;
        } else {
            receiveRewards = false;
        }
    } else if ([sender tag] == 3) {
        if ([switchA isOn]) {
            updates = true;
        } else {
            updates = false;
        }
    } else if ([sender tag] == 4) {
        if ([switchA isOn]) {
            received = true;
        } else {
            received = false;
        }
    } else if ([sender tag] == 5) {
        if ([switchA isOn]) {
            wifiOnly = true;
        } else {
            wifiOnly = false;
        }
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 2;
    }else if (section == 1) {
        return 2;
    } else if (section == 2) {
        return 1;
    } else if (section == 3) {
        return 1;
    } else if (section == 4) {
        return 0;
    } else if (section == 5) {
        return 0;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 3) {
        return 0;
    }
    else if(section == 1){
        return 0;
    }
    else{
        return 35;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return  LOCALIZATION(@"user_information");
    }else if (section == 1) {
        return  LOCALIZATION(@"Video_Uploading");
    } else if (section == 2) {
        //return  @"Receive Rewards Information";
        return  LOCALIZATION(@"language_&_currency");
    } else if (section == 3) {
        return  @"   ";
    } else {
        return  @"   ";
    }
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {

    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    [header.textLabel setTextColor:[ColorConstants appBrownColor]];
    header.textLabel.font = [UIFont systemFontOfSize:12.0];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {   
    if (indexPath.section == 0) {
        SettingUserCell *cell = (SettingUserCell *)[tblView dequeueReusableCellWithIdentifier:@"SettingUserCell"];
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SettingUserCell" owner:self options:nil];
        for (id currentObject in topLevelObjects) {
            if ([currentObject isKindOfClass:[UITableViewCell class]]) {
                cell =  (SettingUserCell *) currentObject;
                break;
            }
        }
        cell.txtValue.delegate = self;
        if (indexPath.row == 0) {
            cell.lblKey.text = LOCALIZATION(@"mobile_number");
            if (mobileNumber && mobileNumber != nil && mobileNumber != (id)[NSNull null]) {
                cell.txtValue.text = mobileNumber;
            }
            cell.txtValue.keyboardType = UIKeyboardTypeNumberPad;
            cell.txtValue.tag = 1;
        } else if (indexPath.row == 1){
            cell.lblKey.text = LOCALIZATION(@"email_settings");
            if (email && email != nil) {
                cell.txtValue.text = email;
                cell.txtValue.userInteractionEnabled = NO;
            }
            cell.txtValue.tag = 2;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.section == 1) {
        SettingLanguageCell *cell = (SettingLanguageCell *)[tableView dequeueReusableCellWithIdentifier:@"SettingLanguageCell"];
        
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SettingLanguageCell" owner:self options:nil];
        for (id currentObject in topLevelObjects) {
            if ([currentObject isKindOfClass:[UITableViewCell class]]) {
                cell =  (SettingLanguageCell *) currentObject;
                break;
            }
        }
        
        
        
        
        
        if (indexPath.row == 0) {
            cell.lblKey.text = LOCALIZATION(@"language_settings");
            
            if (changedLanguage && changedLanguage != nil) {
                if ([changedLanguage isEqualToString:Language_Traditional_Chineese]) {
                    [cell.btn1 sendActionsForControlEvents:UIControlEventTouchUpInside];
                } else if ([changedLanguage isEqualToString:Language_Simple_Chineese]) {
                    [cell.btn2 sendActionsForControlEvents:UIControlEventTouchUpInside];
                } else {
                    [cell.btn3 sendActionsForControlEvents:UIControlEventTouchUpInside];
                }
            }
            [cell.btn1 setTitle:@"繁" forState:UIControlStateNormal];
            [cell.btn2 setTitle:@"简" forState:UIControlStateNormal];
            [cell.btn3 setTitle:@"ENG" forState:UIControlStateNormal];
            [cell.btn1 addTarget:self action:@selector(langChiSelected) forControlEvents:UIControlEventTouchUpInside];
            [cell.btn2 addTarget:self action:@selector(langSimSelected) forControlEvents:UIControlEventTouchUpInside];
            [cell.btn3 addTarget:self action:@selector(langEngSelected) forControlEvents:UIControlEventTouchUpInside];
            
            if ([changedLanguage isEqualToString:@"tc"]) {
                if (!cell.btn1.selected) {
                    [cell btn1Clicked:cell.btn1];
                    [self langChiSelected];
                }
            }
            else if ([changedLanguage isEqualToString:@"sc"]) {
                if (!cell.btn2.selected) {
                    [cell btn2Clicked:cell.btn2];
                    [self langSimSelected];
                }
            }
            else{
                if (!cell.btn3.selected) {
                    [cell btn3Clicked:cell.btn3];
                    [self langEngSelected];
                }
            }
            
            
            //            cell.segControl.tag = 1;
        }  else {
            cell.lblKey.text = LOCALIZATION(@"currency_settings");
            
            if (changedCurrency && changedCurrency != nil) {
                if ([changedCurrency isEqualToString:Currency_RMB]) {
                    [cell.btn1 sendActionsForControlEvents:UIControlEventTouchUpInside];
                } else if ([changedCurrency isEqualToString:Currency_RMB]) {
                    [cell.btn2 sendActionsForControlEvents:UIControlEventTouchUpInside];
                } else {
                    [cell.btn3 sendActionsForControlEvents:UIControlEventTouchUpInside];
                }
            }
            
            [cell.btn1 setTitle:@"HKD" forState:UIControlStateNormal];
            [cell.btn2 setTitle:@"RMB" forState:UIControlStateNormal];
            [cell.btn3 setTitle:@"USD" forState:UIControlStateNormal];
            [cell.btn1 addTarget:self action:@selector(currHKDSelected) forControlEvents:UIControlEventTouchUpInside];
            [cell.btn2 addTarget:self action:@selector(currRMBSelected) forControlEvents:UIControlEventTouchUpInside];
            [cell.btn3 addTarget:self action:@selector(currUSDSelected) forControlEvents:UIControlEventTouchUpInside];
            
            
            //            cell.segControl.tag = 2;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        /*
        SettingUserCell *cell = (SettingUserCell *)[tableView dequeueReusableCellWithIdentifier:@"SettingUserCell"];
    
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SettingUserCell" owner:self options:nil];
        for (id currentObject in topLevelObjects) {
            if ([currentObject isKindOfClass:[UITableViewCell class]]) {
                cell =  (SettingUserCell *) currentObject;
                break;
            }
        }
     
        cell.txtValue.delegate = self;
        if (indexPath.row == 0) {
            cell.lblKey.text = @"Bank";
            if (bankName && bankName != nil) {
                cell.txtValue.text = bankName;
            }
            cell.txtValue.tag = 3;
        }  else if (indexPath.row == 1) {
            cell.lblKey.text = @"Account No";
            if (bankAccount && bankAccount != nil) {
                cell.txtValue.text = bankAccount;
            }
            cell.txtValue.tag = 4;
            cell.txtValue.keyboardType = UIKeyboardTypeNumberPad;
        } else {
            cell.lblKey.text = @"Name";
            if (holderName && holderName != nil) {
                cell.txtValue.text = holderName;
            }
            cell.txtValue.tag = 5;
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
         */
        
    } else {
        SettingUserCell *cell = (SettingUserCell *)[tableView dequeueReusableCellWithIdentifier:@"SettingUserCell"];
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"SettingUserCell" owner:self options:nil];
            for (id currentObject in topLevelObjects) {
                if ([currentObject isKindOfClass:[UITableViewCell class]]) {
                    cell =  (SettingUserCell *) currentObject;
                    break;
                }
            }
        }
        cell.lblKey.text = LOCALIZATION(@"logout");
        cell.txtValue.hidden = true;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2) {
        [self askLogout];
    }
    NSLog(@"Section is %li",(long)indexPath.section);
}

-(void)askLogout {
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Do you want to logout?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * yesAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [[AppDelegate appDelegate] logout];
    }];
    
    UIAlertAction * noAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alert addAction:yesAction];
    [alert addAction:noAction];
    [self.navigationController presentViewController:alert animated:true completion:nil];
}

-(void)accountAuthenticatedWithAccount:(Account*) account{
    if (isSave) {
        [self.navigationController popToRootViewControllerAnimated:true];
    } else {
        [self saveData];
        [self registerNibs];
        tblView.delegate = self;
        tblView.dataSource = self;
        [tblView reloadData];
    }
}

-(void)accountDidFailAuthentication:(NSString*) error{
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:ALERT_TITLE message:@"Something went wrong. Please try after sometimes." preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * okayAction = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:nil];
    [alert addAction:okayAction];
    [self.navigationController presentViewController:alert animated:true completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end


#import <UIKit/UIKit.h>
#import "CommonTextField.h"
#import "Account.h"
@interface ResetPasswordViewController : UIViewController<AccountAuthenticateDelegate, UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet CommonTextField *txtResetEmail;

+(ResetPasswordViewController *)initViewController;
- (IBAction)backClicked:(id)sender;
- (IBAction)actionReset:(id)sender;



@property (strong, nonatomic) IBOutlet UILabel *lblResetPass;
@property (strong, nonatomic) IBOutlet UIButton *btnReset;






@end

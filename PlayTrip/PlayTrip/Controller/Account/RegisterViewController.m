
#import "RegisterViewController.h"
#import "KGModalWrapper.h"
#import "Utils.h"
#import "Constants.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "FbHelper.h"
#import "GoogleHelper.h"
#import "AppDelegate.h"
#import "AccountManager.h"
#import "Localisator.h"

@interface RegisterViewController (){
    NSString *imageUrl;
}
@end

@implementation RegisterViewController

+(RegisterViewController *)initViewController {
    RegisterViewController * controller = [[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    controller->lblTopMain =  [Utils roundCornersOnView:controller->lblTopMain onTopLeft:YES topRight:YES bottomLeft:NO bottomRight:NO radius:10];
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveLanguageChangedNotification:)
                                                 name:kNotificationLanguageChanged
                                               object:nil];
    
    [self setLocalizedStrings];
}

- (void) receiveLanguageChangedNotification:(NSNotification *) notification
{
    if ([notification.name isEqualToString:@"languageChanged"])
    {
        [self setLocalizedStrings];
    }
}

-(void)setLocalizedStrings {
    
    [self.lblRegister setText:LOCALIZATION(@"register")];
    [self.lblOr setText:LOCALIZATION(@"or")];
    [self.lblHaveAnAccount setText:LOCALIZATION(@"have_an_account")];
    
    [self.btnLogin setTitle:LOCALIZATION(@"login") forState:UIControlStateNormal];
    [self.btnSingup setTitle:LOCALIZATION(@"sign_up") forState:UIControlStateNormal];

    [txtUsername setPlaceholder:LOCALIZATION(@"username")];
    [txtEmail setPlaceholder:LOCALIZATION(@"email")];
    [txtPassword setPlaceholder:LOCALIZATION(@"password")];
    [txtConfirmPassword setPlaceholder:LOCALIZATION(@"comfirm_password")];
    
    /*
    self.lblRegister.text = [NSString stringWithFormat:NSLocalizedString(@"register", nil)];
    self.lblOr.text = [NSString stringWithFormat:NSLocalizedString(@"or", nil)];
    self.lblHaveAnAccount.text = [NSString stringWithFormat:NSLocalizedString(@"have_an_account", nil)];
    
    [self.btnLogin setTitle:NSLocalizedString(@"login", nil) forState:UIControlStateNormal];
    [self.btnSingup setTitle:NSLocalizedString(@"sign_up", nil) forState:UIControlStateNormal];
    
    txtUsername.placeholder = [NSString stringWithFormat:NSLocalizedString(@"username", nil)];
    txtEmail.placeholder = [NSString stringWithFormat:NSLocalizedString(@"email", nil)];
    txtPassword.placeholder = [NSString stringWithFormat:NSLocalizedString(@"password", nil)];
    txtConfirmPassword.placeholder = [NSString stringWithFormat:NSLocalizedString(@"comfirm_password", nil)];
    */
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
   self.navigationController.navigationBarHidden = true;
    [self getTermsAndConditions];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    scrollViewMain.contentSize = CGSizeMake(280, 350);
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = false;
}

-(void)getTermsAndConditions {
   /* [[PlayTripManager Instance] getTermsAndConditionsWithBlock:^(id result, NSString *error) {
        if (!error) {
     
            NSMutableDictionary * res = result;
            NSArray * desc = [res valueForKey:@"description"];
            [descView setText:[desc objectAtIndex:0]];
        }
    }];*/
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    [scrollViewMain scrollRectToVisible:CGRectMake(0, 0, 1, 1) animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    UIFont * f = [UIFont systemFontOfSize:15.0];
    CGFloat width = [Utils widthOfString:newString withFont:f];
    
    if (width > 200) {
        UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        textField.leftView = paddingView;
        textField.leftViewMode = UITextFieldViewModeAlways;
        
       
    } else {
        textField.leftViewMode = UITextFieldViewModeNever;
    }
    
    return YES;
}

#pragma mark -- Google Login Delegate Method

- (IBAction)gClicked:(id)sender {
    
    /*
     this method used when signin button clicked
     */
    if(![[AppDelegate appDelegate] isReachable]){
        [Utils showAlertWithMessage:kCheckInternet];
        return;
    }
    GIDSignIn* signIn = [GIDSignIn sharedInstance];
    signIn.scopes = @[ @"profile", @"email" ];
    signIn.delegate = self;
    signIn.uiDelegate = self;
    
    [[GIDSignIn sharedInstance] signIn];
}

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    /*
     in this method we can get response from google
     */
    if (error == nil) {
        
        CGSize thumbSize=CGSizeMake(100, 100);
        if ([GIDSignIn sharedInstance].currentUser.profile.hasImage)
        {
            NSUInteger dimension = round(thumbSize.width * [[UIScreen mainScreen] scale]);
            NSURL *imageURL = [user.profile imageURLWithDimension:dimension];
            imageUrl = imageURL.absoluteString;
        }
        
        Account *account=[[Account alloc]init];
        account.email = user.profile.email;
        account.first_name =user.profile.name;
        account.last_name = user.profile.givenName;
        account.image = [user.profile imageURLWithDimension:200].absoluteString;
        account.provider = @"Google";
        account.username = user.profile.name;
        NSString *selectedLanguage = [Utils getValueForKey:kUpdatedLanguage];
        if ([selectedLanguage isEqualToString:Language_Simple_Chineese]) {
            account.language = @"sc";
        }
        else if ([selectedLanguage isEqualToString:Language_Traditional_Chineese]){
            account.language = @"tc";
        }
        else{
            account.language = @"en";
        }

        
        
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
        [SVProgressHUD showWithStatus:@"Loading..."];
        [account registerWithDelegate:self];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }else{
        [Utils showAlertWithMessage:error.localizedDescription];
    }
}

- (void)signIn:(GIDSignIn *)signIn didDisconnectWithUser:(GIDGoogleUser *)user withError:(NSError *)error {
}

- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error{

}
- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController{
    /*
     used for present view controller
     */
    [self.navigationController presentViewController:viewController animated:YES completion:nil];
}
- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController{
    
}

- (IBAction)signUpClicked:(id)sender {
    
    /*
     this method is used for validation and API calling
     */
    
    NSString *strMessage;
    if (txtUsername.text.length == 0) {
        strMessage = @"Please Enter Username";
    }else if (txtEmail.text.length == 0){
        strMessage = @"Please Enter Email";
    }else if (txtPassword.text.length == 0){
        strMessage = @"Please Enter Password";
    }else if (txtConfirmPassword.text.length == 0){
        strMessage = @"Please Enter Confirm Password";
    }else if (![txtConfirmPassword.text isEqualToString:txtPassword.text]){
        strMessage = @"Password and ConfirmPassword must be same";
    }else if (![Utils validateEmailWithString:txtEmail.text]){
        strMessage = @"Please Enter Email in proper Format";
    }
    else if (txtPassword.text.length < 8) {
        strMessage = @"Password must be atleast 8 characters";
    }
    
    if (strMessage.length > 0) {
        [Utils showAlertWithMessage:strMessage];
        return;
    }
    if(![[AppDelegate appDelegate] isReachable]){
        [Utils showAlertWithMessage:kCheckInternet];
        return;
    }
    Account *account=[[Account alloc]init];
    account.username = txtUsername.text;
    account.password = txtPassword.text;
    account.email = txtEmail.text;
    account.first_name = txtUsername.text;
    account.last_name = @"";
    
    NSString *selectedLanguage = [Utils getValueForKey:kUpdatedLanguage];
    if ([selectedLanguage isEqualToString:Language_Simple_Chineese]) {
        account.language = @"sc";
    }
    else if ([selectedLanguage isEqualToString:Language_Traditional_Chineese]){
        account.language = @"tc";
    }
    else{
        account.language = @"en";
    }

    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Logging In..."];
    [account registerWithDelegate:self];
    
    
}

- (IBAction)gotoLoginClicked:(id)sender {
    [self.navigationController dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)fbClicked:(id)sender {
    /*
     this method is used for facebook Login
     */
    
    if(![[AppDelegate appDelegate] isReachable]){
        [Utils showAlertWithMessage:kCheckInternet];
        return;
    }
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logInWithReadPermissions:@[@"public_profile",@"email",@"user_birthday"] fromViewController:self handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
       if (result.isCancelled) {
            NSLog(@"Token: %@", result.token);
        } else {
            if ([FBSDKAccessToken currentAccessToken]) {
                FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]initWithGraphPath:@"me" parameters:@{@"fields" : @"id,name,email,first_name,last_name,picture"                                                                                                            }];
                [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                    if (result) {
                        Account *account=[[Account alloc]init];
                        account.email = [NSString stringWithFormat:@"%@",[result objectForKey:@"email"]];
                        account.first_name = [NSString stringWithFormat:@"%@",[result objectForKey:@"first_name"]];
                        account.provider = @"facebook";
                         account.loginTypeObject = result;
                        account.username = [result objectForKey:@"first_name"];
                        account.last_name = [result objectForKey:@"last_name"];
                        
                        
                        NSString *selectedLanguage = [Utils getValueForKey:kUpdatedLanguage];
                        if ([selectedLanguage isEqualToString:Language_Simple_Chineese]) {
                            account.language = @"sc";
                        }
                        else if ([selectedLanguage isEqualToString:Language_Traditional_Chineese]){
                            account.language = @"tc";
                        }
                        else{
                            account.language = @"en";
                        }

                        
                        
                        NSString *imageStringOfLoginUser = [[[result valueForKey:@"picture"] valueForKey:@"data"] valueForKey:@"url"];
                        account.avatar = imageStringOfLoginUser;
                        imageUrl = imageStringOfLoginUser;

                        
                        
                        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
                        [SVProgressHUD showWithStatus:@"Signin..."];
                        [account registerWithDelegate:self];
                    }
                }];
            }
        }
    }];
}

#pragma mark -- Account Delegate Methods

-(void)accountAuthenticatedWithAccount:(Account*) account{
    /*
     Delegate method for Account.Here we get success Response of Registration
     */
    account.isLoggedIn = true;
    account.isSkip = false;
    tempAccount = account;
    [SVProgressHUD dismiss];
    
    
    [self termsClicked:self];
    if (imageUrl) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSURL *imageURL = [NSURL URLWithString:imageUrl];
            
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            
            UIImage *image = [UIImage imageWithData:imageData];
            
            [account uploadAvtar:image WithDelegate:nil];
            
            
        });
    }
}

-(void)accountDidFailAuthentication:(NSString*) error{
    /*
     Delegate method for Account.Here we get Failure Response of Registration
     */
    [SVProgressHUD dismiss];
    [Utils showAlertWithMessage:error];
}

- (IBAction)termsClicked:(id)sender {
    btnAcceptTerms.selected = true;
    imgCheckBox.image = [UIImage imageNamed:@"checked_black.png"];
    [KGModalWrapper showWithContentView:termsView];
}

- (IBAction)acceptTermsClicked:(id)sender {
    if ([btnAcceptTerms isSelected]) {
        btnAcceptTerms.selected = false;
        imgCheckBox.image = [UIImage imageNamed:@"unchecked_black.png"];
    } else {
        btnAcceptTerms.selected = true;
        imgCheckBox.image = [UIImage imageNamed:@"checked_black.png"];
    }
}

- (IBAction)understandClicked:(id)sender {
    if ([btnAcceptTerms isSelected]) {
        [KGModalWrapper hideView];
        NSString *selectedLanguage = [Utils getValueForKey:kUpdatedLanguage];
        if ([selectedLanguage isEqualToString:Language_Simple_Chineese]) {
            tempAccount.language = @"sc";
        }
        else if ([selectedLanguage isEqualToString:Language_Traditional_Chineese]){
            tempAccount.language = @"tc";
        }
        else{
            tempAccount.language = @"en";
        }
        [AccountManager Instance].activeAccount = tempAccount;
        [[AppDelegate appDelegate] userDidLogin];
        [[AccountManager Instance].activeAccount saveLanguage:nil];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end

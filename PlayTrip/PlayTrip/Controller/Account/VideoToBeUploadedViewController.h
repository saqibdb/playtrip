
#import <UIKit/UIKit.h>
#import "LocalVideoObject.h"
#import <MediaPlayer/MediaPlayer.h>

@interface VideoToBeUploadedViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>{
    IBOutlet UITableView *tblView;
    NSMutableArray * mainArray;
    NSMutableArray * selectedArray;
}
@property (strong, nonatomic) MPMoviePlayerController *videoController;

+(VideoToBeUploadedViewController *)initViewController;

@end

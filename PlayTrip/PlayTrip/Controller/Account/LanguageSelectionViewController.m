
#import "LanguageSelectionViewController.h"
#import "Utils.h"
#import "Constants.h"
#import "Localisator.h"

@interface LanguageSelectionViewController ()


@property NSArray * arrayOfLanguages;


@end

@implementation LanguageSelectionViewController

+(LanguageSelectionViewController *)initViewController {
    LanguageSelectionViewController * controller = [[LanguageSelectionViewController alloc] initWithNibName:@"LanguageSelectionViewController" bundle:nil];
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.arrayOfLanguages = [[[Localisator sharedInstance] availableLanguagesArray] copy];
    [self setLocalizedStrings];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveLanguageChangedNotification:)
                                                 name:kNotificationLanguageChanged
                                               object:nil];
    
}

- (void) receiveLanguageChangedNotification:(NSNotification *) notification
{
    if ([notification.name isEqualToString:@"languageChanged"])
    {
        [self setLocalizedStrings];
    }
}

-(void)setLocalizedStrings {
    
    [self.lblChooseLanguage setText:LOCALIZATION(@"choose_language")];
    [self.btnNext setTitle:LOCALIZATION(@"next") forState:UIControlStateNormal];
    
    /*
    self.lblChooseLanguage.text = [NSString stringWithFormat:NSLocalizedString(@"choose_language", nil)];
    [self.btnNext setTitle:NSLocalizedString(@"next", nil) forState:UIControlStateNormal];
    */
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = true;
    self.navigationItem.hidesBackButton = YES;
    
    [self btnLanguage3Clicked:btnLanguage3];
}

- (IBAction)btnLanguage1Clicked:(id)sender {
    btnLanguage1.selected = true;
    btnLanguage2.selected = false;
    btnLanguage3.selected = false;
    
    /*
    [[NSUserDefaults standardUserDefaults] setObject:@"zh-Hans" forKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLanguageSet"];
    [[NSUserDefaults standardUserDefaults] synchronize];
     */
}

- (IBAction)btnLanguage2Clicked:(id)sender {
    btnLanguage1.selected = false;
    btnLanguage2.selected = true;
    btnLanguage3.selected = false;
    
    /*
    [[NSUserDefaults standardUserDefaults] setObject:@"zh-Hant" forKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLanguageSet"];
    [[NSUserDefaults standardUserDefaults] synchronize];
     */
}

- (IBAction)btnLanguage3Clicked:(id)sender {
    btnLanguage1.selected = false;
    btnLanguage2.selected = false;
    btnLanguage3.selected = true;
    
     /*
    [[NSUserDefaults standardUserDefaults] setObject:@"en" forKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isLanguageSet"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    */
}

- (IBAction)nextClicked:(id)sender{

    [Utils setBOOL:true Key:kIsUpdatedLanguage];
    if (btnLanguage1.selected) {
        [Utils setValue:Language_Simple_Chineese Key:kUpdatedLanguage];
        if (![[Localisator sharedInstance].currentLanguage isEqualToString:self.arrayOfLanguages[1]]) {
            [[Localisator sharedInstance] setLanguage:self.arrayOfLanguages[1]];
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLanguageChanged object:nil];
        }
    } else if (btnLanguage2.selected) {
        [Utils setValue:Language_Traditional_Chineese Key:kUpdatedLanguage];
        if (![[Localisator sharedInstance].currentLanguage isEqualToString:self.arrayOfLanguages[0]]) {
            [[Localisator sharedInstance] setLanguage:self.arrayOfLanguages[0]];
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLanguageChanged object:nil];
        }
    } else{
        [Utils setValue:Language_English Key:kUpdatedLanguage];
        if (![[Localisator sharedInstance].currentLanguage isEqualToString:self.arrayOfLanguages[2]]) {
            [[Localisator sharedInstance] setLanguage:self.arrayOfLanguages[2]];
            [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLanguageChanged object:nil];
        }
    }
    
    [self setLocalizedStrings];
    [self.view layoutIfNeeded];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.navigationController popToRootViewControllerAnimated:true];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

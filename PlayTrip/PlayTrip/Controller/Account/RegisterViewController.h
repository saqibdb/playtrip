
#import <UIKit/UIKit.h>
#import <GoogleSignIn/GoogleSignIn.h>
#import "Account.h"
#import "RTLabel.h"

@interface RegisterViewController : UIViewController<AccountAuthenticateDelegate,GIDSignInUIDelegate,GIDSignInDelegate, UITextFieldDelegate> {
    
    IBOutlet UILabel *lblTopMain;
    IBOutlet UIScrollView *scrollViewMain;
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtUsername;
    
    IBOutlet UITextField *txtPassword;
    IBOutlet UITextField *txtConfirmPassword;
    
    IBOutlet RTLabel *descView;
    
    Account * tempAccount;
    IBOutlet UIButton *btnAcceptTerms;
    IBOutlet UIImageView *imgCheckBox;
    IBOutlet UIView *termsView;
}

- (IBAction)acceptTermsClicked:(id)sender;
- (IBAction)gClicked:(id)sender;
- (IBAction)signUpClicked:(id)sender;
- (IBAction)understandClicked:(id)sender;
- (IBAction)gotoLoginClicked:(id)sender;
- (IBAction)backClicked:(id)sender;

- (IBAction)fbClicked:(id)sender;
+(RegisterViewController *)initViewController;



@property (strong, nonatomic) IBOutlet UILabel *lblRegister;
@property (strong, nonatomic) IBOutlet UILabel *lblOr;
@property (strong, nonatomic) IBOutlet UILabel *lblHaveAnAccount;
@property (strong, nonatomic) IBOutlet UIButton *btnLogin;
@property (strong, nonatomic) IBOutlet UIButton *btnSingup;






@end

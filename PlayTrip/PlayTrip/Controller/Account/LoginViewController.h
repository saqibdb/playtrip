
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "LatestVideoTourViewController.h"
#import "AccountManager.h"
#import "Account.h"
#import "RTLabel.h"
#import "CommonTextField.h"
#import <GoogleSignIn/GoogleSignIn.h>

@interface LoginViewController : UIViewController <AccountAuthenticateDelegate, CLLocationManagerDelegate, GIDSignInUIDelegate,GIDSignInDelegate, UITextFieldDelegate> {
    
    IBOutlet CommonTextField *txtUserName;
    IBOutlet CommonTextField *txtPassword;
    IBOutlet UILabel *lblTopMain;
    IBOutlet UIButton *btnAgree;
    IBOutlet UILabel *lblAgreeText;
    
    CLLocationManager *locationManager;
    CLLocation *currentLocation;
    NSMutableArray * locArray;
    
    IBOutlet UIView *termsView;
    IBOutlet RTLabel *descView;
    
    IBOutlet UIButton *btnAcceptTerms;
    IBOutlet UIImageView *imgCheckBox;
}

+(LoginViewController *)initViewController;

@property (strong, nonatomic) IBOutlet UILabel *lblLogin;
@property (strong, nonatomic) IBOutlet UILabel *lblOr;
@property (strong, nonatomic) IBOutlet UIButton *btnForgotPassword;
@property (strong, nonatomic) IBOutlet UIButton *btnRegister;
@property (strong, nonatomic) IBOutlet UIButton *btnSkip;
@property (strong, nonatomic) IBOutlet UIButton *btnLogin;




- (IBAction)btnGoogleClicked:(id)sender;
- (IBAction)btnFbClicked:(id)sender;

- (IBAction)acceptTermsClicked:(id)sender;
- (IBAction)understandClicked:(id)sender;

- (IBAction)backClicked:(id)sender;

- (IBAction)forgotPasswordClicked:(id)sender;
- (IBAction)registerClicked:(id)sender;
- (IBAction)loginClicked:(id)sender;
- (IBAction)skipClicked:(id)sender;
@end

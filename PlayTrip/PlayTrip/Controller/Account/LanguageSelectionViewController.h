
#import <UIKit/UIKit.h>

@interface LanguageSelectionViewController : UIViewController {
    
    IBOutlet UIButton *btnLanguage1;
    IBOutlet UIButton *btnLanguage2;
    IBOutlet UIButton *btnLanguage3;
}

+(LanguageSelectionViewController *)initViewController;

- (IBAction)nextClicked:(id)sender;

- (IBAction)btnLanguage1Clicked:(id)sender;
- (IBAction)btnLanguage2Clicked:(id)sender;
- (IBAction)btnLanguage3Clicked:(id)sender;

@property (strong, nonatomic) IBOutlet UILabel *lblChooseLanguage;
@property (strong, nonatomic) IBOutlet UIButton *btnNext;
@property (strong, nonatomic) IBOutlet UILabel *lblPlayTrip;


@end

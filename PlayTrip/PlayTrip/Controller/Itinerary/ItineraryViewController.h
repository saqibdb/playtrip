
#import <UIKit/UIKit.h>
#import "HMSegmentedControl.h"
#import "VideoTour.h"
#import <MessageUI/MessageUI.h>
#import "MenuButtonViewController.h"
#import "MostPopularVideoTour.h"
#import "URLImageView.h"
#import "BookmarkVideotour.h"
#import <MediaPlayer/MediaPlayer.h>
#import "PlanModel.h"

@interface ItineraryViewController : MenuButtonViewController <UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate,MFMailComposeViewControllerDelegate> {
    
    NSMutableArray * reportsArray;
    NSString * selectedSpamId;
    
    IBOutlet UIButton *btnDays;
    IBOutlet UILabel *lblTourDate;
    IBOutlet UIButton *btnSelfDrive;
    IBOutlet UILabel *lblTourName;
    IBOutlet UIImageView *imgBookmarkIcon;
    
    IBOutlet UILabel *lblViewsCount;
    IBOutlet UILabel *lblMoneyCount;
    IBOutlet UILabel *lblShareCount;
    IBOutlet UILabel *lblBookmarkCount;
    IBOutlet UIButton *btnAddtoMyPlan;
    IBOutlet UIView *routeView;
    IBOutlet UIView *bookmarkView;
    IBOutlet UIView * hotelFooterView;
    
    NSDate *startDate;
    IBOutlet UILabel *lblBookmark;
    IBOutlet UILabel *lblRoutemap;
    IBOutlet UILabel *lblSelfDrive;
    IBOutlet UIButton *btnFlag;
    IBOutlet UIButton *btnShare;
    IBOutlet UIView *contentView;
    IBOutlet UITableView *tblView;
    IBOutlet UIScrollView *scrollview;
    
    IBOutlet UIView *airTickeFooterView;
    IBOutlet UIView *firstView;
    
    VideoTour *vTour;
    MostPopularVideoTour *mostPopularTour;
    BookmarkVideotour *bookmarkTour;
    
    PlanModel *plan;
    
    IBOutlet URLImageView *imgTour;
    IBOutlet URLImageView *imgUser;
    __weak IBOutlet UIView *selfDriveView;
    __weak IBOutlet UILabel *lblLanguage;
    IBOutlet UIView *popUpView;
    
    __weak IBOutlet UIImageView *languageImage;
    NSString* playedPath;
    IBOutlet UILabel *lblDate;
    IBOutlet UILabel *lblUserName;
    IBOutlet UIView *secondView;
    NSString *tourId;
    NSString *userId;
    NSString *bookmarkString;
    
    
    IBOutlet UIView *videoView;
    
    IBOutlet UIButton *backBtn;
    IBOutlet UIButton *firstViewPlayButton;
}

@property (retain, nonatomic) IBOutlet HMSegmentedControl *segment;
@property (strong, nonatomic) MPMoviePlayerController *videoController;

- (IBAction)playVideoClicked:(id)sender;
- (IBAction)bookmarkClicked:(id)sender;

- (IBAction)flagClicked:(id)sender;
- (IBAction)backClicked:(id)sender;
- (IBAction)shareClicked:(id)sender;
- (IBAction)addToMyPlanClicked:(id)sender;
- (IBAction)routeMapClicked:(id)sender;


+(ItineraryViewController *)initViewController:(VideoTour *)tour ;
+(ItineraryViewController *)initController:(MostPopularVideoTour *)mostPopularTour ;
+(ItineraryViewController *)initControllerBookmark:(BookmarkVideotour *)bookmarkTour ;
+(ItineraryViewController *)initViewControllerWithPlan:(PlanModel *)plan;

@end

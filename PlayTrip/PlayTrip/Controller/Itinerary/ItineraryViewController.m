
#import "ItineraryViewController.h"
#import "SVProgressHUD.h"
#import "ResetPasswordViewController.h"
#import "PlayTripManager.h"
#import "ColorConstants.h"
#import "Reason.h"
#import "AirTicketCell.h"
#import "HotelBookingCell.h"
#import "itineraryTableViewCell.h"
#import "ReportView.h"
#import "ReportCell.h"
#import "KGModalWrapper.h"
#import "ColorConstants.h"
#import "Attractions.h"
#import "AttractionData.h"
#import "Account.h"
#import "AccountManager.h"
#import "Constant.h"
#import "Categori.h"
#import "URLSchema.h"
#import "PlaceInformationViewController.h"
#import "CommonUser.h"
#import "Info.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIView+WebCache.h>
#import "AddAttractionToMyPlanView.h"
#import "RouteMapViewController.h"
#import <SDWebImage/UIButton+WebCache.h>
#import "Localisator.h"
#import <AVFoundation/AVFoundation.h>

#define ItinerayTableViewTag   @100
#define ReportTableViewTag   @200
static char myDataKey;

@interface ItineraryViewController (){
    NSMutableArray *attractionArray;
    NSMutableArray *attData;
    NSMutableArray *attDataArray;
    
    NSMutableArray *allAtractions;
    int tableHight;
}
@end

@implementation ItineraryViewController

+(ItineraryViewController *)initViewController:(VideoTour *)tour {
    ItineraryViewController * controller = [[ItineraryViewController alloc] initWithNibName:@"ItineraryViewController" bundle:nil];
    controller.title = @"Itinerary";
    controller->vTour = tour;
    return controller;
}
+(ItineraryViewController *)initViewControllerWithPlan:(PlanModel *)plan {
    ItineraryViewController * controller = [[ItineraryViewController alloc] initWithNibName:@"ItineraryViewController" bundle:nil];
    controller.title = @"Itinerary";
    controller->plan = plan;
    return controller;
}
+(ItineraryViewController *)initController:(MostPopularVideoTour *)mostPopularTour {
    ItineraryViewController * controller = [[ItineraryViewController alloc] initWithNibName:@"ItineraryViewController" bundle:nil];
    controller.title = @"Itinerary";
    controller->mostPopularTour = mostPopularTour;
    return controller;
}
+(ItineraryViewController *)initControllerBookmark:(BookmarkVideotour *)bookmarkTour {
    ItineraryViewController * controller = [[ItineraryViewController alloc] initWithNibName:@"ItineraryViewController" bundle:nil];
    controller.title = @"Itinerary";
    controller->bookmarkTour = bookmarkTour;
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = true;
   
    [self registerNib];
    self.navigationController.navigationBarHidden = true;
    
    tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    
    
    if (plan) {
        lblTourName.text = plan.name;
        lblUserName.text = plan.user.full_name;
        //lblDate.text = [Utils dateFormatyyyyMMdd:plan.user.create_date];
        lblDate.text =  ([plan.create_date length]>10 ? [plan.create_date substringToIndex:10] : plan.create_date);
        
        NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,plan.cover_photo];
        imgTour.url = [NSURL URLWithString:urlString];
        
        if(![plan.user.avatar isEqualToString:@""]){
            NSString * imgId = plan.user.avatar;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
            imgUser.url = [NSURL URLWithString:urlString];
            imgUser.layer.cornerRadius = imgUser.frame.size.width / 2;
            imgUser.layer.masksToBounds = YES;
        }else{
            imgUser.backgroundColor = [UIColor groupTableViewBackgroundColor];
            imgUser.layer.cornerRadius = imgUser.frame.size.width / 2;
            imgUser.layer.masksToBounds = YES;
            
        }
        
        //lblTourDate.text =[NSString stringWithFormat:@"%@(%@) to %@(%@)",[Utils dateFormatyyyyMMdd:plan.from_date],[[Utils getDayFromDate:plan.from_date] substringToIndex:3],[Utils dateFormatyyyyMMdd:vTour.to_date],[[Utils getDayFromDate:vTour.to_date] substringToIndex:3]];
        
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *capturedStartDate = [dateFormatter dateFromString:([plan.from_date length]>10 ? [plan.from_date substringToIndex:10] : plan.from_date)];
        NSDate *capturedEndDate = [dateFormatter dateFromString:([plan.to_date length]>10 ? [plan.to_date substringToIndex:10] : plan.to_date)];

        
        lblTourDate.text =[NSString stringWithFormat:@"%@(%@) to %@(%@)",[Utils dateFormatyyyyMMdd:capturedStartDate],[[Utils getDayFromDate:capturedStartDate] substringToIndex:3],[Utils dateFormatyyyyMMdd:capturedEndDate],[[Utils getDayFromDate:capturedEndDate] substringToIndex:3]];

        
        //lblTourDate.text = ([plan.create_date length]>10 ? [plan.create_date substringToIndex:10] : plan.create_date);

        
        NSString *daysStr = LOCALIZATION(@"days_without_bracket");
        
        [btnDays setTitle:[NSString stringWithFormat:@"%@ %@",plan.days, daysStr] forState:UIControlStateNormal];
        btnDays.layer.cornerRadius = 5.0;
        btnDays.layer.borderWidth = 1.0;
        btnDays.layer.borderColor = [ColorConstants appBrownColor].CGColor;
        //    btnAddtoMyPlan.layer.cornerRadius = 5.0;
        btnSelfDrive.layer.borderColor = [ColorConstants appBrownColor].CGColor;
        btnSelfDrive.layer.cornerRadius = 5.0;
        btnSelfDrive.layer.borderWidth = 1.0;
        imgUser.layer.cornerRadius = imgUser.frame.size.width / 2;
        bookmarkView.layer.cornerRadius =bookmarkView.frame.size.height / 2;
        routeView.layer.cornerRadius =routeView.frame.size.height / 2;
        //startDate = plan.from_date;
        [self showBookmark];
        
        lblBookmarkCount.text = [NSString stringWithFormat:@"%@",plan.total_bookmark];
        lblViewsCount.text = [NSString stringWithFormat:@"%@",plan.total_view];
        lblShareCount.text = [NSString stringWithFormat:@"%@",plan.total_share];
        lblMoneyCount.text = [NSString stringWithFormat:@"%@",plan.remuneration_amount];
        
        attractionArray = [NSMutableArray new];
        attDataArray = [NSMutableArray new];
        //attractionArray = [[vTour.attractionsSet allObjects]mutableCopy];
        allAtractions = [NSMutableArray new];
        
        for (AttractionModel *attr in plan.attractions) {
            attr.attr_data = [[attr.attr_data sortedArrayUsingComparator:^NSComparisonResult(AttractionDataModel *a, AttractionDataModel *b) {
                return [a.sequence intValue] > [b.sequence intValue];
            }] mutableCopy];
        }
        
        
        
        allAtractions = plan.attractions;
        
        
        AttractionModel *attr = [allAtractions firstObject];
        for (AttractionDataModel *attrModel in attr.attr_data) {
            NSLog(@"Sequence is %@" , attrModel.sequence);
        }
        
        
        
        if (![plan.self_drive intValue]) {
            selfDriveView.hidden = YES;
        }
        lblLanguage.text = plan.language;
        lblLanguage.hidden = YES;
        if([plan.language  isEqualToString: @"sc"]){
            languageImage.image = [UIImage imageNamed:@"speaker_.png"];
        }else if([plan.language  isEqualToString: @"tc"]){
            languageImage.image = [UIImage imageNamed:@"speaker_2.png"];
        }else{
            languageImage.image = [UIImage imageNamed:@"speaker_Eng.png"];
        }
        lblBookmark.text = @"Bookmark";
        imgBookmarkIcon.image = [UIImage imageNamed:@"bookmark_tag_s.png"];
        Account * account = [AccountManager Instance].activeAccount;

        if (account) {
            for (NSString *user in plan.bookmark_users) {
                if ([user isEqualToString:account.userId]) {
                    
                    lblBookmark.text = @"Bookmarked";
                    imgBookmarkIcon.image = [UIImage imageNamed:@"bookmark_tag_s_brown.png"];
                    break;
                }
            }
        }
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopVideoPlay) name:@"HideKGView" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveLanguageChangedNotification:)
                                                 name:kNotificationLanguageChanged
                                               object:nil];
    
    
    
    [self.view layoutIfNeeded];
    CGRect oldFrame = videoView.frame;
    oldFrame.size.height = oldFrame.size.width;
    videoView.frame = oldFrame;
    [self.view layoutIfNeeded];

 }

-(void)stopVideoPlay{
    [self.videoController stop];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = true;
//    [self setNavBarItems];
    reportsArray = [NSMutableArray new];
    reportsArray = [Reason getAll];
    
    self.leftButton.frame = CGRectMake(0.0, self.view.frame.size.height - 50, 50.0, 50.0);
    self.rightButton.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 50, self.view.frame.size.height - 50, 50.0, 50.0);
    CGRect mf = self.view.frame;
    self.menuView1.frame = CGRectMake(mf.size.width-250,mf.size.height - 110, self.menuView1.frame.size.width, self.menuView1.frame.size.height);
    
    [self setLocalizedStrings];
    backBtn.hidden = NO;

}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    
    
    
   
    
    
    contentView.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 777);
    //[self setUpSegmentView];

//    [_scrollview setContentSize:CGSizeMake(-100, 600)];
    [tblView reloadData];
    [self calculateHeightAndSetScroll];
    [tblView reloadData];
    self.leftButton.frame = CGRectMake(0.0, self.view.frame.size.height - 50, 50.0, 50.0);
    self.rightButton.frame = CGRectMake([UIScreen mainScreen].bounds.size.width - 50, self.view.frame.size.height - 50, 50.0, 50.0);
    CGRect mf = self.view.frame;
    self.menuView1.frame = CGRectMake(mf.size.width-250,mf.size.height - 110, self.menuView1.frame.size.width, self.menuView1.frame.size.height);
    //self.menuView1.frame = CGRectMake(0, self.view.frame.size.height - 110, self.menuView1.frame.size.width, self.menuView1.frame.size.height);
    
    
    
    CGRect firstViewFrame = firstView.frame;
    //firstViewFrame.size.height = firstViewFrame.size.width;
    firstView.frame = CGRectMake(firstViewFrame.origin.x, firstViewFrame.origin.y, firstViewFrame.size.width, firstViewFrame.size.width + 55);
    videoView.frame = CGRectMake(firstViewFrame.origin.x, firstViewFrame.origin.y, firstViewFrame.size.width, firstViewFrame.size.width);
    
    firstViewPlayButton.frame  = CGRectMake(firstViewFrame.origin.x, firstViewFrame.origin.y, firstViewFrame.size.width, firstViewFrame.size.width);
    
    
    imgTour.frame = CGRectMake(firstViewFrame.origin.x, firstViewFrame.origin.y, firstViewFrame.size.width, firstViewFrame.size.width);

    CGRect secondViewFrame = secondView.frame;
    //secondViewFrame.origin.y = firstViewFrame.size.height + 55;
    secondView.frame = CGRectMake(secondViewFrame.origin.x,  firstView.frame.size.height, secondViewFrame.size.width, secondViewFrame.size.width);

    tblView.frame = CGRectMake(tblView.frame.origin.x, secondView.frame.origin.y + 123 , tblView.frame.size.width, tblView.frame.size.height);
    
    
    NSLog(@"First View width = %f" , firstView.frame.size.width);
    NSLog(@"First View height = %f" , firstView.frame.size.height);
    NSLog(@"Second View y = %f" , secondView.frame.origin.y);
    NSLog(@"Video View height = %f" , videoView.frame.size.height);
    NSLog(@"Image View height = %f" , imgTour.frame.size.height);
    NSLog(@"Table View height = %f" , tblView.frame.size.height);
    NSLog(@"Table View y = %f" , tblView.frame.origin.y);
    //tblView.backgroundColor = [UIColor orangeColor];
    //[self calculateHeightAndSetScroll];

    [tblView reloadData];
    [self calculateHeightAndSetScroll];
    [tblView reloadData];
}

-(void)viewWillDisappear:(BOOL)animated {
    [self stopVideoPlay];
}

-(void)setLocalizedStrings {
    
    [lblBookmark setText:LOCALIZATION(@"Bookmark")];
    [lblRoutemap setText:LOCALIZATION(@"route_map")];
    [lblSelfDrive setText:LOCALIZATION(@"self_drive")];

}

- (void) receiveLanguageChangedNotification:(NSNotification *) notification
{
    if ([notification.name isEqualToString:@"languageChanged"])
    {
        [self setLocalizedStrings];
    }
}

- (void)registerNib{
    /*
     this function is used for register all NIB for tableView
     */
    UINib *cellAirTicker = [UINib nibWithNibName:@"AirTicketCell" bundle:nil];
    [tblView registerNib:cellAirTicker forCellReuseIdentifier:@"AirTicketCell"];
    
    UINib *cellHotelBooking = [UINib nibWithNibName:@"HotelBookingCell" bundle:nil];
    [tblView registerNib:cellHotelBooking forCellReuseIdentifier:@"HotelBookingCell"];
    
    UINib *celliterery = [UINib nibWithNibName:@"ItineraryTableViewCell" bundle:nil];
    [tblView registerNib:celliterery forCellReuseIdentifier:@"ItineraryTableViewCell"];
}

-(void)setNavBarItems {
    // Arun :: customizing the nav bar icons
    
    [self.navigationController.navigationBar setTitleTextAttributes: @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    UIBarButtonItem * backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back_white.png"] style:UIBarButtonItemStylePlain target:self action:@selector(backClicked)];
    self.navigationItem.leftBarButtonItem = backButton;
    
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    UIImage *buttonImage = [UIImage imageNamed:@"Flag.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(reportClicked) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    UIImage *buttonImage1 = [UIImage imageNamed:@"share_2.png"];
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button1 setImage:buttonImage1 forState:UIControlStateNormal];
    [button1 addTarget:self action:@selector(shareButton:) forControlEvents:UIControlEventTouchUpInside];
    button1.frame = CGRectMake(0, 0, buttonImage1.size.width, buttonImage1.size.height);
    
    UIBarButtonItem *customBarItem1 = [[UIBarButtonItem alloc] initWithCustomView:button1];
    self.navigationItem.rightBarButtonItems = @[customBarItem1, customBarItem];
}

-(void)backClicked {
    [self.navigationController popToRootViewControllerAnimated:NO];
}

- (IBAction)flagClicked:(id)sender {
    ReportView * view = [ReportView initWithNib];
    view.tblView.delegate = self;
    view.tblView.dataSource = self;
    [view.tblView reloadData];
    [view.btnSend addTarget:self action:@selector(sendReport) forControlEvents:UIControlEventTouchUpInside];
    [KGModalWrapper showWithContentView:view];
}

- (IBAction)backClicked:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)shareClicked:(id)sender {
    NSString *textToShare = @"Play Trip!";
    NSURL *myWebsite = [NSURL URLWithString:@"http://www.playtrip.com/"];
    NSURL *androidLink = [NSURL URLWithString:@"https://play.google.com/store/apps/details?id=com.appone.playtrip&hl=en"];
    NSURL *appLink = [NSURL URLWithString:@"https://itunes.apple.com/us/app/playtrip/id1223364669?ls=1&mt=8"];
    
    NSArray *objectsToShare = @[textToShare, myWebsite, androidLink, appLink];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (IBAction)addToMyPlanClicked:(id)sender {
    
    
    
    AddAttractionToMyPlanView *view = [AddAttractionToMyPlanView initWithNibWithPlanModel:plan];
    [KGModalWrapper showWithContentView:view];
    
    /*
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    [SVProgressHUD showWithStatus:@"Please wait.."];
    if(vTour){
        tourId = vTour.entity_id;
        userId = vTour.user.entity_id ;
    }else if(mostPopularTour){
        tourId = mostPopularTour.entity_id;
        userId = mostPopularTour.user1.entity_id ;
    }else{
        tourId = bookmarkTour.entity_id;
        userId = bookmarkTour.userBookMark.entity_id ;
    }
    if(userId) {
        [[PlayTripManager Instance]createVideoTourFromPlanWithUser:userId withPlanId:tourId withIsPublished:false withDraft:true WithLanguage:@"en" withBlock:^(id result, NSString *error) {
            if(!error){
                [SVProgressHUD dismiss];
                 [Utils showAlertWithMessage:@"Success"];
            }else{
                [SVProgressHUD dismiss];
                 [Utils showAlertWithMessage:@"Not Found"];
            }
        }];
    } else {
        [SVProgressHUD dismiss];
    }
     
     */
}

- (IBAction)routeMapClicked:(id)sender {
    RouteMapViewController * controller = [RouteMapViewController initViewControllerWithPlanModel:plan];
    [self.navigationController pushViewController:controller animated:YES];
}



-(void)showBookmark{
    Account * account = [AccountManager Instance].activeAccount;
    if(vTour){
        bookmarkString = vTour.bookmark_users_string;
    }else if(mostPopularTour){
        bookmarkString = mostPopularTour.bookmark_users_string;
    }else{
        bookmarkString = bookmarkTour.bookmark_users_string;
    }
    if(account){
        if (account.isSkip == true) {
            lblBookmark.text = @"Bookmark";
            imgBookmarkIcon.image = [UIImage imageNamed:@"bookmark_tag_s.png"];
            
        }else {
            if ([bookmarkString containsString:account.userId]) {
                lblBookmark.text = @"Bookmarked";
                imgBookmarkIcon.image = [UIImage imageNamed:@"bookmark_tag_s_brown.png"];
            } else {
                lblBookmark.text = @"Bookmark";
                imgBookmarkIcon.image = [UIImage imageNamed:@"bookmark_tag_s.png"];
            }
        }
    } else {
        lblBookmark.text = @"Bookmark";
        imgBookmarkIcon.image = [UIImage imageNamed:@"bookmark_tag_s.png"];
    }
}

- (IBAction)bookmarkClicked:(id)sender {
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    if ([lblBookmark.text isEqualToString:@"Bookmarked"]) {
        [SVProgressHUD showWithStatus:@"Removing Bookmark.."];
    } else {
        [SVProgressHUD showWithStatus:@"Bookmarking.."];
    }
    if(vTour){
        tourId = vTour.entity_id;
    }else if(mostPopularTour){
        tourId = mostPopularTour.entity_id;
    }
    else if(plan){
        tourId = plan._id;
    }
    else{
         tourId = bookmarkTour.entity_id;
    }
    
    [[PlayTripManager Instance] addCountForModel:ModelPlan withType:TypeBookmark forId:tourId WithBlock:^(id result, NSString *error) {
        if(!error){
            if ([lblBookmark.text isEqualToString:@"Bookmarked"]) {
                lblBookmark.text = @"Bookmark";
                imgBookmarkIcon.image = [UIImage imageNamed:@"bookmark_tag_s.png"];
            } else {
                lblBookmark.text = @"Bookmarked";
                imgBookmarkIcon.image = [UIImage imageNamed:@"bookmark_tag_s_brown.png"];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LatestVideoTourUpdated" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"BookmarkDetailsUpdated" object:nil];
                       
             [[NSNotificationCenter defaultCenter] postNotificationName:@"MostPopularVideoTourUpdated" object:nil];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"ProfileReloadTable" object:nil];
        }
        [SVProgressHUD dismiss];
    }];
}

- (void)calculateHeightAndSetScroll{
    int height = 0;
    if (_segment.selectedSegmentIndex == 0) {
        height = 120*5;
    }else if (_segment.selectedSegmentIndex == 1){
        height = 100*5;
    }else if (_segment.selectedSegmentIndex == 2){
        height = 138*5;
    }else{
        height = tblView.frame.size.height;
    }
    height = height+30;
    //tblView.frame = CGRectMake(0, tblView.frame.origin.y, [[UIScreen mainScreen] bounds].size.width, height+30);
    
    tblView.frame = CGRectMake(0, tblView.frame.origin.y, [[UIScreen mainScreen] bounds].size.width, tableHight );
    tblView.clipsToBounds = YES;
    contentView.frame = CGRectMake(contentView.frame.origin.x, contentView.frame.origin.x, [[UIScreen mainScreen] bounds].size.width, tableHight+tblView.frame.origin.y );
    [scrollview setContentSize:CGSizeMake(0, tableHight+tblView.frame.origin.y)];
    
    
    NSLog(@"Frame y is %f and height %f", tblView.frame.origin.y, tblView.frame.size.height);
    [self.view layoutIfNeeded];
}



- (void)setUpSegmentView{
    /*
     this function is used for setUp segment View.
     */
    UIImage *img1 = [UIImage imageNamed:@"icone_1_US"];
    UIImage *img2 = [UIImage imageNamed:@"icone_2_US"];
    UIImage *img3 = [UIImage imageNamed:@"icone_3_US"];
    UIImage *img4 = [UIImage imageNamed:@"icone_4_US"];
    UIImage *img5 = [UIImage imageNamed:@"icone_5_US"];
    UIImage *img6 = [UIImage imageNamed:@"icone_6_US"];
    
    NSMutableArray *arrImages = [[NSMutableArray alloc] init];
    
    [arrImages addObject:img1];
    [arrImages addObject:img2];
    [arrImages addObject:img3];
    [arrImages addObject:img4];
    [arrImages addObject:img5];
    [arrImages addObject:img6];
    
    
    _segment = [[HMSegmentedControl alloc] initWithSectionImages:arrImages sectionSelectedImages:arrImages];
    _segment.frame = CGRectMake(0,firstView.frame.size.height+secondView.frame.size.height+2, [[UIScreen mainScreen] bounds].size.width, 51);
    _segment.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    _segment.selectionStyle = HMSegmentedControlSelectionStyleTextWidthStripe;
    _segment.selectedSegmentIndex = 0;
    [_segment addTarget:self action:@selector(segmentedControlChangedValue) forControlEvents:UIControlEventValueChanged];
    [contentView addSubview:_segment];
}

-(void)segmentedControlChangedValue{
    /*
     this function is used for get selected index of segment view
     */
    if (_segment.selectedSegmentIndex > 2) {
        return;
    }
    [tblView reloadData];

    [self calculateHeightAndSetScroll];
}

-(void)reportClicked {
    ReportView * view = [ReportView initWithNib];
    view.tblView.delegate = self;
    view.tblView.dataSource = self;
    [view.tblView reloadData];
    [view.btnSend addTarget:self action:@selector(sendReport) forControlEvents:UIControlEventTouchUpInside];
    [KGModalWrapper showWithContentView:view];
}


- (IBAction)playVideoClicked:(id)sender {
    
    

    
    
    NSString * imgId = plan.merged_video;
    
    if (imgId) {
        NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetMergedVideo,imgId];
        
        [self playVideoOnFirstViewWithURL:[NSURL URLWithString:urlString]];
        
        
        //[self playVideoInPopUpViewWithURL:[NSURL URLWithString:urlString]];
        
        //[self playVideoWithURL:[NSURL URLWithString:urlString]];
    }
    else{
        [Utils showAlertWithMessage:@"No Video Found"];
    }
    
    
    /*
    [Utils showAlertWithMessage:@"Under Construction"];
    return;
    [SVProgressHUD showWithStatus:@"Buffering..."];

    if (vTour) {
        NSString * vidStr = vTour.merged_video;
        NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetVideo,vidStr];
        [self playVideoWithURL:[NSURL URLWithString:urlString]];
    }else if (mostPopularTour) {
        NSString * vidStr = mostPopularTour.merged_video;
        NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetVideo,vidStr];
        [self playVideoWithURL:[NSURL URLWithString:urlString]];
    }
     */
}


-(void)playVideoOnFirstViewWithURL:(NSURL *)url {
    NSError *error;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
    [[AVAudioSession sharedInstance] setActive:YES error:&error];
    if (error) {
        NSLog(@"Error is %@", error.localizedDescription);
    }

    if (url) {
        [SVProgressHUD dismiss];
        self.videoController = [[MPMoviePlayerController alloc] init];
        [self.videoController setContentURL:url];
        CGRect oldFrame = videoView.frame;
        
        NSLog(@"Old frame is %f , %f" , oldFrame.size.width ,oldFrame.size.height );
        
        [self.videoController.view setFrame:CGRectMake (0, 0, firstView.frame.size.width, firstView.frame.size.width)];
        self.videoController.controlStyle = MPMovieControlStyleFullscreen;

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoPlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:self.videoController];
        [videoView addSubview:self.videoController.view];
        [self.videoController play];
        firstViewPlayButton.hidden = YES;
        backBtn.hidden = YES;
    }

}





-(void)playVideoWithURL:(NSURL *)url {
    if (url) {
        [SVProgressHUD dismiss];
        self.videoController = [[MPMoviePlayerController alloc] init];
        [self.videoController setContentURL:url];
        [self.videoController.view setFrame:CGRectMake (0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.videoController.controlStyle = MPMovieControlStyleDefault;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoPlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:self.videoController];
        
        [self.view addSubview:self.videoController.view];
        [self.videoController play];
    }
}



- (void)videoPlayBackDidFinish:(NSNotification *)notification {
    backBtn.hidden = NO;
    firstViewPlayButton.hidden = NO;
    [self.videoController stop];
    [self.videoController.view removeFromSuperview];
    self.videoController = nil;
    [KGModalWrapper hideView];
}

-(void)sendReport {
    if (!selectedSpamId) {
        [self showAlertWithMessage:@"First select a reason"];
        return;
    }
    NSString * mainId = @"";
    if (vTour) {
        mainId = vTour.entity_id;
    } else if (mostPopularTour) {
        mainId = mostPopularTour.entity_id;
    }  else if (bookmarkTour) {
        mainId = bookmarkTour.entity_id;
    }
    
    [[PlayTripManager Instance] addSpam:selectedSpamId forPlan:mainId WithBlock:^(id result, NSString *error) {
        if (error) {
            [KGModalWrapper hideView];
            [self showAlertWithMessage:error];
        } else {
            [KGModalWrapper hideView];
            [self showAlertWithMessage:@"Reported successfully"];
        }
    }];
}

- (void)showAlertWithMessage:(NSString *)message{
    /*
     this method is used for showing message Dialog
     */
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:ALERT_TITLE
                                                                             message:message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionOk = [UIAlertAction actionWithTitle:@"Okay"
                                                       style:UIAlertActionStyleDefault
                                                     handler:nil];
    [alertController addAction:actionOk];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (IBAction)shareButton:(UIBarButtonItem *)sender {
    
    
    NSString *textToShare = @"Play Trip!";
    NSURL *myWebsite = [NSURL URLWithString:@"http://www.playtrip.com/"];
    NSURL *androidLink = [NSURL URLWithString:@"https://play.google.com/store/apps/details?id=com.appone.playtrip&hl=en"];
    NSURL *appLink = [NSURL URLWithString:@"https://itunes.apple.com/us/app/playtrip/id1223364669?ls=1&mt=8"];

    NSArray *objectsToShare = @[textToShare, myWebsite, androidLink, appLink];
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:objectsToShare applicationActivities:nil];
    
    NSArray *excludeActivities = @[UIActivityTypeAirDrop,
                                   UIActivityTypePrint,
                                   UIActivityTypeAssignToContact,
                                   UIActivityTypeSaveToCameraRoll,
                                   UIActivityTypeAddToReadingList,
                                   UIActivityTypePostToFlickr,
                                   UIActivityTypePostToVimeo];
    
    activityVC.excludedActivityTypes = excludeActivities;
    
    [self presentViewController:activityVC animated:YES completion:nil];
        
    [activityVC setCompletionWithItemsHandler:^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
        if(activityType){
            [self commanApi];
        }
    }];
    
    [activityVC completionWithItemsHandler];
}



-(void)commanApi{
    if(vTour){
        tourId = vTour.entity_id;
    }else if(mostPopularTour){
        tourId =mostPopularTour.entity_id;
    }else{
        tourId =bookmarkTour.entity_id;
    }
    [[PlayTripManager Instance] addCountForModel:@"Plan" withType:@"Share" forId:tourId WithBlock:^(id result, NSString *error) {
    }];
}

#pragma mark -- TableView Delegate and DataSource Method
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if ([ItinerayTableViewTag isEqualToNumber:[NSNumber numberWithInteger:tableView.tag]]) {
        if(_segment.selectedSegmentIndex == 0){
            return allAtractions.count;
        }else{
            return 1;
        }
    }else{
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    /*
     this function is used for set number of rows in tableview
     */
    if ([ItinerayTableViewTag isEqualToNumber:[NSNumber numberWithInteger:tableView.tag]]) {
        if(_segment.selectedSegmentIndex == 0){
            
            AttractionModel *attraction = allAtractions[section];
            
            tableHight = (int)(allAtractions.count * 30);
            
            for (AttractionModel *attraction in allAtractions) {
                tableHight = (int)(tableHight + (attraction.attr_data.count * 80));
            }
            
            NSLog(@"Height is %d", tableHight);
            
            CGRect frame = tblView.frame;
            frame.size.height = tableHight;
            
            tblView.frame = frame;
            [self.view layoutIfNeeded];
            
            return attraction.attr_data.count;
            
            //Attractions *a = [attractionArray objectAtIndex:section];
            //return a.attractionDataSet.count;
            
            
            
            
        }else{
            return 3;
        }
    } else {
        return reportsArray.count;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    /*
     this method is used to set cell for table view
     */
    
    if ([ItinerayTableViewTag isEqualToNumber:[NSNumber numberWithInteger:tableView.tag]]) {
        if (_segment.selectedSegmentIndex == 1) {
            AirTicketCell *cell = (AirTicketCell*)[tableView dequeueReusableCellWithIdentifier:@"AirTicketCell" forIndexPath:indexPath];
            return cell;
        }
        else if (_segment.selectedSegmentIndex == 2){
            HotelBookingCell *cell = (HotelBookingCell*)[tableView dequeueReusableCellWithIdentifier:@"HotelBookingCell" forIndexPath:indexPath];
            return cell;
        }else if (_segment.selectedSegmentIndex == 0){
            ItineraryTableViewCell *cell = (ItineraryTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ItineraryTableViewCell" forIndexPath:indexPath];
           [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            if(vTour) {
                  cell.lblTime.text = [Utils timeFormatHHmm:vTour.from_date];
            }else if(mostPopularTour){
                  cell.lblTime.text = [Utils timeFormatHHmm:mostPopularTour.from_date];
            }else{
                cell.lblTime.text = [Utils timeFormatHHmm:bookmarkTour.from_date];
            }
            cell.btnGreyDelete.hidden = true;
            
            AttractionModel *attractionModel = allAtractions[indexPath.section];
            AttractionDataModel *dataModel = attractionModel.attr_data[indexPath.row];
            cell.lblName.text = dataModel.info.name;
            cell.lblDescription.text = dataModel.desc;
            
            cell.lblBookmarkCount.text = [NSString stringWithFormat:@"%@",dataModel.total_bookmark];
            cell.lblViewsCount.text = [NSString stringWithFormat:@"%@",dataModel.total_view];
            cell.lblShareCount.text = [NSString stringWithFormat:@"%@",dataModel.total_share];
            //[cell.btnTime setTitle:dataModel.time forState:UIControlStateNormal];
            cell.lblTime.text = dataModel.time;
            
            
            
            
            
            if ([dataModel.time containsString:@"to"]) {
                NSArray *bothDates = [dataModel.time componentsSeparatedByString:@"to"];
                if (bothDates.count == 2) {
                    NSString *startDateAnother = [bothDates firstObject];
                    NSString *endDate = [bothDates lastObject];
                    
                    startDateAnother = [startDateAnother stringByReplacingOccurrencesOfString:@" " withString:@""];
                    endDate = [endDate stringByReplacingOccurrencesOfString:@" " withString:@""];
                    
                    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
                    [formatter setDateFormat:@"hh:mma"];
                    
                    
                    NSDate *startDateNew = [formatter dateFromString:startDateAnother];
                    NSDate *endDateNew = [formatter dateFromString:endDate];
                    
                    
                    if(!startDateNew){
                        NSDateFormatter *newFormatter = [[NSDateFormatter alloc]init];
                        [newFormatter setDateFormat:@"HH:mm"];
                        startDateNew = [newFormatter dateFromString:startDateAnother];
                        endDateNew = [newFormatter dateFromString:endDate];
                    }
                    
                    
                    
                    NSDateFormatter *newFormatter = [[NSDateFormatter alloc]init];
                    [newFormatter setDateFormat:@"HH:mm"];
                    
                    NSString *newStringTime = [NSString stringWithFormat:@"%@ to %@" ,[newFormatter stringFromDate:startDateNew] ,[newFormatter stringFromDate:endDateNew]];
                    
                    NSLog(@"%@" , newStringTime);
                    cell.lblTime.text = newStringTime;
                    
                    
                }
            }
            
            
            
            
            
            
            NSString * imgId = dataModel.video_id.thumbnail;
            NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];

            [cell layoutIfNeeded];

            cell.btnPlayVideo.frame = cell.imgFull.frame;
            [cell layoutIfNeeded];

            [cell.imgFull sd_setShowActivityIndicatorView:YES];
            [cell.imgFull sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            [cell.imgFull sd_setImageWithURL:[NSURL URLWithString:urlString]
                               placeholderImage:[UIImage imageNamed:@"placeholder.png"] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                                   if (!error) {
                                       cell.imgFull.image = image;
                                       [cell layoutIfNeeded];
                                   }
                               }];
            NSString *myData = dataModel.video_id.file_name;
            objc_setAssociatedObject (cell.btnPlayVideo, &myDataKey, myData,
                                      OBJC_ASSOCIATION_RETAIN);
            
            
            [cell.btnPlayVideo addTarget:self action:@selector(playAttractionVideo:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnPlayVideo.imageView.contentMode = UIViewContentModeScaleToFill;
            
            CategoryModel *cat = [dataModel.info.category firstObject];
            
            
            [cell.btnCategory sd_setBackgroundImageWithURL:[NSURL URLWithString:cat.image] forState:UIControlStateNormal placeholderImage:[UIImage imageNamed:@""] options:SDWebImageContinueInBackground completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                [cell.btnCategory setBackgroundImage:image forState:UIControlStateNormal];
                [cell layoutIfNeeded];
            }];
            
            
            [cell.bookMarkButton setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
            cell.bookMarkButton.selected = NO;
            Account * account = [AccountManager Instance].activeAccount;
            if (account) {
                for (NSString *user in dataModel.bookmark_users) {
                    if ([user isEqualToString:account.userId]) {
                        [cell.bookMarkButton setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                        cell.bookMarkButton.selected = YES;
                    }
                }
            }
            
            /*
            [cell.btnCategory.imageView sd_setShowActivityIndicatorView:YES];
            [cell.btnCategory.imageView sd_setIndicatorStyle:UIActivityIndicatorViewStyleWhite];
            [cell.btnCategory.imageView sd_setImageWithURL:[NSURL URLWithString:cat.image]
                            placeholderImage:[UIImage imageNamed:@""] completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
                                if (!error) {
                                    [cell.btnCategory setImage:image forState:UIControlStateNormal];
                                    [cell layoutIfNeeded];
                                }
                            }];
            */
            /*
            
            Attractions * attractions = [attractionArray objectAtIndex:indexPath.section];
            AttractionData *aData = [[attractions.attractionData allObjects] objectAtIndex:indexPath.row];
            cell.lblName.text = aData.info.name;
            cell.lblDescription.text = aData.info.address;
            
            
//            cell.btnPlayVideo.
            
            
            NSMutableArray *catArray = [[NSMutableArray alloc] init];
            catArray = [[aData.info.categori allObjects] mutableCopy];
            if(catArray.count >0){
                Categori *cat =[catArray objectAtIndex:0];
                NSString * imgId= cat.image;
               NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
                cell.imgCategory.url = [NSURL URLWithString:urlString];
                cell.imgCategory.layer.cornerRadius = cell.imgCategory.frame.size.width / 2;
                cell.imgCategory.layer.masksToBounds = YES;
            }else{
                //  cell.imgCategory.backgroundColor = [UIColor grayColor];
                    cell.imgCategory.layer.cornerRadius = cell.imgCategory.frame.size.width / 2;
                    cell.imgCategory.layer.masksToBounds = YES;
            }
             */
            [self calculateHeightAndSetScroll];

            return cell;
        }
        return nil;
    } else {
        ReportCell * cell = (ReportCell *)[tableView dequeueReusableCellWithIdentifier:@"ReportCell"];
        if (cell == nil) {
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"ReportCell" owner:self options:nil];
            for (id currentObject in topLevelObjects) {
                if ([currentObject isKindOfClass:[UITableViewCell class]]) {
                    cell =  (ReportCell *) currentObject;
                    break;
                }
            }
        }
        Reason * reas = [reportsArray objectAtIndex:indexPath.row];
        cell.lblTitle.text = reas.title;
        cell.lblDescription.text = reas.desc;
        cell.lblDescription.textColor = [UIColor blackColor];
        cell.lblTitle.textColor = [UIColor blackColor];
        cell.contentView.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

-(void)playAttractionVideo :(UIButton *)btn {
    NSString *myData =
    (NSString *)objc_getAssociatedObject(btn, &myDataKey);
    NSString * imgId = myData;
    NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,@"/uploads/",imgId];
    NSLog(@"%@",urlString);
    [self playVideoInPopUpViewWithURL:[NSURL URLWithString:urlString]];
    //http://54.254.206.27:9000/uploads/
}
-(void)playVideoInPopUpViewWithURL:(NSURL *)url {
    if (url) {
        self.videoController = [[MPMoviePlayerController alloc] init];
        [self.videoController setContentURL:url];
        [self.videoController.view setFrame:CGRectMake (0, 0, popUpView.frame.size.width, popUpView.frame.size.height)];
        self.videoController.controlStyle = MPMovieControlStyleNone;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoPlayBackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:self.videoController];
        [popUpView addSubview:self.videoController.view];
        [KGModalWrapper showWithContentView:popUpView];
        [self.videoController setControlStyle:MPMovieControlStyleNone];
        [self.videoController play];
    }
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    [tableView reloadData];
    if ([ItinerayTableViewTag isEqualToNumber:[NSNumber numberWithInteger:tableView.tag]]) {
       
        AttractionModel *attractionModel = allAtractions[indexPath.section];
        AttractionDataModel *dataModel = attractionModel.attr_data[indexPath.row];

        PlaceInformationViewController *controller = [PlaceInformationViewController initViewControllerWithAttrModel:attractionModel andAttractionDataModel:dataModel];
                [self.navigationController pushViewController:controller animated:YES];
        
        
//        Attractions * attractions = [attractionArray objectAtIndex:indexPath.section];
//        AttractionData *aData = [[attractions.attractionData allObjects] objectAtIndex:indexPath.row];
//        PlaceInformationViewController *controller = [PlaceInformationViewController initViewControllerWithAttrData:aData];
//        [self.navigationController pushViewController:controller animated:NO];
    }else {
        ReportCell * cell = (ReportCell *)[tableView cellForRowAtIndexPath:indexPath];
//        cell.backBrownView.hidden = false;
        cell.contentView.layer.backgroundColor = [ColorConstants appBrownColor].CGColor;
        cell.lblTitle.textColor = [UIColor whiteColor];
        cell.lblDescription.textColor = [UIColor whiteColor];
        
        Reason * reas = [reportsArray objectAtIndex:indexPath.row];
        selectedSpamId = reas.entity_id;
    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (![ItinerayTableViewTag isEqualToNumber:[NSNumber numberWithInteger:tableView.tag]]) {
        ReportCell * cell = (ReportCell *)[tableView cellForRowAtIndexPath:indexPath];
        //        cell.backBrownView.hidden = false;
        cell.contentView.layer.backgroundColor = [UIColor clearColor].CGColor;
        cell.lblTitle.textColor = [UIColor blackColor];
        cell.lblDescription.textColor = [UIColor blackColor];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    /*
     this function is used to set height of row according to section
     */
    if ([ItinerayTableViewTag isEqualToNumber:[NSNumber numberWithInteger:tableView.tag]]) {
        if (_segment.selectedSegmentIndex == 1) {
            return 100;
        }
        else if (_segment.selectedSegmentIndex == 2){
            return 111;
        }else if (_segment.selectedSegmentIndex == 0){
            return 80.0;
        }
    } else {
        return 65;
    }
    return 0;
}


-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    if ([ItinerayTableViewTag isEqualToNumber:[NSNumber numberWithInteger:tableView.tag]]) {
        {
            if (_segment.selectedSegmentIndex == 0){
                
                return airTickeFooterView;
            }else if (_segment.selectedSegmentIndex == 2){

                return hotelFooterView;
            }
        }
    }
return [[UIView alloc]initWithFrame:CGRectZero];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    /*
     this will set height of header
     */
    if ([ItinerayTableViewTag isEqualToNumber:[NSNumber numberWithInteger:tableView.tag]]) {
        {
            if (_segment.selectedSegmentIndex ==2) {
                return 30;
            }
        }
    }
    return 0;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    /*
     this method is used for set header in first segment
     */
    if ([ItinerayTableViewTag isEqualToNumber:[NSNumber numberWithInteger:tableView.tag]]) {
        {
            if (_segment.selectedSegmentIndex == 0) {
                UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 30)];
                UIImageView *imgWall = [[UIImageView alloc] initWithFrame:CGRectMake(12,0,30,30)];
                imgWall.image = [UIImage imageNamed:@"circle"];
                [view addSubview:imgWall];
                UILabel *lblText = [[UILabel alloc] initWithFrame:CGRectMake(46, 5, [[UIScreen mainScreen] bounds].size.width-100, 20)];
                lblText.textColor = [ColorConstants appBrownColor];
                lblText.font = [UIFont systemFontOfSize:12];
                
                NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
                dayComponent.day = section;
                
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *capturedStartDate = [dateFormatter dateFromString:([plan.from_date length]>10 ? [plan.from_date substringToIndex:10] : plan.from_date)];
                
                NSCalendar *theCalendar = [NSCalendar currentCalendar];
                NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:capturedStartDate options:0];
                NSString *next = [Utils dateFormatyyyyMMdd:nextDate];
                NSString * day =[[Utils getDayFromDate:nextDate] substringToIndex:3];
                
                
                int cSec = (int)section+1;
                lblText.text =[NSString stringWithFormat:@"Day %d - %@(%@)",cSec,next,day];// @"Day 1 2016-9-14 (WED)";
                [view addSubview:lblText];
                
                
                [view setBackgroundColor:[UIColor whiteColor]];
                return view;
            }
        }
    }
    return [[UIView alloc]initWithFrame:CGRectZero];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    /*
     this will set height of header
     */
    if ([ItinerayTableViewTag isEqualToNumber:[NSNumber numberWithInteger:tableView.tag]]) {
        {
            if (_segment.selectedSegmentIndex == 0) {
                return 30;
            }
        }
    }
    return 0;
}

#pragma mark -- ScrollView Delegate Method

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    /*
     this method is used for scrollview scrolling
     */
    [self calculateHeightAndSetScroll];
}

- (CGSize)intrinsicContentSize {
    return CGSizeMake(UIViewNoIntrinsicMetric, tblView.frame.size.height);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (BOOL)prefersStatusBarHidden {
    return YES;
}
@end

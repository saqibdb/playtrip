
#import "RelatedVideoTourViewController.h"
#import "LatestVideoTourCollectionViewCell.h"
#import "ColorConstants.h"
#import "LatestVideoTourCollectionViewCell.h"
#import "VideoSortTableViewCell.h"

@interface RelatedVideoTourViewController ()

@end

@implementation RelatedVideoTourViewController

+(RelatedVideoTourViewController *)initViewController {
    RelatedVideoTourViewController * controller = [[RelatedVideoTourViewController alloc] initWithNibName:@"RelatedVideoTourViewController" bundle:nil];
    controller.title = @"Related Video Tour";
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = false;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self registerNibs];
    [self setNavBarItems];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    sortView = [PopularVideoSort initWithNib];
    sortView.tblView.delegate = self;
    sortView.tblView.dataSource = self;
}

-(void)registerNibs {
    [collViewMain registerClass:[LatestVideoTourCollectionViewCell class] forCellWithReuseIdentifier:@"LatestVideoTourCollectionViewCell"];    
    [collViewMain registerNib:[UINib nibWithNibName:@"LatestVideoTourCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"LatestVideoTourCollectionViewCell"];
}

-(void) setNavBarItems {
    
    [self.navigationController.navigationBar setTitleTextAttributes: @{NSForegroundColorAttributeName:[ColorConstants appBrownColor]}];
    
    UIImage *buttonImage = [UIImage imageNamed:@"back.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(backClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
    
    UIImage *mapImage = [UIImage imageNamed:@"sort_descending.png"];
    UIButton *btnRight1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight1 setImage:mapImage forState:UIControlStateNormal];
    btnRight1.frame = CGRectMake(0, 0, mapImage.size.width, mapImage.size.height);
    [btnRight1 addTarget:self action:@selector(sortClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarItem1 = [[UIBarButtonItem alloc] initWithCustomView:btnRight1];
    self.navigationItem.rightBarButtonItem = rightBarItem1;
    
    self.navigationController.navigationBar.barTintColor = [ColorConstants appYellowColor];
    self.navigationController.navigationBar.translucent = NO;
}

-(void)backClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)sortClicked {
    if (isSortShown) {
        isSortShown = NO;
        [subMenuView removeFromSuperview];
    } else {
        isSortShown = YES;
        CGFloat height;
        height = (8 * 40);// Arun :: number of rows * height of row
        subMenuView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - sortView.frame.size.width, 0, sortView.frame.size.width, height)];
        
        subMenuView.layer.borderWidth = 0.5;
        subMenuView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        [subMenuView addSubview:sortView];
        subMenuView.clipsToBounds = YES;
        [self.view addSubview:subMenuView];
    }
    [sortView.tblView reloadData];
}


#pragma mark -<UICollectionViewDelegate,UICollectionViewDataSource>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return 10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    LatestVideoTourCollectionViewCell *cell = (LatestVideoTourCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"LatestVideoTourCollectionViewCell" forIndexPath:indexPath];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
//    ItineraryViewController *controller = [ItineraryViewController initViewController:[videoTourArray objectAtIndex:indexPath.row]];
//    [self.navigationController pushViewController:controller animated:YES];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((collectionView.frame.size.width/2)-10, (collectionView.frame.size.height/2)-10);
}



#pragma Sort View - Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 8;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"VideoSortTableViewCell" forIndexPath:indexPath];
    cell.btnFirst.layer.cornerRadius = 5.0;
    cell.btnSecond.layer.cornerRadius = 5.0;
    
    if (indexPath.row == 0) {
        cell.lblText.text = @"Title";
    }else if (indexPath.row == 1){
        cell.lblText.text = @"Time";
    }else if (indexPath.row == 2){
        cell.lblText.text = @"Bookmark";
    }else if (indexPath.row == 3){
        cell.lblText.text = @"View";
    }else if (indexPath.row == 4){
        cell.lblText.text = @"Share";
    }else if (indexPath.row == 5){
        cell.lblText.text = @"Rewards";
    }else if (indexPath.row == 6){
        cell.lblText.text = @"Attractions";
    }else if (indexPath.row == 7){
        cell.lblText.text = @"Add to My Plan";
    }
    
    if (indexPath.row == 0) {
        [cell.btnFirst setImage:[UIImage imageNamed:@"a_z_us"] forState: UIControlStateNormal];
    }else {
        [cell.btnFirst setImage:[UIImage imageNamed:@"up_us"] forState: UIControlStateNormal];
    }
    cell.btnFirst.tag = indexPath.row;
    cell.btnSecond.tag = indexPath.row;
    
    [cell.btnFirst addTarget:self action:@selector(btnFirstClickedSort:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnSecond addTarget:self action:@selector(btnSecondClickedSort:) forControlEvents:UIControlEventTouchUpInside];
    
    if (indexPath.row == 0) {
        [cell.btnSecond setImage:[UIImage imageNamed:@"z_a_us"] forState:UIControlStateNormal];
    }else {
        [cell.btnSecond setImage:[UIImage imageNamed:@"down_us"] forState:UIControlStateNormal];
    }
    
    if (lastSortedIndex) {
        if ((int)indexPath.row == lastSortedIndex-1) {
            if (lastSortedUp) {
                if (indexPath.row == 0) {
                    [cell.btnFirst setImage:[UIImage imageNamed:@"a_z_s"] forState: UIControlStateNormal];
                }else {
                    [cell.btnFirst setImage:[UIImage imageNamed:@"up_s"] forState: UIControlStateNormal];
                }
            } else {
                if (indexPath.row == 0) {
                    [cell.btnSecond setImage:[UIImage imageNamed:@"z_a_s"] forState:UIControlStateNormal];
                }else {
                    [cell.btnSecond setImage:[UIImage imageNamed:@"down_s"] forState:UIControlStateNormal];
                }
            }
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)btnFirstClickedSort:(id)sender {
    NSIndexPath * ip = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    
    lastSortedIndex = (int)[sender tag]+1;
    lastSortedUp = true;
    
    for (int i = 0; i<5; i++) {
        NSIndexPath * ip1 = [NSIndexPath indexPathForRow:i inSection:0];
        VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[sortView.tblView cellForRowAtIndexPath:ip1];
        if (i == 0) {
            [cell.btnFirst setImage:[UIImage imageNamed:@"a_z_us"] forState:UIControlStateNormal];
            [cell.btnSecond setImage:[UIImage imageNamed:@"z_a_us"] forState:UIControlStateNormal];
        }else {
            [cell.btnFirst setImage:[UIImage imageNamed:@"up_us"] forState:UIControlStateNormal];
            [cell.btnSecond setImage:[UIImage imageNamed:@"down_us"] forState:UIControlStateNormal];
        }
        
    }
    VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[sortView.tblView cellForRowAtIndexPath:ip];
    if ([sender tag] == 0) {
        [cell.btnFirst setImage:[UIImage imageNamed:@"a_z_s"] forState:UIControlStateNormal];
    }else {
        [cell.btnFirst setImage:[UIImage imageNamed:@"up_s"] forState:UIControlStateNormal];
    }
    
    
//    if ([sender tag] == 0) {
//        userArray = [Users getAllByTitleAsc:true];
//    } else if ([sender tag] == 1) {
//        userArray = [Users getAllByBoookmarkAsc:true];
//    } else if ([sender tag] == 2) {
//        userArray = [Users getAllByRewardsAsc:true];
//    } else if ([sender tag] == 3) {
//        userArray = [Users getAllByVideoTourAsc:true];
//    }
    
    [collViewMain reloadData];
    isSortShown = NO;
    [subMenuView removeFromSuperview];
}

-(void)btnSecondClickedSort:(id)sender {
    NSIndexPath * ip = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    
    lastSortedIndex = (int)[sender tag]+1;
    lastSortedUp = false;
    for (int i = 0; i<5; i++) {
        NSIndexPath * ip1 = [NSIndexPath indexPathForRow:i inSection:0];
        VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[sortView.tblView cellForRowAtIndexPath:ip1];
        if (i == 0) {
            [cell.btnFirst setImage:[UIImage imageNamed:@"a_z_us"] forState:UIControlStateNormal];
            [cell.btnSecond setImage:[UIImage imageNamed:@"z_a_us"] forState:UIControlStateNormal];
        }else {
            [cell.btnFirst setImage:[UIImage imageNamed:@"up_us"] forState:UIControlStateNormal];
            [cell.btnSecond setImage:[UIImage imageNamed:@"down_us"] forState:UIControlStateNormal];
        }
        
    }
    VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[sortView.tblView cellForRowAtIndexPath:ip];
    if ([sender tag] == 0) {
        [cell.btnSecond setImage:[UIImage imageNamed:@"z_a_s"] forState:UIControlStateNormal];
    }else {
        [cell.btnSecond setImage:[UIImage imageNamed:@"down_s"] forState:UIControlStateNormal];
    }
    
    
//    if ([sender tag] == 0) {
//        userArray = [Users getAllByTitleAsc:false];
//    } else if ([sender tag] == 1) {
//        userArray = [Users getAllByBoookmarkAsc:false];
//    } else if ([sender tag] == 2) {
//        userArray = [Users getAllByRewardsAsc:false];
//    } else if ([sender tag] == 3) {
//        userArray = [Users getAllByVideoTourAsc:false];
//    }
    [collViewMain reloadData];
    isSortShown = NO;
    [subMenuView removeFromSuperview];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end


#import <UIKit/UIKit.h>
#import "PopularVideoSort.h"

@interface RelatedVideoTourViewController : UIViewController <UITableViewDataSource, UITableViewDelegate> {
    
    IBOutlet UICollectionView *collViewMain;
    BOOL isSortShown;
    UIView * subMenuView;
    PopularVideoSort * sortView;
    int lastSortedIndex;
    BOOL lastSortedUp;
}

+(RelatedVideoTourViewController *)initViewController;

@end

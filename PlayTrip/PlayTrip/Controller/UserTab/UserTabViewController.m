
#import "UserTabViewController.h"
#import "UserDetailViewController.h"
#import "VideoSortTableViewCell.h"
#import "UserTabCell.h"
#import "SVProgressHUD.h"
#import "Users.h"
#import "Account.h"
#import "AccountManager.h"
#import "Constants.h"
#import "URLSchema.h"
#import "PlayTripManager.h"
#import "ColorConstants.h"
#import "AppDelegate.h"
#import "Localisator.h"

@interface UserTabViewController (){
    NSMutableArray *userArray;
    
}
@end

@implementation UserTabViewController

+(UserTabViewController *)initViewController {
    UserTabViewController * controller = [[UserTabViewController alloc] initWithNibName:@"UserTabViewController" bundle:nil];
    controller.title = LOCALIZATION(@"recommended_users");
    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    userArray = [[NSMutableArray alloc]init];
    [collViewMain registerClass:[UserTabCell class] forCellWithReuseIdentifier:@"UserTabCell"];
    
    UINib *cellNib = [UINib nibWithNibName:@"UserTabCell" bundle:nil];
    [collViewMain registerNib:cellNib forCellWithReuseIdentifier:@"UserTabCell"];
    
    self.allRecomendedUsersOrg = [[NSMutableArray alloc] initWithArray:self.allRecomendedUsers];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setNavBarItems];
    if (![[AppDelegate appDelegate] isReachable]) {
        [Utils showAlertWithMessage:kCheckInternet];
        return;
    }
    //[self getRecUsers];
    bookmarkUpdated = false;
    _searchBar.text = @"";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getRecUsers) name:@"RecommendedUsersUpdated1" object:nil];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    sortView = [PopularVideoSort initWithNib];
    sortView.tblView.delegate = self;
    sortView.tblView.dataSource = self;
    
    UIButton *btn = [[UIButton alloc] init];
    btn.tag = 1;
    [self btnFirstClickedSort:btn];
}

-(void)setNavBarItems {
    // Arun :: customizing the nav bar icons
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[ColorConstants appBrownColor]}];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[ColorConstants appBrownColor]}];
    
    UIImage *buttonImage = [UIImage imageNamed:@"back"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(backClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
    
    
    UIImage *searchImage = [UIImage imageNamed:@"search_brown.png"];
    UIButton *btnRight1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight1 setImage:searchImage forState:UIControlStateNormal];
    btnRight1.frame = CGRectMake(0, 0, searchImage.size.width, searchImage.size.height);
    [btnRight1 addTarget:self action:@selector(searchClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarItem1 = [[UIBarButtonItem alloc] initWithCustomView:btnRight1];
    
    UIImage *filterImage = [UIImage imageNamed:@"sort_descending.png"];
    UIButton *btnRight2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnRight2 setImage:filterImage forState:UIControlStateNormal];
    btnRight2.frame = CGRectMake(0, 0, filterImage.size.width, filterImage.size.height);
    [btnRight2 addTarget:self action:@selector(sortClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightBarItem2 = [[UIBarButtonItem alloc] initWithCustomView:btnRight2];
    self.navigationItem.rightBarButtonItems = @[rightBarItem2, rightBarItem1];
}

-(void)sortClicked {
    if (isSortShown) {
        isSortShown = NO;
        [subMenuView removeFromSuperview];
    } else {
        isSortShown = YES;
        CGFloat height;
        height = (4 * 40);// Arun :: number of rows * height of row
        subMenuView = [[UIView alloc] initWithFrame:CGRectMake(self.view.frame.size.width - sortView.frame.size.width, 0, sortView.frame.size.width, height)];
        
        subMenuView.layer.borderWidth = 0.5;
        subMenuView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        [subMenuView addSubview:sortView];
        subMenuView.clipsToBounds = YES;
        [self.view addSubview:subMenuView];
    }
    [sortView.tblView reloadData];
}

-(void)searchClicked {
    _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0,0,self.view.frame.size.width,45)];
    _searchBar.delegate = self;
    //    searchBar.showsCancelButton = YES;
    _searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    _searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
    //    [self set_searchBar:searchBar];
    self.navigationItem.titleView = _searchBar;
}

-(void)backClicked {
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark -<UICollectionViewDelegate,UICollectionViewDataSource>
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.allRecomendedUsers.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UserTabCell *cell = (UserTabCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"UserTabCell" forIndexPath:indexPath];
    Account * account = [AccountManager Instance].activeAccount;
    
    UserModel *userModel = self.allRecomendedUsers[indexPath.row];
    cell.imgUser.layer.cornerRadius = cell.imgUser.frame.size.width / 2;
    NSString * imgId = userModel.avatar;
    NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
    cell.imgUser.url = [NSURL URLWithString:urlString];
    cell.lblName.text = userModel.full_name;
    cell.btnBookmark.tag = indexPath.row;
    
    if (userModel.credits) {
        cell.lblMoney.text =[NSString stringWithFormat:@"%@",userModel.credits];;
    }
    else{
        cell.lblMoney.text =[NSString stringWithFormat:@"%@",@"0"];;
    }
    if (userModel.total_bookmark) {
        cell.lblBookMark.text = [NSString stringWithFormat:@"%@",userModel.total_bookmark];
    }
    else{
        cell.lblBookMark.text = [NSString stringWithFormat:@"%@",@"0"];
    }
    if (userModel.total_view) {
        cell.lblViews.text = [NSString stringWithFormat:@"%@",userModel.total_view];
    }
    else{
        cell.lblViews.text = [NSString stringWithFormat:@"%@",@"0"];
    }
    if (userModel.total_share) {
        cell.lblShares.text = [NSString stringWithFormat:@"%@",userModel.total_share];
    }
    else{
        cell.lblShares.text = [NSString stringWithFormat:@"%@",@"0"];
    }
    
    cell.btnBookmark.tag = indexPath.row;
    
    [cell.btnBookmark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
    cell.btnBookmark.selected = NO;
    if (account) {
        for (NSString *userBookmarked in userModel.bookmark_users) {
            if ([userBookmarked isEqualToString:account.userId]) {
                [cell.btnBookmark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
                cell.btnBookmark.selected = YES;
            }
        }
    }
    [cell.btnBookmark addTarget:self action:@selector(bookmarkClickedUser:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    
    /*
    
    Users * user = [userArray objectAtIndex:indexPath.row];
    cell.imgUser.layer.cornerRadius = cell.imgUser.frame.size.width / 2;
    NSString * imgId = user.avatar;
    NSString * urlString = [NSString stringWithFormat:@"%@%@%@",baseURL,kGetImage,imgId];
    cell.imgUser.url = [NSURL URLWithString:urlString];
    cell.lblName.text = user.full_name;
    cell.btnBookmark.tag = indexPath.row;
    
    cell.lblPlay.text = [NSString stringWithFormat:@"%@",user.total_share];;
    cell.lblMoney.text =@"100";
    cell.lblBookMark.text = [NSString stringWithFormat:@"%@",user.total_bookmark];
    
    
    if (account) {
        if ([user.bookmark_users_string containsString:account.userId]) {
            [cell.btnBookmark setImage:[UIImage imageNamed:@"bookmark_tag_s.png"] forState:UIControlStateNormal];
            cell.btnBookmark.selected = YES;
        } else {
            [cell.btnBookmark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
            cell.btnBookmark.selected = NO;
        }
    } else {
        [cell.btnBookmark setImage:[UIImage imageNamed:@"bookmark_tag_us.png"] forState:UIControlStateNormal];
        cell.btnBookmark.selected = NO;
    }        [cell.btnBookmark addTarget:self action:@selector(bookmarkClickedUser:) forControlEvents:UIControlEventTouchUpInside];
    */
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //Users *user = [userArray objectAtIndex:indexPath.row];
    
    UserModel *userModel = _allRecomendedUsers[indexPath.row];
    UserDetailViewController * controller = [UserDetailViewController initViewControllerNew:userModel];
    [self.navigationController pushViewController:controller animated:YES];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake((collectionView.frame.size.width/2)-02, (collectionView.frame.size.width/2)-02);
}

-(void)bookmarkClickedUser:(id)sender {
    Users * v = [userArray objectAtIndex:[sender tag]];
    [self commanApiWithModel:ModelUser WithId:v.entity_id WithType:TypeBookmark];
}

-(void)commanApiWithModel:(NSString *)modelValue WithId:(NSString *)idValue WithType:(NSString *)typeValue {
    [SVProgressHUD showWithStatus:@"Please wait.."];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    
    [[PlayTripManager Instance] addCountForModel:modelValue withType:typeValue forId:idValue WithBlock:^(id result, NSString *error) {
        if(!error){
            [self getRecUsers];
            [Utils setBOOL:YES Key:@"BookmarkUserUpd"];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"RecommendedUsersUpdated1" object:nil];
//            [collViewMain reloadData];
            [SVProgressHUD dismiss];
        }
    }];
}

-(void)getRecUsers {
    [SVProgressHUD showWithStatus:@"Please wait.."];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];

    [[PlayTripManager Instance]getRecommendedUsersListWithBlock:^(id result, NSString *error) {
        if (!error) {
            userArray = [Users getAll];
            [collViewMain reloadData];
        }
        [SVProgressHUD dismiss];
    }];
}

#pragma Searchbar Delegate
- (void)searchBarTextDidBeginEditing:(UISearchBar *)sBar {
    [sBar setShowsCancelButton:YES animated:YES];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)sBar{
    [sBar resignFirstResponder];
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)sBar{
    [sBar resignFirstResponder];
    [sBar setShowsCancelButton:NO animated:YES];
    sBar.text = @"";
    self.allRecomendedUsers = [[NSMutableArray alloc] initWithArray:self.allRecomendedUsersOrg];
    [collViewMain reloadData];
}

- (void)searchBar:(UISearchBar *)sBar textDidChange:(NSString *)text {
    NSLog(@"Search Text is %@" , text);
    if (!text.length) {
        self.allRecomendedUsers = [[NSMutableArray alloc] initWithArray:self.allRecomendedUsersOrg];
    }
    else{
        [self.allRecomendedUsers removeAllObjects];
        for (UserModel *user in self.allRecomendedUsersOrg) {
            if ([[user.first_name lowercaseString] containsString:[text lowercaseString]] ||[[user.last_name lowercaseString] containsString:[text lowercaseString]]) {
                [self.allRecomendedUsers addObject:user];
            }
        }
    }
    
    [collViewMain reloadData];

    /*
    userArray =[NSMutableArray new];
    NSMutableArray * aarVideoTour = [Users getAll];
    
    if([text length] != 0) {
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"user_name BEGINSWITH[c] %@", text];
        userArray = [[aarVideoTour filteredArrayUsingPredicate:predicate1]mutableCopy];
    } else {
        [sBar resignFirstResponder];
        userArray = [Users getAll];
    }
    
    [collViewMain reloadData];
    */
    
}

#pragma Sort View - Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"VideoSortTableViewCell" forIndexPath:indexPath];
    cell.btnFirst.layer.cornerRadius = 5.0;
    cell.btnSecond.layer.cornerRadius = 5.0;
    
    if (indexPath.row == 0) {
        cell.lblText.text = @"Title";
    }else if (indexPath.row == 1){
        cell.lblText.text = @"Bookmark";
    }else if (indexPath.row == 2){
        cell.lblText.text = @"Rewards";
    }else if (indexPath.row == 3){
        cell.lblText.text = @"Video Tour";
    }
    
    if (indexPath.row == 0) {
        [cell.btnFirst setImage:[UIImage imageNamed:@"a_z_us"] forState: UIControlStateNormal];
    }else {
        [cell.btnFirst setImage:[UIImage imageNamed:@"up_us"] forState: UIControlStateNormal];
    }
    cell.btnFirst.tag = indexPath.row;
    cell.btnSecond.tag = indexPath.row;
    
    [cell.btnFirst addTarget:self action:@selector(btnFirstClickedSort:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnSecond addTarget:self action:@selector(btnSecondClickedSort:) forControlEvents:UIControlEventTouchUpInside];
    
    if (indexPath.row == 0) {
        [cell.btnSecond setImage:[UIImage imageNamed:@"z_a_us"] forState:UIControlStateNormal];
    }else {
        [cell.btnSecond setImage:[UIImage imageNamed:@"down_us"] forState:UIControlStateNormal];
    }
    
    if (lastSortedIndex) {
        if ((int)indexPath.row == lastSortedIndex-1) {
            if (lastSortedUp) {
                if (indexPath.row == 0) {
                    [cell.btnFirst setImage:[UIImage imageNamed:@"a_z_s"] forState: UIControlStateNormal];
                }else {
                    [cell.btnFirst setImage:[UIImage imageNamed:@"up_s"] forState: UIControlStateNormal];
                }
            } else {
                if (indexPath.row == 0) {
                    [cell.btnSecond setImage:[UIImage imageNamed:@"z_a_s"] forState:UIControlStateNormal];
                }else {
                    [cell.btnSecond setImage:[UIImage imageNamed:@"down_s"] forState:UIControlStateNormal];
                }
            }
        }
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)btnFirstClickedSort:(id)sender {
    _searchBar.text = @"";
    NSIndexPath * ip = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    
    lastSortedIndex = (int)[sender tag]+1;
    lastSortedUp = true;
    
    for (int i = 0; i<5; i++) {
        NSIndexPath * ip1 = [NSIndexPath indexPathForRow:i inSection:0];
        VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[sortView.tblView cellForRowAtIndexPath:ip1];
        if (i == 0) {
            [cell.btnFirst setImage:[UIImage imageNamed:@"a_z_us"] forState:UIControlStateNormal];
            [cell.btnSecond setImage:[UIImage imageNamed:@"z_a_us"] forState:UIControlStateNormal];
        }else {
            [cell.btnFirst setImage:[UIImage imageNamed:@"up_us"] forState:UIControlStateNormal];
            [cell.btnSecond setImage:[UIImage imageNamed:@"down_us"] forState:UIControlStateNormal];
        }
        
    }
    VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[sortView.tblView cellForRowAtIndexPath:ip];
    if ([sender tag] == 0) {
        [cell.btnFirst setImage:[UIImage imageNamed:@"a_z_s"] forState:UIControlStateNormal];
    }else {
        [cell.btnFirst setImage:[UIImage imageNamed:@"up_s"] forState:UIControlStateNormal];
    }
    
    
    if ([sender tag] == 0) {
        //userArray = [Users getAllByTitleAsc:true];
        [self sortWithName:YES];

    } else if ([sender tag] == 1) {
        //userArray = [Users getAllByBoookmarkAsc:true];
        [self sortWithBookMarks:YES];
        
    } else if ([sender tag] == 2) {
        [self sortWithRewards:YES];

//        userArray = [Users getAllByRewardsAsc:true];
    } else if ([sender tag] == 3) {
        userArray = [Users getAllByVideoTourAsc:true];
    }
    
    //[collViewMain reloadData];
    isSortShown = NO;
    [subMenuView removeFromSuperview];
}

-(void)sortWithBookMarks :(BOOL)isAscending {
    self.allRecomendedUsers = [[self.allRecomendedUsers sortedArrayUsingComparator:^NSComparisonResult(UserModel *a, UserModel *b) {
        int aBooks = [a.total_bookmark intValue];
        int bBooks = [b.total_bookmark intValue];

        if (isAscending) {
            return aBooks > bBooks;
        }
        else{
            return aBooks < bBooks;
        }
    }] mutableCopy];
    
    [collViewMain reloadData];

    
}
-(void)sortWithName :(BOOL)isAscending {
    self.allRecomendedUsers = [[self.allRecomendedUsers sortedArrayUsingComparator:^NSComparisonResult(UserModel *a, UserModel *b) {
        NSString* aBooks = a.full_name;
        NSString* bBooks = b.full_name;
        
        if (isAscending) {
            return [aBooks caseInsensitiveCompare:bBooks];
        }
        else{
            return [bBooks caseInsensitiveCompare:aBooks];
        }
    }] mutableCopy];
    
    [collViewMain reloadData];
    
    
}

-(void)sortWithRewards :(BOOL)isAscending {
    self.allRecomendedUsers = [[self.allRecomendedUsers sortedArrayUsingComparator:^NSComparisonResult(UserModel *a, UserModel *b) {
        int aBooks = [a.credits intValue];
        int bBooks = [b.credits intValue];
        
        if (isAscending) {
            return aBooks > bBooks;
        }
        else{
            return aBooks < bBooks;
        }
    }] mutableCopy];
    
    [collViewMain reloadData];
    
    
}

-(void)btnSecondClickedSort:(id)sender {
    _searchBar.text = @"";
    NSIndexPath * ip = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    
    lastSortedIndex = (int)[sender tag]+1;
    lastSortedUp = false;
    for (int i = 0; i<5; i++) {
        NSIndexPath * ip1 = [NSIndexPath indexPathForRow:i inSection:0];
        VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[sortView.tblView cellForRowAtIndexPath:ip1];
        if (i == 0) {
            [cell.btnFirst setImage:[UIImage imageNamed:@"a_z_us"] forState:UIControlStateNormal];
            [cell.btnSecond setImage:[UIImage imageNamed:@"z_a_us"] forState:UIControlStateNormal];
        }else {
            [cell.btnFirst setImage:[UIImage imageNamed:@"up_us"] forState:UIControlStateNormal];
            [cell.btnSecond setImage:[UIImage imageNamed:@"down_us"] forState:UIControlStateNormal];
        }
        
    }
    VideoSortTableViewCell *cell = (VideoSortTableViewCell*)[sortView.tblView cellForRowAtIndexPath:ip];
    if ([sender tag] == 0) {
        [cell.btnSecond setImage:[UIImage imageNamed:@"z_a_s"] forState:UIControlStateNormal];
    }else {
        [cell.btnSecond setImage:[UIImage imageNamed:@"down_s"] forState:UIControlStateNormal];
    }
    
    
    if ([sender tag] == 0) {
        //userArray = [Users getAllByTitleAsc:false];
        [self sortWithName:NO];
    } else if ([sender tag] == 1) {
        [self sortWithBookMarks:NO];

//        userArray = [Users getAllByBoookmarkAsc:false];
    } else if ([sender tag] == 2) {
        [self sortWithRewards:NO];

//        userArray = [Users getAllByRewardsAsc:false];
    } else if ([sender tag] == 3) {
        userArray = [Users getAllByVideoTourAsc:false];
    }
    //[collViewMain reloadData];
    isSortShown = NO;
    [subMenuView removeFromSuperview];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

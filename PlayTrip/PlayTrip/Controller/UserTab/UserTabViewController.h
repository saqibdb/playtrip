
#import <UIKit/UIKit.h>
#import "PlayTripManager.h"
#import "PopularVideoSort.h"
#import "MenuButtonViewController.h"

@interface UserTabViewController : MenuButtonViewController <UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource> {
    BOOL isSortShown;
    PopularVideoSort * sortView;
    IBOutlet UICollectionView *collViewMain;
    UIView * subMenuView;
    
    int lastSortedIndex;
    BOOL lastSortedUp;
    BOOL bookmarkUpdated;
}
@property (nonatomic) UISearchBar *searchBar;

@property (nonatomic) NSMutableArray *allRecomendedUsers;
@property (nonatomic) NSMutableArray *allRecomendedUsersOrg;




+(UserTabViewController *)initViewController;

@end

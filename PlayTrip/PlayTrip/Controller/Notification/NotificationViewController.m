
#import "NotificationViewController.h"
#import "NotificationCell.h"
#import "ColorConstants.h"
#import "Localisator.h"

@interface NotificationViewController ()
@end

@implementation NotificationViewController

+(NotificationViewController *)initViewController {
    NotificationViewController * controller = [[NotificationViewController alloc] initWithNibName:@"NotificationViewController" bundle:nil];
    [controller setTitle:LOCALIZATION(@"notification")];

    return controller;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = false;
    [self registerNibs];
    [self setNavBarItems];
}

-(void)registerNibs {
    [tblView registerNib:[UINib nibWithNibName:@"NotificationCell" bundle:nil] forCellReuseIdentifier:@"NotificationCell"];
}

-(void)setNavBarItems {
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[ColorConstants appBrownColor]}];
    
    UIImage *buttonImage = [UIImage imageNamed:@"back.png"];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:buttonImage forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    [button addTarget:self action:@selector(backClicked) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = customBarItem;
    
    UIBarButtonItem * clearButton = [[UIBarButtonItem alloc] initWithTitle:@"Clear" style:UIBarButtonItemStylePlain target:self action:nil];
    clearButton.tintColor = [ColorConstants appBrownColor];
    self.navigationItem.rightBarButtonItem = clearButton;
}

-(void)backClicked {
    [self.navigationController popViewControllerAnimated:true];
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NotificationCell * cell = (NotificationCell *)[tableView dequeueReusableCellWithIdentifier:@"NotificationCell"];
    if (cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"NotificationCell" owner:self options:nil];
        for (id currentObject in topLevelObjects) {
            if ([currentObject isKindOfClass:[UITableViewCell class]]) {
                cell =  (NotificationCell *) currentObject;
                break;
            }
        }
    }
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
//        NotificationCell * cell = (NotificationCell *)[tableView cellForRowAtIndexPath:indexPath];
//        cell.contentView.layer.backgroundColor = [ColorConstants appBrownColor].CGColor;
//        cell.lblTime.textColor = [UIColor whiteColor];
//        cell.lblMain.textColor = [UIColor whiteColor];
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//        NotificationCell * cell = (NotificationCell *)[tableView cellForRowAtIndexPath:indexPath];
//        cell.contentView.layer.backgroundColor = [UIColor clearColor].CGColor;
//        cell.lblTime.textColor = [UIColor blackColor];
//        cell.lblMain.textColor = [UIColor blackColor];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end

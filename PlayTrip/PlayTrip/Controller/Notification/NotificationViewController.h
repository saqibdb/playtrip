
#import <UIKit/UIKit.h>

@interface NotificationViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    IBOutlet UITableView *tblView;
}

+(NotificationViewController *)initViewController;

@end

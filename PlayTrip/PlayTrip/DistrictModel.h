//
//  District.h
//  PlayTrip
//
//  Created by ibuildx on 5/31/17.
//  Copyright © 2017 SamirMAC. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "Country.h"

@protocol DistrictModel;
@interface DistrictModel : JSONModel

@property (nonatomic) NSString *_id;
@property (nonatomic) NSString<Optional> *name;
//@property (nonatomic) Country<Optional> *country;
@property (nonatomic) NSString<Optional> *country;



@property (nonatomic) NSNumber<Optional> *sqlId;
@property (nonatomic) NSNumber<Optional> *__v;
@property (nonatomic) NSString<Optional> *create_date;
@property (nonatomic) NSNumber<Optional> *is_deleted;
@property (nonatomic) NSNumber<Optional> *status;
@property (nonatomic) NSString<Optional> *image;


/*
 
 
 "_id": "592dda638a82c0171c0e6478",
 "name": "Midlands",
 "country": "592dd05c3e01330be8376d63",
 "sqlId": 4120,
 "__v": 0,
 "create_date": "2017-05-30T20:47:31.920Z",
 "is_deleted": false,
 "status": false,
 "image": ""
 
 */

@end

//
//  AttractionModel.h
//  PlayTrip
//
//  Created by ibuildx on 6/6/17.
//  Copyright © 2017 SamirMAC. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "UserModel.h"
#import "Country.h"
#import "DistrictModel.h"
#import "CityModel.h"
#import "DraftAttraction.h"
#import "AttractionDataModel.h"


@protocol AttractionModel;
@interface AttractionModel : JSONModel


@property (nonatomic) NSString *_id;


@property (nonatomic) NSString<Optional> *name;


@property (nonatomic) NSString<Optional> *loc_long;
@property (nonatomic) NSString<Optional> *loc_lat;
@property (nonatomic) NSString<Optional> *last_updated;
@property (nonatomic) NSString<Optional> *create_date;
@property (nonatomic) NSString<Optional> *geotype;
@property (nonatomic) NSString<Optional> *cover_photo_url;
@property (nonatomic) NSString<Optional> *descriptionAM;
@property (nonatomic) NSString<Optional> *phone_no;
@property (nonatomic) NSString<Optional> *hours_info;
@property (nonatomic) NSString<Optional> *web_url;
@property (nonatomic) NSString<Optional> *address;
@property (nonatomic) NSString<Optional> *place_id;



@property (nonatomic) NSNumber<Optional> *__v;
@property (nonatomic) NSNumber<Optional> *is_deleted;
@property (nonatomic) NSNumber<Optional> *total_view;
@property (nonatomic) NSNumber<Optional> *total_share;
@property (nonatomic) NSNumber<Optional> *total_bookmark;
@property (nonatomic) NSString<Optional> *detail;


@property (nonatomic) UserModel<Optional> *user;

@property (nonatomic) NSMutableArray<Optional> *remarks;
@property (nonatomic) NSMutableArray<Optional> *tags;
@property (nonatomic) NSMutableArray<Optional> *category;

@property (nonatomic) NSMutableArray<UserModel>  *bookmark_users;

@property (nonatomic) Country<Optional> *country_id;
@property (nonatomic) DistrictModel<Optional> *district_id;
@property (nonatomic) CityModel<Optional> *city_id;


@property (nonatomic) NSMutableArray<Optional, AttractionDataModel> *attr_data;


@property (nonatomic) NSNumber<Optional> *remuneration_amount;
@property (nonatomic) NSNumber<Optional> *refer_count;
@property (nonatomic) NSNumber<Optional> *total_attractions;

@property (nonatomic) NSNumber<Optional> *day;

- (instancetype)initWithDraftAttraction:(DraftAttraction*)da;


/*
 
 
 
 "remuneration_amount": 0,
 "refer_count": 0,
 "total_attractions": 0,
 "total_view": 0,
 "total_share": 0,
 "bookmark_users": [],
 "total_bookmark": 0,
 "id": "593696b53222194366bc59fd"
 */

/*
 
 _id	:	59314dd40064f473097811b1
 
	user		{34}
 
 name	:	ffffff
 
	district_id		{9}
 
	country_id		{6}
 
	city_id		{7}
 
 __v	:	0
 
 loc_long	:
 
 loc_lat	:
 
 is_deleted	:	false
 
 last_updated	:	2017-06-02T11:36:52.788Z
 
 create_date	:	2017-06-02T11:36:52.783Z
 
	remarks		[0]
	(empty array)
 
 geotype	:	Point
 
 total_view	:	0
 
 total_share	:	0
 
	bookmark_users		[0]
 
 total_bookmark	:	0
 
	tags		[0]
 
 cover_photo_url	:	https://playtrip.medias.s3.amazonaws.com/E573A742-DCE2-49C8-B1F6-7738480DD403-2363-0000032FE39C32FC.jpg
 
 description	:	fffffg
 
 phone_no	:	85523
 
 hours_info	:
 
 web_url	:
 
 address	:	Rawalpindi, (null) Rawalpindi
 
 detail	:
 
 place_id	:
 
	category		[0]

 
 
 */

@end

//
//  CityModel.h
//  PlayTrip
//
//  Created by ibuildx on 5/31/17.
//  Copyright © 2017 SamirMAC. All rights reserved.
//

#import <JSONModel/JSONModel.h>
#import "DistrictModel.h"
@protocol CityModel;
@interface CityModel : JSONModel

@property (nonatomic) NSString *_id;
@property (nonatomic) NSString<Optional> *name;
//@property (nonatomic) DistrictModel<Optional> *district;
@property (nonatomic) NSString<Optional> *district;



@property (nonatomic) NSNumber<Optional> *sqlId;
@property (nonatomic) NSNumber<Optional> *__v;
@property (nonatomic) NSString<Optional> *last_updated;
@property (nonatomic) NSNumber<Optional> *is_deleted;


/*
 
 "_id": "592de0a0ee185a1710ee63ec",
 "name": "Nadigaon",
 "district": "592dda738a82c0171c0e746a",
 "sqlId": 5000,
 "__v": 0,
 "last_updated": "2017-05-30T21:14:08.797Z",
 "is_deleted": false
 
 */

@end
